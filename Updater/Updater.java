/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2018 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.regex.Pattern;

import javax.swing.ProgressMonitorInputStream;

public class Updater
{
    private static final String VERSION = "1.0.0";
    private static final String URL = "http://lobby.siedler3.net/install/versionUpdate.php";

	public static void main(String[] args)
	{
	    new AutoUpdate().updateProgramIfNecessary();
	    System.exit(1); //only reached in case of error or no update
	}

	private static class AutoUpdate
	{
	    private UpdateDownloadV2 updateDownload;

	    private String downloadURL = "";

	    /**
	     * Testet, ob das Programm inaktuell ist, läd die neuen Dateien vom Server
	     * und ruft dann das Update-Programm auf.
	     * @param settlersLobby
	     * @param sF
	     * @param showNoUpdateAvailableMessage
	     *          True, wenn bei keinem neuen Update eine Nachricht angezeigt
	     *          werden soll, andernfalls false.
	     */
	    public void updateProgramIfNecessary()
	    {
	        if (!checkIfVersionIsUpToDate(VERSION))
	        {
	            //die downloadURL wurde in checkIfVersionIsUpToDate() gesetzt
	            if (downloadURL == null)
	            {
	                return;
	            }
	            if (!downloadURL.startsWith("http://lobby.siedler3.net/")) ////$NON-NLS-1$
	            {
	                return;
	            }
	            updateDownload = new UpdateDownloadV2(downloadURL);

	            File[] signedUpdateFile = updateDownload.downloadSignedFile();
	            if (signedUpdateFile != null)
	            {
	                //check signature
	                if (Gpg.verifySignature(signedUpdateFile[0], signedUpdateFile[1]))
	                {
	                }
	                else
	                {
	                    return;
	                }

	                if (!callUpdater(signedUpdateFile[0]))
	                {
	                    return;
	                }
	                else
	                {
	                    System.exit(0);
	                }
	            }
	        }

	    }

	    private boolean callUpdater(File installer)
	    {
	        String[] command = {
	            "cmd", //$NON-NLS-1$
	            "/c", //$NON-NLS-1$
	            installer.getAbsolutePath(),
	            "/S", //silent mode
	            "/INSTDIR", //provide the current dir as install dir
	            System.getProperty("user.dir")
	        };
	        //the installer will restart the alobby after updating

	        try
	        {
	            new ProcessBuilder(command).start();
	        }
	        catch (IOException io)
	        {
	            System.out.println(io.getMessage());
	            return false;
	        }
	        return true;
	    }

	    /**
	     * Überprüft, ob die Programmversion mit der aktuellen Version übereinstimmt.
	     * Die Programmversion wird über eine Website verglichen.
	     *
	     * @param version
	     *
	     * @return true, wenn keine neue Version verfügbar ist oder ein Fehler entstanden ist
	     *         , sonst false
	     */
	    private boolean checkIfVersionIsUpToDate(String version)
	    {
	        URL url = null;
	        BufferedReader bufferedReader;
	        String website;
	        String content = ""; //$NON-NLS-1$
	        String[] splitted = new String[2];
	        int[] versionSplitted = new int[3];
	        int[] currentVersionSplitted = new int[3];
	        String currentVersion = null;
	        int[] currentTestVersionSplitted = new int[3];
	        String currentTestVersion = null;
	        String downloadTestURL = "";
	        boolean isTestVersion = false;
	        int[] currentMinorTestVersionSplitted = new int[4];
	        String currentMinorTestVersion = null;
	        String downloadMinorTestURL = "";
	        boolean isMinorTestVersion = false;

	        try
	        {
	            url = new URL(URL);
	        }catch (MalformedURLException e)
	        {
	            return true;
	        }

	        try
	        {
	            bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
	            while ((website = bufferedReader.readLine()) != null)
	            {
	                content += website;
	            }
	            bufferedReader.close();
	        }catch (IOException e)
	        {
	            return true;
	        }

	    	if (content != null && content.length() > 0)
	    	{
	    	    splitted = content.split(";");
	    	    if (splitted.length < 2)
	    	    {
	    	    	//wrong response
	    	    	return true;
	    	    }

	    	    currentVersion = splitted[0];
	    	    downloadURL   = splitted[1];

	    	    currentVersionSplitted = splitVersion(currentVersion);
	    	    versionSplitted = splitVersion(version);

	            if (currentVersionSplitted == null || versionSplitted == null ||
		                currentVersionSplitted.length < 2 || versionSplitted.length < 2)
	            {
	            	return true;
	            }

	            isMinorTestVersion = versionSplitted.length == 4;

	            if (splitted.length >= 6)
	            {
	            	currentMinorTestVersion = splitted[4];
	            	downloadMinorTestURL   = splitted[5];
	            	currentMinorTestVersionSplitted = splitVersion(currentMinorTestVersion);
		            if (isMinorTestVersion && currentMinorTestVersionSplitted != null && currentMinorTestVersionSplitted.length >= 3)
		            {
		            	//We are only interested in the test version when it has
		            	//a higher version number than the stable one.
		            	if (!compareVersion(currentVersionSplitted, currentMinorTestVersionSplitted))
		            	{
		            		currentVersion = currentMinorTestVersion;
		            		currentVersionSplitted = currentMinorTestVersionSplitted;
		            		downloadURL = downloadMinorTestURL;
		            	}
		            }
	            }

	            isTestVersion = versionSplitted[2] >= 90;

	            if (splitted.length >= 4)
	            {
	            	currentTestVersion = splitted[2];
		            downloadTestURL   = splitted[3];
		            currentTestVersionSplitted = splitVersion(currentTestVersion);
		            if (isTestVersion && currentTestVersionSplitted != null && currentTestVersionSplitted.length >= 2)
		            {
		            	//We are only interested in the test version when it has
		            	//a higher version number than the stable one.
		            	if (!compareVersion(currentVersionSplitted, currentTestVersionSplitted))
		            	{
		            		currentVersion = currentTestVersion;
		            		currentVersionSplitted = currentTestVersionSplitted;
		            		downloadURL = downloadTestURL;
		            	}
		            }
	            }
	            return compareVersion(versionSplitted, currentVersionSplitted);

	        }
	        else
	        {
	            return true;
	        }
	    }

	    /**
	     * Returns false, if the newVersion is higher than the baseVersion.
	     * Otherwise returns true.
	     * @param baseVersion
	     * @param newVersion
	     * @return
	     */
	    private boolean compareVersion(int[] baseVersion, int[] newVersion)
	    {
	        /*True wenn:
	        1: 1. Stelle: Server-Version ist größer als Rechner-Version
	           2. Stelle: egal
	           3. Stelle: egal
	        2: 1. Stelle: Server-Version ist gleich der Rechner-Version
	           2. Stelle: Server-Version ist größer als Rechner-Version
	           3. Stelle: egal
	        3: 1. Stelle: Server-Version ist gleich der Rechner-Version
	           2. Stelle: Server-Version ist gleich der Rechner-Version
	           3. Stelle: Server-Version ist größer als Rechner-Version
	        4: 1. Stelle: Server-Version ist gleich der Rechner-Version
	           2. Stelle: Server-Version ist gleich der Rechner-Version
	           3. Stelle: Server-Version ist gleich der Rechner-Version
	           4. Stelle: Server-Version ist größer als Rechner-Version*/
	      if (newVersion[0] > baseVersion[0]
	                  ||(newVersion[0] == baseVersion[0] &&
	                		  newVersion[1] >  baseVersion[1])
	                  ||(newVersion[0] == baseVersion[0] &&
	                		  newVersion[1] == baseVersion[1] &&
	                		  newVersion[2] >  baseVersion[2])
	                  ||(newVersion.length >= 4 &&
	                  		  newVersion[0] == baseVersion[0] &&
	        		          newVersion[1] == baseVersion[1] &&
			        		  newVersion[2] == baseVersion[2] &&
	        				  (baseVersion.length <= 3
	        				  || newVersion[3] >  baseVersion[3])))
	      {
	          return false;
	      }
	      else
	      {
	          return true;
	      }
	    }
	    
	    /**
	     * Zum Vergleichen der Versionen werden Integer benötigt. Es wird diese
	     * Methode genommen, um die If-Bedingungen möglichst übersichtlich zu halten.
	     * Der String wird gesplittet und als Integer-Array zurückgegeben.
	     * @param string
	     * @return
	     */
	    private int[] splitVersion(String string)
	    {
	        if (!Pattern.matches("[0-9]{1,2}\\.[0-9]+\\.[0-9]+\\.*[0-9]*", string))
	        {
	            return null;
	        }

	        String[] splitted = string.split("\\.");
	        int[] splittedInt = new int[splitted.length];

	        for(int i = 0 ; i <= splitted.length-1 ; i++)
	        {
	            splittedInt[i] = Integer.parseInt(splitted[i]);
	        }

	        return splittedInt;
	    }
	}

	private static class UpdateDownloadV2
	{
	    private String downloadUrl;

	    /**
	     *
	     * @param settlersLobby
	     * @param downloadUrl URL, unter der das Update heruntergeladen werden kann
	     */
	    public UpdateDownloadV2(String downloadUrl)
	    {
	        this.downloadUrl = downloadUrl;
	    }

	    protected String getDownloadPath()
	    {
	        return downloadUrl;
	    }

	    protected URL getDownloadURL(String serverURL)
	    {
	        try
	        {
	            return new URL(serverURL);
	        }
	        catch (MalformedURLException mue)
	        {
	            return null;
	        }
	    }

	    /**
	     *
	     * @return the file and the corresponding .asc file containing the signature.
	     */
	    public File[] downloadSignedFile()
	    {
	        File[] result = null;

	        String serverURL = getDownloadPath();

	        URL url = null;
	        try
	        {
	            url = getDownloadURL(serverURL);
	        }
	        catch (Exception e)
	        {
	            return result;
	        }
	        if (url == null)
	        {
	            return result;
	        }

	        try
	        {
	            File tmpAlobbyDir = File.createTempFile("alobby_", ""); //$NON-NLS-1$ //$NON-NLS-2$
	            // createTempFile erzeugt eine Datei, kein Verzeichnis
	            // allerdings erzeugt es einen zufälligen Namen, der noch
	            // nicht vorhanden ist. Um die Datei in ein Verzeichnis umzuwandeln,
	            // wird das TmpFile gelöscht und als Verzeichnis angelegt
	            if (!tmpAlobbyDir.delete() || !tmpAlobbyDir.mkdir())
	            {
	                return result;
	            }

	            try
	            {
	                result = downloadSignedFileToDir(url, tmpAlobbyDir);
	            }
	            catch (Exception e)
	            {
	                tmpAlobbyDir.deleteOnExit();
	                return result;
	            }
	            if (result == null)
	            {
	                tmpAlobbyDir.deleteOnExit();
	                return result;
	            }
	        }
	        catch (IOException e)
	        {
	        }
	        return result;
	    }

	    private File[] downloadSignedFileToDir(URL url, File directory)
	    {
	        File[] result = new File[2];
	        URL urlForSignature;

	        URLConnection conn = null;
	        try
	        {
	            conn = url.openConnection();
	            conn.getContentLength(); //resolve any redirects
	            urlForSignature = new URL(conn.getURL().toExternalForm() + ".asc");
	        }
	        catch (Exception e)
	        {
	            return null;
	        }

	        result[0] = downloadFileToDir(conn.getURL(), directory);
	        result[1] = downloadFileToDir(urlForSignature, directory);

	        if (result[0] == null || result[1] == null)
	        {
	            return null;
	        }
	        return result;
	    }

	    protected File downloadFileToDir(URL url, File directory)
	    {
	        boolean deleteResultFile = false;

	        File result = null;
	        if (!directory.exists())
	        {
	            directory.mkdir();
	        }

	        //Die Datei wird über Input- und OutputStreams heruntergeladen
	        //und als File gespeichert
	        OutputStream out = null;
	        URLConnection conn = null;
	        ProgressMonitorInputStream in = null;
	        try
	        {
	            conn = url.openConnection();
	            int totalLength = conn.getContentLength();
	            //conn.getURL() liefert die aktuelle URL, die sich durch eventuelle
	            //redirects von der initial übergebenen URL unterscheiden kann
	            //(erst nachdem durch conn.getContentLength() alle Header
	            // gelesen wurden (dadurch auch der redirect))
	            String filename = getFileNameFromURL(conn.getURL());
	            result = new File(directory, filename);
	            out = new BufferedOutputStream(
	                    new FileOutputStream(result));

	            in = new ProgressMonitorInputStream(
	                        null,
	                        "downloading " + result.getName(), //$NON-NLS-1$
	                        conn.getInputStream());
	            if (totalLength > 0)
	            {
	                // Die Gesamtlänge muss manuell gesetzt werden,
	                // da sonst intern available() abgefragt wird,
	                // das einen anderen Wert liefert
	                in.getProgressMonitor().setMaximum(totalLength);
	                // Das Dialogfenster soll sofort erscheinen,
	                // daher wird setProgress(0) manuell aufgerufen,
	                // falls eventuell mal der tatsächliche download hängt
	                in.getProgressMonitor().setMillisToDecideToPopup(0);
	                in.getProgressMonitor().setMillisToPopup(0);
	                in.getProgressMonitor().setProgress(0);
	            }

	            byte[] buffer = new byte[4096];
	            int numRead;
	            long numWritten = 0;

	            while ((numRead = in.read(buffer)) != -1)
	            {
	                out.write(buffer, 0, numRead);
	                numWritten += numRead;
	            }
	        }
	        catch (InterruptedIOException ie)
	        {
	            //die Datei wurde nicht komplett gelesen, also kann sie gelöscht werden
	            deleteResultFile = true;
	            throw new RuntimeException();
	        }
	        catch (Exception e)
	        {
	            // was auch immer schief gegangen ist, die Datei ist nicht komplett
	            // und damit nutzlos
	            deleteResultFile = true;
	        }
	        finally
	        {
	            try
	            {
	                if (in != null)
	                {
	                    in.close();
	                }
	                if (out != null)
	                {
	                    out.close();
	                }
	                //Das Löschen darf erst passieren, nachdem out geschlossen ist,
	                //anderfalls wird die Datei nicht gelöscht.
	                //daher der Umweg über das Flag "deleteResultFile"
	                if (deleteResultFile && result != null)
	                {
	                    result.delete();
	                    result = null;
	                }
	            }
	            catch (IOException ioe)
	            {
	            }
	        }
	        return result;
	    }

	    private String getFileNameFromURL(URL url)
	    {
	        String filename = "dummy.zip"; //$NON-NLS-1$

	        String decodedUrlWithoutHost;
	        try
	        {
	            decodedUrlWithoutHost = url.toURI().getPath();
	            decodedUrlWithoutHost = decodedUrlWithoutHost.replace('\uFFFD', '_');
	            filename = new File(decodedUrlWithoutHost).getName();
	        }
	        catch (Exception e)
	        {
	        }
	        return filename;
	    }


	}

	private static class Gpg
	{
	    private static final String gpgFilePath = "gpg.exe"; //$NON-NLS-1$
	    private static final String gpgPublicKeyFilePath = "alobby_public_key.gpg"; //$NON-NLS-1$
	    private File directory;
	    private File gpgExe;
	    private File publicKeyFile;
	    private File keyRing;

	    protected boolean extractGpg() throws IOException
	    {
	        boolean result = false;
	        directory = ExtractFile.createTempDir();
	        directory.deleteOnExit();
	        gpgExe = ExtractFile.extract(Gpg.class.getResourceAsStream(gpgFilePath), new File(gpgFilePath).getName(), directory);
	        gpgExe.deleteOnExit();
	        gpgExe.getParentFile().deleteOnExit();
	        publicKeyFile = ExtractFile.extract(Gpg.class.getResourceAsStream(gpgPublicKeyFilePath),
	                                            new File(gpgPublicKeyFilePath).getName(),
	                                            directory);
	        publicKeyFile.deleteOnExit();
	        keyRing = new File(directory, "keyring");
	        keyRing.deleteOnExit();
	        new File(keyRing.getAbsolutePath() + ".bak").deleteOnExit();
	        new File(keyRing.getAbsolutePath() + ".lock").deleteOnExit();


	        String[] commandKeyRing = {
	                        gpgExe.getAbsolutePath(),
	                        "--armor",
	                        "--no-default-keyring",
	                        "--keyring",
	                        keyRing.getAbsolutePath(),
	                        "--import",
	                        publicKeyFile.getAbsolutePath()
	        };

	        ProcessBuilder pb = new ProcessBuilder(Arrays.asList(commandKeyRing));
	        pb.redirectErrorStream(true);
	        Process p = pb.start();

	        //CP850 ist die standard westeuropäische Codepage die bei DOS benutzt wird
	        //zumindest in unserer Gegend
	        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(), "CP850")); //$NON-NLS-1$

	        String line;
	        while ((line = reader.readLine()) != null)
	        {
	        }

	        try
	        {
	            if (p.waitFor() != 0)
	            {
	            }
	            else
	            {
	                result = true;
	            }
	        }
	        catch (InterruptedException e)
	        {
	        }
	        return result;
	    }

	    public boolean verify(File file, File signature) throws IOException
	    {
	        boolean result = false;
	        String[] commandVerify = {
	                        gpgExe.getAbsolutePath(),
	                        "--armor",
	                        "--no-default-keyring",
	                        "--keyring",
	                        keyRing.getAbsolutePath(),
	                        "--verify",
	                        signature.getAbsolutePath(),
	                        file.getAbsolutePath()
	        };

	        ProcessBuilder pb = new ProcessBuilder(Arrays.asList(commandVerify));
	        pb.redirectErrorStream(true);
	        Process p = pb.start();

	        //CP850 ist die standard westeuropäische Codepage die bei DOS benutzt wird
	        //zumindest in unserer Gegend
	        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(), "CP850")); //$NON-NLS-1$

	        String line;
	        while ((line = reader.readLine()) != null)
	        {
	        }

	        try
	        {
	            if (p.waitFor() != 0)
	            {
	            }
	            else
	            {
	                result = true;
	            }
	        }
	        catch (InterruptedException e)
	        {
	        }

	        return result;

	    }

	    public static boolean verifySignature(File file, File signature)
	    {
	        boolean result = false;
	        try
	        {
	            Gpg gpg = new Gpg();
	            gpg.extractGpg();
	            result = gpg.verify(file, signature);
	        }
	        catch (Exception e)
	        {
	            result = false;
	        }
	        return result;
	    }

	}


	public static class ExtractFile
	{
	    public static File createTempDir() throws IOException
	    {
	        File dir = null;
	        dir = File.createTempFile("alobby_", null);
	        if (dir.delete())
	        {
	            dir.mkdir();
	        }
	        return dir;
	    }

	    public static File extract(InputStream resource, String filename, File directory) throws IOException
	    {
	        File result = null;
	        result = new File(directory, filename);
	        OutputStream out = null;
	        int readBytes;
	        byte[] buffer = new byte[4096];
	        try
	        {
	            out = new FileOutputStream(result);
	            while ((readBytes = resource.read(buffer)) > 0)
	            {
	                out.write(buffer, 0, readBytes);
	            }
	        }
	        finally
	        {
	            resource.close();
	            out.close();
	        }
	        return result;
	    }
	}

}
