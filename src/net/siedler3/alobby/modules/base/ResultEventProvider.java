package net.siedler3.alobby.modules.base;

//not sure if this is really needed
public interface ResultEventProvider
{
    public void setListener(ResultEventListener listener);
    public ResultCode[] supportedResultCodes();
}
