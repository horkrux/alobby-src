package net.siedler3.alobby.modules.base;

import java.util.Arrays;
import java.util.EventObject;

public class ResultEvent extends EventObject
{
    private ResultCode result;
    private String errorMessage;

    public ResultEvent(ResultEventProvider source, ResultCode result)
    {
        super(source);
        this.result = result;

    }
    public ResultEvent(ResultEventProvider source, ResultCode result, String errorMessage)
    {
        this(source, result);
        this.errorMessage = errorMessage;
    }
    @Override
    public ResultEventProvider getSource()
    {
        return (ResultEventProvider)source;
    }
    public ResultCode getResult()
    {
        return result;
    }
    public String getErrorMessage()
    {
        return errorMessage;
    }

    @Override
    public String toString()
    {
        return getClass().getName() + "[" + source + ", " + result + ", " + errorMessage + "]";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((errorMessage == null) ? 0 : errorMessage.hashCode());
        result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
        result = prime * result + ((source == null) ? 0 : source.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ResultEvent other = (ResultEvent) obj;
        if (errorMessage == null)
        {
            if (other.errorMessage != null)
                return false;
        } else if (!errorMessage.equals(other.errorMessage))
            return false;
        if (result != other.result)
            return false;
        if (source == null)
        {
            if (other.source != null)
                return false;
        } else if (!source.equals(other.source))
            return false;
        return true;
    }

    public static void notifyResultOk(ResultEventListener listener, ResultEventProvider source)
    {
        ResultEvent event = new ResultEvent(source, ResultCode.OK);
        assert(isSupportedEvent(source, event));
        listener.onResultEvent(event);
    }

    public static void notifyResultFailed(ResultEventListener listener, ResultEventProvider source, String errorMessage)
    {
        ResultEvent event = new ResultEvent(source, ResultCode.FAILED, errorMessage);
        assert(isSupportedEvent(source, event));
        listener.onResultEvent(event);
    }

    public static void notifyResultAborted(ResultEventListener listener, ResultEventProvider source)
    {
        ResultEvent event = new ResultEvent(source, ResultCode.ABORTED);
        assert(isSupportedEvent(source, event));
        listener.onResultEvent(event);
    }

    public static void notifyResult(ResultEventListener listener, ResultEventProvider source, ResultCode result)
    {
        ResultEvent event = new ResultEvent(source, result);
        assert(isSupportedEvent(source, event));
        listener.onResultEvent(event);
    }

    public static void notifyResult(ResultEventListener listener, ResultEventProvider source, ResultCode result, String errorMessage)
    {
        ResultEvent event = new ResultEvent(source, result, errorMessage);
        assert(isSupportedEvent(source, event));
        listener.onResultEvent(event);
    }

    public static void notifySameResult(ResultEventListener listener, ResultEventProvider source, ResultEvent event)
    {
        assert(isSupportedEvent(source, event));
        ResultEvent event2 = new ResultEvent(source, event.getResult(), event.getErrorMessage());
        listener.onResultEvent(event2);
    }

    public static boolean isSupportedEvent(ResultEventProvider source, ResultEvent event)
    {
        return isSupportedEvent(source, event.getResult());
    }

    public static boolean isSupportedEvent(ResultEventProvider source, ResultCode code)
    {
        return Arrays.asList(source.supportedResultCodes()).contains(code);
    }






}
