package net.siedler3.alobby.modules.login;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.i18n.I18n;

public class InputAuthCodeDialog extends JDialog implements ActionListener, PropertyChangeListener
{
    //use a non_capturing group to check for "confirm" or "CONFIRM" (but not cOnFiRm)
    private static final Pattern pattern = Pattern.compile("\\s*(/msg NickServ (?:confirm|CONFIRM) )?(\\S+)\\s*");
    private String typedText = null;
    private JTextField textField;
    private GUI gui;
    private JOptionPane optionPane;

    /**
     * Returns null if the typed string was invalid; otherwise, returns the
     * string as the user entered it.
     */
    public String getValidatedText()
    {
        return typedText;
    }

    /** Creates the reusable dialog. */
    public InputAuthCodeDialog(JFrame owner, GUI gui, SettlersLobby settlersLobby)
    {
        super(owner, true);
        this.gui = gui;
        setIconImage(gui.getIcon());

        setTitle(I18n.getString("GUI.LOGIN")); //$NON-NLS-1$

        textField = new JTextField();

        // Create an array of the text and components to be displayed.
        String msgString1 = I18n.getString("LoginFrame.MESSAGE_INPUT_AUTH_CODE"); //$NON-NLS-1$
        Object[] array = { msgString1, textField };

        // Create the JOptionPane.
        optionPane = new JOptionPane(array, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null, null,
                        null);

        // Make this dialog display it.
        setContentPane(optionPane);

        // Handle window closing correctly.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we)
            {
                /*
                 * Instead of directly closing the window, we're going to change
                 * the JOptionPane's value property.
                 */
                optionPane.setValue(JOptionPane.CANCEL_OPTION);
            }
        });

        // Ensure the text field always gets the first focus.
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent ce)
            {
                textField.requestFocusInWindow();
            }
        });

        // Register an event handler that puts the text into the option pane.
        textField.addActionListener(this);

        // Register an event handler that reacts to option pane state changes.
        optionPane.addPropertyChangeListener(this);
        pack();
    }

    /** This method handles events for the text field. */
    @Override
    public void actionPerformed(ActionEvent e)
    {
        optionPane.setValue(JOptionPane.OK_OPTION);
    }

    /** This method reacts to state changes in the option pane. */
    @Override
    public void propertyChange(PropertyChangeEvent e)
    {
        String prop = e.getPropertyName();

        if (isVisible() && (e.getSource() == optionPane)
                        && (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY.equals(prop)))
        {
            Object value = optionPane.getValue();

            if (value == JOptionPane.UNINITIALIZED_VALUE)
            {
                // ignore reset
                return;
            }

            // Reset the JOptionPane's value.
            // If you don't do this, then if the user
            // presses the same button next time, no
            // property change event will be fired.
            optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

            if (value.equals(JOptionPane.OK_OPTION))
            {
                typedText = textField.getText();
                Matcher m = pattern.matcher(typedText);
                if (m.matches())
                {
                    typedText = m.group(2);
                    // we're done; clear and dismiss the dialog
                    clearAndHide();
                }
                else
                {
                    // text was invalid
                    textField.selectAll();
                    gui.displayError(I18n.getString("InputAuthCodeDialog.ERROR_INVALID_FORMAT")); //$NON-NLS-1$
                    typedText = null;
                    textField.requestFocusInWindow();
                }
            } else
            { // user closed dialog or clicked cancel
                typedText = null;
                clearAndHide();
            }
        }
    }

    /** This method clears the dialog and hides it. */
    public void clearAndHide()
    {
        textField.setText(null);
        setVisible(false);
    }

    @Override
    public void setVisible(boolean b)
    {
        if (b)
        {
            setLocationRelativeTo(getParent());
        }
        super.setVisible(b);
    }

}
