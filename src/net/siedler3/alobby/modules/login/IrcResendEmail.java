package net.siedler3.alobby.modules.login;

import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_REGEX_RESEND_2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_REGEX_RESEND_EMAIL_BLOCKED_2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_REGEX_RESEND_EMAIL_SERVICE_DOWN_2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_REGEX_RESEND_NOT_DONE_2;

import java.util.regex.Matcher;

import net.siedler3.alobby.communication.IrcInterface;
import net.siedler3.alobby.communication.NickServCommand;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.modules.base.ResultEvent;

public class IrcResendEmail extends IrcNickServAccessorBase
{
    private String emailAddress;
    
    public IrcResendEmail(IrcInterface ircInterface, SettlersLobby settlersLobby)
    {
        super(ircInterface, settlersLobby);
    }
    
    public void resendEmail(String nick, String password)
    {
        emailAddress = null;
        doLogin(nick, password);
    }
    
    public String getEmailAddress()
    {
        return emailAddress;
    }

    private void doResendEmailCommand()
    {
        startObservation();
        scheduleTimeoutTask();
        NickServCommand.RESEND.transmitUsing(ircInterface, null);
    }
    
    @Override
    protected void onNotice(String sourceNick, String sourceLogin, String sourceHostname,
            String target, String notice)
    {
        if (ircInterface.isNickServe(sourceNick, sourceLogin, sourceHostname))
        {
            handleResendEmailMessage(notice);
        }
    }
    
    private void handleResendEmailMessage(String notice)
    {
        Matcher m = PATTERN_REGEX_RESEND_2.matcher(notice);
        Matcher m2 = PATTERN_REGEX_RESEND_EMAIL_BLOCKED_2.matcher(notice);
        if (m.matches())
        {
            emailAddress = m.group(1);
            resultOk();
        }
        else if (PATTERN_REGEX_RESEND_NOT_DONE_2.matcher(notice).matches())
        {
            resultFailed(notice);
        }
        else if (PATTERN_REGEX_RESEND_EMAIL_SERVICE_DOWN_2.matcher(notice).matches())
        {
            resultFailed(notice);
        }
        else if (m2.matches())
        {
            resultFailed(notice);
        }
    }
    
    

    @Override
    public void onResultEvent(ResultEvent event)
    {
        switch (event.getResult())
        {
            case OK:
                //if the server requires an authentication code,
                //then ircUserAuthenticator should normally return with
                //SPECIAL_REQUIRES_AUTH_CODE, not with ok
                //Ok says we are already logged in an everything is fine.
                //Nevertheless, even in this case try so send the "resend" command
                doResendEmailCommand();
                break;
            case SPECIAL_REQUIRES_AUTH_CODE:
                //need to be logged in, so send the password hoping that it is correct
                //and send the "resend" command
                ircUserAuthenticator.rawTransmitPassword();
                doResendEmailCommand();
                break;
            default:
                super.onResultEvent(event);
                return;
        }
    }

    @Override
    protected void onInternalResultEventOkAction()
    {
        throw new UnsupportedOperationException();
    }
    
    public boolean isResendEmailSupportedByServer()
    {
        return ircInterface.getIrcServerInfo().isRegistrationRequiresEmailAuthentication();
    }
}
