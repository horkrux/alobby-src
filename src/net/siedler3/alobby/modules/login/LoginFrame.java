package net.siedler3.alobby.modules.login;

import static net.siedler3.alobby.modules.base.ResultCode.ABORTED;
import static net.siedler3.alobby.modules.base.ResultCode.FAILED;
import static net.siedler3.alobby.modules.base.ResultCode.OK;
import static net.siedler3.alobby.modules.base.ResultCode.USER_REQUESTS_REGISTRATION;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.MessageFormat;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import net.siedler3.alobby.communication.IrcEventDispatcher;
import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.gui.additional.AlphaNumericAndUnderlinePlainDocument;
import net.siedler3.alobby.gui.additional.CapsLockFocusListener;
import net.siedler3.alobby.gui.additional.addCursorChangesToButton;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.modules.base.ResultCode;
import net.siedler3.alobby.modules.base.ResultEvent;
import net.siedler3.alobby.modules.base.ResultEventAdapter;
import net.siedler3.alobby.modules.base.ResultEventListener;
import net.siedler3.alobby.modules.base.ResultEventProvider;
import net.siedler3.alobby.util.TimerTaskCompatibility;

public class LoginFrame extends JFrame implements ResultEventListener, ResultEventProvider
{
    private static final ResultCode[] supportedResultCodes = {
        OK,
        FAILED,
        ABORTED,
        USER_REQUESTS_REGISTRATION
    };

	private final GUI gui;
	private SettlersLobby settlersLobby;
	private final Configuration config = Configuration.getInstance();
	private IrcUserAuthenticator ircUserAuthenticator;
	private IrcChangePassword ircChangePassword;
	private IrcResendEmail ircResendEmail;
	private IrcResetPassword ircResetPassword;

	private final JLabel lblUser = new JLabel(I18n.getString("GUI.USERNAME") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
	private final JLabel lblPassword = new JLabel(I18n.getString("GUI.PASSWORD") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
	private final JPanel pnlLogin = new JPanel();
	private final JButton btnLogin = new JButton(I18n.getString("GUI.LOGIN")); //$NON-NLS-1$
	private final JButton btnGoToRegister = new JButton(I18n.getString("GUI.REGISTER")); //$NON-NLS-1$
	private final JButton btnExtras = new JButton(I18n.getString("GUI.EXTRAS")); //$NON-NLS-1$
	private final JCheckBox storePasswordButton = new JCheckBox(I18n.getString("GUI.SAVE_PASSWORD")); //$NON-NLS-1$
	private final JPasswordField inputPassword = new JPasswordField();
	private final JTextField inputUser = new JTextField();
    private final JPopupMenu popupMenu = new JPopupMenu();
    private final JMenuItem menuitemChangePassword = new JMenuItem(I18n.getString("LoginFrame.CHANGE_PASSWORD")); //$NON-NLS-1$
    private final JMenuItem menuitemResendEmail = new JMenuItem(I18n.getString("LoginFrame.RESEND_EMAIL")); //$NON-NLS-1$
    private final JMenuItem menuitemSettings = new JMenuItem(I18n.getString("GUI.SETUP")); //$NON-NLS-1$
    private final JMenuItem menuitemResetPassword = new JMenuItem(I18n.getString("LoginFrame.FORGOTTEN_PASSWORD")); //$NON-NLS-1$
    
	private ResultEventListener listener;
	private InputAuthCodeDialog inputAuthDialog;
	private InputNewPasswordDialog inputNewPasswordDialog;
	private InputResetPasswordDialog inputResetPasswordDialog;
    private String newPassword;

	public LoginFrame(GUI gui, SettlersLobby settlersLobby)
	{
		super(ALobbyConstants.PROGRAM_NAME + " > " + I18n.getString("GUI.LOGIN"));
		this.gui = gui;
		this.settlersLobby = settlersLobby;
		registerListeners();
		buildGuiElements();
		createIrcUserAuthenticator();
		createIrcChangePassword();
		createIrcResendEmail();
		createIrcResetPassword();
		setListener(null);
		// save window-position on specific screen on closing
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent evt)
			{
				settlersLobby.config.setScreen(evt.getComponent().getGraphicsConfiguration().getDevice().getIDstring());
			}			
		});
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

    @Override
    public void setListener(ResultEventListener listener)
    {
        if (listener == null)
        {
            this.listener = new ResultEventAdapter();
        }
        else
        {
            this.listener = listener;
        }
    }

    @Override
    public ResultCode[] supportedResultCodes()
    {
        return supportedResultCodes;
    }

	private void registerListeners()
	{
		addWindowListener(gui.windowAdapterToCloseWindow);
		btnGoToRegister.addActionListener(actionListenerToOpenRegistration);
		btnExtras.addActionListener(new ActionListener() {
		    @Override
            public void actionPerformed(ActionEvent ev) {
		        //update the menuitem resend email every time the popup is shown, to get the most
		        //up to date status of the currently connected irc server
		        menuitemResendEmail.setEnabled(ircResendEmail.isResendEmailSupportedByServer());
		        menuitemResetPassword.setEnabled(ircResetPassword.isResetPasswordSupportedByServer());
		        popupMenu.show(btnExtras,
		                       0,
		                       btnExtras.getBounds().height);
		    }
		});
		
		// initial disable the register-button
		btnGoToRegister.setEnabled(false);
		btnGoToRegister.addMouseListener(new addCursorChangesToButton());
		btnExtras.addMouseListener(new addCursorChangesToButton());
		btnLogin.addMouseListener(new addCursorChangesToButton());
		btnLogin.addActionListener(checkValuesAndLogin);
		inputPassword.addActionListener(checkValuesAndLogin);
		inputUser.addActionListener(checkValuesAndLogin);
		menuitemSettings.addActionListener(gui.actionListenerToOpenSettings);
		menuitemChangePassword.addActionListener(new ActionListener()
		{
            @Override
            public void actionPerformed(ActionEvent e)
            {
                changePassword();
            }
        });
		menuitemResendEmail.addActionListener(new ActionListener() 
		{        
            @Override
            public void actionPerformed(ActionEvent e)
            {
                resendEmail();
            }
        });
		menuitemResetPassword.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				resetPassword();
			}		
		});
	}

	private final ActionListener checkValuesAndLogin = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent evt)
		{
			executeLoginAction();
		}
	};

    public ActionListener actionListenerToOpenRegistration = new ActionListener()
    {
        @Override
        public void actionPerformed(ActionEvent evt)
        {
        	if( false == settlersLobby.getIRCCommunicator().getIRCCapabilities().hideRegistrationInFrontend() ) {
        		userRequestsRegistration();
        	}
        	else {
        		String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
        		MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
             	messagewindow.addMessage(
             		MessageFormat.format("<html>" + I18n.getString("LoginFrame.REGISTRATION_HINT") + "</html>", hexcolor),
             		UIManager.getIcon("OptionPane.infoIcon")
             	);
             	messagewindow.addButton(
             		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
             		new ActionListener() {
             		    @Override
             		    public void actionPerformed(ActionEvent e) {
             		    	// close this window
        			    	messagewindow.dispose();
             		    }
             		}
             	);
             	messagewindow.setSettlersLobby(settlersLobby);
             	messagewindow.setVisible(true);
        	}
        }
    };

	private boolean valuesAreValid()
	{
		return (!inputUser.getText().equals("") && (!new String(inputPassword.getPassword()).equals("") || config.isLocalMode())); //$NON-NLS-1$ //$NON-NLS-2$
	}

	private void showValuesAreInvalidMessage() {
		gui.displayError(I18n.getString("SettlersLobby.ERROR_ALL_INPUT_FIELDS"));
	}

	private void doLogin() {
		String userName = inputUser.getText();
		String userPassword = new String(inputPassword.getPassword());
		boolean storePassword = storePasswordButton.isSelected();
		config.setStorePassword(storePassword);
		gui.displayWait(I18n.getString("SettlersLobby.MSG_LOGIN_WAIT")); //$NON-NLS-1$
        config.setPassword(userPassword);
        config.setUserName(userName);
		ircUserAuthenticator.doLogin(userName, userPassword);
	}
	
	

    @Override
    public void setVisible(boolean b)
    {
        super.setVisible(b);
    }

    private void createIrcUserAuthenticator()
    {
        ircUserAuthenticator = new IrcUserAuthenticator(IrcEventDispatcher.getInstance(), this.settlersLobby);
        ircUserAuthenticator.setListener(this);
    }

    private void createIrcChangePassword()
    {
        ircChangePassword = new IrcChangePassword(IrcEventDispatcher.getInstance(), this.settlersLobby);
        ircChangePassword.setListener(this);
    }
    
    private void createIrcResendEmail()
    {
        ircResendEmail = new IrcResendEmail(IrcEventDispatcher.getInstance(), this.settlersLobby);
        ircResendEmail.setListener(this);
    }
    
    private void createIrcResetPassword()
    {
    	ircResetPassword = new IrcResetPassword(IrcEventDispatcher.getInstance(), this.settlersLobby);
    	ircResetPassword.setListener(this); //TODO Event handling
    }

	public void askUserForCredentials(String userName, String password)
	{
		inputUser.setText(userName);
		if (password != null && (!password.isEmpty()))
		{
			inputPassword.setText(password);
		}
		else
		{
			inputPassword.setText(""); //$NON-NLS-1$
		}
		storePasswordButton.setSelected(config.isStorePassword());
		inputUser.requestFocus();
		gui.setNewCurrentActiveFrame(this);
	}

    public void doAutomaticLogin(String userName, String password)
    {
        askUserForCredentials(userName, password);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run()
            {
                executeLoginAction();
            }
        });
    }

    private void executeLoginAction()
    {
        if (!valuesAreValid())
        {
            showValuesAreInvalidMessage();
        }
        else
        {
            doLogin();
        }
    }

    @Override
    public void onResultEvent(final ResultEvent event)
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run()
            {
                onResultEventEdt(event);
            }
        });
    }

    private void onResultEventEdt(ResultEvent event)
    {
        assert(SwingUtilities.isEventDispatchThread() == true);
        if (!isVisible())
        {
            //only react if we are active
            return;
        }

        if (event.getSource() == ircUserAuthenticator)
        {
            switch (event.getResult())
            {
                case OK:
                    hideFrame();
                    ResultEvent.notifySameResult(listener, this, event);
                    break;
                case FAILED:
                    resultFailed(event.getErrorMessage());
                    break;
                case ABORTED:
                    hideFrame();
                    ResultEvent.notifySameResult(listener, this, event);
                    break;
                case SPECIAL_REQUIRES_AUTH_CODE:
                    rawOnRequiresAuthCode(false);
                    break;
                case RESET_AUTHENTIFICATION_SUCCESS:
                	sendNewPassword();
                	break;
                case RESET_AUTHENTIFICATION_FAILED:
                	resultFailed(event.getErrorMessage());
                	rawOnRequiresAuthCode(true);
                	break;
                case RESET_AUTHENTIFICATION_EXPIRED:
                	hideFrame();
                    setVisible(true);
                	gui.displayInformation("", I18n.getString("LoginFrame.MSG_RESET_AUTHENTIFICATION_EXPIRED"));
                	break;
                default:
                    Test.output("unexpected event"  + event); //$NON-NLS-1$
                    return;
            }
        }
        else if (event.getSource() == ircChangePassword)
        {
            switch (event.getResult())
            {
                case OK:
                	hideFrame();
                	setVisible(true);
                	//password change was successful, do an automatic login
                	//(which also stores the new password in the config)
                	doAutomaticLogin(inputUser.getText(), newPassword);
                    newPassword = null;
                    break;
                case FAILED:
                    newPassword = null;
                    resultFailed(event.getErrorMessage());
                    break;
                case ABORTED:
                    hideFrame();
                    newPassword = null;
                    ResultEvent.notifySameResult(listener, this, event);
                    break;
                case RESET_PASSWORD_SUCCESS:
                	newPassword = null;
                	hideFrame();
                	setVisible(true);
                	gui.displayInformation("", I18n.getString("LoginFrame.MSG_PASSWORD_RESET_SUCCESS"));
                	break;
                case RESET_PASSWORD_FAILED:
                	newPassword = null;
                	resultFailed(event.getErrorMessage());
                	sendNewPassword();
                	break;
                default:
                    newPassword = null;
                    Test.output("unexpected event"  + event); //$NON-NLS-1$
                    return;
            }
        }
        else if (event.getSource() == ircResendEmail)
        {
            switch (event.getResult())
            {
                case OK:
                    hideFrame();
                    setVisible(true);
                    String emailAddress = ircResendEmail.getEmailAddress() != null ? ircResendEmail.getEmailAddress() : "";
                    gui.displayInformation("", MessageFormat.format(I18n.getString("LoginFrame.MSG_RESEND_EMAIL_SUCCESS"), emailAddress));
                    Test.output("resend email to " + emailAddress);
                    break;
                case FAILED:
                    resultFailed(event.getErrorMessage());
                    break;
                case ABORTED:
                    hideFrame();
                    ResultEvent.notifySameResult(listener, this, event);
                    break;
                default:
                    Test.output("unexpected event"  + event); //$NON-NLS-1$
                    return;
            }

        } else if (event.getSource() == ircResetPassword) {
        	switch(event.getResult()) {
        		case OK:
        			hideFrame();
                    setVisible(true);
        			String emailAddress = ircResetPassword.getEmailAddress() != null ? ircResetPassword.getEmailAddress() : "";
        			gui.displayInformation("", MessageFormat.format(I18n.getString("LoginFrame.MSG_RESEND_EMAIL_SUCCESS"), emailAddress));
        			Test.output("authentification-code for password reset sent to " + emailAddress);
        			rawOnRequiresAuthCode(true);
        			break;
        		case FAILED:
        			hideFrame();
                    setVisible(true);
                    gui.displayInformation("", I18n.getString("LoginFrame.MSG_TRANSMITTING_DATA_FAILED"));
                    resetPassword();
        			break;
        		case ABORTED:
                    hideFrame();
                    ResultEvent.notifySameResult(listener, this, event);
                    break;
        		default:
        			Test.output("unexpected event"  + event); //$NON-NLS-1$
        			return;
        	}
        }

    }

	private void resultFailed(String errorMessage)
    {
        inputPassword.setText("");
        // use MessageDialog to show the hint
		String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
		MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
     	messagewindow.addMessage(
     		MessageFormat.format("<html>" + errorMessage + "</html>", hexcolor),
     		UIManager.getIcon("OptionPane.errorIcon")
     	);
     	messagewindow.addButton(
     		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
     		new ActionListener() {
     		    @Override
     		    public void actionPerformed(ActionEvent e) {
     		    	// close this window
			    	messagewindow.dispose();
     		    }
     		}
     	);
     	messagewindow.setSettlersLobby(this.settlersLobby);
     	messagewindow.setVisible(true);
        hideFrame();
        ResultEvent.notifyResultFailed(listener, this, errorMessage);
    }

    private void hideFrame()
    {
        gui.closeWait();
        setVisible(false);
    }

    protected void rawOnRequiresAuthCode(boolean isResetPassword)
    {
        String authCode;
        inputAuthDialog.setVisible(true);
        authCode = inputAuthDialog.getValidatedText();
        if (authCode != null && !authCode.isEmpty())
        {
        	if (isResetPassword) 
        	{
        		ircUserAuthenticator.doAuthentification(config.getUserName(), authCode);
        	}
        	else 
        	{
        		ircUserAuthenticator.doLogin(config.getUserName(), config.getPassword(), authCode);
        	}
        }
        else
        {
            resultFailed(I18n.getString("LoginFrame.ERROR_MISSING_AUTH_CODE")); //$NON-NLS-1$
        }
    }

    private void changePassword()
    {
        String oldPassword = new String(inputPassword.getPassword());
        String userName = inputUser.getText();
        inputNewPasswordDialog.setOldPasswordEnabled(true);
        inputNewPasswordDialog.setOldPassword(oldPassword);
        inputNewPasswordDialog.setVisible(true);
        newPassword = inputNewPasswordDialog.getValidatedText();
        oldPassword = inputNewPasswordDialog.getOldPassword();
        inputNewPasswordDialog.clearResult();

        // change password if both password-fields not empty 
        // (other checks where done in InputNewPasswordDialog.java
        if (oldPassword != null && !oldPassword.isEmpty() &&
            newPassword != null && !newPassword.isEmpty() &&
            userName != null && !userName.isEmpty() &&
            newPassword.matches("\\S+")) //$NON-NLS-1$
        {
            gui.displayWait(I18n.getString("LoginFrame.MSG_CHANGE_PASSWORD_WAIT")); //$NON-NLS-1$
            ircChangePassword.changePassword(userName, oldPassword, newPassword);
        }
        // when oldpassword is empty, show a hint
        else if(oldPassword != null && oldPassword.isEmpty()) {
        	gui.displayError(I18n.getString("LoginFrame.MSG_EMPTY_OLD_PASSWORD")); //$NON-NLS-1$
        }
        // when newpassword is empty, show a hint
        else if(newPassword != null && newPassword.isEmpty()) {
        	gui.displayError(I18n.getString("LoginFrame.MSG_EMPTY_NEW_PASSWORD")); //$NON-NLS-1$
        }
    }
    
    private void sendNewPassword() {
    	String userName = inputUser.getText();
    	inputNewPasswordDialog.setOldPasswordEnabled(false);
        inputNewPasswordDialog.setVisible(true);
        newPassword = inputNewPasswordDialog.getValidatedText();
        inputNewPasswordDialog.clearResult();
        
        // change password if fields not empty
        // -> otherwise do nothing
        if (newPassword != null && !newPassword.isEmpty() &&
            userName != null && !userName.isEmpty() &&
            newPassword.matches("\\S+")) //$NON-NLS-1$
        {
            gui.displayWait(I18n.getString("LoginFrame.MSG_CHANGE_PASSWORD_WAIT")); //$NON-NLS-1$
            ircChangePassword.changePassword(userName, newPassword);
        }
	}
    
    private void resendEmail()
    {
        String password = new String(inputPassword.getPassword());
        String userName = inputUser.getText();

        // send email again if password & username exists
        // -> otherwise do nothing
        if (password != null && !password.isEmpty() &&
            userName != null && !userName.isEmpty())
        {
            gui.displayWait(I18n.getString("LoginFrame.MSG_RESEND_EMAIL")); //$NON-NLS-1$
            ircResendEmail.resendEmail(userName, password);
        }
    }
    

	private void resetPassword() {
		inputResetPasswordDialog.setNickname(inputUser.getText());
		inputResetPasswordDialog.setVisible(true);
		String nickname = inputResetPasswordDialog.getNickname();
		String email = inputResetPasswordDialog.getEmail();
		
		// send email with reseted password if password & username exists
        // -> otherwise do nothing
		if (nickname != null && !nickname.isEmpty() 
            && email != null && !email.isEmpty()) 
		{
			gui.displayWait(I18n.getString("LoginFrame.MSG_TRANSMITTING_DATA")); //$NON-NLS-1$ //TODO
            ircResetPassword.requestPassword(nickname, email);
        }
	}

    private void userRequestsRegistration()
    {
        if (isVisible())
        {
            //the user has aborted the login process,
            //stop the ircUserAuthenticator
            if (ircUserAuthenticator != null)
            {
                ircUserAuthenticator.manualAbort();
            }
            hideFrame();
            ResultEvent.notifyResult(listener, this, ResultCode.USER_REQUESTS_REGISTRATION);
        }
    }


	private void buildGuiElements()
	{
		setResizable(false);
		gui.setIcon(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setBackground(settlersLobby.theme.backgroundColor);
		btnLogin.setMultiClickThreshhold(500);
		inputPassword.addFocusListener(new CapsLockFocusListener(settlersLobby));
		pnlLogin.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null,
				" " + I18n.getString("GUI.LOGIN") + " ", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font(
						"Trebuchet MS italic", 1, 17), settlersLobby.theme.getTextColor())); //$NON-NLS-1$
		pnlLogin.setBackground(settlersLobby.theme.backgroundColor);

        inputUser.setBackground(settlersLobby.theme.inputBoxBackgroundColor);
        inputUser.setDocument(new AlphaNumericAndUnderlinePlainDocument());
        inputPassword.setBackground(settlersLobby.theme.inputBoxBackgroundColor);

        storePasswordButton.setBackground(settlersLobby.theme.backgroundColor);
		storePasswordButton.setSelected(false);

        popupMenu.add(menuitemSettings);
        popupMenu.add(menuitemChangePassword);
        popupMenu.add(menuitemResendEmail);
        popupMenu.add(menuitemResetPassword);
        popupMenu.setBackground(settlersLobby.theme.backgroundColor);
        popupMenu.setForeground(settlersLobby.theme.getTextColor());
        menuitemSettings.setBackground(settlersLobby.theme.backgroundColor);
        menuitemSettings.setForeground(settlersLobby.theme.getTextColor());
        menuitemChangePassword.setBackground(settlersLobby.theme.backgroundColor);
        menuitemChangePassword.setForeground(settlersLobby.theme.getTextColor());
        menuitemResendEmail.setBackground(settlersLobby.theme.backgroundColor);
        menuitemResendEmail.setForeground(settlersLobby.theme.getTextColor());
        menuitemResetPassword.setBackground(settlersLobby.theme.backgroundColor);
        menuitemResetPassword.setForeground(settlersLobby.theme.getTextColor());


		javax.swing.GroupLayout pnlLoginLayout = new javax.swing.GroupLayout(pnlLogin);
		pnlLoginLayout.setHorizontalGroup(
		    pnlLoginLayout.createParallelGroup(Alignment.LEADING)
		        .addGroup(pnlLoginLayout.createSequentialGroup()
		            .addContainerGap()
		            .addGroup(pnlLoginLayout.createParallelGroup(Alignment.TRAILING)
		                .addComponent(lblUser)
		                .addComponent(lblPassword))
		            .addGap(10)
		            .addGroup(pnlLoginLayout.createParallelGroup(Alignment.LEADING)
		                .addGroup(pnlLoginLayout.createSequentialGroup()
		                    .addComponent(btnLogin, GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE)
		                    .addPreferredGap(ComponentPlacement.RELATED)
		                    .addComponent(btnGoToRegister, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
		                    .addPreferredGap(ComponentPlacement.RELATED)
		                    .addComponent(btnExtras, GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE))
		                .addComponent(storePasswordButton, GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE)
		                .addComponent(inputPassword, GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE)
		                .addComponent(inputUser, GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE))
		            .addContainerGap())
		);
		pnlLoginLayout.setVerticalGroup(
		    pnlLoginLayout.createParallelGroup(Alignment.LEADING)
		        .addGroup(pnlLoginLayout.createSequentialGroup()
		            .addContainerGap()
		            .addGroup(pnlLoginLayout.createParallelGroup(Alignment.BASELINE)
		                .addComponent(lblUser)
		                .addComponent(inputUser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
		            .addPreferredGap(ComponentPlacement.RELATED)
		            .addGroup(pnlLoginLayout.createParallelGroup(Alignment.BASELINE)
		                .addComponent(lblPassword)
		                .addComponent(inputPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
		            .addGap(5)
		            .addComponent(storePasswordButton)
		            .addGap(21)
		            .addGroup(pnlLoginLayout.createParallelGroup(Alignment.BASELINE)
		                .addComponent(btnLogin)
		                .addComponent(btnGoToRegister)
		                .addComponent(btnExtras))
		            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		pnlLogin.setLayout(pnlLoginLayout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(pnlLogin, javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(pnlLogin, javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		pack();

		setLocationRelativeTo(gui.getMainWindow());
		inputAuthDialog = new InputAuthCodeDialog(this, gui, settlersLobby);
		inputNewPasswordDialog = new InputNewPasswordDialog(this, gui, settlersLobby);
		inputResetPasswordDialog = new InputResetPasswordDialog(this, gui, settlersLobby);
	}

	/**
	 * Enable the register button after some seconds 
	 * (after which the IRC server should have sent its capabilities)
	 * 
	 * TODO find a better solution to check when the capabilities are all collected
     * (actually there is no signal "ready" from Anope)
	 * 
	 * @param seconds
	 */
	public void enableRegisterButtonAfterMilliseconds(int seconds) {
		TimerTaskCompatibility tskEnableBtnGoToRegister = new TimerTaskCompatibility() {

            @Override
			public void run()
            {
            	btnGoToRegister.setEnabled(true);
            }
        };
        tskEnableBtnGoToRegister.schedule(seconds);
	}

}
