package net.siedler3.alobby.modules.login;

import static net.siedler3.alobby.communication.NickServCommand.CONFIRM;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_REGEX_EMAIL_NOT_CONFIRMED_2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_REGEX_EMAIL_NOT_CONFIRMED_2_anope18;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_REGEX_RESET_FLAG_EMAIL_NOT_CONFIRMED_2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_WRONG_PASSWORD;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_WRONG_PASSWORD_2_freenode;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_AUTHENTICATED_NICK_2_anope2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_AUTHENTICATED_NICK_3_anope2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_LOGGED_IN_NICK_1_freenode;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_LOGGED_IN_NICK_2_freenode;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_INVALID_PASSCODE_2_anope2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_RESET_AUTHENTIFICATION_EXPIRED_2_anope2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_NOT_LOGGED_IN_1_freenode;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_NOT_LOGGED_IN_2_freenode;
import static net.siedler3.alobby.modules.base.ResultCode.ABORTED;
import static net.siedler3.alobby.modules.base.ResultCode.FAILED;
import static net.siedler3.alobby.modules.base.ResultCode.OK;
import static net.siedler3.alobby.modules.base.ResultCode.SPECIAL_REQUIRES_AUTH_CODE;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.regex.Matcher;

import javax.swing.UIManager;

import net.siedler3.alobby.communication.IrcInterface;
import net.siedler3.alobby.communication.NickServCommand;
import net.siedler3.alobby.communication.NickServPatterns;
import net.siedler3.alobby.communication.ProprietaryReplyConstants;
import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.modules.base.IrcResultProvider;
import net.siedler3.alobby.modules.base.ResultCode;
import net.siedler3.alobby.util.TimerTaskCompatibility;
import net.siedler3.alobby.util.httpRequests;

import org.jibble.pircbot.ReplyConstants;

public class IrcUserAuthenticator extends IrcResultProvider
{

    private static final String ERROR_MESSAGE_NICK_NOT_REGISTERED = "IRCCommunicator.ERROR_NICK_NOT_REGISTERED"; //$NON-NLS-1$
    private static final String ERROR_MESSAGE_WRONG_PASSWORD = "IRCCommunicator.ERROR_WRONG_PASSWORD"; //$NON-NLS-1$
    private static final String ERROR_MESSAGE_WRONG_AUTH_CODE = "IRCCommunicator.ERROR_MESSAGE_WRONG_AUTH_CODE"; //$NON-NLS-1$

	private static final ResultCode[] supportedResultCodes = {
	    OK,
	    FAILED,
	    ABORTED,
	    SPECIAL_REQUIRES_AUTH_CODE
	};

	private String currentPassword;
	private String initialNick;
	private String requiredNick;
    private String authCode;
    private TimerTaskCompatibility timeoutTask;
    private State state = new State();
	private boolean isResetPassword;
	private SettlersLobby settlersLobby;

    private static class State
    {
        private boolean loginCommandSent;
        private boolean authCodeSent;
        private boolean emailNotConfirmed;
        private int lastStatus;

        private State()
        {
            reset();
        }

        void reset()
        {
            loginCommandSent = false;
            authCodeSent = false;
            emailNotConfirmed = false;
            lastStatus = 0;
        }

    }


	public IrcUserAuthenticator(IrcInterface ircInterface, SettlersLobby settlersLobby)
	{
		super(ircInterface);
		this.settlersLobby = settlersLobby;
		this.defaultErrorMessage = I18n.getString(ERROR_MESSAGE_WRONG_PASSWORD);
	}

    @Override
    public ResultCode[] supportedResultCodes()
    {
        return supportedResultCodes;
    }


    public void doAuthentification(String nick, String authCode) {
    	if (!ircInterface.isConnected())
	    {
	        resultAborted();
	        return;
	    }
	    isResetPassword = true;
	    startObservation();
    	this.authCode = authCode;
    	this.requiredNick = nick;
    	state.reset();
    	nickServRequiresAuthCode();
    }
    
	/**
	 * @param nick
	 * @param password
	 *
	 */
	public void doLogin(String nick, String password)
	{
	    doLogin(nick, password, null);
	}

	public void doLogin(String nick, String password, String authCode)
	{
	    if (!ircInterface.isConnected())
	    {
	        resultAborted();
	        return;
	    }
	    isResetPassword = false;
	    startObservation();
	    state.reset();
	    this.requiredNick = nick;
	    this.initialNick = ircInterface.getNick();
		this.currentPassword = password;
		this.authCode = authCode;
		if (!initialNick.equals(nick))
		{
		    //first try to get the desired nick
		    //if this succeeds, the loginProcedure
		    //will be started afterwards
		    //see onNickChange()
		    ircInterface.changeNick(nick);
		}
		else
		{
            startLoginProcedure();
		}
	}

	public void manualAbort()
	{
	    stopListening();
	    //do not inform the listener

	    cancelTimeoutTask();
	}

    private void startLoginProcedure()
    {
        //by querying the status, transmitting of password will be triggered
        queryNickState();
    }

    private void queryNickState()
    {
        //always reset the emailNotConfirmed flag
        state.emailNotConfirmed = false;
        
        scheduleTimeoutTask();
        if (ircInterface.getIrcServerInfo().isRegistrationRequiresEmailAuthentication())
        {
            //if "register" without any option is sent in case the email authentication is still pending,
            //then a specific answer is returned only for this case. This can be detected.
            //Otherwise only the standard help message (or an error message) is printed.
            //REGISTER.transmitUsing(ircInterface, null);
            
            //for anope 2.x the previous solution with REGISTER does no longer work.
            //Additionally send an INFO command, which shows a dedicated "not confirmed message"
            //(at least for anope 2.x)
            NickServCommand.INFO.transmitUsing(ircInterface, requiredNick);
        }
        ircInterface.queryNickStatus();
    }



	@Override
    protected void onDisconnect()
    {
	    resultAborted();
    }

    @Override
    protected void onNickChange(String oldNick, String login, String hostname, String newNick)
    {
	    if (oldNick.equals(initialNick) && newNick.equals(requiredNick))
	    {
	        initialNick = requiredNick; //react only once to the nick change
	        ircInterface.setNick(requiredNick); //TODO is this really necessary? the "login" entry in IRC cannot be changed anymore
	        //the user has now the required nick, start IRC login procedure
	        startLoginProcedure();
	    }
    }

    @Override
	protected void onNotice(String sourceNick, String sourceLogin, String sourceHostname,
			String target, String notice)
	{
	    if (ircInterface.isNickServe(sourceNick, sourceLogin, sourceHostname))
	    {
	    	handleResetPasswordMessage(notice);
	        handleWrongPasswordMessage(notice);
	        handleEmailNotConfirmed(notice);
	        String[] splittedNotice = notice.split(" "); //$NON-NLS-1$
            handleNickServStatusReply(target, splittedNotice);
	    }
	}

    /**
     * Handles the nickserv reply.
     * 
     * @param target			the nickname which has been used against the IRC-service
     * @param splittedNotice	the notice send from IRC-service
     */
	@SuppressWarnings("static-access")
	private void handleNickServStatusReply(String target, String[] splittedNotice)
    {
        if (isNickServStatusReply(splittedNotice))
        {
            cancelTimeoutTask();
            try
            {	
                int status = Integer.parseInt(splittedNotice[2]);
                state.lastStatus = status;
                //check the emailNotConfirmed flag before going into the switch
                //this handling was added for anope 2.x
                if (state.emailNotConfirmed == true)
                {
                    if (state.authCodeSent == true)
                    {
                        resultFailed(I18n.getString(ERROR_MESSAGE_WRONG_AUTH_CODE));
                    }
                    else
                    {
                        nickServRequiresAuthCode();
                    }
                    //skip rest of method, decision is already taken
                    return;
                }
                switch(status)
                {
                    case 0:
                        // Nickname is not registered
                    	// -> check via quick web-request if the given nickname is registered in forum
                    	httpRequests httpRequests = new httpRequests();
                    	httpRequests.setUrl("https://lobby.siedler3.net/aLobbyRequests.html");
                    	httpRequests.addParameter("checknickname", target);
                    	// do not use token-security for this request
                    	httpRequests.setIgnorePhpSessId(true);
                    	String response = "";
                    	try {
                    		response = httpRequests.doRequest();
                    	} catch (IOException e) {
                    	}
                    	if( "".equals(response) ) {
                    		// nickname is not registered in forum: show default message
                    		resultFailed(I18n.getString(ERROR_MESSAGE_NICK_NOT_REGISTERED));
                    	}
                    	else {
                    		// nickname is registered in forum
                    		// -> cancel all IRC-notifications
                    		resultAborted();
                    		
                    		// -> show link to wiki
                    		MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
                        	// -> add message
                    		messagewindow.addMessage(
                				MessageFormat.format("<html>" + I18n.getString("IrcUserAuthenticator.NICKNAME_REGISTERED_IN_FORUM") + "</html>", target, settlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor())), 
                    			UIManager.getIcon("OptionPane.informationIcon")
                    		);
                    		// -> add ok-button
                    		messagewindow.addButton(
                    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
                    			new ActionListener() {
                    			    @Override
                    			    public void actionPerformed(ActionEvent e) {
                    			    	settlersLobby.actionWantsToLogOut();
                    			    	messagewindow.dispose();
                    			    }
                    			}
                    		);
                    		messagewindow.setSettlersLobby(settlersLobby);
                    		// show it
                    		messagewindow.setVisible(true);
                    	}
                        break;
                    case 1:
                        if (state.loginCommandSent == false)
                        {
                            transmitPassword();
                        }
                        else
                        {
                            resultFailed();
                        }
                        break;
                    case 3:
                        resultOk();
                        break;
                    default:
                        Test.output("Error, unknown status " + status); //$NON-NLS-1$
                }
            }
            catch (NumberFormatException e) {
                Test.outputException(e);
            }
        } else {
            String notice = String.join(" ", splittedNotice);
            if (notice.startsWith(PATTERN_NOT_LOGGED_IN_1_freenode) || notice.startsWith(PATTERN_NOT_LOGGED_IN_2_freenode)) {
                if (state.loginCommandSent == false)
                {
                    transmitPassword();
                }
                else
                {
                    resultFailed();
                }
            } else if (notice.startsWith(PATTERN_LOGGED_IN_NICK_1_freenode) || notice.startsWith(PATTERN_LOGGED_IN_NICK_2_freenode)) {
                resultOk();
            } else {
                Test.output("Unhandled notice: " + notice);
            }
        }
    }

    private boolean isNickServStatusReply(String[] splittedNotice)
    {
        return NickServPatterns.isNickServStatusReply(ircInterface.getNick(), splittedNotice);
    }

    @Override
    protected void onServerResponse(int code, String response)
    {
	    switch (code)
	    {
	        case ReplyConstants.ERR_ERRONEUSNICKNAME:
	            //response has the form
	            // 432    ERR_ERRONEUSNICKNAME
	            //  "<nick> :Erroneous nickname"
	            //on unreal irc it is in fact
	            // "<nick> <erroneous nick> :Erroneous nickname: <error message>
	            if (response.startsWith(ircInterface.getNick() + " "))
	            {
	                resultFailed(response.substring(ircInterface.getNick().length() + 1));
	            }
	            else
	            {
	                resultFailed(response);
	            }
	            break;
	        case ReplyConstants.ERR_NICKNAMEINUSE:
                resultFailed(I18n.getString("IRCCommunicator.ERROR_NICK_ALREADY_IN_USE")); //$NON-NLS-1$
	            break;
	        case ProprietaryReplyConstants.ERR_SERVICESDOWN:
	            //Message: Services are currently down. Please try again later
	            //handle as if successful
                resultOk();
	            break;
	    }
    }
    


    private void handleResetPasswordMessage(String notice) 
    {
		if (notice.startsWith(PATTERN_AUTHENTICATED_NICK_2_anope2) || notice.startsWith(PATTERN_AUTHENTICATED_NICK_3_anope2)) 
		{
			resultResetAuthentification();
		} 
		else if (notice.equalsIgnoreCase(PATTERN_INVALID_PASSCODE_2_anope2))
		{
			resultResetAuthentificationFailed(I18n.getString(ERROR_MESSAGE_WRONG_AUTH_CODE));
		}
		else if (notice.equalsIgnoreCase(PATTERN_RESET_AUTHENTIFICATION_EXPIRED_2_anope2))
		{
			resultResetAuthentificationExpired();
		}
	}

	private void handleEmailNotConfirmed(String notice)
    {
        Matcher m = PATTERN_REGEX_EMAIL_NOT_CONFIRMED_2.matcher(notice);
        if (m.matches() && m.group(1).equalsIgnoreCase(ircInterface.getNick()))
        {
            state.emailNotConfirmed = true;
            Test.output("stateEmailNotConfirmed: " + state.emailNotConfirmed); //$NON-NLS-1$
        }
        else if (PATTERN_REGEX_EMAIL_NOT_CONFIRMED_2_anope18.matcher(notice).matches())
        {
            state.emailNotConfirmed = true;
            Test.output("stateEmailNotConfirmed: " + state.emailNotConfirmed); //$NON-NLS-1$
        }
        else if (PATTERN_REGEX_RESET_FLAG_EMAIL_NOT_CONFIRMED_2.matcher(notice).matches())
        {
            //this message appears directly after the CONFIRM command, so reset
            //the emailNotConfirmed state
            //Without doing this, we get false error messages on anope 1.8, as
            //the PATTERN_REGEX_EMAIL_NOT_CONFIRMED_2_anope18 also appears when sending the 
            //IDENTIFY command, which overwrites the resetting done in queryNickState(), although
            //this is not intended.
            //In anope 2.x this message already appears during registration process, where
            //it is not checked, so it should not cause any problems.
            state.emailNotConfirmed = false;
            Test.output("stateEmailNotConfirmed: " + state.emailNotConfirmed); //$NON-NLS-1$
        }
    }
    
    private void handleWrongPasswordMessage(String notice)
	{
		// Login fehlgeschlagen
		// "NOTICE nick :Falsches Passwort."
		if (notice.equalsIgnoreCase(PATTERN_WRONG_PASSWORD) || notice.equalsIgnoreCase(PATTERN_WRONG_PASSWORD_2_freenode))
		{
			errorMessage = I18n.getString(ERROR_MESSAGE_WRONG_PASSWORD);
		}
	}

    private void nickServRequiresAuthCode()
    {
        if (isAuthCodeSet())
        {
            transmitAuthCode();
        }
        else
        {
            result(SPECIAL_REQUIRES_AUTH_CODE);
        }
    }

    private boolean isAuthCodeSet()
    {
        return authCode != null && !authCode.isEmpty();
    }


    /**
	 * sendet das Passwort an den Irc-Server, bzw. genauer an den NickServ. Das
	 * Passwort erscheint nicht im logfile. Sollte das Passwort nicht gesetzt
	 * sein, passiert gar nichts.
	 */
	private void transmitPassword()
	{
		if (isPasswordSet())
		{
			rawTransmitPassword();
			queryNickState();
		}
	}
	
	public void rawTransmitPassword()
	{
        ircInterface.setVerbose(false);
        Test.output("sending password"); //$NON-NLS-1$
        ircInterface.identify(currentPassword);
        ircInterface.setVerbose(ALobbyConstants.IRC_DEBUG_ON);
        state.loginCommandSent = true;
	}

    private boolean isPasswordSet()
    {
        return currentPassword != null && !currentPassword.isEmpty();
    }

    private void transmitAuthCode()
    {
        if (isAuthCodeSet())
        {
            //for anope 2.x, the user must be identified, before the email
            //can be authenticated. This does not really fit to the model
            //of the alobby, but there is no other option
        	//BUT: if a password reset is requested, the password can not
        	// be send previously, thus, skip identification
        	if (isResetPassword) {
        		ircInterface.sendNickServCommand(CONFIRM, requiredNick + " " + authCode);
        		return;
        	}
        	else if (isPasswordSet() && state.lastStatus < 3)
            {
                rawTransmitPassword();
            }
        	ircInterface.sendNickServCommand(CONFIRM, authCode);
            state.authCodeSent = true;
            queryNickState();
        }
    }

    private void scheduleTimeoutTask()
    {
        cancelTimeoutTask();
        timeoutTask = new TimerTaskCompatibility() {
            @Override
            public void run()
            {
                resultFailed(I18n.getString("LoginFrame.ERROR_UNEXPETED_TIMEOUT"));  //$NON-NLS-1$
            }
        };
        final int timeLimitBeforeAbort = 10000;
        timeoutTask.schedule(timeLimitBeforeAbort);
    }

    private void cancelTimeoutTask()
    {
        if (timeoutTask != null)
        {
            timeoutTask.cancel();
        }
    }

    @Override
    protected void cleanupBeforeNotifyResult()
    {
        super.cleanupBeforeNotifyResult();
        cancelTimeoutTask();
    }
}
