package net.siedler3.alobby.modules.login;

import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_INVALID_EMAIL_2_anope2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_RESET_NOT_REGISTERED_NICK_2_anope2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_RESET_NOT_REGISTERED_NICK_3_freenode;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_RESET_EMAIL_SENT_2_anope2;

import java.util.regex.Matcher;

import net.siedler3.alobby.communication.IrcInterface;
import net.siedler3.alobby.communication.NickServCommand;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;

public class IrcResetPassword extends IrcNickServAccessorBase {

	private String email;

	public IrcResetPassword(IrcInterface ircInterface, SettlersLobby settlersLobby) {
		super(ircInterface, settlersLobby);
	}
	
	public void requestPassword(String nickname, String email) {
		this.email = email;
		startObservation();
        scheduleTimeoutTask();
		NickServCommand.RESETPASS.transmitUsing(ircInterface, nickname + " " + email);
	}

	@Override
	protected void onInternalResultEventOkAction() {
		Test.output("IrcRequestPassword -> onInternalResultEventOkAction()");
	}
	
	@Override
    protected void onNotice(String sourceNick, String sourceLogin, String sourceHostname,
            String target, String notice)
    {
		if (ircInterface.isNickServe(sourceNick, sourceLogin, sourceHostname))
        {
            Test.output("!!IrcResetPassword notice: " + notice);
            handleIncorrectEmailMessage(notice);
            handleNickNotRegisteredMessage(notice);
            handleEmailWasSendMessage(notice);
        }	
    }
	
	private void handleEmailWasSendMessage(String notice) {
		if (notice.equals(PATTERN_INVALID_EMAIL_2_anope2)) {
			resultFailed(notice);
		}
	}

	private void handleNickNotRegisteredMessage(String notice) {
        Matcher m1 = PATTERN_RESET_NOT_REGISTERED_NICK_2_anope2.matcher(notice);
        Matcher m2 = PATTERN_RESET_NOT_REGISTERED_NICK_3_freenode.matcher(notice);
		if (m1.matches() || m2.matches()) {
			resultFailed(notice);
		}
	}

	private void handleIncorrectEmailMessage(String notice) {
		Matcher m = PATTERN_RESET_EMAIL_SENT_2_anope2.matcher(notice);
		
		if (m.matches()) {
			resultOk();
		}
	}

	public String getEmailAddress() {
		return email;
	}

	public boolean isResetPasswordSupportedByServer() {
		return ircInterface.getIrcServerInfo().isResetPasswordSupported();
	}
}
