package net.siedler3.alobby.modules.login;

import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_PASSWORD_1;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_PASSWORD_2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_PASSWORD_2_anope2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_REGEX_PASSWORD_CHANGED_2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_REGEX_PASSWORD_CHANGED_2_anope2;
import static net.siedler3.alobby.communication.NickServPatterns.PATTERN_REGEX_PASSWORD_CHANGED_3_freenode;
import net.siedler3.alobby.communication.IrcInterface;
import net.siedler3.alobby.communication.NickServCommand;
import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;

public class IrcChangePassword extends IrcNickServAccessorBase
{

    private static final String ERROR_MESSAGE_PASSWORD_TOO_SHORT = "IRCCommunicator.ERROR_PASSWORD_TOO_SHORT"; //$NON-NLS-1$

    private String newPassword;

    public IrcChangePassword(IrcInterface ircInterface, SettlersLobby settlersLobby)
    {
        super(ircInterface, settlersLobby);
    }


    public void changePassword(String nick, String oldPassword, String newPassword)
    {
        this.newPassword = newPassword;
        doLogin(nick, oldPassword);
    }
    
    public void changePassword(String userName, String newPassword) {
    	this.newPassword = newPassword;
    	doChangePassword();
	}

    private void doChangePassword()
    {
        startObservation();
        ircInterface.setVerbose(false);
        Test.output("sending new password"); //$NON-NLS-1$
        //doChangePassword is only called, when the ircUserAuthenticator was successful,
        //i.e. the user is logged in
        NickServCommand.SET_PASSWORD.transmitUsing(ircInterface, newPassword);
        ircInterface.setVerbose(ALobbyConstants.IRC_DEBUG_ON);
    }

    @Override
    protected void onNotice(String sourceNick, String sourceLogin, String sourceHostname,
            String target, String notice)
    {
        if (ircInterface.isNickServe(sourceNick, sourceLogin, sourceHostname))
        {
        	handleResetPasswordMessage(notice);
            handlePasswordTooShortMessage(notice);
            handlePasswordChanged(notice);
        }
    }

    private void handleResetPasswordMessage(String notice) {
		if (notice.contains(PATTERN_PASSWORD_2_anope2))
		{
			resultResetPasswordFailed(I18n.getString(ERROR_MESSAGE_PASSWORD_TOO_SHORT));
		}
		else if (PATTERN_REGEX_PASSWORD_CHANGED_2_anope2.matcher(notice).matches() || PATTERN_REGEX_PASSWORD_CHANGED_3_freenode.matcher(notice).matches() )
		{
			resultResetPasswordSuccess();
		}
	}


	private void handlePasswordTooShortMessage(String notice)
    {
        if (notice.contains(PATTERN_PASSWORD_1) || notice.contains(PATTERN_PASSWORD_2))
        {
            resultFailed(I18n.getString(ERROR_MESSAGE_PASSWORD_TOO_SHORT));
        }
    }

    private void handlePasswordChanged(String notice)
    {
        if (PATTERN_REGEX_PASSWORD_CHANGED_2.matcher(notice).matches()) 
        {
            resultOk();
        }
    }

    @Override
    protected void onInternalResultEventOkAction()
    {
        doChangePassword();
    }
}
