package net.siedler3.alobby.modules.login;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;

import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.gui.additional.MyJPasswordField;
import net.siedler3.alobby.i18n.I18n;

public class InputNewPasswordDialog extends JDialog implements ActionListener, PropertyChangeListener
{
    private String typedText = null;
    private MyJPasswordField newPasswordField;
    private JCheckBox buttonShowInput;
    private GUI gui;
    private MyJPasswordField oldPasswordField;

    private JOptionPane optionPane;

    /**
     * Returns null if the typed string was invalid; otherwise, returns the
     * string as the user entered it.
     */
    public String getValidatedText()
    {
        return typedText;
    }

    /*
     * returns the value from field for the old password
     */
    public String getOldPassword()
    {
        return new String(oldPasswordField.getPassword());
    }

    /*
     * set a new value into the field for the old password
     * 
     * @param insertPassword	the new value to set
     */
    public void setOldPassword(String insertPassword)
    {
        this.oldPasswordField.setText(insertPassword);
    }
    
    /*
     * returns the value from field for the new password
     */
    public String getNewPassword()
    {
        return new String(newPasswordField.getPassword());
    }
    
    /*
     * set a new value into the field for the new password
     * 
     * @param insertPassword	the new value to set
     */
    public void setNewPassword(String insertPassword)
    {
        this.newPasswordField.setText(insertPassword);
    }

    /** Creates the reusable dialog. */
    public InputNewPasswordDialog(JFrame owner, GUI gui, SettlersLobby settlersLobby)
    {
        super(owner, true);
        this.gui = gui;
        setIconImage(gui.getIcon());

        setTitle(I18n.getString("GUI.LOGIN")); //$NON-NLS-1$

        newPasswordField = new MyJPasswordField();
        newPasswordField.setBackground(settlersLobby.theme.inputBoxBackgroundColor);

        oldPasswordField = new MyJPasswordField();
        oldPasswordField.setBackground(settlersLobby.theme.inputBoxBackgroundColor);

        buttonShowInput = new JCheckBox(I18n.getString("InputNewPasswordDialog.MESSAGE_SHOW_PASSWORD"));
        buttonShowInput.setSelected(false);

        // Create an array of the text and components to be displayed.
        String msgString0 = I18n.getString("InputNewPasswordDialog.MESSAGE_INPUT_OLD_PASSWORD"); //$NON-NLS-1$
        String msgString1 = I18n.getString("InputNewPasswordDialog.MESSAGE_INPUT_NEW_PASSWORD"); //$NON-NLS-1$
        Object[] array = { msgString0, oldPasswordField, msgString1, newPasswordField, buttonShowInput };

        // Create the JOptionPane.
        optionPane = new JOptionPane(array, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null, null,
                        null);

        // Make this dialog display it.
        setContentPane(optionPane);

        // Handle window closing correctly.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we)
            {
                /*
                 * Instead of directly closing the window, we're going to change
                 * the JOptionPane's value property.
                 */
                optionPane.setValue(JOptionPane.CANCEL_OPTION);
            }
        });

        // Ensure the text field always gets the first focus.
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent ce)
            {
                newPasswordField.requestFocusInWindow();
            }
        });

        // Register an event handler that puts the text into the option pane.
        newPasswordField.addActionListener(this);

        // Register an event handler that reacts to option pane state changes.
        optionPane.addPropertyChangeListener(this);

        buttonShowInput.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                if (e.getSource() == buttonShowInput)
                {
                    newPasswordField.setInputVisible(e.getStateChange() == ItemEvent.SELECTED);
                }
            }
        });

        pack();
    }

    /** This method handles events for the text field. */
    @Override
    public void actionPerformed(ActionEvent e)
    {
        optionPane.setValue(JOptionPane.OK_OPTION);
    }

    /** This method reacts to state changes in the option pane. */
    @Override
    public void propertyChange(PropertyChangeEvent e)
    {
        String prop = e.getPropertyName();

        if (isVisible() && (e.getSource() == optionPane)
                        && (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY.equals(prop)))
        {
            Object value = optionPane.getValue();

            if (value == JOptionPane.UNINITIALIZED_VALUE)
            {
                // ignore reset
                return;
            }

            // Reset the JOptionPane's value.
            // If you don't do this, then if the user
            // presses the same button next time, no
            // property change event will be fired.
            optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

            if (value.equals(JOptionPane.OK_OPTION))
            {
            	// get the value from old-password-field
                String oldPassword = new String(getOldPassword());
                
                // get the value from new-password-field
                String newPassword = new String(newPasswordField.getPassword());
                
                // If both fields have a length of more than or equal 8
                // and the old-password-field is not disabled
                // and less than 32 (thats the max-length in IRC-Nickserv)
                // and if their value's are different than send it to nickserv via listener.
                // The testing for different values is important, because without that
                // a third person could guess the old password, which the attacked user
                // might use in other systems and communities. That would be a possible privacy violation!
                if( newPassword.length() >= 8 && oldPassword.length() >= 8 && newPassword.length() < 32 && oldPassword.length() < 32 && !newPassword.equals(oldPassword) && oldPasswordField.isEnabled() )
                {
                    // clear password field
                    typedText = new String(newPasswordField.getPassword());
                    Arrays.fill(newPasswordField.getPassword(), '0');
                    clearAndHide();
                }
                // same with disabled old-password-field (e.g. at reset-request via InputResetPasswordDialog.java)
                else if( newPassword.length() >= 8 && newPassword.length() < 32 && !newPassword.equals(oldPassword) && !oldPasswordField.isEnabled() )
                {
                    // clear password field
                    typedText = new String(newPasswordField.getPassword());
                    Arrays.fill(newPasswordField.getPassword(), '0');
                    clearAndHide();
                }
                // if the values are identical, show a hint
                else if( newPassword.length() > 1 && oldPassword.length() > 1 && newPassword.length() < 32 && oldPassword.length() < 32 && newPassword.equals(oldPassword) && oldPasswordField.isEnabled() )
                {
                	oldPasswordField.selectAll();
                	gui.displayError(I18n.getString("LoginFrame.MSG_IDENTICAL_PASSWORDS")); //$NON-NLS-1$
                	// TODO
                	// the following will fail because this field is opened in a JOptionPane, where the OK-Button gets auto-focus
                	oldPasswordField.requestFocusInWindow();
                }
                // if one of the value have a length more than 32
                else if( newPassword.length() > 32 || oldPassword.length() > 32 )
                {
                	newPasswordField.selectAll();
                	gui.displayError(I18n.getString("LoginFrame.MSG_PASSWORDS_LENGTH")); //$NON-NLS-1$
                	// TODO
                	// the following will fail because this field is opened in a JOptionPane, where the OK-Button gets auto-focus
                	newPasswordField.requestFocusInWindow();
                }
                // if the old-password-value is empty, show a hint
                else if( oldPassword.isEmpty() && oldPasswordField.isEnabled() ) {
                	oldPasswordField.selectAll();
                	gui.displayError(I18n.getString("LoginFrame.MSG_EMPTY_OLD_PASSWORD")); //$NON-NLS-1$
                	// TODO
                	// the following will fail because this field is opened in a JOptionPane, where the OK-Button gets auto-focus
                	oldPasswordField.requestFocusInWindow();
                }
                // if the new-password-value is empty, show a hint
                else if( newPassword.isEmpty() ) {
                	newPasswordField.selectAll();
                	gui.displayError(I18n.getString("LoginFrame.MSG_EMPTY_NEW_PASSWORD")); //$NON-NLS-1$
                	// TODO
                	// the following will fail because this field is opened in a JOptionPane, where the OK-Button gets auto-focus
                	newPasswordField.requestFocusInWindow();
                }
                // in all other cases show normal error-message (this should not be happen)
                else
                {
                    // text was invalid
                    gui.displayError(I18n.getString("InputAuthCodeDialog.ERROR_INVALID_FORMAT")); //$NON-NLS-1$
                    newPasswordField.requestFocusInWindow();
                }
            }
            else if (value.equals(JOptionPane.CANCEL_OPTION))
            {
            	// user clicked cancel
            	// than we only close the dialog to prevent activation of other listeners
            	clearAndHide();
            }
            else
            { 	
            	// user closed dialog
                clearResult();
                clearAndHide();
            }
        }
    }

    /** This method clears the dialog and hides it. */
    public void clearAndHide()
    {
        newPasswordField.setText(null);
        setVisible(false);
    }

    public void clearResult()
    {
        Arrays.fill(oldPasswordField.getPassword(), '0');
        oldPasswordField.setText(null);
        typedText = null;
    }

    @Override
    public void setVisible(boolean b)
    {
    	if (b)
        {
            typedText = null;
            setLocationRelativeTo(getParent());
        }
    	super.setVisible(b);
    }
    
    public void setOldPasswordEnabled(boolean b) {
    	oldPasswordField.setEnabled(b);
    }

}
