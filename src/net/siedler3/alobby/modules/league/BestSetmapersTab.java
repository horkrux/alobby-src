package net.siedler3.alobby.modules.league;

import net.siedler3.alobby.controlcenter.SettlersLobby;

public class BestSetmapersTab extends LeagueDialogTab {
	
	public BestSetmapersTab(SettlersLobby settlersLobby, LeagueTournamentDialog myLeagueTournamentDialog) {
		// get the tab-content from parent-object
		super(settlersLobby, myLeagueTournamentDialog);
		// set the sourceURL
		this.sourceURL = settlersLobby.config.getLeagueUrlRankingLists() + "bestsetmapers";
		// get the listing for this table
		listing.setName("setmaperslisting");
		// do not load the listing at this point as it would slow the aLobby-start
		// (and not everybody will use it)
	}
	
}