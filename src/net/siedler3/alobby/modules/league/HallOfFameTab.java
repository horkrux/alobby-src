package net.siedler3.alobby.modules.league;

import net.siedler3.alobby.controlcenter.SettlersLobby;

public class HallOfFameTab extends LeagueDialogTab {
	
	public HallOfFameTab(SettlersLobby settlersLobby, LeagueTournamentDialog myLeagueTournamentDialog) {
		// get the tab-content from parent-object
		super(settlersLobby, myLeagueTournamentDialog);
		// set the sourceURL
		this.sourceURL = settlersLobby.config.getLeagueUrlRankingLists() + "halloffame";
		// set the listing for this table
		listing.setName("halloffamelisting");
		// do not load the listing at this point as it would slow the aLobby-start
		// (and not everybody will use it)
	}
	
}