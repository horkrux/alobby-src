/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.modules.league;

import java.awt.ComponentOrientation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

import net.siedler3.alobby.gui.GUI;

/**
 * Generate a window with tab-navigation to show league-rankings and manage and open tournament-games.
 * 
 * @author Zwirni
 *
 */
public class LeagueTournamentDialog extends JFrame {
	
	// gui-Object
	private GUI gui;
	
	// list of tabs in this window
	private JTabbedPane tabLeiste = new JTabbedPane();
	
	// initialize the Object
	public LeagueTournamentDialog(GUI gui) {
		this.gui = gui;
		
		// set the window-size
		this.setSize(500, 500);
		
		// set always on top so user must use it
		//this.setAlwaysOnTop(true);
		
		// set the default text-orientation in this window (possible language-depending)
		this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		// close this window really
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		// center this window
		this.setLocationRelativeTo(gui.getMainWindow());
		
		// disable that the window is resizable
		this.setResizable(false);
		
		// set EventListener
		this.tabLeiste.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				loadTabContent(tabLeiste.getSelectedIndex());
			}
	    });
		
	}

	/**
	 * Add a new Tab to the Dialog
	 * @param tabName
	 * @param s3PanelDialog
	 * 
	 * @return LeagueTournamentDialog
	 */
	public LeagueTournamentDialog addTab( String tabname, JPanel s3PanelDialog ) {
		
		// add the panel to dialog
		this.tabLeiste.addTab(tabname, s3PanelDialog);
		
		// return the window
		return this;
	}

	/**
	 * Show the dialog with all components
	 */
	public void display() {
		
		// set default aLobby-Icon
		this.gui.setIcon(this);
		
		// if only one tab is visible, then 
		// hide the tab navbar through set the height of it to zero. 
		this.tabLeiste.setUI(new BasicTabbedPaneUI() {  
		    @Override  
		    protected int calculateTabAreaHeight(int tab_placement, int run_count, int max_tab_height) {  
		        if (tabLeiste.getTabCount() > 1)
		            return super.calculateTabAreaHeight(tab_placement, run_count, max_tab_height);  
		        else  
		            return 0;  
		    } 
		});
		
		// add the tabs to the window
		this.add(this.tabLeiste);
		
		// shrink the size of the window to the only necessary size
		this.pack();
		
		// show the dialog
		this.setVisible(true);
		
		// set the window-position
		this.setLocationRelativeTo(this.gui.getMainWindow());
		
	}
	
	/**
	 * Load the content of a specific tab.
	 * 
	 * @param componentAt
	 */
	protected void loadTabContent( int index ) {
		if( this.tabLeiste.getComponentAt(index).getName() != "tournaments" ) {
			LeagueDialogTab tab = (LeagueDialogTab) this.tabLeiste.getComponentAt(index);
			tab.getListing();
		}
		else {
			TournamentDialog tab = (TournamentDialog) this.tabLeiste.getSelectedComponent();
			tab.getOpenTournaments();
		}
	}
	
}