/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONObject;

import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;

/**
 * Manage single gameDataFile for one game.
 * The gameDataFile consists of informations about an game which
 * are not readable for aLobby from SaveGame-Files or additional informations to S3-gameData.
 * These data will be used during the screenshot-upload.
 * The resulting gameDataFile will be saved in userdir and used JSON-objects for its data.
 *
 * @author Zwirni
 *
 */
public class GameDataFile
{
	
	private Map<String, String> data = new HashMap<String, String>();
	private SettlersLobby settlersLobby = null;
	private OpenGame openGame = null;
	private String gameDataFilePath = "";
	private String gameDataMd5FilePath = "";
	private static final String[] necessaryJsonValues = {
      "league",
      "tournamentid",
      "tournamentname",
      "creationTime",
      "round",
      "groupnumber",
      "matchnumber",
      "playerlist",
      "mapName"
    };
	private boolean stopSaving = false;
	
	public GameDataFile(SettlersLobby settlersLobby, OpenGame openGame) {
		this.settlersLobby = settlersLobby;
		if( openGame instanceof OpenGame ) {
			this.openGame = openGame;
			this.checkForExistingGameDataFileAndLoadData();
		}
	}

	/**
	 * Checks for an existing gameDataFile for the actual game
	 * and if it exists loads its data into the HashMap.
	 */
	@SuppressWarnings("static-access")
	private void checkForExistingGameDataFileAndLoadData() {
		if( this.openGame != null ) {
			File gamedataDir = new File(this.settlersLobby.configDir, "gamedata");
			if (!gamedataDir.exists()) {
				gamedataDir.mkdirs();
			}
			String md5 = this.openGame.getMd5();
			if( md5.length() > 0 ) {
				this.gameDataFilePath = gamedataDir + File.separator + md5 + ".txt";
				this.gameDataMd5FilePath = gamedataDir + File.separator + md5 + ".md5";
				File gameDataFile = new File(gameDataFilePath);
				File gameDataMd5File = new File(gameDataMd5FilePath);
				if( gameDataFile.exists() && gameDataMd5File.exists() ) {
					String fileContent = "";
					String fileContentMd5 = "";
					try {
						fileContent = extractFile(gameDataFile);
						fileContentMd5 = extractFile(gameDataMd5File);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
					if( fileContent.length() > 0 && fileContentMd5.length() > 0 ) {
						// check if md5-value of actual gamedatafile-content matches 
						// the content of the corresponding md5-file
						String checkFileContentMd5 = CryptoSupport.md5(fileContent).toLowerCase();
						Test.output(checkFileContentMd5 + " == " + fileContentMd5);
						if( checkFileContentMd5.equals(fileContentMd5) ){
							JSONObject obj = new JSONObject(fileContent);
							// first load all necessary values (if they exists)
							for (String s: necessaryJsonValues) {     
								if( !obj.isNull(s) ) {
									this.add(s, obj.get(s).toString() );
								}
							}
							// than add optional values from the file (if they exists)
							if( !obj.isNull("usersWithRaces") ) {
								this.add("usersWithRaces", obj.get("usersWithRaces").toString() );
							}
						}
						else {
							Test.output("Gamedatafile seems to be manipulated!");
							this.stopSaving = true;
						}
					}
				}
			}
		}
	}

	/**
	 * Extract content of a file.
	 * 
	 * @param gameDataFile
	 * @return
	 * @throws FileNotFoundException
	 */
	private String extractFile(File gameDataFile) throws FileNotFoundException {
		BufferedReader in = null;
		String zeilen = "";
        try {
            in = new BufferedReader(new FileReader(gameDataFile));
            String zeile = null;
            while ((zeile = in.readLine()) != null) {
                zeilen = zeilen + zeile;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
            }
        }
        return zeilen;
	}

	/**
	 * Adds a single information which will be one single entry in the JSON-Object.
	 * 
	 * This will be called from external.
	 * 
	 * @param string
	 * @param leagueGame
	 */
	public void add(String name, boolean value) {
		this.add(name, Boolean.toString(value));
	}
	public void add(String name, int value) {
		this.add(name, Integer.toString(value));
	}
	public void add(String name, long value) {
		this.add(name, Long.toString(value));
	}
	public void add(String name, String value) {
		data.put(name, value);
	}
	
	/**
	 * Generate the JSON-Object with all data from this class.
	 */
	@SuppressWarnings("rawtypes")
	private String generateJSONObject() {
		// create json-object
		JSONObject json = new JSONObject();
		// loop through the data
		Iterator<?> it = data.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			// add the data
			json.put((String) pair.getKey(), pair.getValue());
			it.remove(); // avoids a ConcurrentModificationException
		}
		return json.toString();
	}
	
	/**
	 * Check if allowed and necessary values are available.
	 */
	private boolean checkForAllowedValues() {
		File gameDateFile = new File(this.gameDataFilePath);
		if( gameDateFile.exists() ) {
			String fileContent = "";
			try {
				fileContent = extractFile(gameDateFile);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			if( fileContent.length() > 0 ) {
				JSONObject obj = new JSONObject(fileContent);
				int countValues = 0;
				for (String s: necessaryJsonValues) {     
					if( !obj.isNull(s) ) {
						countValues++;
					}
					else {
						Test.output("empty value: " + obj.getString(s));
					}
				}
				// not all necessary JSON Values are available in the object
				if( countValues < necessaryJsonValues.length ) {
					Test.output("Not alle necesarry JSON-Values are available - gamedatefile seems to be manipulated!");
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Saves the file with the file-contents at this moment.
	 */
	public void saveFile() {
		// only if all necessary values are available
		// and saving is not stopped (e.g. through an manipulated file)
		if( this.checkForAllowedValues() && false == this.stopSaving ) {
			String fileContent = this.generateJSONObject();
			String fileContentMd5 = CryptoSupport.md5(fileContent).toLowerCase();
			try {
				Files.write(Paths.get(this.gameDataFilePath), fileContent.getBytes());
				Files.write(Paths.get(this.gameDataMd5FilePath), fileContentMd5.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Get single entry from HashMap with data for this game.
	 */
	public String get( String name ) {
		return data.get(name);
	}
	
}