/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.MessageFormat;

import javax.swing.UIManager;

import net.siedler3.alobby.controlcenter.ReqQuery;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.i18n.I18n;

/**
 * Generate the S3.exe in Version 1.72.
 * 
 * @author Zwirni
 */

public class S3ExeGeneration172 extends S3Support {

	private RandomAccessFile file;
	private String s3ExePath;
	private SettlersLobby settlersLobby;
	
	/**
	 * Concstructor
	 * 
	 * @param file
	 * @param s3ExePath
	 * @param settlersLobby
	 */
	public S3ExeGeneration172(RandomAccessFile file, String s3ExePath, SettlersLobby settlersLobby) {
		this.file = file;
		this.s3ExePath = s3ExePath;
		this.settlersLobby = settlersLobby;
	}
	
	/**
	 * Generate the exe-file in this version.
	 * 
	 * @return boolean	true if generation is succeeded
	 */
	public boolean doGenerate() {
		try
        {
		
			String md5;
	
	        File s3Path = new File(s3ExePath).getParentFile();
	
	        File gfxDir = new File(s3Path, "GFX");
	        File Siedler3_25_f8007e01fFile = new File(gfxDir, "Siedler3_25.f8007e01f.dat");
	        md5 = getMD5Checksum(Siedler3_25_f8007e01fFile);
	        if (!md5.equalsIgnoreCase(Siedler3_25_f8007e01f_alobby))
	        {
	            // Lets fix the broken L3 Egyptian bows
	            if (md5.equalsIgnoreCase(Siedler3_25_f8007e01f_original))
	            {
	                File Siedler3_25_f8007e01fTmpFile = new File(gfxDir, "Siedler3_25.f8007e01f.tmp");
	                File Siedler3_25_f8007e01fBackupFile = new File(gfxDir, "Siedler3_25.f8007e01f.dat.original");
	                // Remove an old tmp file
	                if (Files.exists(Siedler3_25_f8007e01fTmpFile.toPath())) {
	                    // Make sure we can delete the s3_alobby.tmp file
	                    Files.setAttribute(Siedler3_25_f8007e01fTmpFile.toPath(), "dos:readonly", false);
	                    Files.deleteIfExists(Siedler3_25_f8007e01fTmpFile.toPath());
	                }
	                // Backup the original file
	                if (!Files.exists(Siedler3_25_f8007e01fBackupFile.toPath()))
	                {
	                    Files.copy(Siedler3_25_f8007e01fFile.toPath(), Siedler3_25_f8007e01fBackupFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	                }
	                Files.copy(Siedler3_25_f8007e01fFile.toPath(), Siedler3_25_f8007e01fTmpFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	                Files.setAttribute(Siedler3_25_f8007e01fTmpFile.toPath(), "dos:readonly", false);
	                file = new RandomAccessFile(Siedler3_25_f8007e01fTmpFile, "rw");
	
	                // 1
	                int offsetMapRead = 0x16D40;
	                file.seek(offsetMapRead);
	                file.writeByte(24);
	
	                // 2
	                offsetMapRead = 0x16D54;
	                file.seek(offsetMapRead);
	                file.writeByte(143);
	
	                // 3
	                offsetMapRead = 0x16D56;
	                file.seek(offsetMapRead);
	                file.writeByte(143);
	
	                // 4
	                offsetMapRead = 0x16D6C;
	                file.seek(offsetMapRead);
	                file.writeByte(142);
	
	                // 5
	                offsetMapRead = 0x16D6E;
	                file.seek(offsetMapRead);
	                file.writeByte(142);
	
	                // 6
	                offsetMapRead = 0x16D84;
	                file.seek(offsetMapRead);
	                file.writeByte(141);
	
	                // 7
	                offsetMapRead = 0x16D86;
	                file.seek(offsetMapRead);
	                file.writeByte(141);
	
	                // 8
	                offsetMapRead = 0x16D9C;
	                file.seek(offsetMapRead);
	                file.writeByte(140);
	
	                // 9
	                offsetMapRead = 0x16D9E;
	                file.seek(offsetMapRead);
	                file.writeByte(140);
	
	                // 10
	                offsetMapRead = 0x16DB4;
	                file.seek(offsetMapRead);
	                file.writeByte(139);
	
	                // 11
	                offsetMapRead = 0x16DB6;
	                file.seek(offsetMapRead);
	                file.writeByte(139);
	
	                // 12
	                offsetMapRead = 0x16DCC;
	                file.seek(offsetMapRead);
	                file.writeByte(138);
	
	                // 13
	                offsetMapRead = 0x16DCE;
	                file.seek(offsetMapRead);
	                file.writeByte(138);
	
	                // 14
	                offsetMapRead = 0x16DE4;
	                file.seek(offsetMapRead);
	                file.writeByte(137);
	
	                // 15
	                offsetMapRead = 0x16DE6;
	                file.seek(offsetMapRead);
	                file.writeByte(137);
	
	                // 16
	                offsetMapRead = 0x16DFC;
	                file.seek(offsetMapRead);
	                file.writeByte(136);
	
	                // 17
	                offsetMapRead = 0x16DFE;
	                file.seek(offsetMapRead);
	                file.writeByte(136);
	
	                // 18
	                offsetMapRead = 0x16E14;
	                file.seek(offsetMapRead);
	                file.writeByte(135);
	
	                // 19
	                offsetMapRead = 0x16E16;
	                file.seek(offsetMapRead);
	                file.writeByte(135);
	
	                // 20
	                offsetMapRead = 0x16E2C;
	                file.seek(offsetMapRead);
	                file.writeByte(134);
	
	                // 21
	                offsetMapRead = 0x16E2E;
	                file.seek(offsetMapRead);
	                file.writeByte(134);
	
	                // 22
	                offsetMapRead = 0x16E44;
	                file.seek(offsetMapRead);
	                file.writeByte(133);
	
	                // 23
	                offsetMapRead = 0x16E46;
	                file.seek(offsetMapRead);
	                file.writeByte(133);
	
	                // 24
	                offsetMapRead = 0x16E5C;
	                file.seek(offsetMapRead);
	                file.writeByte(132);
	
	                // 25
	                offsetMapRead = 0x16E5E;
	                file.seek(offsetMapRead);
	                file.writeByte(132);
	
	                // 26
	                offsetMapRead = 0x16E74;
	                file.seek(offsetMapRead);
	                file.writeByte(131);
	
	                // 27
	                offsetMapRead = 0x16E76;
	                file.seek(offsetMapRead);
	                file.writeByte(131);
	
	                // 28
	                offsetMapRead = 0x16E8C;
	                file.seek(offsetMapRead);
	                file.writeByte(130);
	
	                // 29
	                offsetMapRead = 0x16E8E;
	                file.seek(offsetMapRead);
	                file.writeByte(130);
	
	                // 30
	                offsetMapRead = 0x16EA4;
	                file.seek(offsetMapRead);
	                file.writeByte(129);
	
	                // 31
	                offsetMapRead = 0x16EA6;
	                file.seek(offsetMapRead);
	                file.writeByte(129);
	
	                // 32
	                offsetMapRead = 0x16EBC;
	                file.seek(offsetMapRead);
	                file.writeByte(128);
	
	                // 33
	                offsetMapRead = 0x16EBE;
	                file.seek(offsetMapRead);
	                file.writeByte(128);
	
	                // 34 - 1
	                offsetMapRead = 0x16EC0;
	                file.seek(offsetMapRead);
	                file.writeByte(32);
	
	                // 34 - 2
	                offsetMapRead = 0x16EC1;
	                file.seek(offsetMapRead);
	                file.writeByte(0);
	
	                // 34 - 3
	                offsetMapRead = 0x16EC2;
	                file.seek(offsetMapRead);
	                file.writeByte(0);
	
	                // 34 - 4
	                offsetMapRead = 0x16EC3;
	                file.seek(offsetMapRead);
	                file.writeByte(3);
	
	                // 35(1)
	                offsetMapRead = 0x16ED4;
	                file.seek(offsetMapRead);
	                file.writeByte(127);
	
	                // 35(2)
	                offsetMapRead = 0x16ED6;
	                file.seek(offsetMapRead);
	                file.writeByte(127);
	
	                // 36 - 1
	                offsetMapRead = 0x16ED8;
	                file.seek(offsetMapRead);
	                file.writeByte(32);
	
	                // 36 - 2
	                offsetMapRead = 0x16ED9;
	                file.seek(offsetMapRead);
	                file.writeByte(0);
	
	                // 36 - 3
	                offsetMapRead = 0x16EDA;
	                file.seek(offsetMapRead);
	                file.writeByte(0);
	
	                // 36 - 4
	                offsetMapRead = 0x16EDB;
	                file.seek(offsetMapRead);
	                file.writeByte(2);
	
	                // 37(1)
	                offsetMapRead = 0x16EEC;
	                file.seek(offsetMapRead);
	                file.writeByte(126);
	
	                // 37(2)
	                offsetMapRead = 0x16EEE;
	                file.seek(offsetMapRead);
	                file.writeByte(126);
	
	                // 38
	                offsetMapRead = 0x16EF3;
	                file.seek(offsetMapRead);
	                file.writeByte(1);
	
	                // 39(1)
	                offsetMapRead = 0x16F04;
	                file.seek(offsetMapRead);
	                file.writeByte(125);
	
	                // 39(2)
	                offsetMapRead = 0x16F06;
	                file.seek(offsetMapRead);
	                file.writeByte(125);
	
	                // 40
	                offsetMapRead = 0x16F0B;
	                file.seek(offsetMapRead);
	                file.writeByte(0);
	
	                // 41(1)
	                offsetMapRead = 0x16F1C;
	                file.seek(offsetMapRead);
	                file.writeByte(124);
	
	                // 41(2)
	                offsetMapRead = 0x16F1E;
	                file.seek(offsetMapRead);
	                file.writeByte(124);
	
	                // 42 - 1
	                offsetMapRead = 0x16F20;
	                file.seek(offsetMapRead);
	                file.writeByte(255);
	
	                // 42 - 2
	                offsetMapRead = 0x16F21;
	                file.seek(offsetMapRead);
	                file.writeByte(255);
	
	                // 42 - 3
	                offsetMapRead = 0x16F22;
	                file.seek(offsetMapRead);
	                file.writeByte(255);
	
	                // 42 - 4
	                offsetMapRead = 0x16F23;
	                file.seek(offsetMapRead);
	                file.writeByte(255);
	
	                // 43(1)
	                offsetMapRead = 0x16F34;
	                file.seek(offsetMapRead);
	                file.writeByte(123);
	
	                // 43(2)
	                offsetMapRead = 0x16F36;
	                file.seek(offsetMapRead);
	                file.writeByte(123);
	
	                // 44 - 1
	                offsetMapRead = 0x16F38;
	                file.seek(offsetMapRead);
	                file.writeByte(255);
	
	                // 44 - 2
	                offsetMapRead = 0x16F39;
	                file.seek(offsetMapRead);
	                file.writeByte(255);
	
	                // 44 - 3
	                offsetMapRead = 0x16F3A;
	                file.seek(offsetMapRead);
	                file.writeByte(255);
	
	                // 44 - 4
	                offsetMapRead = 0x16F3B;
	                file.seek(offsetMapRead);
	                file.writeByte(255);
	
	                // 45(1)
	                offsetMapRead = 0x16F4C;
	                file.seek(offsetMapRead);
	                file.writeByte(122);
	
	                // 45(2)
	                offsetMapRead = 0x16F4E;
	                file.seek(offsetMapRead);
	                file.writeByte(122);
	
	                // 46
	                offsetMapRead = 0x16F64;
	                file.seek(offsetMapRead);
	                file.writeByte(121);
	
	                // 47
	                offsetMapRead = 0x16F66;
	                file.seek(offsetMapRead);
	                file.writeByte(121);
	
	                // 48
	                offsetMapRead = 0x16F7C;
	                file.seek(offsetMapRead);
	                file.writeByte(120);
	
	                // 49
	                offsetMapRead = 0x16F7E;
	                file.seek(offsetMapRead);
	                file.writeByte(120);
	
	                file.close();
	                md5 = getMD5Checksum(Siedler3_25_f8007e01fTmpFile);
	                if (!md5.equalsIgnoreCase(Siedler3_25_f8007e01f_alobby))
	                {
	                    Test.output("Not the expected checksum of the newly created Siedler3_25.f8007e01f.tmp file: " + md5);
	                    return false;
	                }
	                Test.output("Moving Siedler3_25.f8007e01f.tmp to Siedler3_25.f8007e01f.dat...");
	                Files.move(Siedler3_25_f8007e01fTmpFile.toPath(), Siedler3_25_f8007e01fFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	                Test.output("Moved Siedler3_25.f8007e01f.tmp to Siedler3_25.f8007e01f.dat successfully.");
	                Test.output("Siedler3_25.f8007e01f.exe created successfully");
	            } else {
	                Test.output("Not the expected checksum of the Siedler3_25.f8007e01f.dat file: " + md5);
	                return false;
	            }
	        }
	
	        File s3ALobbyExeFile = new File(s3Path, "s3_alobby.exe");
	        if (Files.exists(s3ALobbyExeFile.toPath()))
	        {
	        	md5 = getMD5Checksum(s3ALobbyExeFile);
	            if (!isSupportedMd5OfS3AlobbyExe(md5))
	            {
	            	Test.output("Not the expected checksum of the s3_alobby.exe: " + md5 + ". Recreating it...");
	            	Files.setAttribute(s3ALobbyExeFile.toPath(), "dos:readonly", false);
	            	Files.deleteIfExists(s3ALobbyExeFile.toPath());
	            } else {
	            	Test.output("s3_alobby.exe was already created");
	            	if (!isGog(s3Path.toString()))
	            	{
		        		// Deactivate the intro, as otherwise there
		                // will be a message about a problem playing the intro...
	            		// Do this here, as it may have been reactivated.
		        		ReqQuery.setS3Intro(0);
	            	}
	            	return true;
	            }
	        }
	        File s3ALobbyExeTmpFile = new File(s3Path, "s3_alobby.tmp");
	        if (Files.exists(s3ALobbyExeTmpFile.toPath())) {
	        	// Make sure we can delete the s3_alobby.tmp file,
	        	// especially, if it was created by aLobby < 2.0.6
	        	Files.setAttribute(s3ALobbyExeTmpFile.toPath(), "dos:readonly", false);
	            Files.deleteIfExists(s3ALobbyExeTmpFile.toPath());
	        }
	        if (isGog(s3Path.toString()))
	        {
	            // Gget the right exe here.
	            File realS3ExeFile = new File(s3Path, "S3_multi.EXE");
	            s3ExePath = realS3ExeFile.toString();
	        } else {
	            // Get the right exe here.
	            File realS3ExeFile = new File(s3Path, "s3.exe");
	            s3ExePath = realS3ExeFile.toString();
	        }
	
	        File s3ExeFile = new File(s3ExePath);
	        md5 = getMD5Checksum(s3ExeFile);
	        if (!isSupportedMd5OfS3Exe(md5))
	        {
	            Test.output("Not the expected checksum of the Settlers 3 exe file: " + md5);
	            // load textcolor of actualy used theme as Color-Object
	            String hexcolor = null;
	            if (settlersLobby != null) {
	                hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
	            } else {
	                hexcolor = "#FFFFFF";
	            }
	            MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
	        	// -> add message
	            String text = I18n.getString("GOGSupport.ERROR_WRONG_S3_MAIN_EXE_CD");
	            if( isGog() ) {
	            	text = I18n.getString("GOGSupport.ERROR_WRONG_S3_MAIN_EXE_GOG");
	            }
	        	messagewindow.addMessage(
	    			"<html>" + MessageFormat.format(text, hexcolor) + "</html>", 
	    			UIManager.getIcon("OptionPane.errorIcon")
				);
	    		// -> add ok-button
	    		messagewindow.addButton(
	    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	    			new ActionListener() {
	    			    @Override
	    			    public void actionPerformed(ActionEvent e) {
	    			    	messagewindow.dispose();
	    			    }
	    			}
	    		);
	    		// show it
	    		messagewindow.setVisible(true);
	            return false;
	        }
	        Files.copy(s3ExeFile.toPath(), s3ALobbyExeTmpFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	
	        // The file may be set to the Windows-like "Read-only".
	        // We need to get rid of that attribute, as it caused trouble for
	        // many people with aLobby < 2.0.6 (writing to such a file is not
	        // possible).
	        Files.setAttribute(s3ALobbyExeTmpFile.toPath(), "dos:readonly", false);
	        file = new RandomAccessFile(s3ALobbyExeTmpFile, "rw");
	
	        // Set a different version for the s3.exe, so it is clear, that this is a different one
	        // "1.60" is "160"; "1.71" is "171"; "1.72" is "172"
	        int offsetMapRead = 0x2970F0;
	        file.seek(offsetMapRead);
	        file.writeByte(172);
	
	        // Fix the broken bee-sound by completely disabling that sound
	        offsetMapRead = 0xB2F00;
	        file.seek(offsetMapRead);
	        file.writeByte(233);
	
	        offsetMapRead = 0xB2F01;
	        file.seek(offsetMapRead);
	        file.writeByte(153);
	
	        offsetMapRead = 0xB2F02;
	        file.seek(offsetMapRead);
	        file.writeByte(0);
	
	        offsetMapRead = 0xB2F05;
	        file.seek(offsetMapRead);
	        file.writeByte(144);
	
	        // Fix the broken Ch'ih Yu's Shield Asian spell
	        offsetMapRead = 0x10F270;
	        file.seek(offsetMapRead);
	        file.writeByte(135);
	
	        // Fix the broken Cursed Arrows Amazon spell
	        offsetMapRead = 0x10F360;
	        file.seek(offsetMapRead);
	        byte[] bytes = hexStringToByteArray("E9F367220090");
	        file.write(bytes);
	
	        offsetMapRead = 0x335B58;
	        file.seek(offsetMapRead);
	        bytes = hexStringToByteArray("31D28A8F745E2A018A95615E2A01833C95648F65000275278A97615E2A018A1495648F650083FA017C1583FA097F1088CA80E21080FA10750631D289542410E9CA97DDFF");
	        file.write(bytes);
	
	        if (!isGog(s3Path.toString()))
	        {
	        	// Change the rest of the s3.exe to make sure, it is exactly the same like the one from GOG
	            offsetMapRead = 0x1380F;
	            file.seek(offsetMapRead);
	            file.writeByte(46);
	            
	            offsetMapRead = 0x13845;
	            file.seek(offsetMapRead);
	            file.writeByte(0);
	            offsetMapRead = 0x13846;
	            file.seek(offsetMapRead);
	            file.writeByte(0);
	            
	            offsetMapRead = 0x13947;
	            file.seek(offsetMapRead);
	            file.writeByte(235);
	        	
	            offsetMapRead = 0x297684;
	            file.seek(offsetMapRead);
	            file.writeByte(46);
	            offsetMapRead = 0x297685;
	            file.seek(offsetMapRead);
	            file.writeByte(92);
	            
	            // Create necessary DAT files
	            File s3Dat = new File(s3Path, "S3.DAT");
	            s3Dat.createNewFile();
	            
	            File s3Qota1Dat = new File(s3Path, "S3QOTA1.DAT");
	            s3Qota1Dat.createNewFile();
	            
	            File s3Mcd1Dat = new File(s3Path, "S3MCD1.DAT");
	            s3Mcd1Dat.createNewFile();
	            
	            File s3Gold1Dat = new File(s3Path, "S3GOLD1.DAT");
	            s3Gold1Dat.createNewFile();
	            
	            File s3Gold2Dat = new File(s3Path, "S3GOLD2.DAT");
	            s3Gold2Dat.createNewFile();
	            
	            File s3Cd1Dat = new File(s3Path, "S3CD1.DAT");
	            if (s3Cd1Dat.createNewFile())
	            {
	            	RandomAccessFile s3Cd1DatRa = new RandomAccessFile(s3Cd1Dat,"rw");
	                s3Cd1DatRa.writeByte(1);
	                s3Cd1DatRa.close();
	            }
	
	            File s3Cd2Dat = new File(s3Path, "S3CD2.DAT");
	            if (s3Cd2Dat.createNewFile())
	            {
	                RandomAccessFile s3Cd2DatRa = new RandomAccessFile(s3Cd2Dat,"rw");
	                s3Cd2DatRa.writeByte(2);
	                s3Cd2DatRa.close();
	            }
	            
	    		// Deactivate the intro, as otherwise there
	            // will be a message about a problem playing the intro...
	    		ReqQuery.setS3Intro(0);
	        }
	        
	        file.close();
	        Test.output("Moving s3_alobby.tmp to s3_alobby.exe...");
	        Files.move(s3ALobbyExeTmpFile.toPath(), s3ALobbyExeFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	        Test.output("Moved s3_alobby.tmp to s3_alobby.exe successfully.");
	        md5 = getMD5Checksum(s3ALobbyExeFile);
	        if (!isSupportedMd5OfS3AlobbyExe(md5))
	        {
	        	Test.output("Not the expected checksum of the newly created s3_alobby.exe: " + md5);
	        	Files.deleteIfExists(s3ALobbyExeFile.toPath());
	        	return false;
	        }
	        Test.output("s3_alobby.exe created successfully");
	        return true;
        }
		catch(Exception e)
        {
            Test.outputException(e);
            if (file != null) {
            	try {
    				file.close();
    			} catch (IOException e2) {
    				Test.outputException(e2);
    			}
            }
            return false;
        }
	}
	
	/**
     * Check if the given path is a supported unified exe-file by a given md5-hash
     * depending on given version.
     * 
     * @param filePath
     * @return boolean	if it is supported
     */
    public static boolean isSupportedMd5OfS3AlobbyExe(String md5)
    {
        return md5.equalsIgnoreCase(s3172ALOBBYexeMd5)
                || md5.equalsIgnoreCase(s3172ALOBBYexeGog23266Md5);
    }
	
}