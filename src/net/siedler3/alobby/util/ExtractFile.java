/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

/**
 * Extract a file to a given directory.
 */
public class ExtractFile
{

    /**
     * Create a directory to hold temporary data.
     * 
     * @return File
     * @throws IOException
     */
    public static File createTempDir() throws IOException
    {
        File dir = null;
        Path tmpDir = Files.createTempDirectory("alobby_");
        dir = tmpDir.toFile();
        return dir;
    }

    /**
     * Extra a given file to a directory.
     * 
     * @param resource
     * @param filename
     * @param directory
     * @return
     * @throws IOException
     */
    public static File extract(InputStream resource, String filename, File directory) throws IOException
    {
        File result = null;
        result = new File(directory, filename);
        try
        {
            Files.copy(resource, result.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        finally
        {
            resource.close();
        }
        return result;
    }
}
