package net.siedler3.alobby.util.s4support;
/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.state.UserInGame;
import net.siedler3.alobby.controlcenter.state.UserInGameList;
import net.siedler3.alobby.gui.additional.MapUserListCellRenderer;
import net.siedler3.alobby.gui.additional.MapUserListModel;
import net.siedler3.alobby.gui.additional.addCursorChangesToButton;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.CryptoSupport;

/**
 * Creates and handle the Window with the list of users in a S4 game
 * and its options.
 */
public class S4GameOptions extends JPanel
{    
    private JList<UserInGame> mapUserJList;
    private JScrollPane scrollpaneMapUserList;
    private JLabel label;
    private UserInGameList users;
    private SettlersLobby settlersLobby;
    private JButton btnHostStart = new JButton("<html><div style='text-align: center;'>" + I18n.getString("GUI.S4_START") + "</div></html>");
    private JButton btnHostClose = new JButton("<html><div style='text-align: center;'>" + I18n.getString("GUI.S4_CLOSE") + "</div></html>");
    private JButton btnPlayerClose = new JButton("<html><div style='text-align: center;'>" + I18n.getString("GUI.S4_LEAVE") + "</div></html>");
    private JPanel pnlHostButtons = new JPanel(new GridLayout(1, 3));
    private JPanel pnlPlayerButtons = new JPanel(new GridLayout(1, 1));

    public S4GameOptions(SettlersLobby settlersLobby)
    {
        super(new BorderLayout());
        this.settlersLobby = settlersLobby;
        
        mapUserJList = new JList<UserInGame>();
        mapUserJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mapUserJList.setLayoutOrientation(JList.VERTICAL);
        mapUserJList.setVisibleRowCount(8);
        mapUserJList.setCellRenderer(new MapUserListCellRenderer(settlersLobby, false));
        //wichtig für die korrekte Grössenberechnung (speziell die Höhe)
        mapUserJList.setPrototypeCellValue(new UserInGame("prototypeUsername"));
        scrollpaneMapUserList = new JScrollPane(mapUserJList);
        
        label = new JLabel(" ");
        add(label, BorderLayout.PAGE_START);
        add(scrollpaneMapUserList);
        
        // define the button to close the game as host
        btnHostClose.setHorizontalAlignment(SwingConstants.CENTER);
        btnHostClose.addActionListener(new ActionListener() {
        	@Override
		    public void actionPerformed(ActionEvent e) {
        		// remove the open game
        		OpenGame game = settlersLobby.getOpenHostGame();
        		if( game != null ) {
        			// -> send info in channel about closing this game
        			settlersLobby.sendProgramMessageCloseGame(game);
        			// -> remove game from own open game list and reset all settings
        			game.destroy();
        			// -> remove game as own hosted game
        			settlersLobby.setAsOpenHostGame(null);
        			// -> remove options-window
        			Test.output("setS4GameOptions ffff");
        			settlersLobby.getGUI().setS4GameOptions(new UserInGameList(), true, false);
        			// -> set user as not playing anymore
            		settlersLobby.getAwayStatus().onGameClosed();
        		}
        	}
        });
        btnHostClose.addMouseListener(new addCursorChangesToButton());

        // define the leave-open-game-button for players
        btnPlayerClose.setHorizontalAlignment(SwingConstants.CENTER);
        btnPlayerClose.addActionListener(new ActionListener() {
        	@Override
		    public void actionPerformed(ActionEvent e) {
        		// leave the game
        		// -> inform host about leaving
        		settlersLobby.getGameState().userHasLeftGame(true);
        		// -> remove options-window
        		Test.output("setS4GameOptions gggg");
        		settlersLobby.getGUI().setS4GameOptions(new UserInGameList(), false, false);
        		// -> send info about decrement the player-number to host
        		settlersLobby.onSendDecrementPlayerNumber();
        		// -> reset marker for open joined game
        		settlersLobby.setAsOpenJoinGame(null);
        		// -> set user as not playing anymore
        		settlersLobby.getAwayStatus().onGameClosed();
        	}
        });
        btnPlayerClose.setEnabled(false);
        btnPlayerClose.addMouseListener(new addCursorChangesToButton());
        
        // define the start button
        btnHostStart.setHorizontalAlignment(SwingConstants.CENTER);
        btnHostStart.addActionListener(new ActionListener() {
        	@Override
		    public void actionPerformed(ActionEvent e) {
        		// Start the game only if all users joined
        		// -> get the game-object
        		OpenGame openGame = settlersLobby.getOpenHostGame();
        		// -> send info to all players in this game that it will be started
        		sendProgramMessageToStartS4Game(openGame);
        		// -> send info to all users in channel about the now closed openGame
        		settlersLobby.sendProgramMessageCloseGame(openGame);
        		// -> set the game as new running game
        		settlersLobby.setAsRunningHostGame(openGame);
                // -> send info about the new running game to all users in the channel
                settlersLobby.sendProgramMessageGameStarted(openGame);
                // -> start game as host
        		settlersLobby.startS4GameAsHost(openGame, getIpUserList(openGame));
                // -> remove game from own open game list and reset all settings
                openGame.destroy();
                // -> remove options-window
                Test.output("setS4GameOptions hhhh");
    			settlersLobby.getGUI().setS4GameOptions(new UserInGameList(), true, false);
        	}
        });
        // -> set it initially to disabled, it will be enabled if userlist is complete
        btnHostStart.setEnabled(false);
        btnHostStart.addMouseListener(new addCursorChangesToButton());
        
        // put both buttons into a panel for the host
        pnlHostButtons.add(btnHostStart);
        pnlHostButtons.add(btnHostClose);
        
        // and one button into a panel for the players
        pnlPlayerButtons.add(btnPlayerClose);
        
    }

    /**
     * Send info to all players in this game,
     * but not the host himself, that the game will be started.
     * This message must include a list of all player with their ips
     * which will be encoded as the list consists of ";".
     * 
     * @param openGame
     */
	@SuppressWarnings("static-access")
	protected void sendProgramMessageToStartS4Game(OpenGame openGame) {
		String ipUserList = getIpUserList( openGame );
		
		// loop through the players and send the message
		for (String user : openGame.getPlayerList() )
        {
			if( !user.equals(settlersLobby.getNick()) ) {
				settlersLobby.getIRCCommunicator().sendProgramMessage(settlersLobby.getProgrammMessageProtocol().createMsgStartS4Game(openGame, user, CryptoSupport.encode(ipUserList, settlersLobby.getNick(), settlersLobby.getIRCCommunicator().CRYPTO_MULTIPART_MESSAGE_TOKEN)));
			}
        }
	}

	/**
	 * Generate player-ip-list for irc-message.
	 * 
 	 * Format: nickname1,ip1;nickname2,ip2;nickname3,ip3 .
 	 * 
	 * @param openGame
	 * 
	 * @return String
	 */
	private String getIpUserList(OpenGame openGame) {
		// -> get the userList from gameState where the ips from each user are saved
		UserInGameList userList = settlersLobby.getGameState().getUserList();
		// -> loop through the users in game, get their ips from userlist
		//    and add them to the list for the message
		String ipUserList = "";
		for(String user : openGame.getPlayerList() ) {
			UserInGame userInGame = userList.getUser(user);
			if( userInGame != null ) {
				String ip = userInGame.getIp();
				// -> if this is the host, get his ip from his own settings
				if( openGame.getHostUserName().equals(user) && settlersLobby.getOpenHostGame() != null ) {
					ip = settlersLobby.ipToUse();
				}
				ipUserList = ipUserList + user + "," + ip + ";";
			}
		}
		return ipUserList;
	}

	/**
     * Display the list of users for this s4-game.
     * This function is called on each change of the userlist,
     * so we can use it to change the button-states.
     * 
     * @param userList
     */
    public void displayUsers(UserInGameList userList)
    {
        try
        {
        	// get the actual userlist
            users = userList.clone();
        }
        catch (CloneNotSupportedException e) {
            Test.outputException(e);
            users = new UserInGameList();
        }
        // create the model with the userlist
        MapUserListModel model = new MapUserListModel(users.getUserList());
        mapUserJList.setModel(model);
        
        // set the title-text for the list
        String text = " " + S4Map.getDisplayName(users.getMapName()).replace(File.separator + ALobbyConstants.PATH_S4MULTI, "");
        label.setText(text);
        // -> also as tooltip
        label.setToolTipText(text);
        
        // if users are in list, show the list
        if (model.getSize() > 0)
        {
            setVisible(true);
            
            // set the of player-button (its disabled by default as the button is
            // visible during game-join and click on it would break the function in this moment)
            if( model.getSize() > 1 ) {
            	btnPlayerClose.setEnabled(true);
            }
            else {
            	btnPlayerClose.setEnabled(false);
            }
            
            // set the state of the host-button to start the game depending on the player-count
            // -> only if min. 1 players as in the game
            // -> We allow starting even if only one player is there, as currently we have a bug,
            //    where the number of players always drops to 1 after some time even though the players
            //    are still in the game and are show in the list...
            // hint: if the game is not full at start, the open spots will be filled with AI
            OpenGame openGame = settlersLobby.getOpenHostGame();
            if( openGame != null ) {
            	if( openGame.getCurrentPlayer() >= 1 ) {
            		try {
            			// wait 10 seconds so that all player can get the gamedata
						Thread.sleep(10000);
						btnHostStart.setEnabled(true);
					} catch (InterruptedException e) {
						Test.outputException(e);
					}
            	}
            	else {
            		btnHostStart.setEnabled(false);
            	}
            }
        }
        else
        {
        	// if list is empty hide the list
            setVisible(false);
        }
    }

    /**
     * Add button for player:
     * - remove player from list
     */
	public void addPlayerButtons() {
		add(pnlPlayerButtons, BorderLayout.SOUTH);
		remove(pnlHostButtons);
	}
    
    /**
     * Add buttons for host:
     * - close game
     * - start game
     */
	public void addHostButtons() {
		add(pnlHostButtons, BorderLayout.SOUTH);
		remove(pnlPlayerButtons);
	}

}
