package net.siedler3.alobby.util.s4support;

public enum GoodsInStock
{
    Wenig(0),
    Mittel(1),
    Viel(2);

    private int index;
    private GoodsInStock(int index)
    {
        this.index = index;
    }

    public int getIndex()
    {
        return index;
    }

    public String getLanguageString()
    {
        S4Language lang = S4Language.getInstance();
        String key = lang.goodsInStock[index];
        return key;
    }
    public String getLanguageMiniChatString()
    {
        S4Language lang = S4Language.getInstance();
        String key = lang.goodsInStockMiniChat[index];
        return key;
    }

    public static GoodsInStock getGoodsInStockByIndex(int index)
    {
        for (GoodsInStock goods : values())
        {
            if (goods.index == index)
            {
                return goods;
            }
        }
        return null;
    }
}