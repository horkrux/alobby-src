/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import net.siedler3.alobby.controlcenter.Test;


public class CryptoSupport
{

    private static final String CHARSET = "UTF-8";

    private enum PwType
    {
        type1, //use pwAddition1
        type2  //use pwAddition2
    }
    private static final byte [] pwAddition1 =
    {
        0x1c, 0x5b, 0x79, 0x06, 0x17, 0x65, 0x32, 0x2a
    };

    private static final byte [] pwAddition2 =
    {
        0x27, 0x5F, 0x60, 0x38, 0x00, 0x7D, 0x23, 0x1D
    };


    private static final Random random = new Random();
    private static final int SCRAMBLE_LENGTH = 4;


    public static String encode(String input, String password, String cryptoIndicator)
    {
        String result;
        try
        {
        	Test.output("crypto-debug2:" + input);
            result = cryptoIndicator +
                CryptoSupport.encode(input, password, PwType.type1);
        }
        catch (Exception e)
        {
            //input bleibt unverändert
            result = input;
            Test.output("ERROR, encryption failed!");
            Test.outputException(e);
        }
        return result;
    }

    public static String decode(String input, String password, String cryptoIndicator)
    {
        String result;
        if (!input.startsWith(cryptoIndicator))
        {
            //Nachricht unverändert lassen
            return input;
        }
        try
        {
            //input ohne CryptoIndcator entschlüsseln
            result = CryptoSupport.decode(input.substring(cryptoIndicator.length()), password, PwType.type1);
        }
        catch (Exception e)
        {
            //wenn die Entschlüsselung fehlschlägt
            //geht der input unverändert zurück
            result = input;
            Test.output("ERROR, decryption failed! ");
            Test.outputException(e);
        }
        return result;
    }


    public static String encode(String input, String password, PwType pwType) throws Exception
    {
        String result;

        byte[] scrambledInput = scramble(input.getBytes(CHARSET));
        byte[] encoded = encodeAes(scrambledInput, password, pwType);
        result = new String(Base64Coder.encode(encoded, encoded.length));
        return result;
    }


    public static String decode(String input, String password, PwType pwType) throws Exception
    {
        String result;
        byte[] data;
        data = Base64Coder.decode(input);
        data = decodeAes(data, password, pwType);
        data = unscramble(data);
        result = new String(data, CHARSET);
        return result;
    }

    public static byte[] scramble(byte[] input)
    {
        //mit einem 4 Byte Zufallswert
        //wir ein XOR über den kompletten input gemacht.
        //der Zufallswert wird am Anfang eingefügt,
        //da er fürs unscramble benötigt wird.
        int val = random.nextInt();
        byte[] scramble = new byte[] {
                (byte) (val >>> 24 & 0xff), (byte) (val >>> 16 & 0xff),
                (byte) (val >>> 8 & 0xff), (byte) (val & 0xff)
        };
        int len = scramble.length;
        byte[] result = new byte[input.length + len];
        System.arraycopy(scramble, 0, result, 0, len);
        int j = 0;
        for (int i = 0; i < input.length; ++i)
        {
            result[i + len] = (byte) (input[i] ^ scramble[j]);
            j = (j + 1) % len;
        }
        return result;
    }

    public static byte[] unscramble(byte[] input)
    {
        //inverse operation zu scramble
        //der 4 Byte Zufallswert steht am Anfang
        if (input.length < SCRAMBLE_LENGTH)
        {
            throw new IndexOutOfBoundsException("invalid length: " + input.length);
        }
        byte[] result = new byte[input.length - SCRAMBLE_LENGTH];
        int j = 0;
        for (int i = 0; i < result.length; ++i)
        {

            result[i] = (byte) (input[i + SCRAMBLE_LENGTH] ^ input[j]);
            j = (j + 1) % SCRAMBLE_LENGTH;
        }
        return result;

    }

    public static byte[] encodeAes( byte[] bytes, String password, PwType pwType) throws Exception
    {
        byte[] key = getAesKey(password, pwType);
        return rawEncodeAes(bytes, key);
    }

    public static byte[] decodeAes( byte[] bytes, String password, PwType pwType) throws Exception
    {
        byte[] key = getAesKey(password, pwType);
        return rawDecodeAes(bytes, key);
    }

    private static byte[] rawEncodeAes(byte[] bytes, byte[] key) throws Exception
    {
        Key k = new SecretKeySpec( key, "AES" );
        Cipher c = Cipher.getInstance( "AES" );
        c.init( Cipher.ENCRYPT_MODE, k );
        return c.doFinal(bytes);
    }

    private static byte[] rawDecodeAes(byte[] bytes, byte[] key) throws Exception
    {
        Key k = new SecretKeySpec( key, "AES" );
        Cipher c = Cipher.getInstance( "AES" );
        c.init( Cipher.DECRYPT_MODE, k );
        return c.doFinal(bytes);
    }

    private static byte[] getAesKey(String password, PwType pwType) throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
        byte[] pwAddition;
        switch (pwType)
        {
            case type2:
                pwAddition = CryptoSupport.pwAddition2;
                break;
            case type1:
            default:
                pwAddition = CryptoSupport.pwAddition1;
                break;

        }
        byte[] input = password.getBytes(CHARSET);
        int len = pwAddition.length / 2;

        byte[] keyInput = new byte[input.length + pwAddition.length];
        System.arraycopy(pwAddition, 0, keyInput, 0, len);
        System.arraycopy(input, 0, keyInput, len, input.length);
        System.arraycopy(pwAddition, len, keyInput, input.length + len, len);
        keyInput = md5(keyInput);
        byte[] key = new byte[128 / 8]; //128 bit key
        System.arraycopy(keyInput, 0, key, 0, key.length);
        return key;
    }

    /**
     * computes the MD5 checksum of <code>input</code>.
     *
     * @param input
     * @return MD5 checksum
     * @throws NoSuchAlgorithmException
     */
    private static byte[] md5(byte[] input) throws NoSuchAlgorithmException
    {
        MessageDigest algorithm = MessageDigest.getInstance("MD5");
        algorithm.reset();
        algorithm.update(input);
        byte[] messageDigest = algorithm.digest();

        return messageDigest;
    }

    /**
     * berechnet den MD5-Hashwert vom übergebenen String.
     *
     * @param input Klartext
     * @return MD5 als HexString, null falls ein interner Fehler auftritt.
     */
    public static String md5(String input)
    {
        String result = null;
        try
        {
            result = toHexString(md5(input.getBytes(CHARSET)));
        }
        catch (Exception e)
        {
        }
        return result;
    }

    /**
     *
     * @param input input that will be encrypted, in this case the IRC password
     * @param password internal password that is used for the encryption
     * @return encoded IRC password or null
     */
    public static String encodePw(String input)
    {
        String result;
        String internalPw = System.getProperty("user.name", "");
        try
        {
            result = CryptoSupport.encodePw(input, internalPw, PwType.type2);
        }
        catch (Exception e)
        {
            result = null;
            Test.outputException(e);
        }
        return result;
    }

    /**
     *
     * @param input input that will be decrypted, in this case the encrypted IRC password
     * @return decoded IRC password or null
     */
    public static String decodePw(String input)
    {
        String result;
        String internalPw = System.getProperty("user.name", "");
        try
        {
            result = CryptoSupport.decodePw(input, internalPw, PwType.type2);
        }
        catch (Exception e)
        {
            result = null;
            Test.outputException(e);
        }
        return result;
    }


    private static String encodePw(String input, String password, PwType pwType) throws Exception
    {
        String result;

        byte[] encoded = encodeAes(input.getBytes(CHARSET), password, pwType);
        result = new String(Base64Coder.encode(encoded, encoded.length));
        return result;
    }

    private static String decodePw(String input, String password, PwType pwType) throws Exception
    {
        String result;
        byte[] data;
        data = Base64Coder.decode(input);
        data = decodeAes(data, password, pwType);
        result = new String(data, CHARSET);
        return result;
    }

    public static String encodePasswordForIrcTransmission(String plainPassword) throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
        if (plainPassword == null)
        {
            throw new NullPointerException();
        }
        if (plainPassword.isEmpty())
        {
            throw new IllegalArgumentException();
        }
        String result;
        String input = "siedler3" + plainPassword;
        byte[] data = md5(input.getBytes(CHARSET));
        data = Arrays.copyOfRange(data, 0, 15); //15 byte => 20 chars after Base64coder
        result = new String(Base64UrlCoder.encode(data));
        return result;
    }



    public static byte[] messageDigest( InputStream in, String algo ) throws Exception
    {
        MessageDigest messageDigest = MessageDigest.getInstance( algo );
        byte[] md = new byte[ 8192 ];

        for ( int n = 0; (n = in.read( md )) > -1; )
        {
            messageDigest.update( md, 0, n );
        }

        return messageDigest.digest();
    }

    /**
     * berechnet den MD5-Hashwert einer Datei.
     *
     * @param file
     * @return
     */
    public static String calcMD5(File file)
    {
        String result = null;
        FileInputStream is = null;
        try
        {
            is = new FileInputStream(file);
            byte[] digest = messageDigest(is, "MD5");
            result = toHexString(digest);
        }
        catch (Exception e)
        {
        	Test.outputException(e);
        }
        finally
        {
            if (is != null)
            {
                try
                {
                    is.close();
                }
                catch (Exception e2)
                {}
            }
        }
        return result;
    }

    /**
     * Berechnet die MD5-Checksumme über inputFile und vergleicht sie mit
     * dem Referenzwert in md5DigestFile. md5DigestFile ist in dem Format,
     * wie es von dem Programm md5sum erzeugt wird.
     *
     * @param inputFile
     * @param md5DigestFile .md5 Datei
     * @return
     */
    public static boolean Md5Sum(File inputFile, File md5DigestFile)
    {
        String md5 = calcMD5(inputFile);
        String referenceMd5 = null;
        FileInputStream fis = null;
        BufferedReader in = null;
        try
        {

            fis = new FileInputStream(md5DigestFile);
            in = new BufferedReader(new InputStreamReader(fis));
            String line;
            //Format:  hexstring *filename.txt
            //der Stern steht für Binärdatei, ein weiteres Space wäre eine
            //Textdatei, dies wird aber von uns nicht unterstützt
            Pattern pattern = Pattern.compile("(\\p{XDigit}{32}) \\*(.+)");
            Matcher matcher;

            //in einer .md5 Datei können die Checksummen von mehreren Dateien
            //stehen. Daher muss erstmal die Zeile gesucht werden, die zur
            //inputDatei passt.
            while ((line = in.readLine()) != null)
            {
                Test.output(line);
                matcher = pattern.matcher(line);
                if (matcher.matches())
                {
                    if (inputFile.getAbsolutePath().endsWith(matcher.group(2)))
                    {
                        referenceMd5 = matcher.group(1);
                        break;
                    }
                }
            }
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
        finally
        {
            if (in != null)
            {
                try {
					in.close();
				} catch (IOException e) {
					Test.outputException(e);
				}
            }
            if (fis != null)
            {
                try {
					fis.close();
				} catch (IOException e) {
					Test.outputException(e);
				}
            }
        }
        if (md5 == null || referenceMd5 == null)
        {
            return false;
        }
        return md5.equalsIgnoreCase(referenceMd5);
    }


    /**
     * Berechnet die MD5-Checksumme über inputFile und vergleicht sie mit
     * dem Referenzwert.
     *
     * @param inputFile
     * @param referenceMd5
     * @return
     */
    public static boolean Md5Sum(File inputFile, String referenceMd5)
    {
        String md5 = calcMD5(inputFile);

        if (md5 == null || referenceMd5 == null)
        {
            return false;
        }
        return md5.equalsIgnoreCase(referenceMd5);
    }

    
    /**
     * überprüft, ob alle der im md5DigestFile aufgelisteten Dateien
     * vorhanden sind und mit dem Hashwert übereinstimmen.
     * Enthält die Datei keinen gültigen Eintrag, wird false zurückgegeben.
     * @param md5DigestFile
     * @return
     */
    public static boolean md5Sum(File md5DigestFile)
    {
        FileInputStream fis = null;
        boolean result = false;
        try
        {
            fis = new FileInputStream(md5DigestFile);
            result = md5Sum(fis, md5DigestFile.getParentFile());
        }
        catch(FileNotFoundException ee)
        {
           result = false;
        }
        finally
        {
            try
            {
                if (fis != null)
                {
                    fis.close();
                }
            }
            catch (IOException e)
            {
            }
        }
        return result;
    }


    /**
     * überprüft, ob alle der im md5DigestFileInputStream aufgelisteten Dateien
     * im Verzeichnis parentDirectory vorhanden sind und mit dem Hashwert übereinstimmen.
     * Enthält der InputStream keinen gültigen Eintrag, wird false zurückgegeben.
     * @param md5DigestFileInputStream
     * @param parentDirectory
     * @return
     */
    public static boolean md5Sum(InputStream md5DigestFileInputStream, File parentDirectory)
    {

        boolean result = true;
        boolean digestFileContainsEntries = false;
        String referenceMd5 = null;
        BufferedReader in = null;
        try
        {
            in = new BufferedReader(new InputStreamReader(md5DigestFileInputStream));
            String line;
            //Format:  hexstring *filename.txt
            //der Stern steht für Binärdatei, ein weiteres Space wäre eine
            //Textdatei, dies wird aber von uns nicht unterstützt
            Pattern pattern = Pattern.compile("(\\p{XDigit}{32}) \\*(.+)");
            Matcher matcher;

            //in einer .md5 Datei können die Checksummen von mehreren Dateien
            //stehen. Für jede wird überprüft, ob die referenzierte Datei vorhanden
            //ist und der Hashwert übereinstimmt.
            while ((line = in.readLine()) != null)
            {
                Test.output(line);
                matcher = pattern.matcher(line);
                if (matcher.matches())
                {
                    //wenigstens einer dem Format entsprechender Eintrag
                    //wurde gefunden
                    digestFileContainsEntries = true;

                    referenceMd5 = matcher.group(1);
                    File file =  new File(parentDirectory, matcher.group(2));
                    if (!file.exists())
                    {
                        Test.output("file not found: " + file.getAbsolutePath());
                        return false;
                    }
                    String md5 = calcMD5(file);
                    if (md5 == null || referenceMd5 == null)
                    {
                    	Test.output("md5 of file " + file.toString() + " is null");
                        return false;
                    }
                    Test.output(referenceMd5);
                    if (!md5.equalsIgnoreCase(referenceMd5))
                    {
                    	Test.output("md5 '" + md5 + "' of file " + file.toString() + " is not equal to '" + referenceMd5 + "'");
                    }
                    result &= md5.equalsIgnoreCase(referenceMd5);
                }
            }
        }
        catch (Exception e)
        {
            Test.outputException(e);
            result = false;
        }
        finally
        {
            if (in != null)
            {
                try {
					in.close();
				} catch (IOException e) {
					Test.outputException(e);
				}
            }
            if (md5DigestFileInputStream != null)
            {
                try {
                	md5DigestFileInputStream.close();
				} catch (IOException e) {
					Test.outputException(e);
				}
            }
        }
        return digestFileContainsEntries && result;
    }



    static char[] hexChar = {
        '0' , '1' , '2' , '3' ,
        '4' , '5' , '6' , '7' ,
        '8' , '9' , 'A' , 'B' ,
        'C' , 'D' , 'E' , 'F'
    };

    public static String toHexString ( byte[] b )
    {
        StringBuffer sb = new StringBuffer( b.length * 2 );
        for ( int i = 0; i < b.length; i++ ) {
            // look up high nibble char
            sb.append( hexChar [( b[i] & 0xf0 ) >>> 4] ); // fill left with zero bits

            // look up low nibble char
            sb.append( hexChar [b[i] & 0x0f] );
        }
        return sb.toString();
    }


}
