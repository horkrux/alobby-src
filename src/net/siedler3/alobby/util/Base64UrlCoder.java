package net.siedler3.alobby.util;

import net.siedler3.alobby.controlcenter.ALobbyConstants;

/**
 * identical to Base64Coder, but uses instead of '+' and '/' the character '-' and '_'.
 *
 */
public class Base64UrlCoder extends Base64Coder
{
    static {
        map1[62] = '-';
        map1[63] = '_';
        for (int i = 0; i < map2.length; i++)
        {
            map2[i] = -1;
        }
        for (int i = 0; i < 64; i++)
        {
            map2[map1[i]] = (byte) i;
        }
    }

    public static String encodeString (String s) {
        return new String(encode(s.getBytes(ALobbyConstants.UTF_8))); }

    public static char[] encode (byte[] in) {
        return encode(in,in.length); }

    public static char[] encode (byte[] in, int iLen) {
        return Base64Coder.encode(in, iLen);
    }

    public static String decodeString (String s) {
        return new String(decode(s), ALobbyConstants.UTF_8); }

    public static byte[] decode (String s) {
        return decode(s.toCharArray()); }

    public static byte[] decode (char[] in)
    {
        return Base64Coder.decode(in);
    }

}
