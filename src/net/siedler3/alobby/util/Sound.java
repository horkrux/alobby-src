// This class provides the necessary functions for playing sounds 

// define package
package net.siedler3.alobby.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedInputStream;

import javax.sound.sampled.AudioInputStream;
//add audiosystem for beep sound
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;

import net.siedler3.alobby.controlcenter.Test;

public class Sound {
	
	//add variables for beep sound
	private String soundFile;
	private Clip clip = null;
	private LineListener listener;
	
	// constructor for this class with no options
	public Sound()
	{
		this.listener = null;
		// get the list of audio-devices on this system
		Mixer.Info[] mixers = AudioSystem.getMixerInfo();
		// create a clip if and audio-device is available
		if( mixers.length > 0 ) {
			try {
				this.clip = AudioSystem.getClip();
			} catch (LineUnavailableException e) {
				Test.outputException(e);
			}
		}
	}
	
	// add a listener to Object which is used during playing the sound
	public void addListener( LineListener listener )
	{
		this.listener = listener;
	}
	
	// remove lister from Object
	public void removeListener( LineListener listener )
	{
		if( this.clip != null ) {
			this.clip.removeLineListener(listener);
		}
	}
	
	// set file to play
	public void addFileAsString( String soundFile )
	{
		this.soundFile = soundFile;
	}
	
	// function to call when a sound should be played
	public void play()
	{
		if( null != this.soundFile )
		{
	        try {
	        	// get audiostream
	        	AudioInputStream audioInputStream = getAudioInputStream();
	        	
	        	// continue only if audioStream exists
	        	if( this.clip != null && null != audioInputStream )
	        	{
		        	// add the existing audiostream to clip
		            this.clip.open(audioInputStream);
		            // add listener to clip
		            // -> at this time because the audioInputStream must added first
		            if( null != this.listener )
		            {
		            	this.clip.addLineListener(this.listener);
		            }
		            // start clip (this plays the sound through the audiostream)
		            this.clip.start();
	        	}
	        }
	        catch (Exception e)
	        {
	            Test.outputException(e);
	        }
		}
	}
	
	// stop playback
	public void stop()
	{
		if( this.clip != null ) {
			this.clip.stop();
		}
	}
	
	// close playback
	public void close()
	{
		if( this.clip != null ) {
			this.clip.close();
		}
	}
	
	// prepare an audiostream-Object for playing the soundfile
    private AudioInputStream getAudioInputStream() throws UnsupportedAudioFileException, IOException
    {
    	
    	// initialize the audioStream
        AudioInputStream audioInputStream = null;
        
        // play sound not within a jar-file
        // -> access to files in jar-file goes only with BufferedInputStream - this is done after this first part
        //    -> source: http://stackoverflow.com/questions/8258244/accessing-a-file-inside-a-jar-file
        // ??? Is this really necessary ???
        if (null != this.soundFile)
        {
            try
            {
            	// create a file-Object within the resources
                File file = new File("/resources/" + this.soundFile);
                
                // does the file exists and is it a file?
                if (file.exists() && file.isFile())
                {
                	// send file-Object to the audiostream to create it
                    audioInputStream = AudioSystem.getAudioInputStream(file);
                }
                else 
                {
	                // check for local file-Object if it doesn't exists in resources
	                File file_local = new File(this.soundFile);
	                
	                // does the file exists and is it a file?
	                if (file_local.exists() && file_local.isFile())
	                {
	                	// send file-Object to the audiostream to create it
	                    audioInputStream = AudioSystem.getAudioInputStream(file_local);
	                }
                }
                
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }

            // if audiostream could not initialized (e.g. when running in a jar-file), create it here with help of BufferedInputStream
	        if (null == audioInputStream)
	        {
	        	try
	            {
	        		// get inputstream from file
	        		InputStream is = getClass().getResourceAsStream(this.soundFile);
	        		// use inputstream to create the audioinputstream
	        		audioInputStream = AudioSystem.getAudioInputStream(new BufferedInputStream(is)); //$NON-NLS-1$
	            }
	            catch (Exception e)
	            {
	                Test.outputException(e);
	            }	            
	        }
       }
        
        // return the resulting audiostream
        return audioInputStream;
    }
	
}
