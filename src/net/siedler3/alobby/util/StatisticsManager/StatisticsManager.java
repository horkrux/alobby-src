/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util.StatisticsManager;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import javax.imageio.ImageIO;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.LoadingJFrame;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.httpRequests;

/**
 * GUI to manage the local saved statistic-screenshots.
 * "manager" means:
 * - delete them
 * - load them up and post the link again
 * 
 * The list of files will be sort by date, newest first.
 * After click on one file its preview will be visible right of the list.
 * 
 * @author Zwirni
 */

public class StatisticsManager extends JFrame {

	private SettlersLobby settlersLobby;
	private JPanel pnlPreview;
	private JButton btnDelete;
	private JButton btnUpload;
	private JLabel pnlPreviewLabel = new JLabel(I18n.getString("StatisticsManager.LABEL_HINT"));
	private String pictureurl = "";
	private JFrame object = null;
	private JList<Object> liDateiListe = new JList<Object>();
	private JButton btnDeleteAll;
	private JButton btnClose;
	
	public StatisticsManager( SettlersLobby settlersLobby ) {
		super();
		this.settlersLobby = settlersLobby;
		
		// secure the JFrame-object for loading window
		this.object = this;
		
		// set window-title
		this.setTitle(I18n.getString("StatisticsManager.TITLE"));
		
		// set the window-size
		this.setSize(980, 600);
		
		// set default aLobby-Icon
		this.settlersLobby.getGUI().setIcon(this);
		
		// set the default text-orientation in this window (possible language-depending)
		this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		// close this window really
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		// center this window
		this.setLocationRelativeTo(this.settlersLobby.getGUI().getMainWindow());
		
		// the window should not be resized by user
		this.setResizable(false);
		
		// the components of the window will be positioned as FlowLayout
		this.setLayout(new BorderLayout());
				
		reloadList();
		
		// create a scroll-pane as wrapper for the file-list
		JScrollPane scrollPane = new JScrollPane();
		
		// create a list-object to show each file
		ListSelectionModel liDateiListeModel = liDateiListe.getSelectionModel();
		liDateiListeModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		liDateiListe.addKeyListener(new KeyListener() {
			@Override
            public void keyPressed(KeyEvent event) {
				
            }
            @Override
            public void keyReleased(KeyEvent event) {
            	// get the info of the clicked file by mouseEvent
				JList<?> list = (JList<?>) event.getSource();
				String f = (String) list.getSelectedValue();
                if( event.getKeyCode() == KeyEvent.VK_DOWN || event.getKeyCode() == KeyEvent.VK_UP ) {
                	usePreviewPanel(f);
                }
            }

            @Override
            public void keyTyped(KeyEvent event) { }
		});
		liDateiListe.setCellRenderer(new DefaultListCellRenderer() {

			// show only the filename as it consists of the date of the screenshot
            @Override
            public Component getListCellRendererComponent(JList<?> list,
                    Object value, int index, boolean isSelected,
                    boolean cellHasFocus) {
                if (value instanceof String) {
                    JLabel label = new JLabel();
                    label.setBackground(settlersLobby.theme.lobbyBackgroundColor);
                    label.setBorder(new EmptyBorder(2,8,2,8));
                    label.setOpaque(false);
                    label.setText((String) value);
                    if( isSelected || cellHasFocus ) {
                    	label.setOpaque(true);
                    }
                    else {
                    	label.setOpaque(false);
                    }
                    return label;
                }
                return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }
        });
		
		// add the select-event
		// -> will open the selected picture to show the statistic
		liDateiListe.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent event) {
				// get the info of the clicked file by mouseEvent
				JList<?> list = (JList<?>) event.getSource();
				String f = (String) list.getSelectedValue();
                if( f instanceof String ) {
	            	usePreviewPanel(f);
                }
                else {
                	resetPreviewPanel();
                }
			}
			@Override
			public void mousePressed(MouseEvent event) {
			}

			@Override
			public void mouseReleased(MouseEvent event) {
			}

			@Override
			public void mouseEntered(MouseEvent event) {
			}

			@Override
			public void mouseExited(MouseEvent event) {
			}
        });
		
		// add the list to its wrapping viewport
		scrollPane.setViewportView(liDateiListe);
		
		// set the dimension for the list
		scrollPane.setPreferredSize(new Dimension(180, this.getHeight())); 
		
		// add the resulting wrapper to the window
		this.add(scrollPane, BorderLayout.LINE_START);
		
		// create a preview-panel for the image, which will be show right
		// from the list if user clicks on one file
		pnlPreview = new JPanel();
		pnlPreview.setVisible(true);
		pnlPreview.setLayout(new BorderLayout());
		pnlPreview.setPreferredSize(new Dimension(800, this.getHeight()));
		pnlPreviewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		pnlPreviewLabel.setForeground(this.settlersLobby.theme.lightTextColor);
		pnlPreview.add(pnlPreviewLabel);
		this.add(pnlPreview, BorderLayout.LINE_END);
		
		// create a button-panel for the bottom
		JPanel pnlButtons = new JPanel();
		pnlButtons.setLayout(new FlowLayout());
		
		// create the delete-button
		btnDelete = new JButton(I18n.getString("StatisticsManager.BUTTON_DELETE"));
		
		// create the upload-button
		btnUpload = new JButton(I18n.getString("StatisticsManager.BUTTON_UPLOAD_AND_POST"));
		
		// create the delete-all-button
		btnDeleteAll = new JButton(I18n.getString("StatisticsManager.BUTTON_DELETE_ALL"));
		
		// create the close-button, simple close the statistics-manager
		btnClose = new JButton(I18n.getString("StatisticsManager.BUTTON_CLOSE"));
		
		// add repeating backgroundImage to the buttons, if defined for actual theme
		if( settlersLobby.theme.isRepeatingButtonStyleAvailable() ) {
			btnDelete = settlersLobby.theme.getButtonWithStyle(btnDelete.getText());
			btnUpload = settlersLobby.theme.getButtonWithStyle(btnUpload.getText());
			btnDeleteAll = settlersLobby.theme.getButtonWithStyle(btnDeleteAll.getText());
			btnClose = settlersLobby.theme.getButtonWithStyle(btnClose.getText());
		}
		// add specific backgroundImage to the buttons, if defined for actual theme
		// -> all together for consistent view
		if( settlersLobby.theme.isButtonStyleAvailable() ) {
			btnDelete = settlersLobby.theme.getButtonWithStyle(btnDelete.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
			btnUpload = settlersLobby.theme.getButtonWithStyle(btnUpload.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
			btnDeleteAll = settlersLobby.theme.getButtonWithStyle(btnDeleteAll.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
			btnClose = settlersLobby.theme.getButtonWithStyle(btnClose.getText(), settlersLobby.theme.buttonBackgroundImage1, settlersLobby.theme.buttonBackgroundImage1Hover);
		}
		
		// add the actionListener to each button
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();				
			}
		});	
		
		btnUpload.setEnabled(false);
		
		btnUpload.addActionListener(uploadActionListener);
		
		btnDelete.setEnabled(false);
		btnDelete.addActionListener(deleteActionListener);
		
		btnDeleteAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
	         	messagewindow.addMessage(
	         		I18n.getString("StatisticsManager.QUESTION_DELETE_ALL"),
	         		UIManager.getIcon("OptionPane.informationIcon")
	         	);
	         	messagewindow.addButton(
	         		I18n.getString("StatisticsManager.QUESTION_DELETE_ALL_YES"),
	         		new ActionListener() {
	         		    @Override
	         		    public void actionPerformed(ActionEvent e) {
	         		    	// get the directory where the screenshots will be saved
	         				File ScreenFileDirectory = new File(SettlersLobby.configDir, "screens");
	         				
	         				// get files and delete each file
	         				File filelist[] = ScreenFileDirectory.listFiles();
	         				for (int i = 0; i < filelist.length; i++) {
	         					filelist[i].delete();
	         				}
	         				
	         				// reload filelist (which is now empty)
	         				reloadList();
	         				
	         				// reset the previewPanel (which now should show another hint for the user)
	         				resetPreviewPanel();
	         				
	         				// disable the delete-Button
	         				btnDeleteAll.setEnabled(false);
	         				
	         				// show an success-window
	         				MessageDialog messagewindow2 = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
	        	         	messagewindow2.addMessage(
	        	         			I18n.getString("StatisticsManager.HINT_DELETE_ALL"),
	        	         		UIManager.getIcon("OptionPane.informationIcon")
	        	         	);
	        	         	messagewindow2.addButton(
	        	         		"OK",
	        	         		new ActionListener() {
	        	         		    @Override
	        	         		    public void actionPerformed(ActionEvent e) {
	        	         		    	// close this window
	        	    			    	messagewindow2.dispose();
	        	         		    }
	        	         		}
	        	         	);	        	         	
	         				
	         		    	// close this window
	    			    	messagewindow.dispose();
	         		    }
	         		}
	         	);
	         	messagewindow.addButton(
	         		I18n.getString("GUI.NO"),
	         		new ActionListener() {
	         		    @Override
	         		    public void actionPerformed(ActionEvent e) {
	         		    	// close this window
	    			    	messagewindow.dispose();
	         		    }
	         		}
	         	);
	         	messagewindow.setVisible(true);
			}
		});
		if( liDateiListe.getModel().getSize() == 0 ) {
			btnDeleteAll.setEnabled(false);
		}
		
		// add the buttons to the panel
		pnlButtons.add(btnDelete);
		pnlButtons.add(btnUpload);
		pnlButtons.add(btnDeleteAll);
		pnlButtons.add(btnClose);
		
		// add the button-panel to the window 
		// to be visible at the bottom of it
		this.add(pnlButtons, BorderLayout.PAGE_END);
		
		// show this window
		this.setVisible(true);
		
		this.pack();
	}
	
	/**
	 * Insert each item from file-list into model of the JList-component.
	 * Only add files from content-type image/png as other elements 
	 * could not be displayed.
	 * 
	 * @param ScreenFileList	The list.
	 * @return
	 */
	private <E> DefaultListModel<Object> generateModel( List<E> ScreenFileList ) {
        DefaultListModel<Object> model = new DefaultListModel<Object>();
        for (ListIterator<E> iter = ScreenFileList.listIterator(); iter.hasNext(); ) {
            E element = iter.next();
            File f = new File(element.toString());
            String mimeType = "";
    		try {
    			mimeType = Files.probeContentType(f.toPath());
    		} catch (IOException e1) {
    			e1.printStackTrace();
    		}
    		if( mimeType != null && mimeType.length() > 0 && mimeType.equals("image/png") ) {
				model.addElement(element);
    		}
        }
        return model;
    }
	
	/**
	 * Reload the file-list.
	 */
	private void reloadList() {
		// get the directory where the screenshots will be saved
		// and create it if it doesn't exists
		File ScreenFileDirectory = new File(SettlersLobby.configDir, "screens");
		if (!ScreenFileDirectory.exists()) {
			ScreenFileDirectory.mkdirs();
		}
		
		// load the file-list
		List<String> ScreenFileList = Arrays.asList(ScreenFileDirectory.list());
		
		// sort the list in reversed alphabetical order
    	Collections.sort(ScreenFileList,Collections.reverseOrder());
    	
    	// add this list to the model
    	liDateiListe.setModel(generateModel(ScreenFileList));
    	
    	// set previewPanel-Test
    	setPreviewPanelText();
	}
	
	/**
	 * Use the previewPanel to show the selected file.
	 * Including check if the selected file are from content-type
	 * image/png as other file-types could not be displayed.
	 * 
	 * @param f	The file to show as File-Object
	 */
	private void usePreviewPanel( String filename ) {
		// clear the panel from unused fields
		clearPreviewPanel();
		
		File f = new File(SettlersLobby.configDir + "//screens//" + filename);
		
		String mimeType = "";
		try {
			mimeType = Files.probeContentType(f.toPath());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		if( mimeType.length() > 0 && mimeType.equals("image/png") ) {
		
			// read the image
	    	BufferedImage originalBufferedImage = null;
		    try {
				
				// read image from Byte-Array
				originalBufferedImage = ImageIO.read(f);
				
				// set the designated width for the image
				// -> do not set this to the width and height of the wrapping panel
				//    because this will be bigger than the image itself
				// -> height will be calculated depending on size of the original image
		        int thumbnailWidth = 720;
		        
		        // calculate the width of the resulting image on-the-fly
		        int widthToScale = (int)(1.1 * thumbnailWidth);
		        int heightToScale = (int)((widthToScale * 1.0) / originalBufferedImage.getWidth() 
		                            * originalBufferedImage.getHeight());
		        
		        // create the target image without contents
		        BufferedImage resizedImage = new BufferedImage(widthToScale, heightToScale, originalBufferedImage.getType());
	        	Graphics2D g = resizedImage.createGraphics();
	        	// -> set alpha-composite 
	        	g.setComposite(AlphaComposite.Src);
	        	// -> set rendering-conditions
	        	g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	        	g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
	        	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	        	// -> draw the image
	        	g.drawImage(originalBufferedImage, 0, 0, widthToScale, heightToScale, null);
	        	// -> forget the result :]
	        	g.dispose();
	        	
	        	// add the bufferedImage as image to the panel
	        	pnlPreview.add(new JLabel(new ImageIcon(resizedImage)));
		    }
		    catch (IOException e) {
		        throw new RuntimeException(e);
		    }
		    
		    // set the buttons so enable
		    btnDelete.setEnabled(true);
		    btnUpload.setEnabled(true);
		    
		    // Reinitialize the window to prevent fragments in GUI
		    pack();
		}
		else {
			resetPreviewPanel();
		}
	}
	
	/**
	 * Reset the previewPanel.
	 */
	private void resetPreviewPanel() {
		// clear the panel from unused fields
		clearPreviewPanel();
		
		// set previewPanel-Test
		setPreviewPanelText();
		
		// if no file is selected, than
    	// -> show the hint again
    	pnlPreview.add(pnlPreviewLabel);
    	
    	// -> set the buttons so disabled
	    btnDelete.setEnabled(false);
	    btnUpload.setEnabled(false);
	}
	
	/**
	 * Clear the preview-Panel from unused fields.
	 */
	private void clearPreviewPanel() {
		// remove all components in previewPanel
    	pnlPreview.removeAll();
    	
		// and reset its graphic
    	pnlPreview.repaint();
	}
	
	/**
	 * Set previewPanel-Text depending on screenshot-count.
	 */
	private void setPreviewPanelText() {
		if( liDateiListe.getModel().getSize() == 0 ) {
    		pnlPreviewLabel.setText("<html>" + I18n.getString("StatisticsManager.NO_FILES_AVAILABLE") + "</html>");
    	}
	}
	
	/**
	 * ActionListener to delete the selected file.
	 */
	private final ActionListener deleteActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			// get the info of the clicked file by mouseEvent
	        String filename = (String) liDateiListe.getSelectedValue();
	        File f = new File(SettlersLobby.configDir + "//screens//" + filename);
	        
	        // delete the file and check the success
	        if( !f.delete() ) {
	        	// file could not be deleted => inform user
	        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
	         	messagewindow.addMessage(
	         		I18n.getString("StatisticsManager.ERROR_DELETE"),
	         		UIManager.getIcon("OptionPane.informationIcon")
	         	);
	         	messagewindow.addButton(
	         		"OK",
	         		new ActionListener() {
	         		    @Override
	         		    public void actionPerformed(ActionEvent e) {
	         		    	// close this window
	    			    	messagewindow.dispose();
	         		    }
	         		}
	         	);
	         	messagewindow.setVisible(true);
	        }
	        else {
	        	// reload the list
	        	reloadList();
	        	
	        	// reset the previewPanel
	        	resetPreviewPanel();
	        }
		}
	};
	
	/**
	 * ActionListener to perform an upload of a selected file
	 * and post the url, if user wants it, into chat.
	 */
	private final ActionListener uploadActionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			// get the info of the clicked file by mouseEvent
			String filename = (String) liDateiListe.getSelectedValue();
            File f = new File(SettlersLobby.configDir + "//screens//" + filename);
            
            if( f instanceof File ) {
            
                // get a separate window with waiting-message
    			LoadingJFrame waitingWindow = new LoadingJFrame(object);
    			
    			SwingWorker<?, ?> swingWorker = new SwingWorker<Object, Object>() {
                
    				@SuppressWarnings("static-access")
					@Override
    				protected Object doInBackground() throws Exception {
    				
    					// preparation for league-screenshots
    					// -> set to true if image should not be deleted through screen-Management
    					boolean preventDeletion = false;
    					
		                // Upload the file
		                httpRequests httpRequests = new httpRequests();
		        		httpRequests.setUrl(settlersLobby.config.getDefaultScreenUploader());
		        		httpRequests.addParameter("smalloutput", "1");
		        		if( preventDeletion ) {
		        			httpRequests.addParameter("preventDeletion", 1);
		        		}
		        		else {
		        			httpRequests.addParameter("preventDeletion", 0);
		        		}
		        		httpRequests.addFile("file", f.getAbsoluteFile());
		        		pictureurl = "";
						try {
							pictureurl = settlersLobby.config.getDefaultScreenUploaderDomain() + httpRequests.doRequest().trim();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
		        		if( httpRequests.isError() || pictureurl.length() == 0 ) {
		        			// file could not be deleted => inform user
		                	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
				         	messagewindow.addMessage(
				         		I18n.getString("StatisticsManager.ERROR_UPLOAD"),
				         		UIManager.getIcon("OptionPane.informationIcon")
				         	);
				         	messagewindow.addButton(
				         		"OK",
				         		new ActionListener() {
				         		    @Override
				         		    public void actionPerformed(ActionEvent e) {
				         		    	// close this window
				    			    	messagewindow.dispose();
				         		    }
				         		}
				         	);
				         	messagewindow.setVisible(true);
		        		}
		        		else {
		        			// file could not be deleted => inform user
		                	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
				         	messagewindow.addMessage(
				         		"<html>" + MessageFormat.format(I18n.getString("StatisticsManager.HINT_UPLOAD"), pictureurl, settlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor())) + "</html>",
				         		UIManager.getIcon("OptionPane.informationIcon")
				         	);
				         	messagewindow.addButton(
				         		"OK",
				         		new ActionListener() {
				         		    @Override
				         		    public void actionPerformed(ActionEvent e) {
				         		    	// close this window
				    			    	messagewindow.dispose();
				         		    }
				         		}
				         	);
				         	messagewindow.addButton(
				         			I18n.getString("StatisticsManager.BUTTON_AFTER_UPLOAD"),
					         		new ActionListener() {
					         		    @Override
					         		    public void actionPerformed(ActionEvent e) {
					         		    	// close this window
					         		    	settlersLobby.actionChatMessage(pictureurl);
					    			    	messagewindow.dispose();
					         		    }
					         		}
					         	);
				         	messagewindow.setVisible(true);
		        		}
		        		
		        		return null;			        		
    				}
    				
    				/**
    		         * Executed in event dispatching thread
    		         */
    		        @Override
    		        public void done() {
    		        	// close the waiting window
    		        	waitingWindow.dispatchEvent(new WindowEvent(waitingWindow, WindowEvent.WINDOW_CLOSING));
    		        }
    			};	 
    			
    			swingWorker.execute();
    		}
		}
	};
}