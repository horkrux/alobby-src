package net.siedler3.alobby.util.s3sm;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Arrays;
import java.util.Date;
import java.util.Comparator;

import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.util.FileLockWrapper;

public class SaveManagerWatchdog extends Thread
{
   private Path Originalverzeichnis;
   private Path Backupverzeichnis;
   private WatchService Überwachung;
   private WatchKey Überwachungsschlüssel;
   private Integer Sicherungsanzahl;
   private String timeUrl;
   public boolean Ausführen;
   private long timeDifference = 0;
   private FileLockWrapper fileLock;
   //need access to the gui to change the button color
   private GUI gui;
   //specify this as field to modify it for the Junit test
   int coolDownTime = 5000;
   //specify this as field to modify it for the Junit test
   int sleepTime = 500;
   
   @SuppressWarnings("unchecked")                                             //Unterdrückung der Warnung bei Cast WatchEvent<?> zu WatchEvent<Path> durch annotierte Methode
   static <T> WatchEvent<T> cast(WatchEvent<?> Ereignis) {
      return (WatchEvent<T>)Ereignis;
   }  
   
   public SaveManagerWatchdog(String OPfad, String ZPfad, Integer NSicherungsanzahl, String timeUrl)
   {
	  this.timeUrl = timeUrl;
      Sicherungsanzahl=NSicherungsanzahl;
      Ausführen=true;
      setName("WatchDogThread");
      try
      {
         Überwachung = FileSystems.getDefault().newWatchService();
         Originalverzeichnis = FileSystems.getDefault().getPath(OPfad);
         Originalverzeichnis.register(Überwachung, ENTRY_CREATE, ENTRY_MODIFY);
         Backupverzeichnis = FileSystems.getDefault().getPath(ZPfad);
         
         File file = new File(Backupverzeichnis.toFile(), "S3Sm.lock");
         fileLock = new FileLockWrapper(file);
      }
      catch (IOException e)
      {
    	 Test.outputException(e);
      }
   }   
    @Override
    public void run()
    {
        createBackupDirectory();
        
        try
        {
            //first try to get the fileLock. If this fails, abort
            //the thread and show a red icon
            //another watchdog currently running
            if (!aquireLock())
            {
                showRed();
                return;
            }
            
            showGreen();
            runWatchdog();
            showRed();
        }
        finally
        {
            releaseLock();
        }
    }

    private void createBackupDirectory()
    {
        try
        {
            if (!Files.isDirectory(Backupverzeichnis))
            {
                Files.createDirectory(Backupverzeichnis);
            }
        }
        catch (IOException e)
        {
            Test.output("Failed to create directory.");
            Test.outputException(e);
        }
    }
    
    private boolean aquireLock() 
    {
        try
        {
            return fileLock.aquireLock();
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
        return false;
    }
    
    private void releaseLock()
    {
        if (fileLock != null)
        {
            fileLock.releaseLock();
        }
    }

    
    private void showGreen()
    {
        //Test.output("green");
        if (gui != null)
        {
            gui.setS3SmActiveState(true);
        }
    }

    private void showRed()
    {
        //Test.output("red");
        if (gui != null)
        {
            gui.setS3SmActiveState(false);
        }
    }
    
    public void setGui(GUI gui)
    {
        this.gui = gui;
    }

   public void runWatchdog()
   {
      long Speicherzeitpunkt = System.currentTimeMillis(), aktuelleZeit, Sperrzeitpunkt = System.currentTimeMillis();

      while (Ausführen && !isInterrupted())
      {
         try
         {
            Überwachungsschlüssel = Überwachung.take();              // Reagiert auf Unterbrechungen (interrupt()-Aufrufe)
         }
         catch (InterruptedException e)                   
         {
             //abort the loop
             Ausführen = false;
             interrupt();
             break;
         }
         for (WatchEvent<?> Ereignis: Überwachungsschlüssel.pollEvents())
         {
            WatchEvent.Kind<?> Art = Ereignis.kind();
            if (Art == OVERFLOW)
               continue;
            WatchEvent<Path> Pfadereignis = cast(Ereignis);
            final Path Dateiname = Pfadereignis.context();
            if (Dateiname.toString().contains("Lock"))
               Sperrzeitpunkt=System.currentTimeMillis();
            aktuelleZeit=System.currentTimeMillis();
            if (aktuelleZeit-Speicherzeitpunkt > coolDownTime && aktuelleZeit-Sperrzeitpunkt>2000 && Files.isRegularFile(Originalverzeichnis.resolve(Dateiname)) && !Dateiname.toString().contains("temp"))    //Nach jedem gemeldeten und verarbeitetetem Ereignis 5 Sekunden ruhen, um systembedingte Mehrfachereignisse zu ignorieren. Das kleinste S3-Speicherintervall beträgt 1:01 Minuten
            {                           
               File[] zugehörigeDateien = (new File(Backupverzeichnis.toString())).listFiles(new FilenameFilter()
               {
                  @Override
                  public boolean accept(File Datei, String DName)
                  {
                     return DName.contains(Dateiname.toString());
                  }
               });
               SaveManagerAlgo.Quicksort(zugehörigeDateien, 0, zugehörigeDateien.length-1);
               try
               {
                  Path newSavePath;
                  if (zugehörigeDateien.length>=Sicherungsanzahl)
                     newSavePath = Backupverzeichnis.resolve(zugehörigeDateien[zugehörigeDateien.length-1].toPath());  
                  else
                  {
                     Arrays.sort(zugehörigeDateien, new Comparator<File>()
                     {
                        public int compare(File eineDatei, File andereDatei)
                        {
                           return liefereDateinummer(eineDatei)-liefereDateinummer(andereDatei);
                        }
                        private int liefereDateinummer(File Datei)
                        {
                           return Integer.parseInt(Datei.getPath().substring(Datei.getPath().lastIndexOf(".")+1));
                        }
                     });
                     int i = 0;
                     for (i=0; i<zugehörigeDateien.length && (i+1)==Integer.valueOf(zugehörigeDateien[i].getPath().substring(zugehörigeDateien[i].getPath().lastIndexOf(".")+1)); ++i);                     
                     newSavePath = Backupverzeichnis.resolve(Dateiname.toString() + "." + String.valueOf(i+1));
                  }
                  Files.copy(Originalverzeichnis.resolve(Dateiname), newSavePath, StandardCopyOption.REPLACE_EXISTING);                    
                  File newSaveFile = newSavePath.toFile();
                  long timeDifference = getTimeDifference();
                  newSaveFile.setLastModified(newSaveFile.lastModified() - timeDifference);
               }
               catch(IOException e)
               {
                  Test.outputException(e);
               }
               catch(Exception e)
               {
                  Test.outputException(e);
               }
               Speicherzeitpunkt = System.currentTimeMillis();
            }
         }
         Überwachungsschlüssel.reset();
      }
                
      try
      {
          Überwachung.close();
      }
      catch (Exception e2)
      {
          Test.outputException(e2);
      }
      //Test.output("Thread finished");
   }
   
   public long getTimeDifference()
   {
       try
       {
	       long localTimestamp = System.currentTimeMillis() / 1000;
	       String remoteValue = getByHttp(timeUrl).trim();
	       if (remoteValue.equals("")) {
	    	   Test.output("Computing time delta resulted in an empty String...");
	           // At least return the last known time delta.
	           return this.timeDifference;
	       }
	       long remoteTimestamp = Long.valueOf(remoteValue);
	       timeDifference = (localTimestamp - remoteTimestamp) * 1000;
	       Test.output("Computed time delta: " + String.valueOf(timeDifference) + "ms"); 
	       return timeDifference;
       }
       catch(Exception e)
       {
    	  Test.output("Computing time delta failed..."); 
          Test.outputException(e);
          // At least return the last known time delta.
          return this.timeDifference;
       }
   }
   
   public String getByHttp(String urlToRead) {
	      URL url;
	      URLConnection urlConn = null;
	      HttpURLConnection conn = null;
	      InputStream is = null;
	      InputStreamReader isr = null;
	      BufferedReader rd = null;
	      String line;
	      String result = "";
	      try {
	         url = new URL(urlToRead);
	         urlConn = url.openConnection();
	         conn = (HttpURLConnection) urlConn;
	         conn.setRequestMethod("GET");
	         conn.setConnectTimeout(7000);
	         conn.setReadTimeout(7000);
	         is = conn.getInputStream();
	         isr = new InputStreamReader(is);
	         rd = new BufferedReader(isr);
	         while ((line = rd.readLine()) != null) {
	            result += line;
	         }
	      } catch (IOException e) {
	    	  Test.outputException(e);
	      } catch (Exception e) {
	    	  Test.outputException(e);
	      }
	      if (rd != null) {
	    	  try {
				rd.close();
			} catch (IOException e) {
				Test.outputException(e);
			}
	      }
	      if (isr != null) {
	    	  try {
				isr.close();
			} catch (IOException e) {
				Test.outputException(e);
			}
	      }
	      if (is != null) {
	    	  try {
				is.close();
			} catch (IOException e) {
				Test.outputException(e);
			}
	      }
	      if (conn != null) {
	    	conn.disconnect();
	      }
	      return result;
	   }

}
