package net.siedler3.alobby.util.s3sm;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.ActionListener;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import java.awt.Insets;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Arrays;
import java.util.Comparator;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.ImageIcon;

import javax.imageio.ImageIO;

import net.siedler3.alobby.controlcenter.SaveGame;
import net.siedler3.alobby.controlcenter.SaveGame.SavegameFilenameFilter;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.GoodsInStock;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.s3gameinterface.GameStarter.StartOptions;
import net.siedler3.alobby.i18n.I18n;

import java.util.regex.PatternSyntaxException;

public class SaveManager extends MouseAdapter implements ActionListener
{
   private JDialog Konfigurationsfenster;
   private JPanel Verwaltungspanel;
   private JTable Tabellen[];
   private JScrollPane Tabellenpanel[];
   private JButton Auswahlknopf, AktualisierungsK, startS3CDorGOGButton, startS3ClassicButton, startS3CeButton, Sicherungsknopf, Löschknopf;
   private JTextField Filterfeld;
   private SaveManagerWatchdog meinThread;
   private String OriginalVZName, ZielVZName, zugehörigeZeitstempel[];
   private int selektierteZeile, backupCount, backupCleanupInterval, IndexdesneuenBackup, Scrollposition;
   private int saveCount, saveCleanupInterval;
   private Boolean doBackupCleanup;
   private Boolean doSaveCleanup;
   private Timer saveCleanupTimer;
   private Timer saveBackupCleanupTimer;
   private SettlersLobby settlersLobby;
   private boolean showViewFirstTime = true, Backupverfügbar;
   private long zulässigeAbweichung;

   public SaveManager(SettlersLobby settlersLobby)
   {
     this.settlersLobby = settlersLobby;
      saveBackupCleanupTimer = new Timer();
      selektierteZeile=-1;
      Scrollposition=0;
      zulässigeAbweichung=10000;
      IndexdesneuenBackup=0;
      doBackupCleanup=false;
      doSaveCleanup=false;
      Backupverfügbar=true;
      Konfigurationsfenster = new JDialog();
      Konfigurationsfenster.setTitle("S3 Save Manager");
      Konfigurationsfenster.setIconImage(settlersLobby.getGUI().getIcon());      
      Konfigurationsfenster.setModal(true);
      Konfigurationsfenster.setSize(900,600);
      Konfigurationsfenster.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      Verwaltungspanel = new JPanel();
      Verwaltungspanel.setName("Saves");
      Verwaltungspanel.setBackground(settlersLobby.theme.lobbyBackgroundColor);
      Tabellen = new JTable[2];
      Tabellenpanel = new JScrollPane[2];
      Auswahlknopf= new JButton("Choose directory");
      Auswahlknopf.addActionListener(this);
      AktualisierungsK = new JButton(I18n.getString("Savemanager.REFRESH"));
      AktualisierungsK.addActionListener(this);
      String cdOrGogText;
      if (settlersLobby.isS3Gog) {
          cdOrGogText = I18n.getString("Savemanager.START_SAVE_S3_GOG");
      } else {
          cdOrGogText = I18n.getString("Savemanager.START_SAVE_S3_CD");
      }
      startS3CDorGOGButton = new JButton(cdOrGogText);
      startS3CDorGOGButton.addActionListener(this);
      startS3ClassicButton = new JButton(I18n.getString("Savemanager.START_SAVE_S3_CLASSIC"));
      startS3ClassicButton.addActionListener(this);
      startS3CeButton = new JButton(I18n.getString("Savemanager.START_SAVE_S3_CE"));
      startS3CeButton.addActionListener(this);
      Sicherungsknopf = new JButton(I18n.getString("Savemanager.CREATE_BACKUP"));
      Sicherungsknopf.addActionListener(this);
      try
      {
         Löschknopf = new JButton(new ImageIcon(ImageIO.read(getClass().getResourceAsStream(ALobbyConstants.PACKAGE_PATH + "/gui/img/delete.png"))));
         Löschknopf.addActionListener(this);
         Löschknopf.setMargin(new Insets(0,0,0,0));
      }
      catch (IOException Ausnahme)
      {
         Test.outputException(Ausnahme);
      }
      Filterfeld = new JTextField(20);
      Filterfeld.addActionListener(this);
      if (meinThread!=null)
         ZeigeDateien();
      
      Konfigurationsfenster.add(Verwaltungspanel);

      if (settlersLobby.config.getGamePathS3() != null)
      {
    	 File s3exe = new File(settlersLobby.config.getGamePathS3());
         File s3Directory = s3exe.getParentFile();
         String s3Path = s3Directory.getAbsolutePath();
         OriginalVZName = s3Path + "\\Save\\Multi";
         ZielVZName = s3Path + "\\Save\\MultiBackups";
         File saveDir = new File(OriginalVZName);
         if (!saveDir.exists())
         {
            saveDir.mkdirs();
         }
         File backupDir = new File(ZielVZName);
         if (!backupDir.exists())
         {
            backupDir.mkdirs();
         }
         if(!laden())
         {
            backupCount = 10;
            backupCleanupInterval = 48;
            saveCleanupInterval = 4;
            configChanged();
         }
         else
         {
            meinThread = createWatchDogThread();
            meinThread.start();
            if (doBackupCleanup && backupCleanupInterval > 0)
            {
               setSaveBackupCleanupTimer();
            }
            if (doSaveCleanup && saveCleanupInterval > 0)
            {
               setSaveCleanupTimer();
            }
            ZeigeDateien();
         }
      }
   }

   public void showConfigWindow()
   {
      if (meinThread!=null && OriginalVZName!=null && ZielVZName!=null)
          ZeigeDateien();
      if (showViewFirstTime)
      {
          //if the window is shown the first time, move it relative to the chat window
          //from there on the position remains unchanged, it stays were the user moves it
          showViewFirstTime = false;
          Konfigurationsfenster.setLocationRelativeTo(settlersLobby.getGUI().getMainWindow());
      }
     Konfigurationsfenster.setVisible(true);
   }

   private void ZeigeDateien()
   {
      IndexdesneuenBackup=0;
      Verwaltungspanel.removeAll();
      Verwaltungspanel.setLayout(new BoxLayout(Verwaltungspanel, BoxLayout.Y_AXIS));
      File Original, Originalverzeichnis= new File(OriginalVZName), Backupverzeichnis = new File(ZielVZName);
      File OriginalDateien[] = Originalverzeichnis.listFiles(), zugehörigeDateien[];
      String Kartennamenfeld[], Spielerliste, Datumsformat =  settlersLobby.config.getGameDateTimeFormat(), DateilisteausStrings[][] = new String[OriginalDateien.length][], Dateiattribute1[] =  new String[]{I18n.getString("Savemanager.TIMELINE"), I18n.getString("Savemanager.MAP"), I18n.getString("Savemanager.PLAYERS_COUNT"),  I18n.getString("Savemanager.PLAYERS")}, Dateiattribute2[] =  new String[]{I18n.getString("Savemanager.DATE_LOCAL"), I18n.getString("Savemanager.DATE_GMT"), I18n.getString("Savemanager.NAME")};
      SaveManagerAlgo.Quicksort(OriginalDateien, 0, OriginalDateien.length-1);
      SaveGame[] Spielstände = new SaveGame[OriginalDateien.length], zugehörigeSpielstände;
      int j=0;
      for (int i=0; i<OriginalDateien.length; ++i, ++j)
      {
         Spielstände[j]=new SaveGame(OriginalDateien[i], Datumsformat);
         DateilisteausStrings[j] = new String[5];
         DateilisteausStrings[j][0]=Spielstände[j].getTimeStampString();
         Kartennamenfeld=Spielstände[j].getMapName().split("\\\\");
         DateilisteausStrings[j][1]=Kartennamenfeld[Kartennamenfeld.length-1];
         Spielerliste = Spielstände[j].getPlayerNames().toString();
         DateilisteausStrings[j][2]=String.valueOf(Spielstände[j].getPlayerNames().size());
         DateilisteausStrings[j][3]=Spielerliste.substring(1, Spielerliste.length()-1);
         DateilisteausStrings[j][4]=OriginalDateien[i].getName();
         if (!Filterfeld.getText().isEmpty())
         {
            String meinFilter = ".*" + Filterfeld.getText() +".*";
            try
            {
               if (!DateilisteausStrings[j][1].toLowerCase().matches(meinFilter.toLowerCase()) && !DateilisteausStrings[j][3].toLowerCase().matches(meinFilter.toLowerCase()))
                  --j;
            }
            catch (PatternSyntaxException Ausnahme)
            {
               settlersLobby.getGUI().displayError(I18n.getString("Savemanager.REGEX_ERROR"));
               Filterfeld.setText("");
            }
         }
      }
      DateilisteausStrings = Arrays.copyOf(DateilisteausStrings, j);
      Tabellen[0] = new JTable(DateilisteausStrings, Dateiattribute1);
      Tabellen[0].addMouseListener(this);
      for (int i=0; i<3; ++i)
         Tabellen[0].setDefaultEditor(Tabellen[0].getColumnClass(i), null);
      Tabellen[0].getColumnModel().getColumn(0).setPreferredWidth(45);
      Tabellen[0].getColumnModel().getColumn(1).setPreferredWidth(55);
      Tabellen[0].getColumnModel().getColumn(2).setMaxWidth(35);                 //setPreferredWidth does not work as intended. Repeated calls of setMaxWidth eventually do.
      Tabellen[0].getColumnModel().getColumn(2).setMaxWidth(100);             
      Tabellen[0].getColumnModel().getColumn(3).setMinWidth(200);
      if (selektierteZeile!=-1 && selektierteZeile < Tabellen[0].getRowCount())
         Tabellen[0].setRowSelectionInterval(selektierteZeile, selektierteZeile);
      else
        selektierteZeile = -1;
      JPanel Knopfpanel = new JPanel();
      Knopfpanel.setBackground(settlersLobby.theme.lobbyBackgroundColor);
      Tabellenpanel[0]=new JScrollPane(Tabellen[0]);
      Tabellenpanel[0].getViewport().setBackground(settlersLobby.theme.lobbyBackgroundColor);
      if (selektierteZeile!=-1)
         Tabellenpanel[0].getViewport().setViewPosition(new java.awt.Point(0,Scrollposition));
      ListSelectionModel Modell = Tabellen[0].getSelectionModel();
      Modell.addListSelectionListener(new ListSelectionListener(){
         @Override
        public void valueChanged(ListSelectionEvent Ereignis)
         {
            selektierteZeile=Tabellen[0].getSelectedRow();
            Scrollposition=Tabellenpanel[0].getVerticalScrollBar().getValue();
            if (!Ereignis.getValueIsAdjusting())
               ZeigeDateien();
         }
      });
      Tabellen[0].setSelectionModel(Modell);
      JPanel Überschriftpanel1 = new JPanel(new GridBagLayout());
      JLabel Anzahlsanzeige = new JLabel(String.valueOf(j));
      GridBagConstraints Konfiguration = new GridBagConstraints();
      Konfiguration.weightx = 0.5;
      Konfiguration.gridx = 0;
      Konfiguration.gridy = 0;
      Konfiguration.gridwidth = 3;
      Anzahlsanzeige.setForeground(settlersLobby.theme.lightTextColor);
      Überschriftpanel1.add(new JLabel(I18n.getString("Savemanager.HEADER_TAB1")), Konfiguration);
      Konfiguration.gridx = 3;
      Überschriftpanel1.add(Anzahlsanzeige, Konfiguration);
      Konfiguration.gridx = 6;
      Konfiguration.anchor=GridBagConstraints.LINE_END;
      Überschriftpanel1.add(new JLabel(I18n.getString("Savemanager.FILTER")), Konfiguration);
      Konfiguration.gridx = 9;
      Überschriftpanel1.add(Filterfeld, Konfiguration);
      Konfiguration.anchor=GridBagConstraints.LINE_START;
      Konfiguration.gridx = 12;
      Konfiguration.gridwidth = 1;
      Überschriftpanel1.add(Löschknopf, Konfiguration);
      Überschriftpanel1.setBackground(settlersLobby.theme.lobbyBackgroundColor);
      Überschriftpanel1.setMaximumSize(new Dimension(700,20));
      Verwaltungspanel.add(Überschriftpanel1);
      Verwaltungspanel.add(Tabellenpanel[0]);
      Knopfpanel.add(AktualisierungsK);
      Verwaltungspanel.add(Knopfpanel);
      if (selektierteZeile!=-1)
      {
         int selektierteZeile2 = -1;
         zugehörigeDateien = Backupverzeichnis.listFiles(new FilenameFilter()
         {
            @Override
            public boolean accept(File Datei, String Dateiname)
            {
               return Dateiname.contains((String) Tabellen[0].getModel().getValueAt(selektierteZeile,4));
            }
         });
         // use modern way to get GMT-Format-String: https://coderanch.com/t/368112/java/Alternative-deprecated-Date-toGMTString
         SimpleDateFormat sdf = new SimpleDateFormat();
         sdf.setTimeZone(new SimpleTimeZone(0, "GMT"));
         sdf.applyPattern("dd MMM yyyy HH:mm:ss z");
         SaveManagerAlgo.Quicksort(zugehörigeDateien, 0, zugehörigeDateien.length-1);
         String zugehörigeDateienausStrings[][] = new String[zugehörigeDateien.length][];
         zugehörigeSpielstände= new SaveGame[zugehörigeDateien.length];
         for (int i=0; i<zugehörigeDateien.length; ++i)
         {
            zugehörigeSpielstände[i]=new SaveGame(zugehörigeDateien[i], Datumsformat);
            zugehörigeDateienausStrings[i] = new String[3];
            zugehörigeDateienausStrings[i][0] = zugehörigeSpielstände[i].getTimeStampString();
            Date lastmodified = new Date(zugehörigeDateien[i].lastModified());
            zugehörigeDateienausStrings[i][1] = sdf.format(lastmodified).toString();
            zugehörigeDateienausStrings[i][2] = zugehörigeDateien[i].getName();
         }
         Tabellen[1] = new JTable(zugehörigeDateienausStrings, Dateiattribute2);
         Tabellen[1].addMouseListener(this);
         for (int i=0; i<2; ++i)
            Tabellen[1].setDefaultEditor(Tabellen[1].getColumnClass(i), null);                           // Editoren entfernen
         Tabellen[1].getColumnModel().getColumn(2).setMinWidth(180);   
         if(zugehörigeSpielstände.length!=0)
         {
            int i=0;
            File ausgewählteDatei = new File(OriginalVZName + File.separator + Tabellen[0].getModel().getValueAt(selektierteZeile,4));
            long Zeitstempel=new SaveGame(ausgewählteDatei, Datumsformat).getRealTimeStamp();
            for (i=0; i<zugehörigeSpielstände.length && zugehörigeSpielstände[i].getRealTimeStamp()!=Zeitstempel; ++i) ;
            if (i<zugehörigeSpielstände.length)
               selektierteZeile2=i;
         }         
         JPanel Überschriftpanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
         JLabel erstesLabel = new JLabel(I18n.getString("Savemanager.HEADER_TAB2")), zweitesLabel = new JLabel((String) Tabellen[0].getModel().getValueAt(selektierteZeile,4));
         Überschriftpanel2.add(erstesLabel);
         zweitesLabel.setForeground(Color.red);
         if (selektierteZeile2==-1)
         {
            erstesLabel.setText(I18n.getString("Savemanager.NOT_BACKUPPED_SAVE"));
            if (zugehörigeDateien.length<backupCount)
            {
               Arrays.sort(zugehörigeDateien, new Comparator<File>()
               {
                  public int compare(File eineDatei, File andereDatei)
                  {
                     return liefereDateinummer(eineDatei)-liefereDateinummer(andereDatei);
                  }
                  private int liefereDateinummer(File Datei)
                  {
                     return Integer.parseInt(Datei.getPath().substring(Datei.getPath().lastIndexOf(".")+1));
                  }
               });
               int i=0;
               for (i=0; i<zugehörigeDateien.length && (i+1)==Integer.valueOf(zugehörigeDateien[i].getPath().substring(zugehörigeDateien[i].getPath().lastIndexOf(".")+1)); ++i) ;      //Suche einen passenden Dateiindex
               IndexdesneuenBackup=i+1;
            }
            Überschriftpanel2.add(Sicherungsknopf);
         }
         else
         {
            Tabellen[1].setRowSelectionInterval(selektierteZeile2, selektierteZeile2);
            Überschriftpanel2.add(zweitesLabel);            
         }
         if (zugehörigeDateien.length==0 || selektierteZeile2==-1)
            Backupverfügbar=false;
         else
            Backupverfügbar=true;
         Auswahlknopf.setEnabled(zugehörigeDateien.length!=0);
         Überschriftpanel2.setBackground(settlersLobby.theme.lobbyBackgroundColor);
         Verwaltungspanel.add(Überschriftpanel2);
         Tabellenpanel[1] = new JScrollPane(Tabellen[1]);
         Tabellenpanel[1].getViewport().setBackground(settlersLobby.theme.lobbyBackgroundColor);
         Verwaltungspanel.add(Tabellenpanel[1]);
         Auswahlknopf.setText(I18n.getString("Savemanager.USE_SAVE"));
         JPanel Zentrierungspanel = new JPanel();
         Zentrierungspanel.add(Auswahlknopf);
         Zentrierungspanel.add(startS3CDorGOGButton);
         Zentrierungspanel.add(startS3ClassicButton);
         Zentrierungspanel.add(startS3CeButton);
         Zentrierungspanel.setBackground(settlersLobby.theme.lobbyBackgroundColor);
         Verwaltungspanel.add(Zentrierungspanel);
      }
      //the panel was changed, although already being displayed.
      //call validate() and repaint() to properly update the view
      //see https://stackoverflow.com/questions/5769813/when-does-one-need-to-call-revalidate-on-a-swing-component-to-make-it-refresh
      Verwaltungspanel.validate();
      Verwaltungspanel.repaint();
   }
   
   public boolean copyBackupToSaveDir(final String saveBackupName)
   {
      stopWatchDogThread();
       try
       {

           Path backupPath = FileSystems.getDefault().getPath(ZielVZName).resolve(saveBackupName);
           Path savePath = FileSystems.getDefault().getPath(OriginalVZName).resolve(saveBackupName.substring(0, 40));
           Test.output("Automatic save placement before creating or joining game: " + backupPath.toString() + " -> " + savePath.toString());
           Files.copy(backupPath, savePath, StandardCopyOption.REPLACE_EXISTING);
       }
       catch (IOException e)
       {
           Test.outputException(e);
           return false;
       }
       finally
       {
           meinThread = createWatchDogThread();
           meinThread.start();
       }
      return true;
   }
   
   public File[] getAllBackups()
   {
      File Backupdir = new File(ZielVZName);
       File[] backupFiles = Backupdir.listFiles(new SavegameFilenameFilter());
       return backupFiles;
   }
   
   public File[] getAllNotBackupedGames()
   {
      File Gamedir = new File(OriginalVZName);
      File Backupdir = new File(ZielVZName);
       File[] gameFiles = Gamedir.listFiles(new SavegameFilenameFilter());
       
       if (gameFiles == null)
       {
         return gameFiles;
       }
       
       ArrayList<File> missingList = new ArrayList<File>();
       

       for (int i=0; i<gameFiles.length; i++)
       {
         final String gameName = gameFiles[i].getName();
           File[] connectedFiles = getAllBackupsOfGame(gameName);
           if (connectedFiles.length<1)
           {
            // No backup of this save exists, so this was not backuped at all
            missingList.add(gameFiles[i]);
           } else {
            // We try to solve here the issue, that a user has backups of the
            // game, but the latest was not backuped e.g. because of an
            // aLobby crash.
            if (!wasThisSaveBackuped(gameName, connectedFiles))
            {
               missingList.add(gameFiles[i]);
            }
           }
       }

       File[] missingGames = new File[missingList.size()];
       for (int i=0; i<missingList.size(); i++)
       {
         missingGames[i] = missingList.get(i);
       }
       return missingGames;
   }

   public File[] getAllBackupsOfGame(final String SaveFileName)
   {
      File Backupdir = new File(ZielVZName);
       File[] connectedFiles = Backupdir.listFiles(new FilenameFilter()
       {
          @Override
          public boolean accept(File file, String FileName)
          {
             return FileName.contains(SaveFileName);
          }
       });
       return connectedFiles;
   }

   public Boolean wasThisSaveBackuped(String SaveFileName)
   {
      File[] connectedFiles = getAllBackupsOfGame(SaveFileName);
      return wasThisSaveBackuped(SaveFileName, connectedFiles);
   }

   public Boolean wasThisSaveBackuped(String SaveFileName, File[] connectedFiles)
   {
      String saveDateTime = SaveGame.getTimeDisplayedInGameSelection(SaveFileName, settlersLobby.config.getGamePathS3(), settlersLobby.config.getGameDateTimeFormat());
      String backupDateTime;
      String backupFileName;
      for (int j=0; j<connectedFiles.length; j++)
      {
         backupFileName = connectedFiles[j].getName();
         backupDateTime = SaveGame.getTimeDisplayedInGameSelection(backupFileName, settlersLobby.config.getGamePathS3(), settlersLobby.config.getGameDateTimeFormat());
         if (saveDateTime.equals(backupDateTime))
         {
            return true;
         }
      }
      return false;
   }

   public Boolean findSave(final String SaveFileName, long timestamp, boolean placeTheSaveIfNeeded)
   {
      // How many seconds off can the save be to still be the wanted save     
      long tolerance = 25 * 1000; //ms
      Test.output("Tolerance: " + Long.toString(tolerance));
      Test.output("Save name: " + SaveFileName);
      Test.output("Save timestamp: " + Long.toString(timestamp));
       File[] connectedFiles = getAllBackupsOfGame(SaveFileName);
       SaveManagerAlgo.Quicksort(connectedFiles, 0, connectedFiles.length-1);
       String saveBackupName;
       long saveTimestamp;
       for (int i=0; i<connectedFiles.length; ++i)
       {
         saveBackupName = connectedFiles[i].getName();
         saveTimestamp = connectedFiles[i].lastModified();
         Test.output("Testing save name: " + saveBackupName);
         Test.output("Testing save timestamp: " + Long.toString(saveTimestamp));
         if ((saveTimestamp + tolerance) >= timestamp && (saveTimestamp - tolerance) <= timestamp)
         {
            if (placeTheSaveIfNeeded)
            {
               return copyBackupToSaveDir(saveBackupName);
            }
         }
       }
       
       // We didn't find a matching save in the backup dir.
       // As last resort, try to see, if there is a matching save in the save dir.
      File Savedir = new File(OriginalVZName);
       connectedFiles = Savedir.listFiles(new FilenameFilter()
       {
          @Override
          public boolean accept(File file, String FileName)
          {
             return FileName.contains(SaveFileName);
          }
       });
       for (int i=0; i<connectedFiles.length; ++i)
       {
         saveTimestamp = connectedFiles[i].lastModified();
         if ((saveTimestamp + tolerance) > timestamp && (saveTimestamp - tolerance) < timestamp)
         {
            return true;
         }
       }
       
       return false;
   }
   
   
   public void speichern()
   {
      try
      {
         settlersLobby.config.setSaveBackupCount(backupCount);
         settlersLobby.config.setSaveBackupCleanupInterval(backupCleanupInterval);
         settlersLobby.config.setDoSaveBackupCleanup(doBackupCleanup);
         settlersLobby.config.setSaveCleanupInterval(saveCleanupInterval);
         settlersLobby.config.setDoSaveCleanup(doSaveCleanup);
      }
      catch (Exception e)
      {
         Test.outputException(e);
      }
   }

   private Boolean laden()
   {
      try
      {
         backupCount = settlersLobby.config.getSaveBackupCount();
         backupCleanupInterval = settlersLobby.config.getSaveBackupCleanupInterval();
         doBackupCleanup = settlersLobby.config.isDoSaveBackupCleanup();
         saveCleanupInterval = settlersLobby.config.getSaveCleanupInterval();
         doSaveCleanup = settlersLobby.config.isDoSaveCleanup();
         return true;
      }
      catch (Exception e)
      {
         Test.outputException(e);
      }
      return false;
   }

   private void setSaveCleanupTimer()
   {
      saveCleanupTimer = new Timer();
      saveCleanupTimer.scheduleAtFixedRate(new TimerTask()
      {
         @Override
        public void run()
         {
            File saveFiles[] = new File(OriginalVZName).listFiles();
            SaveManagerAlgo.Quicksort(saveFiles);
            int i;
            long now = System.currentTimeMillis();
            long Intervall = 24L * 7L * saveCleanupInterval * 3600000L;
            Test.output("saveCleanupInterval: " + saveCleanupInterval + "; now: " + now + " ; interval: " + Intervall);
            for (i=0; i<saveFiles.length && now - saveFiles[i].lastModified()< Intervall; ++i) ;
            for (; i<saveFiles.length; ++i)
            {
               try
               {
                  Files.delete(saveFiles[i].toPath().toAbsolutePath());
               }
               catch (IOException e)
               {
                 Test.outputException(e);
               }
            }
         }
      }, 10000L, 3600000L);
   }

   private void setSaveBackupCleanupTimer()
   {
      saveBackupCleanupTimer = new Timer();
      saveBackupCleanupTimer.scheduleAtFixedRate(new TimerTask()
      {
         @Override
        public void run()
         {
            File backupFiles[] = new File(ZielVZName).listFiles();
            SaveManagerAlgo.Quicksort(backupFiles);
            int i;
            long now = System.currentTimeMillis();
            long Intervall = backupCleanupInterval * 3600000L;
            for (i=0; i<backupFiles.length && now - backupFiles[i].lastModified()< Intervall; ++i) ;
            for (; i<backupFiles.length; ++i)
            {
               try
               {
                  Files.delete(backupFiles[i].toPath().toAbsolutePath());
               }
               catch (IOException e)
               {
                 Test.outputException(e);
               }
            }
         }
      }, 10000L, 3600000L);
   }

   public void configChanged()
   {
       if (meinThread != null)
       {
          stopWatchDogThread();
       }
       backupCount = settlersLobby.config.getSaveBackupCount();
       backupCleanupInterval = settlersLobby.config.getSaveBackupCleanupInterval();
       doBackupCleanup = settlersLobby.config.isDoSaveBackupCleanup();
       saveBackupCleanupTimer.cancel();
       saveCleanupInterval = settlersLobby.config.getSaveCleanupInterval();
       doSaveCleanup = settlersLobby.config.isDoSaveCleanup();
       saveCleanupTimer.cancel();
       if (doBackupCleanup && backupCleanupInterval > 0)
           setSaveBackupCleanupTimer();
       if (doSaveCleanup && saveCleanupInterval > 0)
           setSaveCleanupTimer();
       speichern();
       meinThread = createWatchDogThread();
       if (meinThread == null) {
      	 Test.output("Starting SaveManager WatchDog failed! SaveManager will not run!");
      	 return;
       }
       meinThread.start();
       ZeigeDateien();
   }

   private int rückspielen(int option)
   {
      int Zeile = Tabellen[1].getSelectedRow();
      if (Zeile!=-1)
      {
         stopWatchDogThread();
         String SaveFileName = (String)Tabellen[0].getModel().getValueAt(selektierteZeile,4);
         String BackupFileName = (String)Tabellen[1].getModel().getValueAt(Zeile,2);
         Path Quelle = FileSystems.getDefault().getPath(ZielVZName).resolve(BackupFileName);
         Path Ziel = FileSystems.getDefault().getPath(OriginalVZName).resolve(SaveFileName);
         
         if (!wasThisSaveBackuped(SaveFileName))
         {
            JOptionPane Bestätigungsdialog = new JOptionPane(I18n.getString("Savemanager.BACKUP_NOT_FOUND_LABEL"), JOptionPane.WARNING_MESSAGE);
            if (option==0)
               Bestätigungsdialog.setOptions(new String[] {I18n.getString("Savemanager.MOD_OVERWRITE"), I18n.getString("Savemanager.MOD_NOT_OVERWRITE")});
            else
               Bestätigungsdialog.setOptions(new String[] {I18n.getString("Savemanager.MOD_OVERWRITE"), I18n.getString("Savemanager.MOD_NOT_OVERWRITE"), I18n.getString("Savemanager.MOD_CANCEL")});
            JDialog Bestätigungsfenster = Bestätigungsdialog.createDialog(Konfigurationsfenster, I18n.getString("Savemanager.BACKUP_NOT_FOUND_TITLE"));
            // replaced deprecated "show" with "setVisible"
            Bestätigungsfenster.setVisible(true);
            String Auswahl = (String) Bestätigungsdialog.getValue();            
            if (!Auswahl.equals(I18n.getString("Savemanager.MOD_OVERWRITE")))
            {
               meinThread = createWatchDogThread();
               meinThread.start();
               if (Auswahl.equals(I18n.getString("Savemanager.MOD_NOT_OVERWRITE")))
                  return 0;
               else
                  return 2;
            }
         }
         
         try
         {
            Test.output(Quelle.toString() + " -> " + Ziel.toString());
            kopieren(Quelle,Ziel, StandardCopyOption.REPLACE_EXISTING);
         }
         catch (IOException e)
         {
            Test.outputException(e);
         }

         ZeigeDateien();
         if (settlersLobby.config.isStartWatchdog())
         {
            meinThread = createWatchDogThread();
            meinThread.start();
         }
      }
      else
         settlersLobby.getGUI().displayError(I18n.getString("Savemanager.ERROR"));
      return 1;
   }

   @Override
   public void actionPerformed(ActionEvent Ereignis)
   {
      if (Ereignis.getSource()==Auswahlknopf)
         rückspielen(0);
      else if (Ereignis.getSource()==AktualisierungsK || Ereignis.getSource()==Filterfeld)
         ZeigeDateien();
      else if (Ereignis.getSource()==startS3CDorGOGButton || Ereignis.getSource()==startS3ClassicButton || Ereignis.getSource()==startS3CeButton)
      {
         int Zeile = Tabellen[1].getSelectedRow();
         if (settlersLobby.getGameStarter().isReady())
         {
            if (Zeile!=-1 || !Backupverfügbar)
            {
               String Auswahl;
               if (Zeile!=-1)
               {
                  Auswahl = (String)Tabellen[1].getModel().getValueAt(Zeile,2);
                  if (rückspielen(1)==2)
                     return;
               }
               else
                  Auswahl = (String)Tabellen[0].getModel().getValueAt(Tabellen[0].getSelectedRow(),4);
               String Nutzer = settlersLobby.config.getUserName();
               String Spielname = settlersLobby.config.getUserName()+"'s game";
               Test.output("Auswahl: " + Auswahl);
               String Spielstand = "Savegame" + File.separator + Auswahl;
               // get additional gamedata from separate saved file
               // -> load the savegame to identify the setting in this
               SaveGame saveGame = new SaveGame(new File(ZielVZName + File.separator + Auswahl), settlersLobby.config.getGameDateTimeFormat());
               // -> load opengame-object where the md5-compiler waits
               OpenGame openGame = new OpenGame(settlersLobby.getIp(), Nutzer, Spielstand, saveGame.getMapName(), false, 0, saveGame.getPlayer(), saveGame.getPlayer(), false, false, 0, "", 0, 0, 0, new ArrayList<>());
               // -> add the playerlist to openGame
               openGame.addPlayer(saveGame.getPlayerNamesAsString(), false);
               // -> get the additional date for this game within savegame-object with md5-hash
               saveGame.getAdditionalDataFromFile( settlersLobby, openGame );
               // start the game
               // FIXME GoodsInStock should match the real GoodsInStock for this game
               MiscType miscType;
               if (Ereignis.getSource() == startS3ClassicButton) {
                   settlersLobby.config.setGameCompability(true);
                   miscType = MiscType.ALOBBY;
               } else if (Ereignis.getSource() == startS3CeButton) {
                   settlersLobby.config.setGameCompability(true);
                   miscType = MiscType.S3CE;
               } else {
                   settlersLobby.config.setGameCompability(false);
                   if (settlersLobby.isS3Gog) {
                       miscType = MiscType.GOG;
                   } else {
                       miscType = MiscType.CD;
                   }
               }
               settlersLobby.actionWantsToCreateMapGame(new StartOptions(Nutzer, Spielname, Spielstand, GoodsInStock.Vorgabe, false, false, saveGame.isLeagueGame(), false, saveGame.getTournamentId(), saveGame.getTournamentName(), saveGame.getTournamentRound(), saveGame.getTournamentGroupnumber(), saveGame.getTournamentMatchnumber(), saveGame.getTournamentPlayerlist(), saveGame.getMapName(), 2, miscType));
               Konfigurationsfenster.dispose();
            }
            else
               settlersLobby.getGUI().displayError(I18n.getString("Savemanager.ERROR"));
         }
         else
            settlersLobby.getGUI().displayError(I18n.getString("SettlersLobby.ERROR_ALREADY_RUNNING"));
      }
      else if(Ereignis.getSource()==Sicherungsknopf)
      {
         try
         {
            String Dateiname=(String) Tabellen[0].getModel().getValueAt(selektierteZeile,4);
            Path Quelle = FileSystems.getDefault().getPath(OriginalVZName).resolve(Dateiname);
            Path Ziel;
            StandardCopyOption meineOption= StandardCopyOption.COPY_ATTRIBUTES;
            if (IndexdesneuenBackup==0)                                                            //Sicherungsanzahl>=zugehörigerDateien.length
            {
               Object Zieldateien[] = new Object[Tabellen[1].getModel().getRowCount()];
               for (int i=0; i<Tabellen[1].getModel().getRowCount(); ++i)
                  Zieldateien[i]=Tabellen[1].getModel().getValueAt(i,0);
               Object Zieldatei = JOptionPane.showInputDialog(Verwaltungspanel, I18n.getString("Savemanager.MOD_CHOOSE_FILE"), I18n.getString("Savemanager.MOD_TITLE"), JOptionPane.QUESTION_MESSAGE, null, Zieldateien, Zieldateien[0]);
               int j=0;
               for (j=0; Zieldatei!=Zieldateien[j]; ++j) ;
               String Zielstring = (String)Tabellen[1].getModel().getValueAt(j,2);
               IndexdesneuenBackup=Integer.valueOf(Zielstring.substring(Zielstring.length()-1));
               meineOption=StandardCopyOption.REPLACE_EXISTING;
            }
            Ziel = FileSystems.getDefault().getPath(ZielVZName).resolve(Dateiname+"."+String.valueOf(IndexdesneuenBackup));
            kopieren(Quelle, Ziel, meineOption);
         }
         catch (IOException e)
         {
            Test.outputException(e);
         }
         ZeigeDateien();
      }
      else if (Ereignis.getSource()==Löschknopf)
      {
         Filterfeld.setText("");
         ZeigeDateien();
      }
   }

   private void kopieren(Path Quelle, Path Ziel, StandardCopyOption Kopieroption) throws IOException
   {
      if (!meinThread.isAlive())
      {
         Path Dummy = FileSystems.getDefault().getPath(OriginalVZName).resolve("Lock");
         Files.createFile(Dummy);
         Files.delete(Dummy);
      }
      Files.copy(Quelle, Ziel, Kopieroption);
   }

    public void stopWatchDogThread()
    {
        if (meinThread == null)
        {
            return;
        }
        meinThread.interrupt();
        try
        {
            meinThread.join(); // Das Threadende abwarten, anderenfalls treten im Folgenden IO-Konflikte auf
        }
        catch(InterruptedException e)
        {
            Thread.currentThread().interrupt();
            Test.outputException(e);
        }
    }

    public void startWatchDogThread()
    {
      if (meinThread!=null)
         return;
      else
      {
         meinThread = createWatchDogThread();
         if (meinThread == null) {
        	 Test.output("Starting SaveManager WatchDog failed! SaveManager will not run!");
        	 return;
         }
         meinThread.start();
      }
    }

    private SaveManagerWatchdog createWatchDogThread()
    {
        if (settlersLobby.config.getGamePathS3() == null) {
            return null;
        }
        SaveManagerWatchdog thread;
        thread = new SaveManagerWatchdog(OriginalVZName, ZielVZName, backupCount, settlersLobby.config.getUrlTime());
        thread.setGui(settlersLobby.getGUI());
        return thread;
    }
}
