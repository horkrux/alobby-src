package net.siedler3.alobby.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;
import javax.swing.ProgressMonitor;
import javax.swing.ProgressMonitorInputStream;

import net.siedler3.alobby.controlcenter.Configuration;

import net.siedler3.alobby.controlcenter.Test;

/**
 * Build httpRequests to siedler3.net with different parameter and data
 * including pay attention on security-conditions on server-side
 * 
 * Usage:
 * - first call this class as normal, but without any parameters
 * - than call the parameter-functions to set the properties
 * - last step: call doRequest()
 * 
 * @author Zwirni
 * 
 */
public class httpRequests {
	
	private String url;
	private String phpSessionId;
	private String token;
	private HashMap<String, String> params = new HashMap<String, String>();
	private HashMap<String, File> binaryfiles = new HashMap<String, File>();
	private boolean ignorePhpSessId = false;
	private final Configuration config = Configuration.getInstance();
	private ArrayList<Object> error = new ArrayList<Object>();
	private boolean showProgressMonitor = false;
	
	public httpRequests() {}

	// set the url
	public void setUrl( String url ) {
		this.url = url;
	}
	
	// add a single parameter to the param-array-list
	public void addParameter( String paramname, int paramvalue ) {
		String value = "" + paramvalue;
		this.params.put(paramname, value);
	}
	
	public void addParameter(String paramname, long paramvalue) {
		String value = "" + paramvalue;
		this.params.put(paramname, value);
	}
	
	// add a single parameter to the param-array-list
	public void addParameter( String paramname, String paramvalue ) {
		this.params.put(paramname , paramvalue);
	}
	
	// add a single binary-file to the param-array-list
	public void addFile( String paramname, File file ) {
		this.binaryfiles.put(paramname , file);
	}
	
	public void setIgnorePhpSessId( boolean ignorePhpSessId ) {
		this.ignorePhpSessId = ignorePhpSessId;
	}
	
	/**
	 *  start the request
	 *  
	 * @return response as String
	 * @throws IOException 
	 */
	public String doRequest() throws IOException {
		
		// do nothing without url and params
		if( this.url.length() == 0 || this.params.isEmpty() ) {
			return "";
		}
		
		// 1. step: get PHP-SessionId and Token
		// -> without this values the server reject the connection in second step
		if( !ignorePhpSessId ) {
			this.getPhpSessIdAndToken();
		}
		
		// 2. step: send the request
		return this.httpsPostRequest();
		
	}

	// generate a http-Request
	// -> based on http://stackoverflow.com/questions/2469451/upload-files-with-java
	private String httpsPostRequest() throws IOException {

		// set the boundary for the parameters in this request
		String boundary = Long.toHexString(System.currentTimeMillis());
		
		// set the charset for this request
		String charset = "UTF-8";
		
		// set the linebreak
		String CRLF = "\r\n";
		
		// 1. step: get the URL
        URL myurl = new URL(this.url);
        
        // 2. step: open the connection and set its properties
        HttpURLConnection con;
        if( this.url.contains("https://") ) {
        	con = (HttpsURLConnection)myurl.openConnection();
        }
        else {
        	con = (HttpURLConnection)myurl.openConnection();
        }
        
		// -> enable the usage of output from the request
        con.setDoOutput(true);
        
        // -> send only POST-Requests 
        con.setRequestMethod("POST");
        
        // -> set the timeout to 20000ms
        con.setConnectTimeout(20000);
        
        // -> set HTTP-Header-Data
        con.setRequestProperty("Accept-Encoding", "deflate");
        con.setRequestProperty("Accept-Language", config.getLanguage().toString() + "-" + config.getLanguage().toString());
        con.setRequestProperty("Pragma", "no-cache");
        con.setRequestProperty("Cache-Control", "no-cache");
        
        // -> set UserAgent
        //    -> do not change this because it's important for communication with the mapBase
        con.setRequestProperty("User-Agent", "aLobby");
        
        // 3. step: if PHP-SessionId is available use it in HTTP-Header
        if( this.phpSessionId != null )
        {
        	con.setRequestProperty("Cookie", "PHPSESSID=" + this.phpSessionId);
        }
        
        // 4. step: if token is available use it in the parameter-list
        if( this.token != null )
        {
        	this.params.put("token", this.token);
        }
        
        // get the Content-Length for the Stream
        // -> create the body for the request
        String body = "";
        for(Entry<String, String> entry : this.params.entrySet()) {
        	String key = entry.getKey();
        	String value = entry.getValue();
        	// add normal parameter to the HTTP-Request
        	body = body + "--" + boundary;
        	body = body + CRLF;
        	body = body + "Content-Disposition: form-data; name=\"" + key + "\"";
        	body = body + CRLF;
        	body = body + "Content-Type: text/plain; charset=" + charset;
        	body = body + CRLF;
        	body = body + CRLF;
        	body = body + value;
        	body = body + CRLF;
        }
        // -> add the binary-data to the body
        if( !this.binaryfiles.isEmpty() ) {
        	for(Entry<String, File> entry : this.binaryfiles.entrySet()) {
        		String key = entry.getKey();
        		File binaryFile = entry.getValue();
        		Test.output(key + " -> " + binaryFile + " -> " + binaryFile.getPath());
        		
        		String mimetype = URLConnection.guessContentTypeFromName(binaryFile.getName());
        		if( mimetype == null ) {
        			mimetype = "text/plain";
        		}
        		
        		// copy the file to get lock for the copied file
        		File binaryFileToUse = new File(entry.getValue().getPath() + ".upload");
        		Files.copy(binaryFile.toPath(), binaryFileToUse.toPath(), StandardCopyOption.REPLACE_EXISTING);
        		
        		// add binary-file-data to the HTTP-Request
        		body = body + "--" + boundary;
        		body = body + CRLF;
        		body = body + "Content-Disposition: form-data; name=\"" + key + "\"; filename=\"" + binaryFile.getName() + "\"";
        		body = body + CRLF;
        		body = body + "Content-Type: " + mimetype;
        		body = body + CRLF;
        		body = body + "Content-Transfer-Encoding: binary";
        		body = body + CRLF;
        		body = body + CRLF;
        		body = body + extractFile(binaryFileToUse.getPath());
        		body = body + CRLF;
        		
        		// delete copied file
        		binaryFileToUse.delete();
        	}
        }
        // end of multipart/form-data.
        body = body + "--" + boundary + "--" + CRLF;
        
        // Do not set the body for the request at this point
        // -> the request will be collected bellow
        // Why? Binary-data cannot collected on this way as clean string, 
        // they would be detected as application/octet-stream on server, not as e.g. image/png.
        // Thats why we must use OutputStream bellow for the real request.
        
        // set the Content-Length
        con.setRequestProperty("Content-Length", String.valueOf(body.length()));
                
        // 5. step: set the parameter for the request
        con.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
        try (
    	    OutputStream output = con.getOutputStream();
    	    PrintWriter writer = new PrintWriter(new OutputStreamWriter(output, charset), true);
    	) {
        	
        	// send the normal text-parameters
        	if( !this.params.isEmpty() ) {
		        for(Entry<String, String> entry : this.params.entrySet()) {
		            String key = entry.getKey();
		            String value = entry.getValue();
		            // add normal parameter to the HTTP-Request
		            writer.append("--" + boundary).append(CRLF);
		            writer.append("Content-Disposition: form-data; name=\"" + key + "\"").append(CRLF);
		            writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
		            writer.append(CRLF).append(value).append(CRLF).flush(); // value and end of boundary
		        }
        	}
	        
        	// send the binary-data
        	if( !this.binaryfiles.isEmpty() ) {
		        for(Entry<String, File> entry : this.binaryfiles.entrySet()) {
		        	String key = entry.getKey();
		            File binaryFile = entry.getValue();
		            
		            // add binary-file-data to the HTTP-Request
		        	writer.append("--" + boundary).append(CRLF);
		            writer.append("Content-Disposition: form-data; name=\"" + key + "\"; filename=\"" + binaryFile.getName() + "\"").append(CRLF);
		            writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(binaryFile.getName())).append(CRLF);
		            writer.append("Content-Transfer-Encoding: binary").append(CRLF);
		            writer.append(CRLF).flush();
		            Files.copy(binaryFile.toPath(), output);
		            output.flush(); // important before continuing with writer!
		            writer.append(CRLF).flush(); // end of boundary
		        }
        	}
	        // end of multipart/form-data.
            writer.append("--" + boundary + "--").append(CRLF).flush();
	        writer.flush();
	        writer.close();
        }
        catch(IOException e) {
        	// add this error to the list which could be used
        	// from the calling function to analyze the problem
        	// and show appropriated messages to the user
        	this.error.add(e.getMessage());
        }
                
        // only if HTTP-Status in response is 200 analyze the response
        if( con.getResponseCode() == 200 )
        {
        	// 6. step: prepare to get the response
	        InputStream ins = con.getInputStream();
	        InputStreamReader isr = new InputStreamReader(ins, "UTF-8");
	        BufferedReader in = new BufferedReader(isr);
	        String inputLine;
	        StringBuffer responseBuffer = new StringBuffer();
	        
	        // if a progress-monitor is configured, use it
	        if( this.showProgressMonitor ) {
		        // Chain a ProgressMonitorInputStream to the 
		        // URLConnection's InputStream
		        ProgressMonitorInputStream pin = new ProgressMonitorInputStream(null, con.getURL().toString(), ins);
		         
		        // Set the maximum value of the ProgressMonitor
		        ProgressMonitor pm = pin.getProgressMonitor();
		        pm.setMillisToDecideToPopup(0);
		        pm.setMaximum(con.getContentLength());
		        	        
		        // 7. step: get the response line by line
		        while( (inputLine = in.readLine()) != null ) {
		        	responseBuffer.append(inputLine + "\n");
		        }
		        
		        pin.close();
	        }
	        else {
	        	// 7. step: get the response line by line
		        while( (inputLine = in.readLine()) != null ) {
		        	responseBuffer.append(inputLine + "\n");
		        }
	        }
	        
	        // 8. step: get the cookie to get the PHP-SessionId
	        // -> only if phpSessionId is not set at this moment
	        String cookie = con.getHeaderField("Set-Cookie");
	        if( cookie != null && this.phpSessionId == null )
	        {
	        	// extract the PHP-SessionId from the response
	        	this.phpSessionId = Pattern.compile(".*PHPSESSID=([^;]*);.*", Pattern.DOTALL).matcher(cookie).replaceAll("$1");
	        }
   
	        // 9. step: debug the HTTP-Response-Header
			/*Map<String, java.util.List<String>> headers = con.getHeaderFields();
	        for (Entry<String, List<String>> entry : headers.entrySet())
	        {
	        	String key = entry.getKey();
	            List<String> value = entry.getValue();
	        	Test.output("got " + key + " = " + value);
	        }*/
	        
	        // 10. step: close connection
	        in.close();
	        
	        // 11.step: return the response as String
	        return responseBuffer.toString();
        }
        else {
        	// HTTP-Status in response is not 200
        	// -> document this as error
        	Test.output("got another http-status than 200: " + con.getResponseCode());
        	this.error.add("HTTP-Status: " + con.getResponseCode());
        	
        	con.disconnect();
        	
        }
        
        return "";
    }
	
	/**
	 * Get PHP-SessionId and Token from Server 
	 */
	private void getPhpSessIdAndToken()
    {
		
		// do nothing without an url
		if( this.url.length() == 0 ) {
			return;
		}
		
		// set parameters for this request
		// -> first secure existing params
		HashMap<String, String> params_backup = this.params;
		HashMap<String, File> binaryfiles_backup = this.binaryfiles;
		// -> than set the only param for this request
    	this.params = new HashMap<String, String>();
    	this.binaryfiles = new HashMap<String, File>();
    	this.addParameter("onlytoken", "1");
    	
    	// use the response
    	String response = "";
        try {
        	// if response is empty try it again until we get an token
        	// but max 10 trys
        	int count = 0;
        	while( response.length() == 0 && count < 10 ) {
        		response = this.httpsPostRequest();
        		count++;
        	}
		} catch (FileNotFoundException e) {
			Test.output("We've got a FileNotFound exception with the login-site!");
			return;
		} catch (IOException e) {
			Test.outputException(e);
			return;
		}

        // get the token from the response if there is a string
        if( response.length() > 0 ) {
        	this.token = response.replace("\r\n", "").trim();
        }
        
        // reset the params for next request
        this.params = params_backup;
        this.binaryfiles = binaryfiles_backup;
        
    }
	
	/**
	 * Return info whether an error happened during the request.
	 * 
	 * @return boolean
	 */
	public boolean isError() {
		if( this.error.size() > 0 ) {
			Test.output("errorlist: " + this.error);
			return true;
		}
		return false;
	}
	
	/**
	 * Decide to show or hide a progress-monitor for this action
	 * 
	 * @param boolean
	 */
	public void setShowProgressMonitor( boolean showProgressMonitor ) {
		this.showProgressMonitor = showProgressMonitor;
		
	}
	
	/**
	 * Extract content of a file.
	 * 
	 * @param gameDataFile
	 * @return
	 * @throws FileNotFoundException
	 */
	private String extractFile(String filePath) throws FileNotFoundException {
		String content = "";
	    try
	    {
	        content = new String ( Files.readAllBytes( Paths.get(filePath) ) );
	    }
	    catch (IOException e)
	    {
	        e.printStackTrace();
	    }
	    return content;
	}
	
}