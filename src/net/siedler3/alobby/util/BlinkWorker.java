/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util;

import java.awt.Color;
import java.util.List;

import javax.swing.JButton;
import javax.swing.SwingWorker;

/**
 * Changes the foreground-color of a button 
 * until another event stops it.
 * 
 * The base-color of the object and the secondary color,
 * given as parameter on constructor,
 * will be used for animation.
 * 
 * @author Zwirni
 */

public class BlinkWorker extends SwingWorker<Void, Color> {

    private JButton btn;
    private Color color;
    private Color normal;

    /**
     * Create the object which will change the foreground-color 
     * of a button.
     * 
     * @param btn	The button to change
     * @param color	The secondary color
     */
    public BlinkWorker(JButton btn, Color color) {
        this.btn = btn;
        this.normal = btn.getForeground();
        this.color = color;
    }

    @Override
    protected Void doInBackground() throws Exception {
    	if( !isCancelled() && !isDone() ) {
	        for (int index = 10; index > 1; index--) {
	        	if( isCancelled() || isDone() ) {
	        		index = 0;
	        		break;
	        	}
        		publish(this.color);
        		Thread.sleep(index * 100);
        		publish(this.normal);
        		Thread.sleep(index * 100);
	        }
    	}
        return null;
    }

    @Override
    protected void process(List<Color> chunks) {
    	if( !isCancelled() && !isDone() ) {
    		Color color =  chunks.get(chunks.size() - 1);
    		btn.setForeground(color);
    	}
    }

}