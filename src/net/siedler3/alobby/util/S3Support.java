/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util;

import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.util.ArrayList;

import net.siedler3.alobby.controlcenter.ReqQuery;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;

/**
 * Check and generate the unified S3.exe-file which aLobby will use for each game.
 * 
 * Hint: Create for each new version a separate class-file, like "S3ExeGeneration17x.java"
 *       where the changes between S3-original and new unified-version must be filed.
 *       This file extends the class here and therefore has access to all variables and functions set here.
 * 
 * Steps to add a new version:
 * 1. Create a new S3ExeGenerationX.java-file (e.g. copy the latest) under a new name with versionnumber in it.
 * 2. Change the generation in the newly generated file.
 * 3. Add the file with a new switch-case in this file here (see makeS3ALobbyExe()).
 * 4. Add the new version in OptionsFrame.java in s3versionnumber-combobox incl. its translation in every supported language.
 * 5. Add in OptionsFrame.java saveSiedlerSettings() the new translation to get the selected version-number.
 * 5. If the new version should be published for stable release change the version in config.xml in field "S3versionnumber" (without point).
 * 
 * @author unknown
 */

public class S3Support
{
    // Valid CD exe files ("S3.EXE)
    public static final String s3160XPexeMd5 = "a48092cf5a63ccee5182488a344ece47";  // The 1.60 exe for WinXP and later

    // Valid GOG exe files ("S3_multi.EXE)
    public static final String s3160GOGexeMd5 = "b898b272fbb0184897ee344595d66deb";  // The standard S3_multi.EXE from GOG up to version 2.1.0.22
    public static final String s3160GOGexeNewMd5 = "f28ee3cfe75c44abf89a8afe91c56ce2";  // The standard S3_multi.EXE from GOG version 23266 (or maybe also newer versions)

    // BAD exe files ("S3.EXE)
    public static final String s3160preXPexeMd5 = "47133884F8867E9BD59545A3A78F7C23"; // The 1.60 exe for Win9x/NT-only
    public static final String s3160BadGOGexeMd5 = "df3e4d917f1b0a4956d0918423c496f0";  // GOG exe meant only for single player up to version 2.1.0.22
    public static final String s3160BadGOGexeNewMd5 = "6a025e4a21fbf26fb0d5892bec332932";  // GOG exe meant only for single player from version 23266 (or maybe also newer versions)
    public static final String s3160BadGOGMusicPatchMd5 = "a7387847beffa2730509f38d18b67f84";  // The 1.60 exe meant only for single player from the "Music Patch GOG" http://www.siedler-maps.de/forum/Technische-Hilfe/Neuer-Musik-Patch-22074.htm?sid=1824ba5056ffcf0201a2e43d688bb0ca
    public static final String s3160GOGMusicPatchMd5 = "5184e63ffc0f8b66dab2ce09dffaeb23";  // The 1.60 exe from the "Music Patch GOG" http://www.siedler-maps.de/forum/Technische-Hilfe/Neuer-Musik-Patch-22074.htm?sid=1824ba5056ffcf0201a2e43d688bb0ca
    public static final String s3160CDMusicPatchMd5 = "49f77915d74b096a2465c088f14f0bc4";  // The 1.60 exe from the "Music Patch CD" http://www.siedler-maps.de/forum/Technische-Hilfe/Neuer-Musik-Patch-22074.htm?sid=1824ba5056ffcf0201a2e43d688bb0ca

    // Our own generated exe files ("S3_alobby.EXE)
    public static final String s3170ALOBBYexeMd5 = "cadc9e2c729d8ee2535c5feffcc6a820";  // Our own s3.exe when created from the CD exe or GOG up to version 2.1.0.22
    public static final String s3170ALOBBYexeGog23266Md5 = "141f527d548dc9a706aad9a7dc1e2bad";  // Our own s3.exe when created from the GOG version 23266 exe (or maybe also newer versions)
    public static final String s3171ALOBBYexeMd5 = "B44F0120718222A24DE9BB9AB2A502EB";  // Our own s3.exe when created from the CD exe or GOG up to version 2.1.0.22
    public static final String s3171ALOBBYexeGog23266Md5 = "28573E7A76E14BCA8A28403208DDE4C6";  // Our own s3.exe when created from the GOG version 23266 exe (or maybe also newer versions)
    public static final String s3172ALOBBYexeMd5 = "2d4ea5a018f7231b7ab75fbb42149a80";  // Our own s3.exe when created from the CD exe or GOG up to version 2.1.0.22
    public static final String s3172ALOBBYexeGog23266Md5 = "CFCE9B39DE9ECFF4D4A24C65E7319CBE";  // Our own s3.exe when created from the GOG version 23266 exe (or maybe also newer versions)
    public static final String s3173ALOBBYexeMd5 = "FCEC6FB62E7F7DCBD28876E7B3654EC7";  // Our own s3.exe when created from the CD exe or GOG up to version 2.1.0.22
    public static final String s3173ALOBBYexeGog23266Md5 = "BC4999CAE0A940A4152D92CC987A8E63";  // Our own s3.exe when created from the GOG version 23266 exe (or maybe also newer versions)

    // Siedler3_25.f8007e01f.dat files
    public static final String Siedler3_25_f8007e01f_alobby = "4a85869a052ecb3df08fcf6c0d996ce7";  // With L3 egy bows patch
    public static final String Siedler3_25_f8007e01f_original = "3937ef0aff97922180e8bfe6bd0b24a4";  // Original from version 1.60

    /**
     * Constructor for running as Standalone without aLobby
     * 
     * @deprecated	really needed?
     * @param args
     */
    public static void main(String args[])
    {
        //System.out.println("Intro status: " + ReqQuery.getS3Intro());
        //System.out.println(is1366x768Supported());
        System.out.println(isGog());
        System.out.println(makeS3ALobbyExe(ReqQuery.getS3ExeFilePath(), null));
    }

    /**
     * Get all display modes that the system supports.
     *
     * @param minimumHeightFilter: Filter out resolutions not matching the minimum pixel height. Set to -1 to ignore.
     * @param maximumHeightFilter: Filter out resolutions not matching the maximum pixel height. Set to -1 to ignore.
     *
     * @return ArrayList	the list of display-modes
     */
    public static ArrayList<String> getAllDisplayModes(int minimumHeightFilter, int maximumHeightFilter)
    {
        ArrayList<String> dmList = new ArrayList<String>();
        String resolution = "";
        GraphicsEnvironment g = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] devices = g.getScreenDevices();

        // loop through available devices
        for (int i = 0; i < devices.length; i++) {
        	// loop through display modes of this device
            for (DisplayMode dm : devices[i].getDisplayModes())
            {
                resolution = dm.getWidth() + "x" + dm.getHeight();
                Test.output(resolution + " (" + dm.getBitDepth() + ", " + dm.getRefreshRate() + "Hz)");
                // add the resolution to the list if it does not exists there until now
                if (!dmList.contains(resolution) && dm.getHeight() >= minimumHeightFilter && (maximumHeightFilter == -1 || dm.getHeight() <= maximumHeightFilter))
                {
                    dmList.add(resolution);
                }
            }
        }
        
        // return the resulting list of modes
        return dmList;
    }

    /**
     * Return the display mode of the default device.
     * 
     * @return
     */
    public static DisplayMode getDefaultDeviceDisplayMode() {
    	return GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode();
    }

    /**
     * Return if a specific resolution is supported.
     * 
     * @return
     */
    public static boolean isResolutionSupported(String resolution)
    {
        return getAllDisplayModes(-1, -1).contains(resolution);
    }

    /**
     * Returns the highest supported S3 resolution. Currently this is any,
     * resolution with a high of not more than 900 pixel,
     * as higher resolutions cause on most configurations objects to disappear.
     * The fallback-resolution is 1024x768, in case we cannot find a supported
     * resolution.
     * The minimum height of any resolution is 768px.
     * 
     * @return Returns the highest supported S3 resolution
     */
    public static String getHighestSupportedResoultion()
    {
        int currentResolutionHeight =  getDefaultDeviceDisplayMode().getHeight();
        int currentResolutionWidth =  getDefaultDeviceDisplayMode().getWidth();
        if (currentResolutionHeight >= 768 && currentResolutionHeight <= 900) {
            // The current resolution is OK, as its height is not above 900 pixel while it has at least 768px in height, so lets take it
            return MessageFormat.format("{0}x{1}", currentResolutionWidth, currentResolutionHeight);
        } else if (isResolutionSupported("1600x900")) {
            return "1600x900";
        } else if (isResolutionSupported("1440x900")) {
            return "1440x900";
        } else if (isResolutionSupported("1368x768")) {
            return "1368x768";
        } else if (isResolutionSupported("1366x768")) {
            return "1366x768";
        } else if (isResolutionSupported("1152x864")) {
            return "1152x864";
        } else {
            return "1024x768";
        }
    }

    /**
     * Generate the S3.exe-file depending on settings which S3-version-number should be used.
     * 
     * @param s3ExePath
     * @param settlersLobby
     * @return boolean  true if the generation succeeded
     */
    public static boolean makeS3ALobbyExe(String s3ExePath, SettlersLobby settlersLobby)
    {
        RandomAccessFile file = null;
        
        if (s3ExePath == null)
        {
            Test.output("makeS3ALobbyExe() received a null s3ExePath"); 
            return false;
        }
      
        // Generate all actual s3.exe-versions
        S3ExeGeneration173 generator173 = new S3ExeGeneration173(file, s3ExePath, settlersLobby);
        S3CEGeneration generator300 = new S3CEGeneration(file, s3ExePath, settlersLobby);
        return generator300.doGenerate() && generator173.doGenerate();
    }

    /**
     * Check if the S3.exe-file is one from GOG.
     * 
     * @return boolean	true if it is from GOG
     */
    public static boolean isGog()
    {
        try
        {
        	File s3Path = ReqQuery.getS3ExeDirectory();
            if (s3Path == null)
            {
                return false;
            }
            return isGog(s3Path.getAbsolutePath());
        }
        catch(Exception e)
        {
            Test.outputException(e);
            return false;
        }
    }
    
    /**
     * Check if the S3.exe-file is one from GOG with given path.
     * 
     * @return boolean	true if it is from GOG
     */
    public static boolean isGog(String s3Dir)
    {
        try
        {
        	File s3Path = new File(s3Dir);
            File s3MultiExeFile = new File(s3Path, "S3_multi.EXE");

            if (Files.exists(s3MultiExeFile.toPath()))
            {
                return true;
            } else {
                return false;
            }
        }
        catch(Exception e)
        {
            Test.outputException(e);
            return false;
        }
    }
    
    /**
     * Generates a md5-hash for a file.
     * 
     * @return string	the md5-hash
     */
    public static String getMD5Checksum(File file)
    {
    	String result = null;
    	if( file.exists() ) {
    		result = CryptoSupport.calcMD5(file);
    	}
        return (result != null) ? result : "";
    	
    }
    
    /**
     * Generates a md5-hash for a file given as String.
     * 
     * @return string	the md5-hash
     */
    public static String getMD5Checksum(String filename)
    {
    	return getMD5Checksum(new File(filename));
    }

    /**
     * Check if the given path is a supported unified exe-file.
     * 
     * @param filePath
     * @return boolean	if it is supported
     */
    public static boolean isSupportedS3AlobbyExe(String filePath) 
    {
        String md5 = getMD5Checksum(filePath);
        return isSupportedMd5OfS3Exe(md5);
    }
    
    /**
     * Check if the given path is a supported unified exe-file by a given md5-hash
     * depending on given version.
     * Will be override in separate version-depending class-files.
     * 
     * @param filePath
     * @return boolean	if it is supported
     */
    public static boolean isSupportedMd5OfS3AlobbyExe(String md5)
    {
        return false;
    }

    /**
     * Checks if a given file is on blacklist by generating its md5-Hash.
     * 
     * @param filePath
     * @return boolean	true if it is on blacklist
     */
    public static boolean isBadS3Exe(String filePath) 
    {
        String md5 = getMD5Checksum(filePath);
        return isSupportedMd5OfS3Exe(md5);
    }
    
    /**
     * Check whether a given md5-hash is on blacklist.
     * 
     * @return boolean	true if it is on blacklist.
     */
    public static boolean isBadMd5OfS3Exe(String md5)
    {
        return md5.equalsIgnoreCase(s3160BadGOGexeMd5)
                || md5.equalsIgnoreCase(s3160BadGOGexeNewMd5)
                || md5.equalsIgnoreCase(s3160BadGOGMusicPatchMd5)
                || md5.equalsIgnoreCase(s3160GOGMusicPatchMd5)
                || md5.equalsIgnoreCase(s3160CDMusicPatchMd5)
                || isPreXPexe(md5);
    }

    /**
     * Checks if a given file is on whitelist by generating its md5-Hash.
     * 
     * @param filePath
     * @return boolean	true if it is on whitelist
     */
    public static boolean isSupportedS3Exe(String filePath) 
    {
        String md5 = getMD5Checksum(filePath);
        return isSupportedMd5OfS3Exe(md5);
    }
    
    /**
     * Check whether a given md5-hash is on whitelist.
     * 
     * @return boolean	true if it is on whitelist.
     */
    public static boolean isSupportedMd5OfS3Exe(String md5)
    {
        return md5.equalsIgnoreCase(s3160GOGexeMd5)
                || md5.equalsIgnoreCase(s3160XPexeMd5)
                || md5.equalsIgnoreCase(s3160GOGexeNewMd5)
                || isSupportedMd5OfS3AlobbyExe(md5);
    }

    public static boolean isPreXPexe(String md5)
    {
    	return md5.equalsIgnoreCase(s3160preXPexeMd5);
    }

    /**
     * Convert a hex-string to a byteArray.
     * 
     * @param s
     * @return byte[] 	the resulting byteArray
     */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
}