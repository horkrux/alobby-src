package net.siedler3.alobby.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.util.S3MapDataStructures.EMapFileVersion;

/**
 * Get data from an .map-file.
 * 
 * Source: jsettler - Settler 3 Remake
 */
public class S3MapData {
	// --------------------------------------------------//
	public static class MapResourceInfo {
		S3MapDataStructures.EMapFilePartType partType;
		public int offset = 0;
		public int size = 0;
		public int cryptKey = 0;
		public boolean hasBeenDecrypted = false;
	}
	// --------------------------------------------------//

	private List<MapResourceInfo> resources;

	public int fileChecksum = 0;
	public int widthHeight;
	public boolean isSinglePlayerMap = false;
	
	private byte[] mapContent;

	public S3MapDataContent mapData = new S3MapDataContent(0);

	/**
	 * Charset of read strings
	 */
	private static final Charset TEXT_CHARSET = Charset.forName("ISO-8859-1");

	public void S3MapDataContentReader(InputStream originalMapFile) throws IOException {
		// - init Resource Info
		resources = new LinkedList<MapResourceInfo>();

		// - init players
		mapData.setPlayerCount(1);

		// - read File into buffer
		mapContent = getBytesFromInputStream(originalMapFile);
	}
	
	public S3MapDataContent getS3MapDataContent() {
		return this.mapData;
	}

	// - reads the whole stream and returns it as BYTE-Array
	public static byte[] getBytesFromInputStream(InputStream is) throws IOException {

		// - read file to buffer
		try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
			byte[] buffer = new byte[0xFFFF];

			for (int len; (len = is.read(buffer)) != -1;) {
				os.write(buffer, 0, len);
			}
			os.flush();

			return os.toByteArray();
		} catch (Exception e) {
			return new byte[0];
		}
	}

	// - Read UNSIGNED Byte from Buffer
	public int readByteFrom(int offset) {
		if (mapContent == null)
			return 0;
		return mapContent[offset] & 0xFF;
	}

	// - Read Big-Ending INT from Buffer
	public int readBEIntFrom(int offset) {
		if (mapContent == null)
			return 0;
		return ((mapContent[offset] & 0xFF)) |
				((mapContent[offset + 1] & 0xFF) << 8) |
				((mapContent[offset + 2] & 0xFF) << 16) |
				((mapContent[offset + 3] & 0xFF) << 24);
	}

	// - Read Big-Ending 2 Byte Number from Buffer
	public int readBEWordFrom(int offset) {
		if (mapContent == null)
			return 0;
		return ((mapContent[offset] & 0xFF)) |
				((mapContent[offset + 1] & 0xFF) << 8);
	}

	// - read the Higher 4-Bit of the buffer
	public int readHighNibbleFrom(int offset) {
		if (mapContent == null)
			return 0;
		return (mapContent[offset] >> 4) & 0x0F;
	}

	// - read the Lower 4-Bit of the buffer
	public int readLowNibbleFrom(int offset) {
		if (mapContent == null)
			return 0;
		return (mapContent[offset]) & 0x0F;

	}

	// - read a C-Style String from Buffer (ends with the first \0)
	public String readCStrFrom(int offset, int length) {
		if (mapContent == null)
			return "";
		if (mapContent.length <= offset + length)
			return "";

		// - find \0 char in buffer
		int i = 0;
		for (; i < length; i++) {
			if (mapContent[offset + i] == 0) {
				break;
			}
		}

		if (i == 0) {
			return "";
		}

		// - substring + encoding
		return new String(mapContent, offset, i - 1, TEXT_CHARSET);
	}

	// - returns a File Resources
	private MapResourceInfo findResource(S3MapDataStructures.EMapFilePartType type) {
		for (MapResourceInfo element : resources) {
			if (element.partType == type)
				return element;
		}

		return null;
	}

	// - calculates the checksum of the file and compares it
	boolean isChecksumValid() {
		// - read Checksum from File
		int fileChecksum = readBEIntFrom(0);

		mapData.fileChecksum = fileChecksum;

		// - make "count" a Multiple of four
		int count = mapContent.length & 0xFFFFFFFC;
		int currentChecksum = 0;

		// - Map Content starts at Byte 8
		for (int i = 8; i < count; i += 4) {

			// - read DWord
			int currentInt = ((mapContent[i] & 0xFF)) |
					((mapContent[i + 1] & 0xFF) << 8) |
					((mapContent[i + 2] & 0xFF) << 16) |
					((mapContent[i + 3] & 0xFF) << 24);

			// - using: Logic Right-Shift-Operator: >>>
			currentChecksum = ((currentChecksum >>> 31) | ((currentChecksum << 1) ^ currentInt));
		}

		// - return TRUE if the checksum is OK!
		return (currentChecksum == fileChecksum);
	}

	// - Reads in the Map-File-Structure
	public boolean loadMapResources() {
		// - Version of File: 0x0A : Original Settlers Map ; 0x0B : Amazon Map
		int fileVersion = readBEIntFrom(4);

		// - check if the Version is compatible?
		if ((fileVersion != EMapFileVersion.DEFAULT.value) && (fileVersion != EMapFileVersion.AMAZONS.value))
			return false;

		// - Data length
		int dataLength = mapContent.length;

		// - start of map-content
		int filePos = 8;
		int partTypeTemp;

		do {
			partTypeTemp = readBEIntFrom(filePos);
			int partLen = readBEIntFrom(filePos + 4);

			// - don't know what the [FileTypeSub] is for -> it should by zero
			int partType = (partTypeTemp & 0x0000FFFF);

			// - position/start of data
			int mapPartPos = filePos + 8;

			// - next position in File
			filePos = filePos + partLen;

			// - save the values
			if ((partType > 0) && (partType < S3MapDataStructures.EMapFilePartType.length) && (partLen >= 0)) {
				MapResourceInfo newRes = new MapResourceInfo();

				newRes.partType = S3MapDataStructures.EMapFilePartType.getTypeByInt(partType);
				newRes.cryptKey = partType;
				newRes.hasBeenDecrypted = false;
				newRes.offset = mapPartPos;
				newRes.size = partLen - 8;

				resources.add(newRes);
			}

		} while ((partTypeTemp != 0) && ((filePos + 8) <= dataLength));

		return true;
	}

	// - freeing the internal File-Buffer
	public void freeBuffer() {
		mapContent = null;
		mapData.freeBuffer();
	}

	// - to process a map File this class loads the whole file to memory. To save memory this File-Buffer is
	// - closed after using/when done processing. If more data are requested from the File, the File-Biffer
	// - is loaded again with this reOpen() function.
	public void reOpen(InputStream originalMapFile) {
		// - read File into buffer
		try {
			mapContent = getBytesFromInputStream(originalMapFile);
		} catch (Exception e) {
			Test.output(e.getMessage());
		}

		// - reset Crypt Info
		for (MapResourceInfo element : resources) {
			element.hasBeenDecrypted = false;
		}
	}

	public void readBasicMapInformation() {
		// - Reset
		fileChecksum = 0;
		widthHeight = 0;

		// - safety checks
		if (mapContent == null) {
			return;
		}
		if (mapContent.length < 100) {
			return;
		}

		// - checksum is the first DWord in File
		fileChecksum = readBEIntFrom(0);

		// - read Map Information
		readMapInfo();
		readPlayerInfo();

		// - get resource information for the area to get map height and width
		MapResourceInfo filePart = findResource(S3MapDataStructures.EMapFilePartType.AREA);

		if (filePart == null)
			return;
		if (filePart.size < 4)
			return;

		// TODO: original map: the whole AREA-Block is decrypted but we only need the first 4 byte. Problem... maybe later we need the rest but only
		// if this map is selected for playing AND there was no freeBuffer() and reOpen() call in between.
		// - Decrypt this resource if necessary
		if (!doDecrypt(filePart))
			return;

		// - file position of this part
		int pos = filePart.offset;
		
		// - read height and width (they are the same)
		this.widthHeight = readBEIntFrom(pos);
		
	}
	
	public int getWidthHeight() {
		return this.widthHeight;
	}

	// - Read some common information from the map-file
	public void readMapInfo() {
		MapResourceInfo filePart = findResource(S3MapDataStructures.EMapFilePartType.MAP_INFO);

		if ((filePart == null) || (filePart.size == 0)) {
			Test.output("No Map information available in mapfile " + filePart);
			return;
		}

		// - Decrypt this resource if necessary
		if (!doDecrypt(filePart))
			return;

		// - file position
		int pos = filePart.offset;

		// ----------------------------------
		// - read mapType (single / multiplayer map?)
		int mapType = readBEIntFrom(pos);
		pos += 4;

		if (mapType == 1) {
			isSinglePlayerMap = true;
		} else if (mapType == 0) {
			isSinglePlayerMap = false;
		} else {
			System.err.println("wrong value for 'isSinglePlayerMap' " + Integer.toString(mapType) + " in mapfile!");
		}

		// ----------------------------------
		// - read Player count
		int playerCount = readBEIntFrom(pos);
		pos += 4;

		mapData.setPlayerCount(playerCount);

		// ----------------------------------
		// - read start resources
		//int startResourcesValue = readBEIntFrom(pos);
		//this.startResources = EMapStartResources.fromMapValue(startResourcesValue);
	}

	// - Read stacks from the map-file
	public boolean readStacks() {
		MapResourceInfo filePart = findResource(S3MapDataStructures.EMapFilePartType.STACKS);

		if ((filePart == null) || (filePart.size == 0)) {
			Test.output("No Stacks available in mapfile " + filePart);
			return false;
		}

		// - Decrypt this resource if necessary
		if (!doDecrypt(filePart))
			return false;

		// - file position
		int pos = filePart.offset;

		// - Number of buildings
		int stackCount = readBEIntFrom(pos);
		pos += 4;

		// - safety check
		if ((stackCount * 8 > filePart.size) || (stackCount < 0)) {
			System.err.println("wrong number of stacks in map File: " + stackCount);
			return false;
		}

		// - read all Stacks
		for (int i = 0; i < stackCount; i++) {

			int posX = readBEWordFrom(pos);
			pos += 2;
			int posY = readBEWordFrom(pos);
			pos += 2;

			int stackType = readByteFrom(pos++);
			int count = readByteFrom(pos++);

			pos += 2; // not used - maybe: padding to size of 8 (2 INTs)

			// -------------
			// - update data
			mapData.setStack(posX, posY, stackType, count);
		}

		return true;
	}

	// - Read the Player Info
	public void readPlayerInfo() {
		MapResourceInfo filePart = findResource(S3MapDataStructures.EMapFilePartType.PLAYER_INFO);

		if ((filePart == null) || (filePart.size == 0)) {
			Test.output("No Player information available in mapfile " + filePart);
			return;
		}

		// - Decrypt this resource if necessary
		if (!doDecrypt(filePart))
			return;

		// - file position
		int pos = filePart.offset;

		for (int i = 0; i < mapData.getPlayerCount(); i++) {

			int nation = readBEIntFrom(pos);
			pos += 4;

			int startX = readBEIntFrom(pos);
			pos += 4;

			int startY = readBEIntFrom(pos);
			pos += 4;

			String playerName = readCStrFrom(pos, 33);
			pos += 33;

			mapData.setPlayer(i, startX, startY, nation, playerName);

		}
	}

	// - Reads in the Map Data / Landscape and MapObjects like trees
	public boolean readMapData() {
		// - get resource information for the area
		MapResourceInfo filePart = findResource(S3MapDataStructures.EMapFilePartType.AREA);

		if ((filePart == null) || (filePart.size == 0)) {
			Test.output("No area information available in mapfile " + filePart);
			return false;
		}

		// - Decrypt this resource if necessary
		if (!doDecrypt(filePart))
			return false;

		// - file position
		int pos = filePart.offset;

		// - height and width are the same
		int widthHeight = readBEIntFrom(pos);
		pos += 4;

		// - init size of MapData
		mapData.setWidthHeight(widthHeight);

		// - points to read
		int dataCount = widthHeight * widthHeight;

		for (int i = 0; i < dataCount; i++) {
			mapData.setLandscapeHeight(i, readByteFrom(pos++));
			mapData.setLandscape(i, readByteFrom(pos++));
			mapData.setMapObject(i, readByteFrom(pos++));
			readByteFrom(pos++); // - which Player is the owner of this position
			mapData.setAccessible(i, mapContent[pos++]);

			mapData.setResources(i, readHighNibbleFrom(pos), readLowNibbleFrom(pos));
			pos++;
		}

		return true;
	}

	// - Decrypt a file resource
	private boolean doDecrypt(MapResourceInfo filePart) {

		if (filePart == null)
			return false;

		if (mapContent == null) {
			Test.output("OriginalMapFile-Warning: Unable to decrypt map file: no data loaded from " + filePart);
			return false;
		}

		// - already encrypted
		if (filePart.hasBeenDecrypted)
			return true;

		// - length of data
		int length = filePart.size;
		if (length <= 0)
			return true;

		// - start of data
		int pos = filePart.offset;

		// - check if the file has enough data
		if ((pos + length) >= mapContent.length) {
			Test.output("Error: Unable to decrypt map file: out of data!");
			return false;
		}

		// - init the key
		int key = (filePart.cryptKey & 0xFF);

		for (int i = length; i > 0; i--) {

			// - read one byte and uncrypt it
			int byt = (mapContent[pos] ^ key);

			// - calculate next Key
			key = (key << 1) ^ byt;

			// - write Byte
			mapContent[pos] = (byte) byt;
			pos++;
		}

		filePart.hasBeenDecrypted = true;
		return true;
	}

}