package net.siedler3.alobby.util;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import net.siedler3.alobby.controlcenter.SettlersLobby;

/**
 * Class introduced to port TimerTasks to new Executor Service
 * with minimal impact to the interface calls (especially call to cancel())
 * @author jim
 *
 */
public abstract class TimerTaskCompatibility implements Runnable
{

    private ScheduledFuture<?> self = null;
    private boolean isCanceled = false;

    public void cancel()
    {
        isCanceled = true;
        if (self != null)
        {
            self.cancel(true);
        }
    }

    /**
     * schedule at a fixed rate after initalDelay.
     * @param initialDelay
     * @param rate
     */
    public void schedule(long initialDelay, long rate)
    {
        if (!isCanceled)
        {
            self = SettlersLobby.scheduler.scheduleAtFixedRate(this, initialDelay, rate, TimeUnit.MILLISECONDS);
        }
    }

    /**
     * schedule for execution after an initial delay.
     * @param delay
     */
    public void schedule(long delay)
    {
        if (!isCanceled)
        {
            self = SettlersLobby.scheduler.schedule(this, delay, TimeUnit.MILLISECONDS);
        }
    }
}
