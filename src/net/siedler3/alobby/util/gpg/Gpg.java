package net.siedler3.alobby.util.gpg;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.util.ExtractFile;

public class Gpg
{
    private static final String gpgFilePath = ALobbyConstants.PACKAGE_PATH + "/util/gpg/gpg.exe"; //$NON-NLS-1$
    private static final String gpgPublicKeyFilePath = ALobbyConstants.PACKAGE_PATH + "/util/gpg/alobby_public_key.gpg"; //$NON-NLS-1$
    private static final String gpgIconvDllFilePath = ALobbyConstants.PACKAGE_PATH + "/util/gpg/iconv.dll"; //$NON-NLS-1$
    private File directory;
    private File gpgExe;
    private File gpgIconvDll;
    private File publicKeyFile;
    private File keyRing;

    protected boolean extractGpg() throws IOException
    {
        boolean result = false;
        directory = ExtractFile.createTempDir();
        directory.deleteOnExit();
        gpgExe = ExtractFile.extract(Gpg.class.getResourceAsStream(gpgFilePath), new File(gpgFilePath).getName(), directory);
        gpgExe.deleteOnExit();
        gpgExe.getParentFile().deleteOnExit();
        publicKeyFile = ExtractFile.extract(Gpg.class.getResourceAsStream(gpgPublicKeyFilePath),
                                            new File(gpgPublicKeyFilePath).getName(),
                                            directory);
        publicKeyFile.deleteOnExit();
        keyRing = new File(directory, "keyring");
        keyRing.deleteOnExit();
        new File(keyRing.getAbsolutePath() + ".bak").deleteOnExit();
        new File(keyRing.getAbsolutePath() + ".lock").deleteOnExit();
        
        gpgIconvDll = ExtractFile.extract(Gpg.class.getResourceAsStream(gpgIconvDllFilePath), new File(gpgIconvDllFilePath).getName(), directory);
        gpgIconvDll.deleteOnExit();


        String[] commandKeyRing = {
                        gpgExe.getAbsolutePath(),
                        "--armor",
                        "--no-default-keyring",
                        "--keyring",
                        keyRing.getAbsolutePath(),
                        "--import",
                        publicKeyFile.getAbsolutePath()
        };

        ProcessBuilder pb = new ProcessBuilder(Arrays.asList(commandKeyRing));
        pb.redirectErrorStream(true);
        Process p = pb.start();

        //CP850 ist die standard westeuropäische Codepage die bei DOS benutzt wird
        //zumindest in unserer Gegend
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(), "CP850")); //$NON-NLS-1$

        String line;
        while ((line = reader.readLine()) != null)
        {
            Test.output(line);
        }

        try
        {
            if (p.waitFor() != 0)
            {
                Test.output("exit value = " + p.exitValue()); //$NON-NLS-1$
            }
            else
            {
                result = true;
            }
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
            Test.outputException(e);
        }
        return result;
    }

    public boolean verify(File file, File signature) throws IOException
    {
        boolean result = false;
        String[] commandVerify = {
                        gpgExe.getAbsolutePath(),
                        "--armor",
                        "--no-default-keyring",
                        "--keyring",
                        keyRing.getAbsolutePath(),
                        "--verify",
                        signature.getAbsolutePath(),
                        file.getAbsolutePath()
        };

        ProcessBuilder pb = new ProcessBuilder(Arrays.asList(commandVerify));
        pb.redirectErrorStream(true);
        Process p = pb.start();

        //CP850 ist die standard westeuropäische Codepage die bei DOS benutzt wird
        //zumindest in unserer Gegend
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(), "CP850")); //$NON-NLS-1$

        String line;
        while ((line = reader.readLine()) != null)
        {
            Test.output(line);
        }

        try
        {
            if (p.waitFor() != 0)
            {
                Test.output("exit value = " + p.exitValue()); //$NON-NLS-1$
            }
            else
            {
                result = true;
            }
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
            Test.outputException(e);
        }

        return result;

    }

    public static boolean verifySignature(File file, File signature)
    {
        boolean result = false;
        try
        {
            Gpg gpg = new Gpg();
            gpg.extractGpg();
            result = gpg.verify(file, signature);
        }
        catch (Exception e)
        {
            Test.outputException(e);
            result = false;
        }
        return result;
    }

}
