/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.text.DateFormat;
import java.util.Date;

import net.siedler3.alobby.controlcenter.ReqQuery;
import net.siedler3.alobby.controlcenter.Test;


public class SavegameReader
{
    public static void main(String args[])
    {
        RandomAccessFile file = null;
        PrintStream out = null;

        try
        {
            out = new PrintStream(new FileOutputStream(new File("savegames.txt")));
            String gamePathFromReg = ReqQuery.getS3ExeFilePath();
            int s3Index = gamePathFromReg.lastIndexOf(File.separator);
            if (s3Index == -1)
            {
                println(out, gamePathFromReg);
                System.exit(1);
            }
            
            String path = gamePathFromReg.substring(0, s3Index + 1);
            path += "Save" + File.separator + "Multi"; 
             
            File[] savegames = new File(path).listFiles(new FilenameFilter()
            {
                public boolean accept(File dir, String fileName)
                {
                    return fileName.toLowerCase().endsWith(".mps");
                }
            });

            for(File savegame : savegames)
            {
                println(out, "file: " + savegame.getName());
                file = new RandomAccessFile(savegame,"r");

                //Wenn man bei offset 0x10 beginnt, funktioniert
                //die Entschlüsselung des folgenden Abschnitts.
                //Innerhalb des Abschnitts ab offset 24 beginnt der mapname
                int offsetMapRead = 0x10;
                file.seek(offsetMapRead);
                String map = readFixedString(file, 0x100);
                map = decodeString(map);
                map = map.substring(24);
                map = getZeroString(map);
                println(out, "map: " + map);
                
                long lastModified = savegame.lastModified();
                println(out, DateFormat.getDateTimeInstance().format(new Date(lastModified)));
                
                
                for (int ii = 0; ii < 20;++ii)
                {
                    int off = 0x392 + ii*0x134;
                    file.seek(off);
                    String str = readFixedString(file, 100);
                    String str1 = decodeString(str);
                    String str2 = getZeroString(str1);
                    println(out, "offset: " + off + "(0x" + Integer.toHexString(off) + ")" + 
                            ", name: " + str2);
                }
                println(out, "------------------------------------------");
                println(out, "");

                if (file != null) {
                	try {
        				file.close();
        			} catch (IOException e) {
        				Test.outputException(e);
        			}
                }
            }
        }
        catch(Exception e)
        {
            System.err.println("**Error: " + e.getMessage() );
        }
        if (out != null) {
            out.close();
        }
        if (file != null) {
        	try {
				file.close();
			} catch (IOException e) {
				Test.outputException(e);
			}
        }
    }
    
    private static void println(PrintStream out, String string)
    {
        System.out.println(string);
        out.println(string);
    }

    /**
     * versucht die Verschlüsselung der S3 savegames rückgängig zu
     * machen. Der String input muß mit einem Zeichen beginnen,
     * bei dem der verschlüsselte Wert gleich dem entschlüsselten
     * Wert ist. Wo solche Starts von Verschlüsselungssequenzen sind,
     * lässt sich nicht definitiv sagen, häufig steht zumindest links
     * davon mindestens eine '0'. 
     * 
     * Aus dem aktuellen Zeichen und dessen Verschlüsselung wird
     * eine XOR-mask für das folgende Zeichen berechnet.
     * Für das erste Zeichen kennt man diese XOR-mask nicht, daher
     * wird sie auf 0 gesetzt, was zur Folge hat, daß Ver- und Entschlüsselung
     * vom ersten Zeichen identisch sind (bzw. sein müssen).
     * Für die Spielernamen in den S3-savgames ist dieses tatsächlich der Fall,
     * der erste Buchstabe steht immer im Klartext drin.
     * 
     * So wird die XorMask für das folgende Zeichen aus dem aktuellen Zeichen berechnet:
     * plainValueXorMask = (2 * unverschlüsselter Wert) XOR unverschlüsselter Wert
     * 
     * xorMask = (2 * verschlüsselter Wert) XOR plainValueXorMask
     * 
     * Mit dieser xorMask kann jetzt das folgende Zeichen entschlüsselt werden.
     * 
     * @param input
     * @return
     */
    public static String decodeString(String input)
    {
        byte[] byteArray = new byte[input.length()];
        int xorMask = 0;
        int value = 0;
        int decValue = 0;
        for (int i = 0; i < input.length(); ++i)
        {
            value = input.charAt(i) & 0xff;
            //die XOR-Maske, die aus dem vorherigen Zeichen berechnet wurde
            //entschlüsselt das aktuelle Zeichen
            decValue = (value ^ xorMask) & 0xff;
            byteArray[i] = (byte)(decValue & 0xff);
            //Berechnung der xorMask für das folgende Zeichen
            int plainValueXorMask = (2*decValue ^ decValue) & 0xff;
            xorMask = ((2*value) ^ plainValueXorMask) & 0xff;
        }
        String result = "";
        try
        {
            result = new String(byteArray, "ISO-8859-1");
        } 
        catch (Exception e) {
        }
        return result;
    }
    
    /**
     * Wenn input einen Null-terminierten String enthält (C/C++ Standard),
     * wird dieser zurückgeliefert.
     * 
     * @param input
     * @return
     */
    public static String getZeroString(String input)
    {
        String result = "";
        int index = input.indexOf(0);
        if (index >=0)
        {
            result = input.substring(0, index);
        }
        return result;
    }
    
    private static String readFixedString(RandomAccessFile file, int length)
    {
        byte[] byteArray = new byte[length];
        int readLength = 0;
        String result = "";
        try
        {
            readLength = file.read(byteArray);
            result = new String(byteArray, 0, readLength, "ISO-8859-1");
        }
        catch (Exception e) 
        {
        }
        return result;
    }
    
}