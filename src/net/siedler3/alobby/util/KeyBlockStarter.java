/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;

/**
 * Start the exe-file which blocks the usage of both windows-keys after gamestart.
 * 
 * @author adrianer
 *
 */
public class KeyBlockStarter extends Thread
{
    private Process process;

    /**
     * Constructor for KeyBlockStarter.
     * 
     * @param settlersLobby
     * @throws IOException
     */
    public KeyBlockStarter(SettlersLobby settlersLobby) throws IOException
    {
        String threadName = getName() + "-KeyBlockStarter";
        Test.output("KeyBlockStarter threadName: " + threadName);
        setName(threadName);

        String installDir = System.getProperty("user.dir");
        Test.output("installDir: " + installDir);

        // block F1-key and the windows-keys
        String blockExe = "block_f1_and_win_keys.exe";
        if( settlersLobby.is1366x768Supported ){
            // only block the windows-keys on higher resolution
            blockExe = "block_win_keys.exe";
        }

        // check of the file exists
        File blockExeFile = new File(installDir, blockExe);
        if( blockExeFile.exists() ) {
            // define the command
            List<String> command = new ArrayList<String>();

            // add the command
            command.add(installDir + File.separator + blockExe);

            // build the process with the generated command
            ProcessBuilder builder = new ProcessBuilder(command);
            builder.directory(new File(installDir));
            builder.inheritIO();
            Test.output("starting KeyBlockStarter: " + builder.command());
            // -> start the process
            process = builder.start();

            if (process != null)
            {
                Test.output("using class " + process.getClass().getName());
            }
        }
    }

    @Override
    public void run()
    {
        try
        {
            int rc = process.waitFor();
            Test.output("KeyBlock exited: " + rc);
        }
        catch (InterruptedException e)
        {
            //the gamestarter may interrupt this thread, in this case
            //cleanup is false
            Test.outputException(e);
            Thread.currentThread().interrupt();
        }
        catch (Exception ex)
        {
            Test.outputException(ex);
            //something unexpected happened
            //clear the link to the process
        }
        finally
        {
            Test.output("Setting KeyBlock process to null");
            process = null;
        }
    }

    /**
     * Kill the process.
     */
    public void destroyKeyBlock()
    {
        if (process != null)
        {
            process.destroy();
            try {
                Thread.sleep(750);
            } catch (InterruptedException e) {}
            if (process != null)
            {
                process.destroyForcibly();
            }
        }
    }

    /**
     * Return marker if process is running.
     * 
     * @return boolean	true if it is running
     */
    public boolean isKeyBlockRunning()
    {
        return process != null;
    }
}
