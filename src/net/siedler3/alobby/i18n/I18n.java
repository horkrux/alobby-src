package net.siedler3.alobby.i18n;
/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */


import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class I18n
{
    public final static Locale LOCALE_POLISH = new Locale("pl");
    
    public static Locale currentLocale = Locale.getDefault();

    public static ResourceBundle text = ResourceBundle.getBundle(
            "net.siedler3.alobby.i18n.TextBundle", currentLocale); //$NON-NLS-1$
    
    /**
     * Liefert den lokalisierten Text zum übergebenen key.
     * Ist der Text nicht vorhanden, wird der key zurückgegeben.
     * Referenziert das Bundle "TextBundle".
     * @param key
     * @return lokalisierter Text
     */
    public static String getString(String key)
    {
        try
        {
            return text.getString(key);
        }
        catch (MissingResourceException e)
        {
            return e.getKey();
        }
    }
    
    /**
     * Setzt die Sprache.
     * @param currentLocale Die neue Sprache.
     */
    public static void setLanguage(Locale currentLocale) {
    	I18n.currentLocale = currentLocale;
    	text = ResourceBundle.getBundle(
            "net.siedler3.alobby.i18n.TextBundle", currentLocale); //$NON-NLS-1$
    }

}
