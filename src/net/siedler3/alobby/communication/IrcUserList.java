/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.communication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.Test;

import org.jibble.pircbot.User;


public class IrcUserList
{
    private String channel = "";
    private Configuration config;
    
    //speichert lokal die Liste der User auf dem (einzigen) channel
    //sie ist synchronisiert, da sowohl der IrcThread als auch die GUI drauf zugreifen
    private Map<String, IrcUser> ircUsers = Collections.synchronizedMap(new HashMap<String, IrcUser>());
    //ein Kopie der User die der PircBot liefert, um schnell auf einzelne user zugreifen zu können
    private Map<String, User> pircUsers = Collections.synchronizedMap(new HashMap<String, User>());

    public IrcUserList(Configuration config) {
    	super();
    	this.config = config;
    }
    
    public void setChannel(String channel)
    {
        this.channel = channel;
    }
    
    public void clearAll()
    {
        ircUsers.clear();
        pircUsers.clear();
    }
    /**
     * aktualisiert die Liste ircUsers dahingehend, daß nur die
     * Einträge existieren, die auch in pircUsers stehen.
     */
    public void sychronizeUsers()
    {
        try
        {
            //zuerst werden alle nicks gelöscht, die nicht mehr im Channel sind
            Iterator<IrcUser> it = ircUsers.values().iterator();
            while(it.hasNext())
            {
                IrcUser user = it.next();
                if (!pircUsers.containsKey(user.getNick()))
                {
                    it.remove();
                }
            }
            //dann werden alle nicks hinzugefügt, die noch nicht existieren
            for (User user : pircUsers.values())
            {
                if (!this.ircUsers.containsKey(user.getNick()))
                {
                    rawAddUser(user.getNick());
                }
            }
            dumpUsers();
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
    }
    
    private void rawAddUser(String nick)
    {
        ircUsers.put(nick, new IrcUser(nick, pircUsers, config, config.getUserName().equals(nick)));
    }
    public void addUser(String nick)
    {
        rawAddUser(nick);
        //dumpUsers();
    }
    
    public void removeUser(String nick)
    {
        ircUsers.remove(nick);
        //dumpUsers();
    }
    
    public void renameUser(String oldNick, String newNick)
    {
        IrcUser user = ircUsers.remove(oldNick);
        if (user != null)
        {
            user.setNick(newNick);
        }
        else
        {
            Test.output("WARNING, " + oldNick + " was not in List");
            user = new IrcUser(newNick, pircUsers, config, config.getUserName().equals(newNick));
        }
        ircUsers.put(newNick, user);
        //dumpUsers();
    }
    
    public void setAwayUser(String nick, String awayMessage)
    {
        IrcUser user = ircUsers.get(nick);
        if (user != null)
        {
            user.setAwayMsg(awayMessage);
            //dumpUsers();
        }
    }
    
    public void clearInfoUser(String nick)
    {
        IrcUser user = ircUsers.get(nick);
        if (user != null)
        {
            user.clearInfo();
        }
    }

    public void setAwayUser(String nick, boolean isAway)
    {
        IrcUser user = ircUsers.get(nick);
        if (user != null)
        {
            user.setAway(isAway);
        }
        
    }

    
    private void dumpUsers()
    {
        StackTraceElement trace = new Throwable().getStackTrace()[1];
        Test.output(trace.getMethodName() + "(): IrcUsers:");
        for (IrcUser user : ircUsers.values())
        {
            Test.output(user.toString());
        }
    }
    
    public void updatePircUsers(User[] users)
    {
        this.pircUsers.clear();
        for (User user : users)
        {
            pircUsers.put(user.getNick(), user);
        }

    }
    
    public void updatePircUsers(String channel, User[] users)
    {
        //es wird nur ein Channel unterstützt
        if (channel.equalsIgnoreCase(this.channel))
        {
            updatePircUsers(users);
        }
    }
    
    public IrcUser[] getSortedUserList()
    {
        IrcUser[] list = ircUsers.values().toArray(new IrcUser[0]); 
        Arrays.sort(list);
        return list;
    }
    
    public IrcUser getUser(String nick)
    {
        return ircUsers.get(nick);
    }

    /**
     * Gib ein Array mit den Usern zurück, die momentan away sind.
     * @return
     */
    public List<IrcUser> getAwayUserList()
    {
        List<IrcUser> awayUsers = new ArrayList<IrcUser>();
        for (IrcUser user : ircUsers.values())
        {
            if (user.isAway())
            {
                awayUsers.add(user);
            }
        }
        return awayUsers;
    }

    /**
     * Set the vpn-state of a single user in this list.
     * 
     * @param nick
     * @param userVpnState
     */
	public void setUserVPNState(String nick, boolean userVpnState) {
		IrcUser user = ircUsers.get(nick);
        if (user != null)
        {
            user.setVpnState(userVpnState);
        }
	}
}
