/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.communication;

import java.util.Map;

import net.siedler3.alobby.controlcenter.AwayStatus;
import net.siedler3.alobby.controlcenter.Configuration;

import org.jibble.pircbot.User;

/**
 * Speichert Informationen von einem IRC user.
 * Für die LowLevel Informationen wird auf 
 * eine Userliste vom PircBot referenziert, auf
 * die bei Bedarf zugegriffen wird.
 * 
 * @author jim
 * @author Stephan Bauer (aka maximilius)
 *
 */
public class IrcUser implements Comparable<IrcUser>
{
    public static boolean awayUsersAtEndOfList = true;
    public static boolean friendUsersAtStartOfList = true;
    
    private String nick;
    private String nickLowercase;
    private boolean isOwnUser;
    //jeder User braucht eine Referenz auf die Map in IrcUserList,
    //damit der User immer bei Bedarf erfragt werden kann.
    //dadurch muss man sich nicht um op und voice kümmern
    private Map<String, User> pircUsers;

    //falls die away message des users bekannt ist
    private String awayMsg = "";
    //zeigt an ob der user away ist (sofern bekannt)
    private boolean isAway;
    //zeigt an, ob der User im Game ist.
    private boolean isInGame = false;
    private Configuration config;
    // marker for vpn-state of this user
	private boolean vpnState = false;
    
    public IrcUser(String nick, Map<String, User> pircUsers, Configuration config, boolean isOwnUser)
    {
        super();
        this.nick = nick;
        this.nickLowercase = nick.toLowerCase();
        this.pircUsers = pircUsers;
        this.config = config;
        this.isOwnUser = isOwnUser;
        this.vpnState = false;
    }
    
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(getPrefix());
        sb.append(getNick());
        sb.append(", ");
        sb.append(isAway);
        sb.append(", ");
        sb.append(awayMsg);
        
        return sb.toString();
    }

    private User getRawUser()
    {
        User user = pircUsers.get(nick);
        return user;
    }
    
    public boolean isOp()
    {
        User user = getRawUser();
        return user == null ? false : user.isOp();
    }
    
    public boolean hasVpnActive() {
    	return vpnState;
    }
    
    public boolean hasVoice()
    {
        User user = getRawUser();
        return user == null ? false : user.hasVoice();
    }
    
    public String getPrefix()
    {
        User user = getRawUser();
        return user == null ? "" : user.getPrefix();
    }
    
    public String getNick()
    {
        return nick;
    }
    public void setNick(String nick)
    {
        this.nick = nick;
        this.nickLowercase = nick.toLowerCase();
    }

    public String getAwayMsg()
    {
        return awayMsg;
    }

    public void setAwayMsg(String awayMsg)
    {
        if (awayMsg != null)
        {
        	this.awayMsg = awayMsg;
            if (awayMsg.isEmpty()) {
                isAway = false;
                isInGame = false;
            }
            else {
                isAway = true;
                isInGame = AwayStatus.isInGameMessage(awayMsg);
            }
        }
    }

    public boolean isAway()
    {
        return isAway;
    }

    public void setAway(boolean isAway)
    {
        this.isAway = isAway;
        if (!isAway)
        {
            this.awayMsg = "";
            isInGame = false;
        }
    }

    public void clearInfo()
    {
        isAway = false;
        isInGame = false;
        awayMsg = "";
    }

    public int compareTo(IrcUser o)
    {
    	if (isOwnUser) {
    		return -1;
    	} else if (o.isOwnUser) {
    		return 1;
    	}
        if (config.isSortBuddys())
        {
            //user die away sind, sollen ans Ende der Liste rücken
            if (isBuddy() != o.isBuddy())
            {
                //wenn sich die Objekte im friend Status unterscheiden,
                //dann ist dasjenige "größer", das kein Freund ist 
                return isBuddy() ? -1 : 1;
            }
            //wenn der Friend status gleich ist, wird einfach
            //ganz normal weiter sortiert
        }
        if (config.isSortInGame())
        {
            //user die inGame sind, sollan ans Ende der Liste rücken.
        	if (isInGame != o.isInGame) {
                //wenn sich die Objekte im inGame Status unterscheiden,
                //dann ist dasjenige "größer", das inGame ist 
                return isInGame ? 1 : -1;
            }
        }
        if (config.isSortAway())
        {
        	//user die away sind, sollen ans Ende der Liste rücken
            if (isAway != o.isAway)
            {
                //wenn sich die Objekte im away Status unterscheiden,
                //dann ist dasjenige "größer", das away ist 
                return isAway ? 1 : -1;
            }
            //wenn der away status gleich ist, wird einfach
            //ganz normal alphabetisch sortiert
        }
        //alphabetische Sortierung ohne Groß-/Kleinschreibung
        return this.nickLowercase.compareTo(o.nickLowercase);
    }

    /**
     * Getter
     * @return Ob der ircUser ein Freund ist.
     */
	public boolean isBuddy() {
		return config.isBuddy(nick);
	}

	/**
	 * Set vpn-state of this user.
	 * 
	 * @param userVpnState
	 */
	public void setVpnState(boolean userVpnState) {
		vpnState  = userVpnState;
	}
}
