package net.siedler3.alobby.communication.protocol;

import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;

public class PrgMsgVPNStateListener extends ProgramMessageAdapter
{
    private SettlersLobby settlersLobby;

    public PrgMsgVPNStateListener(SettlersLobby settlerLobby)
    {
        super();
        this.settlersLobby = settlerLobby;
    }
    
    @Override
    public void onPrgMsgVPNStateMessage(String userName, boolean vpnState)
    {
    	settlersLobby.getIRCCommunicator().onVPNStateChange(userName, vpnState);
    }
    
    @Override
    public void onPrgMsgAwayMessage(String userName, String awayMessage)
    {
    }

    @Override
    public void onPrgMsgAwayBack(String userName)
    {
    }

	@Override
	public void onPrgMsgGameRunning(OpenGame game) {
	}

	@Override
	public void onPrgMsgCloseRunningGame(String ip, String hostUserName) {
	}

	@Override
	public void onPrgMsgQueryRunningGame(String userName) {
	}

	@Override
	public void onPrgMsgSetRaceInGame(String hostUserName, String senderName, String race) {
	}

	@Override
	public void onPrgMsgCloseStream(String user) {
	}

	@Override
	public void onPrgMsgNewStream(String user, String link, String onlinesince) {
	}

	@Override
	public void onPrgMsgQueryVPNState(String userName) {
		settlersLobby.onPrgMsgQueryVPNState(userName);
	}

	@Override
	public void onPrgMsgS4StartGame(String senderName, String ip, String hostUserName, String ipUserList) {
	}

	@Override
	public void onPrgMsgDirectMessage(String receiver, String message) {
	}

	@Override
	public void onPrgMsgBetaUserJoined(String user) {
	}

	@Override
	public void onPrgMsgQueryBetaUsers(String userName) {
	}

	@Override
	public void onPrgMsgCommunityNews(String prio, String md5, String date, String title, String text, String link,	String type) {
	}

	@Override
	public void onPrgMsgCommunityNewsRemove(String type) {
	}

}
