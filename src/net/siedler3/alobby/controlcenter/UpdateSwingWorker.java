/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import javax.swing.SwingWorker;

import net.siedler3.alobby.controlcenter.download.AutoUpdate;

/**
 * Objekte dieser Klassen können das Downloaden des Updates im Hintergrund
 * anstoßen. Das ist erforderlich, damit die Swing Komponenten weiterhin
 * gezeichnet werden.
 * 
 * @author Knight Jim
 * @author Stephan Bauer (aka maximilius)
 */
public class UpdateSwingWorker extends SwingWorker<Boolean, Boolean> {

	private SettlersLobby settlersLobby;
	private boolean showMessageNoUpdateAvailable;
	
	/**
	 * Konstruktor
	 * @param settlersLobby
	 * 		Die SettlersLobby.
	 * @param showMessageNoUpdateAvailable
	 * 		Ob bei nicht vorhandensein eines neuen Updates eine Nachricht
	 * 		ausgegeben werden soll.
	 */
	public UpdateSwingWorker(SettlersLobby settlersLobby, boolean showMessageNoUpdateAvailable) {
		this.settlersLobby = settlersLobby;
		this.showMessageNoUpdateAvailable = showMessageNoUpdateAvailable;
	}

    @Override
    protected Boolean doInBackground() throws Exception
    {
        // der Returnwert wird nirgendwo abgefragt,
        // Hauptsache das updaten wird durchgeführt
        new AutoUpdate(settlersLobby).updateProgramIfNecessary(null, showMessageNoUpdateAvailable);
        return true;  
    }
    
}
