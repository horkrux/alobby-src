/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter.state;

/**
 * Object to represent one user who is in an open or running game. 
 */
public class UserInGame
{

    private String nick;
    private boolean isJoining = false;
    private boolean isValid = true;
    private boolean isInChatVerified = false;
    private String race = "";
	private String ip = "";
    
	/**
	 * Add users nickname who is not join atm.
	 * 
	 * @param nick	the nickname
	 */
    public UserInGame(String nick)
    {
        this(nick, false);
    }
    
    /**
     * Add users nickname with marker if he is joining or not.
     * 
     * @param nick		the nickname
     * @param isJoining	true if he is joining atm
     */
    public UserInGame(String nick, boolean isJoining)
    {
        super();
        this.nick = nick;
        this.isJoining = isJoining;
    }
    
    /**
     * Return the nickname of the user.
     * 
     * @return
     */
    public String getNick()
    {
        return nick;
    }
    
    /**
     * Return marker if user is joining.
     * 
     * @return
     */
    public boolean isJoining()
    {
        return isJoining;
    }

    /**
     * Set marker if user is joining or not.
     *  
     * @param isJoining
     */
    public void setJoining(boolean isJoining)
    {
        this.isJoining = isJoining;
    }

    /**
     * zwei User sind gleich, wenn ihr Name gleich ist.
     * Diese Art der Gleichheit wird für die Listenoperationen
     * benötigt.
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof UserInGame)
        {
            return nick.toLowerCase().equals(((UserInGame)obj).nick.toLowerCase());
        }
        return super.equals(obj);
    }

    @Override
    public String toString()
    {
        return nick;
    }

    public boolean isValid()
    {
        return isValid;
    }

    public void setValid(boolean isValid)
    {
        this.isValid = isValid;
    }
    
    public boolean isInChatVerified()
    {
        return isInChatVerified;
    }

    public void setInChatVerified(boolean isInChatVerified)
    {
        this.isInChatVerified = isInChatVerified;
    }
    
    public void setRace( String race ) {
    	this.race = race;
    }

    public String getRace() {
    	return this.race;
    }

    /**
     * Set the ip of this user.
     * Only used for S4-games as we need this to start the game.
     * 
     * @param ip
     */
	public void setIp(String ip) {
		this.ip  = ip;
	}
	
	/**
     * Get the ip of this user.
     * Only used for S4-games as we need this to start the game.
     * 
     * @param ip
     */
	public String getIp() {
		return this.ip;
	}
    
}
