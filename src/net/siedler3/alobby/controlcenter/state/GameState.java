/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter.state;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.controlcenter.SaveGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.s3gameinterface.GameStarter.StartOptions;
import net.siedler3.alobby.util.httpRequests;


/**
 * Basisklasse für die Verwaltung der Zustände während des 
 * Startens eines Spieles.
 * Sämtliche Methoden sind {@code synchronized}, da sowohl der GUI
 * thread, der IRC thread, als auch die Observer vom gamestarter
 * auf das Objekt zugreifen und Veränderungen vornehmen.
 * die "onXXX" Methoden werden vom IRC aufgerufen, während die
 * "userXXX" Methoden von der GUI und den Observern angesteuert werden.
 * 
 * @author jim
 *
 */
abstract public class GameState
{
    protected UserInGameList userList;

    protected boolean isHost = false;
    protected SettlersLobby settlersLobby;
    protected UserInGame user;
    protected UserInGame host = null;
    
    protected GameState(SettlersLobby settlersLobby, UserInGame user)
    {
        this.user = user;
        this.settlersLobby = settlersLobby;
        this.userList = new UserInGameList();
    }
    
    protected GameState(SettlersLobby settlersLobby, UserInGameList userList,
            UserInGame user, UserInGame host)
    {
        super();
        this.userList = userList;
        this.settlersLobby = settlersLobby;
        this.user = user;
        this.host = host;
        this.isHost = (user == host);
    }

    public synchronized void onWantsToJoin(UserInGame user, String mapName)
    {
        
    }
    
    public synchronized void onJoin(String mapUserList, String mapName, String hostName)
    {
    	// if S4interface is available and the user does not host a game
    	if( !settlersLobby.config.isHideS4Interface() && settlersLobby.getOpenHostGame() == null ) {
    		// than check if the host of the joined game equals the given hostname
	    	if (host != null && host.getNick().equals(hostName))
	        {
	    		// than get the openGame-object to check if this is a S4-game
	    		OpenGame openGame = settlersLobby.getOpenGames().getOpenGame(hostName);
	    		if( openGame != null && openGame.getMisc() == MiscType.S4 ) {
	    			// update the userlist
		            String[] tokens = mapUserList.split(",");
		            for (String user : tokens)
		            {
		                if (!user.isEmpty())
		                {
		                    UserInGame userObject = new UserInGame(user);
		                    if(!isValid(mapName))
		                    {
		                        //der user hat eine andere map gestartet als wir?!?
		                        //entsprechend markieren
		                    	userObject.setValid(false); 
		                    }
		                    //Spieler einfügen/aktualisieren und farblich als "anwesend" markieren 
		                    userObject.setJoining(false);
		                    userList.updateUser(userObject, true);
		                }
		            }
		            Test.output("updateS4GamePlayerListInGui aaaa");
		            // update the gui
		            updateS4GamePlayerListInGui();
	    		}
	        }
    	}
    }

    public synchronized void onPart(UserInGame user, String mapName, String hostName)
    {
    	if( !settlersLobby.config.isHideS4Interface() && settlersLobby.getOpenHostGame() != null && settlersLobby.getOpenHostGame().getMisc() == MiscType.S4 ) {
    		// handle s4-leaving
    		userList.removeUser(user);
    		Test.output("updateS4GamePlayerListInGui bbbb");
    		updateS4GamePlayerListInGui();
    		settlersLobby.newCurrentPlayerNumber(settlersLobby.getOpenHostGame().getCurrentPlayer() - 1);
    	}
    }

    
    public synchronized void onJoin(UserInGame user, String mapName, String ip)
    {
    	if( !settlersLobby.config.isHideS4Interface() && settlersLobby.getOpenHostGame() != null && settlersLobby.getOpenHostGame().getMisc() == MiscType.S4 ) {
    		user.setIp(ip);
    		// handle s4-join
    		userList.updateUser(user, true);
    		settlersLobby.scheduleSendMapUserList();
    		Test.output("updateS4GamePlayerListInGui cccc");
    		updateS4GamePlayerListInGui();
    		// add the user to the openHostGame-userlist
    		settlersLobby.getOpenHostGame().addPlayer(user.getNick(), false);
    		settlersLobby.getOpenHostGame().setCurrentPlayer();
    	}
    }
    
    public synchronized void onPart(UserInGame user, String mapName)
    {
    	if( !settlersLobby.config.isHideS4Interface() && settlersLobby.getOpenHostGame() != null && settlersLobby.getOpenHostGame().getMisc() == MiscType.S4 ) {
    		boolean isChanged = userList.removeUser(user);
	        if (isChanged)
	        {
	        	Test.output("updateS4GamePlayerListInGui dddd");
	            updateS4GamePlayerListInGui();
	            if (isHost)
	            {
	                //wenn ein Spieler das Spiel verlässt, wird das vom
	                //Host an alle verteilt
	                sendBroadcastGameProgramMessage(
	                        ALobbyConstants.TOKEN_USER_XY_HAS_LEFT, user, mapName);
	                // prepare message to update info on webserver
	            	// -> responsive is not relevant for us here
	            	httpRequests httpRequests = new httpRequests();
	            	httpRequests.setUrl("https://lobby.siedler3.net/aLobbyRequests.html");
	            	httpRequests.addParameter("updategame", 1);
	            	httpRequests.addParameter("state", "open");
	            	httpRequests.addParameter("hostname", settlersLobby.getNick());
	            	httpRequests.addParameter("ip", settlersLobby.ipToUse());
	            	// no token-security for this request
	            	httpRequests.setIgnorePhpSessId(true);
	            	// send the list of players in this game as JSON
	    			List<UserInGame> users = getUserList().getUserList();
	    			JSONArray usersAsJSON = new JSONArray();
	    			for( int i = 0;i<users.size();i++ ) {
	    				JSONObject item = new JSONObject();
	    				item.put("name", users.get(i).getNick());
	    				usersAsJSON.put(item);
	    			}
	    			Test.output("playerlist: " + usersAsJSON.toString());
	    			httpRequests.addParameter("playerlist", usersAsJSON.toString());
	    			// send the player-count
	            	httpRequests.addParameter("playercount", users.size());
	            	try {
	            		httpRequests.doRequest();
	            	} catch (IOException e) {
	            	}
	            }
	        }
    	}
    }
    
    /**
     * Wird aufgerufen, wenn der Benutzer das Spiel gestartet
     * hat. Momentan wird dies nur vom Host ausgelöst, es 
     * soll aber eigentlich bei allen Usern ausgelöst werden,
     * sobald Spiel startet.
     */
    public synchronized void userHasStartedGame()
    {
    	
    }
    
    public synchronized void userHasStartedGame( StartOptions startOptions )
    {
    	
    	// do not send statistic-data if this is a random or save-game
    	if( startOptions.getMapName() == null || startOptions.getMapName().isEmpty() || startOptions.getMapName().indexOf(ALobbyConstants.LABEL_SAVE_GAME) != -1 || startOptions.getMapName().indexOf("RANDOM") != -1 ) {
    		return;
    	}
    	
    	// save on mapbase that this map was started
		String response = "";
		
		// check whether the md5 of the local file is the same as on mapbase
    	httpRequests httpRequests = new httpRequests();
    	// -> set some parameter
    	if( startOptions.isWiMo() ) {
    		httpRequests.addParameter("wimo", 1 );
    	}
    	else {
    		httpRequests.addParameter("wimo", 0 );
    	}
    	// -> set an md5-hash to unique identify the game
    	// ---> including mapname, goods and playercount
    	// ---> not including any dates (to prevent savegames) or ip's (for host-changes)
    	OpenGame openGame = settlersLobby.getOpenHostGame();
    	httpRequests.addParameter("md5", openGame.getMd5() );
    	// -> set the player-count
    	httpRequests.addParameter("player", openGame.getCurrentPlayer() );
    	// -> set the misc-value
    	httpRequests.addParameter("misc", openGame.getMisc().value() );
    	// -> ignore security-token
    	httpRequests.setIgnorePhpSessId(true);
    	// -> set the url
    	String url = "";
		try {
			// encode the searchstring and add it to the url
			url = settlersLobby.config.getDefaultGetMapStat() + URLEncoder.encode(startOptions.getMapName().replace(ALobbyConstants.PATH_USER + File.separator, "").replace(ALobbyConstants.PATH_S3MULTI + File.separator, ""), "UTF-8");
			httpRequests.setUrl(url);
			// -> get the response-String
			response = httpRequests.doRequest();
						
    		if( !httpRequests.isError() ) {
    			response = response.replace("\r\n", "").trim();
    		}	   
		} catch (UnsupportedEncodingException e2) {
			e2.getStackTrace();
		} catch (IOException e) {
			e.getStackTrace();
		}
    }
    
    /**
     * Wird aufgerufen, wenn der Benutzer im Minichat
     * angekommen ist. Entweder hat er selber ein Spiel
     * geöffnet, oder er ist einem Spiel beigetreten.
     */
    public synchronized void userHasJoinedGame()
    {
    }
    
    /**
     * wird aufgerufen, wenn der Spieler ein Spiel verlassen hat.
     * Momentan passiert es nur, wenn der Spieler S3 beendet.
     */
    public synchronized void userHasLeftGame( boolean s4game )
    {
        if( !settlersLobby.config.isHideTeamspeakWindow() ) {
            settlersLobby.getGUI().getTeamSpeakPanel().startLoading();
        }
        user.setJoining(false);
        userList.removeUser(user);
        if (!isHost && host != null)
        {
            //sende Nachricht an Host: "leaving"
            sendGameProgramMessage(ALobbyConstants.TOKEN_LEAVING, host);
        }
        //es geht von vorne los, user und userInGameList werden zurückgesetzt
        setGameState(new GameStateInLobby(settlersLobby, new UserInGame(user.getNick())), s4game);
    }
    
    /**
     * Returns Host-State
     */
    public boolean isHost() {
    	return this.isHost;
    }
    
    /**
     * Is called if user wants to join a game.
     * 
     * @param hostName 	nickname of the host
     * @param mapName	the map-name
     * @param s4game	marker if this is a s4-game or not
     */
    public synchronized void userWantsToJoinGame(String hostName, String mapName, boolean s4game)
    {
        userList = new UserInGameList(hostName, mapName);
        host = new UserInGame(hostName);

        user.setJoining(true);
        Test.output("My user: " + user.getNick());
        userList.updateUser(user, true);
        List<UserInGame> users = userList.getUserList();
        for (int i=0; i<users.size();i++) {
            Test.output("Other user: " + user.getNick());
        }
        //sende Nachricht an Host "joining"
        sendGameProgramMessage(ALobbyConstants.TOKEN_WANTS_TO_JOIN, host);

        //setze State auf GameStateJoining
        if( false == s4game ) {
        	setGameState(new GameStateJoining(settlersLobby, userList, user, host), false);
        }
    }
    
    /**
     * wird aufgerufen, wenn der Benutzer ein Spiel öffnen möchte.
     */
    public synchronized void userWantsToCreateGame(String mapName, boolean s4game)
    {
    	if (mapName.startsWith(ALobbyConstants.LABEL_SAVE_GAME))
    	{
    		Test.output("Previous map name: " + mapName);
            try
            {
            	String realMapName = SaveGame.getS3MapnameOfSaveGame(mapName,
						 settlersLobby.config.getGamePathS3(), 
						 settlersLobby.config.getGameDateTimeFormat());
				mapName = ALobbyConstants.LABEL_SAVE_GAME + File.separator + realMapName;
				Test.output("Real map name: " + mapName);
            }
            catch (Exception e) {
            	Test.outputException(e);
            }
    	}
        userList = new UserInGameList(user.getNick(), mapName);
        userList.updateUser(user, true);
        setGameState(new GameStateJoining(settlersLobby, userList, user, user), s4game);
    }
    
    /**
     * erzeugt eine Liste an GameProgramMessages, um die komplette MapUserList
     * zu übertragen. Bei 20 Teilnehmern reicht eventuell eine einzelne Nachricht
     * nicht mehr aus, daher wird eine Liste an Nachrichten zurückgegeben.
     * Nur wenn der User sowohl der Host als auch im Status 
     * {@code GameStateInMinichat} ist, wird
     * überhaupt etwas erzeugt, ansonsten ist die Liste immer leer.
     * 
     * @return Liste an GameProgramMessages, um die komplette MapUserList
     * zu übertragen. 
     */
    public synchronized List<String> getMapUserListGameProgramMessage()
    {
    	// get userlist if this is a s4-game
    	if( !settlersLobby.config.isHideS4Interface() && settlersLobby.getOpenHostGame() != null && settlersLobby.getOpenHostGame().getMisc() == MiscType.S4 ) {
    		List<String> result = new ArrayList<String>();
            int i = 0;
            int size = userList.getUserList().size();
            List<UserInGame> users = userList.getUserList();
            final String messageBegin = ALobbyConstants.TOKEN_MAP_USER_LIST + ";" + 
                                        user.getNick() + ";" + 
                                        userList.getMapName() + ";";
            //Verschlüsselung und messageBegin bei der maximalen Länge berücksichtigen
            final int maxLen = (450 * 6 / 8) - messageBegin.length();
            while (i < size)
            {
                StringBuilder sb = new StringBuilder();
                for (;i < size; i++)
                {
                	UserInGame user = users.get(i);
                    String nick = user.getNick();
                    if (sb.length() > maxLen)
                    {
                    	Test.output("Message too long, ignoring user: " + nick);
                        continue;
                    }
                    Test.output("Adding user: " + nick);
                    sb.append(nick);
                    sb.append(',');
                }
                if (sb.length() > 0)
                {
                    //das letzte Komma löschen
                    sb.deleteCharAt(sb.length() - 1);
                }
                String msg = messageBegin + sb.toString();
                result.add(msg);
            }
            Test.output("sende nachricht: " + result);
            return result;
    	}
        return Collections.emptyList();
    }
    
    protected void sendGameProgramMessage(String token, UserInGame recipient)
    {
        if (token.equals(ALobbyConstants.TOKEN_JOINED) ||
            token.equals(ALobbyConstants.TOKEN_WANTS_TO_JOIN) ||
            token.equals(ALobbyConstants.TOKEN_LEAVING))
        {
            settlersLobby.sendGameProgramMessage(token, recipient, userList.getMapName());
        }
    }
    
    protected void sendBroadcastGameProgramMessage(String token, UserInGame user, String mapName)
    {
        if (token.equals(ALobbyConstants.TOKEN_USER_XY_HAS_LEFT))
        {
            settlersLobby.sendBroadcastGameProgramMessage(
                    token, user.getNick(), mapName);
        }
    }

    protected void updateListInGui()
    {
        settlersLobby.showMapUserList(userList);
    }
    
    protected void updateS4GamePlayerListInGui()
    {
        settlersLobby.showS4GameUserList(userList);
    }
    
    protected boolean isValid(String mapName)
    {
    	//Test.output("Requested map name: " + userList.getMapName() + ", got map name: " + mapName);
        return userList.getMapName().equalsIgnoreCase(mapName);
    }
    
    protected void setGameState(GameState gameState, boolean s4game)
    {
        settlersLobby.setGameState(gameState);
        // show list in gui
        if( false != s4game ) {
        	Test.output("updateS4GamePlayerListInGui eeee");
        	gameState.updateS4GamePlayerListInGui();
        }
        else {
        	gameState.updateListInGui();
        }
    }
    
    /**
     * Liefert eine Liste der Namen, die aktuell im Spiel sind.
     * 
     * @return
     */
    public List<String> getNameList()
    {
    	return getNameList(false);
    }
    
    /**
     * Return list of users in game which are not joining the game.
     * 
     * @return
     */
    public List<String> getNameList( boolean checkJoining )
    {
        List<String> list = new ArrayList<String>();
        for (UserInGame user : userList.userList)
        {
        	if( checkJoining && !user.isJoining() ) {
        		list.add(user.getNick());
        	}
        	else if ( !checkJoining ) {
        		list.add(user.getNick());
        	}
        }
        return list;
    }
    
    /**
     * Setzt das isInMiniChat Attribut vom User mit dem übergebenen nick, sofern
     * der aktuelle Gamestate dies erlaubt.
     * 
     * @param nick
     * @param isInMiniChat
     */
    public synchronized void setIsInMiniChat(String nick, boolean isInMiniChat)
    {
        
    }

    /**
     * Liefer true falls der Spieler in einem Spiel ist.
     * @return true falls der Spieler in einem Spiel ist.
     */
    public boolean isInGame()
    {
        return false;
    }

    /**
     * Liefer true falls der Spieler im Minichat ist.
     * @return true falls der Spieler im Minichat ist.
     */
    public boolean isInMiniChat()
    {
        return false;
    }
    
    /**
     * Return host-username.
     */
    public UserInGame getHostUserName() {
    	return this.host;
    }

    public UserInGameList getUserList() {
    	return this.userList;
    }

	public void setHost(boolean isHost) {
		this.isHost = isHost;
	}
    
}
