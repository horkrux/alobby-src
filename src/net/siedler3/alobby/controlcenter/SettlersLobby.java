/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinBase;
import com.sun.jna.platform.win32.WinDef;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.platform.win32.WinNT.SECURITY_DESCRIPTOR_RELATIVE;
import com.sun.jna.ptr.IntByReference;

import net.siedler3.alobby.communication.IRCCommunicator;
import net.siedler3.alobby.communication.IrcUser;
import net.siedler3.alobby.communication.protocol.PrgMsgAwayMessageListener;
import net.siedler3.alobby.communication.protocol.PrgMsgCommunityNewsListener;
import net.siedler3.alobby.communication.protocol.PrgMsgGameStateListener;
import net.siedler3.alobby.communication.protocol.PrgMsgMessageHandler;
import net.siedler3.alobby.communication.protocol.PrgMsgOpenGameListener;
import net.siedler3.alobby.communication.protocol.PrgMsgRunningGameListener;
import net.siedler3.alobby.communication.protocol.PrgMsgStreamListener;
import net.siedler3.alobby.communication.protocol.PrgMsgVPNStateListener;
import net.siedler3.alobby.communication.protocol.ProgramMessageProtocol;
import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.controlcenter.download.AutoUpdate;
import net.siedler3.alobby.controlcenter.download.CheckAndDownloadMapFile;
import net.siedler3.alobby.controlcenter.download.DownloadActions;
import net.siedler3.alobby.controlcenter.state.GameState;
import net.siedler3.alobby.controlcenter.state.GameStateInLobby;
import net.siedler3.alobby.controlcenter.state.UserInGame;
import net.siedler3.alobby.controlcenter.state.UserInGameList;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.gui.LanguageDialog;
import net.siedler3.alobby.gui.LinkLabel;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.gui.StartFrame;
import net.siedler3.alobby.gui.additional.IngameMessage;
import net.siedler3.alobby.gui.additional.TableCellItalicRenderer;
import net.siedler3.alobby.gui.additional.AdventCalendar;
import net.siedler3.alobby.gui.additional.DefaultTableModelForCommunityNews;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.modules.base.ResultEvent;
import net.siedler3.alobby.modules.base.ResultEventListener;
import net.siedler3.alobby.modules.connect.IrcConnector;
import net.siedler3.alobby.modules.login.LoginFrame;
import net.siedler3.alobby.modules.register.RegisterFrame;
import net.siedler3.alobby.nativesupport.NativeFunctions;
import net.siedler3.alobby.nativesupport.WinFunctions;
import net.siedler3.alobby.s3gameinterface.AutoSaveTask;
import net.siedler3.alobby.s3gameinterface.AutoSaveTaskStateByKey;
import net.siedler3.alobby.s3gameinterface.CheckForS3Stats;
import net.siedler3.alobby.s3gameinterface.GameStarter;
import net.siedler3.alobby.s3gameinterface.GameStarterVanilla;
import net.siedler3.alobby.s3gameinterface.GameStarter.StartOptions;
import net.siedler3.alobby.s3gameinterface.GameStarterLauncher;
import net.siedler3.alobby.s3gameinterface.GameStarterThread;
import net.siedler3.alobby.s3gameinterface.getRaceTask;
import net.siedler3.alobby.s3gameinterface.ProcessWindowSupportHandler;
import net.siedler3.alobby.s3gameinterface.StopToolStoringTask;
import net.siedler3.alobby.util.ClipBoardListener;
import net.siedler3.alobby.util.CryptoSupport;
import net.siedler3.alobby.util.FileLockWrapper;
import net.siedler3.alobby.util.GameDataFile;
import net.siedler3.alobby.util.KeyBlockStarter;
import net.siedler3.alobby.util.S3Support;
import net.siedler3.alobby.util.TimerTaskCompatibility;
import net.siedler3.alobby.util.VpnStarter;
import net.siedler3.alobby.util.httpRequests;
import net.siedler3.alobby.util.s3sm.SaveManager;
import net.siedler3.alobby.util.s4support.S4Map;
import net.siedler3.alobby.util.s4support.S4Starter;
import net.siedler3.alobby.util.s4support.S4Support;
import net.siedler3.alobby.xmlcommunication.XMLCommunicator;

/**
 * Zentrale Steuereinheit der aLobby.
 *
 * @author Stephan Bauer (maximilius)
 * @author Jim
 * @author amopyr
 */
public class SettlersLobby implements ResultEventListener
{
    public static CommandLineOptions commandLineOptions;
    public static final ExecutorService executor = Executors.newCachedThreadPool();
    public static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    public static final Random random = new Random();
    public static String configDir = getUserDir();

    private final LoginFrame loginFrame;
    private final RegisterFrame registerFrame;
    private GameStarter gameStarter;
    private final GUI gui;
    private IRCCommunicator ircCommunicator;
    private IrcConnector ircConnector;
    private final OpenGameContainer openGames;
    private final RunningGameContainer runningGames;
    public final MessageHandler messageHandler;
    private AutoUpdate autoUpdate;
    public Configuration config = Configuration.getInstance();
    private boolean isConnectedWithServer;
    private OpenGame openHostGame;
    private OpenGame runningHostGame;
    private OpenGame runningGame;
    private OpenGame openJoinGame;
    private String hostUserNameForDecrementCommand;

    private String ip;
    private String vpnIp = null;
    private String OriginalIpVpn = null;
    private IpViaUrlRetriever ipViaUrl = null;
    private AwayStatus awayStatus = null;
    private GameState gameState = null;

    private boolean showUserJoinsAndLefts = false;
    private TimerTaskCompatibility ircSendMapUserListTask;
    private Thread ircSendMapUserListThread;
    private AutoSaveTask autosaveTask;
    private CheckForS3Stats checkForS3StatsTask = null;
    private StopToolStoringTask stopToolStoringTask;
    private getRaceTask getRaceTask;
    private boolean isIngameOptionsStandalone;

    private boolean showTime = false;

    public ALobbyTheme theme = new ALobbyTheme();
    public SaveManager saveManager;

    private Chatlog chatlog = null;
    private boolean logChat = false;

    public boolean is1366x768Supported = false;

    // ingame options
    private boolean autoSave;
    private boolean s3Stats;
    private int autoSaveTimer;
    private boolean stopStoringTools;
    private GameStarterThread gameStarterThread;

    private ProgramMessageProtocol protocol;

    private FileLockWrapper fileLock = new FileLockWrapper(new File(configDir, "alobby.lock"));

    private final String OPENVPN_EXE = "openvpn" + File.separator + "bin" + File.separator + "openvpn.exe";
    private final String OPENVPN_BIN_WRAPPER = "run_unix_openvpn";
    public final String OPENVPN_KILL_WRAPPER = "kill_unix_openvpn";
    public final String WINE_VERSION = NativeFunctions.getInstance().getWineVersion();
    public final boolean IS_WINE = WINE_VERSION != null;
    public boolean isS3Gog = false;
    public boolean isS4Gog = false;
    private boolean windowsFirewallOn = true;
    private String otherFirewall = null;
    private String antiVirus = null;
    private VpnStarter vpnStarter;
	private boolean playingTetris = false;
	private AutoSaveTaskStateByKey autoSaveTaskStateByKey = null;
	private ClipBoardListener clipBoardListener;
	private JCheckBox cbxS4BetaHint = null;

    /**
     * Konstruktor.
     * Initialisiert die alternative Lobby und zeigt den Einstellungsdialog an.
     */
    @SuppressWarnings("static-access")
	public SettlersLobby()
    {
    	// activate Logging
        SettlersLobby.writeInLogFile();
        
        // log Systeminfo
        logSystemInfo();

        // load the default aLobby-configuration
        if (!config.loadDefaultConfig()) {
            System.exit(1);
        }
        
        // configure wine-settings if wine is used to run aLobby
        if (IS_WINE) {
            config.adjustToWineDefaultSettings(WINE_VERSION);
        }
        
        // load the local aLobby-configuration
        if (!config.tryToLoadConfigOrDie(this)) {
            System.exit(1);
        }

        // select the theme before we show anything
        theme.configureColorsForSelectedTheme();
        
        // print out system-properties, only for beta to check differences
        if( this.isBetaUsed() ) {
	        Properties pr = System.getProperties();
	        Enumeration<?> keys = pr.keys();
	        String props = "";
	        while (keys.hasMoreElements()) {
	            String key = (String)keys.nextElement();
	            String value = (String)pr.get(key);
	            props = props + "\n" + key + ": " + value;
	            Test.output(key + ": " + value);
	        }
        }
                
        // generate and show the loading-frame
        StartFrame sF = new StartFrame(this, 7, I18n.getString("SettlersLobby.STARTFRAME_3")); //$NON-NLS-1$
        
        // set the loading-frame on configured screen
        this.showOnSpecifiedScreen(this.config.getScreen(), (Window)sF);

        // check if language has been set
        if (config.isLanguageSet()) {
            //We can't ask the user here to set the language, as we may be
        	//running here as an second instance - so we only set the language
        	//here, if it is already set.
        	I18n.setLanguage(config.getLanguage());
        }
        
        // create named pipe on windows, not on wine, to communicate from external apps with the aLobby
        if( !IS_WINE ) {
	        executor.execute(new Runnable() {
	        	@Override
	        	public void run()
	        	{
	        		createNamedPipe();
	        	}
	        });
        }
        
        // Get the reported Windows NT version and the real one (from Registry).
        double realWinNtVersion = ReqQuery.getNtVer();
        double reportedWinNtVersion = Double.parseDouble(System.getProperty("os.version"));
        
        // Check, if the real Windows NT version is one, that we support
        if( !IS_WINE && realWinNtVersion != 0.0 && realWinNtVersion < ALobbyConstants.MIN_WINNT_VERSION ) {
        	// it's not a supported Windows-Version
        	// -> show hint to user and close aLobby
        	MessageDialog messagewindowVersionHint = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
        	// -> add message
        	messagewindowVersionHint.addMessage(
    			"<html>" + I18n.getString("SettlersLobby.ERROR_MIN_WINNT_VERSION") + "</html>", 
    			UIManager.getIcon("OptionPane.errorIcon")
    		);
    		// -> add ok-button
        	messagewindowVersionHint.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	System.exit(1);
    			    }
    			}
    		);
    		// show it
        	messagewindowVersionHint.setVisible(true);
        }
        
        // Check, if the reported Windows NT version is the same like
        // the real one (or higher, in case a user did upgrade it's Windows).
        if (!IS_WINE && realWinNtVersion != 0.0 && realWinNtVersion > reportedWinNtVersion) {
        	Test.output("The system reports to be: NT " + reportedWinNtVersion + ", but is in fact: NT: " + realWinNtVersion + "!");
        	// The user is running the aLobby in a compatibility mode... damn.
            JOptionPane.showMessageDialog(sF,
            		I18n.getString("SettlersLobby.ERROR_COMPATIBILITY_ACTIVE"),
        			I18n.getString("SettlersLobby.ERROR"), //$NON-NLS-1$
        			JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        } else {
        	Test.output("The system reports to be: NT " + System.getProperty("os.version"));
        }
        
        // load the text-color of actually used theme as Color-Object
    	String hexcolor = getHexColorStringByColor(theme.getTextColor());
    	
        // show a hint if the user has a beta-version
        // -> not if the hint was already shown this day
        // -> not if the hint is disabled by commandline-argument
        if( false != isBetaUsed() && false == config.getPreventDailyBetaHint() && false == commandLineOptions.disableBetaHint() ) {
        	MessageDialog messagewindowBetaHint = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
        	// -> add message
        	messagewindowBetaHint.addMessage(
    			MessageFormat.format("<html>" + I18n.getString("SettlersLobby.INFO_BETA_VERSION") + "</html>", hexcolor), 
    			UIManager.getIcon("OptionPane.informationIcon")
    		);
    		// -> add ok-button
        	messagewindowBetaHint.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	config.setPreventDailyBetaHint(true);
    			    	messagewindowBetaHint.dispose();
    			    }
    			},
    			addActionListenerForMessageWindowButton(messagewindowBetaHint)
    		);
    		// show it
        	messagewindowBetaHint.setVisible(true);
        }
        
        // show a hint if the user does not have installed any supported S3- or S4-edition
        // but the history edition
        if( ( false != ReqQuery.isS3HistoryEditionInstalled() || false != ReqQuery.isS4HistoryEditionInstalled() ) && config.getGamePathS4().isEmpty() && (config.getGamePathS3() == null || config.getGamePathS3().isEmpty())) {
        	MessageDialog messagewindowHEHint = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
        	// -> add message
        	messagewindowHEHint.addMessage(
    			I18n.getString("SettlersLobby.INFO_HE"), 
    			UIManager.getIcon("OptionPane.informationIcon")
    		);
    		// -> add ok-button
        	messagewindowHEHint.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	messagewindowHEHint.dispose();
    			    }
    			},
    			addActionListenerForMessageWindowButton(messagewindowHEHint)
    		);
    		// show it
        	messagewindowHEHint.setVisible(true);
        }
        
        // show a hint if the user has S4 installed
        // -> not if the hint was already show in this version
        // TODO remove this with release of aLobby 4.0
        if( !config.getGamePathS4().isEmpty() && false == config.getPreventWeeklyS4BetaHint() && false == config.isPreventS4BetaHint() ) {
        	MessageDialog messagewindowS4BetaHint = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
        	// -> add message
        	messagewindowS4BetaHint.addMessage(
    			MessageFormat.format("<html>" + I18n.getString("SettlersLobby.INFO_S4_INTERFACE") + "</html>", hexcolor), 
    			UIManager.getIcon("OptionPane.informationIcon")
    		);
        	cbxS4BetaHint = messagewindowS4BetaHint.addPanelWithLabelAndCheckbox(I18n.getString("SettlersLobby.INFO_S4_INTERFACE_DISABLING"), "");
    		// -> add ok-button
        	messagewindowS4BetaHint.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	config.setPreventWeeklyS4BetaHint(true);
   			    		config.setPreventS4BetaHint(cbxS4BetaHint.isSelected());
    			    	messagewindowS4BetaHint.dispose();
    			    }
    			},
    			addActionListenerForMessageWindowButton(messagewindowS4BetaHint)
    		);
    		// show it
        	messagewindowS4BetaHint.setVisible(true);
        }
        
        // if its Christmas-time 
        // the actual theme is not the Christmas-theme
        // and the prevent is not active
        // and this window was not shown before
        // than show a hint about new theme and Christmas-time
        if( !config.getPreventChristmasHint() && !config.getPreventAutomaticChristmasTheme() && config.isChristmasTheme() && config.itsChristmastime() ) {
        	// and show a hint about this new configuration
	        try {
	        	// add gfx before message
	        	// source (license: public domain): http://www.clipartbest.com/clipart-RiAK9a6iL
	        	InputStream imgstream = SettlersLobby.class.getResourceAsStream(ALobbyConstants.PACKAGE_PATH + "/gui/img/weihnachtsmann.png");
				BufferedImage imgio = ImageIO.read(imgstream);
				ImageIcon img = new ImageIcon(imgio);
				// generate messagewindow
	        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
	        	// -> add message
	    		messagewindow.addMessage(
	    			I18n.getString("ALobbyTheme.CHRISTMASTIME"), 
	    			img
	    		);
	    		// -> add ok-button
	    		messagewindow.addButton(
	    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	    			new ActionListener() {
	    			    @Override
	    			    public void actionPerformed(ActionEvent e) {
	    			    	config.setPreventChristmasHint(true);
	    			    	messagewindow.dispose();
	    			    }
	    			},
	    			addActionListenerForMessageWindowButton(messagewindow)
	    		);
	    		// show it
	    		messagewindow.setVisible(true);
        	}
        	catch(Exception e) {
        		// error-part
        	}
        }
        
        // if its Halloween-time 
        // the actual theme is not the halloween-theme
        // and the prevent is not active
        // and this window was not shown before
        // than show a hint about new theme and Christmas-time
        if( !config.getPreventHalloweenHint() && !config.getPreventAutomaticHalloweenTheme() && config.isHalloweenTheme() && config.itsHalloweentime() ) {
        	// and show a hint about this new configuration
	        try {
	        	// add gfx before message
	        	// source (license: public domain): http://www.clipartbest.com/clipart-nTEzykbTA
	        	InputStream imgstream = SettlersLobby.class.getResourceAsStream(ALobbyConstants.PACKAGE_PATH + "/gui/img/kuerbis_big.png");
				BufferedImage imgio = ImageIO.read(imgstream);
				ImageIcon img = new ImageIcon(imgio);
				// generate messagewindow
	        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
	        	// -> add message
	    		messagewindow.addMessage(
	    			I18n.getString("ALobbyTheme.HALLOWEENTIME"), 
	    			img
	    		);
	    		// -> add ok-button
	    		messagewindow.addButton(
	    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	    			new ActionListener() {
	    			    @Override
	    			    public void actionPerformed(ActionEvent e) {
	    			    	config.setPreventHalloweenHint(true);
	    			    	messagewindow.dispose();
	    			    }
	    			},
	    			addActionListenerForMessageWindowButton(messagewindow)
	    		);
	    		// show it
	    		messagewindow.setVisible(true);
        	}
        	catch(Exception e) {
        		// error-part
        	}
        }
        
        // if its easter-time
        // and the actual theme is not the easer-theme
        if( !config.getPreventEasterHint() && !config.getPreventAutomaticEasterTheme() && config.isEasterTheme() && config.itsEastertime() ) {
        	// and show a hint about this new configuration
	        try {
	        	// add gfx before message
	        	InputStream imgstream = SettlersLobby.class.getResourceAsStream(ALobbyConstants.PACKAGE_PATH + "/gui/img/bunny.png");
				BufferedImage imgio = ImageIO.read(imgstream);
				ImageIcon img = new ImageIcon(imgio);
				// generate messagewindow
	        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
	        	// -> add message
	    		messagewindow.addMessage(
	    			I18n.getString("ALobbyTheme.EASTERTIME"), 
	    			img
	    		);
	    		// -> add ok-button 
	    		messagewindow.addButton(
	    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	    			new ActionListener() {
	    			    @Override
	    			    public void actionPerformed(ActionEvent e) {
	    			    	config.setPreventEasterHint(true);
	    			    	messagewindow.dispose();
	    			    }
	    			},
	    			addActionListenerForMessageWindowButton(messagewindow)
	    		);
	    		// show it
	    		messagewindow.setVisible(true);
        	}
        	catch(Exception e) {
        		// error-part
        	}
        }

		if (!fileLock.aquireLock())
		{
			// use MessageDialog to show a hint: lobby is already running
			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
        	messagewindow.addMessage(
        		I18n.getString("SettlersLobby.ERROR_INSTANCE_ALREADY_RUNNING"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
        	messagewindow.addButton(
        		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
        		new ActionListener() {
        		    @Override
        		    public void actionPerformed(ActionEvent e) {
        		        System.exit(0);
        		    }
        		}
        	);
        	messagewindow.setVisible(true);
        	System.exit(1);
		}

        sF.setNewStep(I18n.getString("SettlersLobby.STARTFRAME_6")); //$NON-NLS-1$

        if (!config.isLanguageSet()) {
        	// set language to system-language if available
        	if( System.getProperty("user.language").length() > 0 && System.getProperty("user.country").length() > 0 ) {
        		Locale locale = new Locale(System.getProperty("user.language"));
        		config.setLanguage(locale);
        		I18n.setLanguage(config.getLanguage());
        	}

        	// show the language-choose-dialog
            JDialog languageDialog = new LanguageDialog(config, this);
            languageDialog.setVisible(true);

            //nochmal setVisible() aufrufen, ansonsten
            //is sF nicht zu sehen, warum auch immer
            sF.setVisible(true);
            
            // reset language
            I18n.setLanguage(config.getLanguage());
        }

        String alobbyExePath = SettlersLobby.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        try {
            alobbyExePath = URLDecoder.decode(alobbyExePath, "UTF-8");
            alobbyExePath = new File(alobbyExePath).getAbsolutePath();
            Test.output("The path to the alobby.exe is: " + alobbyExePath);
            if (!IS_WINE && ReqQuery.isAppCompatSetForAlobbyExe(alobbyExePath)) {
                // The user is running the aLobby in a compatibility mode... damn.
                JOptionPane.showMessageDialog(sF,
                        I18n.getString("SettlersLobby.ERROR_COMPATIBILITY_ACTIVE"),
                        I18n.getString("SettlersLobby.ERROR"), //$NON-NLS-1$
                        JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            }
        } catch (UnsupportedEncodingException e1) {
            Test.outputException(e1);
        }

        if (NativeFunctions.getInstance().isUserWindowsAdmin())
        {
        	Test.output("Program running with administrator privileges");
        }
        else 
        {
        	Test.output("Program running WITHOUT administrator privileges");
        	boolean success = checkAccessRights();

        	// Only do the following, if one of the above checks failed.
        	if (!success) 
        	{
                JOptionPane.showMessageDialog(sF,
            			I18n.getString("SettlersLobby.ERROR_NOT_ADMIN"),
            			I18n.getString("SettlersLobby.ERROR"), //$NON-NLS-1$
            			JOptionPane.ERROR_MESSAGE);
                System.exit(1);
        	}
        }
        
        // check for free space for drives which are used by the aLobby, Settlers 3
        // and Settlers 4 and warn the user if there is less than 1 GB free space 
        // on one of the required drives left
        if (!IS_WINE)
        {
        	
        	int s3gameDirDriveFreeSpace = 1000;
        	int s4gameDirDriveFreeSpace = 1000;
        	
        	// get the drive where the configDir is located
	        Path p = Paths.get(SettlersLobby.configDir);
	        String configDirDrive = p.getRoot().toString();
	        File drive = new File(configDirDrive);
	        
	        // get the drive where the s3.exe is located
	        if( isS3GamePathValid() ) {
	        	Path p2 = Paths.get(config.getGamePathS3());
	        	String s3gameDirDrive = p2.getRoot().toString();
	        	File drive2 = new File(s3gameDirDrive);
	        	s3gameDirDriveFreeSpace = (int) (drive2.getFreeSpace() / 1024 / 1024);
	        }
	        
	        // get the drive where the s4.exe is located
	        if( isS4GamePathValid() ) {
	        	Path p3 = Paths.get(config.getGamePathS4());
	        	String s4gameDirDrive = p3.getRoot().toString();
	        	File drive3 = new File(s4gameDirDrive);
	        	s4gameDirDriveFreeSpace = (int) (drive3.getFreeSpace() / 1024 / 1024);
	        }
	        
	        // calculate the free space of the alobby-drive in MB
	        int configDirDriveFreeSpace = (int) (drive.getFreeSpace() / 1024 / 1024);
	        
	        // if one drive has less than 1 GB free space, show a hint
	        if( configDirDriveFreeSpace < 1000 || s3gameDirDriveFreeSpace < 1000 || s4gameDirDriveFreeSpace < 1000 ) {
	        	// use MessageDialog to show a hint: lobby is already running
				MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
	        	messagewindow.addMessage(
	        		I18n.getString("SettlersLobby.WARN_FREE_SPACE"),
	        		UIManager.getIcon("OptionPane.errorIcon")
	        	);
	        	messagewindow.addButton(
	        		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	        		new ActionListener() {
	        		    @Override
	        		    public void actionPerformed(ActionEvent e) {
	        		    	sF.setVisible(true);
	        		    	messagewindow.dispose();
	        		    }
	        		},
	    			addActionListenerForMessageWindowButton(messagewindow)
	        	);
	        	messagewindow.setVisible(true);
	        }
        }

        // Check, if the Windows Firewall is ON
		try
		{
			if (!IS_WINE) {
				if (reportedWinNtVersion > 5.1) {
					// Vista or newer
					Process process = new ProcessBuilder("netsh", "advfirewall", "show", "currentprofile").start();
					process.waitFor(4, TimeUnit.SECONDS);
					InputStream inStream = process.getInputStream();
					InputStreamReader inputReader = new InputStreamReader(inStream,"utf-8");
					BufferedReader buffReader = new BufferedReader(inputReader);
					String[] lines = null;
					String line = null;
					while (buffReader.ready()) {
						line = buffReader.readLine();
						lines = null;
						// English Windows
						if (line.startsWith("State"))
						{
							lines = line.split("State");
						}
						// German Windows
						else if (line.startsWith("Status")) {
							lines = line.split("Status");
						}
                        // Italian Windows
                        else if (line.startsWith("Stato")) {
                            lines = line.split("Stato");
                        }
						if (lines != null) {
							line = lines[lines.length-1].trim();
							if (line.contains("ON") || line.contains("EIN")) {
								windowsFirewallOn = true;
								Test.output("Windows Firewall is ON!");
							} else if (line.contains("OFF") || line.contains("AUS")){
								windowsFirewallOn = false;
								Test.output("Windows Firewall is OFF!");
							} else {
								Test.output("Windows Firewall is in an unknown state: " + line);
							}
							break;
						}
					}
					buffReader.close();
				} else {
					// XP
					Process process = new ProcessBuilder("netsh", "firewall", "show", "state").start();
					process.waitFor(4, TimeUnit.SECONDS);
					InputStream inStream = process.getInputStream();
					InputStreamReader inputReader = new InputStreamReader(inStream,"utf-8");
					BufferedReader buffReader = new BufferedReader(inputReader);
					String[] lines = null;
					String line = null;
					while (buffReader.ready()) {
						line = buffReader.readLine();
						lines = null;
						// The German Windows version uses localized texts here...
						// All other Windows languages (tested with all major European languages),
						// use English here.
						if (line.startsWith("Operational mode") || line.startsWith("Betriebsmodus")) {
							lines = line.split("=");
							line = lines[lines.length-1].trim();
							if (line.contains("Enable") || line.contains("Aktiv")) {
								windowsFirewallOn = true;
								Test.output("Windows Firewall is ON");
							} else if (line.contains("Disable") || line.contains("Inaktiv")) {
								windowsFirewallOn = false;
								Test.output("Windows Firewall is OFF!");
							} else {
								Test.output("Windows Firewall is in an unknown state: " + line);
							}
							break;
						}
					}
					buffReader.close();
				}
			}
		} catch (Exception e) {
			Test.outputException(e);
		}
		
        // Check, if there is any other Firewall product installed
		// (breaks off after finding one).
		// This will not work on Windows Server editions!
		try
		{
			if (!IS_WINE) {
				Process process;
				if (reportedWinNtVersion > 5.1) {
					// Vista or newer
					process = new ProcessBuilder("WMIC", "/Node:localhost", "/Namespace:\\\\root\\SecurityCenter2", "Path", "FirewallProduct", "Get", "displayName", "/Format:List").start();

				} else {
					// XP
					process = new ProcessBuilder("cmd", "/c", "WMIC /Node:localhost /Namespace:\\\\root\\SecurityCenter Path FirewallProduct Get displayName /Format:List <NUL ").start();
				}
				if (process.waitFor(5, TimeUnit.SECONDS))
					{
					InputStream inStream = process.getInputStream();
					InputStreamReader inputReader = new InputStreamReader(inStream,"utf-8");
					BufferedReader buffReader = new BufferedReader(inputReader);
					while (buffReader.ready()) {
						String line = buffReader.readLine();
						if (line.startsWith("displayName=")) {
							String[] lines = line.split("displayName=");
							otherFirewall = lines[lines.length-1].trim();
							Test.output("Detected a 3rd party Firewall: " + otherFirewall);
							break;
						}
					}
					buffReader.close();
				}
			}
		} catch (Exception e) {
			Test.outputException(e);
		}

		if (!IS_WINE) {
			if (reportedWinNtVersion == 5.1 && otherFirewall == null) {
				if (windowsFirewallOn) {
	                // print warn-message including clickable link
					MessageDialog message = new MessageDialog(null, I18n.getString("SettlersLobby.WARN"), true);
					message.addMessage("<html>" + MessageFormat.format(I18n.getString("SettlersLobby.WARN_NO_FIREWALL_XP") + "</html>",hexcolor), UIManager.getIcon("OptionPane.warningIcon"));
					message.addButton(
						I18n.getString("SettlersLobby.CLOSE"),
		        		new ActionListener() {
		        		    @Override
		        		    public void actionPerformed(ActionEvent e) {
		        		    	// close aLobby
		        		    	System.exit(0);
		        		    }
		        		}
		        	);
					message.addButton(
		        		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
		        		new ActionListener() {
		        		    @Override
		        		    public void actionPerformed(ActionEvent e) {
		        		    	// simply close the window with warn-message
		        		        message.dispose();
		        		    }
		        		}
		        	);
					message.setVisible(true);
				} else {
	                // print warn-message including clickable link
					MessageDialog message = new MessageDialog(null, I18n.getString("SettlersLobby.WARN"), true);
					message.addMessage("<html>" + MessageFormat.format(I18n.getString("SettlersLobby.WARN_NO_REAL_FIREWALL_XP") + "</html>",hexcolor), UIManager.getIcon("OptionPane.warningIcon"));
					message.addButton(
						I18n.getString("SettlersLobby.CLOSE"),
		        		new ActionListener() {
		        		    @Override
		        		    public void actionPerformed(ActionEvent e) {
		        		    	// close aLobby
		        		    	System.exit(0);
		        		    }
		        		}
		        	);
					message.addButton(
		        		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
		        		new ActionListener() {
		        		    @Override
		        		    public void actionPerformed(ActionEvent e) {
		        		    	// simply close the window with warn-message
		        		        message.dispose();
		        		    }
		        		}
		        	);
					message.setVisible(true);
				}
			} else if (reportedWinNtVersion > 5.1 && otherFirewall == null) {
				if (!windowsFirewallOn) {
					// print warn-message including clickable link
					MessageDialog message = new MessageDialog(null, I18n.getString("SettlersLobby.WARN"), true);
					message.addMessage("<html>" + MessageFormat.format(I18n.getString("SettlersLobby.WARN_FIREWALL_DISABLED") + "</html>",hexcolor), UIManager.getIcon("OptionPane.warningIcon"));
					message.addButton(
						I18n.getString("SettlersLobby.CLOSE"),
		        		new ActionListener() {
		        		    @Override
		        		    public void actionPerformed(ActionEvent e) {
		        		    	// close aLobby
		        		    	System.exit(0);
		        		    }
		        		}
		        	);
					message.addButton(
		        		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
		        		new ActionListener() {
		        		    @Override
		        		    public void actionPerformed(ActionEvent e) {
		        		    	// simply close the window with warn-message
		        		        message.dispose();
		        		    }
		        		}
		        	);
					message.setVisible(true);
				}
			} else if (otherFirewall != null) {
				if (reportedWinNtVersion > 5.1 && config.isWarnAboutThirdPartyFirewall()) {
	                // print warn-message including clickable link
					MessageDialog message = new MessageDialog(null, I18n.getString("SettlersLobby.WARN") + "; Firewall: " + otherFirewall, true);
					message.addMessage("<html>" + MessageFormat.format(I18n.getString("SettlersLobby.WARN_WINDOWS_FIREWALL_DISABLED") + "</html>",hexcolor), UIManager.getIcon("OptionPane.warningIcon"));
					message.addButton(
						I18n.getString("SettlersLobby.CLOSE"),
		        		new ActionListener() {
		        		    @Override
		        		    public void actionPerformed(ActionEvent e) {
		        		    	// close aLobby
		        		    	System.exit(0);
		        		    }
		        		}
		        	);
					message.addButton(
		        		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
		        		new ActionListener() {
		        		    @Override
		        		    public void actionPerformed(ActionEvent e) {
		        		    	// simply close the window with warn-message
		        		        message.dispose();
		        		    }
		        		}
		        	);
					message.setVisible(true);
				}
				if (reportedWinNtVersion == 5.1) {
					if (windowsFirewallOn) { 
		                // print warn-message including clickable link
						MessageDialog message = new MessageDialog(null, I18n.getString("SettlersLobby.WARN") + "; Firewall: " + otherFirewall, true);
						message.addMessage("<html>" + MessageFormat.format(I18n.getString("SettlersLobby.WARN_WINDOWS_FIREWALL_ENABLED_XP") + "</html>",hexcolor), UIManager.getIcon("OptionPane.warningIcon"));
						message.addButton(
							I18n.getString("SettlersLobby.CLOSE"),
			        		new ActionListener() {
			        		    @Override
			        		    public void actionPerformed(ActionEvent e) {
			        		    	// close aLobby
			        		    	System.exit(0);
			        		    }
			        		}
			        	);
						message.addButton(
			        		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
			        		new ActionListener() {
			        		    @Override
			        		    public void actionPerformed(ActionEvent e) {
			        		    	// simply close the window with warn-message
			        		        message.dispose();
			        		    }
			        		}
			        	);
						message.setVisible(true);
					} else if (config.isWarnAboutThirdPartyFirewall()) {
		                // print warn-message including clickable link
						MessageDialog message = new MessageDialog(null, I18n.getString("SettlersLobby.WARN") + "; Firewall: " + otherFirewall, true);
						message.addMessage("<html>" + MessageFormat.format(I18n.getString("SettlersLobby.WARN_WINDOWS_FIREWALL_DISABLED_XP") + "</html>",hexcolor), UIManager.getIcon("OptionPane.warningIcon"));
						message.addButton(
							I18n.getString("SettlersLobby.CLOSE"),
			        		new ActionListener() {
			        		    @Override
			        		    public void actionPerformed(ActionEvent e) {
			        		    	// close aLobby
			        		    	System.exit(0);
			        		    }
			        		}
			        	);
						message.addButton(
			        		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
			        		new ActionListener() {
			        		    @Override
			        		    public void actionPerformed(ActionEvent e) {
			        		    	// simply close the window with warn-message
			        		        message.dispose();
			        		    }
			        		}
			        	);
						message.setVisible(true);
					}
				}
			}
		}
		
        // Check, if there is any Antivirus product installed
		// (breaks off after finding one).
		// This will not work on Windows Server editions!
		try
		{
			if (!IS_WINE) {
				Process process;
				if (reportedWinNtVersion > 5.1) {
					// Vista or newer
					process = new ProcessBuilder("WMIC", "/Node:localhost", "/Namespace:\\\\root\\SecurityCenter2", "Path", "AntiVirusProduct", "Get", "displayName,productState", "/Format:List").start();

				} else {
					// XP
					process = new ProcessBuilder("cmd", "/c", "WMIC /Node:localhost /Namespace:\\\\root\\SecurityCenter Path AntiVirusProduct Get displayName,productUptoDate /Format:List <NUL ").start();
				}
				if (process.waitFor(5, TimeUnit.SECONDS))
				{
					InputStream inStream = process.getInputStream();
					InputStreamReader inputReader = new InputStreamReader(inStream,"utf-8");
					BufferedReader buffReader = new BufferedReader(inputReader);
					while (buffReader.ready()) {
						String line = buffReader.readLine();
						if (line.startsWith("displayName=")) {
							String[] lines = line.split("displayName=");
							antiVirus = lines[lines.length-1].trim();
							Test.output("Detected an Antivirus: " + antiVirus);
							break;
						}
					}
					if (antiVirus == null) {
						Test.output("No installed Antivirus detected!");
                        MessageDialog message = new MessageDialog(null, I18n.getString("SettlersLobby.WARN"), true);
                        
                        if (reportedWinNtVersion <= 6.1) {
                            // Win7 and older
                            message.addMessage("<html>" + MessageFormat.format(I18n.getString("SettlersLobby.WARN_NO_ANTIVIRUS_OLD_WINDOWS") + "</html>",hexcolor), UIManager.getIcon("OptionPane.warningIcon"));
                        } else {
                            // Win8 and newer
                            message.addMessage("<html>" + MessageFormat.format(I18n.getString("SettlersLobby.WARN_NO_ANTIVIRUS_NEW_WINDOWS") + "</html>",hexcolor), UIManager.getIcon("OptionPane.warningIcon"));
                        }
                        message.addButton(
                            UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
                            new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    // simply close the window with warn-message
                                    message.dispose();
                                }
                            },
        	    			addActionListenerForMessageWindowButton(message)
                        );
                        message.setVisible(true);
					}
					buffReader.close();
				}
			}
		} catch (Exception e) {
			Test.outputException(e);
		}

        // DxDiag can log useful data about the hardware and software
        // (including e.g. the information about the DPI setting).
        // Since dxdiag returns before the log file has been written,
        // the data logged in the log.txt will usually be from be the run
        // before the last run...
		if( false == commandLineOptions.disableDxDebug() ) {
			try
			{
				File dxDiagLog = new File(SettlersLobby.configDir, "dxdiag.log");
				Test.output("Running dxdiag...");
				Process process;
				if (reportedWinNtVersion > 5.1 || IS_WINE) {
					// Vista or newer
					process = new ProcessBuilder("dxdiag.exe", "/t", dxDiagLog.getAbsolutePath()).start();
				} else {
					process = new ProcessBuilder("cmd", "/c", "dxdiag.exe /t " +  dxDiagLog.getAbsolutePath() + " <NUL ").start();
				}
				process.waitFor(5, TimeUnit.SECONDS);
				if (dxDiagLog.exists()) {
					List<String> lines = Files.readAllLines(dxDiagLog.toPath(), StandardCharsets.ISO_8859_1);
					StringBuilder sb = new StringBuilder();
					for (String s : lines) {
						sb.append(s);
						sb.append("\r\n");
					}
					Test.output(sb.toString());
				}
			} catch (Exception e) {
				Test.outputException(e);
			}
		}

        // Below is also needed to make the AutoHotKey work!
        is1366x768Supported = S3Support.isResolutionSupported("1366x768");
        if (!is1366x768Supported) {
            Test.output("1366x768 resolution is not support on this system!");
            if (ReqQuery.getS3Resolution() == 0) {
                // Resolution was set to the unsupported resolution - resetting it to 1024x768 now
                ReqQuery.setS3Resolution(2);
            }
        }

		/**
		 * Check if the user has a GOG-Version of Settlers 3.
		 */
        isS3Gog = false;
        if( isS3GamePathValid() )
        {
        	isS3Gog = S3Support.isGog(getS3Directory().toString());
        	// GogSupport.makeS3ALobbyExe(config.getGamePathS3(), this);
        } 
        else if (config != null && config.getGamePathS3() != null) {
        	// Try to rescue the situation, where s3_alobby.exe is
        	// set in the settings, but does not exist as a file.
            File file = new File(config.getGamePathS3());
            File s3Dir = file.getParentFile();
            if (s3Dir.exists() && s3Dir.isDirectory()) {
            	isS3Gog = S3Support.isGog(getS3Directory().toString());
            	S3Support.makeS3ALobbyExe(config.getGamePathS3(), this);
            }
            else {
            	// Ok, the S3 dir does also not exist, so not that much to do
            	// here...
            	isS3Gog = S3Support.isGog();
            }
        }
        else {
        	isS3Gog = S3Support.isGog();
        }

        if (isS3GamePathValid())
        {
            if (isS3Gog)
            {
            	Test.output("S3 is a GOG version");
            } else {
            	Test.output("S3 is not a GOG version");
            }
            File s3dir = ReqQuery.getS3ExeDirectory();
            Test.output("S3 is installed at: " + s3dir.getAbsolutePath());
        } else {
            Test.output("S3 is not or not correctly installed");
        }

        /**
         * Check if the user has a GOG-Version of Settlers 4.
         */
        isS4Gog = false;
        try
        {
            File s4Exedir = ReqQuery.getS4ExeDirectory();
            if (s4Exedir != null && s4Exedir.exists() && s4Exedir.isDirectory())
            {
            	isS4Gog = S4Support.isGog(s4Exedir.getAbsolutePath());
            	// S4Support.makeS4ALobbyExe(s4Exedir.getAbsolutePath(), this);
            }
            else {
            	isS4Gog = S4Support.isGog();
            }

            if (isS4GamePathValid())
            {
                if (isS4Gog)
                {
                	Test.output("S4 is a GOG version");
                } else {
                	Test.output("S4 is not a GOG version");
                }
                File s4dir = ReqQuery.getS4Directory();
                Test.output("S4 is installed at: " + s4dir.getAbsolutePath());
            } else {
                Test.output("S4 is not or not correctly installed");
            }
        }
        catch (Exception e)
        {
            Test.output("Something unexpected happened when checking, if a potential S4 is a GOG version:");
            Test.outputException(e);
        }

        /**
         * Check for configuration-problems for the Settler 3-files,
         * e.g. if an compatibility-mode is set.
         */
        if (!IS_WINE && isS3GamePathValid()) {
        	// collect all errors in one string
        	String fileList = "";
            if (ReqQuery.isAppCompatSetForS3AlobbyExe()) {
                // The user is running S3 in a compatibility mode... damn.
                fileList = fileList + MessageFormat.format("<li><a href=''file:\\{0}'' color=''{1}''>" + new File (ReqQuery.getS3ExeDirectory(), "s3_alobby.exe").toString() + "</a></li>", new File (ReqQuery.getS3ExeDirectory(), "s3_alobby.exe").toString(), hexcolor);
            }
            if (ReqQuery.isAppCompatSetForS3Exe()) {
                // The user is running S3 in a compatibility mode... damn.
                fileList = fileList + MessageFormat.format("<li><a href=''file:\\{0}'' color=''{1}''>" + ReqQuery.getS3ExeFilePath() + "</a></li>", ReqQuery.getS3ExeFilePath(), hexcolor);
            }
            if (ReqQuery.isAppCompatSetForS3MultiExe()) {
                // The user is running S3 in a compatibility mode... damn.
            	fileList = fileList + MessageFormat.format("<li><a href=''file:\\{0}'' color=''{1}''>" + new File (ReqQuery.getS3ExeDirectory(), "S3_multi.exe").toString() + "</a></li>", new File (ReqQuery.getS3ExeDirectory(), "S3_multi.exe").toString(), hexcolor);
            }
            if( fileList.length() > 0 ) {
            	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
            	messagewindow.addMessage(
            		"<html>" + I18n.getString("SettlersLobby.ERROR_S3_COMPATIBILITY_ACTIVE_1") + "<ul>" + fileList + "</ul>" + I18n.getString("SettlersLobby.ERROR_S3_COMPATIBILITY_ACTIVE_2") + "</html>",
		         	UIManager.getIcon("OptionPane.errorIcon")
		        );
            	messagewindow.addButton(
        			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	         		new ActionListener() {
	         		    @Override
	         		    public void actionPerformed(ActionEvent e) {
	         		    	// close the aLobby
	         		    	System.exit(1);
	         		    }
	         		}
	         	);
            	messagewindow.setVisible(true);
            }
        }
        
        // clean up the gamedata-directory
        cleanUpGameDataDirectory();

        // the following steps are time consuming, so show several steps with the same text
        sF.setNewStep(I18n.getString("SettlersLobby.STARTFRAME_1")); //$NON-NLS-1$
        isConnectedWithServer = false;
        messageHandler = new MessageHandler();
        
        // create the GUI
        gui = new GUI(this);
        sF.setNewStep(I18n.getString("SettlersLobby.STARTFRAME_1")); //$NON-NLS-1$
        
        // create some container
        openGames = new OpenGameContainer();
        runningGames = new RunningGameContainer();
        
        // must be called before loginFrame and registerframe are created
        createCommunicationObjects();
        
        // create loginFrame
        loginFrame = new LoginFrame(gui, this);
        loginFrame.setListener(this);
        this.showOnSpecifiedScreen(this.config.getScreen(), loginFrame);
        
        // create RegisterFrame
        sF.setNewStep(I18n.getString("SettlersLobby.STARTFRAME_1")); //$NON-NLS-1$
        registerFrame = new RegisterFrame(gui, this);
        registerFrame.setListener(this);
        this.showOnSpecifiedScreen(this.config.getScreen(), registerFrame);

        // show next step
        sF.setNewStep(I18n.getString("SettlersLobby.STARTFRAME_2")); //$NON-NLS-1$
        
        // get the gameStarter depending on actual configured game-version
      	gameStarter = getGameStarter();

        showTime = config.isShowTime();
        logChat = config.isLogChat();
        setChatlog(new Chatlog(this));

        // automatically update the aLobby if a new version is available
        sF.setNewStep(I18n.getString("SettlersLobby.STARTFRAME_4")); //$NON-NLS-1$
        autoUpdate = new AutoUpdate(this);
        autoUpdate.updateProgramIfNecessary(sF, false);

        sF.setNewStep(I18n.getString("SettlersLobby.STARTFRAME_5")); //$NON-NLS-1$

        // show the start-window
        displayFirstFrame();
        
        // remove the loading-frame
        sF.setVisible(false);
        sF.dispose();
        if (config.isStartSavemanager()) {
            loadSaveManager();
        }
        
        // finally do the initialization of the Native Memory used by JNA functions
        // It should be finished before we are logged in, without any synchronization
        executor.execute(new Runnable() {
            @Override
            public void run()
            {
                WinFunctions.getInstance().init();
            }
        });
    }

    /**
     * ActionListener for detect pressed enter-key to close a messageWindow-dialog.
     * 
     * @param messagewindowBetaHint
     * @return KeyListener
     */
    private KeyListener addActionListenerForMessageWindowButton(MessageDialog messagewindowBetaHint) {
    	return new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode()==KeyEvent.VK_ENTER){
					messagewindowBetaHint.dispose();
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {
			}
    	};
	}

	/** 
     * checks if the alobby can write to the registry entries of S3 and to the S3 directory.
     * Without access rights starting S3 might fail or save games cannot be written. 
     * 
     * @return true if access rights are sufficient
     */
	private boolean checkAccessRights() 
	{
		boolean success = true;
		// Check if we can execute the s3.exe without admin privileges
		String s3Exe = config.getGamePathS3();
		if (isS3GamePathValid())
		{
			// s3.exe -help does not open a window, so this can be used to
			// silently check, if we can execute the s3.exe.
			// This will fail if the user did activate the option "Run as administrator"
			// for the s3.exe and the aLobby was not started as administrator.
			// In this case we know, that we need to start the aLobby as
			// admin to be able to start the s3.exe.
			try
			{
				Process process = new ProcessBuilder(s3Exe, "-help").start();
				
				if (process != null)
				{
					if (process.waitFor() != 0)
					{
						success = false;
					}
				} 
				else 
				{
					success = false;
				}
				
			} catch (Exception e) {
				success = false;
				Test.outputException(e);
			}
		}

		// Check, if we can write into the S3 dir
		if (isS3GamePathValid() && s3Exe != null) {
			File s3Dir = new File(s3Exe).getParentFile();
			File testFile = new File(s3Dir, ".alobbytest");
			BufferedWriter out = null;
			try  
			{
			    FileWriter fstream = new FileWriter(testFile);
			    out = new BufferedWriter(fstream);
			    out.write("\nthisisjustatest.ignoreit");
			}
			catch (IOException e)
			{
				success = false;
				Test.outputException(e);
			}
			finally
			{
			    if(out != null) {
			        try {
						out.close();
						testFile.delete();
					} catch (IOException e) {
						// ignore
					} catch (SecurityException e) {
		        		success = false;
		    			Test.outputException(e);
					}
			    }
			}
		}

		// Check, if we can write into the S3 registry values
		int introValue = ReqQuery.getS3Intro();
		if (introValue == 0 || introValue == 1)
		{
			int newValue = 1 - introValue;
			if (ReqQuery.setS3Intro(newValue))
			{
				//restore original value
				ReqQuery.setS3Intro(introValue);
			}
			else
			{
				success = false;        			
			}
		}

		// Check, if OpenVPN is installed.
		// Since the openvpn.exe needs always to be started as admin, we also
		// need to start the aLobby as admin in such a case.
		// 
		// So if the user wants to start the aLobby as normal user,
		// he/she *must not* choose to install OpenVPN during the aLobby
		// installation!
		if (!IS_WINE) {
			File openvpnExe = new File(getOpenVpnPath());
			if (openvpnExe.exists()) {
				success = false;
			}
		}
		return success;
	}

    public void loadSaveManager()
    {
    	try
    	{
         if (saveManager==null)
            saveManager = new SaveManager(this);
         else
            saveManager.configChanged();
         if (config.isStartWatchdog())
            saveManager.startWatchDogThread();
         else
            saveManager.stopWatchDogThread();
    	}
    	catch (Exception e) {
    		Test.outputException(e);
    	}
    }
    public void stopSaveManager()
    {
      if(saveManager!=null)
      {
         saveManager.stopWatchDogThread();
         saveManager=null;
      }
    }

    private void createCommunicationObjects()
    {
        ircCommunicator = new IRCCommunicator(this);
        ircCommunicator.setVerbose(ALobbyConstants.IRC_DEBUG_ON); // Log-Einträge werden ausgegeben
        if (getNick() != null && (!getNick().isEmpty()))
        {
            ircCommunicator.setNick(getNick());
        }
        ircCommunicator.setChannel(config.getChannel());
        createIrcConnector();
        setupProgramMessageProtocol();
    }

    /**
     * creates the ProgramMessageProtocol instance and
     * adds the required listeners that react on incoming
     * ProgramMessages.
     */
    private void setupProgramMessageProtocol()
    {
        protocol = new ProgramMessageProtocol();
        protocol.addListener(new PrgMsgOpenGameListener(this));
        protocol.addListener(new PrgMsgRunningGameListener(this));
        protocol.addListener(new PrgMsgGameStateListener(this));
        protocol.addListener(new PrgMsgAwayMessageListener(this));
        protocol.addListener(new PrgMsgStreamListener(this));
        protocol.addListener(new PrgMsgVPNStateListener(this));
        protocol.addListener(new PrgMsgMessageHandler(this));
        protocol.addListener(new PrgMsgCommunityNewsListener(this));
    }
    
    public ProgramMessageProtocol getProgrammMessageProtocol() {
    	return protocol;
    }

    public void setUserName(String userName)
    {
        config.setUserName(userName);
    }

    /**
     * Called if the program will be closed.
     */
    public void actionWantsToKillProgramm()
    {
        gui.displayWait(I18n.getString("SettlersLobby.MSG_EXIT")); //$NON-NLS-1$
        onChatIsClosing();
        if (!config.saveCurrentConfiguration())
        {
            error(I18n.getString("SettlersLobby.ERROR_SAVING_CONFIG")); //$NON-NLS-1$
        }
        gui.closeWait();
        gui.hideChat();
        try
        {
            stopVpn();
        } catch (Exception e) {
			Test.outputException(e);
		}
        finally
        {
            System.exit(0);
        }
    }

    /**
     * Called if the program is ready to update.
     */
    public void prepareForUpdateOfProgram()
    {
        try
        {
            gui.displayWait(I18n.getString("SettlersLobby.MSG_EXIT")); //$NON-NLS-1$
            onChatIsClosing();
            if (!config.saveCurrentConfiguration())
            {
                error(I18n.getString("SettlersLobby.ERROR_SAVING_CONFIG")); //$NON-NLS-1$
            }
            gui.closeWait();
        }
        catch(Exception e)
        {
            Test.outputException(e);
        }
    }

    public void actionIngameOptions(boolean display)
    {
        if (display)
        {
            initIngameOptionsFromConfig();
            updateSelectedIngameOptions();
        }
        gui.displayIngameOptions(display);
    }

    public void actionIngameOptionsStandalone()
    {
        setIngameOptionsStandalone(true);
        try
        {
            gameStarter = getGameStarter();
            actionDefaultIngameOptionsTasks();
            gui.displayIngameOptionsStandalone();
        }
        catch (Exception e)
        {
            gui.displayError(e.getMessage());
        }
    }

    public void actionDefaultIngameOptionsTasks()
    {
        initIngameOptionsFromConfig();
        updateAutoSaveState();
        updateStopStoringToolsState();
    }

    public void actionWantsToEditSettings()
    {
        gui.displaySettings();
    }

    public void actionWantsToConnect()
    {
        //Test.output("connect to Server");
        connectToServer();
    }

    private void connectToServer()
    {
        gui.displayWait(I18n.getString("SettlersLobby.MSG_CONNECTING"));
        ircConnector.doConnect(config.getIrcServerUrl(), config.getIrcServerPort());
    }

    private void createIrcConnector()
    {
        if (ircConnector == null)
        {
            ircConnector = new IrcConnector(ircCommunicator);
        }
        else
        {
            ircConnector.updateIrcInterface(ircCommunicator);
        }
        // kann onConnect() triggern:
        ircConnector.setListener(this);

    }

    private void iniAwayStatus()
    {
        if (config.isActivateAwayOnIdle())
        {
            awayStatus = new AwayStatusAutomatic(this, ircCommunicator, false);
        } else {
            awayStatus = new AwayStatusManual(this, ircCommunicator, false);
        }
    }

    private void resetAwayStatus()
    {
        if (awayStatus != null)
        {
            awayStatus.destroy();
        }
        //wenn der awayStatus auf null gesetzt wird
        //kann es zu einer NullpointerException kommen
        //awayStatus = null;
    }

    /**
     * Prüft zunächst, ob chatMessage leer, wenn ja, mache nix. Anschließend
     * prüft er, ob es sich um eine private oder öffentliche Nachricht handelt
     * und ruft die Funktion ircCommunicator.sendMessageToServer mit den
     * entsprechenden Parametern auf. Anschließend ruft lässt sie die GUI die
     * Nachrichten ins Chatfenster schreiben. Dazu wird gui.addWhisperedMessage
     * aufgerufen, falls es sich um eine geflüsterte Nachricht handelt, sonst
     * gui.addChatMessage, falls es sich um eine öffentliche Nachricht handelt
     *
     * @param chatMessage
     * @
     */
    public void actionChatMessage(String chatMessage)
    {
        userWasActive();
        if (!"".equals(chatMessage)) //$NON-NLS-1$
        {
            // Erklärung zum Pattern:
            // eine private Nachricht beginnt mit "/w ", "/whisper ", "/m " oder "/msg ",
            // alle 4 sind erlaubt
            Matcher m = Pattern.compile(
                    "(?:/w |/whisper |/m |/msg )\\s*(.+)").matcher(chatMessage);
            if (m.matches())
            {
                //es scheint eine private Nachricht zu sein, restliches Format prüfen
                // Erklärung zum Pattern:
                // (.+?) enthält nachher den Nick des Empfängers (mit reluctant qualifier,
                // damit auf jeden Fall der erste "Trenner" genommen wird).
                // (?:: |:| ) ist eine non-capturing group (?:X)
                // in der entweder ": ", ":" oder " " gematcht wird, alle drei Varianten
                // sind gültige Trenner um den Nick von der Nachricht zu trennen.
                // für die Nachricht muss (.*) und nicht (.+) genommen werden,
                // da ansonsten bei einem "/w nick: " das space grundsätzlich
                // zur Nachricht wird, als Trenner gilt dann nur der Doppelpunkt,
                // das ist aber nicht gewünscht.
                // Mit (.*) erhält man als Trenner ": " und eine leere Nachricht,
                // so wie es sein soll. Dadurch wird dann allerdings eine Zusatzabfrage
                // auf die leere Nachricht notwendig.
                m = Pattern.compile("(.+?)(?:: |:| )(.*)").matcher(m.group(1));

                if (m.matches())
                {
                    // falls private Message:
                    String receiverOfPrivateMessage = m.group(1);
                    String privateMessage = m.group(2);
                    if (privateMessage.isEmpty())
                    {
                        return;
                    }

                    ircCommunicator.sendMessageToServer(privateMessage, true,
                            receiverOfPrivateMessage);
                    messageHandler.addWhisperedMessage(getNick(), receiverOfPrivateMessage,
                            privateMessage);
                }
                else
                {
                    //das Format war nicht korrekt, die Nachricht wird ignoriert.
                    //es wird aber auch keine Fehlermeldung angezeigt.
                }
                return;
            }
            else if (chatMessage.startsWith("/me ")) //$NON-NLS-1$
            {
                // eine Action Message
                String actionMessage = chatMessage.substring(4);
                ircCommunicator.sendAction(getChannel(), actionMessage);
                messageHandler.addActionMessage(getNick(), actionMessage);
                return;
            }

            // falls öffentliche Message:
            if (ircCommunicator.sendMessageToServer(chatMessage, false, "")) //$NON-NLS-1$
            {
                messageHandler.addChatMessage(getNick(), chatMessage, false);
            }
        }
    }

    /**
     * If user wants to create a game.
     * 
     * @param startOptions
     */
    public void actionWantsToCreateMapGame(GameStarter.StartOptions startOptions)
    {
        if (!isS3GamePathValid())
        {
            gui.displayError(I18n.getString("SettlersLobby.ERROR_MISSING_S3")); //$NON-NLS-1$
            return;
        }
        if (!ReqQuery.isS3SerialNumberSet() && !isIngameOptionsStandalone())
        {
            gui.displayError(I18n.getString("SettlersLobby.ERROR_S3_SERIAL_NUMBER_NOT_SET")); //$NON-NLS-1$
            return;
        }

        if (isIngameOptionsStandalone)
        {
            actionWantsToCreateMapGameStandalone(startOptions);
            return;
        }

        userWasActive();
        try {
            // get the gameStarter depending on version of the open game
            gameStarter = getGameStarterByGivenVersion(startOptions.getMisc());
            String mapName = startOptions.getMapName();
            if (gameStarter.isReady()) {
                gameState.userWantsToCreateGame(mapName, false);
                awayStatus.onGameStarted();
                gameStarter.createGame(startOptions);
            }
            else
            {
                //der GameStarter ist nicht bereit, Abbruch
                actionCreateGameAborted();
            }
        } catch (Exception e) {
        	Test.outputException(e);
            gui.displayError(e.getMessage());
        }
    }

    public void actionWantsToCreateMapGameStandalone(GameStarterVanilla.StartOptions startOptions)
    {
        String mapPath = startOptions.getMapName();
        gameStarterThread = new GameStarterThread(
                this, config.getGamePathS3(), config.getTimeOutAsInt(), startOptions);
        gameStarterThread.start();
        gui.displayMapWait(MessageFormat.format(
                I18n.getString("SettlersLobby.MSG_MAP_SELECTION_WINDOW"), mapPath));  //$NON-NLS-1$
    }

    /**
     * User wants to start a game. So we open the newGameDialog for him.
     */
    public synchronized void actionWantsToCreateGame()
    {
    	// set user as active
        userWasActive();
        // Check if S3 or S4-exe are available. If not, show a hint and do not open the dialog.
        if (!isS3GamePathValid() && !isS4GamePathValid())
        {
            gui.displayError(I18n.getString("SettlersLobby.ERROR_MISSING_S3_AND_S4")); //$NON-NLS-1$
            return;
        }
        //wenn von der lobby schon ein Spiel geöffnet wurde (openHostGame != null), darf
        //kein zweites geöffnet werden, zuerst muß das erste beendet werden.
        //Solange das Spiel beim öffnen aber noch nicht im minichat angekommen ist,
        //ist openHostGame noch == null, daher wird auch noch gameStarter.isReady()
        //abgefragt, dieser liefert nur true, wenn weder der gamestarter noch S3 laufen
        if (openHostGame == null && gameStarter.isReady() && openJoinGame == null)
        {
            gui.displayMapChoosingDialog();
        } else
        {
            gui.displayErrorTerminateS3(I18n.getString("SettlersLobby.ERROR_ALREADY_RUNNING"), //$NON-NLS-1$
                    gameStarter.getS3Starter());
        }
    }

    public void actionWantsToCreateGameStandalone()
    {
        //bisherigen thread anhalten
        onCloseGameStandalone();
        gui.displayMapChoosingDialog();
    }

    /**
     * Actions if a player wants to join an open game.
     * 
     * @param openGame  the open game
     */
    public synchronized void actionWantsToJoinGame(OpenGame openGame)
    {
        userWasActive();
        MiscType gameVersion = openGame.getMisc();
        //Dieser Check auf die S3.exe ist notwendig, da es schwierig ist,
        //ohne Seiteneffekte wieder in einen gültigen Zustand zu kommen,
        //wenn der Gamestarter erstmal gestartet wurde und mit Fehler
        //abbricht
        if ((gameVersion == MiscType.ALOBBY || gameVersion == MiscType.CD || gameVersion == MiscType.GOG || gameVersion == MiscType.S3CE) && !isS3GamePathValid())
        {
            unselectGameInGui();
            gui.displayError(I18n.getString("SettlersLobby.ERROR_MISSING_S3")); //$NON-NLS-1$
            return;
        }
        if ((gameVersion == MiscType.ALOBBY || gameVersion == MiscType.CD || gameVersion == MiscType.GOG || gameVersion == MiscType.S3CE) && !ReqQuery.isS3SerialNumberSet())
        {
            unselectGameInGui();
            gui.displayError(I18n.getString("SettlersLobby.ERROR_S3_SERIAL_NUMBER_NOT_SET")); //$NON-NLS-1$
            return;
        }

        // The order does matter here!
        // -> if the open game has been created with future irc-protocol-version 
        if (openGame.doesRequireFutureVersion())
        {
            unselectGameInGui();
            gui.displayError(I18n.getString("SettlersLobby.NEW_ALOBBY_REQUIRED")); //$NON-NLS-1$
            return;
        }
        // -> if the open game is an Settler 4-game and the S4-interface is been disabled in the actual aLobby-instance
        else if (gameVersion == MiscType.S4 && config.isHideS4Interface() )
        {
            unselectGameInGui();
            gui.displayError(I18n.getString("SettlersLobby.S4_NOT_SUPPORTED")); //$NON-NLS-1$
            return;
        }
        // -> if the open game uses the original CD-version and the actual player uses a GOG-based version
        else if (S3Support.isGog() && gameVersion == MiscType.CD)
    	{
        	unselectGameInGui();
            gui.displayError(I18n.getString("SettlersLobby.ERROR_NOT_GOG")); //$NON-NLS-1$
            return;
        }
        // -> if the open game uses the GOG-version and the actual player does not have a support for this
        else if (!S3Support.isGog() && gameVersion == MiscType.GOG)
        {
        	unselectGameInGui();
            gui.displayError(I18n.getString("SettlersLobby.ERROR_NOT_CD")); //$NON-NLS-1$
            return;
        }
        // -> if the open game is not an CD- or GOG-version or S4 or S3CE
        else if (gameVersion != MiscType.CD && gameVersion != MiscType.GOG && gameVersion != MiscType.ALOBBY && gameVersion != MiscType.S4 && gameVersion != MiscType.S3CE)
        {
        	unselectGameInGui();
            gui.displayError(I18n.getString("SettlersLobby.ERROR_UNKNOWN_GAME_MODE")); //$NON-NLS-1$
            return;
        }

        // if the game is hosted via VPN and the user does not have an active VPN-connection
        // warn him.
        if (openGame.getIP().startsWith(ALobbyConstants.VPN_IP_RANGE) && (getVpnIpFromSystem() == null || !getVpnIpFromSystem().startsWith(ALobbyConstants.VPN_IP_RANGE))) {
        	unselectGameInGui();
        	
        	// load textcolor of actualy used theme as Color-Object
        	String hexcolor = getHexColorStringByColor(theme.getTextColor());
        	
 			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
         	messagewindow.addMessage(
         		I18n.getString("SettlersLobby.VPN_NOT_ACTIVE"),
         		UIManager.getIcon("OptionPane.errorIcon")
         	);
         	messagewindow.addMessage(
         			"<html>" + MessageFormat.format(I18n.getString("SettlersLobby.VPN_NOT_ACTIVE_INFO"),hexcolor) + "</html>",
         		UIManager.getIcon("OptionPane.informationIcon")
         	);
         	messagewindow.addButton(
         		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
         		new ActionListener() {
         		    @Override
         		    public void actionPerformed(ActionEvent e) {
         		    	// close this window
    			    	messagewindow.dispose();
         		    }
         		},
    			addActionListenerForMessageWindowButton(messagewindow)
         	);
         	messagewindow.setVisible(true);
            return;
        }

        if (gameVersion == MiscType.ALOBBY && gameVersion == MiscType.S3CE && getS3Directory() != null && !S3Support.isGog(getS3Directory().toString())) {
        	// Make sure the intro is off, when we use the s3_alobby.exe on *not* the S3-GOG version
        	ReqQuery.setS3Intro(0);
        }

        // do not join if the acutal user plays tetris
        if( this.isPlayingTetris() ) {
        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
        	messagewindow.addMessage(
				I18n.getString("OpenNewS3GameDialog.ERROR_TETRIS"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
        	messagewindow.addButton(
         		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
         		new ActionListener() {
         		    @Override
         		    public void actionPerformed(ActionEvent e) {
         		    	// close this window
    			    	messagewindow.dispose();
         		    }
         		},
    			addActionListenerForMessageWindowButton(messagewindow)
         	);
         	messagewindow.setVisible(true);
            return;
        }
        
        // check if s4 is running
		if( !config.isHideS4Interface() && ( getOpenHostGame() != null || getOpenJoinGame() != null ) ) {
			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
			messagewindow.addMessage(
				I18n.getString("SettlersLobby.ERROR_ALREADY_RUNNING"),
        		UIManager.getIcon("OptionPane.errorIcon")
        	);
			messagewindow.addButton(
				UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
        		new ActionListener() {
        		    @Override
        		    public void actionPerformed(ActionEvent e) {
        	        	// and close the hint-window
        		    	messagewindow.dispose();
        		    }
        		},
    			addActionListenerForMessageWindowButton(messagewindow)
        	);
			messagewindow.setVisible(true);
            return;
		}

		// check if a S4 CD is mounted
		if (gameVersion == MiscType.S4) {
		    boolean mounted = S4Support.isCdMounted();
		    if (mounted != true) {
	            MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
	            messagewindow.addMessage(
	                I18n.getString("SettlersLobby.ERROR_S4_CD_Missing"),
	                UIManager.getIcon("OptionPane.errorIcon")
	            );
	            messagewindow.addButton(
	                UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	                new ActionListener() {
	                    @Override
	                    public void actionPerformed(ActionEvent e) {
	                        // and close the hint-window
	                        messagewindow.dispose();
	                    }
	                },
	                addActionListenerForMessageWindowButton(messagewindow)
	            );
	            messagewindow.setVisible(true);
	            return;
		    }
		}

        // -> if the open game uses the original GOG-version we must set the compatibility to false; otherwise set it bask to true
        if (S3Support.isGog() && gameVersion == MiscType.GOG)
        {
            config.setGameCompability(false);
        } else if (!S3Support.isGog() && gameVersion != MiscType.GOG){
            config.setGameCompability(true);
        }
        // -> if the open game uses the original CD-version we must set the compatibility to false; otherwise set it bask to true
        if (!S3Support.isGog() && gameVersion == MiscType.CD)
        {
            config.setGameCompability(false);
        } else if (!S3Support.isGog() && gameVersion != MiscType.CD){
            config.setGameCompability(true);
        }

        // check if the given mapfile is local available
        // -> if not download it from mapbase
        // -> TODO do not check for S4-Games until Mapbase is ready for it
        if (isS3MapFileMissing(openGame) && gameVersion != MiscType.S4)
        {
            //die map ist nicht vorhanden, also das Spiel erstmal wieder deselektieren
            unselectGameInGui();
            
            // download map automatically and start the game after the download
            String mapNameWithoutFileExtension = openGame.getMap().substring(openGame.getMap().indexOf("\\")+1, openGame.getMap().lastIndexOf(".")); //$NON-NLS-1$ //$NON-NLS-2$ 
            DownloadActions.startAutomaticMapDownload(this, mapNameWithoutFileExtension, openGame); 
            return;
        }
        else
        {
        	// try to join the game
        	// -> get the mapname
        	String mapName = openGame.getMap();
        	// -> get the savegame-filename (if it is a savegame)
        	String saveName = openGame.getSaveGameFileName();
        	long saveTimestamp = openGame.getSaveGameTimestamp();
        	// -> check if the savegame-filename is available (not for S4)
        	if (mapName.startsWith(ALobbyConstants.LABEL_SAVE_GAME) && saveName != null && saveTimestamp > 0 && gameVersion != MiscType.S4)
        	{
        		if (saveManager != null && !saveManager.findSave(saveName, saveTimestamp, config.isAutoPlace()))
        		{
        			String msg = I18n.getString("SettlersLobby.SAVE_NOT_FOUND_MSG1") + " " + saveName +
        							I18n.getString("SettlersLobby.SAVE_NOT_FOUND_MSG2") + " " +
        							(new Date(saveTimestamp)).toString() + 
        							I18n.getString("SettlersLobby.SAVE_NOT_FOUND_MSG3"); 
        			int result = JOptionPane.showConfirmDialog(null,
        					msg,
        					I18n.getString("SettlersLobby.SAVE_NOT_FOUND_TITLE"),
        					JOptionPane.YES_NO_OPTION,
        					JOptionPane.WARNING_MESSAGE);
        			if (result == JOptionPane.NO_OPTION)
    				{
        				unselectGameInGui();
        				return;
    				}
        		}

        	}
            try
            {
                // get the gameStarter depending on version of the open game
                gameStarter = getGameStarterByGivenVersion(openGame.getMisc());
                if (gameStarter.isReady())
                {
                	// check the md5-hash of this file on mapbase
                	// -> not for S4
                	if( gameVersion != MiscType.S4 ) {
	                	CheckAndDownloadMapFile mapFile = new CheckAndDownloadMapFile(this);
	        			mapFile.setMap(mapName);
	        			if( mapFile.checkMapFile() || mapName.startsWith(ALobbyConstants.LABEL_SAVE_GAME) ) {
	        				// -> file exists on mapbase: ok
		                    setAsOpenJoinGame(openGame);
		                    actionIngameOptions(true);
		                    gameState.userWantsToJoinGame(openGame.getHostUserName(), mapName, false);
		                    awayStatus.onGameStarted();
		                    Test.output("openGame: " + openGame.getMisc());
		                    gameStarter.joinGame(
			                    new GameStarterVanilla.StartOptions(getNick(), openGame.getIP(), openGame.getMap(), openGame.getGoods(), openGame.isLeagueGame(), openGame.getTournamentId(), openGame.getTournamentName(), openGame.getMisc()),
			                    openGame.getHostUserName(),
			                    openGame.getMap(),
			                    openGame.getGoods(),
			                    openGame.isLeagueGame(),
			                    openGame.getTournamentId(),
			                    openGame.getTournamentName()
		                    );
	        			}
	        			else {
	        				// file does not exists on mapbase: warn user first
	        				String hexcolor = SettlersLobby.getHexColorStringByColor(theme.getTextColor());
	        				// create a window for hints if something is missing
	        				MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
	        				messagewindow.addMessage(
	        					MessageFormat.format("<html>" + I18n.getString("OpenNewS3GameDialog.MAP_DOES_NOT_EXISTS_ON_MAPBASE_PLAYER") + "<br>" + I18n.getString("OpenNewS3GameDialog.MAP_DOES_NOT_EXISTS_ON_MAPBASE_PLAYER_LINE_2") + "</html>", hexcolor),
	        	        		UIManager.getIcon("OptionPane.errorIcon")
	        	        	);
	        	        	messagewindow.addButton(
	    	        			I18n.getString("OpenNewS3GameDialog.VPN_HINT_AT_START_BUTTON_PLAYER"),
	        	        		new ActionListener() {
	        	        		    @Override
	        	        		    public void actionPerformed(ActionEvent e) {
	        	        	        	// and close the hint-window
	        	        		    	messagewindow.dispose();
	        	        		    	// create the game-conditions
	        	        		    	setAsOpenJoinGame(openGame);
	        		                    actionIngameOptions(true);
	        		                    gameState.userWantsToJoinGame(openGame.getHostUserName(), mapName, false);
	        		                    awayStatus.onGameStarted();
	        		                    gameStarter.joinGame(
	        			                    new GameStarterVanilla.StartOptions(getNick(), openGame.getIP(), openGame.getMap(), openGame.getGoods(), openGame.isLeagueGame(), openGame.getTournamentId(), openGame.getTournamentName(), openGame.getMisc() ),
	        			                    openGame.getHostUserName(),
	        			                    openGame.getMap(),
	        			                    openGame.getGoods(),
	        			                    openGame.isLeagueGame(),
	        			                    openGame.getTournamentId(),
	        			                    openGame.getTournamentName()
	        		                    );
	        	        		    }
	        	        		}
	        	        	);
	        	        	messagewindow.addButton(
	        	        		I18n.getString("OpenNewS3GameDialog.VPN_HINT_AT_START_BUTTON_2"),
	        	        		new ActionListener() {
	        	        		    @Override
	        	        		    public void actionPerformed(ActionEvent e) {
	        	        	        	// and close the hint-window
	        	        		    	messagewindow.dispose();
	        	        		    }
	        	        		}
	        	        	);
	        	        	messagewindow.setVisible(true);
	        			}
                	}
                	else {
                		// join an open s4-game
                		// -> first check if chosen map is local available
                		if( isS4MapFileMissing(openGame) ) {
                			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
                			messagewindow.addMessage(
	        					I18n.getString("OpenNewS4GameDialog.MAP_DOES_NOT_EXISTS"),
	        	        		UIManager.getIcon("OptionPane.errorIcon")
	        	        	);
                			messagewindow.addButton(
            					UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	        	        		new ActionListener() {
	        	        		    @Override
	        	        		    public void actionPerformed(ActionEvent e) {
	        	        	        	// and close the hint-window
	        	        		    	messagewindow.dispose();
	        	        		    }
	        	        		},
	        	    			addActionListenerForMessageWindowButton(messagewindow)
	        	        	);
                			messagewindow.setVisible(true);
                			return;
                		}
                		// -> than check if user should join game
                		// -> if s3 is running already checked above
                		// -> if clicked game is full
                		if( openGame.getCurrentPlayer() >= openGame.getMaxPlayer() ) {
            				MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
                			messagewindow.addMessage(
	        					I18n.getString("SettlersLobby.S4_FULL_GAME"),
	        	        		UIManager.getIcon("OptionPane.errorIcon")
	        	        	);
                			messagewindow.addButton(
            					UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	        	        		new ActionListener() {
	        	        		    @Override
	        	        		    public void actionPerformed(ActionEvent e) {
	        	        	        	// and close the hint-window
	        	        		    	messagewindow.dispose();
	        	        		    }
	        	        		},
	        	    			addActionListenerForMessageWindowButton(messagewindow)
	        	        	);
                			messagewindow.setVisible(true);
                			unselectGameInGui();
                            return;
                		}
                		// its ok to join the open S4-game
                		// -> set a game which will be joined
                		setAsOpenJoinGame(openGame);
                		// -> send info to host that his game will be joined
                		sendGameProgramMessage(ALobbyConstants.TOKEN_JOINED, new UserInGame(openGame.getHostUserName()), openGame.getMap(), this.ipToUse());
                		// -> send info to host about increment playernumber
                		onSendIncrementPlayerNumber(openGame.getHostUserName());
                		// generate a userlist in gameState
						getGameState().userWantsToJoinGame(openGame.getHostUserName(), openGame.getMap(), true);
						// activate the game options-window with userlist from game
						UserInGameList userListInGame = new UserInGameList();
						List<String> playerList = openGame.getPlayerList();
						for(String user : playerList) {
							userListInGame.updateUser(new UserInGame(user), true);
						}
						Test.output("setS4GameOptions bbbb");
						getGUI().setS4GameOptions(userListInGame, false, true);
						// set user as playing
						userWasActive();
						getAwayStatus().onGameStarted();
                	}
                }
                else
                {
                    unselectGameInGui();
                    gui.displayErrorTerminateS3(I18n.getString("SettlersLobby.ERROR_ALREADY_RUNNING"), //$NON-NLS-1$
                            gameStarter.getS3Starter());
                }
            } catch (Exception e)
            {
                unselectGameInGui();
                gui.displayError(e.getMessage());
            }
        }
    }

    /**
     * Set the game object where actual user wants or is joined to.
     * 
     * @param openGame
     */
	public void setAsOpenJoinGame(OpenGame openGame)
    {
        openJoinGame = openGame;
    }

	/**
	 * Check if the map for a given S3 game exists local.
	 * If not return false.
	 * 
	 * @param openGame
	 * @return
	 */
    private boolean isS3MapFileMissing(OpenGame openGame)
    {
        String mapName = openGame.getMap();
        return (!mapName.startsWith(ALobbyConstants.LABEL_SAVE_GAME))
                && (!mapName.startsWith(ALobbyConstants.PATH_RANDOM))
                && (!createS3MapFilePath(mapName).exists());
    }
    
    /**
	 * Check if the map for a given S4 game exists local.
	 * If not return false.
	 * 
	 * @param openGame
	 * @return
	 */
    private boolean isS4MapFileMissing(OpenGame openGame)
    {
        String mapName = openGame.getMap();
        return (!mapName.startsWith(ALobbyConstants.PATH_S4SAVEGAMES))
                && (!mapName.startsWith(ALobbyConstants.PATH_RANDOM))
                && (!createS4MapFilePath(mapName).exists());
    }

    /**
     * Returns the directory where the s3.exe is stored
     * 
     * @return the directory where the s3.exe is stored
     */
    public File getS3Directory()
    {
    	if (config.getGamePathS3() == null) {
    		return null;
    	}
        else if (config.getGamePathS3().isEmpty()) {
            return null;
        }
        File S3Dir = new File(config.getGamePathS3()).getParentFile();
        return S3Dir;
    }
    
    /**
     * Returns the directory where the s4.exe is stored
     * 
     * @return the directory where the s4.exe is stored
     */
    public File getS4Directory()
    {
    	if (config.getGamePathS4() == null) {
    		return null;
    	}
    	else if (config.getGamePathS4().isEmpty()) {
    	    return null;
    	}
        File S4Dir = new File(config.getGamePathS4()).getParentFile().getParentFile();
        return S4Dir;
    }

    /**
     *
     * @return mapDirectory, i.e. 'C:\BlueByte\Siedler3\Map'
     */
    public File getS3MapDirectory()
    {
        return new File(getS3Directory(), ALobbyConstants.PATH_MAP);
    }
    
    /**
    *
    * @return mapDirectory, i.e. 'C:\BlueByte\Siedler4\Map'
    */
   public File getS4MapDirectory()
   {
	   File directory = new File(getS4Directory(), ALobbyConstants.PATH_MAP);
       return directory;
   }

    /**
     * Return the filepath for a given S3-map.
     *
     * @param mapName i.e. 'USER\alien.map'
     * @return mapFilePath, i.e. 'C:\BlueByte\Siedler3\Map\USER\alien.map'
     */
    private File createS3MapFilePath(String mapName)
    {
        File S3MapDir = getS3MapDirectory();
        File mapFilePath = new File(S3MapDir, mapName);
        return mapFilePath;
    }
    
    /**
    * Return the filepath for a given S4-map.
    *
    * @param mapName i.e. 'multiplayer\alien.map'
    * @return mapFilePath, i.e. 'C:\BlueByte\die siedler vi\Map\multiplayer\alien.map'
    */
   private File createS4MapFilePath(String mapName)
   {
       File S4MapDir = getS4MapDirectory();
       File mapFilePath = new File(S4MapDir, mapName);
       return mapFilePath;
   }

   /**
    * Logout from IRC and remove alle GUI-components.
    * Show only the first window with login- and settings-option.
    */
    public void actionWantsToLogOut()
    {
        gui.displayWait(I18n.getString("SettlersLobby.MSG_EXIT")); //$NON-NLS-1$
        onChatIsClosing();
        try
        {
            stopVpn();
        } catch (Exception e) {
			Test.outputException(e);
		}
        gui.closeWait();
        gui.hideChat();
        gui.getOptionsFrame().setEmail("");
        displayFirstFrame();
        gui.hideCurrentFrame(); //call this explicitly in case not the chat but the login window was visible
    }

    public void displayFirstFrame()
    {
        gui.closeWait(); //wait-Fenster könnte geöffnet sein
        gui.displayFirstFrame();
    }

    /**
     * Called if the chat is closing. 
     */
    private void onChatIsClosing()
    {
        openGames.clear();
        runningGames.clear();
        removeGameFromRunningGames();
        if( openHostGame != null ) {
        	// prepare message to update info on webserver
        	// -> responsive is not relevant for us here
        	httpRequests httpRequests = new httpRequests();
        	httpRequests.setUrl("https://lobby.siedler3.net/aLobbyRequests.html");
        	httpRequests.addParameter("updategame", 1); 
        	httpRequests.addParameter("state", "closed");
        	httpRequests.addParameter("hostname", getNick());
        	httpRequests.addParameter("ip", ipToUse());
        	// no token-security for this request
        	httpRequests.setIgnorePhpSessId(true);
        	try {
        		httpRequests.doRequest();
        	} catch (IOException e) {
        	}
        }
        DefaultTableModelForCommunityNews communityNewsTable = getGUI().getCommunityNews();
        if( communityNewsTable != null ) {
        	getGUI().getCommunityNews().clear();
        }
        // secure the active main tab
        config.setActiveMainTab(gui.getTabbedPane().getSelectedIndex());
        onCloseGame();
        if (chatlog.isLogging())
            chatlog.stopLogging();

        disconnect();

        //kurz schlafen, um noch eventuell vorhandene
        //Nachrichten zu schicken
        try
        {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        if (config.isAutoLoadAndSaveBuddys()) {
            try
            {
                BuddyHandling.exportBuddyIgnore();
            } catch (Exception e)
            {
                // Loggen und Ignorieren
                Test.outputException(e);
            }
        }

        //make sure that there remains no reference to a S3-process in the
        //processWindowSupportHandler (maybe it is not really needed, but it is
        //a proper cleanup)
        ProcessWindowSupportHandler.getInstance().setProcessWindowSupport(null);
    }

    /**
     * Disconnect from IRC. Wait max. 5 Seconds to close the connection.
     */
    private void disconnect()
    {
        if (ircConnector.doDisconnect())
        {
            //wait at least 5 seconds for the Server to terminate the connection
            for (int i = 0; i < 10; ++i)
            {
                //checking isConnectedWithServer should be sufficient, but
                //at the moment also check the low level ircCommunicator
                if  (isConnectedWithServer && ircCommunicator.isConnected())
                {
                    try
                    {
                        Test.output("disconnect " + i);
                        Thread.sleep(500);
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
        }
    }

    /**
     * wird aufgerufen, wenn der MapChoosingDialog ohne Auswahl geschlossen
     * wird. Macht die IngameOptions wieder unsichtbar.
     */
    public void actionCreateGameAbortedFromMapSelectionWindow()
    {
        userWasActive();
        if (!gameStarter.isRunning())
        {
            actionIngameOptions(false);
        }
    }

    public void actionCreateGameAborted()
    {
        userWasActive();
        try
        {
            actionIngameOptions(false);
            gameStarter.stopGame();
        } catch (Exception e)
        {
            gui.displayError(e.getMessage());
        }
        finally
        {
            onCloseGame();
        }
    }

    public void actionCreateGameAbortedStandalone()
    {
        onCloseGameStandalone();
    }

    /**
     * Erstellt ein neues OpenGame Objekt, setzt es als openHostGame
     * und übermittelt es an alle Spieler im Channel.
     *
     * @param mapName
     * @param maxPlayerNumber
     * @param currentPlayerNumber
     * @param amazonen
     * @param goodsInStock
     * @param wimo
     */
    public void newGame(String gameName, String mapName, int maxPlayerNumber, int currentPlayerNumber,
						boolean amazonen, GoodsInStock goodsInStock, boolean isLeagueGame,
						boolean wimo, int tournamentid, String tournamentname, int round, int groupnumber,
						int matchnumber, String playerlist,
						List<OpenGame.S3CEGameOption> gameModes)
    {
    	long timestamp = 0;
    	String saveGameFileName = "";
        if (mapName.startsWith(ALobbyConstants.LABEL_SAVE_GAME))
        {
	        int index = mapName.indexOf('\\');
	        if (index != -1)
	        {
	        	String mapFileName = mapName.substring(index+1);
	        	if (SaveGame.isBackupSave(mapFileName))
	        	{
	        		mapName = mapName.substring(0, index+1+40); // +1 because of the index itself
	        	}
	        }
        	String gamePath = config.getGamePathS3();
        	File saveGameFile = SaveGame.getFileFromSaveGameName(mapName, gamePath);
        	saveGameFileName = saveGameFile.getName();
        	timestamp = SaveGame.getTimestamp(saveGameFile);
            //Anstatt des tatsächlichen Dateinamens soll für
            //savegames der Name der gespielten Karte übertragen werden
            //Dieses muss zwingend passieren, bevor das openHostGame
            //erzeugt wird
            mapName = ALobbyConstants.LABEL_SAVE_GAME + File.separator +
                SaveGame.getS3MapnameOfSaveGame(saveGameFile, config.getGameDateTimeFormat());

        }
        int goodsInStockString = goodsInStock != null ? goodsInStock.getIndex() : 0; //$NON-NLS-1$
        OpenGame newGame = new OpenGame(ipToUse(), getNick(), gameName, mapName,
                amazonen, goodsInStockString, maxPlayerNumber, currentPlayerNumber, isLeagueGame, wimo, tournamentid, tournamentname, round, groupnumber, matchnumber, gameModes);
        newGame.setSaveGame(saveGameFileName, timestamp);

        if ( config.getS3Versionnumber() >= 300 ) {
        	newGame.setMisc(MiscType.S3CE);
        } else if (config.getGameCompability()) {
        	newGame.setMisc(MiscType.ALOBBY);
        } else if (isS3Gog) {
        	newGame.setMisc(MiscType.GOG);
        } else {
        	newGame.setMisc(MiscType.CD);
        }
        
        // add the creation-time
        Calendar calendar = Calendar.getInstance();
    	calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        newGame.setCreationTime(calendar.getTimeInMillis());
        
        setAsOpenHostGame(newGame);
        actionIngameOptions(true);
        // das openHostGame wird niemals in der GUI angezeigt, stattdessen
        // erzeugt das sendOpenHostGame() auch beim Host selber nochmal ein
        // OpenGame (durch onPrgMsgNewGame()), welches dann in der GUI angezeigt wird.
        // Bevor dieses angezeigt wird, wird dort aber ein etwaiges altes Spiel von dem
        // Host aus der Liste gelöscht. Aus diesem Grund ist der folgende Aufruf
        // auskommentiert, denn der Host würde dann das openHostGame, was er
        // gerade erst in die Liste gepackt hat, sofort wieder aus der Liste löschen.
        // Damit kann man sich das hinzufügen auch gleich sparen.
        //openGames.addOpenGame(openHostGame);
        // TODO Inwiefern es Sinn macht, ein openHostGame Objekt zu haben, was nicht
        // in der Liste openGames existiert, muss noch überprüft werden. Da es
        // aber schon immer so war, ist es zumindest lauffähig.

        // Allen usern im channel mitteilen
        sendOpenHostGame(null);
    }

    /**
     * Set a game as open host game. This object exists only on host-user.
     * If "null" is given the host game is removed. 
     * 
     * @param openGame	the game-object
     */
    public void setAsOpenHostGame(OpenGame openGame)
    {
        openHostGame = openGame;
        if( openGame == null ) {
        	if( this.getGameState().isHost() ) {
	        	// send info about closed game to webserver
	        	// if this user was host of this game
	        	// -> responsive is not relevant for us here
	        	httpRequests httpRequests = new httpRequests();
	        	httpRequests.setUrl("https://lobby.siedler3.net/aLobbyRequests.html");
	        	httpRequests.addParameter("updategame", 1);
	        	httpRequests.addParameter("state", "closed");
	        	httpRequests.addParameter("hostname", getNick());
	        	httpRequests.addParameter("ip", ipToUse());
	        	// no token-security for this request
	        	httpRequests.setIgnorePhpSessId(true);
	        	try {
	        		httpRequests.doRequest();
	        	} catch (IOException e) {
	        	}
        	}
        	this.getGameState().setHost(false);
        }
        else {
        	// send info about new game to webserver
        	// -> response is not relevant for us here
        	httpRequests httpRequests = new httpRequests();
        	httpRequests.setUrl("https://lobby.siedler3.net/aLobbyRequests.html");
        	httpRequests.addParameter("addgame", 1);
        	httpRequests.addParameter("gamename", openHostGame.getName());
        	httpRequests.addParameter("mapname", openHostGame.getMap());
        	httpRequests.addParameter("playercount", 1);
        	httpRequests.addParameter("maxplayercount", openHostGame.getMaxPlayer());
        	httpRequests.addParameter("misc", openHostGame.getMisc().toString());
        	httpRequests.addParameter("hostname", openHostGame.getHostUserName());
        	httpRequests.addParameter("ip", ipToUse());
        	httpRequests.addParameter("openSince", openHostGame.getCreationTime() / 1000);
        	httpRequests.addParameter("goods", openHostGame.getGoods());
        	List<String> users = openGame.getPlayerList();
			JSONArray usersAsJSON = new JSONArray();
			for( int i = 0;i<users.size();i++ ) {
				JSONObject item = new JSONObject();
				item.put("name", users.get(i));
				usersAsJSON.put(item);
			}
			httpRequests.addParameter("playerlist", usersAsJSON.toString());
			
        	// no token-security for this request
        	httpRequests.setIgnorePhpSessId(true);
        	try {
        		httpRequests.doRequest();
        	} catch (IOException e) {
        	}
        }
    }
    
    public OpenGame getRunningHostGame() {
    	return runningHostGame;
    }
    
    public OpenGame getRunningGame() {
    	return runningGame;
    }
    
    public void setAsRunningHostGame(OpenGame openGame)
    {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
    	openGame.setRunningGameStartTimestamp(calendar.getTimeInMillis());
        runningHostGame = openGame;
        
        // send message to update info on webserver
    	// -> responsive is not relevant for us here
    	httpRequests httpRequests = new httpRequests();
    	httpRequests.setUrl("https://lobby.siedler3.net/aLobbyRequests.html");
    	httpRequests.addParameter("updategame", 1);
    	httpRequests.addParameter("state", "running");
    	httpRequests.addParameter("runningSince", openGame.getRunningGameStartTimestamp() / 1000);
    	httpRequests.addParameter("hostname", openGame.getHostUserName());
    	httpRequests.addParameter("ip", openGame.getIP());
    	// no token-security for this request
    	httpRequests.setIgnorePhpSessId(true);
    	try {
    		httpRequests.doRequest();
    	} catch (IOException e) {
    	}
        
    }
    
    public void setAsRunningGame( String hostUserName, String ip, String mapName, int goods, int tournamentid, String tournamentname, int round, int groupnumber, int matchnumber ) {
    	OpenGame runningGame = new OpenGame(ipToUse(), getNick(), "", mapName, false, goods, 0, 0, true, false, tournamentid, tournamentname, round, groupnumber, matchnumber, new ArrayList<>());
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
    	runningGame.setRunningGameStartTimestamp(calendar.getTimeInMillis());
        this.runningGame = runningGame;
    }

    /**
     * Set a new current player number for an open game.
     * 
     * @param newCurrentPlayerNumber	the new current player number
     */
    public void newCurrentPlayerNumber(int newCurrentPlayerNumber)
    {
        if ( newCurrentPlayerNumber > 0 && ( ( newCurrentPlayerNumber <= gameStarter.getMaximumPlayerNumber() ) || ( !config.isHideS4Interface() && this.openHostGame.getMisc() == MiscType.S4 ) ) )
        {
            openHostGame.setCurrentPlayer(newCurrentPlayerNumber);
            ircCommunicator.sendProgramMessage(protocol.createMsgNewCurrentPlayerNumber(openHostGame));
            // prepare message to update info on webserver
        	// -> responsive is not relevant for us here
        	httpRequests httpRequests = new httpRequests();
        	httpRequests.setUrl("https://lobby.siedler3.net/aLobbyRequests.html");
        	httpRequests.addParameter("updategame", 1);
        	httpRequests.addParameter("state", "open");
        	httpRequests.addParameter("hostname", getNick());
        	httpRequests.addParameter("ip", ipToUse());
        	// no token-security for this request
        	httpRequests.setIgnorePhpSessId(true);
        	httpRequests.addParameter("playercount", newCurrentPlayerNumber);
        	try {
        		httpRequests.doRequest();
        	} catch (IOException e) {
        	}
            // update s4-userlist to enable or disable buttons
            if( openHostGame.getMisc() == MiscType.S4 ) {
            	UserInGameList userListInGame = new UserInGameList(openHostGame.getHostUserName(), openHostGame.getMap());
				List<String> playerList = openHostGame.getPlayerList();
				for(String user : playerList) {
					userListInGame.updateUser(new UserInGame(user), true);
				}
				Test.output("setS4GameOptions cccc");
            	this.getGUI().setS4GameOptions(userListInGame, true, true);
            }
        }
    }

    /**
     * Set a new max-player number for an open game.
     * 
     * @param newCurrentPlayerNumber	the new max-player number
     */
    public void newMaxPlayerNumber(int newMaxPlayerNumber)
    {
        if (openHostGame.getCurrentPlayer() > newMaxPlayerNumber)
        {
            openHostGame.setCurrentPlayer(newMaxPlayerNumber);
        }
        openHostGame.setMaxPlayer(newMaxPlayerNumber);
        ircCommunicator.sendProgramMessage(protocol.createMsgNewMaxPlayerNumber(openHostGame));
        // prepare message to update info on webserver
    	// -> responsive is not relevant for us here
    	httpRequests httpRequests = new httpRequests();
    	httpRequests.setUrl("https://lobby.siedler3.net/aLobbyRequests.html");
    	httpRequests.addParameter("updategame", 1);
    	httpRequests.addParameter("state", "open");
    	httpRequests.addParameter("hostname", getNick());
    	httpRequests.addParameter("ip", ipToUse());
    	// no token-security for this request
    	httpRequests.setIgnorePhpSessId(true);
    	httpRequests.addParameter("maxplayercount", newMaxPlayerNumber);
    	try {
    		httpRequests.doRequest();
    	} catch (IOException e) {
    	}
    }

	/**
	 * Sends the game mode for the currently hosted game to all players.
	 */
	public void sendNewGameMode(long newGameMode) {
    	if (openHostGame != null) {
    		openHostGame.setS3C3Options(OpenGame.S3CEGameOption.parse(newGameMode));
    		ircCommunicator.sendProgramMessage(protocol.createMsgNewGameMode(openHostGame, newGameMode));
    		// TODO: Should this be sent to the website?
		}
	}

    /**
     * setzt einen neuen Name für das aktuell geöffnete Spiel und übermittelt
     * ihn an die user.
     * @param name Spielname
     */
    public void newGameName(String name)
    {
        if (openHostGame != null)
        {
            //die Funktion wird aus der GUI heraus aufgerufen,
            //dort wurde zuvor die Zelle im Table verändert.
            //Das zum TableModel gehörige OpenGame wurde zu dem Zeitpunkt
            //noch nicht verändert (es unterscheidet sich übrigens von
            //openHostGame, obwohl es denselben Inhalt darstellt).
            //als erstes wird jetzt hier das openHostGame aktualisiert,
            //anschliessend wird der neue Name an alle User verteilt, auch
            //an einen selber. Sobald diese Nachricht empfangen wird,
            //wird dann auch das OpenGame des TableModels angepasst
            //(was schlussendlich wiederum nochmal die TableCell setzt)
            // wird der Name zu schnell hintereinander geändert, könnten
            // da lustige Effekte entstehen.
            // der neue Name kann prinzipiell Semikolons enthalten, was
            // beim Empfang Probleme macht, da das Semikolon das Trennzeichen
            // zwischen den Werten ist, daher wird der Name grundsätzlich
            // base64 encoded.
            openHostGame.setName(name);
            ircCommunicator.sendProgramMessage(protocol.createMsgNewGameName(openHostGame));
            
            // send message to update info on webserver
        	// -> responsive is not relevant for us here
        	httpRequests httpRequests = new httpRequests();
        	httpRequests.setUrl("https://lobby.siedler3.net/aLobbyRequests.html");
        	httpRequests.addParameter("updategame", 1);
        	httpRequests.addParameter("state", "open");
        	httpRequests.addParameter("hostname", openHostGame.getHostUserName());
        	httpRequests.addParameter("ip", openHostGame.getIP());
        	httpRequests.addParameter("gamename", name);
        	// no token-security for this request
        	httpRequests.setIgnorePhpSessId(true);
        	try {
        		httpRequests.doRequest();
        	} catch (IOException e) {
        	}            
        }
    }


    public synchronized void actionGameStarted( StartOptions startOptions )
    {
    	IngameMessage.setIsInMiniChat(false);
    	//Autosavetask wird aktiviert oder deaktiviert
        //dies passiert direkt beim tatsächlichen Start des Spiels,
        //so dass der AutosaveTimer halbwegs mit der Spielzeit übereinstimmt.
        updateAutoSaveState();
        //WerkzeugeeinlagerungsTask wird aktivert oder deaktiviert
        updateStopStoringToolsState();
        //updateGetRaceState();
        
        startClipBoardListener();
        
        // start check for S3-Stats
       	updateCheckForS3Stats(startOptions);
       	
       	// stop loading ts-panel automaticly
        getGUI().getTeamSpeakPanel().stopLoading();
       	
        gameState.userHasStartedGame( startOptions );
        //der gerade veränderte gamestate darf durch diesen Aufruf nicht
        //weiter modifiziert werden, daher der Aufruf von
        //onCloseGamePreservingGameState(), wodurch nur
        //der gamestarter mit seinen observern beendet und das eventuell offene
        //Spiel aus der Anzeige gelöscht wird
        onCloseGamePreservingGameState( startOptions );
    }
    
    public synchronized void actionGameStarted( String hostUserName, String ip, String mapName, int goods, boolean league, int tournamentid, String tournamentname )
    {
    	IngameMessage.setIsInMiniChat(false);
    	//Autosavetask wird aktiviert oder deaktiviert
        //dies passiert direkt beim tatsächlichen Start des Spiels,
        //so dass der AutosaveTimer halbwegs mit der Spielzeit übereinstimmt.
        updateAutoSaveState();
        //WerkzeugeeinlagerungsTask wird aktivert oder deaktiviert
        updateStopStoringToolsState();
        //updateGetRaceState();
        
        startClipBoardListener();
        
        // start check for S3-Stats
       	updateCheckForS3Stats( hostUserName, ip, mapName, goods, tournamentid );
       	
       	// stop loading ts-panel automaticly
        getGUI().getTeamSpeakPanel().stopLoading();

        gameState.userHasStartedGame();
        //der gerade veränderte gamestate darf durch diesen Aufruf nicht
        //weiter modifiziert werden, daher der Aufruf von
        //onCloseGamePreservingGameState(), wodurch nur
        //der gamestarter mit seinen observern beendet und das eventuell offene
        //Spiel aus der Anzeige gelöscht wird
        onCloseGamePreservingGameState( hostUserName, ip, mapName, goods, tournamentid, tournamentname );
    }
    
    public synchronized void actionGameStarted()
    {
    	IngameMessage.setIsInMiniChat(false);
        //Autosavetask wird aktiviert oder deaktiviert
        //dies passiert direkt beim tatsächlichen Start des Spiels,
        //so dass der AutosaveTimer halbwegs mit der Spielzeit übereinstimmt.
        updateAutoSaveState();
        //WerkzeugeeinlagerungsTask wird aktivert oder deaktiviert
        updateStopStoringToolsState();
        //updateGetRaceState();
        
        startClipBoardListener();
        
        // start check for S3-Stats
       	updateCheckForS3Stats();
       	
       	// stop loading ts-panel automaticly
        getGUI().getTeamSpeakPanel().stopLoading();

        gameState.userHasStartedGame();
        //der gerade veränderte gamestate darf durch diesen Aufruf nicht
        //weiter modifiziert werden, daher der Aufruf von
        //onCloseGamePreservingGameState(), wodurch nur
        //der gamestarter mit seinen observern beendet und das eventuell offene
        //Spiel aus der Anzeige gelöscht wird
        onCloseGamePreservingGameState(null);
    }

    public void actionMiniChatReached()
    {
        gameState.userHasJoinedGame();
    }

    /**
     * beendet den gameStarter und veranlasst das Löschen des eigenen
     * offenen Spiels aus der Liste der offenen Spiele.
     * Ändert aber nicht den GameState.
     * @param startOptions 
     */
    private void onCloseGamePreservingGameState(StartOptions startOptions)
    {
        try
        {
            if (gameStarter != null)
            {
                gameStarter.stopGame();
            }
        } catch (Exception e)
        {
            gui.displayError(e.getMessage());
        }

        openJoinGame = null;
        if (openHostGame != null)
        {
            try
            {
                sendProgramMessageCloseGame(openHostGame);
                if( startOptions != null ) {
                	saveGameSettings( openHostGame );
                	sendProgramMessageGameStarted(openHostGame);
                	setAsRunningHostGame(openHostGame);
                }
                else {
                	// prepare message to update info on webserver
	            	// -> responsive is not relevant for us here
	            	httpRequests httpRequests = new httpRequests();
	            	httpRequests.setUrl("https://lobby.siedler3.net/aLobbyRequests.html");
	            	httpRequests.addParameter("updategame", 1);
	            	httpRequests.addParameter("state", "closed");
	            	httpRequests.addParameter("hostname", getNick());
	            	httpRequests.addParameter("ip", ipToUse());
	            	// no token-security for this request
	            	httpRequests.setIgnorePhpSessId(true);
	            	try {
	            		httpRequests.doRequest();
	            	} catch (IOException e) {
	            	}
                }
                openHostGame.destroy();
            }
            finally
            {
                openHostGame = null;
            }
        }
        gui.removeButtonFromMapUserListWindow();
    }
    
    /**
     * beendet den gameStarter und veranlasst das Löschen des eigenen
     * offenen Spiels aus der Liste der offenen Spiele.
     * Ändert aber nicht den GameState.
     * @param startOptions 
     */
    private void onCloseGamePreservingGameState( String hostUserName, String ip, String mapName, int goods, int tournamentid, String tournamentname )
    {
        try
        {
            if (gameStarter != null)
            {
                gameStarter.stopGame();
            }
        } catch (Exception e)
        {
            gui.displayError(e.getMessage());
        }
        
        OpenGame openGame = this.getOpenGames().getOpenGame(ip, hostUserName);
    	if( openGame == null ) {
    		try {
	    		for (int i = 0; i < 1000; ++i)
	            {
	    			openGame = this.getRunningGames().getOpenGame(ip, hostUserName);
	                if (openGame != null)
	                {
	                    break;
	                }
	                Thread.sleep(20);
	            }
    		} catch (InterruptedException e) {
				Test.outputException(e);
			}
    	}
    	saveGameSettings( openGame );
    	if( hostUserName.length() > 0 && ip.length() > 0 && mapName.length() > 0 && goods >= 0 && openGame != null ) {
        	setAsRunningGame(hostUserName, ip, mapName, goods, tournamentid, tournamentname, openGame.getTournamentRound(), openGame.getTournamentGroupnumber(), openGame.getTournamentMatchnumber());
        }

        openJoinGame = null;
        if (openHostGame != null)
        {
            try
            {
                sendProgramMessageCloseGame(openHostGame);
                if( hostUserName.length() > 0 && ip.length() > 0 && mapName.length() > 0 && goods >= 0 ) {
                	setAsRunningGame(hostUserName, ip, mapName, goods, tournamentid, tournamentname, openHostGame.getTournamentRound(), openHostGame.getTournamentGroupnumber(), openHostGame.getTournamentMatchnumber());
                }
                openHostGame.destroy();
            }
            finally
            {
                openHostGame = null;
            }
        }
        gui.removeButtonFromMapUserListWindow();
    }

    /**
     * Save the game-Settings which are not saved in S3-Save-File, such as:
     * - League-State
     * - Tournament-Settings
     * in a separate file, which will be used from savemanager to open a savegame.
     * 
     * @param openGame
     */
    private void saveGameSettings(OpenGame openGame) {
    	if( openGame != null && false == openGame.getMap().contains("Savegame") ) {
    		// call gameDataFile to save the game-data
    		GameDataFile gameDataFile = new GameDataFile(this, openGame);
    		gameDataFile.add( "league", openGame.isLeagueGame() );
    		gameDataFile.add( "tournamentid", openGame.getTournamentId() );
    		gameDataFile.add( "tournamentname", openGame.getTournamentName() );
    		gameDataFile.add( "creationTime", openGame.getCreationTime() );
    		gameDataFile.add( "round", openGame.getTournamentRound() );
    		gameDataFile.add( "groupnumber", openGame.getTournamentGroupnumber() );
    		gameDataFile.add( "matchnumber", openGame.getTournamentMatchnumber() );
    		gameDataFile.add( "playerlist", openGame.getSortedPlayerListAsString() );
    		gameDataFile.add( "mapName", openGame.getMap());
    		gameDataFile.saveFile();
    	}
	}
    
    /**
     * Clean up the game-data-directory: delete all file which are older than 7 days
     */
    private void cleanUpGameDataDirectory() {
    	// save file in a separate directory, create it if it doesn't exists
		File gamedataDir = new File(SettlersLobby.configDir, "gamedata");
		if (!gamedataDir.exists()) {
			gamedataDir.mkdirs();
		}
		if (gamedataDir.isDirectory()) {
	        for (File f : gamedataDir.listFiles()) {
	        	long diff = new Date().getTime() - f.lastModified();
	        	if (diff > 7 * 24 * 60 * 60 * 1000) {
	        		f.delete();
	        	}
	        }
		}
    }

	/**
     * Sends a program message that the game started by the corresponding
     * hostUserName is closed.
     * The message will only be handled by other clients if hostUserName matches
     * the current user name.
     *
     * @param game
     */
    public void sendProgramMessageCloseGame(OpenGame game)
    {
        ircCommunicator.sendProgramMessage(protocol.createMsgCloseGame(game));
    }
    
    /**
     * Sends a program message that the game is started.
     * Adds the timestamp from UTC-timezone of this moment to the game.
     *
     * @param game
     */
    public void sendProgramMessageGameStarted(OpenGame game)
    {
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
    	game.setRunningGameStartTimestamp(calendar.getTimeInMillis());
        ircCommunicator.sendProgramMessage(protocol.createMsgGameStarted(game));
    }

    /**
     * wie {@link #onCloseGamePreservingGameState()}, verändert
     * aber zusätzlich noch den GameState.
     */
    private void onCloseGame()
    {
        try
        {
            if (gameState != null)
            {
                gameState.userHasLeftGame(false);
            }
        }
        finally
        {
            onCloseGamePreservingGameState(null);
        }
    }

    private void onCloseGameStandalone()
    {
        gui.closeMapWait();
        try
        {
            if (gameStarterThread != null && gameStarterThread.isAlive())
            {
                gameStarterThread.abort();
            }
            //in der standalone Variante wird kein
            //openHostGame Objekt erzeugt, daher muss es hier
            //auch nicht gelöscht werden.
        } catch (Exception e)
        {
            gui.displayError(e.getMessage());
        }
    }

    public synchronized void actionCloseMapWait()
    {
        gui.closeMapWait();
    }


    public void ircChatMessage(String user, String chatMessage, boolean bold)
    {
        messageHandler.addChatMessage(user, chatMessage, bold);
    }

    /**
     * Die empfangene Programm-Nachricht wird entsprechend umgesetzt.
     *
     * @param    programMessage    Die Programm-Nachricht
     * @param senderName userName, der die Programm-Nachricht geschickt hat
     * @param target Ziel, an den die Nachricht geschickt wurde (channel oder user)
     */
    public void ircProgramMessage(final String programMessage, final String senderName, final String target)
    {
        if (isConnectedWithServer)
        {
            //TODO by forcing the parsing to run in the Event Dispatch Thread,
            //also all listener will be running in the EDT.
            //not really sure if this is the right place to do it
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run()
                {
                    protocol.parseProgramMessage(programMessage, senderName, target, getNick());
                }
            });
        }
    }

    public void onPrgMsgQueryOpenGame(String senderName)
    {
        if (openHostGame != null)
        {
            sendOpenHostGame(senderName);
            //zusätzlich zum offenen Spiel muß auch noch die Liste der
            //Spieler gesendet werden (damit auch die neu in die Lobby
            //gekommenen Leute darüber informiert werden)
            scheduleSendMapUserListThreaded();
        }
    }

    /**
     * Sendet die Daten für das aktuell erstellte Spiel an user.
     * Falls user null ist, geht es an den ganzen Channel (inklusive
     * des eigenen users).
     * @param user recipient, or null for the complete channel
     */
    public void sendOpenHostGame(String user)
    {
        if (openHostGame == null)
        {
            return;
        }
        String programMessageData = protocol.createMsgGameInfo(openHostGame);

        if (user == null)
        {
            //sende Nachricht an den Channel + eigenen user
            ircCommunicator.sendProgramMessage(programMessageData);
        }
        else
        {
            //sende Nachricht nur an den user
            ircCommunicator.sendProgramMessageTo(programMessageData, user);
        }
    }
    
    /**
     * Sends data about the running game on requesting user.
     * 
     * @param user recipient, or null for the complete channel
     */
    private void sendRunningHostGame(String user)
    {
        if (runningHostGame == null)
        {
            return;
        }
        String programMessageData = protocol.createMsgRunningGameInfo(runningHostGame);
        if (user == null)
        {
            //sende Nachricht an den Channel + eigenen user
            ircCommunicator.sendProgramMessage(programMessageData);
        }
        else
        {
            //sende Nachricht nur an den user
            ircCommunicator.sendProgramMessageTo(programMessageData, user);
        }
    }

    /**
     * Entfernt das geöffnete Spiel aus der Spieleverwaltung und GUI.
     *
     * @param   ip  IP des Hosts
     * @param   hostUserName userName des Hosts
     */
    public void closeGame(String ip, String hostUserName)
    {
        OpenGame openGame;
        //use a do while loop. Although there should be at maximum
        //one open Game per user, there are strange errors
        //where this might not be true. So this code will clean
        //any game of the user with the specified ip
        do
        {
            openGame = openGames.getOpenGame(ip, hostUserName);
            if (openGame != null)
            {
                openGame.destroy();
            }
        } while (openGame != null);
    }
    
    /**
     * Removes running game from game-management and GUI.
     *
     * @param   ip  IP des Hosts
     * @param   hostUserName userName des Hosts
     */
    public void closeRunningGame(String ip, String hostUserName)
    {
        OpenGame runningGame;
        //use a do while loop. Although there should be at maximum
        //one open Game per user, there are strange errors
        //where this might not be true. So this code will clean
        //any game of the user with the specified ip
        do
        {
            runningGame = runningGames.getOpenGame(ip, hostUserName);
            if (runningGame != null)
            {
            	runningGame.destroy();
            }
        } while (runningGame != null);
    }
    
    public void ircWhisperedMessage(String user, String chatMessage)
    {
        messageHandler.addWhisperedMessage(user, "", chatMessage); //$NON-NLS-1$
        //TODO eigentlich müsste man aus showWhisperedMessageIngame() einen eigenen
        //messsageHandler machen, genau dafür ist der Mechanismus ja da
        showWhisperedMessageIngame(user + ": " + chatMessage); //$NON-NLS-1$
    }

    public void ircSendUserList(final IrcUser[] users)
    {
        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run()
            {
                gui.setUserList(users);
                // update the userlist for the autocomplete-function for the input-message-field in chat
                gui.updateAutoComplete(ircCommunicator.getUserListSortedByName());
            }
        });
    }

    public void showMapUserList(final UserInGameList userList)
    {
        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run()
            {
                gui.setMapUserList(userList);
            }
        });
    }
    
    public void showS4GameUserList(final UserInGameList userList)
    {
        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run()
            {
            	Test.output("setS4GameOptions dddd");
                gui.setS4GameOptions(userList, isHost(), true);
            }
        });
    }

    public void userHasLeft(String user)
    {
        if (showUserJoinsAndLefts)
        {
            messageHandler.addServerMessage(
                    MessageFormat.format(I18n.getString("SettlersLobby.MSG_CHANNEL_LEFT"), user)); //$NON-NLS-1$
        }
    }

    /**
     * This function handles all actions which will be done if someone joined the chat.
     * 
     * @param user	name of the user who joined the chat
     */
    public void onJoin(String user)
    {
        if (getNick().equals(user))
        {
            // user himself joined the channel => query for open games
            ircCommunicator.sendProgramMessageTo(protocol.createMsgQueryOpenGames(user), getChannel());
            // also query for running games
            ircCommunicator.sendProgramMessageTo(protocol.createMsgQueryRunningGames(user), getChannel());
            // and for vpn-state of each user
            ircCommunicator.sendProgramMessageTo(protocol.createMsgQueryVPNState(user), getChannel());
            // if user has a beta-version send info about in the channel
            // -> will only be visible for admin/mods and other beta-user
            if( isBetaUsed() ) {
            	ircCommunicator.sendProgramMessageTo(protocol.createMsgBetaUsersJoined(user), getChannel());
            	// and query for other beta-users
                ircCommunicator.sendProgramMessageTo(protocol.createMsgQueryBetaUsers(user), getChannel());
            }
        }

        if (config.isBuddy(user) && config.isNotifyIfBuddyComesOnline() && !gui.isChatWindowFocused()) {
            messageHandler.addServerMessage(
                    MessageFormat.format(I18n.getString("SettlersLobby.MSG_CHANNEL_ENTERED"), user)); //$NON-NLS-1$
            gui.flashWindowIfNotFocused();
        } else if (showUserJoinsAndLefts)
        {
            messageHandler.addServerMessage(
                    MessageFormat.format(I18n.getString("SettlersLobby.MSG_CHANNEL_ENTERED"), user)); //$NON-NLS-1$
        }
    }

    /**
     * Query for active streams via request from siedler3.net-server.
     */
    public void queryForActiveStreams() {
    	
    	SwingWorker<Void, Void> swingWorker = new SwingWorker<Void, Void>() {
    	
    		@Override
			protected Void doInBackground() throws Exception {
    		
		    	// get the content for this table from league-server
				httpRequests httpRequests = new httpRequests();
				httpRequests.setUrl("https://lobby.siedler3.net/streamChannels/aLobby/" + getNick());
				httpRequests.addParameter("getdata", 1);
				// no token-security for this request
				httpRequests.setIgnorePhpSessId(true);
				String response = "";
				// -> get the response-String
				try {
					response = httpRequests.doRequest();
				} catch (IOException e1) {
				}
				if( !httpRequests.isError() && response.length() > 0 ) {
					JSONObject obj = new JSONObject(response);
					JSONArray listArray = obj.getJSONArray("streams");
					if( listArray.length() > 0 ) {
						// go through the list and add each entry to the table for this tab
						for (int i = 0; i < listArray.length(); i++) {
							// get the nickname
							String user = "";
							if( !listArray.getJSONObject(i).isNull("user") ) {
								user = listArray.getJSONObject(i).get("user").toString();
							}
							
							// get the link
							String link = "";
							if( !listArray.getJSONObject(i).isNull("link") ) {
								link = listArray.getJSONObject(i).get("link").toString();
							}
							
							// get the online-date
							String onlinesince = "";
							if( !listArray.getJSONObject(i).isNull("onlinesince") ) {
								onlinesince = listArray.getJSONObject(i).get("onlinesince").toString();
							}
							
							getGUI().addStream(user, link, onlinesince);
						}
					}
				}
				return null;
    		}
			
    	};
    	swingWorker.execute();
	}
    
    /**
     * Query for community news via request from siedler3.net-server.
     */
    public void queryForCommunityNews() {
    	
    	SwingWorker<Void, Void> swingWorker = new SwingWorker<Void, Void>() {
    	
    		@Override
			protected Void doInBackground() throws Exception {
    		
		    	// get the content for this table from league-server
				httpRequests httpRequests = new httpRequests();
				httpRequests.setUrl("https://lobby.siedler3.net/messages/communitynews/" + getNick());
				httpRequests.addParameter("getdata", 1);
				// no token-security for this request
				httpRequests.setIgnorePhpSessId(true);
				String response = "";
				// -> get the response-String
				try {
					response = httpRequests.doRequest();
				} catch (IOException e1) {
				}
				if( !httpRequests.isError() && response.length() > 0 ) {
					JSONObject obj = new JSONObject(response);
					JSONArray listArray = obj.getJSONArray("news");
					if( listArray.length() > 0 ) {
						// go through the list and add each entry to the table for this tab
						for (int i = 0; i < listArray.length(); i++) {
							
							// get the priority
							String prio = "";
							if( !listArray.getJSONObject(i).isNull("priority") ) {
								prio = listArray.getJSONObject(i).get("priority").toString();
							}
							
							// get the md5
							String md5 = "";
							if( !listArray.getJSONObject(i).isNull("md5") ) {
								md5 = listArray.getJSONObject(i).get("md5").toString();
							}
							
							// get the date
							String date = "";
							if( !listArray.getJSONObject(i).isNull("date") ) {
								date = listArray.getJSONObject(i).get("date").toString();
							}
							
							// get the title
							String title = "";
							if( !listArray.getJSONObject(i).isNull("title") ) {
								title = listArray.getJSONObject(i).get("title").toString();
							}
							
							// get the text
							String text = "";
							if( !listArray.getJSONObject(i).isNull("text") ) {
								text = listArray.getJSONObject(i).get("text").toString();
							}
							
							// get the link
							String link = "";
							if( !listArray.getJSONObject(i).isNull("link") ) {
								link = listArray.getJSONObject(i).get("link").toString();
							}
							
							// get the type
							String type = "";
							if( !listArray.getJSONObject(i).isNull("type") ) {
								type = listArray.getJSONObject(i).get("type").toString();
							}
							
							getGUI().addCommunityNews(prio, md5, date, title, text, link, type, true);
						}
					}
				}
				// listen for changes in table for community news to update the tab-title
				// but first after initial load above
				gui.resetCommunityNewsCount(false);
				gui.getCommunityNewsTable().setDefaultRenderer(Object.class, new TableCellItalicRenderer());
				gui.getCommunityNews().addTableModelListener(new TableModelListener() {
			        @Override
			        public void tableChanged(TableModelEvent evt) {
			        	SwingUtilities.invokeLater(new Runnable()
			        	{
			        	    @Override
			        	    public void run()
			        	    {
			        	    	gui.resetCommunityNewsCount(true);
			        	    }
			        	});
			        }
			    });
				return null;
    		}
			
    	};
    	swingWorker.execute();
	}

	public void onNickChange(String oldNick, String newNick)
    {
        messageHandler.addServerMessage(MessageFormat.format(
                I18n.getString("SettlersLobby.MSG_NICK_CHANGE"), oldNick, newNick)); //$NON-NLS-1$
        //if oldNick hosted any games, remove the game from the opengames list,
        //even if it might still be open. But at least we do not keep a invalid entry
        closeGameByName(oldNick);
        if (gui != null)
        {
            gui.onNickChange(oldNick, newNick);
        }
    }

    /**
     * Nur zu verwenden, wenn die IP nicht bekannt ist!
     * Löscht das offene Spiel mit dem hostUserName.
     * @param hostUserName Der Spielname des Hosts des Spiels.
     */
    private void closeGameByName(String hostUserName) {
        OpenGame openGame;
        //use a do while loop. Although there should be at maximum
        //one open Game per user, there are strange errors
        //where this might not be true. So this code will clean
        //any game ot the user with the specified ip
        do
        {
            openGame = openGames.getOpenGame(hostUserName);
            if (openGame != null)
            {
                openGame.destroy();
            }
        } while (openGame != null);
    }

    /**
     * falls ein eigenes Spiel erstellt wurde, wird der user
     * aus der MapUserList entfernt, falls er darin vorkommen sollte.
     * @param sourceName
     */
    private void removeNickFromMapUserLists(String sourceName)
    {
        if (openHostGame != null)
        {
            //man hat ein Spiel offen
            //falls der user dort drin war, wird er gelöscht, selbst wenn er
            //tatsächlich noch im eigentlichen Spiel in Siedler3 drin sein sollte.
            //Dies kann leider nicht erkannt werden
            gameState.onPart(new UserInGame(sourceName), openHostGame.getMap());
        }
    }

    public void onPart(String sourceName, String sourceHostname)
    {
        //der sourceHostname enthält leider nicht die komplette IP
        //daher kann man nur nach dem Nick suchen
        closeGameByName(sourceName);
        removeNickFromMapUserLists(sourceName);
    }

    public void onQuit(String sourceName, String sourceHostname, String reason)
    {
    	Test.output("onQuit 1: " + sourceName);
        //der sourceHostname enthält leider nicht die komplette IP
        //daher kann man nur nach dem Nick suchen
        closeGameByName(sourceName);
        removeNickFromMapUserLists(sourceName);
        gui.removeStream(sourceName);
        
        Test.output("onQuit 2: " + reason);
        
        // if reason for the quit is a timeout:
        if( reason.contains("Ping timeout") ) {
        	
        	Test.output("onQuit 3");
        	
	        // remove open game if user has open one as host
	        OpenGame openGame = this.getOpenGames().getOpenGame(sourceName);
	        if( openGame != null ) {
	        	openGame.destroy();
	        }
	        
	        Test.output("onQuit 4");
	        
	        // remove running game if user has open one as host
	        OpenGame runningGame = this.getRunningGames().getOpenGame(sourceName);
	        if( runningGame != null ) {
	        	runningGame.destroy();
	        }
	        
	        Test.output("onQuit 5");
	        
        }
    }

    /**
     * Send info about increased playernumber in open game.
     * 
     * @param hostUserName
     */
    public void onSendIncrementPlayerNumber(String hostUserName)
    {
        hostUserNameForDecrementCommand = hostUserName;
        ircCommunicator.sendProgramMessageTo(protocol.createMsgIncrementCurrentPlayerNumber(hostUserName), hostUserName);
    }

    /**
     * Send info about decreased playernumber in open game.
     */
    public void onSendDecrementPlayerNumber()
    {
        userWasActive();
        if (hostUserNameForDecrementCommand != null)
        {
            ircCommunicator.sendProgramMessageTo(protocol.createMsgDecrementCurrentPlayerNumber(hostUserNameForDecrementCommand),
                                                 hostUserNameForDecrementCommand);
        }
    }

    private static void writeInLogFile()
    {
        File log = new File(SettlersLobby.configDir, "log.txt");
        File logOld = new File(SettlersLobby.configDir, "log.1.txt");
        try
        {
            if (logOld.exists())
            {
                logOld.delete();
            }
            if (log.exists())
            {
                log.renameTo(logOld);
            }
        }
        catch (Exception e)
        {
            //ignore
        }
        try
        {
            PrintStream ps = new PrintStream(new FileOutputStream(log));
            System.setOut(ps);
            System.setErr(ps);
        } catch (FileNotFoundException e)
        {
            // Tue nichts, gibt es eben keine log.txt
        }
    }

    public void error(String error)
    {
        gui.displayError(error);
    }

    private String[] getS3Maps(String dirSuffix)
    {
    	return getS3Maps(dirSuffix, null);
    }
    
    private String[] getS4Maps(String dirSuffix, String fileExtension)
    {
    	return getS4Maps(dirSuffix, null, fileExtension);
    }
    
    /**
     * Get S3-Maps from Directory, if filter is set use it to
     * filter for filenames.
     * 
     * @param dirSuffix
     * @param filter
     * @return
     */
    private String[] getS3Maps(String dirSuffix, String filter)
    {
        String[] maps = new File(getS3MapDirectory(), dirSuffix).list(new FilenameFilter()
        {
            @Override
            public boolean accept(File dir, String fileName)
            {
            	// if filter is set and the filename doesn't consists the filter-string,
            	// than do not use this file
            	if( filter != null && !fileName.toLowerCase().contains(filter.toLowerCase()) ) {
            		return false;
            	}
                int index = fileName.lastIndexOf("."); //$NON-NLS-1$
                return (index != -1 && fileName.substring(index + 1).toLowerCase()
                        .startsWith("map")); //$NON-NLS-1$
            }
        });
        if (maps == null)
        {
            // error-case, e.g. if directory not found
            maps = new String[0];
        }
        return maps;
    }
    
    /**
     * Get S4-Maps from Directory, if filter is set use it to
     * filter for filenames.
     * 
     * @param dirSuffix
     * @param filter
     * @return
     */
    private String[] getS4Maps(String dirSuffix, String filter, String fileExtension)
    {
        String[] maps = new File(getS4MapDirectory(), dirSuffix).list(new FilenameFilter()
        {
            @Override
            public boolean accept(File dir, String fileName)
            {
            	// if filter is set and the filename doesn't consists the filter-string,
            	// than do not use this file
            	if( filter != null && !fileName.toLowerCase().contains(filter.toLowerCase()) ) {
            		return false;
            	}
                int index = fileName.lastIndexOf("."); //$NON-NLS-1$
                return (index != -1 && fileName.substring(index + 1).toLowerCase()
                        .startsWith(fileExtension)); //$NON-NLS-1$
            }
        });
        if (maps == null)
        {
            // error-case, e.g. if directory not found
            maps = new String[0];
        }
        return maps;
    }

    public String[] getUserMaps()
    {
        return getS3Maps(ALobbyConstants.PATH_USER);
    }
    
    public String[] getUserMaps( String filter )
    {
        return getS3Maps(ALobbyConstants.PATH_USER, filter);
    }

    public String[] getMultiS3Maps()
    {
        return getS3Maps(ALobbyConstants.PATH_S3MULTI);
    }
    
    public String[] getS4MultiplayerMaps()
    {
        return getS4Maps(ALobbyConstants.PATH_S4MULTI, "map");
    }
    
    public String[] getS4UserMaps()
    {
        return getS4Maps(ALobbyConstants.PATH_S4USER, "map");
    }
    
    public String[] getS4Savegames()
    {
        return getS4Maps(".." + File.separator + ALobbyConstants.PATH_S4SAVEGAMES, "exe");
    }

    public Object[] getFavoriteS3Maps()
    {
        List<String> favoritemaps = config.getFavoriteS3Maps();
        List<Object> maps = new ArrayList<Object>();
        //Die Strings aus der Favoritenliste werden in
        //S3-Map bzw. S3RandomMap Objekte umgewandelt
        //dies ist zwingend notwendig für die Mehrsprachigkeit
        //bei Random maps
        for (String mapPath : favoritemaps)
        {
            ISMap map = S3Map.newInstance(mapPath);
            if (map.isValid())
            {
                maps.add(map);
            }
        }
        return maps.toArray();
    }
    
    public Object[] getFavoriteS4Maps()
    {
        List<String> favoritemaps = config.getFavoriteS4Maps();
        List<Object> maps = new ArrayList<Object>();
        for (String mapPath : favoritemaps)
        {
            ISMap map = S4Map.newInstance(mapPath);
            if (map.isValid())
            {
                maps.add(map);
            }
        }
        return maps.toArray();
    }

    public String[] getSavegameFileNames() {
        String gamePath = config.getGamePathS3();
        String[] files =  new File(SaveGame.getSavegameDirFromGamePath(gamePath))
            .list(new SaveGame.SavegameFilenameFilter());
        if (files == null)
        {
            files = new String[0];
        }
        return files;
    }

    /**
     * Gibt alle Savegames, die sich im Savegameordner befinden, zurück.
     * @return Ein Array von Fileobjekten, die die Savegames repräsentieren.
     */
    public SaveGame[] getSavegames() {
        String gamePath = config.getGamePathS3();
        File[] saveGameFiles = SaveGame.getSavegameFiles(gamePath);
        if (saveGameFiles == null)
        {
            return new SaveGame[0];
        }
        saveGameFiles = sortFiles(saveGameFiles);
        SaveGame[] saveGames = new SaveGame[saveGameFiles.length];
        for (int i = 0; i < saveGameFiles.length; i++) {
            saveGames[i] = new SaveGame(saveGameFiles[i], config.getGameDateTimeFormat());
        }
        return saveGames;
    }

    public SaveGame[] getAllSavegames() {
        if (saveManager == null)
        {
        	return getSavegames();
        }
        File[] saveManagerFiles = saveManager.getAllBackups();
        File[] saveGameFiles = saveManager.getAllNotBackupedGames();
        File[] allSaveFiles;

        if (saveManagerFiles == null && saveGameFiles == null)
        {
            return new SaveGame[0];
        } else if (saveManagerFiles == null && saveGameFiles != null)
        {
        	allSaveFiles = saveGameFiles;
        } else if (saveManagerFiles != null && saveGameFiles == null)
        {
        	allSaveFiles = saveManagerFiles;
        } else {
	        allSaveFiles = new File[saveManagerFiles.length + saveGameFiles.length];
	        System.arraycopy(saveManagerFiles, 0, allSaveFiles, 0, saveManagerFiles.length);
	        System.arraycopy(saveGameFiles, 0, allSaveFiles, saveManagerFiles.length, saveGameFiles.length);
        }
        
        allSaveFiles = sortFiles(allSaveFiles);
        SaveGame[] saveGames = new SaveGame[allSaveFiles.length];
        for (int i = 0; i < allSaveFiles.length; i++) {
            saveGames[i] = new SaveGame(allSaveFiles[i], config.getGameDateTimeFormat());
        }
        return saveGames;
    }
    
    /**
     * Sortiert das übergebene File Array absteigend nach letztem Änderungsdatum
     *
     * @param saveGameFiles
     *            Das zu sortierende File Array.
     * @return Das sortierte File Array.
     */
    private File[] sortFiles(File[] saveGameFiles) {
        File buffer;
        int max = 0;
        for (int i = 0; i < saveGameFiles.length; i++) {
            max = i;
            for (int ii = i; ii < saveGameFiles.length; ii++) {
                if (saveGameFiles[ii].lastModified() > saveGameFiles[max].lastModified()) {
                    max = ii;
                }
            }
            buffer = saveGameFiles[i];
            saveGameFiles[i] = saveGameFiles[max];
            saveGameFiles[max] = buffer;
        }
        return saveGameFiles;
    }

    /**
     * Get the nickname of the actual user.
     * 
     * @return
     */
    public String getNick()
    {
        return config.getUserName();
    }

    /*public String getGamePath()
    {
        return config.getGamePathS3();
    }*/

    /**
     * Diese Datei wird später als Programmstart fungieren. Sie startet eine
     * Instanz der SettlersLobby.
     *
     * @param args
     */
    public static void main(String[] args)
    {
        //by disabling the buffering of swing, this seems to help to avoid
        //a memory leak.
        //see http://bugs.java.com/view_bug.do?bug_id=6928788
        //Otherwise when the display changes, java stores a displayChangeListener,
        //which seems to store a handle to a volatile image of type
        //sun.awt.image.BufImgVolatileSurfaceManager (java 7) or
        //sun.java2d.d3d.D3DVolatileSurfaceManager (java 8)
        //which will never be garbage collected afterwards (at least some of them).
        System.setProperty("swing.volatileImageBufferEnabled", "false");
        
        //setting the following option would disable d3d on windows, so that
        //also with java 8 the class sun.awt.image.BufImgVolatileSurfaceManager
        //is used. But currently this is not needed.
        //System.setProperty("sun.java2d.d3d", "false");
        
        try
        {
            commandLineOptions = new CommandLineOptions(args);
            if (commandLineOptions.getConfigDir() != null)
            {
                configDir = commandLineOptions.getConfigDir();
            }
            try
            {
                File file = new File(configDir);
                if (!file.exists())
                {
                    file.mkdirs();
                }
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }

            new SettlersLobby();
            
            Test.output("command line arguments: " + Arrays.toString(args));

            if (commandLineOptions.getInstallerDir() != null)
            {
                deleteInstaller(commandLineOptions.getInstallerDir());
            }
        }
        catch (Throwable t)
        {
            Test.outputException(t);
            showErrorWithOpenConfigDir(null, formatExceptionText(t), I18n.getString("SettlersLobby.ERROR"));
            System.exit(1);
        }
    }


    private static void deleteInstaller(String installerPath)
    {
        File installer = new File(installerPath);
        File installerAsc = new File(installer.getAbsolutePath() + ".asc");
        if (installer.exists() && installer.isFile())
        {
            try
            {
                String tmpDir = System.getProperty("java.io.tmpdir");
                tmpDir = new File(tmpDir).getAbsolutePath();
                if (tmpDir.equalsIgnoreCase(installer.getParentFile().getParent()))
                {
                	Test.output("delete " + installer.getAbsolutePath());
                    installer.delete();
                    if (installerAsc.exists() && installerAsc.isFile())
                    {
                        installerAsc.delete();
                    }
                    installer.getParentFile().delete();
                }
                else
                {
                	Test.output("skipping deletion of " + installer.getAbsolutePath());
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public static String formatExceptionText(Throwable t)
    {
        return (t.getCause() != null) ? t.toString() + "\n" + t.getCause().toString() : t.toString();
    }

    public void onCommandAnswer(String command)
    {
        messageHandler.addChatMessage(ALobbyConstants.MSG_SERVER, command, false);
    }

    /**
     * wird aufgerufen, wenn man selber gekickt wurde
     * @param reason
     */
    public void onKick(String reason)
    {
        actionWantsToLogOut();
        gui.displayError(MessageFormat.format(
                I18n.getString("SettlersLobby.MSG_KICK") , reason));         //$NON-NLS-1$
    }

    /**
     * wird aufgerufen, wenn jemand aus dem channel gekickt wurde.
     *
     * @param recipientNick user der gekickt wurde
     * @param kickerNick user der den Kick durchgeführt hat
     * @param kickerHostname
     */
    public void onKick(String recipientNick, String kickerNick,
            String kickerHostname)
    {
        //man hat keine IP vom recipientNick
        //falls vom Spieler noch ein Spiel offen ist, wird es geschlossen
        this.closeGameByName(recipientNick);
        removeNickFromMapUserLists(recipientNick);
    }


    public boolean isShowUserJoinsAndLefts()
    {
        return showUserJoinsAndLefts;
    }

    public void switchShowUserJoinsAndLefts()
    {
        showUserJoinsAndLefts = !showUserJoinsAndLefts;
    }
    
    /**
     * Return the local IP (LAN-IP) of the system where the aLobby-client runs on.
     */
    private String getLocalIp() {
    	if( ircCommunicator != null && ircCommunicator.getInetAddress() != null ) {
    		return ircCommunicator.getInetAddress().getHostAddress();
    	}
    	return "";
    }

    /**
     * versucht die externe IP mit Hilfe des IrcCommunicators zu ermitteln.
     * Wenn die externe nicht bekannt ist, wird zumindest die interne IP gesetzt,
     * sofern der IrcCommunicator verbunden ist und noch keine andere IP
     * bekannt ist.
     *
     * Wenn die Lobby im localMode gestartet wurde, wird nur auf getInetAddress()
     * zurückgregriffen.
     *
     */
    private void getAndStoreExternalIp()
    {
        if (!ircCommunicator.isConnected())
        {
            return;
        }
        if (config.isLocalMode())
        {
            ip = getLocalIp();
        } else
        {
            InetAddress address = ircCommunicator.getDccInetAddress();
            if (address != null)
            {
                if (this.ip == null)
                {
                    // bisher keine IP gespeichert, auf jeden Fall die vom IRC
                    // nehmen
                    setIp(address.getHostAddress());
                } else
                {
                    if (!this.ip.equals(address.getHostAddress()))
                    {
                        // IRC liefert eine andere IP als die bisher
                        // gespeicherte.
                        // übernehme die neue,
                        // eventuell lag eine 24h-Zwangstrennung vor
                        Test.output("Alte IP : " + this.ip); //$NON-NLS-1$
                        setIp(address.getHostAddress());
                        Test.output("Neue IP : " + this.ip); //$NON-NLS-1$
                    } else
                    {
                        // IP passt schon
                    }
                }
            } else
            {
                // getInetAddress() enthält einen gültigen Wert,
                // falls der IrcCommunicator verbunden ist.
                // Mit einem Router ist es die interne IP, sonst sogar die
                // externe
    			address = ircCommunicator.getInetAddress();
                if (this.ip == null)
                {
                    setIp(address.getHostAddress());
                    Test.output("WARNING, using internal IP: " + this.ip); //$NON-NLS-1$
                    // gleichzeitig startet die IP-Abfrage via URL
                    // falls diese einen Wert liefert, wird er benutzt
                    if (ipViaUrl != null && ipViaUrl.isAlive())
                    {
                        // alten thread anhalten, falls noch vorhanden
                        ipViaUrl.interrupt();
                    }
                    ipViaUrl = new IpViaUrlRetriever(this);
                    ipViaUrl.start();
                } else
                {
                    // Es ist schon eine IP gespeichert.
                    // Da vom IRC Server keine externe IP gemeldet wurde
                    // (dccInetAddress==null), wird die bisherige beibehalten
                    // falls es keine lokale Adresse ist, vielleicht
                    // doch die vom IRC übernehmen?
                }
            }
        }
    }

    /**
     * @return the xmlConfig
     */
    public XMLCommunicator getXmlConfig()
    {
        return config.getXmlConfig();
    }

    /**
     * setzt die IP.
     *
     * @param ip
     */
    public synchronized void setIp(String ip)
    {
        // die Methode muss sychronized sein, da IP via URL
        // in einem eigenen thread abläuft.
        // das Lesen sollte auch ohne synchronized möglich und korrekt sein
        this.ip = ip;
    }

    /**
     * @return the ip
     */
    public String getIp()
    {
        return ip;
    }

    /**
     * We always check the IP if asked for, as the VPN status may have changed.
     * 
     * @return the current VPN ip
     */
    public String getVpnIpFromSystem()
    {
        Enumeration<NetworkInterface> nets;
		try {
			nets = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e) {
			Test.outputException(e);
			return this.vpnIp;
		}
		boolean ipFound = false;
        for (NetworkInterface netint : Collections.list(nets)) {
	        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
	        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
	        	if (inetAddress.getHostAddress().startsWith(ALobbyConstants.VPN_IP_RANGE)) {
        			setVpnIp(inetAddress.getHostAddress());
	            	/*Test.output("Display name: " + netint.getDisplayName());
	            	Test.output("Name: " + netint.getName());
	        		Test.output("InetAddress: " + this.ipVpn);*/
	        		ipFound = true;
	        		break;
	        	}
	        	
	        }
        }
        
        if (!ipFound) {
        	setVpnIp(null);
        }
        return this.vpnIp;
    }

    public synchronized void setVpnIp(String ipVpn)
    {
        // die Methode muss sychronized sein, da IP get
        // in einem eigenen thread abläuft.
        // das Lesen sollte auch ohne synchronized möglich und korrekt sein
        this.vpnIp = ipVpn;
    }

    /**
     * @return the relative path to the openvpn exe
     */
    public String getOpenVpnPath()
    {
    	if (IS_WINE) {
    		return OPENVPN_BIN_WRAPPER;
    	} else {
    		return OPENVPN_EXE;
    	}
    }

	public void startVpn() throws Exception
	{
	    gui.setVpnText("\uf127", Color.yellow, I18n.getString("SettlersLobby.VPN_STARTING"));
		if (vpnStarter != null && vpnStarter.isAlive())
		{
		    //wenn VPN noch läuft, hätte diese Funktion gar nicht aufgerufen
		    //werden dürfen.
		    //Kill VPN
			vpnStarter.destroyVpn();
		    Thread.sleep(500);
		    Test.output("interrupting vpnStarter");
		    vpnStarter.interrupt();
		    //wait until the thread finally ends
		    vpnStarter.join();
		}
		try
		{
			vpnStarter = new VpnStarter(this);
			vpnStarter.start();
		} catch (Exception e)
		{
			if (vpnStarter != null) {
				try {
					vpnStarter.destroyVpn();
				} catch (Exception e2) {
					Test.outputException(e2);
				} finally {
					vpnStarter = null;
				}
			}
		    Test.outputException(e);
			throw new Exception(MessageFormat.format("Error starting VPN!",
			                                         e.getMessage() != null ? e.getMessage() : e.toString()));
		}
	}

	public void stopVpn() throws Exception
	{
		if ((vpnStarter != null && vpnStarter.isAlive()) || IS_WINE)
		{
		    //wenn VPN noch läuft, hätte diese Funktion gar nicht aufgerufen
		    //werden dürfen.
		    //Kill VPN
			vpnStarter.destroyVpn();
		    Thread.sleep(500);
		    Test.output("interrupting vpnStarter");
		    vpnStarter.interrupt();
		    //wait until the thread finally ends
		    vpnStarter.join();
		    gui.setVpnText("\uf127", Color.red, I18n.getString("SettlersLobby.VPN_OFF"));
		    InetAddress address = ircCommunicator.getDccInetAddress();
		    gui.showIp(address.getHostAddress());
		    sendProgramMessageVPNState(false);
		    ircCommunicator.onVPNStateChange(getNick(), false);
		    OriginalIpVpn = null;
		}
	}

    public String getDefaultAwayMsg()
    {
        return config.getDefaultAwayMsg();
    }

    public void setDefaultAwayMsg(String defaultAwayMsg)
    {
        config.setDefaultAwayMsg(defaultAwayMsg);
    }

    /**
     * Liefert die Zeit, nach wievielen Millisekunden der Benutzer automatisch
     * in den Awaymodus gehen soll.
     * @return Die Millisekunden.
     */
    public int getAwayTime()
    {
        return config.getAwayTime();
    }

    /**
     * Liefert die URL zur Hilfewebseite zurück.
     * @return Die URL der Hilfewebseite.
     */
    public String getHelpPath()
    {
        return config.getHelpPath();
    }

    public boolean isLocalMode()
    {
        return config.isLocalMode();
    }

    public boolean isActivateAwayOnIdle()
    {
        return config.isActivateAwayOnIdle();
    }

    public boolean isActivateAwayOnGameStart()
    {
        return config.isActivateAwayOnGameStart();
    }
    
    public AwayStatus getAwayStatus() {
    	return awayStatus;
    }

    public void setAwayStatus(AwayStatus newAwayStatus)
    {
        awayStatus.destroy();
        awayStatus = newAwayStatus;
    }

    /**
     * Actions done if S3 is shut down, e.g. after end of a game.
     */
    synchronized public void onS3ShutDown()
    {
        stopAutosaveTask();
        stopCheckForS3Stats();
        stopStopToolStoringTask();
        stopClipBoardListener();

        if (gameState.isInMiniChat())
        {
            //nur wenn der user im Minichat war,
            //muss eine Decrement Nachricht gesendet werden.
            //der Gamestate muss abgefragt werden, bevor
            //man actionCreateGameAborted() aufruft
            onSendDecrementPlayerNumber();
        }
        removeGameFromRunningGames();
        actionCreateGameAborted();
        awayStatus.onGameClosed();
        gameState.userHasLeftGame(false);
        unselectGameInGui();
        stopAutoSaveTaskStateByKey();
        KeyBlockStarter keyBlockStarter = gameStarter.getKeyBlockStarter();
        if( keyBlockStarter instanceof KeyBlockStarter ) {
            keyBlockStarter.destroyKeyBlock();
        }
    }

    /**
     * Actions done if S4 is shut down, e.g. after end of a game.
     */
    synchronized public void onS4ShutDown() {
    	Test.output("s4 was shut down");
		removeGameFromRunningGames();
		setAsOpenJoinGame(null);
		setAsOpenHostGame(null);
		awayStatus.onGameClosed();
		gameState.userHasLeftGame(true);
		unselectGameInGui();
	}

    public void doAway(String msg)
    {
        awayStatus.doAway(msg);
    }

    public void doBack()
    {
        awayStatus.doBack();
        //nochmal händisch userWasActive() aufrufen
        //ansonsten funktioniert das zurücksetzen über das popupmenü nicht
        userWasActive();
    }

    public boolean isAway(){
        if(awayStatus != null && awayStatus.isUserAway())
        {
            return true;
        }

        return false;
    }

    public void sendProgramMessageAwayMessage(String awayMessage)
    {
        //die away programm nachricht schicken, es reicht sie an den channel zu senden
        ircCommunicator.sendProgramMessageTo(protocol.createMsgAwayMessage(getNick(), awayMessage), getChannel());
    }
    
    /**
     * Send info about new vpn-state of actual user in the channel.
     * 
     * @param vpnState	true if vpn is connected / false if not
     */
    public void sendProgramMessageVPNState(boolean vpnState)
    {
        ircCommunicator.sendProgramMessageTo(protocol.createMsgVPNState(getNick(), vpnState), getChannel());
    }
    
    /**
     * Send info about new vpn-state of actual user to a single user.
     * 
     * @param vpnState	true if vpn is connected / false if not
     * @param userName	nickname of receiver
     */
    public void sendProgramMessageVPNState(boolean vpnState, String userName)
    {
        ircCommunicator.sendProgramMessageTo(protocol.createMsgVPNState(getNick(), vpnState), userName);
    }

    public void sendProgramMessageAwayBack()
    {
        //die away_back programm nachricht schicken, es reicht sie an den channel zu senden
        ircCommunicator.sendProgramMessageTo(protocol.createMsgAwayBack(getNick()), getChannel());
    }

    public void sendGameProgramMessage(String token, UserInGame recipient, String map)
    {
        ircCommunicator.sendProgramMessageTo(protocol.createMsgUpdateGameState(token, getNick(), map),
                                             recipient.getNick());
    }
    
    public void sendGameProgramMessage(String token, UserInGame recipient, String map, String playerIp)
    {
        ircCommunicator.sendProgramMessageTo(protocol.createMsgUpdateGameState(token, getNick(), map, playerIp),
                                             recipient.getNick());
    }

    public void sendBroadcastGameProgramMessage(String token, String userToUpdate, String mapName)
    {
        String message = protocol.createMsgBrodcastUpdateGameState(token, getNick(), mapName, userToUpdate);
        ircCommunicator.sendProgramMessage(message);
    }

    public void onTopic(String channel, String topic)
    {
        gui.setTopic(channel, topic);
    }

    public GameState getGameState()
    {
        return gameState;
    }

    public void setGameState(GameState gameState)
    {
        this.gameState = gameState;
        IngameMessage.setIsInMiniChat(gameState != null && gameState.isInMiniChat());
    }

    protected void joinChannel()
    {
        //when this method is called, the GUI is already active
        //so now ask for the /motd
        ircCommunicator.sendMotd();
        ircCommunicator.joinChannel();
        setGameState(new GameStateInLobby(this, new UserInGame(getNick())));
    }

    /**
     * Die MapUserList soll an alle verteilt werden. Erst wenn mindestens
     * 2 Sekunden lang kein neuer User hinzugekommen ist, wird die dann
     * aktuelle Liste tatsächlich verteilt. Dieses dient als flooding Schutz,
     * damit der Host nicht ständig die Liste verteilt, wenn ein user
     * mehrfach schnell hintereinander versucht zu joinen.
     * Es werden eventuell sogar zwei Nachrichten benötigt, um die komplette
     * Liste zu übertragen, daher sollte das möglichst selten passieren.
     *
     */
    // TODO should be rewritten after launcher-release
    public void scheduleSendMapUserList()
    {
        scheduleSendMapUserListViaTask();
        //es gibt Berichte, dass das Update der Liste häufig nicht
        //kommt oder erst sehr spät.
        //Als workaround wird versucht, auch noch die threaded-Variante
        //zu starten. Ob das wirklich etwas bewirkt ist ungewiss.
        if( config.getS3Versionnumber() < 300 ) {
        	scheduleSendMapUserListThreaded();
        }
    }

    private void scheduleSendMapUserListViaTask()
    {
        if (ircSendMapUserListTask != null)
        {
            ircSendMapUserListTask.cancel();
        }
        ircSendMapUserListTask = new TimerTaskCompatibility() {
            @Override
            public void run()
            {
                sendMapUserListGameProgramMessage();
                //falls parallel noch ein Thread angestossen wurde,
                //wird er abgebrochen
                if (ircSendMapUserListThread != null)
                {
                    ircSendMapUserListThread.interrupt();
                }
            }
        };
        //erst nach 2 Sekunden ausführen
        ircSendMapUserListTask.schedule(2000);
    }

    /**
     * Diese Methode wird benutzt, um User, die zwischenzeitlich den Channel betreten
     * haben, über die Spieler in den offenen Spielen zu informieren.
     * Die Funktionalität liess sich nicht mit dem bisherigen
     * {@link #scheduleSendMapUserList()} umsetzen, daher eine weitere Funktion, die
     * sich mehr Zeit lässt um die Aktualisierung zu verteilen, damit auch hier
     * die Liste nicht zu oft verschickt wird.
     *
     */
    private void scheduleSendMapUserListThreaded()
    {
    	Test.output("threaded mapuserlist 1");
        if (ircSendMapUserListThread != null &&
                ircSendMapUserListThread.isAlive())
        {
            //es läuft bereits ein Thread
            return;
        }

        ircSendMapUserListThread = new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    //5 bis 10 Sekunden warten, bevor gesendet wird
                	Test.output("threaded mapuserlist 2");
                    Thread.sleep(5000 + random.nextInt(5000));
                    if (!isInterrupted())
                    {
                        sendMapUserListGameProgramMessage();
                    }
                }
                catch (Exception e) {
                }
            }
        };
        ircSendMapUserListThread.setName(ircSendMapUserListThread.getName() + "-MapUserList"); //$NON-NLS-1$
        ircSendMapUserListThread.start();
    }

    /**
     * sendet die aktuelle MapUserList an den kompletten Channel,
     * damit alle Mitspieler informiert sind. Andere Clients, die nicht
     * im Spiel sind, ignorieren diese Nachricht.
     * Indem man an den kompletten Channel schickt, muß die Liste nur
     * einmal verschickt werden. Würde man jeden Mitspielern einzeln
     * adressieren, müßte die Liste entsprechend oft verschickt werden.
     * Gerade bei 20 Mitspielern bekäme man dort schnell flooding-Probleme.
     * So bekommen jetzt zwar mehr Leute diese Nachricht, als eigentlich
     * nötig, aber dies wird als unkritisch bewertet.
     *
     */
    public void sendMapUserListGameProgramMessage()
    {
        List<String> messages = new ArrayList<String>();
        // launcher playerlist is saved only in openHostGame
        if( config.getS3Versionnumber() >= 300 ) {
        	messages.add(ALobbyConstants.TOKEN_MAP_USER_LIST + ";" + getNick() + ";" + openHostGame.getMap() + ";" + openHostGame.getPlayerListAsString());
        }
        else {
        	messages = gameState.getMapUserListGameProgramMessage();        	
        }
        for (String message : messages)
        {
        	Test.output("userlist-message: " + message);
            ircCommunicator.sendProgramMessage(message);
        }
    }

    private String getChannel()
    {
        return config.getChannel();
    }

    public GUI getGUI()
    {
        return gui;
    }

    public XMLCommunicator getDefaultConfig()
    {
        return config.getDefaultConfig();
    }

    /**
     * Aktiviert oder deaktiviert Autosave Task, je nach
     * {@code Configuration#isAutoSave()}.
     */
    public void updateAutoSaveState()
    {
        if (isAutoSave())
        {
            Test.output("Autosave aktiv"); //$NON-NLS-1$
            if (isIngameOptionsStandalone || gameStarter.isS3Running())
            {
                //Autosave wird nur aktiviert, wenn auch ein Spiel läuft
                Test.output("Autosave schedule by following interval: " + getAutoSaveTimer());    //$NON-NLS-1$
                scheduleAutosave(getAutoSaveTimer());
            }
        }
        else
        {
            Test.output("Autosave cancel"); //$NON-NLS-1$
            stopAutosaveTask();
        }
        // start monitoring for pressed ctrl+s-combination to change state of autosavetask
        stopAutoSaveTaskStateByKey();
    	autoSaveTaskStateByKey = new AutoSaveTaskStateByKey(this);
    	autoSaveTaskStateByKey.startMonitoring();
    }

    private void stopAutoSaveTaskStateByKey() {
		if( autoSaveTaskStateByKey != null ) {
			autoSaveTaskStateByKey = null;
		}
	}

	/**
     * Aktiviert oder deaktiviert den Werkzeugeinlagerungsstop Task, je nach
     * {@code Configuration#isStopStoringTools()}.
     */
    public void updateStopStoringToolsState()
    {
        if (isStopStoringTools())
        {
            Test.output("StopStoringTools enabled"); //$NON-NLS-1$
            if (isIngameOptionsStandalone || gameStarter.isS3Running())
            {
                Test.output("StopStoringTools schedule"); //$NON-NLS-1$
                scheduleStopToolStoringTask();
            }
        }
        else
        {
            Test.output("StopStoringTools disabled"); //$NON-NLS-1$
            stopStopToolStoringTask();
        }
    }
    
    /**
     * Enable or disable the race-check after game-start.
     * {@code Configuration#isStopStoringTools()}.
     */
    public void updateGetRaceState()
    {
        if (isIngameOptionsStandalone || gameStarter.isS3Running())
        {
            Test.output("getRaceTask schedule"); //$NON-NLS-1$
            scheduleGetRaceTask();
        }
    }
    
    /**
     * Enable or disable the check for S3 Stats
     * {@code Configuration#isAutoSave()}.
     */
    public void updateCheckForS3Stats( )
    {
    	if( config.getS3Versionnumber() < 300 ) {
	    	Test.output("isS3Stats: " + isS3Stats());
	        if (isS3Stats())
	        {
	            Test.output("S3-Stats-Check enabled"); //$NON-NLS-1$
	            if (isIngameOptionsStandalone || gameStarter.isS3Running())
	            {
	                Test.output("S3-Stats-Check schedule started");    //$NON-NLS-1$
	                scheduleCheckForS3Stats();
	            }
	        }
	        else
	        {
	            Test.output("S3-Stats-Check disabled #1"); //$NON-NLS-1$
	            stopCheckForS3Stats();
	        }
    	}
    }
    
    /**
     * Enable or disable the check for S3 Stats
     * {@code Configuration#isAutoSave()}.
     */
    public void updateCheckForS3Stats( StartOptions startOptions )
    {
    	if( config.getS3Versionnumber() < 300 ) {
	    	Test.output("isS3Stats: " + isS3Stats());
	        if (isS3Stats())
	        {
	            Test.output("S3-Stats-Check enabled"); //$NON-NLS-1$
	            if (isIngameOptionsStandalone || gameStarter.isS3Running())
	            {
	                Test.output("S3-Stats-Check schedule started");    //$NON-NLS-1$
	                scheduleCheckForS3Stats( startOptions );
	            }
	        }
	        else
	        {
	            Test.output("S3-Stats-Check disabled #2"); //$NON-NLS-1$
	            stopCheckForS3Stats();
	        }
    	}
    }
    
    /**
     * Enable or disable the check for S3 Stats
     * {@code Configuration#isAutoSave()}.
     */
    public void updateCheckForS3Stats( String hostUserName, String ip, String mapName, int goods, int tournamentid )
    {
    	if( config.getS3Versionnumber() < 300 ) {
	        if (isS3Stats())
	        {
	            Test.output("S3-Stats-Check enabled"); //$NON-NLS-1$
	            if (isIngameOptionsStandalone || gameStarter.isS3Running())
	            {
	                Test.output("S3-Stats-Check schedule started");    //$NON-NLS-1$
	                scheduleCheckForS3Stats( hostUserName, ip, mapName, goods, tournamentid);
	            }
	        }
	        else
	        {
	            Test.output("S3-Stats-Check disabled #3"); //$NON-NLS-1$
	            stopCheckForS3Stats();
	        }
    	}
    }
    

    /**
     * führe Autosave alle {@code minutes} Minuten aus.
     * @param minutes
     */
    public void scheduleAutosave(long minutes)
    {
        // randomise the initial delay, so that hopefully not all players
        // save at the same timepoint
        int randomDelay = random.nextInt(60 * 1000) - 30 * 1000;
        long initialDelay = minutes * 64 * 1000 + randomDelay;
        initialDelay = Math.max(initialDelay, 64 * 1000);

        // Es werden 4 Sekunden draufgeschlagen, weil die Siedler3 Uhr
        // langsamer läuft.
        scheduleAutosave(minutes * 64 * 1000 + randomDelay, minutes * 64 * 1000);
        //scheduleAutosave(minutes * 64 * 1000/4, minutes * 64 * 1000/4); // for debugging
    }


    /**
     * Schedule autosave starting after {@code initialDelay} and
     * then repeating every {@code rate} milliseconds
     * @param initialDelay start in milliseconds
     * @param rate interval in milliseconds
     */
    public void scheduleAutosave(long initialDelay, long rate)
    {
        //initialDelay /= 2;
        //rate /= 2;
        stopAutosaveTask();
        autosaveTask = new AutoSaveTask(config.isDisabledAutosaveSound(), config.getAutoSaveSoundFile(), config.getGamePathS3(), config.getShowMessagesIngame());
        autosaveTask.schedule(initialDelay, rate);
    }
    
    /**
     * Check if the actual user are the host of the active game
     * 
     * @return boolean
     */
    public boolean isHost() {
    	return this.gameState.isHost();
    }
    
    /**
     * Schedule check for S3-Stats starting after {@code initialDelay} and
     * then repeating
     */
    public void scheduleCheckForS3Stats( StartOptions startOptions )
    {
   		scheduleCheckForS3Stats(startOptions.getPlayerName(), startOptions.getIp(), startOptions.getMapNameOriginal(), startOptions.getSelectGoodsInStock().getIndex(), startOptions.getTournamentId(), 10, 1500);    		
    }
    
    /**
     * Schedule check for S3-Stats starting after {@code initialDelay} and
     * then repeating
     */
    public void scheduleCheckForS3Stats( String hostUserName, String ip, String mapName, int goods, int tournamentid )
    {
   		scheduleCheckForS3Stats( hostUserName, ip, mapName, goods, tournamentid, 10, 1500);    		
    }
    
    /**
     * Schedule check for S3-Stats starting after {@code initialDelay} and
     * then repeating
     */
    public void scheduleCheckForS3Stats()
    {
   		scheduleCheckForS3Stats("", "", "", 0, 0, 10, 1500);    		
    }
    
    /**
     * Schedule check for S3-Stats starting after {@code initialDelay} and
     * then repeating every {@code rate} milliseconds
     * @param initialDelay start in milliseconds
     * @param rate interval in milliseconds
     */
    public void scheduleCheckForS3Stats( String hostUserName, String ip, String mapName, int goods, int tournamentid, long initialDelay, long rate)
    {
        stopCheckForS3Stats();
        checkForS3StatsTask = new CheckForS3Stats(this, hostUserName, ip, mapName, goods);
        checkForS3StatsTask.resetSettings();
        checkForS3StatsTask.schedule(initialDelay, rate);
    }

    private void stopAutosaveTask()
    {
        if (autosaveTask != null)
        {
            autosaveTask.cancel();
        }
    }
    
    public void stopCheckForS3Stats()
    {
        if (checkForS3StatsTask != null)
        {
        	Test.output("cancel checkForS3StatsTask");
        	checkForS3StatsTask.cancel();
        	Test.output("set checkForS3StatsTask to null");
        	checkForS3StatsTask = null;
        }
    }

    private void scheduleStopToolStoringTask()
    {
        stopStopToolStoringTask();
        stopToolStoringTask = new StopToolStoringTask(this);
        stopToolStoringTask.schedule(100, 100);
    }

    private void stopStopToolStoringTask()
    {
        if (stopToolStoringTask != null)
        {
            stopToolStoringTask.cancel();
        }
    }

    private void scheduleGetRaceTask()
    {
    	stopGetRaceTask();
        getRaceTask = new getRaceTask(this);
        getRaceTask.schedule(100, 100);
    }

    private void stopGetRaceTask()
    {
        if (getRaceTask != null)
        {
        	getRaceTask.cancel();
        }
    }
    
    /**
     * zeigt alle away Messages der User im Chatfenster an.
     */
    public void doListAwayMessages()
    {
        IrcUser[] userList = ircCommunicator.getSortedUserList();
        onCommandAnswer(I18n.getString("SettlersLobby.MSG_LIST_ALL_AWAY_MSGS")); //$NON-NLS-1$
        for (IrcUser user : userList)
        {
            if (user.isAway())
            {
                onCommandAnswer(MessageFormat.format(
                        I18n.getString("SettlersLobby.MSG_USER_AWAY"), user.getNick(), user.getAwayMsg())); //$NON-NLS-1$
            }
        }
    }

    public void onNotice(String sourceNick, String target, String notice)
    {
        messageHandler.addNoticeMessage(sourceNick, target, notice);
    }

    public void onAction(String sender, String action)
    {
        messageHandler.addActionMessage(sender, action);
    }

    public boolean isIngameOptionsStandalone()
    {
        return isIngameOptionsStandalone;
    }



    public void setIngameOptionsStandalone(boolean isIngameOptionsStandalone)
    {
        this.isIngameOptionsStandalone = isIngameOptionsStandalone;
    }

    public boolean isStopStoringTools()
    {
        return stopStoringTools;
    }

    public synchronized void setStopStoringTools(boolean stopStoringTools)
    {
        this.stopStoringTools = stopStoringTools;
    }
    
    public boolean isAutoSave()
    {
        return autoSave;
    }
    
    public boolean isS3Stats()
    {
        return s3Stats;
    }

    public void setAutoSave(boolean autoSave)
    {
        this.autoSave = autoSave;
    }
    
    public void setS3StatsCheck(boolean s3Stats)
    {
        this.s3Stats = s3Stats;
    }

    public int getAutoSaveTimer()
    {
        return autoSaveTimer;
    }

    public void setAutoSaveTimer(int autoSaveTimer)
    {
        this.autoSaveTimer = autoSaveTimer;
    }

    private void initIngameOptionsFromConfig()
    {
        setAutoSave(config.isAutoSave());
        setAutoSaveTimer(config.getAutoSaveTimer());
        setStopStoringTools(config.isStopStoringTools());
        setS3StatsCheck(true);
    }

    public void updateSelectedIngameOptions()
    {
        gui.updateSelectedIngameOptions();
    }

    public boolean isShowTime()
    {
        return showTime;
    }

    public void setShowTime(boolean showTime)
    {
        this.showTime = showTime;
    }

    public Chatlog getChatlog()
    {
        return chatlog;
    }

    public void setChatlog(Chatlog chatlog)
    {
        this.chatlog = chatlog;
    }

    public boolean isLogChat()
    {
        return logChat;
    }

    public void setLogChat(boolean logChat){
        this.logChat = logChat;
    }

    public MessageHandler getMessageHandler()
    {
        return messageHandler;
    }

    public boolean isConnectedWithServer() {
        return isConnectedWithServer;
    }

    public OpenGame getOpenHostGame()
    {
    	Test.output("openHostGame: " + openHostGame);
        return openHostGame;
    }

    public IRCCommunicator getIRCCommunicator() {
        return ircCommunicator;
    }

    public void clearChat()
    {
        gui.clearChat();
    }

    public void unselectGameInGui()
    {
        gui.unselectGame();
    }

    /**
     * Zeigt eine private Nachricht im Spiel an, allerdings nur falls der
     * User sich tatsächlich im Minichat oder im eigentlichen Spiel befindet.
     * @param message Nachricht
     */
    public void showWhisperedMessageIngame(String message)
    {
        //nur wenn man im Spiel oder im Minichat ist, wird versucht
        //eine Nachricht über den Ingame-Mechanismus anzuzeigen
        if ((gameState.isInGame() || gameState.isInMiniChat()) &&
                config.getShowWhisperIngame() && config.getShowMessagesIngame())
        {
            IngameMessage.addToWaitingList(message, 0, 0, -1, -1, false, true);
        }
    }

    /**
     * überprüft, ob config.getGamePathS3() auf eine gültige Datei
     * zeigt. Es wird nicht geprüft, ob die Datei ausführbar ist.
     * @return true falls config.getGamePathS3() auf eine gültige Datei zeigt.
     */
    public boolean isS3GamePathValid()
    {
        if (config != null && config.getGamePathS3() != null)
        {
            File file = new File(config.getGamePathS3());
            return file.exists() && file.isFile();
        }
        return false;
    }
    
    /**
     * Check if config.getGamePathS4() returns a valid file.
     * We will not check if the file is executable.
     * @return true if config.getGamePathS4() returns a valid file.
     */
    public boolean isS4GamePathValid()
    {
        if (config != null && config.getGamePathS4() != null)
        {
            File file = new File(config.getGamePathS4());
            return file.exists() && file.isFile();
        }
        return false;
    }

    /**
     * Liefert den aktuellen Servernamen (ohne eventuellen port).
     * @return
     */
    public String getServerName()
    {
        String server = config.getServerURL();
        if (server.indexOf(':') >= 0)
        {
            server = server.split(":")[0];
        }
        return server;
    }

    /**
     * The user requested to delete a game manually.
     * @param game
     */
    public void deleteGameManually(OpenGame game)
    {
        //check if we are the host of this game
        if (openHostGame != null &&
            openHostGame.getIP().equals(game.getIP()) &&
            openHostGame.getHostUserName().equals(game.getHostUserName()))
        {
            //use the default method
            onCloseGame();
        }
        else if (game.getHostUserName().equals(config.getUserName()))
        {
            //although there is no openHostGame, this seems to be
            //an entry from us!
            sendProgramMessageCloseGame(game);
        }
        else
        {
            //simply delete the game
            game.destroy();
        }
    }
    
    /**
     * is called if successfully connected to the server
     */
    private void onConnectionEstablished()
    {
        gui.closeWait();
        isConnectedWithServer = true;
        //call with a small delay, as the DccInetAddress is set by a different task
        //TODO find a better implementation
        Runnable ipRetriever = new Runnable() {
            @Override
            public void run()
            {
                getAndStoreExternalIp();
                gui.showIp(getIp());
            }
        };
        scheduler.schedule(ipRetriever, 1, TimeUnit.SECONDS);

        // Check every 10 seconds for VPN IP changes
        //TODO find a better implementation
        Runnable ipVpnRetriever = new Runnable() {
            @Override
            public void run()
            {
            	// get the actual vpn-ip from local system-ressources
            	// -> if not vpn is connected the return-value will be null
            	vpnIp = getVpnIpFromSystem();
            	if (vpnIp != null ) {
            		// if vpnip is available, check if it has been changed/is available and is online
            		if( false == vpnIp.equals(OriginalIpVpn) ) {
            			// yes, it has been changed
            			// -> send info to all users that this users has an active vpn-connection
            			sendProgramMessageVPNState(true);
            			// set own vpn-state in GUI
            			ircCommunicator.onVPNStateChange(getNick(), true);
            		}
            		// show vpn-ip in GUI
            		gui.setIpVpn(vpnIp);
            		// save the ip for next check if it has been changed
            		OriginalIpVpn = vpnIp;
            	} else {
            		if (vpnStarter == null || (!gui.getVpnStateButton().getToolTipText().equals(I18n.getString("SettlersLobby.VPN_STARTING")) && !gui.getVpnStateButton().getToolTipText().equals(I18n.getString("SettlersLobby.VPN_ERROR")))) {
            			gui.setVpnText("\uf127", Color.red, I18n.getString("SettlersLobby.VPN_OFF"));
            		}
            	}
            }
        };
        scheduler.scheduleAtFixedRate(ipVpnRetriever, 1, 10, TimeUnit.SECONDS);

        displayLoginDialog();
    }

    /**
     * called if the inital connection process fails.
     * @param errorMessage
     */
    private void onConnectionFailed(String errorMessage)
    {
        gui.displayError(errorMessage);
        //for the moment we do the same as if we were connected before
        onConnectionTerminatedCleanup();
    }

    /**
     * is called if the IRC server terminated our connection.
     */
    private void onConnectionTerminated()
    {
        Test.output("disconnected from server"); //$NON-NLS-1$
        gui.displayError(I18n.getString("IRCCommunicator.ERROR_CONNECTION_TERMINATED"));
        onConnectionTerminatedCleanup();
    }
    
    private void onConnectionTerminatedCleanup()
    {
        isConnectedWithServer = false;
        resetAwayStatus();
        actionWantsToLogOut();
    }

    /**
     * is called if the connection to the server was closed
     * (initiated by us).
     */
    private void onConnectionClosed()
    {
        isConnectedWithServer = false;
        resetAwayStatus();
    }

    private void onConnectorUnexpectedErrorMessage(String errorMessage)
    {
        //only show the error, nothing else
        gui.displayError(errorMessage);
    }

    private void displayLoginDialog()
    {
        if (isConnectedWithServer)
        {
            loginFrame.askUserForCredentials(getNick(), config.getPassword());
        }
    }

    private void displayLoginDialogAndDoAutomaticLogin()
    {
        if (isConnectedWithServer)
        {
            loginFrame.doAutomaticLogin(getNick(), config.getPassword());
        }
    }

    private void displayRegistrationDialog()
    {
        registerFrame.askUserForCredentials(getNick());
    }

    /**
     * Check against the siedler3.net server if user has a vpn key.
     * 
     * If he has a vpnkey, check if the serverkey has changed. If yes download it.
     * Delete in any fail-cases any ovpn-files from configDir.
     * 
     * @param withExtendedMessageForUser	show extended messages for user (for each error that could appear)
     * @param doNotSendStatistic			as the var says
     * 
     */
    @SuppressWarnings("static-access")
	public void getVpnKeyFromServerIfNeeded( boolean withExtendedMessageForUser, boolean doNotSendStatistic )
    {
        if (isConnectedWithServer)
        {	
        	
        	// initialize vars
        	String response = null;
        	String response2 = null;
        	String ovpnFileName = "";
            String ovpnMd5FileHash = "";
            String ovpnContent = "";
        	
        	// get the md5password-Hash
        	String passwordHash = CryptoSupport.md5(config.getPassword()).toLowerCase();
        	
        	// get the unique installation-Id
        	String installationId = ReqQuery.getId();
        	if (installationId == null) {
        		// if no installation-Id was found, do not load any vpn key
        		Test.output("No ID found!!!");
        		removeAllOvpnFiles();
        		if( false != withExtendedMessageForUser ) {
        			gui.displayError(I18n.getString("SettlersLobby.ERROR_VPN_MISSING_ID"));
        		}
        		return;
        	}
        	
        	// get the os
        	String osVersion;
        	if (IS_WINE) {
        		// if wine is running set it to "Wine"
        		osVersion = "Wine";
        	} else {
        		osVersion = System.getProperty("os.version");
        	}
        	
        	// start first request to get actual VPN-Key-Settings
        	httpRequests httpRequests = new httpRequests();
			httpRequests.setUrl("https://vpnkey.siedler3.net/aLobbyRequests.html");
			// -> get the response as json
			httpRequests.addParameter("format", "json");
			httpRequests.addParameter("checkproxy", "1");
			httpRequests.addParameter("username", getNick());
			httpRequests.addParameter("passwordHash", "md5:" + passwordHash);
			if( false == doNotSendStatistic ) {
				httpRequests.addParameter("id", installationId);
				httpRequests.addParameter("os", osVersion);
				if( this.otherFirewall != null ) {
					httpRequests.addParameter("firewallname", this.otherFirewall);
				}
				else {
					httpRequests.addParameter("firewallname", "");
				}
				if( this.windowsFirewallOn ) {
					httpRequests.addParameter("firewallstatus", 1);
				}
				else {
					httpRequests.addParameter("firewallstatus", 0);
				}
				httpRequests.addParameter("theme", this.config.getTheme().name());
				// add info about screensize
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				httpRequests.addParameter("screenWidth", screenSize.width);
				httpRequests.addParameter("screenHeight", screenSize.height);
			}
			httpRequests.addParameter("lang", this.config.getLanguage().getLanguage());
			// -> get the response-String
			try {
				response = httpRequests.doRequest();
			} catch (IOException e1) {
			}
			
			// If siedler3.net-server was not available response will be null.
			// -> Cancel all following checks (but do not deletes keys as this is a server-fault).
			if( response == null ) {
				if( false != withExtendedMessageForUser ) {
	    			gui.displayError(I18n.getString("SettlersLobby.ERROR_VPN_SERVER_NOT_AVAILABLE"));
	    		}
				return;
			}
			
			// check the response
			// -> response doesn't consists of ";"
			JSONArray errors = null;
			if( response.length() > 0 ) {
				// if response is that user has a proxy/vpn active warn him
				// and close aLobby
				if( response.replace("\n", "").equals("proxyip") ) {
		        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
		        	messagewindow.preventClosing(true);
		        	// -> add message
		    		messagewindow.addMessage(
		    			"<html>" + I18n.getString("SettlersLobby.PROXY_HINT") + "</html>", 
		    			UIManager.getIcon("OptionPane.errorIcon")
		    		);
		    		// -> add ok-button
		    		messagewindow.addButton(
	    				I18n.getString("SettlersLobby.PROXY_HINT_BUTTON"),
		    			new ActionListener() {
		    			    @Override
		    			    public void actionPerformed(ActionEvent e) {
		    			    	System.exit(1);
		    			    }
		    			}
		    		);
		    		// show it
		    		messagewindow.setVisible(true);
		    		return;
				}
				else {
					try {
						JSONObject obj = new JSONObject(response);
						if( !obj.isNull("vpnkeyfilename") ) {
							ovpnFileName = obj.getString("vpnkeyfilename");
						}
						if( !obj.isNull("vpnkeymd5") ) {
							ovpnMd5FileHash = obj.getString("vpnkeymd5");
						}
						if( !obj.isNull("errors") ) {
							errors = obj.getJSONArray("errors");
						}
					}
					catch(JSONException e) {
						Test.outputException(e);
					}
				}
			}
			else {
				Test.output("We've got an empty response - this most likely means, the user did not get a key, yet: " + response);
            	removeAllOvpnFiles();
				return;
			}
            // Sanity checks
            if (ovpnFileName.contains(" ") || ovpnFileName.length() > 80)
            {
            	Test.output("The file name of the new key is invalid: " + ovpnFileName);
            	removeAllOvpnFiles();
            	if( false != withExtendedMessageForUser ) {
	    			gui.displayError(I18n.getString("SettlersLobby.ERROR_VPN_FORMAT_ERROR"));
	    		}
            	return;
            }
            if (ovpnMd5FileHash.contains(" ") || ovpnMd5FileHash.length() > 80)
            {
            	Test.output("The file name of the new key is invalid: " + ovpnMd5FileHash);
            	removeAllOvpnFiles();
            	if( false != withExtendedMessageForUser ) {
	    			gui.displayError(I18n.getString("SettlersLobby.ERROR_VPN_FORMAT_ERROR"));
	    		}
            	return;
            }
            
            // if there are any vpn-key-validation-errors show them now as list
            if( errors != null && errors.length() > 0 ) {
            	Test.output("The VPN-key-validation results in errors the user has to fix.");
            	removeAllOvpnFiles();
            	try {
            		// get the fontawesome-font with font-size 13
            		Font fontAwesome = config.getFontAweSome( 14, Font.BOLD );
            		// load textcolor of actualy used theme as Color-Object
                	String hexcolor = getHexColorStringByColor(theme.getTextColor());
	            	// add gfx before message
		        	// source (license: public domain): http://macsstuff.net/photobov/clipart-keys-free
			    	InputStream imgstream = SettlersLobby.class.getResourceAsStream(ALobbyConstants.PACKAGE_PATH + "/gui/img/key.png");
			    	BufferedImage imgio = ImageIO.read(imgstream);
			    	ImageIcon img = new ImageIcon(imgio);
			    	// generate message-window
				    MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
		    		LinkLabel messageText = messagewindow.addMessage("<html><h3>" + I18n.getString("SettlersLobby.ERROR_VPN_VALIDATION_FAILED") + "</h3></html>", img);
		    		for( int e = 0; e < errors.length(); e++ ) {
		    			String errorText = errors.getJSONObject(e).getString("error");
		    			String solutionText = errors.getJSONObject(e).getString("solution");
		    			String text = "<strong><u>" + I18n.getString("SettlersLobby.ERROR_VPN_VALIDATION_FAILED_ERRORTITLE") + ":</u></strong><br>" + MessageFormat.format(errorText, hexcolor);
		    			if( solutionText.length() > 0 ) {
		    				text = text + "<br><strong><u>" + I18n.getString("SettlersLobby.ERROR_VPN_VALIDATION_FAILED_SOLUTIONTITLE") + ":</u></strong><br>" + MessageFormat.format(solutionText, hexcolor);
		    			}
		    			messagewindow.addMessage("<html>" + text + "</html>", UIManager.getIcon("OptionPane.errorIcon"));
		    		}
		    		JButton btnClose = messagewindow.addButton(
		    			"\uf021",
		    			new ActionListener() {
		    			    @Override
		    			    public void actionPerformed(ActionEvent e) {
		    			    	// close this window
	    			    		messagewindow.dispose();
		    			    }
		    			}
		    		);
		    		JButton btnTime = messagewindow.addButton(
		    			"30 " + I18n.getString("SettlersLobby.SEC_PLURAL"),
		    			new ActionListener() {
		    			    @Override
		    			    public void actionPerformed(ActionEvent e) {
		    			    	return;
		    			    }
		    			}
		    		);
		    		btnTime.setBorder(BorderFactory.createEmptyBorder());
		    		btnClose.setFont(fontAwesome);
		    		// disable the ok-button
		    		btnClose.setEnabled(false);
		    		// define a timer which will enable the disabled button 
		            // after 30 seconds
		            int delay = 30000; //milliseconds
		            Timer timer = new Timer(delay, new ActionListener() {
		                public void actionPerformed(ActionEvent evt) {
	                		btnClose.setEnabled(true);
	                		btnClose.setFont(messageText.getFont().deriveFont(Font.BOLD, config.getFontSize()));
	                		btnClose.setText(UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK");
	                		btnTime.setVisible(false);
	                	}
		            });
       				// start the countdown-timer
		            // -> shows the time until the ok-button is available
       				timer.start();
       				scheduler.scheduleAtFixedRate(new Runnable() {
						@Override
						public void run() {
							String text = btnTime.getText();
							text = text.replaceAll(" " + I18n.getString("SettlersLobby.SEC_PLURAL"), "");
							int seconds = Integer.valueOf(text);
							seconds--;
							if( seconds == 1 ) {
								btnTime.setText("1 " + I18n.getString("SettlersLobby.SEC_SINGULAR"));
							}
							else if( seconds > 1 ) {
								btnTime.setText(seconds + " " + I18n.getString("SettlersLobby.SEC_PLURAL"));
							}
						}
       				}, 0, 1, TimeUnit.SECONDS);
       				messagewindow.setSettlersLobby(this);
       				// prevent closing by close-button
       				messagewindow.preventClosing(true);
       				// set window visible
		    		messagewindow.setVisible(true);
	            	return;
            	}
            	catch(Exception e) {
            		Test.outputException(e);
            	}
            }

            // get the configDir of local installation
			File configDir = new File(SettlersLobby.configDir);
			
			// set the configFile
			File configFile = null;

			// seach for configFile in configDir
		    File[] configFiles = configDir.listFiles();
		    for (File file : configFiles) {
				if (file.getName().toLowerCase().equalsIgnoreCase(ovpnFileName)) {
					configFile = file;
					break;
				}
				else {
					if (file.getName().toLowerCase().endsWith(".ovpn")) {
						// remove all other ovpn-files
						file.delete();
					}
				}
			}
		    
		    // if no configFile was found OR the md5 of the local file differ from the remote file
		    // than get the vpnkey from remote server
		    if( configFile == null || !CryptoSupport.Md5Sum(configFile, ovpnMd5FileHash) )
		    {
		    	// set all parameters to be send
	            // -> username
	            // -> passwortHash (IRC-password, so wie didn't public the real password ..)
	            // -> uniqueID
	            // -> os (only the number)
	            // -> theme (actual used style)
		    	// -> lang (actual used language)
	            httpRequests = new httpRequests();
				httpRequests.setUrl("https://vpnkey.siedler3.net/aLobbyRequests.html");
				// -> get the response as json
				httpRequests.addParameter("json", "1");
				httpRequests.addParameter("username", getNick());
				httpRequests.addParameter("passwordHash", "md5:" + passwordHash);
				if( false == doNotSendStatistic ) {
					httpRequests.addParameter("id", installationId);
					httpRequests.addParameter("os", osVersion);
					httpRequests.addParameter("theme", this.config.getTheme().name());
				}
				httpRequests.addParameter("lang", this.config.getLanguage().getLanguage());
				// -> AND a flag that we need the vpn key in response
	            httpRequests.addParameter("requestVpnKey", "1");
	            // -> get the response as json
	            httpRequests.addParameter("format", "json");
	            
	            // -> get the response-String
				try {
					response2 = httpRequests.doRequest();
				} catch (IOException e1) {
				}
	            
				// -> get VPN-data from JSON-response
				JSONObject obj = new JSONObject(response2);
				if( !obj.isNull("vpnkeyfilename") ) {
					ovpnFileName = obj.getString("vpnkeyfilename");
				}
				if( !obj.isNull("vpnkeymd5") ) {
					ovpnMd5FileHash = obj.getString("vpnkeymd5");
				}
				if( !obj.isNull("vpnkey") ) {
					ovpnContent = obj.getString("vpnkey");
				}
	            
	            removeAllOvpnFiles();
	            
	            // Sanity checks
	            if (ovpnFileName.contains(" ") || ovpnFileName.length() > 80)
	            {
	            	Test.output("The file name of the new key is invalid: " + ovpnFileName);
	            	if( false != withExtendedMessageForUser ) {
		    			gui.displayError(I18n.getString("SettlersLobby.ERROR_VPN_FORMAT_ERROR"));
		    		}
	            	return;
	            }
	            if (ovpnMd5FileHash.contains(" ") || ovpnMd5FileHash.length() > 80)
	            {
	            	Test.output("The file name of the new key is invalid: " + ovpnMd5FileHash);
	            	if( false != withExtendedMessageForUser ) {
		    			gui.displayError(I18n.getString("SettlersLobby.ERROR_VPN_FORMAT_ERROR"));
		    		}
	            	return;
	            }
	            
			    // Now create the new config
			    try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(configDir.getAbsolutePath() + "\\" + ovpnFileName), "utf-8"))) {
			    	writer.write(ovpnContent);
			    } catch (FileNotFoundException e) {
					Test.output("We've got a FileNotFound exception with the login-site!");
					if( false != withExtendedMessageForUser ) {
		    			gui.displayError(I18n.getString("SettlersLobby.ERROR_VPN_NOT_WRITABLE"));
		    		}
					return;
				} catch (IOException e) {
			    	Test.output("Error while writing the new VPN key " + configDir.getAbsolutePath() + "\\" + ovpnFileName + ":");
			    	Test.outputException(e);
			    	if( false != withExtendedMessageForUser ) {
		    			gui.displayError(I18n.getString("SettlersLobby.ERROR_VPN_NOT_WRITABLE"));
		    		}
			    	return;
			    }
			    Test.output("New key downloaded and installed: " + ovpnFileName + " " + ovpnMd5FileHash);
			    
			    // show infowindow with message that vpn key is downloaded and now usable
			    try {
			    	// add gfx before message
		        	// source (license: public domain): http://macsstuff.net/photobov/clipart-keys-free
			    	InputStream imgstream = SettlersLobby.class.getResourceAsStream(ALobbyConstants.PACKAGE_PATH + "/gui/img/key.png");
			    	BufferedImage imgio = ImageIO.read(imgstream);
			    	ImageIcon img = new ImageIcon(imgio);
			    	// generate message-window
				    MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
		    		messagewindow.addMessage(I18n.getString("SettlersLobby.INFO_NEW_KEY"), img);
		    		messagewindow.addButton(
		    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
		    			new ActionListener() {
		    			    @Override
		    			    public void actionPerformed(ActionEvent e) {
		    			    	// activate the vpn-autoconnection
		    			    	config.setAutoconnectVpn(true);
		    			    	// close this window
		    			    	messagewindow.dispose();
		    			    }
		    			},
		    			addActionListenerForMessageWindowButton(messagewindow)
		    		);
		    		messagewindow.setVisible(true);
			    }
			    catch(Exception e) {
			    	
			    }
	        } else {
		    	Test.output("The key " + ovpnFileName + " did not change. Update of the key skipped.");
		    	if( false != withExtendedMessageForUser ) {
		    		MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
		        	// -> add message
		    		messagewindow.addMessage(
		    			I18n.getString("SettlersLobby.VPN_KEY_EXISTS"), 
		    			UIManager.getIcon("OptionPane.infoIcon")
		    		);
		    		// -> add ok-button
		    		messagewindow.addButton(
		    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
		    			new ActionListener() {
		    			    @Override
		    			    public void actionPerformed(ActionEvent e) {
		    			    	messagewindow.dispose();
		    			    }
		    			},
		    			addActionListenerForMessageWindowButton(messagewindow)
		    		);
		    		// show it
		    		messagewindow.setVisible(true);
	    		}
		    }
        }
    }

    /**
     * Removes all existing files with .ovpn-Extension from configDir.
     */
    private void removeAllOvpnFiles() {
    	
    	// first stop vpn if it is already running
    	try {
			stopVpn();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	// get the configDir of local installation
		File configDir = new File(SettlersLobby.configDir);

		// seach for configFile in configDir
	    File[] configFiles = configDir.listFiles();
    	// Remove all existing OVPN files in the aLobby user dir before creating the new one
	    for (File file : configFiles) {
			if (file.getName().toLowerCase().endsWith(".ovpn")) {
				file.delete();
			}
		}
	}

    /**
     * Actions to run if user enters the chat.
     */
	@SuppressWarnings("static-access")
	private void enterChannelAndDisplayChat()
    {
		// april fools joke (if active)
		getAprilFoolsJoke();
		
		// adventscalender (if active) 
		//getAdventCalendar(false);
		
		// VPN-Key
    	getVpnKeyFromServerIfNeeded( false, false );
    	
    	// load Community News
  		queryForCommunityNews();
    	
        //update showTime-value before displaying the chat window
        setShowTime(config.isShowTime());
        if(!getChatlog().isLogging() && isLogChat())
        {
            getChatlog().startLogging();
        }
        
        // display the chat
        gui.displayChat();
        enterChannel();
        if (config.isAutoconnectVpn()) {
	    	try {
				startVpn();
			} catch (Exception e) {
				Test.outputException(e);
			}
        }
    }

	/**
	 * Create AdventCalendar if itsChristmasTime is true.
	 * 
	 * @param forceShow	force visibility of calendar 
	 */
	public void getAdventCalendar(boolean forceShow) {
		// show Adventcalendar if its chistmastime
        if( false != this.config.itsChristmastime() ) {
        	AdventCalendar adventcalendar = new AdventCalendar(this);
        	if( false != adventcalendar.alreadyShowToday() || false != forceShow ) {
        		adventcalendar.pack();
        		adventcalendar.setLocationRelativeTo(this.getGUI().getMainWindow());
        		adventcalendar.setVisible(true);
        	}
        }
	}

	/**
	 * Create an April Fool's joke if it is the 1. April
	 */
    private void getAprilFoolsJoke() { 
    	/*
    	if( config.itsApril1th() && false == config.getPreventAprilsFool() ) {
	    	String hexcolor = SettlersLobby.getHexColorStringByColor(theme.getTextColor());
			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
			messagewindow.addMessage(
				MessageFormat.format(I18n.getString("SettlersLobby.APRIL_FOOL_MESSAGE"), hexcolor),
				UIManager.getIcon("OptionPane.informationIcon")
	     	);
			messagewindow.addButton(
	     		I18n.getString("SettlersLobby.APRIL_FOOL_SUCCEED"),
	     		new ActionListener() {
	     		    @Override
	     		    public void actionPerformed(ActionEvent e) {
	     		    	messagewindow.dispose();
	     		    }
	     		}
	     	);
			messagewindow.setVisible(true);
    	}*/
	}

	private void enterChannel()
    {
        iniAwayStatus();
        joinChannel(); //join the channel
        if (config.isAutoLoadAndSaveBuddys()) {
            try
            {
                BuddyHandling.importBuddyIgnore();
            }
            catch (Exception e) {
                Test.outputException(e);
            }
        }
    }

	/**
	 * Get the gamestarter depending on configured s3-version.
	 * 
	 * @return
	 */
    public GameStarter getGameStarter()
    {
    	// use gamestarter before 3.00
    	if( config.getS3Versionnumber() < 300 ) {
    		try {
				gameStarter = new GameStarterVanilla(this);
			} catch (Exception e) {
				MessageDialog messagewindowVersionHint = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
	        	// -> add message
	        	messagewindowVersionHint.addMessage(
	    			"<html>" + formatExceptionText(e) + "</html>", 
	    			UIManager.getIcon("OptionPane.errorIcon")
	    		);
	    		// -> add ok-button
	        	messagewindowVersionHint.addButton(
	    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	    			new ActionListener() {
	    			    @Override
	    			    public void actionPerformed(ActionEvent e) {
	    			    	System.exit(1);
	    			    }
	    			}
	    		);
	    		// show it
	        	messagewindowVersionHint.setVisible(true);
				System.exit(1);
			}
    	}
    	else {
    		// use s3-launcher für S3CE
    		gameStarter = new GameStarterLauncher(this);        		
    	}
        return gameStarter;
    }
    
    /**
     * Get the gamestarter depending on the given version by parameter..
     * 
     * @param miscType   the given version
     * @return
     */
    public GameStarter getGameStarterByGivenVersion( MiscType miscType )
    {
        // use gamestarter for classic version
        if( miscType != MiscType.S3CE ) {
            try {
                gameStarter = new GameStarterVanilla(this);
            } catch (Exception e) {
                MessageDialog messagewindowVersionHint = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
                // -> add message
                messagewindowVersionHint.addMessage(
                    "<html>" + formatExceptionText(e) + "</html>", 
                    UIManager.getIcon("OptionPane.errorIcon")
                );
                // -> add ok-button
                messagewindowVersionHint.addButton(
                    UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
                    new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            System.exit(1);
                        }
                    }
                );
                // show it
                messagewindowVersionHint.setVisible(true);
                System.exit(1);
            }
        }
        else {
            // use s3-launcher for S3CE
            gameStarter = new GameStarterLauncher(this);                
        }
        return gameStarter;
    }

    public OpenGameContainer getOpenGames()
    {
        return openGames;
    }
    
    public RunningGameContainer getRunningGames()
    {
        return runningGames;
    }

    @Override
    public void onResultEvent(final ResultEvent event)
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run()
            {
                onResultEventEdt(event);
            }
        });

    }

    private void onResultEventEdt(ResultEvent event)
    {
        if (event.getSource() == ircConnector)
        {
            onConnectorResultEvent(event);
        }
        //only react to resultEvents from loginFrame or registerframe
        //if we are still connected to the IRC server.
        //Otherwise these are most likey results from 'onDisconnect'
        //which can be ignored here, as the main reaction is handled
        //in onConnectorResultEvent()
        else if (event.getSource() == loginFrame)
        {
            if (isConnectedWithServer)
            {
                onLoginResultEvent(event);
            }
        }
        else if (event.getSource() == registerFrame)
        {
            if (isConnectedWithServer)
            {
                onRegisterResultEvent(event);
            }
        }
        else
        {
            Test.output("unexpected event source"); //$NON-NLS-1$
            assert(false);
        }

    }

    private void onConnectorResultEvent(ResultEvent event)
    {
        switch (event.getResult())
        {
            case OK:
                onConnectionEstablished();
                break;
            case FAILED:
                onConnectionFailed(event.getErrorMessage());
                break;
            case ABORTED:
                onConnectionClosed();
                break;
            case TERMINATED:
                onConnectionTerminated();
                break;
            case UNEXPECTED_ERROR_MESSAGE:
                onConnectorUnexpectedErrorMessage(event.getErrorMessage());
                break;
            default:
                Test.output("unexpected event" + event); //$NON-NLS-1$
                assert(false);
                return;
        }

    }

    private void onLoginResultEvent(ResultEvent event)
    {
        switch (event.getResult())
        {
            case OK:
                enterChannelAndDisplayChat();
                break;
            case FAILED:
                //try again
                displayLoginDialog();
                break;
            case ABORTED:
                displayFirstFrame();
                break;
            case USER_REQUESTS_REGISTRATION:
                displayRegistrationDialog();
                break;
            default:
                Test.output("unexpected event" + event); //$NON-NLS-1$
                assert(false);
                return;
        }

    }

    private void onRegisterResultEvent(ResultEvent event)
    {
        switch (event.getResult())
        {
            case OK:
                displayLoginDialogAndDoAutomaticLogin();
                break;
            case FAILED:
                displayRegistrationDialog();
                break;
            case ABORTED:
                displayLoginDialog();
                break;
            default:
                Test.output("unexpected event" + event); //$NON-NLS-1$
                assert(false);
                return;
        }
    }

    private void logSystemInfo()
    {
        String result = String.format("%s %s, %s %s, Java %s, %s, %s\n%s\ncharset: %s",
                ALobbyConstants.PROGRAM_NAME,
                ALobbyConstants.VERSION,
                System.getProperty("os.name"),
                System.getProperty("os.arch"),
                System.getProperty("java.version"),
                System.getProperty("java.vm.version"),
                System.getProperty("java.home"),
                System.getProperty("user.dir"),
                Charset.defaultCharset());
        Test.output(result);
        result = WINE_VERSION;
        if (result != null)
        {
            Test.output("Running on Wine... " + result);
        }
        Test.output(System.getProperty("java.home"));
        Test.output("configDir: " + configDir);

    }

    public OpenGame getOpenJoinGame()
    {
        return openJoinGame;
    }

    public static String getUserDir()
    {
        //this only works on windows systems
        String result = System.getenv("APPDATA");

        if (result == null || result.isEmpty())
        {
            //use current working directory instead (old behaviour)
            result = System.getProperty("user.dir");
        }
        else
        {
            result = new File(result, "alobby").getAbsolutePath();
        }
        return result;
    }

	public static void showConfigDirFolderInExplorer() 
	{
    	try
		{
    		if (Desktop.isDesktopSupported())
    		{
    			Desktop.getDesktop().open(new File(configDir));
    		}
		} 
    	catch (Exception e)
		{
			Test.outputException(e);
		}
	}
	
	/**
	 * Wrapper for dialog-window with ok-button and button to open the configdir
	 * 
	 * @param parent	parent object for the dialog-window
	 * @param message	message to display
	 * @param title		window-title
	 */
	public static void showErrorWithOpenConfigDir(Object parent, String message, String title) {
		JFrame parentframe = null;
		if (parent instanceof Window)
		{
			parentframe = (JFrame)parent;
		}
		else if (parent instanceof Frame)
		{
			parentframe = (JFrame)parent;
		}
		else 
		{
			parentframe = (JFrame)null;
		}
		MessageDialog messagewindow = new MessageDialog(parentframe, title, true);
		messagewindow.addMessage(message, UIManager.getIcon("OptionPane.errorIcon"));
		messagewindow.addButton(
			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
			new ActionListener() {
			    @Override
			    public void actionPerformed(ActionEvent e) {
			        System.exit(0);
			    }
			}
		);
		messagewindow.addButton(
			I18n.getString("Configuration.OPEN_CONFIG_DIR"), 
			new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
	       			SettlersLobby.showConfigDirFolderInExplorer();
				}
	    	}
		);
		messagewindow.setVisible(true);
	}
    
	/**
	 * Convert given color to hex-code for html-usage.
	 * 
	 * @param color		The color to convert.
	 * @return hexcolor	The resulting hex-code.
	 */
    public static String getHexColorStringByColor( Color color ) {
		String hexcolor = Integer.toHexString(color.getRGB() & 0xffffff);
		if (hexcolor.length() < 6) {
			hexcolor = "0" + hexcolor;
		}
		hexcolor = "#" + hexcolor;
    	return hexcolor;
    }
    
    /**
     * Check if the user use a vpn-ip.
     * 
     * @return boolean
     */
    public boolean checkIfVpnIsUsed() {
    	return (config.isHostUsingVpn() && getVpnIpFromSystem() != null && getVpnIpFromSystem().startsWith(ALobbyConstants.VPN_IP_RANGE));
    }
    
    /**
     * Get the ip to use for host-a-game.
     * 
     * @return
     */
    public String ipToUse() {
        if (false != checkIfVpnIsUsed()) {
        	return vpnIp;
        } else {
        	return ip;
        }
    }
    
    /**
     * Send a message to remove the closed game from the list of running games
     * to all users in the aLobby.
     * Only if actual user is the host.
     */
    public void removeGameFromRunningGames() {
    	boolean go = false;
    	// host and runningHostGame available?
    	if( gameState != null && gameState.isHost() && runningHostGame != null ) {
    		go = true;
    	}
    	if( config.getS3Versionnumber() >= 300 && runningHostGame != null ) {
    		go = true;
    	}
    	if( false != go ) {
    		// send info about closed game to webserver
        	// if this user was host of this game
        	// -> responsive is not relevant for us here
        	httpRequests httpRequests = new httpRequests();
        	httpRequests.setUrl("https://lobby.siedler3.net/aLobbyRequests.html");
        	httpRequests.addParameter("updategame", 1);
        	httpRequests.addParameter("state", "closed");
        	httpRequests.addParameter("hostname", runningHostGame.getHostUserName());
        	httpRequests.addParameter("ip", runningHostGame.getIP());
        	// no token-security for this request
        	httpRequests.setIgnorePhpSessId(true);
        	try {
        		httpRequests.doRequest();
        	} catch (IOException e) {
        	}
    		// send message
   			ircCommunicator.sendProgramMessage(protocol.createMsgCloseRunningGame(runningHostGame));
   			// remove the game from own session
   			runningHostGame.destroy();
   			runningHostGame = null;
    	}
    }

    /**
     * Answer on request from other user in aLobby for running games.
     */
	public void onPrgMsgQueryRunningGame(String senderName) {
		if (runningHostGame != null)
        {
            sendRunningHostGame(senderName);
        }
	}

	/**
	 * Save the race to the sender if the actual user are in the same game.
	 * 
	 * @param hostUserName
	 * @param senderName
	 * @param race
	 */
	public void onPrgMsgSetRaceInGame(String hostUserName, String senderName, String race) {
		if( this.getGameState() != null && this.getGameState().getHostUserName() != null && this.getGameState().getHostUserName().getNick().equals(hostUserName) ) {
			// ok, both user are in the same host-game
			UserInGameList userInGameList = this.getGameState().getUserList();
			// -> than save the data
			UserInGame userInGame = userInGameList.getUser(senderName);
			if( userInGame != null ) {
				userInGame.setRace(race);
			}
			// check if each user in this game has send his race
			List<UserInGame> users = userInGameList.getUserList();
			int usersWithRace = 0;
			for( int i = 0;i<users.size();i++ ) {
				if( users.get(i).getRace().length() > 0 ) {
					usersWithRace++;
				}
			}
			if( usersWithRace == userInGameList.getUserList().size() ) {
				// generate the resulting string with races for each user
				String usersWithRaces = "";
				for( int i = 0;i<users.size();i++ ) {
					if( usersWithRaces.length() > 0 ) {
						usersWithRaces = usersWithRaces + ";";
					}
					usersWithRaces = usersWithRaces + "'" + users.get(i).getNick() + "','" + users.get(i).getRace() + "'";
				}
				// get the openGame-object
				OpenGame openGame = this.getOpenGames().getOpenGame(hostUserName);
		    	if( openGame == null ) {
		    		openGame = this.getRunningGames().getOpenGame(hostUserName);
		    	}
		    	if( openGame != null && openGame.getMap() != null ) {
			    	if( false == openGame.getMap().contains("Savegame") ) {
			    		// call gameDataFile to save the game-data
			    		GameDataFile gameDataFile = new GameDataFile(this, openGame);
			    		gameDataFile.add("usersWithRaces", usersWithRaces);
			    		gameDataFile.saveFile();
			    	}
		    	}
			}
		}
	}
  
	// set user as playing tetris
	public void setPlayingTetris( boolean isPlayingTetris ) {
		this.playingTetris  = isPlayingTetris;
	}
	
	// return if user is playing tetris
	public boolean isPlayingTetris() {
		return this.playingTetris;
	}
	
	/**
	 * Check if the user has played a league-game in the past.
	 * 
	 * @return boolean
	 */
	public boolean checkIfUserIsInLeague() {
		if( false == Configuration.getInstance().isHideLeagueInterface() ) {
			// get file-info from mapBase independently whether the map-file is local available or not
			String response = "";
			
			// check whether the md5 of the local file is the same as on mapbase
	    	httpRequests httpRequests = new httpRequests();
	    	// -> set an parameter
	    	httpRequests.addParameter("json", 1);
	    	// -> ignore security-token
	    	httpRequests.setIgnorePhpSessId(true);
	    	// -> set the url
	    	String url = "";
			try {
				// encode the searchstring and add it to the url
				url = config.getLeagueUrlGameCount() + getNick();
				httpRequests.setUrl(url);
				// -> get the response-String
				response = httpRequests.doRequest();
	    		if( !httpRequests.isError() ) {
	    			if( response.trim().length() > 0 ) {
	    				if( Integer.parseInt(response.trim()) > 0 ) {
	    					return true;
	    				}
	    			}
	    		}
			} catch (UnsupportedEncodingException e2) {
				e2.getStackTrace();
			} catch (IOException e) {
				e.getStackTrace();
			}
			return false;
		}
		return true;
	}
	
	/**
	 * Set that user was active.
	 */
	public void userWasActive() {
		Test.output("set user was active");
		if( awayStatus != null ) {
			awayStatus.userWasActive();
		}
	}
	
	/**
	 * Start clipboardlistener.
	 */
	private void startClipBoardListener() {
		if( !IS_WINE && config.getS3Versionnumber() < 300 ) {
		    Test.output("start clipBoardListener");
			clipBoardListener = new ClipBoardListener();
			clipBoardListener.start();
		}
	}
	
	/**
	 * Stop clipBoardListener.
	 */
	private void stopClipBoardListener() {
		if( clipBoardListener != null ) {
		    Test.output("stop clipBoardListener");
			clipBoardListener.setRunning(false);
			clipBoardListener = null;
		}
	}

	/**
	 * Check if a Beta-Version is used 
	 * 
	 * @return boolean
	 */
	public static boolean isBetaUsed() {
		return ALobbyConstants.VERSION.split("\\.").length == 4 ? true : false;
	}

	/**
	 * Check if a Beta-Version is used and if its true 
	 * than return "BETA" als String
	 * 
	 * @return String
	 */
	public static String getBetaVersionMarker() {
		return commandLineOptions.isRunInEcplise() ? " DEV" : (isBetaUsed() ? " BETA" : "");
	}
	
	/**
	 * Search and replace IPs and hostnames in String.
	 * 
	 * This function should be used to prevent logging of IPs and hostnames e.g. in the alobby-own logfiles.
	 */
	public static String searchAndReplaceIPAndHostname( String line ) {
		// set the regex to search for IPs and hostname
		// -> created with help of: https://www.regextester.com
	    /* 
	     * :FIXME: We need to fix this pattern, as it proces weird results with file names, e.g.:
	     * 
	     * 21:08:31.865 GogSupport.makeS3ALobbyExe(): Moving s3_ANONYMIZED to s3_alobby.exe...
         * 21:08:31.865 GogSupport.makeS3ALobbyExe(): Moved s3_ANONYMIZED to s3_ANONYMIZED successfully.
         * 21:08:31.904 GogSupport.makeS3ALobbyExe(): s3_ANONYMIZED created successfully
	     *
	     * Also, in the first line we see, that "alobby.exe" was not "ANONYMIZED" while in the second line it was...
	     */
		Pattern validHostnameRegex = Pattern.compile("(?!.*(exe|dll|map))(((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|((([a-zA-Z]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)+([A-Za-z][A-Za-z0-9\\‌​-]*[A-Za-z0-9])))(?=\\s|$)");
    	
		// replace community-own hostnames before we check the string for other hostname
    	line = line.replace("irc.siedler3.net", "alobbypreparations_live");
    	line = line.replace("www.siedler3.net", "alobbypreparations_web");
    	line = line.replace("lobby.siedler3.net", "alobbypreparations_lobby");
    	line = line.replace("liga.siedler3.net", "alobbypreparations_liga");
    	line = line.replace("vpn.siedler3.net", "alobbypreparations_vpn");
    	line = line.replace("screen.siedler3.net", "alobbypreparations_screen");
    	
    	// create the StringBuffer to create the new String
    	StringBuffer sb = new StringBuffer();
    	
    	// create the list of matching results
    	Matcher m = validHostnameRegex.matcher(line);

    	// loop through the results
    	while(m.find())
    	{
    		// and replace each found item
   			m.appendReplacement(sb, "ANONYMIZED");
    	}
    	// append the String
    	m.appendTail(sb);
    	line = sb.toString();
    	
    	// replace community-own hostnamens back to its original spelling
   	  	line = line.replace("alobbypreparations_live", "irc.siedler3.net");
   	  	line = line.replace("alobbypreparations_web", "www.siedler3.net");
   	  	line = line.replace("alobbypreparations_lobby", "lobby.siedler3.net");
   	  	line = line.replace("alobbypreparations_liga", "liga.siedler3.net");
   	  	line = line.replace("alobbypreparations_vpn", "vpn.siedler3.net");
   	  	line = line.replace("alobbypreparations_screen", "screen.siedler3.net");
   	  	
   	  	// return value
   	  	return line;
	}

	/**
	 * Send info about own vpn-state to requesting user.
	 * 
	 * @param userName
	 */
	public void onPrgMsgQueryVPNState(String userName) {
		sendProgramMessageVPNState(vpnIp == null ? false : true, userName);		
	}

	/**
	 * Start a S4-game as player after request from host.
	 * 
	 * @param senderName			Name of the host
	 * @param ip					IP of the host
	 * @param playername			Name of the player
	 * @param ipUserlistEncoded		list of the ips for each user in this game
	 */
	public void startS4Game(String senderName, String ip, String playername, String ipUserlistEncoded) {
		OpenGame game = getOpenJoinGame();
		// reset goods for players (this is done on host on game-creation)
		game.setGoods(game.getGoods() - 1);
		@SuppressWarnings("static-access")
		String ipUserlist = CryptoSupport.decode(ipUserlistEncoded, senderName, this.getIRCCommunicator().CRYPTO_MULTIPART_MESSAGE_TOKEN);
		// -> show a countdown of 10 seconds until the game starts
		if( game != null ) {
			if( getOpenJoinGame().getHostUserName().equals(senderName) ) {
				showS4Countdown(game, ipUserlist);
			}
		}
		//-> remove openjoingame as we never know when a s4-game is over
		setAsOpenJoinGame(null);
		// -> remove options-window
		Test.output("setS4GameOptions eeee");
		getGUI().setS4GameOptions(new UserInGameList(), false, false);
	}
	
	/**
	 * Start a S4-game as host after a countdown of 10 seconds.
	 * 
	 * @param openGame
	 * @param ipUserList
	 */
	public void startS4GameAsHost(OpenGame openGame, String ipUserList) {
		showS4Countdown(openGame, ipUserList);
	}
	
	/**
	 * Show a countdown for host and player.
	 * 
	 * @param ipUserList 
	 * @param openGame 
	 */
	private void showS4Countdown(OpenGame openGame, String ipUserList) {
		// show hint with countdown
		MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
		messagewindow.addMessage(
			"Dein Siedler 4 Spiel startet gleich!",
			UIManager.getIcon("OptionPane.infoIcon")
		);
		JButton btnTime = messagewindow.addButton(
			"10 Sekunden",
			new ActionListener() {
			    @Override
			    public void actionPerformed(ActionEvent e) {
			    	return;
			    }
			},
			addActionListenerForMessageWindowButton(messagewindow)
		);
		btnTime.setBorder(BorderFactory.createEmptyBorder());
		
		// define a timer which will start the game 
        // after 10 seconds
        int delay = 10000; //milliseconds
        Timer timer = new Timer(delay, new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	// after countdown:
            	// -> change button-text as it will be see as long s4 is not started
            	btnTime.setText("Spiel startet");
            	// -> start the game
            	startS4Game(openGame, ipUserList);
            	// -> stop the timer as it may start s4 again after [delay] seconds
            	((Timer)evt.getSource()).stop();
            	// -> remove messagewindow
            	messagewindow.dispose();
            }
        });
		timer.start();
		
        // start the countdown-timer
        // -> shows the time until the ok-button is available
		scheduler.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				String text = btnTime.getText();
				text = text.replaceAll(" Sekunden", "");
				int seconds = Integer.valueOf(text);
				seconds--;
				if( seconds == 1 ) {
					btnTime.setText("1 Sekunde");
				}
				else if( seconds > 1 ) {
					btnTime.setText(seconds + " Sekunden");
				}
			}
		}, 0, 1, TimeUnit.SECONDS);
		
		// prevent closing by close-button
		messagewindow.preventClosing(true);
		// set window visible
		messagewindow.setVisible(true);
		
	}
	
	/**
	 * Start a S4-game. This function is used from both: host and players.
	 * 
	 * Steps:
	 * 1. Generate netgame.ini with contents described in function.
	 * 2. Call S4Starter to start "s4.exe netgame=1"
	 * 
	 * @param game
	 * @param ipUserlist
	 */
	private void startS4Game(OpenGame game, String ipUserlist) {
		if( !config.isHideS4Interface() && game != null ) {
			// marker for successfull netgame.ini-writing
			boolean success = false;
			
			// is this user the host?
			boolean host = game.getHostUserName().equals(getNick()) ? true : false;
			
			// marker for slot of actual user
			int slot = 0;
			
			// get the resources
			// -> 0 => low goods
			// -> 1 => medium goods
			// -> 2 => high goods
			// => difference to S3: -1 for each value & "default goods" is not available
			int resources = game.getGoods();
			
			// calculate the player per team
			int playerPerTeam;
			int currentPlayer = game.getPlayerList().size();
            Test.output("currentPlayer number: " + currentPlayer);
			if (currentPlayer <= 1 || game.getTeamCount() < 1) {
			    // handle the case, when there is only one player in the game
			    // This also avoids a possible division by zero, that was already seen
			    playerPerTeam = 1;
			} else {
			    playerPerTeam = currentPlayer / game.getTeamCount();
			}
			
			// in which team this player is
			// -> will be overwritten for non-host in following code
			int playerIsInTeam = 1;
			
			// parse the ipUserlist
			// -> format: nickname1,ip;nickname2,ip2;nickname3,ip3 ..
			String[] ipUserArray = ipUserlist.split(";");
			UserInGameList gameStateUserList = getGameState().getUserList();
			for( String userData : ipUserArray ) {
				String[] userDataArray = userData.split(",");
				UserInGame user = gameStateUserList.getUser(userDataArray[0]);
				if( user != null ) {
					user.setIp(userDataArray[1]);
				}
			}
			// -> fill the list with placebo-users (AI and local-IP) to reach the max-players
			if( currentPlayer < game.getMaxPlayer() ) {
				// -> create the AI-user
				UserInGame user = new UserInGame("AI");
				user.setIp("127.0.0.1");
				// -> loop through the remaining users to add the AI-user for each spot
				int aiUsers = game.getMaxPlayer() - currentPlayer;
				for( int i = 0; i < aiUsers; i++ ) {
					gameStateUserList.updateUser(user, true);
					game.addPlayer(user.toString(), true);
				}
			}
			
			// marker for savegame (1) or not (0)
			int savegame = 0;
			
			// get the mapname
			// -> if the mapname consists of "Save\" than replace the mapname with
			//    its original-mapname and set Savegame to 1
			String mapname = game.getMap();
			if( mapname.startsWith(ALobbyConstants.PATH_S4SAVEGAMES + File.separator) ) {
				mapname = ALobbyConstants.PATH_S4MULTI + File.separator + mapname.replace(ALobbyConstants.PATH_S4SAVEGAMES + File.separator, "") + ".map"; 
				savegame = 1;
			}
			// -> if the mapname consists of "Random\" than replace this
			//    with nothing and wrap the resulting string with < and >
			if( mapname.startsWith(ALobbyConstants.PATH_RANDOM + File.separator) ) {
				mapname = "<" + mapname.replace(ALobbyConstants.PATH_RANDOM + File.separator, "") + ">";
			}
			else {
				// otherwhise add "Map\" in front of the mapname-string
				mapname = "Map\\" + mapname;
			}
			
			// -> GameMode
		    // --> possible values:
		    // -----> 1 = normal game:
		    // ---------> At least two players required, though one of the two can be the AI.
		    // -----> 2 = Cooperation Mode (Coop/Koop)
		    // ---------> At least two players required, though one of the two can be the AI.
		    // ---------> Always opens with the max players the map supports, so even if e.g. a game was created
		    // ---------> for 2 players, but the map supports 8 players, all 8 spots will be open. The host needs
		    // ---------> to set those additional spots to be played by the AI.
		    // -----> 3 = Economy Mode (Eco/WiMo)
		    // ---------> At least two *human* players required to start a game. Maybe (not tested) even all
		    // ---------> players must be human players.
		    // -----> >= 4 => same as 1
		    // --> Setmaps have hardcoded values which one of the three modes they support.
		    // --> Starting a map in a mode it does not support *seems* to be possible via the netgame.ini,
		    // --> but more than just starting was not tested!
			int gameMode = 1;
			if( false != game.isWimo() ) {
				gameMode = 3;
			}
			if( false != game.isCoop() ) {
				gameMode = 2;
			}
			
			// open directory where netgame.ini should be written
			File s4ExeDir = new File(this.config.getGamePathS4()).getParentFile();
			
			// open the .ip-file for writing
			File ipFile = new File(s4ExeDir, ".ip");
			
			// write actual used LAN-ip in the .ip-file if this is _not_ a VPN-game
			if( false == checkIfVpnIsUsed() ) {
				
				// generate buffer for writing in file
				BufferedWriter ipout = null;
				try  
				{
					// generate filewriter
				    FileWriter fstream = new FileWriter(ipFile);
				    ipout = new BufferedWriter(fstream);
				    ipout.write(this.getLocalIp());
	    		}
				catch (IOException e)
				{
					Test.outputException(e);
				}
				finally
				{
				    if(ipout != null) {
				        try {
				        	// close the open buffer and end writing
				        	ipout.close();
						} catch (IOException e) {
							// ignore
						} catch (SecurityException e) {
			    			Test.outputException(e);
						}
				    }
				}
			}
			else {
				// otherwise (if it is a VPN-game) delete the .ip-file if it exists
				if( ipFile.exists() ) {
					ipFile.delete();
				}
			}
			
			// open directory where netgame.ini should be written
			File s4Dir = new File(this.config.getGamePathS4()).getParentFile().getParentFile();
			
			// open netgame.ini itself for writing
			File netgameIni = new File(s4Dir, "netgame.ini");
			
			// generate buffer for writing in file
			BufferedWriter out = null;
			try  
			{
				// generate filewriter
			    FileWriter fstream = new FileWriter(netgameIni);
			    out = new BufferedWriter(fstream);
			    
			    /** write each of the following lines:
			    GameID=1
	    		GameName=TestGame
	    		MapName=Map/Multiplayer/Alien.map
	    		GameMode=1
	    		Resources=2
	    		ProductID=3
	    		NumberOfPlayers=2
	    		NumberOfTeams=2
	    		PlayerIP0=10.3.0.18
	    		PlayerIP1=10.3.1.122
	    		PlayerName0=Player0
	    		PlayerName1=Player1
	    		PlayerID0=1
	    		PlayerID1=2
	    		SessionID=1
	    		ProcedureServer=127.0.0.1
	    		ProcedureServerPort=0
	    		SaveGame=0
	    		ClanShortcut=0
	    		IsClanGame=0
	    		IsLadderGame=0
	    		PlayerTeam=0
	    		LocalPlayerCreator=true
	    		LocalPlayerSlot=0
	    		IsTrojan=1
	    		Multiplayertimedelay=800
	    		*/
			    // -> GameId
			    out.write("GameID=1\n");
			    // -> GameName
			    out.write("GameName=" + game.getName() + "\n");
			    // -> MapName
			    out.write("MapName=" + mapname + "\n");
			    // -> GameMode
			    out.write("GameMode=" + gameMode +"\n");
			    // -> Resources
			    out.write("Resources=" + resources + "\n");
			    // -> ProductID
			    out.write("ProductID=3\n");
			    // -> Number of Players
			    // ---> No matter of the GameMode must be at least 2 and max 8
			    out.write("NumberOfPlayers=" + game.getMaxPlayer() + "\n");
			    // -> Number of Teams
			    out.write("NumberOfTeams=" + game.getTeamCount() + "\n");
			    // -> loop through the players an generate for each entries
			    int playercount = 0;
			    // -> but first the host:
			    // --> PlayerIP (yes, his ip)
		    	out.write("PlayerIP" + playercount + "=" + game.getIP() + "\n");
		    	// --> Playername (his name)
		    	out.write("PlayerName" + playercount + "=" + game.getHostUserName() + "\n");
		    	// --> PlayerID (his playernumber in the game, beginning with 1)
		    	out.write("PlayerID" + playercount + "=" + ( playercount+1 ) + "\n");
		    	playercount++;
		    	// -> than the players
		    	int teamcount = 1;
			    for( String user : game.getPlayerList() ) {
			        if (playercount >= game.getMaxPlayer()) {
			            Test.output("The game has max players " + game.getMaxPlayer() + ", but we try to create " + game.getPlayerList().size() + " players...");
			            break;
			        }
			    	if( !user.equals(game.getHostUserName()) ) {
			    		String ip = "";
			    		if( gameStateUserList != null && gameStateUserList.getUser(user) != null ) {
			    			ip = gameStateUserList.getUser(user).getIp();
			    		}
				    	// --> PlayerIP
				    	out.write("PlayerIP" + playercount + "=" + ip + "\n");
				    	// --> Playername
				    	out.write("PlayerName" + playercount + "=" + user + "\n");
				    	// --> PlayerID
				    	out.write("PlayerID" + playercount + "=" + ( playercount+1 ) + "\n");
				    	// --> Team assignment
				    	if( ( playercount + 1 ) % playerPerTeam == 0 ) {
				    		teamcount++;
				    	}
				    	playerIsInTeam = teamcount;
				    	// --> save the position of the actual user for other configuration-point
				    	if( user.equals(getNick()) ) {
				    		slot = playercount;
				    	}
				    	playercount++;
			    	}
			    }
			    // -> SessionID
			    out.write("SessionID=1\n");
			    // -> ProcedureServer
			    out.write("ProcedureServer=127.0.0.1\n");
			    // -> ProcedureServerPort
			    out.write("ProcedureServerPort=0\n");
			    // -> Savegame
			    out.write("SaveGame=" + savegame + "\n");
			    // -> ClanShortcut
			    out.write("ClanShortcut=0\n");
			    // -> IsClanGame
			    out.write("IsClanGame=0\n");
			    // -> IsLadderGame
			    out.write("IsLadderGame=0\n");
			    // -> PlayerTeam => number of team this player should be join
			    out.write("PlayerTeam=" + ( host ? 1 : playerIsInTeam ) + "\n");
			    // -> LocalPlayerCreator => marker for host
			    out.write("LocalPlayerCreator=" + ( host ? "true" : "false" ) + "\n");
			    // -> LocalPlayerSlot => position for user in playerlist
			    out.write("LocalPlayerSlot=" + slot + "\n");
			    // -> IsTrojan
			    out.write("IsTrojan=" + ( game.isTrojans() ? "1" : "0" ) + "\n");
			    // -> Multiplayertimedelay
			    out.write("Multiplayertimedelay=800\n");
			}
			catch (IOException e)
			{
				Test.outputException(e);
			}
			finally
			{
			    if(out != null) {
			        try {
			        	// close the open buffer and end writing
						out.close();
						// mark this as succeed
						success = true;
					} catch (IOException e) {
						// ignore
					} catch (SecurityException e) {
		    			Test.outputException(e);
					}
			    }
			}
			if( success ) {
				// netgame.ini has been successfull written
				// -> lets start the game
				S4Starter s4Starter;
				try {
					s4Starter = new S4Starter(this);
					s4Starter.start();
				} catch (IOException e) {
					Test.outputException(e);
				}
			}
		}
	}

	/**
	 * Answer query for beta-users, which is send from other beta-users, if this here user runs a beta-version.
	 * 
	 * @param userName	the receiver for this answer
	 */
	public void onPrgMsgQueryBetaUsers(String userName) {
		if( isBetaUsed() ) {
			ircCommunicator.sendProgramMessageTo(protocol.createMsgBetaUsersJoined(getNick()), userName);
		}
	}
	
	/**
	 * Create a named pipe which can be used from external tools to communicate with
	 * the aLobby-instance. E.g. the protocolhandler to pass a link to download a map.
	 * 
	 * Only used on windows-systems.
	 * 
	 * hint: use PipeList to show the available namedpipes on the actual system: https://docs.microsoft.com/en-us/sysinternals/downloads/pipelist
	 * 
	 * @author Zwirni
	 * @Source https://github.com/java-native-access/jna/blob/master/contrib/platform/test/com/sun/jna/platform/win32/Kernel32NamedPipeTest.java
	 */
	private void createNamedPipe() {
    	// define the pipename which will be available for external tools to talk to
		// the namedpipe of the alobby
		String pipename = "\\\\.\\pipe\\alobby";
		
		// load the security-settings from alobby-logfile as preset for the named pipe
		SECURITY_DESCRIPTOR_RELATIVE securityDescriptor = Advapi32Util.getFileSecurityDescriptor(new File(SettlersLobby.configDir, "log.txt"), false);
		
		// define the security-settings for named pipe
		WinBase.SECURITY_ATTRIBUTES saAttr = new WinBase.SECURITY_ATTRIBUTES();
	    saAttr.dwLength = new WinDef.DWORD(saAttr.size());
	    saAttr.bInheritHandle = true;
	    saAttr.lpSecurityDescriptor = securityDescriptor.getPointer();
		
		// create the named pipe with the defined name
		HANDLE hNamedPipe = Kernel32.INSTANCE.CreateNamedPipe(
				pipename,																	// name of the pipe
				WinBase.PIPE_ACCESS_DUPLEX,        											// dwOpenMode
				WinBase.PIPE_TYPE_BYTE | WinBase.PIPE_READMODE_BYTE | WinBase.PIPE_WAIT,    // dwPipeMode
				1,    																		// nMaxInstances,
				Byte.MAX_VALUE,    															// nOutBufferSize,
				Byte.MAX_VALUE,    															// nInBufferSize,
				1000,    																	// nDefaultTimeOut,
				saAttr    																	// lpSecurityAttributes
		);
		
		// if namedpipe could not be created do nothing more and log an error
		if( WinBase.INVALID_HANDLE_VALUE.equals(hNamedPipe) ) {
			Test.output("error named pipe creation");
		}
		else {
			// wait for an incoming connection to the namedpipe
			try {
				Test.output("connectednamedpine: " + Kernel32.INSTANCE.ConnectNamedPipe(hNamedPipe, null));

				// create a byte-array for the content
				byte[] readBuffer = new byte[1024];
				IntByReference lpNumberOfBytesRead = new IntByReference(0);
				
				// read the data from named pipe
				Test.output("readfile: " + Kernel32.INSTANCE.ReadFile(hNamedPipe, readBuffer, readBuffer.length, lpNumberOfBytesRead, null));
				
				// get the content from the named pipe
				String s = new String(readBuffer, 0, lpNumberOfBytesRead.getValue());
				// log the given content
				Test.output("content via named pipe: " + s);

				// process the given content depending on what is given and what the aLobby supports at the moment
				JSONObject obj = new JSONObject(s);
				if( !obj.isNull("URL") ) {
					// read the given url-string and format it as https-URL
					String url = obj.getString("URL").replace("alobby://", "https://");
					if( url.contains("mapbase.siedler3.net") ) {
						// if it is a mapbase-URL start the map-downloader after getting the mapname from url
						URL urlObject = new URL(url);
						String decodedUrlWithoutHost = urlObject.toURI().getPath();
			            decodedUrlWithoutHost = decodedUrlWithoutHost.replace('\uFFFD', '_');
			            // now we have the mapname
			            String mapname = new File(decodedUrlWithoutHost).getName();
			            Test.output("detected mapname from named pipe: " + mapname);
			            
			            // start download with alobby-own functions therefor
						CheckAndDownloadMapFile mapFile = new CheckAndDownloadMapFile(this);
						mapFile.setMap(mapname);
						mapFile.setNoMessage(true);
						if ( mapFile.checkMapFile() ) {
							// all ok
							// -> show hint to user
				        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
				        	// -> add message
				    		messagewindow.addMessage(
				    			"<html>" + I18n.getString("SettlersLobby.MAP_DOWNLOAD_OK") + "</html>", 
				    			UIManager.getIcon("OptionPane.infoIcon")
				    		);
				    		// -> add ok-button
				    		messagewindow.addButton(
				    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
				    			new ActionListener() {
				    			    @Override
				    			    public void actionPerformed(ActionEvent e) {
				    			    	messagewindow.dispose();
				    			    }
				    			}
				    		);
				    		// show it
				    		messagewindow.setVisible(true);
						}
						else {
							// error occured (e.g. if the map already exist)
							// -> show hint to user
				        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
				        	// -> add message
				    		messagewindow.addMessage(
				    			"<html>" + I18n.getString("SettlersLobby.MAP_DOWNLOAD_ERROR") + "</html>", 
				    			UIManager.getIcon("OptionPane.errorIcon")
				    		);
				    		// -> add ok-button
				    		messagewindow.addButton(
				    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
				    			new ActionListener() {
				    			    @Override
				    			    public void actionPerformed(ActionEvent e) {
				    			    	messagewindow.dispose();
				    			    }
				    			}
				    		);
				    		// show it
				    		messagewindow.setVisible(true);
						}
					}
				}

				// get the size which has been given
				int readSize = lpNumberOfBytesRead.getValue();
				Test.output("readsize: " + readSize);

				// reflect the content to the sender
				IntByReference lpNumberOfBytesWritten = new IntByReference(0);
				Test.output("writeFile: " + Kernel32.INSTANCE.WriteFile(hNamedPipe, readBuffer, readSize, lpNumberOfBytesWritten, null));

				// flush the pipe to allow the client to read the pipe's contents before disconnecting
				Test.output("flushbuffer: " + Kernel32.INSTANCE.FlushFileBuffers(hNamedPipe));
				// disconnect the pipe
				Test.output("disconnect: " + Kernel32.INSTANCE.DisconnectNamedPipe(hNamedPipe));
			} catch (URISyntaxException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} finally {    
				// clean up the pipe
				Test.output("Named pipe handle close: " + Kernel32.INSTANCE.CloseHandle(hNamedPipe));
				// restart the pipe
				createNamedPipe();
			}
		}
    }
	
	/**
	 * Set a specific JFrame, JWindow etc. on specified screen for multi screen environment.
	 * 
	 * @author Zwirni
	 * @param screenIdentifier
	 * @param window
	 */
	public void showOnSpecifiedScreen( String screenIdentifier, Window window )
	{
		// get the environment
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		
		// get the screen devices
	    GraphicsDevice[] gd = ge.getScreenDevices();
	    
	    // set index for screen devices initial to 0
	    int screen = 0;
	    int maxX = 0;

	    // search for the given screen in list of devices
	    for( int i=0;i<gd.length;i++ )
	    {
	    	maxX = maxX + (int)gd[i].getDefaultConfiguration().getBounds().getWidth();
	    	if( gd[i].getIDstring().equals(screenIdentifier) ) {
	    		screen = i;
	    	}
	    }
	    
	    // if screen is given and exists .. 
	    if( screen > -1 && screen < gd.length ) {
	    	// .. set the JFrame on specified position on this screen
	    	int x = gd[screen].getDefaultConfiguration().getBounds().x + window.getX();
	    	if( x > maxX ) {
	    		x = gd[screen].getDefaultConfiguration().getBounds().x;
	    	}
	    	window.setLocation(x, window.getY());
	    } else if( gd.length > 0 ) {
	    	// .. otherwise use screen 0
	    	window.setLocation(gd[0].getDefaultConfiguration().getBounds().x + window.getX(), window.getY());
	    } else {
	        Test.output("No Screen-data found");
	    }
   
	}

	/**
	 * Return the loginframe-object.
	 * 
	 * @return
	 */
	public LoginFrame getLoginframe() {
		return loginFrame;
	}

	public static Object getDisplayModes() {
		// TODO Automatisch generierter Methodenstub
		return null;
	}
	
}
