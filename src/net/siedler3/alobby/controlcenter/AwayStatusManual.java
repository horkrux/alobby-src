/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import net.siedler3.alobby.communication.IRCCommunicator;

public class AwayStatusManual extends AwayStatus
{
	private boolean isGameOpen = false;

	protected AwayStatusManual(SettlersLobby settlersLobby, IRCCommunicator ircCommunicator, boolean isUserAway)
	{
		super(settlersLobby, ircCommunicator, isUserAway);
	}

	protected void setGameOpen(boolean value)
	{
		isGameOpen = value;
	}

	@Override
    protected void doBack()
	{
		super.doBack();
		if (settlersLobby.isActivateAwayOnGameStart() && isGameOpen)
		{
			settlersLobby.setAwayStatus(new AwayStatusInGame(settlersLobby, ircCommunicator, false));
		} else if (settlersLobby.isActivateAwayOnIdle())
		{
			settlersLobby.setAwayStatus(new AwayStatusAutomatic(settlersLobby, ircCommunicator, false));
		}
	}

	@Override
    public void onGameStarted()
	{
		isGameOpen = true;
        if (settlersLobby.isActivateAwayOnGameStart())
        {
            settlersLobby.setAwayStatus(new AwayStatusInGame(settlersLobby, ircCommunicator, isUserAway));
        }
	}

	@Override
	public void onGameClosed()
	{
		isGameOpen = false;
	}
}
