/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import net.siedler3.alobby.controlcenter.ALobbyTheme.ALobbyThemes;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.s3gameinterface.GameStarterVanilla;
import net.siedler3.alobby.s3gameinterface.PixelColorChecker;
import net.siedler3.alobby.s3gameinterface.S3Language;
import net.siedler3.alobby.util.CryptoSupport;
import net.siedler3.alobby.util.S3Support;
import net.siedler3.alobby.util.s4support.S4Support;
import net.siedler3.alobby.xmlcommunication.XMLCommunicator;
import net.siedler3.alobby.xmlcommunication.XMLFiles;


/**
 * Enthält alle Elemente, die aus der config.xml gelesen bzw. in sie geschrieben werden.
 *
 * @author jim
 * @author Stephan Bauer (aka maximilius)
 *
 */
public class Configuration
{
	private final static int STANDARD_PORT = 6667;

	private static final String XML_FAVORITE_S3MAPS_MAP = "favoriteMaps/map"; //$NON-NLS-1$
	private static final String XML_FAVORITE_S4MAPS_MAP = "favoriteS4Maps/map"; //$NON-NLS-1$

    private static final String XML_FAVORITE_S3MAPS_MAX_SIZE = "favoriteMaps/maxSize"; //$NON-NLS-1$
    private static final String XML_FAVORITE_S4MAPS_MAX_SIZE = "favoriteMaps/maxSize"; //$NON-NLS-1$

    public static final String CONFIG_XML = "config.xml"; //$NON-NLS-1$

    private static final String XML_LANGUAGE = "language"; //$NON-NLS-1$
    private static final String XML_IP_VIA_URL = "url/ip"; //$NON-NLS-1$
    private static final String XML_WINDOW_POSITION = "windowPosition"; //$NON-NLS-1$
    private static final String XML_WINDOW_MAXIMISED = "windowMaximised"; //$NON-NLS-1$
    private static final String XML_SEARCH_AND_INSTALL_UPDATE_AUTOMATICALLY = "searchAndInstallUpdateAutomatically"; //$NON-NLS-1$
    private static final String XML_FLASHING = "flashing"; //$NON-NLS-1$
    private static final String XML_FLASHING_PLAY_SOUND = "playSoundWhenFlashing";
    private static final String XML_CHAT_LOGGING = "chatLogging"; //$NON-NLS-1$
    private static final String XML_SHOW_TIME = "showTime"; //$NON-NLS-1$
    private static final String XML_THEME = "theme"; //$NON-NLS-1$
    private static final String XML_PREVENT_AUTOMATIC_CHRISTMAS_THEMES = "preventAutomaticChristmasThemes"; //$NON-NLS-1$
    private static final String XML_PREVENT_CHRISTMAS_HINTS = "preventChristmasHints"; //$NON-NLS-1$
    private static final String XML_PREVENT_AUTOMATIC_HALLOWEEN_THEMES = "preventAutomaticHalloweenThemes"; //$NON-NLS-1$
    private static final String XML_PREVENT_HALLOWEEN_HINTS = "preventHalloweenHints"; //$NON-NLS-1$
    private static final String XML_PREVENT_AUTOMATIC_EASTER_THEMES = "preventAutomaticEeasterThemes"; //$NON-NLS-1$
    private static final String XML_PREVENT_EASTER_HINTS = "preventEasterHints"; //$NON-NLS-1$
    private static final String XML_PREVENT_APRILSFOOL = "aprilsFool"; //$NON-NLS-1$
    private static final String XML_SAVE_WINDOW_POSITION = "saveWindowPosition"; //$NON-NLS-1$
    private static final String XML_IN_GAME_STOP_STORING_TOOLS = "inGame/stopStoringTools"; //$NON-NLS-1$
    private static final String XML_IN_GAME_DISABLE_AUTOSAVE_SOUND = "inGame/disableAutosaveSound"; //$NON-NLS-1$
    private static final String XML_IN_GAME_AUTO_SAVE_SOUND_FILE = "inGame/autoSaveSoundFile"; //$NON-NLS-1$
    private static final String XML_IN_GAME_BLINK_SOUND_FILE = "blinkSoundFile"; //$NON-NLS-1$
    private static final String XML_IN_GAME_AUTO_SAVE_TIMER = "inGame/autoSaveTimer"; //$NON-NLS-1$
    private static final String XML_IN_GAME_AUTO_SAVE = "inGame/autoSave"; //$NON-NLS-1$
    private static final String XML_GAME_STARTER_ACTIVATE_SCREEN_SIZE_CHECK = "gameStarter/activateScreenSizeCheck"; //$NON-NLS-1$
    private static final String XML_GAME_STARTER_ACTIVATE_FOREGROUND_WINDOW_CHECK = "gameStarter/activateForegroundWindowCheck"; //$NON-NLS-1$
    private static final String XML_GAME_STARTER_ACTIVATE_S3STATS_SCREEN_CAPTURE = "inGame/activateS3StatsScreenCapture"; //$NON-NLS-1$
    private static final String XML_GAME_STARTER_COLOR_CHECK_VARIATION = "gameStarter/colorCheckVariation"; //$NON-NLS-1$
    private static final String XML_GAME_STARTER_MAP_SELECT_FAST = "gameStarter/mapSelectFast"; //$NON-NLS-1$
    private static final String XML_GAME_STARTER_MAP_SELECT_DELAY_AFTER_CLICK = "gameStarter/mapSelectDelayAfterClick"; //$NON-NLS-1$
    private static final String XML_GAME_STARTER_USE_SEND_INPUT_NATIVE = "gameStarter/useSendInputNative"; //$NON-NLS-1$
    private static final String XML_GAME_STARTER_GAME_DATETIME_FORMAT = "gameStarter/gameDateTimeFormat"; //$NON-NLS-1$
    private static final String XML_HIGHLIGHTING_WORDS_WORD = "highlightingWords/word"; //$NON-NLS-1$
    private static final String XML_ACTIVATE_AWAY_ON_GAME_START = "activateAwayOnGameStart"; //$NON-NLS-1$
    private static final String XML_ACTIVATE_AWAY_ON_IDLE = "activateAwayOnIdle"; //$NON-NLS-1$
    private static final String XML_LOCAL_MODE = "localMode"; //$NON-NLS-1$
    private static final String XML_AUTO_CONNECT_VPN = "autoconnectVpn"; //$NON-NLS-1$
    private static final String XML_THIRD_PARTY_FIREWALL_WARNING = "warnAboutThirdPartyFirewall"; //$NON-NLS-1$
    private static final String XML_HOST_USING_VPN = "hostUsingVpn"; //$NON-NLS-1$
    private static final String XML_AWAY_TIME = "awayTime"; //$NON-NLS-1$
    private static final String XML_FONT_SIZE = "fontSize"; //$NON-NLS-1$
    private static final String XML_DEFAULT_AWAY_MSG = "defaultAwayMsg"; //$NON-NLS-1$
    private static final String XML_AUTO_LOAD_AND_SAVE_BUDDYS = "autoLoadAndSaveBuddys"; //$NON-NLS-1$
    private static final String XML_TRUST_ONLY_ENCRYPTED_MESSAGES = "trustOnlyEncryptedMessages"; //$NON-NLS-1$
    private static final String XML_NOTIFY_IF_BUDDY_COMES_ONLINE = "notifyIfBuddyComesOnline"; //$NON-NLS-1$
    private static final String XML_ENCRYPTION = "encryption"; //$NON-NLS-1$
    private static final String XML_URL_HILFE = "url/hilfe"; //$NON-NLS-1$
    private static final String XML_TIMEOUT = "timeOut"; //$NON-NLS-1$
    private static final String XML_GAME_PATH = "gamePath"; //$NON-NLS-1$
    private static final String XML_GAME_PATHS4 = "gamePathS4"; //$NON-NLS-1$
    private static final String XML_GAME_COMPATIBILITY = "gameCompatibility"; //$NON-NLS-1$
    private static final String XML_PASSWORD = "password"; //$NON-NLS-1$
    private static final String XML_USER_NAME = "userName"; //$NON-NLS-1$
    private static final String XML_URL_MAP_DOWNLOAD = "url/mapDownload"; //$NON-NLS-1$
    private static final String XML_URL_BUDDY_IGNORE_URL = "url/buddyIgnoreUrl"; //$NON-NLS-1$
    private static final String XML_URL_AUTOMATIC_MAP_DOWNLOAD = "url/automaticMapDownload"; //$NON-NLS-1$
    private static final String XML_URL_GET_MAP_DATA = "url/mapBaseMapData"; //$NON-NLS-1$
    private static final String XML_URL_MAP_SEARCH = "url/mapBaseSearch"; //$NON-NLS-1$
    private static final String XML_URL_GET_MAP_MD5 = "url/mapBaseMapMd5"; //$NON-NLS-1$
    private static final String XML_URL_GET_MAP_STAT = "url/mapBaseMapStat"; //$NON-NLS-1$
    private static final String XML_URL_GET_RANDOM_MAP = "url/mapBaseRandomMap"; //$NON-NLS-1$
    private static final String XML_URL_SCREENUPLOADER = "url/screenUploader"; //$NON-NLS-1$
    private static final String XML_URL_SCREENUPLOADER_DOMAIN = "url/screenUploaderDomain"; //$NON-NLS-1$
    private static final String XML_URL_LEAGUE = "url/league"; //$NON-NLS-1$
    private static final String XML_URL_LEAGUE_INTERFACE = "url/leagueInterface"; //$NON-NLS-1$
    private static final String XML_URL_LEAGUE_GAME_COUNT = "url/leagueGameCount"; //$NON-NLS-1$
    private static final String XML_URL_LEAGUE_RANKING_LISTS = "url/leagueRankingLists"; //$NON-NLS-1$
    private static final String XML_URL_TOURNAMENT_PLAYER_LIST = "url/tournamentPlayerList"; //$NON-NLS-1$
    private static final String XML_URL_TOURNAMENT_USER_ALLOWED = "url/tournamentUserAllowed"; //$NON-NLS-1$
    private static final String XML_URL_TETRIS_HIGHSCORE = "url/tetrisHighscore"; //$NON-NLS-1$
    private static final String XML_CHANNEL = "channel"; //$NON-NLS-1$
    private static final String XML_SERVER_URL = "serverURL"; //$NON-NLS-1$
    private static final String XML_SORT_BUDDYS = "sort/buddys"; //$NON-NLS-1$
    private static final String XML_SORT_AWAY = "sort/away"; //$NON-NLS-1$
    private static final String XML_SORT_INGAME = "sort/ingame"; //$NON-NLS-1$
    private static final String XML_URL_LOBBY_IS_NEW_VERSION = "url/isLobbyNewVersion"; //$NON-NLS-1$
    private static final String XML_URL_TIME = "url/time"; //$NON-NLS-1$
    private static final String XML_HIDE_LEAGUE_INTERFACE = "hideLeagueInterface"; //$NON-NLS-1$
    private static final String XML_HIDE_TEAMSPEAK_WINDOW = "hideTeamspeakWindow"; //$NON-NLS-1$
    private static final String XML_TABBED_CHAT_PANE = "tabbedChatPane"; //$NON-NLS-1$
    private static final String XML_SAVE_MANAGER_START_SAVEMANAGER = "saveManager/startSavemanager"; //$NON-NLS-1$
    private static final String XML_SAVE_MANAGER_START_WATCHDOG = "saveManager/startWatchdog"; //$NON-NLS-1$
    private static final String XML_SAVE_MANAGER_AUTO_PLACE = "saveManager/autoPlace"; //$NON-NLS-1$
    private static final String XML_SAVE_MANAGER_BACKUP_COUNT = "saveManager/backupCount"; //$NON-NLS-1$
    private static final String XML_SAVE_MANAGER_CLEANUP_TIME = "saveManager/cleanupTime"; //$NON-NLS-1$
    private static final String XML_SAVE_MANAGER_DO_CLEANUP = "saveManager/doCleanup"; //$NON-NLS-1$
    private static final String XML_SAVE_MANAGER_SAVE_CLEANUP_TIME = "saveManager/saveCleanupTime"; //$NON-NLS-1$
    private static final String XML_SAVE_MANAGER_DO_SAVE_CLEANUP = "saveManager/doSaveCleanup"; //$NON-NLS-1$
    private static final String XML_AUTOCOMPLETENICKNAMES = "autoCompleteNicknames"; //$NON-NLS-1$
    private static final String XML_TETRISHIGHSCORE = "tetrisHighscore"; //$NON-NLS-1$
    private static final String XML_HIDE_S4_INTERFACE = "hideS4Interface"; //$NON-NLS-1$
    private static final String XML_S3VERSIONNUMBER = "S3versionnumber"; //$NON-NLS-1$
    private static final String XML_ACTIVEMAINTAB = "activemaintab"; //$NON-NLS-1$
    private static final String XML_S3CERESOLUTION = "s3ceResolution"; //$NON-NLS-1$

    // favorite settings for new s3-game window
    private static final String XML_OPENNEWGAME_TAB = "newGameFavoriteSettings/tab"; //$NON-NLS-1$
    private static final String XML_OPENNEWS3GAME_TAB = "newS3GameFavoriteSettings/tab"; //$NON-NLS-1$
    private static final String XML_OPENNEWS4GAME_TAB = "newS4GameFavoriteSettings/tab"; //$NON-NLS-1$
    private static final String XML_OPENNEWS3GAME_GOODS = "newS3GameFavoriteSettings/goods"; //$NON-NLS-1$
    private static final String XML_OPENNEWS4GAME_GOODS = "newS4GameFavoriteSettings/goods"; //$NON-NLS-1$
    private static final String XML_OPENNEWS3GAME_RANDOMPOSITION = "newS3GameFavoriteSettings/randomPosition"; //$NON-NLS-1$
    private static final String XML_OPENNEWS3GAME_MINICHAT = "newS3GameFavoriteSettings/minichat"; //$NON-NLS-1$
    private static final String XML_OPENNEWS3GAME_WIMO = "newS3GameFavoriteSettings/wimo"; //$NON-NLS-1$
    
    // settings for s4-game window
    private static final String XML_OPENNEWS4GAME_WIMO = "newS4GameFavoriteSettings/wimo"; //$NON-NLS-1$
    private static final String XML_OPENNEWS3GAME_LEAGUE = "newS3GameFavoriteSettings/league"; //$NON-NLS-1$
    private static final String XML_OPENNEWS3GAME_TOURNAMENT = "newS3GameFavoriteSettings/tournament"; //$NON-NLS-1$
    private static final String XML_OPENNEWS4GAME_TROJANS = "newS4GameFavoriteSettings/trojans"; //$NON-NLS-1$
    private static final String XML_OPENNEWS4GAME_COOP = "newS4GameFavoriteSettings/coop"; //$NON-NLS-1$
    private static final String XML_OPENNEWS4GAME_PLAYERS = "newS4GameFavoriteSettings/players"; //$NON-NLS-1$
    private static final String XML_OPENNEWS4GAME_TEAMS = "newS4GameFavoriteSettings/teams"; //$NON-NLS-1$
    private static final String XML_OPENNEWS4GAME_RANDOMMAPSIZE = "newS4GameFavoriteSettings/randommapsize"; //$NON-NLS-1$
    private static final String XML_OPENNEWS4GAME_RANDOMLANDMASS = "newS4GameFavoriteSettings/randomlandmass"; //$NON-NLS-1$
    private static final String XML_OPENNEWS4GAME_RANDOMAXES = "newS4GameFavoriteSettings/randomaxes"; //$NON-NLS-1$
    private static final String XML_OPENNEWS4GAME_RANDOMRESOURCES = "newS4GameFavoriteSettings/randomresources"; //$NON-NLS-1$
    private static final String XML_OPENNEWS4GAME_RANDOMCODE = "newS4GameFavoriteSettings/randomcode"; //$NON-NLS-1$
    
    // beta-hint for beta-users
    private static final String XML_BETA_HINT = "betahint"; //$NON-NLS-1$
    private static final String XML_S4_BETA_HINT = "newS4GameFavoriteSettings/betahint"; //$NON-NLS-1$
    private static final String XML_S4_DISABLE_BETA_HINT = "newS4GameFavoriteSettings/disablebetahint"; //$NON-NLS-1$
    
    private static final String XML_USE_DEL_KEY = "usedelkey"; //$NON-NLS-1$
    
    public final static int MAP_DOWNLOAD_STATUS_SHOW_DIALOG = 0;
	public final static int MAP_DOWNLOAD_STATUS_DOWNLOAD_MAP = 1;
	public final static int MAP_DOWNLOAD_STATUS_MANUAL_SEARCH = 2;

	private static final String XML_INGAME_WHISPER = "ingameWhisper";
	private static final String XML_INGAME_MESSAGES = "ingameMessages";
	
	private static final String XML_SCREEN = "screen";

	private static Configuration myInstance;

	private final Vector<ConfigurationListener> configurationListeners;
	public boolean isNotifyListener = true;

    private XMLCommunicator xmlConfig;
    private XMLCommunicator defaultConfig;

    private String urlIsLobbyNewVersion;
    private String defaultUrlIsLobbyNewVersion;
    private String urlTime;
    private String userName;
    private String encryptedPassword;
    private String gamePathS3;
    private String gamePathS4;
    private String serverURL;
    private List<String> defaultServerURLs;
    private String timeOut;
    private String channel;
    private List<String> defaultChannels;
    private String mapDownloadPath;
    private String defaultMapDownloadPath;
    private String automaticMapDownloadPath;
    private String defaultAutomaticMapDownloadPath;
    private String defaultGetMapData;
    private String defaultMapSearch;
    private String defaultGetMapMd5;
    private String defaultGetMapStat;
    private String defaultGetRandomMap;
    private String defaultScreenUploader;
    private String defaultScreenUploaderDomain;
    private String helpPath;
    private String buddyIgnoreUrl;
    private String defaultBuddyIgnoreUrl;

    private boolean gameCompability;
    private boolean doEncryption;
    private boolean trustOnlyEncryptedMessages;
    private int awayTime;
    private boolean activateAwayOnIdle;
    private boolean activateAwayOnGameStart;
    private boolean isLocalMode;
    private boolean isAutoLoadAndSaveBuddys;
    private boolean isStorePassword;
    private boolean isNotifyIfBuddyComesOnline;
    private String defaultAwayMsg;

    private List<String> ipViaUrl = new ArrayList<String>();
    private List<String> favoriteMaps = new ArrayList<String>();
    private List<String> favoriteS4Maps = new ArrayList<String>();
    private List<String> highlightingWords = new ArrayList<String>();
    private int favoriteS3MapsMaxSize = 10;
    private int favoriteS4MapsMaxSize = 10;
    private boolean saveWindowPosition = false;
    private Rectangle windowMainPosition;
    private boolean isWindowMainMaximised;

    private int fontSize;

    private boolean autoSave;
    private int autoSaveTimer;
    private String autoSaveSoundFile;
    private String blinkSoundFile;
    private boolean stopStoringTools;
    private boolean disableAutosaveSound;
    private boolean activateScreenSizeCheck;
    private boolean activateForegroundWindowCheck;
    private boolean activateS3StatsScreenCapture;
    private String gameDateTimeFormat;
    
    private long tetrisHighscore;
    private int newGameTab;
    private int newS3GameTab;
    private int newS4GameTab;
    private int newS3GameGoods;
    private int newS4GameGoods;
    private boolean newS3GameRandomPosition;
    private boolean newS3GameMinichat;
    private boolean newS3GameWimo;
    private boolean newS4GameWimo;
    private boolean newS3GameLeague;
    private int newS3GameTournament;
    private boolean newS4GameTrojans;
    private int newS4GamePlayers;
    private int newS4GameTeams;

    private boolean autoconnectVpn;
    private boolean warnAboutThirdPartyFirewall;
    private boolean hostUsingVpn;

    private boolean showTime = false;

    private ALobbyThemes theme;
    
    private boolean preventAutomaticChristmasTheme;
    private boolean preventChristmasHint;
    
    private boolean preventAutomaticHalloweenTheme;
    private boolean preventHalloweenHint;

    private boolean chatLogging = false;

    private boolean flashingWhenHighlighted = true;
    
    // configuration for playingfile when highlighted
    // -> deactivated at start
    private boolean playSoundWhenHighlighted = false;

    private boolean searchAndInstallUpdateAutomatically = false;

    private boolean isSortBuddys;
    private boolean isSortAway;
    private boolean isSortInGame;

    private final HashMap<String, List<String>> buddys = new HashMap<String, List<String>>();
    private final HashMap<String, List<String>> ignoredUsers = new HashMap<String, List<String>>();

	private Locale language;

	private boolean showWhisperIngame;
	private boolean showMessagesIngame;

    private String leagueUrl;
    private String leagueUrlInterface;
    private boolean hideLeagueInterface;
    private boolean hideTeamspeakWindow;

    private boolean isTabbedChatPane;

    private int backupCount;
    private int cleanupTime;
    private boolean doCleanup;
    private int saveCleanupTime;
    private boolean doSaveCleanup;
    private boolean startSavemanager;
    private boolean startWatchdog;
    private boolean autoPlace;
    private boolean autoCompleteNicknames;

	private String leagueUrlRankingLists;
	private String tournamentPlayerList;

	private String leagueUrlGameCount;

	private String tournamentUserAllowed;
	
	private String tetrisHighscoreURL;

	private boolean preventAutomaticEasterTheme;

	private boolean preventEasterHint;
	
	private boolean preventAprilsFool;

	private boolean hideS4Interface;

	private boolean preventDailyBetaHint;

	private int newS4GameRandomMapSize;

	private int newS4GameRandomLandmass;

	private int newS4GameRandomAxes;

	private String newS4GameRandomCode;

	private int newS4GameRandomResources;

	private boolean preventWeeklyS4BetaHint;

	private boolean preventS4BetaHint;

	private boolean useDelKey;

	private boolean newS4GameCoop;

	private int S3versionnumber;

	private int activemainTab;

	private String screen;

	private String s3ceResolution;

	public static Configuration getInstance() {
		if (myInstance == null) {
			myInstance = new Configuration();
		}
		return myInstance;
	}

    private Configuration() {
    	configurationListeners = new Vector<ConfigurationListener>();
    }

    public void addConfigurationListener(ConfigurationListener listener) {
    	configurationListeners.add(listener);
    }

    public void removeConfigurationListener(ConfigurationListener listener) {
    	configurationListeners.remove(listener);
    }

    /**
     * Wenn isNotifyListener true ist, werden alle ConfigurationListener über
     * die Veränderung in Kenntnis gesetzt.
     * Man kan mittels isNotifyListener bei Einstellungsdialog verhindern,
     * dass beim Speichern 300 mal die Listeners benachrichtigt werden.
     */
    public void notifyConfigurationListeners() {
    	if (isNotifyListener) {
	    	for (ConfigurationListener listener : configurationListeners) {
	    		listener.configurationWasChanged();
	    	}
	    }
    }

    /**
     * Versucht die config.xml zu laden.
     * Gibt bei Scheitern Fehlernachricht aus und beendet das Programm.
     */
    boolean tryToLoadConfigOrDie( SettlersLobby settlersLobby )
    {
        boolean initialConfigCreated = false;
        try
        {
            File configFile = getConfigFilePath();

            if (!configFile.exists())
            {
                XMLFiles.createInitialFile(configFile.getAbsolutePath());
                initialConfigCreated = true;
            }
            //zuerst werden einige default Werte aus defaultconfig ausgelesen
            //und gespeichert
            defaultUrlIsLobbyNewVersion = emptyIfNull(defaultConfig.getByPath(XML_URL_LOBBY_IS_NEW_VERSION));
            defaultServerURLs = readListFromDefaultConfig(XML_SERVER_URL);
            defaultChannels = readListFromDefaultConfig(XML_CHANNEL);
            defaultAutomaticMapDownloadPath = emptyIfNull(defaultConfig.getByPath(XML_URL_AUTOMATIC_MAP_DOWNLOAD));
            defaultGetMapData = emptyIfNull(defaultConfig.getByPath(XML_URL_GET_MAP_DATA));
            defaultMapSearch = emptyIfNull(defaultConfig.getByPath(XML_URL_MAP_SEARCH));
            defaultGetMapMd5 = emptyIfNull(defaultConfig.getByPath(XML_URL_GET_MAP_MD5));
            defaultGetMapStat = emptyIfNull(defaultConfig.getByPath(XML_URL_GET_MAP_STAT));
            defaultGetRandomMap = emptyIfNull(defaultConfig.getByPath(XML_URL_GET_RANDOM_MAP));
            defaultScreenUploader = emptyIfNull(defaultConfig.getByPath(XML_URL_SCREENUPLOADER));
            defaultScreenUploaderDomain = emptyIfNull(defaultConfig.getByPath(XML_URL_SCREENUPLOADER_DOMAIN));
            defaultBuddyIgnoreUrl = emptyIfNull(defaultConfig.getByPath(XML_URL_BUDDY_IGNORE_URL));
            defaultMapDownloadPath = emptyIfNull(defaultConfig.getByPath(XML_URL_MAP_DOWNLOAD));
            //jetzt kommt die eigentliche config, fehlende Werte werden aus der
            //defaultConfig übernommen
            XMLFiles xmlFiles = XMLFiles.getInstance();
            xmlConfig = xmlFiles.getXMLCommunicatorOfFile(configFile.getAbsolutePath());
            xmlConfig.isDefaultValueWasUsed = false;
            String languageString = emptyIfNull(xmlConfig.getByPath(XML_LANGUAGE, defaultConfig));
            if (!languageString.equals("")) {
            	language = new Locale(languageString);
            }
            urlIsLobbyNewVersion = emptyIfNull(xmlConfig.getByPath(XML_URL_LOBBY_IS_NEW_VERSION, defaultConfig));
            urlTime = emptyIfNull(xmlConfig.getByPath(XML_URL_TIME, defaultConfig));
            userName = emptyIfNull(xmlConfig.getByPath(XML_USER_NAME, defaultConfig));
            encryptedPassword = emptyIfNull(xmlConfig.getByPath(XML_PASSWORD, defaultConfig));
            isStorePassword = !encryptedPassword.equals(""); //$NON-NLS-1$
            gamePathS3 = emptyIfNull(null);
            gamePathS4 = emptyIfNull(null);
            gameCompability = Boolean.parseBoolean(xmlConfig.getByPath(XML_GAME_COMPATIBILITY, defaultConfig));
            buddyIgnoreUrl = emptyIfNull(xmlConfig.getByPath(XML_URL_BUDDY_IGNORE_URL, defaultConfig));
            serverURL = emptyIfNull(xmlConfig.getByPath(XML_SERVER_URL, defaultConfig));
            channel = emptyIfNull(xmlConfig.getByPath(XML_CHANNEL, defaultConfig));
            timeOut = emptyIfNull(xmlConfig.getByPath(XML_TIMEOUT, defaultConfig));
            mapDownloadPath = emptyIfNull(xmlConfig.getByPath(XML_URL_MAP_DOWNLOAD, defaultConfig));
            automaticMapDownloadPath = emptyIfNull(
                    xmlConfig.getByPath(XML_URL_AUTOMATIC_MAP_DOWNLOAD, defaultConfig));
            leagueUrl = emptyIfNull(xmlConfig.getByPath(XML_URL_LEAGUE, defaultConfig));
            leagueUrlInterface = emptyIfNull(xmlConfig.getByPath(XML_URL_LEAGUE_INTERFACE, defaultConfig));
            leagueUrlGameCount = emptyIfNull(xmlConfig.getByPath(XML_URL_LEAGUE_GAME_COUNT, defaultConfig));
            leagueUrlRankingLists = emptyIfNull(xmlConfig.getByPath(XML_URL_LEAGUE_RANKING_LISTS, defaultConfig));
            tournamentPlayerList = emptyIfNull(xmlConfig.getByPath(XML_URL_TOURNAMENT_PLAYER_LIST, defaultConfig));
            tournamentUserAllowed = emptyIfNull(xmlConfig.getByPath(XML_URL_TOURNAMENT_USER_ALLOWED, defaultConfig));
            tetrisHighscoreURL = emptyIfNull(xmlConfig.getByPath(XML_URL_TETRIS_HIGHSCORE, defaultConfig));
            hideLeagueInterface = Boolean.parseBoolean(xmlConfig.getByPath(XML_HIDE_LEAGUE_INTERFACE, defaultConfig));
            hideTeamspeakWindow = Boolean.parseBoolean(xmlConfig.getByPath(XML_HIDE_TEAMSPEAK_WINDOW, defaultConfig));
            helpPath = emptyIfNull(xmlConfig.getByPath(XML_URL_HILFE, defaultConfig));
            doEncryption = Boolean.parseBoolean(xmlConfig.getByPath(XML_ENCRYPTION, defaultConfig));
            isSortBuddys = Boolean.parseBoolean(xmlConfig.getByPath(XML_SORT_BUDDYS, defaultConfig));
            isSortAway = Boolean.parseBoolean(xmlConfig.getByPath(XML_SORT_AWAY, defaultConfig));
            isSortInGame = Boolean.parseBoolean(xmlConfig.getByPath(XML_SORT_INGAME, defaultConfig));
            isNotifyIfBuddyComesOnline = Boolean.parseBoolean(
                    xmlConfig.getByPath(XML_NOTIFY_IF_BUDDY_COMES_ONLINE, defaultConfig));
            trustOnlyEncryptedMessages = Boolean.parseBoolean(
                    xmlConfig.getByPath(XML_TRUST_ONLY_ENCRYPTED_MESSAGES, defaultConfig));
            hideS4Interface = Boolean.parseBoolean(xmlConfig.getByPath(XML_HIDE_S4_INTERFACE, defaultConfig));
            S3versionnumber = Integer.parseInt(xmlConfig.getByPath(XML_S3VERSIONNUMBER, defaultConfig));
            s3ceResolution = emptyIfNull(xmlConfig.getByPath(XML_S3CERESOLUTION, defaultConfig));

            tetrisHighscore = zeroIfNull(Integer.parseInt(emptyIfNull(xmlConfig.getByPath(XML_TETRISHIGHSCORE, defaultConfig))));
            isAutoLoadAndSaveBuddys = Boolean.parseBoolean(xmlConfig.getByPath(XML_AUTO_LOAD_AND_SAVE_BUDDYS, defaultConfig));
            defaultAwayMsg = emptyIfNull(xmlConfig.getByPath(XML_DEFAULT_AWAY_MSG, defaultConfig));
            fontSize = Integer.parseInt(xmlConfig.getByPath(XML_FONT_SIZE, defaultConfig));
            if (defaultAwayMsg.isEmpty())
            {
                //diese zusätzliche Überprüfung ist nötig, damit die Default message angezeigt
                //wird, wenn "null" in der config steht.
                //beim oberen Aufruf wird die Default message nur gesetzt, wenn die message
                //gar nicht in der config Datei existiert.
                defaultAwayMsg = AwayStatus.MESSAGE_AWAY_DEFAULT;
            }
            awayTime = 1000*Integer.parseInt(xmlConfig.getByPath(XML_AWAY_TIME, defaultConfig));
            isLocalMode = Boolean.parseBoolean(xmlConfig.getByPath(XML_LOCAL_MODE, defaultConfig));
            activateAwayOnIdle = Boolean.parseBoolean(xmlConfig.getByPath(XML_ACTIVATE_AWAY_ON_IDLE, defaultConfig));
            activateAwayOnGameStart= Boolean.parseBoolean(xmlConfig.getByPath(XML_ACTIVATE_AWAY_ON_GAME_START, defaultConfig));
            ipViaUrl = readListFromConfig(XML_IP_VIA_URL, true);
            highlightingWords = readListFromConfig(XML_HIGHLIGHTING_WORDS_WORD);
            GameStarterVanilla.mapSelectDelayAfterClick = Integer.parseInt(emptyIfNull(
                    xmlConfig.getByPath(XML_GAME_STARTER_MAP_SELECT_DELAY_AFTER_CLICK, defaultConfig)));
            GameStarterVanilla.mapSelectFastVariant = Boolean.parseBoolean(
                    xmlConfig.getByPath(XML_GAME_STARTER_MAP_SELECT_FAST, defaultConfig));
            PixelColorChecker.colorVariation = Integer.parseInt(emptyIfNull(
                    xmlConfig.getByPath(XML_GAME_STARTER_COLOR_CHECK_VARIATION, defaultConfig)));
            GameStarterVanilla.useSendInputNative = Boolean.parseBoolean(
                    xmlConfig.getByPath(XML_GAME_STARTER_USE_SEND_INPUT_NATIVE, defaultConfig));
            activateScreenSizeCheck = Boolean.parseBoolean(
                            xmlConfig.getByPath(XML_GAME_STARTER_ACTIVATE_SCREEN_SIZE_CHECK, defaultConfig));
            activateForegroundWindowCheck = Boolean.parseBoolean(
                            xmlConfig.getByPath(XML_GAME_STARTER_ACTIVATE_FOREGROUND_WINDOW_CHECK, defaultConfig));
            activateS3StatsScreenCapture = Boolean.parseBoolean(
                    xmlConfig.getByPath(XML_GAME_STARTER_ACTIVATE_S3STATS_SCREEN_CAPTURE, defaultConfig));
            gameDateTimeFormat = emptyIfNull(xmlConfig.getByPath(XML_GAME_STARTER_GAME_DATETIME_FORMAT, defaultConfig));
            autoSave = Boolean.parseBoolean(
                    xmlConfig.getByPath(XML_IN_GAME_AUTO_SAVE, defaultConfig));
            autoSaveTimer = Integer.parseInt(emptyIfNull(
                    xmlConfig.getByPath(XML_IN_GAME_AUTO_SAVE_TIMER, defaultConfig)));
            autoSaveSoundFile = emptyIfNull(xmlConfig.getByPath(XML_IN_GAME_AUTO_SAVE_SOUND_FILE, defaultConfig));
            blinkSoundFile = emptyIfNull(xmlConfig.getByPath(XML_IN_GAME_BLINK_SOUND_FILE, defaultConfig));
            stopStoringTools = Boolean.parseBoolean(
                    xmlConfig.getByPath(XML_IN_GAME_STOP_STORING_TOOLS, defaultConfig));
            disableAutosaveSound = Boolean.parseBoolean(
                    xmlConfig.getByPath(XML_IN_GAME_DISABLE_AUTOSAVE_SOUND, defaultConfig));

            activemainTab = zeroIfNull(Integer.parseInt(xmlConfig.getByPath(XML_ACTIVEMAINTAB, defaultConfig)));
            
            newGameTab = zeroIfNull(Integer.parseInt(xmlConfig.getByPath(XML_OPENNEWGAME_TAB, defaultConfig)));
            newS3GameTab = zeroIfNull(Integer.parseInt(xmlConfig.getByPath(XML_OPENNEWS3GAME_TAB, defaultConfig)));
            newS4GameTab = zeroIfNull(Integer.parseInt(xmlConfig.getByPath(XML_OPENNEWS4GAME_TAB, defaultConfig)));
            newS3GameGoods = zeroIfNull(Integer.parseInt(xmlConfig.getByPath(XML_OPENNEWS3GAME_GOODS, defaultConfig)));
            newS4GameGoods = zeroIfNull(Integer.parseInt(xmlConfig.getByPath(XML_OPENNEWS4GAME_GOODS, defaultConfig)));
            newS3GameRandomPosition = Boolean.parseBoolean(xmlConfig.getByPath(XML_OPENNEWS3GAME_RANDOMPOSITION, defaultConfig));
            newS3GameMinichat = Boolean.parseBoolean(xmlConfig.getByPath(XML_OPENNEWS3GAME_MINICHAT, defaultConfig));
            newS3GameWimo = Boolean.parseBoolean(xmlConfig.getByPath(XML_OPENNEWS3GAME_WIMO, defaultConfig));
            newS3GameLeague = Boolean.parseBoolean(xmlConfig.getByPath(XML_OPENNEWS3GAME_LEAGUE, defaultConfig));
            newS3GameTournament = zeroIfNull(Integer.parseInt(xmlConfig.getByPath(XML_OPENNEWS3GAME_TOURNAMENT, defaultConfig)));
            newS4GamePlayers = Integer.parseInt(emptyIfNull(xmlConfig.getByPath(XML_OPENNEWS4GAME_PLAYERS, defaultConfig)));
            newS4GameTeams = Integer.parseInt(emptyIfNull(xmlConfig.getByPath(XML_OPENNEWS4GAME_TEAMS, defaultConfig)));
            newS4GameRandomMapSize = Integer.parseInt(emptyIfNull(xmlConfig.getByPath(XML_OPENNEWS4GAME_RANDOMMAPSIZE, defaultConfig)));
            newS4GameRandomLandmass = Integer.parseInt(emptyIfNull(xmlConfig.getByPath(XML_OPENNEWS4GAME_RANDOMLANDMASS, defaultConfig)));
            newS4GameRandomAxes = Integer.parseInt(emptyIfNull(xmlConfig.getByPath(XML_OPENNEWS4GAME_RANDOMAXES, defaultConfig)));
            newS4GameRandomResources = Integer.parseInt(emptyIfNull(xmlConfig.getByPath(XML_OPENNEWS4GAME_RANDOMRESOURCES, defaultConfig)));
            newS4GameRandomCode = emptyIfNull(xmlConfig.getByPath(XML_OPENNEWS4GAME_RANDOMCODE, defaultConfig));
            
            saveWindowPosition = Boolean.parseBoolean(xmlConfig.getByPath(XML_SAVE_WINDOW_POSITION, defaultConfig));
            
            preventS4BetaHint = Boolean.parseBoolean(xmlConfig.getByPath(XML_S4_DISABLE_BETA_HINT, defaultConfig));

            showTime = Boolean.parseBoolean(xmlConfig.getByPath(XML_SHOW_TIME, defaultConfig));
            
            useDelKey = Boolean.parseBoolean(xmlConfig.getByPath(XML_USE_DEL_KEY, defaultConfig));
            
            // create a Calendar-object to get the christmas-theme-config for the actual year
    		Calendar c = new GregorianCalendar();
            preventAutomaticChristmasTheme = Boolean.parseBoolean(xmlConfig.getByPath(XML_PREVENT_AUTOMATIC_CHRISTMAS_THEMES + "/year" + c.get(Calendar.YEAR), defaultConfig));
            preventChristmasHint = Boolean.parseBoolean(xmlConfig.getByPath(XML_PREVENT_CHRISTMAS_HINTS + "/year" + c.get(Calendar.YEAR), defaultConfig));
            preventAutomaticHalloweenTheme = Boolean.parseBoolean(xmlConfig.getByPath(XML_PREVENT_AUTOMATIC_HALLOWEEN_THEMES + "/year" + c.get(Calendar.YEAR), defaultConfig));
            preventHalloweenHint = Boolean.parseBoolean(xmlConfig.getByPath(XML_PREVENT_HALLOWEEN_HINTS + "/year" + c.get(Calendar.YEAR), defaultConfig));
            preventAutomaticEasterTheme = Boolean.parseBoolean(xmlConfig.getByPath(XML_PREVENT_AUTOMATIC_EASTER_THEMES + "/year" + c.get(Calendar.YEAR), defaultConfig));
            preventEasterHint = Boolean.parseBoolean(xmlConfig.getByPath(XML_PREVENT_EASTER_HINTS + "/year" + c.get(Calendar.YEAR), defaultConfig));
            
            preventAprilsFool = Boolean.parseBoolean(xmlConfig.getByPath(XML_PREVENT_APRILSFOOL + "/year" + c.get(Calendar.YEAR), defaultConfig));
            
            int month = c.get(Calendar.MONTH) + 1;
            String monthFormated = Integer.toString(month);
        	if( month < 10 ) {
        		monthFormated = "0" + month;
        	}
            preventDailyBetaHint = Boolean.parseBoolean(xmlConfig.getByPath(XML_BETA_HINT + "/day" + c.get(Calendar.YEAR) + monthFormated + c.get(Calendar.DAY_OF_MONTH), defaultConfig));
            
            preventWeeklyS4BetaHint = Boolean.parseBoolean(xmlConfig.getByPath(XML_S4_BETA_HINT + "/week" + c.get(Calendar.WEEK_OF_YEAR), defaultConfig));
            
            theme = ALobbyThemes.getEnum(xmlConfig.getByPath(XML_THEME, defaultConfig));
            
            // ignore the following if easter-theme should be ignored
            if( !this.getPreventAutomaticEasterTheme() ) {
	            // if theme is the easter-theme and its actual not eastertime,
	            // than set theme to default
	            if( this.isEasterTheme() && !this.itsEastertime() ) {
	            	theme = ALobbyThemes.S3;
	            }
	            
	            // if theme is not the easter-theme and its actual eastertime,
	            // than set the theme to easter
	            if( !this.isEasterTheme() && this.itsEastertime() ) {
	            	theme = ALobbyThemes.EASTER;
	            }
            }
            if( !this.getPreventAutomaticHalloweenTheme() ) {
            	// if theme is the Halloween-theme and its actual not Halloweentime,
	            // than set theme to default
	            if( this.isHalloweenTheme() && !this.itsHalloweentime() ) {
	            	theme = ALobbyThemes.S3;
	            }
	             
	            // if theme is not the Halloween-theme and its actual Halloweentime,
	            // than set the theme to Halloween
	            if( !this.isHalloweenTheme() && this.itsHalloweentime() ) {
	            	theme = ALobbyThemes.HALLOWEEN;
	            }
            }
            if( !this.getPreventAutomaticChristmasTheme() ) {
            	// if its not Halloweentime prevent the usage of halloween-theme
            	if( !this.itsHalloweentime() && this.isHalloweenTheme() ) {
            		theme = ALobbyThemes.S3;
            	}
            	
	            // if theme is the Christmas-theme and its actual not Christmastime,
	            // than set theme to default
	            if( this.isChristmasTheme() && !this.itsChristmastime() ) {
	            	theme = ALobbyThemes.S3;
	            }
	            
	            // if theme is not the Christmas-theme and its actual Christmastime,
	            // than set the theme to christmas
	            if( !this.isChristmasTheme() && this.itsChristmastime() ) {
	            	theme = ALobbyThemes.CHRISTMAS;
	            }
            }
            else {
            	if( !this.itsChristmastime() && this.isChristmasTheme() ) {
            		theme = ALobbyThemes.S3;
            	}
            	// if its not Halloweentime prevent the usage of halloween-theme
            	if( !this.itsHalloweentime() && this.isHalloweenTheme() ) {
            		theme = ALobbyThemes.S3;
            	}
            	// if its not Eastertime prevent the usage of easter-theme
            	if( !this.itsEastertime() && this.isEasterTheme() ) {
            		theme = ALobbyThemes.S3;
            	}
            }

            chatLogging = Boolean.parseBoolean(xmlConfig.getByPath(XML_CHAT_LOGGING, defaultConfig));

            flashingWhenHighlighted = Boolean.parseBoolean(xmlConfig.getByPath(XML_FLASHING, defaultConfig));

            // get configuration for playing sound when highlighted
            playSoundWhenHighlighted = Boolean.parseBoolean(xmlConfig.getByPath(XML_FLASHING_PLAY_SOUND, defaultConfig));
            
            searchAndInstallUpdateAutomatically = Boolean.parseBoolean(
            		xmlConfig.getByPath(XML_SEARCH_AND_INSTALL_UPDATE_AUTOMATICALLY, defaultConfig));

            showWhisperIngame = Boolean.parseBoolean(xmlConfig.getByPath(XML_INGAME_WHISPER, defaultConfig));
            showMessagesIngame = Boolean.parseBoolean(xmlConfig.getByPath(XML_INGAME_MESSAGES, defaultConfig));
            isTabbedChatPane = Boolean.parseBoolean(xmlConfig.getByPath(XML_TABBED_CHAT_PANE, defaultConfig));

            startSavemanager = Boolean.parseBoolean(xmlConfig.getByPath(XML_SAVE_MANAGER_START_SAVEMANAGER, defaultConfig));
            startWatchdog = Boolean.parseBoolean(xmlConfig.getByPath(XML_SAVE_MANAGER_START_WATCHDOG, defaultConfig));
            autoPlace = Boolean.parseBoolean(xmlConfig.getByPath(XML_SAVE_MANAGER_AUTO_PLACE, defaultConfig));
            backupCount = Integer.parseInt(xmlConfig.getByPath(XML_SAVE_MANAGER_BACKUP_COUNT, defaultConfig));
            cleanupTime = Integer.parseInt(xmlConfig.getByPath(XML_SAVE_MANAGER_CLEANUP_TIME, defaultConfig));
            doCleanup = Boolean.parseBoolean(xmlConfig.getByPath(XML_SAVE_MANAGER_DO_CLEANUP, defaultConfig));
            saveCleanupTime = Integer.parseInt(xmlConfig.getByPath(XML_SAVE_MANAGER_SAVE_CLEANUP_TIME, defaultConfig));
            doSaveCleanup = Boolean.parseBoolean(xmlConfig.getByPath(XML_SAVE_MANAGER_DO_SAVE_CLEANUP, defaultConfig));
            autoCompleteNicknames = Boolean.parseBoolean(xmlConfig.getByPath(XML_AUTOCOMPLETENICKNAMES, defaultConfig));

            screen = emptyIfNull(xmlConfig.getByPath(XML_SCREEN, defaultConfig));
            
            if (saveWindowPosition)
            {
                try
                {
                    String windowPosition = emptyIfNull(
                            xmlConfig.getByPath(XML_WINDOW_POSITION, defaultConfig));
                    String[] values = windowPosition.split(","); //$NON-NLS-1$
                    windowMainPosition = new Rectangle(Integer.parseInt(values[0]),
                            Integer.parseInt(values[1]),
                            Integer.parseInt(values[2]),
                            Integer.parseInt(values[3]));
                    isWindowMainMaximised = Boolean.parseBoolean(xmlConfig.getByPath(XML_WINDOW_MAXIMISED, defaultConfig));
                }
                catch (Exception e)
                {
                    windowMainPosition = null;
                    isWindowMainMaximised = false;
                }
            }

            autoconnectVpn = Boolean.parseBoolean(xmlConfig.getByPath(XML_AUTO_CONNECT_VPN, defaultConfig));
            warnAboutThirdPartyFirewall = Boolean.parseBoolean(xmlConfig.getByPath(XML_THIRD_PARTY_FIREWALL_WARNING, defaultConfig));
            hostUsingVpn = Boolean.parseBoolean(xmlConfig.getByPath(XML_HOST_USING_VPN, defaultConfig));

            readBuddys();
            readIgnoredUsers();

            if (initialConfigCreated)
            {
            	// set language to system-language if available
    	    	// before the following hint load the text
            	if( System.getProperty("user.language").length() > 0 && System.getProperty("user.country").length() > 0 ) {
            		Locale locale = new Locale(System.getProperty("user.language"));
            		setLanguage(locale);
            		I18n.setLanguage(getLanguage());
            	}
            	
                JOptionPane.showMessageDialog(new JFrame(),
                    I18n.getString("Configuration.MSG_NEW_CONFIG")); //$NON-NLS-1$
            }
            else if (xmlConfig.isDefaultValueWasUsed)
            {
                // Die folgenden Anzeige macht momentan keinen Sinn mehr,
                // da nur noch die Werte gespeichert werden, die sich vom default
                // unterscheiden, daher wird man fast jedesmal durch diesen Zweig
                // laufen
//                JOptionPane.showMessageDialog(new JFrame(),
//                        "Die Datei config.xml ist veraltet. Es werden teilweise Standardwerte für die Konfiguration verwendet.");
            }
            try
            {
                gamePathS3 = emptyIfNull(null);
                // Try to load the path to the S3.exe from the registry
                String gamePathFromReg = ReqQuery.s3ExeFilePath;
                if (gamePathFromReg != null)
                {
                    if (Files.exists(new File(gamePathFromReg).toPath()))
                    {
                        if (!S3Support.isGog())
                        {
                            gamePathS3 = gamePathFromReg;
                        } else {
                            //GOG requires us to use the S3_multi.EXE and not the
                            //s3.exe
                            File s3Path = new File(gamePathFromReg).getParentFile();
                            if (s3Path.isDirectory())
                            {
                                String s3MultiExe = new File(s3Path, "S3_multi.EXE").getAbsolutePath();
                                gamePathS3 = s3MultiExe;
                            }
                        }

                        if (!gamePathS3.isEmpty() && !getGameCompability() )
                        {
                            String md5 = S3Support.getMD5Checksum(gamePathS3);
                            if (!S3Support.isSupportedMd5OfS3Exe(md5))
                            {
                                Test.output("Not the expected checksum of the Settlers 3 exe file: " + md5);
                                // load textcolor of actualy used theme as Color-Object
                                String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
                                MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
                                // -> add message
                                String text = I18n.getString("GOGSupport.ERROR_WRONG_S3_MAIN_EXE_CD");
                                if( S3Support.isGog() ) {
                                    text = I18n.getString("GOGSupport.ERROR_WRONG_S3_MAIN_EXE_GOG");
                                }
                                messagewindow.addMessage(
                                    "<html>" + MessageFormat.format(text, hexcolor) + "</html>", 
                                    UIManager.getIcon("OptionPane.errorIcon")
                                );
                                // -> add ok-button
                                messagewindow.addButton(
                                    UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
                                    new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            messagewindow.dispose();
                                        }
                                    }
                                );
                                // show it
                                messagewindow.setVisible(true);
                            }
                        }
                    }
                    else
                    {
                        Test.output("According to Windows the correct path to The Settlers 3 is '" + gamePathFromReg + "' , but we could not find it there!");
                        JOptionPane.showMessageDialog(null,
                                I18n.getString("Configuration.BROKEN_S3_INSTALLATION_1") + gamePathFromReg + I18n.getString("Configuration.BROKEN_S3_INSTALLATION_2"),
                                I18n.getString("SettlersLobby.ERROR"),
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
                if (!gamePathS3.isEmpty() && getGameCompability() && S3Support.makeS3ALobbyExe(gamePathS3, settlersLobby))
                {
                    //s3_alobby.exe was successfully created, so use it
                    File s3Path = new File(gamePathS3).getParentFile();
                    if (s3Path.isDirectory())
                    {
                        File s3AlobbyExe = new File(s3Path, "s3_alobby.exe");
                        if (s3AlobbyExe.exists())
                        {
                            gamePathS3 = s3AlobbyExe.getAbsolutePath();
                        }
                    }
                }
                // log S3-language
                Test.output("S3-language: " + S3Language.getCurrentS3Language(settlersLobby.getS3Directory()));
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        	try
            {
                gamePathS4 = emptyIfNull(null);
                // versuche den Pfad zur S4 exe aus der registry zu lesen
        		File s4dir = ReqQuery.getS4ExeDirectory();
        		if( s4dir != null ) {
                    String s4gamePathFromReg = s4dir.getAbsolutePath();
                    if (s4gamePathFromReg != null)
                    {
                        if (Files.exists(new File(s4gamePathFromReg).toPath()))
                        {
                            if (S4Support.makeS4ALobbyExe(s4gamePathFromReg, settlersLobby))
                            {
                                File s4AlobbyExe = new File(s4gamePathFromReg, "S4_alobby.exe");
                                File s4AlobbyStarterExe = new File(s4gamePathFromReg, "S4_alobby_starter.exe");
                                if (s4AlobbyStarterExe.exists() && s4AlobbyExe.exists())
                                {
                                    gamePathS4 = s4AlobbyExe.getAbsolutePath();
                                }
                            }
                        }
                        else
                        {
                            Test.output("According to Windows the correct path to The Settlers 4 is '" + s4gamePathFromReg + "' , but we could not find it there!");
                            JOptionPane.showMessageDialog(null,
                                    I18n.getString("Configuration.BROKEN_S4_INSTALLATION_1") + s4gamePathFromReg + I18n.getString("Configuration.BROKEN_S4_INSTALLATION_2"),
                                    I18n.getString("SettlersLobby.ERROR"),
                                    JOptionPane.ERROR_MESSAGE);
                        }
                    }
        		}
            }
        	catch (Exception e)
            {
                Test.outputException(e);
            }
        	
        	readFavoriteS3Maps();
            readFavoriteS4Maps();
        	
        } 
        catch (Exception e)
        {
        	
        	// set language to system-language if available
	    	// before the following hint load the text
        	if( !isLanguageSet() && System.getProperty("user.language").length() > 0 && System.getProperty("user.country").length() > 0 ) {
        		Locale locale = new Locale(System.getProperty("user.language"));
        		setLanguage(locale);
        		I18n.setLanguage(getLanguage());
        	}
        	
        	// show hint in window that config.xml could not be loaded
        	// -> click on OK will generate a new value config.xml
        	//    and backup the previous file
        	// -> second button to go into configdir
        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
    		messagewindow.addMessage(I18n.getString("SettlersLobby.ERROR_CONFIGFILE_NOT_AVAILABLE"), UIManager.getIcon("OptionPane.errorIcon"));
    		messagewindow.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	
    			    	// get configFile
    			    	File configFile = getConfigFilePath();
    			    	
    			    	// secure existing file as config.xml.YYYYMMDDHHII for debugging if user requests (unnecessary) help on this event
    			    	Calendar c = Calendar.getInstance();
    			    	int month = c.get(Calendar.MONTH) + 1;
    		            String monthFormated = Integer.toString(month);
    		        	if( month < 10 ) {
    		        		monthFormated = "0" + month;
    		        	}
    			    	File configFileBackup = new File(configFile.getAbsolutePath() + "." + c.get(Calendar.YEAR) + monthFormated + c.get(Calendar.DAY_OF_MONTH) + c.get(Calendar.HOUR_OF_DAY) + c.get(Calendar.MINUTE));
    			    	configFile.renameTo(configFileBackup);
    			    	
    			    	// generate the initial file
    			    	XMLFiles.createInitialFile(configFile.getAbsolutePath());
    			    	
    			    	// show a hint that user should restart aLobby after click on ok
    			    	// TODO: is it possible, that aLobby restart themself?
    			    	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
    		    		messagewindow.addMessage(I18n.getString("SettlersLobby.ERROR_CONFIGFILE_AVAILABLE"), UIManager.getIcon("OptionPane.errorIcon"));
    		    		messagewindow.addButton(
    		    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    		    			new ActionListener() {
    		    			    @Override
    		    			    public void actionPerformed(ActionEvent e) {
    		    			        System.exit(0);
    		    			    }
    		    			}
    		    		);
    		    		messagewindow.setVisible(true);
    			    }
    			}
    		);
    		messagewindow.addButton(
    			I18n.getString("Configuration.OPEN_CONFIG_DIR"), 
    			new ActionListener() {
    				@Override
    				public void actionPerformed(ActionEvent e) {
    	       			SettlersLobby.showConfigDirFolderInExplorer();
    				}
    	    	}
    		);
    		messagewindow.setVisible(true);
        	
    		// Log this exception
            Test.outputException(e);
            return false;
        }
        return true;
    }
    
    private int zeroIfNull(int value)
    {
    	if( value < 0 ) {
    		return 0;
    	}
    	else {
    		return value;
    	}
    }

    /**
     * Return empty string if parameter is null or "null".
     * 
     * @param text	the given parameter
     * @return String
     */
	private String emptyIfNull(String text)
    {
        if (text == null || text.equals("null")) //$NON-NLS-1$
        {
            return ""; //$NON-NLS-1$
        } else
        {
            return text;
        }
    }

	/**
	 * Return "null" if given parameter is empty.
	 * 
	 * @param text
	 * @return
	 */
    private String nullIfEmpty(String text)
    {
        if (text == null || text.isEmpty())
        {
            return "null"; //$NON-NLS-1$
        } else
        {
            return text;
        }
    }

    /**
     * Setzt in dem Knoten mit dem übergebenen Pfad in der config.xml auf den
     * übergebenen Wert. Falls der Wert dem default-Wert entspricht,
     * wird der Wert allerdings gelöscht, damit beim nächsten Lesen
     * auf jeden Fall der default-Wert genommen wird.
     * Sonderfall: Enthält der path einen ArrayIndex, so wird der value
     * grundsätzlich geschrieben.
     *
     * Fängt die NullPointerException ab ohne zu reagieren.
     *
     * @param path Der Pfad zum Knoten, der gesetzt werden soll.
     * @param value Der neue Wert des Knotens.
     */
    void setConfigData(String path, String value)
    {
        boolean bStoreValue = false;
        if (value != null)
        {
            if (path.indexOf('[') >= 0 && path.indexOf(']') >= 0)
            {
                //Listen werden entweder komplett oder gar nicht geschrieben,
                //diese Entscheidung muss außerhalb dieser Methode getroffen
                //werden.
                //damit nicht aus Versehen einzelne Elemente verloren gehen,
                //werden Listenelemente immer geschrieben
                bStoreValue = true;
            }
            else
            {
                try
                {
                    String defaultValue = defaultConfig.getByPath(path);
                    if (!value.equals(defaultValue))
                    {
                        // der Wert unterscheidet sich vom Wert in der default config,
                        // also muss er geschrieben werden
                        bStoreValue = true;
                    }
                }
                catch (IllegalArgumentException e)
                {
                    // der Wert existiert nicht in der default config, also muss
                    // er geschrieben werden
                    bStoreValue = true;
                }
            }
        }
        try
        {
            if (bStoreValue)
            {
                xmlConfig.setByPath(path, value);
            }
            else
            {
                //andernfalls wird er explizit gelöscht, damit auch wirklich
                // beim nächsten Lesen der default wert genommen wird
                xmlConfig.deleteByPath(path);
            }
        } catch (NullPointerException e)
        {
            //die NullPointerException sollte inzwischen nicht mehr auftreten,
            //früher wurde sie geworfen um anzuzeigen, daß der Wert nicht existiert
            Test.outputException(e);
        }
    }

    void setConfigData(String path, Object obj)
    {
        setConfigData(path, obj.toString());
    }
    
    /**
     * liest die Liste der zuletzt gestarteten S3 maps
     * aus der Config Datei.
     */
    private void readFavoriteS3Maps()
    {
        try
        {
            favoriteS3MapsMaxSize = 10;
            String val = xmlConfig.getByPath(XML_FAVORITE_S3MAPS_MAX_SIZE, defaultConfig);
            favoriteS3MapsMaxSize = Integer.parseInt(val);
        }
        catch(Exception e)
        {
        }
        finally
        {
            if (favoriteS3MapsMaxSize < 0 || favoriteS3MapsMaxSize > 1000)
            {
            	favoriteS3MapsMaxSize = 10;
            }
        }

        favoriteMaps.clear();
        try
        {
            String[] maps = xmlConfig.getAllByPath(XML_FAVORITE_S3MAPS_MAP);
            for (int i = 0; i < favoriteS3MapsMaxSize && i < maps.length; i++)
            {
                String map = maps[i];
                if (isValidMap(map))
                {
                    favoriteMaps.add(map);
                }
            }
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
    }
    
    /**
     * liest die Liste der zuletzt gestarteten S4 maps
     * aus der Config Datei.
     */
    private void readFavoriteS4Maps()
    {
        try
        {
            favoriteS4MapsMaxSize = 10;
            String val = xmlConfig.getByPath(XML_FAVORITE_S4MAPS_MAX_SIZE, defaultConfig);
            favoriteS4MapsMaxSize = Integer.parseInt(val);
        }
        catch(Exception e)
        {
        }
        finally
        {
            if (favoriteS4MapsMaxSize < 0 || favoriteS4MapsMaxSize > 1000)
            {
                favoriteS4MapsMaxSize = 10;
            }
        }

        favoriteS4Maps.clear();
        try
        {
            String[] maps = xmlConfig.getAllByPath(XML_FAVORITE_S4MAPS_MAP);

            for (int i = 0; i < favoriteS4MapsMaxSize && i < maps.length; i++)
            {
                String map = maps[i];
                if (isS4MapFileAValidFile(map))
                {
                    favoriteS4Maps.add(map);
                }
            }
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
    }

    /**
     *
     * @param mapFilePath filePath matching "Multi\*.map" or "User\*.map"
     * @return
     */
    public boolean isValidMap(String mapFilePath)
    {
    	if (mapFilePath.startsWith(ALobbyConstants.PATH_RANDOM)) {
    		return true;
    	} else {
	        if (mapFilePath == null || mapFilePath.isEmpty())
	        {
	            return false;
	        }
	        File file = new File(gamePathS3.substring(0, gamePathS3.lastIndexOf("\\") + 1) //$NON-NLS-1$
	                + "Map\\" + mapFilePath); //$NON-NLS-1$
	        if (file.exists() && file.isFile() && mapFilePath.toLowerCase().endsWith(".map")) //$NON-NLS-1$
	        {
	            return true;
	        }
	        return false;
    	}
    }
    
    /**
    * Check if the given S4-map-file is a valid file.
    * It is not checked if the file is a really map-file.
    *
    * @param mapFilePath filePath matching "Multiplayer\*.map"
    * @return
    */
   public boolean isS4MapFileAValidFile(String mapFilePath)
   {
		if (mapFilePath == null || mapFilePath.isEmpty())
		{
		    return false;
		}
		String path = new File(gamePathS4).getParent();
		File file = new File(path.substring(0, path.lastIndexOf("\\") + 1) //$NON-NLS-1$
		        + "Map\\" + mapFilePath); //$NON-NLS-1$
		if (file.exists() && file.isFile() && mapFilePath.toLowerCase().endsWith(".map")) //$NON-NLS-1$
		{
		    return true;
		}
		return false;
   }

    /**
     * schreibt die Liste der zuletzt gestarteten maps
     * in die Config Datei.
     */
    private void writeFavoriteS3Maps()
    {
        //sowohl addFavoriteMap() als auch readFavoriteMaps() beachten
        // favoriteMapsMaxSize, von daher sollte die Liste niemals grösser
        // sein als favoriteMapsMaxSize und muss daher beim schreiben nicht
        // nochmal auf eine gültige Grösse überprüft werden
        setConfigData(XML_FAVORITE_S3MAPS_MAX_SIZE, Integer.toString(favoriteS3MapsMaxSize));
        writeListToConfig(favoriteMaps, XML_FAVORITE_S3MAPS_MAP);
    }
    
    /**
     * schreibt die Liste der zuletzt gestarteten maps
     * in die Config Datei.
     */
    private void writeFavoriteS4Maps()
    {
        //sowohl addFavoriteMap() als auch readFavoriteMaps() beachten
        // favoriteMapsMaxSize, von daher sollte die Liste niemals grösser
        // sein als favoriteMapsMaxSize und muss daher beim schreiben nicht
        // nochmal auf eine gültige Grösse überprüft werden
        setConfigData(XML_FAVORITE_S4MAPS_MAX_SIZE, Integer.toString(favoriteS4MapsMaxSize));
        writeListToConfig(favoriteS4Maps, XML_FAVORITE_S4MAPS_MAP);
    }

    /**
     * fügt die map am Anfang der Liste mit den zuletzt
     * gestarteten maps ein. Die Liste hat maximal die
     * Grösse <code>favoriteS3MapsMaxSize</code>.
     *
     * @param mapPath
     */
    public void addFavoriteS3Map(String mapPath)
    {
        if (mapPath == null || mapPath.isEmpty() || mapPath.indexOf(ALobbyConstants.LABEL_SAVE_GAME) != -1)
        {
            return;
        }
        favoriteMaps.remove(mapPath);
        favoriteMaps.add(0, mapPath);
        while (favoriteMaps.size() > favoriteS3MapsMaxSize)
        {
            favoriteMaps.remove(favoriteMaps.size() - 1);
        }
    }
    
    /**
     * fügt die map am Anfang der Liste mit den zuletzt
     * gestarteten maps ein. Die Liste hat maximal die
     * Grösse <code>favoriteS4MapsMaxSize</code>.
     *
     * @param mapPath
     */
    public void addFavoriteS4Map(String mapPath)
    {
        if (mapPath == null || mapPath.isEmpty() || mapPath.indexOf(ALobbyConstants.LABEL_SAVE_GAME) != -1)
        {
            return;
        }
        favoriteS4Maps.remove(mapPath);
        favoriteS4Maps.add(0, mapPath);
        while (favoriteS4Maps.size() > favoriteS4MapsMaxSize)
        {
        	favoriteS4Maps.remove(favoriteS4Maps.size() - 1);
        }
    }

    private void readBuddys()
    {
    	try {
	    	String[] values = xmlConfig.getAllNodeNamesByPath("userList/*"); //$NON-NLS-1$
	    	for (String value : values) {
	    		buddys.put(value, readListFromConfig("userList/" + value + "/buddys/user")); //$NON-NLS-1$ //$NON-NLS-2$
	    	}
    	} catch (NullPointerException e) {
    		//Ignorieren - es sind keine in der Config gespeichert.
    	}
    }

    private void writeBuddys()
    {
        for (String key : buddys.keySet()) {
        	writeListToConfig(buddys.get(key), "userList/" + key + "/buddys/user"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }

    private void readIgnoredUsers()
    {
    	try {
		    String[] values = xmlConfig.getAllNodeNamesByPath("userList/*"); //$NON-NLS-1$
	    	for (String value : values) {
	    		ignoredUsers.put(value, readListFromConfig("userList/" + value + "/ignoredUsers/user")); //$NON-NLS-1$ //$NON-NLS-2$
	    	}
    	} catch (NullPointerException e) {
    		//Ignorieren - es sind keine in der Config gespeichert.
    	}
    }

    private void writeIgnoredUsers()
    {
        for (String key : ignoredUsers.keySet()) {
        	writeListToConfig(ignoredUsers.get(key), "userList/" + key + "/ignoredUsers/user"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }


    private List<String> readListFromConfig(String xmlPath)
    {
        return readListFromConfig(xmlPath, false);
    }

    /**
     * Liest eine Liste aus der config.xml.
     * @param xmlPath
     * @param additionallySearchInDefaultConfig true, falls auch in der defaultConfig nach der
     * Liste gesucht werden soll, wenn sie in der config.xml nicht existiert.
     * @return
     */
    private List<String> readListFromConfig(String xmlPath, boolean additionallySearchInDefaultConfig)
    {
        List<String> list = new ArrayList<String>() {
			@Override
		    public boolean contains(Object obj) {
		        String object = (String)obj;
		        for (String string : this) {
		            if (object.equalsIgnoreCase(string)) {
		              return true;
		            }
		        }
		        return false;
		    }
		};
        try
        {
            String[] values = xmlConfig.getAllByPath(xmlPath);
            //Listen werden entweder komplett oder gar nicht gespeichert
            //Falls sie nicht vorhanden ist, schaue ob sie in der
            //default config existiert
            if (additionallySearchInDefaultConfig && (values == null || values.length == 0))
            {
                values = defaultConfig.getAllByPath(xmlPath);
            }

            for (int i = 0; i < values.length; i++)
            {
                String value = values[i];
                if (value != null && !value.isEmpty())
                {
                    list.add(value);
                }
            }
        }
        catch (Exception e)
        {
        	Test.output(xmlPath);
            Test.outputException(e);
        }
        return list;
    }

    /**
     * Liest eine Liste aus der Default config.
     * @param xmlPath
     * @return Liste der gefundenen Einträge, kann leer sein.
     */
    private List<String> readListFromDefaultConfig(String xmlPath)
    {
        List<String> list = new ArrayList<String>();
        try
        {
            String[] values = defaultConfig.getAllByPath(xmlPath);

            for (String value : values)
            {
                if (value != null && !value.isEmpty())
                {
                    list.add(value);
                }
            }
        }
        catch (Exception e)
        {
            Test.output(xmlPath);
            Test.outputException(e);
        }
        return list;
    }

    /**
     * Schreibt eine Liste in die config.xml. Falls additionallySearchInDefaultConfig
     * true ist, wird die Liste nur geschrieben, wenn sie nicht der Default-Liste
     * in der DefaultConfig entspricht (in diesem Fall wird sie sogar aus der config.xml
     * gelöscht).
     *
     * @param list
     * @param xmlPath
     * @param additionallySearchInDefaultConfig
     */
    private void writeListToConfig(List<String> list, String xmlPath, boolean additionallySearchInDefaultConfig)
    {
        if (additionallySearchInDefaultConfig)
        {
            String[] defaultValues = defaultConfig.getAllByPath(xmlPath);
            String[] currentValues = list.toArray(new String[0]);

            if (Arrays.equals(defaultValues, currentValues))
            {
                //wenn die Liste der defaultListe entspricht,
                //wird writeListToConfig mit einer leeren Liste aufgerufen
                //dieses löscht effektiv diese Liste in der lokalen config.xml
                writeListToConfig(new ArrayList<String>(0), xmlPath);
                return;
            }
        }
        //ansonsten schreibe die Liste komplett in die lokale config.xml
        writeListToConfig(list, xmlPath);

    }

    /**
     * Schreibt eine Liste in die config.xml. Falls die Liste zuvor
     * größer war, werden eventuell vorhandene überzählige Elemente in der
     * config.xml gelöscht.
     *
     * @param list
     * @param xmlPath
     */
    private void writeListToConfig(List<String> list, String xmlPath)
    {

        for (int i = 0; i < list.size() ; i++)
        {
            //XPath index startet bei 1, nicht bei 0, daher i+1
            String path = xmlPath + "[" + (i+1) + "]"; //$NON-NLS-1$ //$NON-NLS-2$
            try
            {
                setConfigData(path, list.get(i));
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }

        //lösche alle restlichen vorkommenden Werte (falls die Liste früher
        // mal größer war)
        //XPath index startet bei 1, nicht bei 0, daher +1
        int deleteIndex = list.size();
        String path = xmlPath + "[" + (deleteIndex+1) + "]"; //$NON-NLS-1$ //$NON-NLS-2$
        while (xmlConfig.deleteByPath(path))
        {
            ;
        }
    }

    boolean loadDefaultConfig()
    {
        InputStream inputStream = getClass().getResourceAsStream(
                ALobbyConstants.PACKAGE_PATH + "/xmlcommunication/config.xml"); //$NON-NLS-1$
        try
        {
            this.defaultConfig = XMLFiles.getXMLCommunicatorOfStream(inputStream);
            if (inputStream != null) {
            	try {
					inputStream.close();
				} catch (IOException e) {
					Test.outputException(e);
				}
            }
            return true;
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null,
                            MessageFormat.format(I18n.getString("Configuration.ERROR_LOAD_DEFAULT_CONFIG"), //$NON-NLS-1$
                                                 SettlersLobby.formatExceptionText(e)),
                            I18n.getString("SettlersLobby.ERROR"), //$NON-NLS-1$
                            JOptionPane.ERROR_MESSAGE);
            Test.outputException(e);
            if (inputStream != null) {
            	try {
					inputStream.close();
				} catch (IOException e2) {
					Test.outputException(e2);
				}
            }
            return false;
        }
    }

    void adjustToWineDefaultSettings(String wineVersion)
    {
        if (defaultConfig != null)
        {
            if (wineVersion != null)
            {
                //the new gamestarter checks do not work properly with Wine, deactivate them in the default options
                defaultConfig.setByPath(XML_GAME_STARTER_ACTIVATE_FOREGROUND_WINDOW_CHECK, "false");
                defaultConfig.setByPath(XML_GAME_STARTER_ACTIVATE_SCREEN_SIZE_CHECK, "false");
                //The Teamspeak panel does not work properly on newer Java 7 releases
                //and does not work at all on Java 8 (both only applies when running on Wine)
                // Edit: -Dprism.order=j2d workarounds the problem
                defaultConfig.setByPath(XML_HIDE_TEAMSPEAK_WINDOW, "false");
            }
        }
    }

    boolean saveCurrentConfiguration()
    {
        setConfigData(XML_USER_NAME, nullIfEmpty(userName));
        if (isStorePassword)
        {
            setConfigData(XML_PASSWORD, nullIfEmpty(encryptedPassword));
        } else {
        	setConfigData(XML_PASSWORD, nullIfEmpty("")); //$NON-NLS-1$
        }

        setConfigData(XML_URL_LOBBY_IS_NEW_VERSION, urlIsLobbyNewVersion);
        setConfigData(XML_URL_TIME, urlTime);
        setConfigData(XML_GAME_PATH, nullIfEmpty(null));
        setConfigData(XML_GAME_PATHS4, nullIfEmpty(null));
        setConfigData(XML_GAME_COMPATIBILITY, Boolean.toString(gameCompability));
		setConfigData(XML_URL_MAP_DOWNLOAD, mapDownloadPath);
        setConfigData(XML_URL_AUTOMATIC_MAP_DOWNLOAD, automaticMapDownloadPath);
		setConfigData(XML_URL_BUDDY_IGNORE_URL, nullIfEmpty(buddyIgnoreUrl));
		setConfigData(XML_URL_LEAGUE, nullIfEmpty(leagueUrl));
		setConfigData(XML_URL_LEAGUE_INTERFACE, nullIfEmpty(leagueUrlInterface));
		setConfigData(XML_URL_LEAGUE_RANKING_LISTS, nullIfEmpty(leagueUrlRankingLists));
		setConfigData(XML_URL_TOURNAMENT_PLAYER_LIST, nullIfEmpty(tournamentPlayerList));
		setConfigData(XML_URL_TOURNAMENT_USER_ALLOWED, nullIfEmpty(tournamentUserAllowed));
		setConfigData(XML_URL_TETRIS_HIGHSCORE, nullIfEmpty(tetrisHighscoreURL));
		setConfigData(XML_HIDE_LEAGUE_INTERFACE, hideLeagueInterface);
		setConfigData(XML_HIDE_TEAMSPEAK_WINDOW, hideTeamspeakWindow);
		setConfigData(XML_URL_HILFE, helpPath);
		setConfigData(XML_ENCRYPTION, doEncryption);
		setConfigData(XML_TRUST_ONLY_ENCRYPTED_MESSAGES, trustOnlyEncryptedMessages);
		setConfigData(XML_HIDE_S4_INTERFACE, hideS4Interface);
		setConfigData(XML_S3VERSIONNUMBER, Integer.toString(S3versionnumber));
		setConfigData(XML_S3CERESOLUTION, s3ceResolution);

		if (language != null) {
			setConfigData(XML_LANGUAGE, nullIfEmpty(language.toString()));
		}
		setConfigData(XML_SERVER_URL, nullIfEmpty(serverURL));
		setConfigData(XML_CHANNEL, nullIfEmpty(channel));
		setConfigData(XML_TIMEOUT, nullIfEmpty(timeOut));
		setConfigData(XML_FONT_SIZE, Integer.toString(fontSize));
		setConfigData(XML_ACTIVATE_AWAY_ON_IDLE, Boolean.toString(activateAwayOnIdle));
		setConfigData(XML_ACTIVATE_AWAY_ON_GAME_START, Boolean.toString(activateAwayOnGameStart));
		setConfigData(XML_AWAY_TIME, Integer.toString(awayTime/1000));
		setConfigData(XML_LOCAL_MODE, Boolean.toString(isLocalMode));
		setConfigData(XML_AUTO_LOAD_AND_SAVE_BUDDYS, Boolean.toString(isAutoLoadAndSaveBuddys));
		setConfigData(XML_DEFAULT_AWAY_MSG, nullIfEmpty(defaultAwayMsg));
		setConfigData(XML_SORT_BUDDYS, Boolean.toString(isSortBuddys));
		setConfigData(XML_SORT_AWAY, Boolean.toString(isSortAway));
		setConfigData(XML_SORT_INGAME, Boolean.toString(isSortInGame));

		setConfigData(XML_GAME_STARTER_MAP_SELECT_DELAY_AFTER_CLICK,
		        Integer.toString(GameStarterVanilla.mapSelectDelayAfterClick));
        setConfigData(XML_GAME_STARTER_MAP_SELECT_FAST,
                Boolean.toString(GameStarterVanilla.mapSelectFastVariant));
        setConfigData(XML_GAME_STARTER_COLOR_CHECK_VARIATION,
                Integer.toString(PixelColorChecker.colorVariation));
        setConfigData(XML_GAME_STARTER_USE_SEND_INPUT_NATIVE,
                Boolean.toString(GameStarterVanilla.useSendInputNative));
        setConfigData(XML_GAME_STARTER_ACTIVATE_SCREEN_SIZE_CHECK,
                Boolean.toString(activateScreenSizeCheck));
        setConfigData(XML_GAME_STARTER_ACTIVATE_FOREGROUND_WINDOW_CHECK,
                        Boolean.toString(activateForegroundWindowCheck));
        setConfigData(XML_GAME_STARTER_ACTIVATE_S3STATS_SCREEN_CAPTURE,
                Boolean.toString(activateS3StatsScreenCapture));
        setConfigData(XML_GAME_STARTER_GAME_DATETIME_FORMAT, gameDateTimeFormat);

        setConfigData(XML_IN_GAME_AUTO_SAVE, Boolean.toString(autoSave));
        setConfigData(XML_IN_GAME_AUTO_SAVE_TIMER, Integer.toString(autoSaveTimer));
        setConfigData(XML_IN_GAME_AUTO_SAVE_SOUND_FILE, nullIfEmpty(autoSaveSoundFile));
        setConfigData(XML_IN_GAME_BLINK_SOUND_FILE, nullIfEmpty(blinkSoundFile));
        setConfigData(XML_IN_GAME_STOP_STORING_TOOLS, Boolean.toString(stopStoringTools));
        setConfigData(XML_IN_GAME_DISABLE_AUTOSAVE_SOUND, Boolean.toString(disableAutosaveSound));

        setConfigData(XML_ACTIVEMAINTAB, zeroIfNull(activemainTab));
        
        setConfigData(XML_TETRISHIGHSCORE, tetrisHighscore);
        setConfigData(XML_OPENNEWGAME_TAB, zeroIfNull(newGameTab));
        setConfigData(XML_OPENNEWS3GAME_TAB, zeroIfNull(newS3GameTab));
        setConfigData(XML_OPENNEWS4GAME_TAB, zeroIfNull(newS4GameTab));
        setConfigData(XML_OPENNEWS3GAME_GOODS, zeroIfNull(newS3GameGoods));
        setConfigData(XML_OPENNEWS4GAME_GOODS, zeroIfNull(newS4GameGoods));
        setConfigData(XML_OPENNEWS3GAME_RANDOMPOSITION, Boolean.toString(newS3GameRandomPosition));
        setConfigData(XML_OPENNEWS3GAME_MINICHAT, Boolean.toString(newS3GameMinichat));
        setConfigData(XML_OPENNEWS3GAME_WIMO, Boolean.toString(newS3GameWimo));
        setConfigData(XML_OPENNEWS4GAME_WIMO, Boolean.toString(newS4GameWimo));
        setConfigData(XML_OPENNEWS3GAME_LEAGUE, Boolean.toString(newS3GameLeague));
        setConfigData(XML_OPENNEWS4GAME_TROJANS, Boolean.toString(newS4GameTrojans));
        setConfigData(XML_OPENNEWS4GAME_COOP, Boolean.toString(newS4GameCoop));
        setConfigData(XML_OPENNEWS4GAME_PLAYERS, Integer.toString(newS4GamePlayers));
        setConfigData(XML_OPENNEWS4GAME_TEAMS, Integer.toString(newS4GameTeams));
        
        setConfigData(XML_OPENNEWS4GAME_RANDOMMAPSIZE, Integer.toString(newS4GameRandomMapSize));
        setConfigData(XML_OPENNEWS4GAME_RANDOMLANDMASS, Integer.toString(newS4GameRandomLandmass));
        setConfigData(XML_OPENNEWS4GAME_RANDOMAXES, Integer.toString(newS4GameRandomAxes));
        setConfigData(XML_OPENNEWS4GAME_RANDOMRESOURCES, Integer.toString(newS4GameRandomResources));
        setConfigData(XML_OPENNEWS4GAME_RANDOMCODE, nullIfEmpty(newS4GameRandomCode));
        setConfigData(XML_S4_DISABLE_BETA_HINT, Boolean.toString(preventS4BetaHint));
        
        setConfigData(XML_SAVE_WINDOW_POSITION, Boolean.toString(saveWindowPosition));
        if (saveWindowPosition && windowMainPosition != null)
        {
            String value = windowMainPosition.x + "," + windowMainPosition.y + "," + //$NON-NLS-1$ //$NON-NLS-2$
                windowMainPosition.width + "," + windowMainPosition.height; //$NON-NLS-1$
            setConfigData(XML_WINDOW_POSITION, value);
            setConfigData(XML_WINDOW_MAXIMISED, isWindowMainMaximised);
        }
        else
        {
            //setze die Windows Position auf den default-Wert, effektiv wird dadurch
            //der Eintrag aus der config.xml gelöscht
            setConfigData(XML_WINDOW_POSITION, defaultConfig.getByPath(XML_WINDOW_POSITION));
            setConfigData(XML_WINDOW_MAXIMISED, defaultConfig.getByPath(XML_WINDOW_MAXIMISED));
        }

        setConfigData(XML_AUTO_CONNECT_VPN, Boolean.toString(autoconnectVpn));
        setConfigData(XML_THIRD_PARTY_FIREWALL_WARNING, Boolean.toString(warnAboutThirdPartyFirewall));
        setConfigData(XML_HOST_USING_VPN, Boolean.toString(hostUsingVpn));

        setConfigData(XML_SHOW_TIME, Boolean.toString(showTime));
        setConfigData(XML_USE_DEL_KEY, Boolean.toString(useDelKey));
        setConfigData(XML_THEME, theme.name());
        Calendar c = new GregorianCalendar();
        setConfigData(XML_PREVENT_AUTOMATIC_CHRISTMAS_THEMES + "/year" + c.get(Calendar.YEAR), Boolean.toString(preventAutomaticChristmasTheme));
        setConfigData(XML_PREVENT_CHRISTMAS_HINTS + "/year" + c.get(Calendar.YEAR), Boolean.toString(preventChristmasHint));
        setConfigData(XML_PREVENT_AUTOMATIC_HALLOWEEN_THEMES + "/year" + c.get(Calendar.YEAR), Boolean.toString(preventAutomaticHalloweenTheme));
        setConfigData(XML_PREVENT_HALLOWEEN_HINTS + "/year" + c.get(Calendar.YEAR), Boolean.toString(preventHalloweenHint));
        setConfigData(XML_PREVENT_AUTOMATIC_EASTER_THEMES + "/year" + c.get(Calendar.YEAR), Boolean.toString(preventAutomaticEasterTheme));
        setConfigData(XML_PREVENT_EASTER_HINTS + "/year" + c.get(Calendar.YEAR), Boolean.toString(preventEasterHint));
        setConfigData(XML_PREVENT_APRILSFOOL + "/year" + c.get(Calendar.YEAR), Boolean.toString(preventAprilsFool));
        int month = c.get(Calendar.MONTH) + 1;
        String monthFormated = Integer.toString(month);
    	if( month < 10 ) {
    		monthFormated = "0" + month;
    	}
        setConfigData(XML_BETA_HINT + "/day" + c.get(Calendar.YEAR) + monthFormated + c.get(Calendar.DAY_OF_MONTH), Boolean.toString(preventDailyBetaHint));
        setConfigData(XML_S4_BETA_HINT + "/week" + c.get(Calendar.WEEK_OF_YEAR), Boolean.toString(preventWeeklyS4BetaHint));
        setConfigData(XML_CHAT_LOGGING, Boolean.toString(chatLogging));
        setConfigData(XML_FLASHING, Boolean.toString(flashingWhenHighlighted));
        // save config for activation of audiofile when highlighted
        setConfigData(XML_FLASHING_PLAY_SOUND, Boolean.toString(playSoundWhenHighlighted));
        setConfigData(XML_NOTIFY_IF_BUDDY_COMES_ONLINE, Boolean.toString(isNotifyIfBuddyComesOnline));
        setConfigData(XML_SEARCH_AND_INSTALL_UPDATE_AUTOMATICALLY,
        		Boolean.toString(searchAndInstallUpdateAutomatically));

        writeListToConfig(ipViaUrl, XML_IP_VIA_URL, true);
        writeListToConfig(highlightingWords, XML_HIGHLIGHTING_WORDS_WORD);
        writeFavoriteS3Maps();
        writeFavoriteS4Maps();
        writeBuddys();
        writeIgnoredUsers();

        setConfigData(XML_INGAME_WHISPER, Boolean.toString(showWhisperIngame));
        setConfigData(XML_INGAME_MESSAGES, Boolean.toString(showMessagesIngame));
        setConfigData(XML_TABBED_CHAT_PANE, Boolean.toString(isTabbedChatPane));
        
        setConfigData(XML_SAVE_MANAGER_START_SAVEMANAGER, Boolean.toString(startSavemanager));
        setConfigData(XML_SAVE_MANAGER_START_WATCHDOG, Boolean.toString(startWatchdog));
        setConfigData(XML_SAVE_MANAGER_AUTO_PLACE, Boolean.toString(autoPlace));
        setConfigData(XML_SAVE_MANAGER_BACKUP_COUNT, Integer.toString(backupCount));
        setConfigData(XML_SAVE_MANAGER_CLEANUP_TIME, Integer.toString(cleanupTime));
        setConfigData(XML_SAVE_MANAGER_DO_CLEANUP, Boolean.toString(doCleanup));
        setConfigData(XML_SAVE_MANAGER_SAVE_CLEANUP_TIME, Integer.toString(saveCleanupTime));
        setConfigData(XML_SAVE_MANAGER_DO_SAVE_CLEANUP, Boolean.toString(doSaveCleanup));
        setConfigData(XML_AUTOCOMPLETENICKNAMES, Boolean.toString(autoCompleteNicknames));

        setConfigData(XML_SCREEN, nullIfEmpty(screen));

        try
        {
            xmlConfig.saveInXMLFile(getConfigFilePath().getAbsolutePath());
        } catch (Exception e)
        {
            Test.outputException(e);
            return false;
        }
        return true;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
        if (buddys.get(userName) == null) {
        	buddys.put(userName, new ArrayList<String>());
        }
        if (ignoredUsers.get(userName) == null) {
        	ignoredUsers.put(userName, new ArrayList<String>());
        }
        notifyConfigurationListeners();
    }
    public String getPassword()
    {
        String password = encryptedPassword;
        if (password != null && password.length() > 0)
        {
            password = CryptoSupport.decodePw(password);
        }
        return password;
    }
    public void setPassword(String password)
    {
        //always store the password encypted
        if (password != null && password.length() > 0)
        {
            password = CryptoSupport.encodePw(password);
        }
        this.encryptedPassword = password;
        notifyConfigurationListeners();
    }

    public String getGamePathS3()
    {
        // Workaround for an unexpected case, where the path is empty...
        if (gamePathS3.trim().isEmpty()) {
            return null;
        }
        File s3Path = new File(gamePathS3).getParentFile();
        if (getGameCompability())
        {
            File s3AlobbyExe = new File(s3Path, "s3_alobby.exe");
            gamePathS3 = s3AlobbyExe.getAbsolutePath();
        } else {
            if (S3Support.isGog())
            {
                File s3MultiExe = new File(s3Path, "S3_multi.EXE");
                gamePathS3 = s3MultiExe.getAbsolutePath();
            } else {
                File s3Exe = new File(s3Path, "s3.exe");
                gamePathS3 = s3Exe.getAbsolutePath();
            }
        }
        return gamePathS3;
    }

    public String getGamePathS4()
    {
        return gamePathS4;
    }
    public void setGamePathS4(String gamePath)
    {
        this.gamePathS4 = gamePath;
        notifyConfigurationListeners();
    }
    
    public boolean getGameCompability()
    {
        return gameCompability;
    }
    public void setGameCompability(boolean gameCompability)
    {
        this.gameCompability = gameCompability;
        notifyConfigurationListeners();
    }
    public String getServerURL()
    {
        return serverURL;
    }
    public void setServerURL(String serverURL)
    {
        this.serverURL = serverURL;
        notifyConfigurationListeners();
    }
    public String getTimeOut()
    {
        return timeOut;
    }
    public int getTimeOutAsInt()
    {
    	// TODO possible problem if config.xml does contain an empty timeout value?
        int result = 30;
        Test.output("timeOut: " + timeOut);
        try
        {
            result = Integer.parseInt(timeOut);
        }
        catch (Exception e)
        {
        	Test.outputException(e);
        }
        return Math.max(result, 1);
    }
    public void setTimeOut(String timeOut)
    {
        this.timeOut = timeOut;
        notifyConfigurationListeners();
    }
    public String getChannel()
    {
        return channel;
    }
    public void setChannel(String channel)
    {
        this.channel = channel;
        notifyConfigurationListeners();
    }
    public String getMapDownloadPath()
    {
        return mapDownloadPath;
    }
    public void setMapDownloadPath(String mapDownloadPath)
    {
        this.mapDownloadPath = mapDownloadPath;
        notifyConfigurationListeners();
    }
    public String getHelpPath()
    {
        return helpPath;
    }
    public void setHelpPath(String helpPath)
    {
        this.helpPath = helpPath;
        notifyConfigurationListeners();
    }
    public boolean isDoEncryption()
    {
        return doEncryption;
    }
    public void setDoEncryption(boolean doEncryption)
    {
        this.doEncryption = doEncryption;
        notifyConfigurationListeners();
    }
    public int getAwayTime()
    {
        return awayTime;
    }
    public void setAwayTime(int awayTime)
    {
        this.awayTime = awayTime;
        notifyConfigurationListeners();
    }
    public boolean isActivateAwayOnIdle()
    {
        return activateAwayOnIdle;
    }
    public void setActivateAwayOnIdle(boolean activateAwayOnIdle)
    {
        this.activateAwayOnIdle = activateAwayOnIdle;
        notifyConfigurationListeners();
    }
    public boolean isActivateAwayOnGameStart()
    {
        return activateAwayOnGameStart;
    }
    public void setActivateAwayOnGameStart(boolean activateAwayOnGameStart)
    {
        this.activateAwayOnGameStart = activateAwayOnGameStart;
        notifyConfigurationListeners();
    }
    public boolean isLocalMode()
    {
        return isLocalMode;
    }
    public void setLocalMode(boolean isLocalMode)
    {
        this.isLocalMode = isLocalMode;
        notifyConfigurationListeners();
    }
    public boolean isAutoconnectVpn()
    {
        return autoconnectVpn;
    }
    public void setAutoconnectVpn(boolean autoconnectVpn)
    {
        this.autoconnectVpn = autoconnectVpn;
        notifyConfigurationListeners();
    }
    public boolean isWarnAboutThirdPartyFirewall()
    {
        return warnAboutThirdPartyFirewall;
    }
    public void setWarnAboutThirdPartyFirewall(boolean warnAboutThirdPartyFirewall)
    {
        this.warnAboutThirdPartyFirewall = warnAboutThirdPartyFirewall;
        notifyConfigurationListeners();
    }
    public boolean isHostUsingVpn()
    {
        return hostUsingVpn;
    }
    public void setHostUsingVpn(boolean hostUsingVpn)
    {
        this.hostUsingVpn = hostUsingVpn;
        notifyConfigurationListeners();
    }
    public String getDefaultAwayMsg()
    {
        return defaultAwayMsg;
    }
    public void setDefaultAwayMsg(String defaultAwayMsg)
    {
        this.defaultAwayMsg = defaultAwayMsg;
        notifyConfigurationListeners();
    }
    public List<String> getFavoriteS3Maps()
    {
        return favoriteMaps;
    }
    public List<String> getFavoriteS4Maps()
    {
        return favoriteS4Maps;
    }
    public void setFavoriteS3Maps(List<String> favoriteMaps)
    {
        this.favoriteMaps = favoriteMaps;
        notifyConfigurationListeners();
    }
    public void setFavoriteS4Maps(List<String> favoriteMaps)
    {
        this.favoriteS4Maps = favoriteMaps;
        notifyConfigurationListeners();
    }
    public int getFavoriteS3MapsMaxSize()
    {
        return favoriteS3MapsMaxSize;
    }
    public void setFavoriteS3MapsMaxSize(int favoriteS3MapsMaxSize)
    {
        this.favoriteS3MapsMaxSize = favoriteS3MapsMaxSize;
        notifyConfigurationListeners();
    }
    public int getFavoriteS4MapsMaxSize()
    {
        return favoriteS4MapsMaxSize;
    }
    public void setFavoriteS4MapsMaxSize(int favoriteS4MapsMaxSize)
    {
        this.favoriteS4MapsMaxSize = favoriteS4MapsMaxSize;
        notifyConfigurationListeners();
    }
    public boolean isSaveWindowPosition()
    {
        return saveWindowPosition;
    }
    public void setSaveWindowPosition(boolean saveWindowPosition)
    {
        this.saveWindowPosition = saveWindowPosition;
        notifyConfigurationListeners();
    }
    
    /**
     * Return the secured position and size of the main aLobby-Window.
     * 
     * @return Rectangle
     */
    public Rectangle getWindowMainPosition()
    {
        return windowMainPosition;
    }
    
    /**
     * Set the position and size of the main aLobby-Window.
     * 
     * @param windowMainPosition
     */
    public void setWindowMainPosition(Rectangle windowMainPosition)
    {
        this.windowMainPosition = windowMainPosition;
        notifyConfigurationListeners();
    }
    
    /**
     * Return the default aLobby-config.
     * 
     * @return
     */
    public XMLCommunicator getDefaultConfig()
    {
        return defaultConfig;
    }
    public void setDefaultConfig(XMLCommunicator defaultConfig)
    {
        this.defaultConfig = defaultConfig;
        notifyConfigurationListeners();
    }

    public XMLCommunicator getXmlConfig()
    {
        return xmlConfig;
    }

    public void setXmlConfig(XMLCommunicator xmlConfig)
    {
        this.xmlConfig = xmlConfig;
        notifyConfigurationListeners();
    }

    public String getAutoSaveSoundFile()
    {
        return autoSaveSoundFile;
    }
    
    /**
     * Get the screen where the windows will be displayed.
     * 
     * @return
     */
    public String getScreen()
    {
        return screen;
    }

    /**
     * Set the screen where the windows will be displayed.
     * 
     * @param screen
     */
    public void setScreen(String screen)
    {
        this.screen = screen;
        notifyConfigurationListeners();
    }
    
    // get the settings for the blinksound-file
    public String getBlinkSoundFile()
    {
        return blinkSoundFile;
    }

    public void setAutoSaveSoundFile(String autoSaveSoundFile)
    {
        this.autoSaveSoundFile = autoSaveSoundFile;
        notifyConfigurationListeners();
    }
    
    // set blinksound in settings
    public void setBlinkSoundFile(String blinkSoundFile)
    {
        this.blinkSoundFile = blinkSoundFile;
        notifyConfigurationListeners();
    }

    public void setFontSize(int size) {
    	fontSize = size;
    }

    public int getFontSize() {
    	return fontSize;
    }

    public boolean isDisabledAutosaveSound()
    {
        return disableAutosaveSound;
    }
    
    public void setDisabledAutosaveSound(boolean disableAutosaveSound)
    {
        this.disableAutosaveSound = disableAutosaveSound;
    }
    
    public boolean isStopStoringTools()
    {
        return stopStoringTools;
    }

    public void setStopStoringTools(boolean stopStoringTools)
    {
        this.stopStoringTools = stopStoringTools;
    }

    public boolean isAutoSave()
    {
        return autoSave;
    }

    public void setAutoSave(boolean autoSave)
    {
        this.autoSave = autoSave;
        notifyConfigurationListeners();
    }

    public int getAutoSaveTimer()
    {
        return autoSaveTimer;
    }

    public void setAutoSaveTimer(int autoSaveTimer)
    {
        this.autoSaveTimer = autoSaveTimer;
        notifyConfigurationListeners();
    }

    public boolean isTrustOnlyEncryptedMessages()
    {
        return trustOnlyEncryptedMessages;
    }

    public List<String> getDefaultServerURLs()
    {
        return defaultServerURLs;
    }

    public List<String> getDefaultChannels()
    {
        return defaultChannels;
    }

    public boolean isShowTime()
    {
    	return showTime;
    }

    public void setShowTime(boolean showTime)
    {
        this.showTime = showTime;
        notifyConfigurationListeners();
    }

    public boolean isBbTheme()
    {
    	return theme == ALobbyThemes.BB;
    }
    
    public boolean isClassicV1Theme()
    {
        return theme == ALobbyThemes.classicV1;
    }

    public boolean isClassicV2Theme()
    {
        return theme == ALobbyThemes.classicV2; 
    }

    public boolean isClassicTheme()
    {
        return isClassicV1Theme() || isClassicV2Theme(); 
    }
    
    // return true if the Christmas-Theme is the actual theme
    public boolean isChristmasTheme() {
    	return theme == ALobbyThemes.CHRISTMAS;
    }
    
    // return true if the Halloween-Theme is the actual theme
    public boolean isHalloweenTheme() {
    	return theme == ALobbyThemes.HALLOWEEN;
    }
    
    // return true if the Easter-Theme is the actual theme
    public boolean isEasterTheme() {
    	return theme == ALobbyThemes.EASTER;
    }

    public void setTheme(ALobbyThemes theme)
    {
        this.theme = theme;
        notifyConfigurationListeners();
    }
    
    /**
     * Set marker to prevent return to christmas theme
     * during christmas time
     * 
     * @author Zwirni
     */
    public void setPreventAutomaticChristmasTheme(boolean prevent) {
    	this.preventAutomaticChristmasTheme = prevent;
    	notifyConfigurationListeners();
    }
    
    /**
     * Get marker to prevent return to christmas theme
     * during christmas time
     *      
     * @author Zwirni
     */
    public boolean getPreventAutomaticChristmasTheme() {
    	return this.preventAutomaticChristmasTheme;
    }
    
    /**
     * Set marker to prevent the Christmas-time-hint-window for the actual year
     * 
     * @author Zwirni
     */
    public void setPreventChristmasHint(boolean prevent) {
    	this.preventChristmasHint = prevent;
    	notifyConfigurationListeners();
    }
    
    /**
     * Get marker to prevent return to christmas theme
     * during Christmas time
     *      
     * @author Zwirni
     */
    public boolean getPreventChristmasHint() {
    	return this.preventChristmasHint;
    }
    
	/**
	 * Set marker to prevent return to halloween theme
	 * during halloween time
	 * 
	 * @author Zwirni
	 */
	public void setPreventAutomaticHalloweenTheme(boolean prevent) {
		this.preventAutomaticHalloweenTheme = prevent;
		notifyConfigurationListeners();
	}
	
	/**
	 * Get marker to prevent return to christmas theme
	 * during Halloween time
	 *      
	 * @author Zwirni
	 */
	public boolean getPreventAutomaticHalloweenTheme() {
		return this.preventAutomaticHalloweenTheme;
	}
	
	/**
	 * Set marker to prevent the Halloween-time-hint-window for the actual year
	 * 
	 * @author Zwirni
	 */
	public void setPreventHalloweenHint(boolean prevent) {
		this.preventHalloweenHint = prevent; 
		notifyConfigurationListeners();
	}
	
	/**
	 * Get marker to prevent return to Halloween theme
	 * during Halloween time
	 *      
	 * @author Zwirni
	 */
	public boolean getPreventHalloweenHint() {
		return this.preventHalloweenHint;
	}
	
	/**
     * Set marker to prevent return to easter theme
     * during easter time
     * 
     * @author Zwirni
     */
    public void setPreventAutomaticEasterTheme(boolean prevent) {
    	this.preventAutomaticEasterTheme = prevent;
    	notifyConfigurationListeners();
    }
    
    /**
     * Get marker to prevent return to easter theme
     * during easter time
     *      
     * @author Zwirni
     */
    public boolean getPreventAutomaticEasterTheme() {
    	return this.preventAutomaticEasterTheme;
    }
    
    /**
     * Set marker to prevent the Easter-time-hint-window for the actual year
     * 
     * @author Zwirni
     */
    public void setPreventEasterHint(boolean prevent) {
    	this.preventEasterHint = prevent;
    	notifyConfigurationListeners();
    }
    
    /**
     * Set marker to prevent the hint about using a aLobby beta-version for the actual day
     * 
     * @author Zwirni
     */
    public void setPreventDailyBetaHint(boolean prevent) {
    	this.preventDailyBetaHint = prevent;
    	notifyConfigurationListeners();
    }
    
    /**
     * Get marker to prevent daily beta hint
     *      
     * @author Zwirni
     */
    public boolean getPreventDailyBetaHint() {
    	return this.preventDailyBetaHint;
    }
    
    /**
     * Set marker to prevent the hint about using s4 in beta-phase in the aLobby for the actual week
     * 
     * @author Zwirni
     */
    public void setPreventWeeklyS4BetaHint(boolean prevent) {
    	this.preventWeeklyS4BetaHint = prevent;
    	notifyConfigurationListeners();
    }
    
    /**
     * Get marker to prevent weekly s4-beta hint
     *      
     * @author Zwirni
     */
    public boolean getPreventWeeklyS4BetaHint() {
    	return this.preventWeeklyS4BetaHint;
    }
    
    /**
     * Set marker to prevent the April-fool for the actual year
     * 
     * @author Zwirni
     */
    public void setPreventAprilsFood(boolean prevent) {
    	this.preventAprilsFool = prevent;
    	notifyConfigurationListeners();
    }
    
    /**
     * Get marker to prevent return to Easter theme
     * during Easter time
     *      
     * @author Zwirni
     */
    public boolean getPreventEasterHint() {
    	return this.preventEasterHint;
    }
    
    /**
     * Get marker to prevent aprils fool
     * during Easter time
     *      
     * @author Zwirni
     */
    public boolean getPreventAprilsFool() {
    	return this.preventAprilsFool;
    }
    
    public ALobbyThemes getTheme()
    {
        return theme;
    }

    public boolean isLogChat()
    {
    	return chatLogging;
    }

    public void setLogChat(boolean chatLogging)
    {
        this.chatLogging = chatLogging;
        notifyConfigurationListeners();
    }

    public boolean isFlashingWhenHighlighted()
    {
        return flashingWhenHighlighted;
    }

    public void setFlashingWhenHighlighted(boolean flashingWhenHighlighted)
    {
    	this.flashingWhenHighlighted = flashingWhenHighlighted;
        notifyConfigurationListeners();
    }

    public String getAutomaticMapDownloadPath()
    {
        return automaticMapDownloadPath;
    }

    public void setAutomaticMapDownloadPath(String automaticMapDownloadPath)
    {
        this.automaticMapDownloadPath = automaticMapDownloadPath;
    }

    public List<String> getBuddys()
    {
        return buddys.get(userName);
    }

    public void setBuddys(List<String> buddys)
    {
        this.buddys.put(userName, buddys);
    }

    public List<String> getIgnoredUsers()
    {
        return ignoredUsers.get(userName);
    }

    public void setIgnoredUsers(List<String> ignoredUsers)
    {
        this.ignoredUsers.put(userName, ignoredUsers);
    }


    public void addBuddy(String nick)
    {
    	if (buddys.get(userName) == null) {
    		buddys.put(userName, new ArrayList<String>() {
    			@Override
    		    public boolean contains(Object obj) {
    		        String object = (String)obj;
    		        for (String string : this) {
    		            if (object.equalsIgnoreCase(string)) {
    		              return true;
    		            }
    		        }
    		        return false;
    		    }
    		});
    	}
        if (buddys.get(userName).indexOf(nick) == -1)
        {
            buddys.get(userName).add(nick);
        }
    }

    /**
     * Remove a buddy from list.
     * 
     * @param nick
     */
    public void removeBuddy(String nick)
    {
        int index;
        //falls Namen mehrfach drinstehen, werden
        //sie hier alle gelöscht
        while ((index = buddys.get(userName).indexOf(nick)) != -1)
        {
            buddys.get(userName).remove(index);
        }
    }
    
    /**
     * Check if a nickname exists in buddy-list.
     * 
     * Warning: this check is done case-insensitive.
     * 
     * @param nick
     * @return
     */
    public boolean isBuddy(String nick)
    {
        List<String> buddyList = buddys.get(userName);
        return buddyList != null && buddyList.indexOf(nick) != -1;
    }

    /**
     * Check if the buddy-list contains a nickname.
     * This check is done case insensitive as we override the ArrayList.contains, 
     * but may result in imprecise returnvalues. 
     * 
     * @param nick
     * @return
     */
    public boolean isBuddyList(String nick)
    {
        List<String> buddyList = buddys.get(userName);
        return buddyList != null && buddyList.contains(nick) != false;
    }

    /**
     * Add a nick to the ignore-list.
     * 
     * @param nick
     */
    public void addIgnoredUser(String nick)
    {
    	nick = nick.toLowerCase();
    	if (ignoredUsers.get(userName) == null) {
    		ignoredUsers.put(userName, new ArrayList<String>());
    	}
        if (ignoredUsers.get(userName).indexOf(nick) == -1)
        {
            ignoredUsers.get(userName).add(nick);
        }
    }

    public void removeIgnoredUser(String nick)
    {
        int index;
        nick = nick.toLowerCase();
        //falls Namen mehrfach drinstehen, werden
        //sie hier alle gelöscht
        while ((index = ignoredUsers.get(userName).indexOf(nick)) != -1)
        {
            ignoredUsers.get(userName).remove(index);
        }
    }

    public boolean isIgnoredUser(String nick)
    {
        return ignoredUsers.get(userName) != null && ignoredUsers.get(userName).indexOf(nick.toLowerCase()) != -1;
    }

	public String getBuddyIgnoreUrl() {
		return buddyIgnoreUrl;
	}

	public void setBuddyIgnoreUrl(String buddyIgnoreUrl) {
		this.buddyIgnoreUrl = buddyIgnoreUrl;
	}

	public boolean isAutoLoadAndSaveBuddys() {
		return isAutoLoadAndSaveBuddys;
	}

	public void setAutoLoadAndSaveBuddys(boolean isAutoLoadAndSaveBuddys) {
		this.isAutoLoadAndSaveBuddys = isAutoLoadAndSaveBuddys;
	}

	public void setStorePassword(boolean isStorePassword) {
		this.isStorePassword = isStorePassword;
	}

	public boolean isStorePassword() {
		return isStorePassword;
	}

	public boolean isNotifyIfBuddyComesOnline() {
		return isNotifyIfBuddyComesOnline;
	}

	public void setNotifyIfBuddyComesOnline(boolean isNotifyIfBuddyComesOnline) {
		this.isNotifyIfBuddyComesOnline = isNotifyIfBuddyComesOnline;
	}

	public void setHighlightingWords(List<String> words) {
		highlightingWords = words;
	}

	public List<String> getHighlightingWords() {
		return highlightingWords;
	}

	public String getDefaultMapDownloadPath() {
		return defaultMapDownloadPath;
	}

	public String getDefaultAutomaticMapDownloadPath() {
		return defaultAutomaticMapDownloadPath;
	}
	
	public String getDefaultGetMapData() {
		return defaultGetMapData;
	}
	
	public String getDefaultMapSearch() {
		return defaultMapSearch;
	}
	
	public String getDefaultGetMapMd5() {
		return defaultGetMapMd5;
	}
	
	public String getDefaultGetMapStat() {
		return defaultGetMapStat;
	}
	
	public String getDefaultGetRandomMap() {
		return defaultGetRandomMap;
	}
	
	public String getDefaultScreenUploader() {
		return defaultScreenUploader;
	}
	
	public String getDefaultScreenUploaderDomain() {
		return defaultScreenUploaderDomain;
	}

	public String getDefaultBuddyIgnoreUrl() {
		return defaultBuddyIgnoreUrl;
	}

	public boolean getSearchAndInstallUpdateAutomatically()
	{
		return searchAndInstallUpdateAutomatically;
	}

	public void setSearchAndInstallUpdateAutomatically(boolean searchAndInstallUpdateAutomatically)
	{
	    this.searchAndInstallUpdateAutomatically = searchAndInstallUpdateAutomatically;
	}

    public List<String> getIpViaUrl()
    {
        return ipViaUrl;
    }

    public void setIpViaUrl(List<String> ipViaUrl)
    {
        this.ipViaUrl = ipViaUrl;
    }

	public boolean isSortBuddys() {
		return isSortBuddys;
	}

	public void setSortBuddys(boolean isSortBuddys) {
		this.isSortBuddys = isSortBuddys;
		notifyConfigurationListeners();
	}

	public boolean isSortAway() {
		return isSortAway;
	}

	public void setSortAway(boolean isSortAway) {
		this.isSortAway = isSortAway;
		notifyConfigurationListeners();
	}

	public boolean isSortInGame() {
		return isSortInGame;
	}

	public void setSortInGame(boolean isSortInGame) {
		this.isSortInGame = isSortInGame;
		notifyConfigurationListeners();
	}

    public void setWindowMainMaximised(boolean isWindowMainMaximised)
    {
        this.isWindowMainMaximised = isWindowMainMaximised;
    }

    public boolean isWindowMainMaximised()
    {
        return isWindowMainMaximised;
    }

	public String getUrlIsLobbyNewVersion() {
		return urlIsLobbyNewVersion;
	}

	public void setUrlIsLobbyNewVersion(String urlIsLobbyNewVersion) {
		this.urlIsLobbyNewVersion = urlIsLobbyNewVersion;
	}

	public String getDefaultUrlIsLobbyNewVersion() {
		return defaultUrlIsLobbyNewVersion;
	}

	public String getUrlTime() {
		return urlTime;
	}

	public boolean getShowMessagesIngame(){
		return showMessagesIngame;
	}

	public void setShowMessagesIngame(boolean showMessagesIngame){
		this.showMessagesIngame = showMessagesIngame;
	}

	public boolean getShowWhisperIngame(){
		return showWhisperIngame;
	}

	public void setShowWhisperIngame(boolean showWhisperIngame){
		this.showWhisperIngame = showWhisperIngame;
	}

	public Locale getLanguage() {
		return language;
	}

	public void setLanguage(Locale language) {
		this.language = language;
	}

	/**
	 * Gibt an, ob die Langugae in der config.xml gesetzt war.
	 * @return True, wenn sie gesetzt war, andernfalls false.
	 */
	public boolean isLanguageSet() {
		return language != null;
	}

	public int getIrcServerPort()
	{
		String[] splittedUrl = getSplittedServerUrl();
		int port = STANDARD_PORT;
		if (splittedUrl.length == 2)
		{
			try
			{
				port = Integer.parseInt(splittedUrl[1]);
			} catch (NumberFormatException e)
			{
				port = STANDARD_PORT;
			}
		}
		return port;
	}

	public String getIrcServerUrl()
	{
		return getSplittedServerUrl()[0];
	}

	private String[] getSplittedServerUrl()
	{
		return serverURL.split(":");
	}

    public boolean isActivateScreenSizeCheck()
    {
        return activateScreenSizeCheck;
    }

    public void setActivateScreenSizeCheck(boolean activateScreenSizeCheck)
    {
        this.activateScreenSizeCheck = activateScreenSizeCheck;
    }

    public String getGameDateTimeFormat()
    {
        return gameDateTimeFormat;
    }

    public void setGameDateTimeFormat(String dateTimeFormat)
    {
    	gameDateTimeFormat = dateTimeFormat;
    	notifyConfigurationListeners();
    }

    public String getLeagueUrl()
    {
        return leagueUrl;
    }

    public String getLeagueUrlInterface()
    {
        return leagueUrlInterface;
    }
    
    public String getLeagueUrlState()
    {
        return leagueUrlRankingLists + "state";
    }
    
    public String getLeagueUrlGameCount()
    {
        return leagueUrlGameCount;
    }
    
    public String getLeagueUrlRankingLists()
    {
        return leagueUrlRankingLists;
    }
    
    public String getTournamentPlayerList() {
		return tournamentPlayerList;
	}
    
    public String getTournamentUserAllowedUrl() {
		return tournamentUserAllowed;
	}
    
    public String getTetrisHighscoreUrl() {
		return tetrisHighscoreURL;
	}

    public boolean isActivateForegroundWindowCheck()
    {
        return activateForegroundWindowCheck;
    }
    
    public boolean isActivateS3StatsScreenCapture() {
    	return activateS3StatsScreenCapture;
    }

    public void setActivateForegroundWindowCheck(boolean activateForegroundWindowCheck)
    {
        this.activateForegroundWindowCheck = activateForegroundWindowCheck;
    }
    
    public void setActivateS3StatsScreenCapture(boolean activateS3StatsScreenCapture)
    {
    	this.activateS3StatsScreenCapture = activateS3StatsScreenCapture;
    }

    public boolean isHideLeagueInterface()
    {
        return hideLeagueInterface;
    }

    public boolean isHideTeamspeakWindow()
    {
        return hideTeamspeakWindow;
    }

    public void setHideTeamspeakWindow(boolean hideTeamspeakWindow)
    {
        this.hideTeamspeakWindow = hideTeamspeakWindow;
    }

    public boolean isTabbedChatPane()
    {
        return isTabbedChatPane;
    }

    public void setTabbedChatPane(boolean isTabbedChatPane)
    {
        this.isTabbedChatPane = isTabbedChatPane;
    }

    public boolean isStartSavemanager()
    {
        return startSavemanager;
    }
    public boolean isStartWatchdog()
    {
        return startWatchdog;
    }    
    public void setStartSavemanager(boolean doStart)
    {
        startSavemanager = doStart;
        notifyConfigurationListeners();
    }
    public void setStartWatchdog(boolean doStart)
    {
        startWatchdog = doStart;
        notifyConfigurationListeners();
    }
    public boolean isAutoPlace()
    {
        return autoPlace;
    }
    
    public boolean isHideS4Interface()
    {
        if (!gamePathS4.isEmpty()) {
            hideS4Interface = false;
        }
        return hideS4Interface;
    }

    public void setAutoPlace(boolean doAutoPlace)
    {
    	autoPlace = doAutoPlace;
        notifyConfigurationListeners();
    }

    public int getSaveBackupCount()
    {
        return backupCount;
    }

    public void setSaveBackupCount(int backupCount)
    {
    	this.backupCount = backupCount;
    	notifyConfigurationListeners();
    }

    public int getSaveBackupCleanupInterval()
    {
        return cleanupTime;
    }

    public void setSaveBackupCleanupInterval(int cleanupTime)
    {
    	this.cleanupTime = cleanupTime;
    	notifyConfigurationListeners();
    }

    public boolean isDoSaveBackupCleanup()
    {
        return doCleanup;
    }

    public void setDoSaveBackupCleanup(boolean doCleanup)
    {
    	this.doCleanup = doCleanup;
    	notifyConfigurationListeners();
    }

    public int getSaveCleanupInterval()
    {
        return saveCleanupTime;
    }

    public void setSaveCleanupInterval(int saveCleanupTime)
    {
        this.saveCleanupTime = saveCleanupTime;
        notifyConfigurationListeners();
    }

    public boolean isDoSaveCleanup()
    {
        return doSaveCleanup;
    }

    public void setDoSaveCleanup(boolean doSaveCleanup)
    {
        this.doSaveCleanup = doSaveCleanup;
        notifyConfigurationListeners();
    }

    public boolean isAutoCompleteNicknames()
    {
        return autoCompleteNicknames;
    }

    public void setAutoCompleteNicknames(boolean autoCompleteNicknames)
    {
    	this.autoCompleteNicknames = autoCompleteNicknames;
    	notifyConfigurationListeners();
    }

    /*
     * Get the configfile as File
     * 
     * @return File	the file
     */
	public File getConfigFilePath() 
	{
		return new File(SettlersLobby.configDir, CONFIG_XML);
	}

	public boolean isPlaySoundWhenHighlighted() {
		return playSoundWhenHighlighted;
	}

	public void setPlaySoundWhenHighlighted(boolean selected) {
		this.playSoundWhenHighlighted = selected;
		notifyConfigurationListeners();
	}

	/**
	 * Set active main tab.
	 * 
	 * @param tab
	 */
	public void setActiveMainTab(int tab) {
    	activemainTab = tab;
    	notifyConfigurationListeners();
    }
	
	/**
	 * Get active main tab.
	 * 
	 * @return int
	 */
    public int getActiveMainTab() {
    	return activemainTab;
    }
	
	public void setnewGameTab(int tab) {
    	newGameTab = tab;
    	notifyConfigurationListeners();
    }
    public int getnewGameTab() {
    	return newGameTab;
    }
	
	public void setnewS3GameTab(int tab) {
    	newS3GameTab = tab;
    	notifyConfigurationListeners();
    }
    public int getnewS3GameTab() {
    	return newS3GameTab;
    }
    
    public void setnewS4GameTab(int tab) {
    	newS4GameTab = tab;
    	notifyConfigurationListeners();
    }
    public int getnewS4GameTab() {
		return newS4GameTab;
	}
    
    /**
     * Save the S4-team-setting.
     * 
     * @param teams
     */
    public void setnewS4GameTeams(int teams) {
    	newS4GameTeams = teams;
    	notifyConfigurationListeners();
    }
    
    /**
     * Get the S4-team-setting.
     * 
     * @return
     */
    public int getnewS4GameTeams() {
		return newS4GameTeams;
	}
    
    public void setnewS4GamePlayers(int players) {
    	newS4GamePlayers = players;
    	notifyConfigurationListeners();
    }
    public int getnewS4GamePlayers() {
		return newS4GamePlayers;
	}
    
    /**
     * Save the S4-random-mapsize-setting.
     * 
     * @param teams
     */
    public void setnewS4GameRandomMapSize(int mapsize) {
    	newS4GameRandomMapSize = mapsize;
    	notifyConfigurationListeners();
    }
    
    /**
     * Get the S4-random-mapsize-setting.
     * 
     * @return
     */
    public int getnewS4GameRandomMapSize() {
		return newS4GameRandomMapSize;
	}
    
    /**
     * Save the S4-random-landmass-setting.
     * 
     * @param landmass
     */
    public void setnewS4GameRandomLandmass(int landmass) {
    	newS4GameRandomMapSize = landmass;
    	notifyConfigurationListeners();
    }
    
    /**
     * Get the S4-random-landmass-setting.
     * 
     * @return
     */
    public int getnewS4GameRandomLandmass() {
		return newS4GameRandomMapSize;
	}
    
    /**
     * Get the S4-random-landmass-setting.
     * 
     * @return
     */
    public int getnewS4GameRandomAxes() {
		return newS4GameRandomAxes;
	}
    
    /**
     * Save the S4-random-axes-setting.
     * 
     * @param axes
     */
    public void setnewS4GameRandomAxes(int axes) {
    	newS4GameRandomAxes = axes;
    	notifyConfigurationListeners();
    }
    
    /**
     * Get the S4-random-mineral-resources-setting.
     * 
     * @return
     */
    public int getnewS4GameRandomMineralResources() {
		return newS4GameRandomResources;
	}
    
    /**
     * Save the S4-random-axes-setting.
     * 
     * @param axes
     */
    public void setnewS4GameRandomMineralResources(int resources) {
    	newS4GameRandomResources = resources;
    	notifyConfigurationListeners();
    }
    
    /**
     * Get the S4-random-code-setting.
     * 
     * @return
     */
    public String getnewS4GameRandomCode() {
		return newS4GameRandomCode;
	}
    
    /**
     * Save the S4-random-code-setting.
     * 
     * @param code
     */
    public void setnewS4GameRandomCode(String code) {
    	newS4GameRandomCode = code;
    	notifyConfigurationListeners();
    }
    
    /**
     * Get the state for the S4-Beta-hint
     * 
     * @return
     */
    public boolean isPreventS4BetaHint() {
		return preventS4BetaHint;
	}
    
    /**
     * Save the S4-random-code-setting.
     * 
     * @param code
     */
    public void setPreventS4BetaHint(boolean state) {
    	preventS4BetaHint = state;
    	notifyConfigurationListeners();
    }
    
    public void setnewS3GameGoods(int goods) {
    	newS3GameGoods = goods;
    	notifyConfigurationListeners();
    }
    public int getnewS3GameGoods() {
    	return newS3GameGoods;
    }
    
    public void setnewS4GameGoods(int goods) {
    	newS4GameGoods = goods;
    	notifyConfigurationListeners();
    }
    public int getnewS4GameGoods() {
    	return newS4GameGoods;
    }
    
    public void setnewS3GameRandomPositon(boolean randomPositon) {
    	newS3GameRandomPosition = randomPositon;
    	notifyConfigurationListeners();
    }
    public boolean getnewS3GameRandomPositon() {
    	return newS3GameRandomPosition;
    }
    
    public void setnewS3GameMinichat(boolean minichat) {
    	newS3GameMinichat = minichat;
    	notifyConfigurationListeners();
    }
    public boolean getnewS3GameMinichat() {
    	return newS3GameMinichat;
    }
    
    public void setnewS3GameWimo(boolean wimo) {
    	newS3GameWimo = wimo;
    	notifyConfigurationListeners();
    }
    public boolean getnewS3GameWimo() {
    	return newS3GameWimo;
    }
    
    public void setnewS4GameWimo(boolean wimo) {
    	newS4GameWimo = wimo;
    	notifyConfigurationListeners();
    }
    public boolean getnewS4GameWimo() {
    	return newS4GameWimo;
    }
    
    public void setnewS3GameLeague(boolean league) {
    	newS3GameLeague = league;
    	notifyConfigurationListeners();
    }
    public boolean getnewS3GameLeague() {
    	return newS3GameLeague;
    }
    
    public int getnewS3GameTournament() {
    	return newS3GameTournament;
    }
    
    public void setnewS4GameTrojans(boolean trojans) {
    	newS4GameTrojans = trojans;
    	notifyConfigurationListeners();
    }
    public boolean getnewS4GameTrojans() {
    	return newS4GameTrojans;
    }
    
    public void setTetrisHighscore(long score) {
    	tetrisHighscore = score;
    	notifyConfigurationListeners();
    }
    public long getTetrisHighscore() {
    	return tetrisHighscore;
    }
	
	/*
	 * Check if its Christmastime
	 * 
	 * @return boolean	true if its Christmastime
	 */
	public boolean itsChristmastime() {
		// create a Calendar-object
		Calendar c = new GregorianCalendar();
		
		// set it to 31.12. with actual year
		// -> so we do not need to set the year
		// 11 = December
		c.set(Calendar.MONTH, 11); 
		c.set(Calendar.DAY_OF_MONTH, 31); 
		 
		// calculate the days until 31.12.
		Date xmas = c.getTime();
		Date today = new Date();
		long diff = xmas.getTime() - today.getTime();
		diff = diff / (1000L*60L*60L*24L);
		
		if( diff >= 0 && diff < 31 ) {
			return true;
		}
		return false;
	}
	
	/**
	 * Check if its halloween
	 * 
	 * @return boolean	true if its halloween-time
	 */
	public boolean itsHalloweentime() {
		// create a Calendar-object
		Calendar c = new GregorianCalendar();
		
		// set it to 05.11. with actual year
		// -> so we do not need to set the year
		// 10 = November
		c.set(Calendar.MONTH, 10); 
		c.set(Calendar.DAY_OF_MONTH, 5); 
		 
		// calculate the days until 05.11.
		Date halloween = c.getTime();
		Date today = new Date();
		long diff = halloween.getTime() - today.getTime();
		diff = diff / (1000L*60L*60L*24L);
		
		if( diff >= 0 && diff < 14 ) {
			return true;
		}
		return false;
	}
	
	/**
	 * Calculate easter-sunday-date on year-base.
	 * 
	 * @param year
	 * @return GregorianCalendar
	 */
	private Calendar getEasterSundayAsDate(int year) {
		Calendar cal = new GregorianCalendar();
        int a = year % 19,
            b = year / 100,
            c = year % 100,
            d = b / 4,
            e = b % 4,
            g = (8 * b + 13) / 25,
            h = (19 * a + b - d - g + 15) % 30,
            j = c / 4,
            k = c % 4,
            m = (a + 11 * h) / 319,
            r = (2 * e + 2 * j - k - h + m + 32) % 7,
            n = (h - m + r + 90) / 25,
            p = (h - m + r + n + 19) % 32;
        cal.set(Calendar.DAY_OF_MONTH, p);
        cal.set(Calendar.MONTH, n - 1);
        cal.set(Calendar.YEAR, year);
        return cal;
    }
	
	/**
	 * Check if its easter-time.
	 * 
	 * @return boolean	true if its easter-time
	 */
	public boolean itsEastertime() {
		// get the easter-sunday-date for actual year
		Calendar calForYear = new GregorianCalendar();
		Calendar cal = getEasterSundayAsDate(calForYear.get(Calendar.YEAR));
		
		// add 5 days after easter-sunday
		cal.add(Calendar.DAY_OF_MONTH, +5);
		
		// calculate the days until this calculcated.
		Date easter = cal.getTime();
		Date today = new Date();
		long diff = easter.getTime() - today.getTime();
		diff = diff / (1000L*60L*60L*24L);
		if( diff >= 0 && diff < 14 ) {
			return true;
		}
		
		return false;
	}
	
	/*
	 * Check if it is april 1th
	 * 
	 * @return boolean	true if it is april 1th
	 */
	public boolean itsApril1th() {
		// create a Calendar-object
		Calendar c = new GregorianCalendar();
		
		// set it to 01.04. with actual year
		// -> so we do not need to set the year
		// 03 = December
		c.set(Calendar.MONTH, 03); 
		c.set(Calendar.DAY_OF_MONTH, 01); 
		
		Calendar cal1 = Calendar.getInstance();
        cal1.setTime(c.getTime());
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(new Date());
		
		return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
				cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
		
	}
	
	/**
	 * Get the fontAweSome to use many icons as text.
	 * Source: http://fortawesome.github.io
	 * 
	 * @return Font
	 */
	public Font getFontAweSome() {
		return getFontAweSome( (float)this.getFontSize(), Font.PLAIN );
	}
	public Font getFontAweSome( float fontSize ) {
		return getFontAweSome( (float)this.getFontSize(), Font.PLAIN );
	}
	public Font getFontAweSome( float fontSize, int fontStyle ) {
		// get the special font named "awesome font"
		InputStream fontStream = this.getClass().getResourceAsStream(ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/fontawesome-webfont.ttf");
		Font f = null;
		try {
			f = Font.createFont( Font.TRUETYPE_FONT, fontStream );
			// set the font-size and get the font
			return f.deriveFont(fontStyle, fontSize);
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return f;
	}

	/**
	 * Check if the option to use del-key in input-field is enabled.
	 * 
	 * @return
	 */
	public boolean isDelKey() {
		return useDelKey;
	}

	/**
	 * Set the option to use del-key in input-field.
	 * 
	 * @param showTime
	 */
	public void setDelKey(boolean state)
    {
        this.useDelKey = state;
        notifyConfigurationListeners();
    }

	/**
	 * Save the S4-coop-setting.
	 * 
	 * @param coop
	 */
	public void setnewS4GameCoop(boolean coop) {
		this.newS4GameCoop = coop;
		notifyConfigurationListeners();
	}
	
	/**
	 * Get the S4-coop-setting.
	 * 
	 * @return boolean
	 */
	public boolean getnewS4GameCoop() {
    	return newS4GameCoop;
    }
	
	/**
	 * Get info if given day has been used in adventcalendar.
	 * 
	 * @param year
	 * @param day
	 * @return
	 */
	public boolean getAdventCalendarDayShow(int year, int day) {
		return Boolean.parseBoolean(xmlConfig.getByPath("adventcalendar/year" + year + "/day" + day + "/used"));
	}
	
	/**
	 * Set info if given day has been used in adventcalendar.
	 * 
	 * @param year
	 * @param day
	 * @param value
	 */
	public void setAdventCalendarDayShow(int year, int day, boolean value) {
		setConfigData("adventcalendar/year" + year + "/day" + day + "/used", value);
	}

    /**
     * Get configured S3-versionnumber
     */
    public int getS3Versionnumber() {
        if (this.S3versionnumber <= 172) {
            // Upgrade everyone still using 1.72 to 1.73
            setS3Versionnumber(173);
        }
        return this.S3versionnumber;
    }

    /**
     * Get whether the current version is a CE version.
     */
	public boolean isCEVersion() {
	    return this.getS3Versionnumber() >= 300;
    }
	
	/**
	 * Set the S3versionnumber this user will be use.
	 * 
	 * @param S3versionnumber
	 */
	public void setS3Versionnumber(int S3versionnumber)
    {
        this.S3versionnumber = S3versionnumber;
        notifyConfigurationListeners();
    }
	
	/**
	 * Check if its winter-time.
	 * 
	 * @return boolean 	true if it is winter, false if not
	 */
	public boolean itsWinter() {
		if( this.getSeason().equals("Winter") ) {
			return true;
		}
		return false;
	}
	
	/**
	 * Check the actual season.
	 * 
	 * @return String	Spring, Summer, Fall or Winter depending on actual date
	 */
	private String getSeason() {
		
		// get an object to parse the date and prepare the format
		SimpleDateFormat f = new SimpleDateFormat("MM-dd");
        f.setLenient(false);
        
        // get the calendar-object
        Calendar c = new GregorianCalendar();
        
        // set the actual date (only month and day of month)
        int m = c.get(Calendar.MONTH) + 1;
        int d = c.get(Calendar.DAY_OF_MONTH);
        
        // prepare return-value
        String season = "";
        
        // set date-object
        Date date;
		try {
			// parse the actual date to get and valide date-object
			date = f.parse( m + "-" + d);
			
			// check if the date is in spring
			if(date.after(f.parse("04-21")) && date.before(f.parse("06-21"))){
                season = "Spring";
            }
			// check if date is in summer
            else if(date.after(f.parse("06-20")) && (date.before(f.parse("09-23"))))
            {
                season = "Summer";
            }
			// check if date is in fall
            else if(date.after(f.parse("09-22")) && date.before(f.parse("12-22")))
            {
                season = "Fall";
            }
            else {
            	// otherwise it must be winter
            	season = "Winter";
            }
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return season;
	}

	/**
	 * Return the configured s3-resolution for Community Edition.
	 * If nothing is configured return the highest possible resolution the aLobby would suggest.
	 * 
	 * @return String
	 */
    public String getS3CEResolution() {
        if (this.s3ceResolution.isEmpty()) {
                this.s3ceResolution = S3Support.getHighestSupportedResoultion();
            Test.output("S3 CE resolution settings empty; Setting S3 CE resolution to: " + this.s3ceResolution);
            }
        Test.output("S3 CE resolution: " + this.s3ceResolution);
        return this.s3ceResolution;
    }

	/**
	 * Set the s3-resolution for Community Edition.
	 * 
	 * @param string
	 */
	public void setS3CEResolution(String string) {
        Test.output("Setting S3 CE resolution to: " + string);
		this.s3ceResolution = string;
		notifyConfigurationListeners();
	}

	/**
	 * Return the MiscType for configured s3-versionnubmer.
	 * 
	 * @return
	 */
    public net.siedler3.alobby.controlcenter.OpenGame.MiscType getS3VersionAsMiscType() {
        return (this.S3versionnumber < 300) ? net.siedler3.alobby.controlcenter.OpenGame.MiscType.ALOBBY : net.siedler3.alobby.controlcenter.OpenGame.MiscType.S3CE;
    }
	
}
