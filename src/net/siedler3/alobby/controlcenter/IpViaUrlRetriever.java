/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

import net.siedler3.alobby.util.httpRequests;

/**
 * Diese Klasse versucht die externe IP mit Hilfe
 * spezieller Server URLs zu ermitteln, die die IP anzeigen.
 * Die Klasse ist als thread implementiert, damit man
 * schon in der Lobby chatten kann, während noch versucht
 * wird die IP zu ermitteln und die Serververbindung
 * dabei längere Zeit blockiert sein kann.
 * 
 * @author jim
 *
 */
public class IpViaUrlRetriever extends Thread
{
    private SettlersLobby settlersLobby;
    String ip = null;

    public IpViaUrlRetriever(SettlersLobby settlersLobby)
    {
        super();
        this.settlersLobby = settlersLobby;
    }

    
    /**
     * Geht durch alle angegebenen Server in der confix.xml (url/ip) und versucht
     * die eigene IP zu ermitteln.
     * Kann zu keinem der Server eine Verbindung aufgebaut werden, terminiert
     * Das Program mit Fehlernachricht.
     */
    private void tryToInitIP()
    {   
        ip = null;
        for (String url : settlersLobby.config.getIpViaUrl())
        {
            if (isInterrupted())
            {
                //Thread wurde unterbrochen
                break;
            }
            try
            {
                if (url != null)
                {
                    initIP(url);
                    if (ip != null)
                    {
                        //IP ermittelt, Schleife abbrechen
                        break;
                    }
                }
            } catch (SocketTimeoutException ste)
            {
                //URL nicht erreichbar, nächste probieren
                continue;
            } 
            catch (Exception e)
            {
                //Unerwarteter Fehler, nächste URL probieren
                Test.output("Fehler beim Initialisieren der IP: " + e.getMessage());
                continue;
            }
        }
        
        if (ip != null && (!isInterrupted()))
        {
            //es konnte eine IP ermittelt werden, als Host-IP in 
            //settlersLobby speichern
            Test.output("IP via URL: " + ip);
            settlersLobby.setIp(ip);
        }
    }
    
    /**
     * Versucht die Seite der übergebenen url zu parsen und das Ergebnis in 
     * die ip Objektvariable zu schreiben.
     * 
     * @param   url         Die URL der Seite, die geparst werden soll.
     * @throws  Exception   Wird bei Problemen mit dem Verbindungsaufbau
     *                      geschmissen.
     */
    private void initIP(String url) throws Exception
    {
    	httpRequests httpRequests = new httpRequests();
		httpRequests.setUrl(url);
		httpRequests.addParameter("get", "1");
		httpRequests.setIgnorePhpSessId(true);
		// -> get the response-String
		String response = null;
		try {
			response = httpRequests.doRequest();
		} catch (IOException e1) {
		}
		if( response == null ) {
			return;
		}
		//überprüft ob das Resultat eine gültige IP oder hostname ist
        InetAddress address = InetAddress.getByName(response.trim());
        ip = address.getHostAddress();
    }


    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run()
    {
        tryToInitIP();
    }
    
    
}
