/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Test
{
    public static boolean stopLogging = false;

    private static final Format dateFormat = new SimpleDateFormat("HH:mm:ss.SSS"); //$NON-NLS-1$
    private static PrintWriter pw = null;

    public static void enableDebugLog()
    {
        try
        {
            pw = new PrintWriter(new FileOutputStream(new File(SettlersLobby.configDir, "debugLog.txt")), true); //$NON-NLS-1$
        }
        catch (Exception e)
        {
            pw = null;
        }
    }
    public static void disableDebugLog()
    {
        pw.close();
        pw = null;
    }

	public static void output(String line)
	{
	    outputWithTrace(line, 2, false);
	}

	public static void outputWithIpFilter(String line)
	{
        //simple filter for ip addresses, but sufficient for logging purpose.
        //Log only first two parts of the ip address for security reasons
        String filterIpRegEx = "([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})";
        String tmpOutput = line.replaceAll(filterIpRegEx, "$1.$2.*.*"); //$NON-NLS-1$
        outputWithTrace(tmpOutput, 2, false);
	}

	public static void outputException(Throwable t)
	{
	    if (stopLogging == false)
	    {
	        outputWithTrace("Exception: ", 2, true);
	        t.printStackTrace(System.out);
	        if (pw != null)
	        {
	            t.printStackTrace(pw);
	        }
	    }

	}

    private static void outputWithTrace(String line, int element, boolean logFileName)
    {
        try
        {
            StackTraceElement trace = new Throwable().getStackTrace()[element];
            //nur den Klassennamen ausgeben, ohne Paketnamen
            String className = trace.getClassName();
            int index = className.lastIndexOf('.');
            if (index >= 0)
            {
                className = className.substring(index + 1);
            }
            String result;
            if (logFileName)
            {
                result = String.format("%s %s.%s(%s:%d): %s",
                                dateFormat.format(new Date()),
                                className,
                                trace.getMethodName(),
                                trace.getFileName(),
                                trace.getLineNumber(),
                                line);
            }
            else
            {
                result = String.format("%s %s.%s(): %s",
                                dateFormat.format(new Date()),
                                className,
                                trace.getMethodName(),
                                line);
            }
            
            result = SettlersLobby.searchAndReplaceIPAndHostname(result);

            System.out.println(result);
            if (pw != null)
            {
                pw.println(result);
            }
        }
        catch (Exception e)
        {
        }
    }

}
