/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

public class AwayByAbstinence extends Thread
{
	private AwayStatus awayStatus;
	private long lastActivityTime;
	Configuration config;

	public AwayByAbstinence(Configuration config, AwayStatus awayStatus)
	{
		this.config = config;
		lastActivityTime = System.currentTimeMillis();
		this.awayStatus = awayStatus;
		setName(getName()+ "-AwayByAbstinence");
		start();
	}

	/**
	 * Setzt den User auf Away, falls er eine gewisse Zeit lang keine Aktionen
	 * durchgeführt hat.
	 */
	public void run()
	{
		try
		{
			Thread.sleep(config.getAwayTime());
			while (!isInterrupted())
			{
			    long time = System.currentTimeMillis();
				if (lastActivityTime + config.getAwayTime() <= time)
				{
					awayStatus.doIdleAway();
					interrupt();
				} else
				{
					sleep(lastActivityTime + config.getAwayTime()
							- time);
				}
			}
		} catch (InterruptedException e1)
		{
			interrupt();
		}
	}

	/**
	 * Setzt die letzte Aktivität des Users auf jetzt.
	 */
	public void userWasActive()
	{
		this.lastActivityTime = System.currentTimeMillis();
	}
}
