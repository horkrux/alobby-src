package net.siedler3.alobby.controlcenter.integrity;

import java.io.File;
import java.io.InputStream;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.download.Download;
import net.siedler3.alobby.util.CryptoSupport;

public class CheckIntegrity
{

    /**
     * Überprüft, ob die Checksummen der UnRar Dateien
     * mit den im jar-file gespeicherten Referenzwerten übereinstimmen.
     *
     * @return
     */
    public static boolean checkUnrarFiles()
    {
        //Die Datei unrar.md5 ist eine Kopie der Datei
        //aus dem "/unrar"-Verzeichnis
        InputStream is = CheckIntegrity.class.getResourceAsStream(
                ALobbyConstants.PACKAGE_PATH + "/controlcenter/integrity/unrar.md5"); //$NON-NLS-1$

        File parentDir = new File(Download.UNRAR_EXE).getParentFile();

        return CryptoSupport.md5Sum(is, parentDir);
    }
}
