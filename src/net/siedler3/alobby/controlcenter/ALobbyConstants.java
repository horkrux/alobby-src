package net.siedler3.alobby.controlcenter;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Properties;

public class ALobbyConstants
{
    public static final String MSG_SERVER = "[Server]"; //$NON-NLS-1$
    public static final Charset UTF_8 = Charset.forName("UTF-8"); //$NON-NLS-1$
    public static final Properties versionProps = new Properties();
    static
    {
        try
        {
            versionProps.load(ALobbyConstants.class.getResourceAsStream("version.properties")); //$NON-NLS-1$
        }
        catch (IOException e)
        {
            Test.outputException(e);
        }
    }
    //die aktuelle Version steht im version.properties file. So kann sie sowohl
    //von der alobby als auch vom build.xml benutzt werden
    //die Version 1.0.0 wird als default genommen, falls beim Lesen etwas
    //schiefgegangen ist, was eigentlich nicht passieren kann
    public final static String VERSION = ALobbyConstants.versionProps.getProperty("version", "1.0.0"); //$NON-NLS-1$, //$NON-NLS-2$
    public final static String PROGRAM_NAME = "aLobby"; //$NON-NLS-1$
    /**
     * @since 0.14.2
     */
    public final static String TOKEN_NEW_GAME_ENC = "9"; //$NON-NLS-1$
    public final static String TOKEN_WANTS_TO_JOIN = "10"; //$NON-NLS-1$
    public final static String TOKEN_JOINED = "11"; //$NON-NLS-1$
    public final static String TOKEN_LEAVING = "12"; //$NON-NLS-1$
    public final static String TOKEN_MAP_USER_LIST = "13"; //$NON-NLS-1$
    public final static String TOKEN_USER_XY_HAS_LEFT = "14"; //$NON-NLS-1$
    public final static String TOKEN_AWAY_MESSAGE = "20"; //$NON-NLS-1$
    public final static String TOKEN_AWAY_BACK = "21"; //$NON-NLS-1$
    public final static String PATH_MAP = "Map"; //$NON-NLS-1$
    public final static String PATH_USER = "USER"; //$NON-NLS-1$
    public final static String PATH_S3MULTI = "MULTI"; //$NON-NLS-1$
    public final static String PATH_S4MULTI = "Multiplayer"; //$NON-NLS-1$
    public final static String PATH_S4USER = "User"; //$NON-NLS-1$
    public final static String PATH_S4SAVEGAMES = "Save"; //$NON-NLS-1$
    public final static String PATH_RANDOM = "RANDOM"; //$NON-NLS-1$
    public final static String LABEL_SAVE_GAME = "Savegame"; //$NON-NLS-1$
    public final static String LABEL_UNKNOWN_MAP = "unknown"; //$NON-NLS-1$
    //der gemeinsame paket pfad für alle Klassen
    //wird für getResourceAsStream() an diversen Stellen benötigt
    public static final String PACKAGE_PATH = "/net/siedler3/alobby"; //$NON-NLS-1$
    public final static boolean IRC_DEBUG_ON = true;
    //allow a-zA-Z0-9_ and special characters '[', ']', '|', '^'
    public static final String NICKNAME_PATTERN_STRING = "(\\w|[\\[\\]\\|\\^])+";
    // regex to validate an email-address
    public static final String EMAIL_VALIDATION_STRING = "^\\b[\\w.%-]+@[-.\\w]+\\.[-\\w]+\\b$";
    // regex to validate an url
    public static final String REGEX_URL = "^[A-Za-z][A-Za-z0-9+.-]{1,120}:[A-Za-z0-9/](([A-Za-z0-9$_.+!*,;/?:@&~=-])|%[A-Fa-f0-9]{2}){1,333}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*,;/?:@&~=%-]{0,1000}))?$";
    // min Windows NT Version we support (6.1 = Windows 7) => https://de.wikipedia.org/wiki/Microsoft_Windows_NT#Versionsgeschichte
    public static final double MIN_WINNT_VERSION = 6.1;
    // IP-Range for VPN-IP
    public final static String VPN_IP_RANGE = "10.3.";
    
    public static enum Races
    {
        ROMANS(0),
        ASIAN(1),
        EGYS(2),
        AMAZ(3);

        private int value;

        private Races(int value)
        {
            this.value = value;
        }

        public String getValue()
        {
            return Integer.toString(this.value);
        }

        public static Races valueOf(int value)
        {
            for (Races token : values())
            {
                if (token.value == value)
                {
                    return token;
                }
            }
            return null;
        }

    }
    
}
