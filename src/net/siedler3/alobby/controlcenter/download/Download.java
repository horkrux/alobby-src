/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter.download;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.swing.ProgressMonitorInputStream;
import javax.swing.UIManager;

import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.integrity.CheckIntegrity;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.i18n.I18n;

/**
 * Bietet Funktionen zum Herunterladen von Dateien an.
 * Damit dieses Herunterladen funktioniert, müssen
 * getDownloadURL, getDownloadPath und getDestinationDirectory
 * überschrieben werden. Für eine angepasste Fehlerausgabe kann
 * getMessage überschrieben werden, wobei von Download aus
 * Nachrichten verschickt werden.
 *
 * @author jim
 * @author amopyr
 *
 */
public abstract class Download
{

    private enum Exe
    {
        _7Zip(_7ZIP_EXE)
        {
            @Override
            public String toString()
            {
                return "7-Zip"; //$NON-NLS-1$
            }
        },
        UnRAR(UNRAR_EXE);


        private String exe;
        private Exe(String exe)
        {
            this.exe = exe;
        }

        public String getExe()
        {
            return this.exe;
        }

        /**
         * erzeugt die Kommandozeile, die man zum Auflisten der Dateien
         * im Archiv benötigt.
         *
         * @param archive
         * @return Kommandozeile
         */
        public String[] commandLineListArchive(File archive)
        {
            String[] command;

            switch (this)
            {
                case UnRAR:
                    //Kommando vb liefert nur die Dateinamen im Archiv
                    command = new String[]{
                            this.exe,
                            "vb", //$NON-NLS-1$
                            archive.getAbsolutePath()
                    };
                    break;
                case _7Zip:
                    //Kommando l liefert eine Liste, -slt noch so, daß
                    //in der Zeile mit "path = " nur der Name steht
                    //ohne -slt ist es schwierig den Dateinamen zu ermitteln
                    command = new String[]{
                            this.exe,
                            "l",  //$NON-NLS-1$
                            "-slt", //$NON-NLS-1$
                            archive.getAbsolutePath()
                    };
                    break;
                default:
                    throw new UnsupportedOperationException();
            }
            return command;
        }

        /**
         * erzeugt die Kommandozeile, die man zum extrahieren einer Datei
         * aus dem Archiv benötigt.
         *
         * @param archive
         * @param filenameInArchive die zu extrahierende Datei im Archiv
         * @param extractingDir Zielverzeichnis
         * @return Kommandozeile
         */
        public String[] commandLineExtractFileFromArchive(
                File archive, String filenameInArchive, String extractingDir)
        {
            String[] command;

            switch (this)
            {
                case UnRAR:
                    command = new String[]{
                            this.exe,
                            "x", //$NON-NLS-1$
                            "-y", //$NON-NLS-1$
                            "-n" + filenameInArchive, //$NON-NLS-1$
                            archive.getAbsolutePath(),
                            extractingDir
                    };
                    break;
                case _7Zip:

                    command = new String[]{
                            this.exe,
                            "x", //$NON-NLS-1$
                            "-o" + extractingDir, //$NON-NLS-1$
                            archive.getAbsolutePath(),
                            filenameInArchive
                    };
                    break;
                default:
                    throw new UnsupportedOperationException();
            }
            return command;
        }

        /**
         * Liefert den Dateinamen dieser Ausgabezeile des ListArchive Kommandos,
         * falls dort einer vorhanden ist.
         *
         * @param line Ausgabezeile erzeugt durch das ListArchive Kommando
         * @return Dateinamen, falls die Zeile einen enthält, sonst null
         */
        public String getFilenameFromListOutputLine(String line)
        {
            switch (this)
            {
                case UnRAR:
                	//bei unrar enthält jede Zeile einen Dateinamen
                    return line;
                case _7Zip:
                	//Zeilen die mit "Path = " beginnen, enthalten einen Dateinamen
                    if (line != null && line.startsWith("Path = ")) //$NON-NLS-1$
                    {
                        return line.substring(7);
                    }
                    return line;
                default:
                    throw new UnsupportedOperationException();
            }

        }

        /**
         *
         * @return true falls das executable existiert.
         */
        public boolean exists()
        {
            return new File(exe).exists();
        }

    }
	protected static final String MELDUNG_ERFOLGREICH = "Meldung:erfolgreich"; //$NON-NLS-1$
	protected static final String ERROR_KOPIEREN = "Error:kopieren"; //$NON-NLS-1$
	protected static final String ERROR_ENTPACKEN = "Error:entpacken"; //$NON-NLS-1$
	protected static final String ERROR_HERUNTERLADEN = "Error:herunterladen"; //$NON-NLS-1$
	protected static final String ERROR_TEMPORAERES_VERZEICHNIS = "Error:temporaeres Verzeichnis"; //$NON-NLS-1$
	protected static final String ERROR_VERZEICHNIS = "Error:Verzeichnis"; //$NON-NLS-1$
	protected static final String ERROR_SERVER = "Error:Server"; //$NON-NLS-1$
	protected static final String ERROR_VERSCHIEBEN = "Error:Verschieben"; //$NON-NLS-1$
	protected static final String ERROR_INVALID_SIGNATURE = "Error:invalidSignature";

    public static final String UNRAR_EXE = "unrar" + File.separator + "unrar.exe"; //$NON-NLS-1$ //$NON-NLS-2$
    public static final String _7ZIP_EXE = "unzip" + File.separator + "7za.exe"; //$NON-NLS-1$ //$NON-NLS-2$

	// overwrite file or not -> default: not
	private boolean overwriteFileWithoutHint = false;

	protected SettlersLobby settlersLobby;

    protected StringBuilder errorMessage = new StringBuilder();

    protected boolean doNotExtract = false;

    protected File archiveFile = null;
    protected GUI gui;

    public Download(SettlersLobby settlersLobby)
    {
    	this.settlersLobby = settlersLobby;
        this.gui = settlersLobby.getGUI();
    }

    /**
     * Lädt die Datei fileName von einer fest vorgegebenen
     * ServerUrl herunter.
     * Prinzipiell ist die Funktion darauf ausgelegt, maps vom mapserver
     * herunterzuladen, in diesem Fall ist fileName ein mapname.
     * Allerdings wurde die Funktion so angepasst, dass sie auch für
     * andere Zwecke benutzt werden kann, z.B. dem Herunterladen von updates.
     * Das unterschiedliche Verhalten wird durch die Implementierung
     * der Funktionen {@link #getDownloadURL(String, String)}
     * und {@link #getDestinationDirectory(String)} gesteuert.
     *
     * Die Datei wird von der URL heruntergeladen, und im Falle eines gepackten
     * Archivs auch entpackt.
     *
     * @return true wenn die Datei heruntergeladen und entpackt wurde.
     */
	public boolean downloadFile(String fileName, OpenGame openGame)
	{
		if( downloadFile(fileName, true) )
		{
			
			// hide mapdetail-window if it is active in this moment
			// to prevent an unusable alobby-state
			if( settlersLobby.getGUI().showMapDetail != null ) {
				settlersLobby.getGUI().showMapDetail.dispose();
			}
			
			// use MessageDialog to show the hint
			String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
	     	messagewindow.addMessage(
	     		MessageFormat.format("<html>" + I18n.getString("Download.MSG_DOWNLOAD_SUCCESSFULL") + "</html>", fileName, hexcolor),
	     		UIManager.getIcon("OptionPane.informationIcon")
	     	);
	     	messagewindow.addButton(
	     		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	     		new ActionListener() {
	     		    @Override
	     		    public void actionPerformed(ActionEvent e) {
				    	// start the game
			    		settlersLobby.actionWantsToJoinGame(openGame);
			    		// close this window
			    		messagewindow.dispose();
	     		    }
	     		}
	     	);
	     	messagewindow.addButton(
	     			I18n.getString("GUI.CANCEL"),
		     		new ActionListener() {
		     		    @Override
		     		    public void actionPerformed(ActionEvent e) {
					    	// simply close this window and do nothing
				    		messagewindow.dispose();
		     		    }
		     		}
		     	);
	     	messagewindow.setVisible(true);
			settlersLobby.getGUI().s3PanelDialogObject.loadMaps();
			return true;
		}
		return false;
	}
	
	public boolean downloadFile(String fileName) {
		return downloadFile(fileName, false);
	}
	
	public boolean downloadFile(String fileName, boolean nomessage)
	{
        String serverURL = getDownloadPath();

        URL url = null;
        try
        {
            // finde die zugehörige Download URL für die Karte
            // falls sie nicht auf dem Mapserver vorhanden ist, wird abgebrochen

        	// replace filename from \\User\abcde.map to abcde
        	// -> without this replacement the map would not be downloaded
        	// -> the removed chars are neccessary for openmapdialog
            url = getDownloadURL(serverURL, fileName);
        }
        catch (UserAbortException e)
        {
            return false;
        }
        if (url == null)
        {
            displayError(getMessage(ERROR_SERVER));
            return false;
        }

        File dir = getDestinationDirectory(settlersLobby.config.getGamePathS3());
        if (dir == null)
        {
            displayError(getMessage(ERROR_VERZEICHNIS));
            return false;
        }

        // Im Folgenden wird im temp-Dir ein Unterverzeichnis angelegt.
        // Dort hinein wird das Archiv heruntergeladen und entpackt.
        // Die entpackte Datei wird dann ins Ziel Verzeichnis
        // verschoben.
        // Falls alles klappt werden sowohl Archiv als auch temporäres
        // Unterverzeichnis wieder gelöscht.
        try
        {
            Path tmpDir = Files.createTempDirectory("alobby_");
            if (tmpDir == null)
            {
                displayError(getMessage(ERROR_TEMPORAERES_VERZEICHNIS));
                return false;
            }
            File tmpAlobbyDir = tmpDir.toFile();
            // Archiv herunterladen
            archiveFile = null;
            try
            {
                archiveFile = downloadFileToDir(url, tmpAlobbyDir);
            }
            catch (UserAbortException e)
            {
                tmpAlobbyDir.deleteOnExit();
                return false;
            }
            if (archiveFile == null)
            {
                displayError(getMessage(ERROR_HERUNTERLADEN));
                tmpAlobbyDir.deleteOnExit();
                return false;
            }

            if (doNotExtract)
            {
                //the file should only be download, no extraction required
                return true;
            }
            else
            {
            	// Archiv entpacken
            	File extractedFile = extractFile(archiveFile, fileName);
            	if (extractedFile == null)
            	{
                    onExtractError(archiveFile);
                	return false;
            	}

                // entpackte Datei verschieben
        		boolean result = moveFileToDir(extractedFile, dir, nomessage);
        		if (result)
        		{
            		// alles hat funktioniert, Archiv und Verzeichnis
            		// wieder löschen
            		try
            		{
                		// lösche alle Files, die eventuell noch
            			// vorhanden sind
                		for (File file : tmpAlobbyDir.listFiles())
                		{
                    		try
                    		{
                    			file.delete();
                    		}
                    		catch (Exception e)
                    		{
                    			Test.outputException(e);
                    		}
                		}
                		//falls doch noch Dateien vorhanden sind, wird
                		//das Löschen leider fehlschlagen
                		tmpAlobbyDir.delete();
            		}
            		catch (Exception e)
            		{
                		Test.outputException(e);
            		}
            		if( !nomessage ) {
	            		// use MessageDialog to show the hint
						String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
		     			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
		             	messagewindow.addMessage(
		             		MessageFormat.format("<html>" + I18n.getString("Download.MSG_DOWNLOAD_SUCCESSFULL_2") + "</html>", fileName, hexcolor),
		             		UIManager.getIcon("OptionPane.informationIcon")
		             	);
		             	messagewindow.addButton(
		             		UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
		             		new ActionListener() {
		             		    @Override
		             		    public void actionPerformed(ActionEvent e) {
		             		    	// close this window
		        			    	messagewindow.dispose();
		             		    }
		             		}
		             	);
		             	messagewindow.setVisible(true);
	            		settlersLobby.getGUI().s3PanelDialogObject.loadMaps();
            		}
        		}
        		else
        		{
            		// etwas ist schief gegangen, öffne
            		// temporäres Verzeichnis und Zielverzeichnis,
            		// damit der User entscheiden kann, was zu tun ist
        			if( false == nomessage ) {
	            		if (displayErrorAndConfirm(getMessage(ERROR_KOPIEREN)))
	            		{
	                		openFileLocation(new File(dir, extractedFile.getName()));
	                		openFileLocation(extractedFile);
	            		}
        			}
            		//sollte der User die Downloaddateien löschen, so kann es später
            		//gelöscht werden. Sollten dort noch Dateien vorhanden sein
            		//schlägt das Löschen fehl.
            		tmpAlobbyDir.deleteOnExit();
        		}
        		return result;
            }
        }

        catch (IOException e)
        {
            return false;
        }
	}

	 /**
     * Extrahiert die Karte aus dem Archiv.
     * Es werden .zip und .rar Archive unterstützt.
     *
     * @param mapArchive
     * @return File Objekt der extrahierten Karte
     */
    protected File extractFile(File archive, String name)
    {
        String fileExtension = getExtension(archive);
        if (fileExtension.equals("rar")) //$NON-NLS-1$
        {
            return extractRarFile(archive, name);
        }
        else if (fileExtension.equals("zip")) //$NON-NLS-1$
        {
            return extractZipFile(archive, name);
        }
        else
        {
            Test.output("unknown File Format: " + fileExtension); //$NON-NLS-1$
        }
        return null;
    }

    protected File extractRarFile(File archive, String name)
    {
        if (!CheckIntegrity.checkUnrarFiles())
        {
            //die Unrar Dateien wurden manipuliert, Abbruch
            Test.output("integrity check failed"); //$NON-NLS-1$
            return null;
        }
        return extractFile(Exe.UnRAR, archive, name);
    }

    /**
     * Extrahiert eine Datei aus einem Archiv, unter Zuhilfenahme
     * einer .exe. Die Datei muss auf name matchen, dazu wird mit der
     * Funktion {@link #getRegExPattern(String)} das zu name passende
     * Pattern erzeugt.
     * @param exe
     * @param archive
     * @param name Name der zu extrahierenden Datei, für maps ohne Dateiendung
     * @return extrahierte Datei
     */
    protected File extractFile(Exe exe, File archive, String name)
    {
        if (!exe.exists())
        {
            appendError(MessageFormat.format(
                    I18n.getString("Download.ERROR_EXE_NOT_FOUND"), //$NON-NLS-1$
                    exe, exe.getExe()));
            return null;
        }
        String filenameInArchive = determineFilenameInArchive(exe, archive, name);

        if (filenameInArchive == null)
        {
            //der mapname wurde nicht im Archiv entdeckt
            return null;
        }
        try
        {
            String extractingDir = archive.getParent();
            String[] command = exe.commandLineExtractFileFromArchive(
                    archive, filenameInArchive, extractingDir);

            Process p = Runtime.getRuntime().exec(command, null, null);

            //CP850 ist die standard westeuropäische Codepage die bei DOS benutzt wird
            //zumindest in unserer Gegend
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(), "CP850")); //$NON-NLS-1$

            String line;
            // Ausgabe vom unrar Programm loggen
            while ((line = reader.readLine()) != null)
            {
                Test.output(line);
            }

            try
            {
                if (p.waitFor() != 0)
                {
                    Test.output("exit value = " + p.exitValue()); //$NON-NLS-1$
                    appendError(MessageFormat.format(
                            I18n.getString("Download.ERROR_RETURNCODE"), exe, p.exitValue())); //$NON-NLS-1$
                }
            }
            catch (InterruptedException e)
            {
                Thread.currentThread().interrupt();
                Test.outputException(e);
            }

            // Es wird überprüft, ob eine Datei extrahiert wurde,
            // die zu unserem gesuchten Kartennamen passt.
            // Auch wenn inzwischen nur noch maximal eine Datei extrahiert wird,
            // bleibt der allgemeingültige Code erstmal drin
            File result = null;
            Pattern pattern = getRegExPattern(name);
            for (File file : archive.getParentFile().listFiles())
            {
                if (file.isDirectory())
                {
                    continue;
                }
                if (isExpectedFile(pattern, file.getName()))
                {
                    result = file;
                    break;
                }
            }
            if (result == null)
            {
                //keine der Dateien passte auf das gewünschte pattern
                appendError(I18n.getString("Download.ERROR_FILE_NOT_IN_ARCHIVE")); //$NON-NLS-1$
            }
            return result;
        }
        catch (Exception e)
        {
            appendError(e.toString());
            Test.outputException(e);
        }
        return null;
    }

    /**
     * erzeugt ein passende Pattern anhand des Kartennamens.
     * Das Pattern ist für die Funktion {@link #isExpectedFile(Pattern, String)}
     * gedacht.
     *
     * @param name
     * @return
     */
    protected abstract Pattern getRegExPattern(String name);


    /**
     * überprüft, ob der Dateiname konform zum pattern ist.
     *
     * @param pattern
     * @param filename
     * @return
     */
    private boolean isExpectedFile(Pattern pattern, String filename)
    {
        return pattern.matcher(filename).matches();
    }

    /**
     * Extracts the map-file from zip-archive if the filename matches
     * the mapName.
     * Uses different charset for detecting the files in the zip-archive.
     *
     * @param mapArchive
     * @param mapName
     * @return extracted file, null if error occured
     */
    protected File extractZipFile(File archive, String name)
    {
        ZipFile zipFile = null;
        try
        {
        	// create list with charsets for the zip-archive
        	List<String> charsetList = new ArrayList<String>();
        	charsetList.add("Cp437");
        	charsetList.add("UTF-8");
        	
        	// loop through the charsets-list
        	for (int i = 0; i < charsetList.size(); i++) {
        		
        		// open the zip-archive with actual charset
        		zipFile = new ZipFile(archive, Charset.forName(charsetList.get(i)));
        		
        		// get the entrys
                Enumeration<? extends ZipEntry> zipEntryEnum = zipFile.entries();

                // set the pattern to search for
                Pattern pattern = getRegExPattern(name);

                // loop through the zip-archive-entrys
                while ( zipEntryEnum.hasMoreElements() )
                {
                	// get the entry
                    ZipEntry zipEntry = zipEntryEnum.nextElement();
                    
                    Test.output( zipEntry.getName() + " -> " + pattern);
                    
                    // only extract the file with matching filename
                    if (isExpectedFile(pattern, zipEntry.getName()))
                    {
                        return extractZipEntry( zipFile, zipEntry, archive.getParent());
                    }
                }
        	}

            // if no filename matched show a warning to the user
            appendError(I18n.getString("Download.ERROR_FILE_NOT_IN_ARCHIVE")); //$NON-NLS-1$
        }
        catch ( Exception e )
        {
            appendError(e.toString());
        }
        finally
        {
            if (zipFile != null)
            {
                try
                {
                    zipFile.close();
                }
                catch (IOException e)
                {
                }
            }
        }
        return null;
    }

    /**
     * entpackt das komplette Archiv und gibt die erzeugten File-Objekte zurück.
     * Tritt dabei ein irgendein Fehler auf, so wird null zurückgegeben und es
     * wird versucht alle bis dahin extrahierten Dateien wieder zu löschen.
     * @param archive zipArchiv
     * @return
     */
    protected List<File> extractEntireZipFile(File archive)
    {
        List<File> files = new ArrayList<File>();
        ZipFile zipFile = null;
        try
        {
            zipFile = new ZipFile(archive);
            Enumeration<? extends ZipEntry> zipEntryEnum = zipFile.entries();

            while ( zipEntryEnum.hasMoreElements() )
            {
                ZipEntry zipEntry = zipEntryEnum.nextElement();
                Test.output(zipEntry.getName() + "." ); //$NON-NLS-1$
                File extractedEntry = extractZipEntry(zipFile, zipEntry, archive.getParent());
                if (extractedEntry == null)
                {
                    appendError(MessageFormat.format(
                            I18n.getString("Download.ERROR_DURING_EXTRACTION"), zipEntry.getName())); //$NON-NLS-1$
                    deleteFiles(files); //bisherige extrahierte files löschen
                    if (zipFile != null) {
                    	try {
                    		zipFile.close();
            			} catch (IOException e) {
            				Test.outputException(e);
            			}
                    }
                    return null;
                }
                files.add(extractedEntry);
            }
        }
        catch (Exception e)
        {
            appendError(e.toString());
            deleteFiles(files); //bisherige extrahierte files löschen
            if (zipFile != null) {
            	try {
            		zipFile.close();
    			} catch (IOException e2) {
    				Test.outputException(e2);
    			}
            }
            return null;
        }
        //komplette Liste zurückgeben, kann theoretisch auch leer sein, aber zumindest
        //nicht null
        if (zipFile != null) {
        	try {
        		zipFile.close();
			} catch (IOException e) {
				Test.outputException(e);
			}
        }
        return files;
    }

    /**
     * entpackt einen ZipEntry aus dem ZipFile in das Zielverzeichnis destDir.
     *
     * @param zipfile
     * @param entry
     * @param destDir
     * @throws IOException
     */
    protected File extractZipEntry(ZipFile zipfile, ZipEntry entry,
            String destDir) throws IOException
    {
        File file = new File(destDir, entry.getName());

        if (entry.isDirectory())
        {
            file.mkdirs();
        }
        else
        {
            new File(file.getParent()).mkdirs();

            InputStream is = null;

            try
            {
                is = zipfile.getInputStream(entry);
                Files.copy(is, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
            finally
            {
                is.close();
            }
        }
        return file;
    }

    /**
     * liefert die Dateiendung ohne '.'.
     * @param file
     * @return
     */
    public static String getExtension(File file)
    {
        String extension = null;
        String s = file.getName();
        int index = s.lastIndexOf('.');

        if (index > 0 &&  index < s.length() - 1) {
            extension = s.substring(index+1).toLowerCase();
        }
        return extension;
    }


	/**
     * verschiebt mapfile vom aktuellen Ort in das Verzeichnis userMapDir.
	 * @param nomessage 
     * @param mapfile Datei, die verschoben werden soll
     * @param userMapDir Zielverzeichnis
     * @return true falls das Verschieben erfolgreich war
     */
    private boolean moveFileToDir(File file, File dir, boolean nomessage)
    {
        Path source = file.toPath();
        Path destination = dir.toPath().resolve(source.getFileName());
        if( !this.overwriteFileWithoutHint && Files.exists(destination))
        {
        	if( false == nomessage ) {
        		appendError(I18n.getString("Download.ERROR_FILE_ALREADY_EXISTS")); //$NON-NLS-1$
        	}
            return false;
        }
        try
        {
            Files.move(source, destination, StandardCopyOption.REPLACE_EXISTING);
        }
        catch (Exception e)
        {
            Test.outputException(e);
            return false;
        }
        return true;
    }

	/**
     * öffnet ein Explorer Fenster und zeigt file an.
     *
     * @param file
     * @return
     */
    public boolean openFileLocation(File file)
    {
        if (!file.exists())
        {
            return false;
        }
        try
        {
//            Process p = Runtime.getRuntime().exec(
//                    "rundll32 url.dll,FileProtocolHandler " + file.getAbsolutePath(), null, null);
            Process p = Runtime.getRuntime().exec(
                    "explorer /e,/select," + file.getAbsolutePath(), null, null); //$NON-NLS-1$
            return p.waitFor() == 0;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    /**
     * finde die zugehörige Download URL für den übergebenen Dateinamen.
     * Die URL wird vom Server mit der übergebenen serverURL geliefert.
     *
     * @param serverURL server bei dem die URL angefragt wird
     * @param fileName Dateiname der Datei, die man herunterladen möchte
     * @return Download URL für den übergebenen Dateinamen
     */
	protected abstract URL getDownloadURL(String serverURL, String fileName);



	/**
	 * ermittelt aus einer URL einen Dateinamen (das Ende der URL).
	 *
	 * @param url
	 * @return
	 */
	private String getFileNameFromURL(URL url)
	{
        String filename = "dummy.zip"; //$NON-NLS-1$

        // die URL kann z.B. %20 enthalten. Um einen sinnvollen
        // Dateinamen zu bekommen, muss die URL dekodiert werden.
        // Die URI Funktion getPath() tut dies zumindest für den
        // hinteren Teil der URL, da nur dieser für den Dateinamen
        // relevant ist, ist die Funktion ausreichend
        String decodedUrlWithoutHost;
        try
        {
            decodedUrlWithoutHost = url.toURI().getPath();
            //die decode Funktion der URI Klasse erwartet UTF-8 kodierte
            //Eingaben. Sollte die URL aber anders kodiert sein,
            //kann der output für nicht erkannte Zeichen den Unicode
            //replacement character \uFFFD enthalten.
            //Ersetze diesen durch '_', damit zumindest ein gültiger
            //Dateiname entsteht, wenn es auch nicht der originale ist
            decodedUrlWithoutHost = decodedUrlWithoutHost.replace('\uFFFD', '_');
            filename = new File(decodedUrlWithoutHost).getName();
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
        return filename;
	}
    /**
     * lädt die Datei, die unter der URL referenziert ist herunter und
     * speichert sie wenn möglich unter demselben Dateinamen im Verzeichnis
     * {@code directory} ab.
     * @param url
     * @param directory
     * @return das resultierende File Objekt der heruntergeladenen Datei
     */
	protected File downloadFileToDir(URL url, File directory)
    {
        boolean deleteResultFile = false;

        File result = null;
        if (!directory.exists())
        {
            directory.mkdir();
        }

        URLConnection conn = null;
        ProgressMonitorInputStream in = null;
        try
        {
            conn = url.openConnection();
            int totalLength = conn.getContentLength();
            //conn.getURL() liefert die aktuelle URL, die sich durch eventuelle
            //redirects von der initial übergebenen URL unterscheiden kann
            //(erst nachdem durch conn.getContentLength() alle Header
            // gelesen wurden (dadurch auch der redirect))
            String filename = getFileNameFromURL(conn.getURL());
            result = new File(directory, filename);

            in = new ProgressMonitorInputStream(
                        null,
                        MessageFormat.format(
                                I18n.getString("Download.MSG_DOWNLOADING"), //$NON-NLS-1$
                                result.getName()),
                        conn.getInputStream());
            if (totalLength > 0)
            {
                // Die Gesamtlänge muss manuell gesetzt werden,
                // da sonst intern available() abgefragt wird,
                // das einen anderen Wert liefert
                in.getProgressMonitor().setMaximum(totalLength);
                // Das Dialogfenster soll sofort erscheinen,
                // daher wird setProgress(0) manuell aufgerufen,
                // falls eventuell mal der tatsächliche download hängt
                in.getProgressMonitor().setMillisToDecideToPopup(0);
                in.getProgressMonitor().setMillisToPopup(0);
                in.getProgressMonitor().setProgress(0);
            }
            long numWritten = Files.copy(in, result.toPath(), StandardCopyOption.REPLACE_EXISTING);
            Test.output(result + "\t" + numWritten); //$NON-NLS-1$
        }
        catch (InterruptedIOException ie)
        {
            //die Datei wurde nicht komplett gelesen, also kann sie gelöscht werden
            deleteResultFile = true;
            throw new UserAbortException();
        }
        catch (Exception e)
        {
            // was auch immer schief gegangen ist, die Datei ist nicht komplett
            // und damit nutzlos
            deleteResultFile = true;
            appendError(e.toString());
            Test.outputException(e);
        }
        finally
        {
            try
            {
                if (in != null)
                {
                    in.close();
                }
                //Das Löschen darf erst passieren, nachdem out geschlossen ist,
                //anderfalls wird die Datei nicht gelöscht.
                //daher der Umweg über das Flag "deleteResultFile"
                if (deleteResultFile && result != null)
                {
                    deleteFile(result);
                    result = null;
                }
            }
            catch (IOException ioe)
            {
            }
        }
        return result;
    }

	/**
     * Ermittelt aus dem S3.exe Pfad den zugehörigen Pfad des Usermap
     * Verzeichnisses und gibt ihn als File-Objekt zurück
     * @param path
     * @return
     */
    protected abstract File getDestinationDirectory(String path);


    /**
     * Liefert angepasste Nachrichten von den jeweiligen Unterklassen
     *
     * @param message kann folgende Nachrichten enthalten:
     * Error:Server;
     * Error:Verzeichnis;
     * Error:temporäres Verzeichnis;
     * Error:herunterladen;
     * Error:entpacken;
     * Error:kopieren;
     * Meldung:erfolgreich;
     * Meldung:downloading;
     * @return
     */
    protected abstract String getMessage(String message);

    protected abstract String getDownloadPath();


    /**
     * fügt eine interne Fehlermeldung zu {@code MapDownload#errorMessage}
     * hinzu.
     *
     * @param message
     */
    protected void appendError(String message)
    {
        errorMessage.append(message);
        errorMessage.append('\n');
    }

    /**
     * Zeigt eine Fehlermeldung in der GUI an. Zusätzlich zur
     * eigentlichen Nachricht werden noch interne Fehlermeldungen
     * angezeigt, die in {@code MapDownload#errorMessage} gespeichert sind.
     * @param message
     */
    public void displayError(String message)
    {
        if (errorMessage.length() > 0)
        {
            message += "\n" + errorMessage; //$NON-NLS-1$
            errorMessage.delete(0, errorMessage.length());
        }
        if (gui != null)
        {
            message = message.replace("\n", "<br>"); //$NON-NLS-1$ //$NON-NLS-2$
            gui.displayError(message, false);
        }
        else
        {
            Test.output(message);
        }
    }

    protected boolean displayErrorAndConfirm(String message)
    {
        if (errorMessage.length() > 0)
        {
            errorMessage.append(message);
            message = errorMessage.toString();
            errorMessage.delete(0, errorMessage.length());
        }
        if (gui != null)
        {
            return gui.displayConfirmDialog(I18n.getString("Download.DIALOG_TITLE3"), message); //$NON-NLS-1$
        }
        else
        {
            Test.output(message);
            return false;
        }
    }

    /**
     * Ruft eine Liste der im Archiv vorhandenen Dateien ab und
     * versucht anhand des Mapnamen die passende Datei im Archiv zu ermitteln.
     * Zurückgegeben wird der passende Dateiname im Archiv.
     * @param archive
     * @param name
     * @return Dateinamen im Archiv, der zu name passt, null falls kein passender
     * gefunden wird.
     */
    private String determineFilenameInArchive(Exe exe, File archive, String name)
    {
        String resultFilename = null;
        try
        {
            Pattern pattern = getRegExPattern(name);
            String[] command = exe.commandLineListArchive(archive);

            Process p = Runtime.getRuntime().exec(command, null, null);

            //CP850 ist die standard westeuropäische Codepage die bei DOS benutzt wird
            //zumindest in unserer Gegend
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(), "CP850")); //$NON-NLS-1$
            String line;

            // Ausgabe vom Programm parsen
            while ((line = reader.readLine()) != null)
            {
                Test.output(line);
                //Sobald der erste passende Dateiname gefunden wird,
                //wird nicht mehr weiter gesucht, nur noch geloggt
                String filename = exe.getFilenameFromListOutputLine(line);
                if (resultFilename == null && filename != null && isExpectedFile(pattern, filename))
                {
                    resultFilename = filename.trim();
                }
            }

            try
            {
                if (p.waitFor() != 0)
                {
                    Test.output("exit value = " + p.exitValue()); //$NON-NLS-1$
                }
            }
            catch (InterruptedException e)
            {
                Thread.currentThread().interrupt();
                Test.outputException(e);
            }
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
        return resultFilename;

    }

    /**
     * wird aufgerufen, wenn bei downloadFile ein Fehler beim
     * Entpacken auftrat.
     * @param archiveFile
     */
    protected void onExtractError(File archiveFile)
    {
        displayError(getMessage(ERROR_ENTPACKEN));
    }

    private boolean deleteFile(File file)
    {
        try
        {
            return file.delete();
        }
        catch (Exception e)
        {
            return false;
        }
    }


    /**
     * versucht alle Dateien und Verzeichnisse aus {@code files} zu löschen.
     * Enthält ein Verzeichnis weitere Unterverzeichnisse oder Dateien, die nicht
     * in {@code files} enthalten sind, so wird es nicht gelöscht.
     *
     * @param files
     */
    protected static void deleteFiles(List<File> files)
    {
        ArrayList<File> tmpList = new ArrayList<File>(files);
        for (Iterator<File> iter = tmpList.iterator(); iter.hasNext(); )
        {
            File file = iter.next();
            try
            {
                if (file.exists())
                {
                    if (file.delete())
                    {
                        iter.remove();
                    }
                }
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }
        //im Normalfall sollten in tmpList jetzt höchstens noch Verzeichnisse
        //sein, die bisher nicht gelöscht werden konnten, da sie noch Dateien
        //enthielten. (Ausnahme Dateien, die generell nicht gelöscht werden können,
        //da sie noch von irgendwem geöffnet sind).
        //laufe noch mal vorwärts und rückwärts drüber, um hoffentlich auch diese
        //Verzeichnisse noch zu löschen.
        //Dieser ganze Aufwand wird getrieben, da die Dateien eventuell in eine
        //bestehende Dateistruktur entpackt wurden. Daher sollen Dateien, die
        //vorher schon vorhanden waren, natürlich erhalten bleiben.
        ListIterator<File> iter2 = tmpList.listIterator();
        while (iter2.hasNext())
        {
            File file = iter2.next();
            try
            {
                if (file.exists())
                {
                    if (file.delete())
                    {
                        iter2.remove();
                    }
                }
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }
        //rückwärts
        while (iter2.hasPrevious())
        {
            File file = iter2.previous();
            try
            {
                if (file.exists())
                {
                    if (file.delete())
                    {
                        iter2.remove();
                    }
                }
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }


    }

    protected class UserAbortException extends RuntimeException
    {
        private static final long serialVersionUID = 1L;

        public UserAbortException()
        {
            super();
        }
    }
    
    /**
     * Setting wether to overwrite an existing file or not
     */
    public void overwriteFileWithoutHint( boolean overwriteFileWithoutHint ) {
    	this.overwriteFileWithoutHint = overwriteFileWithoutHint;
    }
}
