package net.siedler3.alobby.controlcenter.download;

import java.net.URL;

import javax.swing.SwingWorker;

import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.UpdateSwingWorker;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.util.UrlHelper;

public class DownloadActions
{
    //es wird ein SwingWorker benutzt, allerdings wird er momentan
    //eher wie ein normaler Thread benutzt.
    //Aber vielleicht ändert sich das noch
    private static SwingWorker<Boolean , Boolean> automaticDownloadWorker;
    private static SwingWorker<Boolean , Boolean> autoUpdateWorker;
    
    public static void startAutomaticMapDownload(final SettlersLobby settlersLobby, final String mapName)
    {
        if (automaticDownloadWorker != null)
        {
            // es läuft noch ein download im Hintergrund,
            // solange der nicht fertig ist, wird kein neuer download
            // gestartet
            if (!automaticDownloadWorker.isDone())
            {
                return;
            }
        }
        //einen neuen backgroundthread anlegen und starten
        automaticDownloadWorker = new SwingWorker<Boolean, Boolean>()
        {
            @Override
            protected Boolean doInBackground()
            {
                // der Returnwert wird nirgendwo abgefragt,
                // Hauptsache das downloaden wird durchgeführt
                return new MapDownload(settlersLobby).downloadFile(mapName);
            }
        };
        automaticDownloadWorker.execute();
    }
    
    public static void startAutomaticMapDownload(final SettlersLobby settlersLobby, final String mapName, OpenGame openGame)
    {
        if (automaticDownloadWorker != null)
        {
            // es läuft noch ein download im Hintergrund,
            // solange der nicht fertig ist, wird kein neuer download
            // gestartet
            if (!automaticDownloadWorker.isDone())
            {
                return;
            }
        }
        //einen neuen backgroundthread anlegen und starten
        automaticDownloadWorker = new SwingWorker<Boolean, Boolean>()
        {
            @Override
            protected Boolean doInBackground()
            {
                // der Returnwert wird nirgendwo abgefragt,
                // Hauptsache das downloaden wird durchgeführt
                return new MapDownload(settlersLobby).downloadFile(mapName, openGame);
            }
        };
        automaticDownloadWorker.execute();
    }
    
    /**
     * führt das AutoUpdate in einem Hintergrundthread aus.
     * Aktuell wird dazu ein SwingWorker benutzt.
     * @param showNoUpdateAvailableMessage
     *         True, wenn bei keinem neuen Update eine Nachricht angezeigt werden
     *         soll, andernfalls false.
     */
    public static void startAutoUpdateInBackground(final SettlersLobby settlersLobby, boolean showMessageNoUpdateAvailable)
    {
        if (autoUpdateWorker != null)
        {
            // es läuft noch ein AutoUpdate im Hintergrund,
            // solange der nicht fertig ist, wird kein neuer download
            // gestartet
            if (!autoUpdateWorker.isDone())
            {
                return;
            }
        }
        //einen neuen backgroundthread anlegen und starten
        autoUpdateWorker = new UpdateSwingWorker(settlersLobby, showMessageNoUpdateAvailable);
        autoUpdateWorker.execute();
    }

    public static void openManualMapDownload(String mapName)
    {
        try
        {
            Configuration config = Configuration.getInstance();
            String encodedMapName = UrlHelper.urlEncode(mapName);
            URL url = new URL(config.getMapDownloadPath() + encodedMapName);
            GUI.openURL(url.toURI());
        } 
        catch (Exception e)
        {
            Test.outputException(e);
        }
    }

}
