/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

/**
 * Kapselt die Unterscheidung zwischen internem und 
 * extern sichtbarem Kartennamen.
 * Bei normalen Maps sind interner und externer Name
 * gleich, aber bei Random-maps und savegames gibt es
 * unterschiedliche Darstellungen.
 * 
 * @author jim
 *
 */
public interface ISMap
{
    /**
     * Liefert die interne Darstellung des MapNamen,
     * normalerweise *.map oder *.mps im Falle eines
     * Savegames.
     * @return
     */
    public String toString();
    
    /**
     * Liefert den String, der in der GUI bei der Mapauswahl angezeigt werden soll.
     * @return
     */
    public String toDisplayString();
    
    /**
     * Liefert true, falls die Karte in einem korrekten Format angegeben ist.
     * Es wird nicht geprüft, ob die Mapdatei tatsächlich existiert.
     * 
     * @return true, falls die Karte in einem korrekten Format angegeben ist.
     */
    public boolean isValid();

	public int getPlayer();

	public int getWidthHeight();

}
