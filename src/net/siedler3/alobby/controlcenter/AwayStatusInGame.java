/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.controlcenter;

import net.siedler3.alobby.communication.IRCCommunicator;

public class AwayStatusInGame extends AwayStatus
{
	private AwayByAbstinence awayByAbstinence;
	
	protected AwayStatusInGame(SettlersLobby settlersLobby, IRCCommunicator ircCommunicator, boolean isUserAway)
	{
		super(settlersLobby, ircCommunicator, isUserAway);		
		super.doAway(MESSAGE_AWAY_IN_GAME);
		awayByAbstinence = null;
		super.isUserAway = true;
	}

	protected void doIdleAway()
	{
		super.doAway(MESSAGE_AWAY_IN_GAME);	
		awayByAbstinence = null;
		isUserAway = true;
	}
	
	protected void doAway(String msg)
	{
		super.doAway(msg);
		if (awayByAbstinence != null)
		{
			awayByAbstinence.interrupt();
		}
		AwayStatusManual aS = new AwayStatusManual(settlersLobby, ircCommunicator, true);
		aS.setGameOpen(true);		
		settlersLobby.setAwayStatus(aS);
	}
	
	protected void doBack()
	{
		super.doBack();
    	if (settlersLobby.isActivateAwayOnGameStart())
    	{
    	    //nur einen neuen AwayByAbstinenceThread anlegen, wenn noch
    	    //keiner da ist. Tippt man mehrmals hintereinander /back, ist
    	    //schon einer vorhanden.
            if (awayByAbstinence != null && awayByAbstinence.isAlive())
            {
                awayByAbstinence.userWasActive();
            }
            else
            {
                //trotz doBack läßt man den AwayStatusInGame aktiv, wechselt also nicht
                //nach AwayStatusManual. Dies passiert erst, wenn das Spiel tatsächlich
                //beendet wird, in onGameClosed().
                awayByAbstinence = new AwayByAbstinence(settlersLobby.config, this);
            }
    	}
	}
	
	public void onGameClosed()
	{
		super.doBack();
		if (settlersLobby.isActivateAwayOnIdle())
		{	
			settlersLobby.setAwayStatus(new AwayStatusAutomatic(settlersLobby, ircCommunicator, isUserAway));
		} else {
			settlersLobby.setAwayStatus(new AwayStatusManual(settlersLobby, ircCommunicator, isUserAway));
		}
	}
	
	protected void destroy()
	{
		if (awayByAbstinence != null)
		{
			awayByAbstinence.interrupt();
		}
	}
	
    protected void userWasActive()
    {
        //Falls ein AwayByAbstinence Thread aktiv ist, so wird sein Zeitpunkt
        //aktualisiert, damit er nicht unversehens zuschlägt.
        //Man führt aber kein doBack() aus, ändert also nicht den awaystatus.
        if (awayByAbstinence != null)
        {
            awayByAbstinence.userWasActive();
        }
    }
}
