/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.xmlcommunication;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * XMLFiles erzeugt und cached XMLCommunicators.
 * 
 * @author Stephan Bauer
 * @version 1.0
 */
public class XMLFiles
{
	/**
	 * Die Instanz des Singletons.
	 * 
	 * @since 1.0
	 */
	static private XMLFiles instance;
	/**
	 * Dieser Vector speichert Dateinamen.
	 * 
	 * @since 1.0
	 */
	private Vector<String> cacheFileNames;
	/**
	 * Dieser Vector speichert die erzeugten XMLCommunicators.
	 * 
	 * @since 1.0
	 */
	private Vector<XMLCommunicator> cacheXMLCommunicators;

	/**
	 * Der Konstruktor ist unsichtbar, weil es sich hier um ein Singleton handelt.
	 * 
	 * @since 1.0
	 */
	private XMLFiles()
	{
		cacheXMLCommunicators = new Vector<XMLCommunicator>();
		cacheFileNames = new Vector<String>();
	}

	/**
	 * Gibt die einzige Instanz der Klasse zurück.
	 * 
	 * @return Die einzige Instanz.
	 * @since 1.0
	 */
	public static XMLFiles getInstance()
	{
		if (instance == null)
		{
			instance = new XMLFiles();
		}
		return instance;
	}

	/**
	 * Gibt den entsprechenden XMLCommunicator zum angegebenen Dateinamen wieder. Cached die XMLCOmmunicatoren.
	 * 
	 * @param fileName
	 *            Der XML Dateiname.
	 * @return Der entsprechende XMLCommunicator.
	 * @throws FileNotFoundException
	 *             Die Nachricht der FileNotFoundException ist der übergebene Dateiname.
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @since 1.0
	 */
	public XMLCommunicator getXMLCommunicatorOfFile(String fileName)
			throws ParserConfigurationException, SAXException, IOException
	{
		XMLCommunicator xmlCommunicator = getCached(fileName);

		if (xmlCommunicator == null)
		{
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = documentBuilderFactory
					.newDocumentBuilder();
			Document document;
			try
			{
				document = builder.parse(new File(fileName));
			} catch (FileNotFoundException e)
			{
				throw new FileNotFoundException(fileName);
			}
			xmlCommunicator = new XMLCommunicator(document.getDocumentElement());
			cacheFileNames.add(fileName);
			cacheXMLCommunicators.add(xmlCommunicator);
		}
		return xmlCommunicator;
	}
	
    public static XMLCommunicator getXMLCommunicatorOfStream(InputStream inputStream) 
            throws ParserConfigurationException, SAXException, IOException
    {
        XMLCommunicator xmlCommunicator;
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
        Document document = builder.parse(inputStream);
        xmlCommunicator = new XMLCommunicator(document.getDocumentElement());
        return xmlCommunicator;
    }

	/**
	 * Returns the cached XMLCOmmunicator if it was created already.
	 * 
	 * @param fileName
	 *            The name of the XML file.
	 * @return The cached XMLCommunicator.
	 * @since 1.0
	 */
	private XMLCommunicator getCached(String fileName)
	{
		int i = cacheFileNames.indexOf(fileName);
		if (i == -1)
		{
			return null;
		} else
		{
			return (XMLCommunicator) cacheXMLCommunicators.elementAt(i);
		}
	}
	
	/**
	 * erzeugt eine Xml Datei, die nur den Wurzelknoten "config" enthält.
	 * Darin können alle benötigten Optionen gespeichert werden.
	 * 
	 * @param filename
	 */
	public static void createInitialFile(String filename)
	{
	    try
	    {
	        Document doc = createDomDocument();
	        doc.appendChild(doc.createElement("config"));
	        new XMLCommunicator(doc.getDocumentElement()).saveInXMLFile(filename);
	    }
	    catch (Exception e) {
        }
	}
	
	/**
	 * erzeugt ein leeres DOM Dokument.
	 * @return
	 */
    public static Document createDomDocument() {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.newDocument();
            return doc;
        } catch (ParserConfigurationException e) {
        }
        return null;
    }
}
