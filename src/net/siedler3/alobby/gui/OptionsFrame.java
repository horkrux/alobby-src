/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import net.siedler3.alobby.controlcenter.ALobbyTheme.ALobbyThemes;
import net.siedler3.alobby.controlcenter.BuddyHandling;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.download.DownloadActions;
import net.siedler3.alobby.gui.additional.FileChooser;
import net.siedler3.alobby.gui.additional.IntegerPlainDocument;
import net.siedler3.alobby.gui.additional.LanguageComboBox;
import net.siedler3.alobby.gui.additional.MouseWheelListenerForJTabbedPane;
import net.siedler3.alobby.gui.additional.WidthTextField;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.LocaleChanger;
import net.siedler3.alobby.util.S3Support;

/**
 * Eine View für die vielen Einstellungsmöglichkeiten in der aLobby
 *
 * @author Stephan Bauer (aka maximilius)
 *
 * Wenn eine Komponente hinzugefügt wird (Hinweis: die 3 X stehen für die Registrierkarte)
 * - in cr8XXX() die Komponente erzeugen
 * - in resetXXX() die Komponente befüllen
 * - in saveXXX() die Komponente speichern
 * - in addListeners()der Komponente den entsprechenden Listener verpassen,
 *   damit der Übernehmen Button bei Veränderung enabled wird.
 * - in checkValues() eventuelle Falscheingaben abfangen
 * - in resetBackgroundColors() die Hintergrundfarbe setzen,
 *   falls sie bei checkValues() verändert wird.
 */
public class OptionsFrame extends JFrame {

    public static final Integer[] AUTOSAVE_TIMER_ITEMS = {2, 3, 5, 10, 15, 20, 30, 60};
    public static final Integer[] SAVEMANAGER_BACKUP_COUNT_ITEMS = {7, 8, 9, 10, 15, 20, 40, 60, 120, 180, 240};
    public static final Integer[] SAVEMANAGER_CLEANUP_TIME = {6, 12, 24, 48, 72, 96, 120, 144, 168};
    public static final Integer[] SAVEMANAGER_SAVE_CLEANUP_TIME = {1, 2, 3, 4, 9, 13, 26, 52, 105};
	public static final ArrayList<String> DATETIME_FORMATS = new ArrayList<String>();

	private static final int ABSTAND = 5;

	private SettlersLobby settlersLobby;
	private JButton uebernehmenButton;
	private Color inputBackgroundColor;
	private Color inputBackgroundColorError;
	private FileChooser s3exeFileChooser;
	private FileChooser soundFileChooser;

	private JLabel lblShowWhisperIngame;
	private JLabel inputURLLabel;
	private JLabel themeLabel;
	private LinkLabel deleteLabel;
	private LinkLabel changeEmailLabel;
	private JLabel lblLocale;
	private JLabel lblGameDateTime;
	private JLabel inputChannelLabel;
	private JLabel inputAwayMsgLabel;
	private JLabel inputFontSizeLabel;
	private JLabel highlightLabel;
	private JLabel blinkingLabel;
	private JLabel autoAway1Label;
	private JLabel autoAway2Label;
	private JLabel timeInChatLabel;
	private JLabel chatLogLabel;
	private JLabel saveWindowPositionLabel;
	private JLabel sortBuddysLabel;
	private JLabel sortAwayLabel;
	private JLabel sortAwayInGameLabel;
	private JLabel inputAutoSaveSoundFileLabel;
	private JLabel inputTimeoutLabel;
	private JLabel stopStoringToolLabel;
	private JLabel inputAutoSave1Label;
	private JLabel inputAutoSave2Label;
	private JLabel lblMessagesIngame;
	private JLabel lblActivateScreenSizeCheck;
	private JLabel lblActivateForegroundWindowCheck;
	private JLabel lblActivateS3StatsScreenCapture;
	private JLabel lblAutoLoadBuddys;
	private JLabel lblNotifyIfBuddyComesOnline;
	private JLabel lblBuddyUrl;
	private JLabel lblLanpartyModus;
	private JLabel lblInternetModus;
	private JLabel lblHostDirectIpGames;
	private JLabel lblHostVpnGames;
	private JLabel lblAutoVPNStart;
	private JLabel lblWarnAboutThirdPartyFirewall;
	private JLabel lblLanguage;
	private JLabel lblMapbaseUrl;
	private JLabel lblAutomaticMapDownloadPath;
	private JLabel lblAutoPlace;
	private JLabel lblBackupCount;
    private JLabel lblCleanupTime1;
    private JLabel lblCleanupTime2;
    private JLabel lblSaveCleanupTime1;
    private JLabel lblSaveCleanupTime2;
	private JLabel lblShowTeamspeak;
	private JLabel lblPlaySound;
	private JLabel lblBlinkSound;
	private JLabel lblPlaySoundWhenHighlighted;
    private JLabel lblStartSavemanager;
    private JLabel lblStartWatchdog;
    private JLabel lblAutoCompleteNicknames;
	private JTabbedPane tabbedPane;
	private JTextField inputAwayMsg;
	private JTextField inputAwayTime;
	private JTextField inputFontSize;
	private JTextField inputTimeout;
	private JTextField inputAutoSaveSoundFile;
	private JTextField inputHighlightingWords;
	private JTextField inputBlinkSound;
	private LanguageComboBox inputLanguage;
	private JComboBox<String> inputChannel;
	private JComboBox<String> inputURL;
	private JComboBox<ALobbyThemes> theme;
	private JComboBox<String> locale;
	private JComboBox<String> gameDateTimeFormat;
	private JComboBox<String> inputMapbaseUrl;
	private JComboBox<String> inputBuddyUrl;
	private JComboBox<Integer> inputAutoSaveTimer;
	private JComboBox<String> inputAutomaticMapDownloadPath;
	private JComboBox<Integer> inputBackupCount;
    private JComboBox<Integer> inputCleanupTime;
    private JComboBox<Integer> inputSaveCleanupTime;
	private JRadioButton radioLanpartyModus;
	private JRadioButton radioInternetModus;
	private JRadioButton radioHostDirectIpGames;
	private JRadioButton radioHostVpnGames;
	// checkbox for auto-vpn-start
    private JCheckBox chbAutoVPNStart;
    private JCheckBox chbWarnAboutThirdPartyFirewall;
	private JCheckBox chbIsSortBuddys;
	private JCheckBox chbIsSortAway;
	private JCheckBox chbIsSortInGame;
	private JCheckBox chbActivateAwayOnIdle;
	private JCheckBox chbActivateTimeInChat;
	private JCheckBox chbActivateAutoChatLogging;
	private JCheckBox chbActivateFlashing;
	private JCheckBox chbAutoSave;
	private JCheckBox chbAutoCompleteNicknames;
	// checkbox for highlight-audiofile
    private JCheckBox chbPlaySoundWhenHighlighted;
	private JCheckBox chbStopStoringTools;
	private JCheckBox chbDisableAutosaveSound;
	private JCheckBox chbActivateScreenSizeCheck;
	private JCheckBox chbActivateForegroundWindowCheck;
	private JCheckBox chbActivateS3StatsScreenCapture;
	private JCheckBox chbAutoLoadBuddys;
	private JCheckBox chbNotifyIfBuddyComesOnline;
	private JCheckBox chbSaveWindowPosition;
	private JCheckBox chbShowWhisperIngame;
	private JCheckBox chbMessagesIngame;
    private JCheckBox chbShowTeamspeak;
    private JCheckBox chbAutoPlace;
    private JCheckBox chbDoCleanup;
    private JCheckBox chbDoSaveCleanup;
    private JCheckBox chbStartSavemanager;
    private JCheckBox chbStartWatchdog;
	private JLabel emailLabel;
	private JLabel lblDisableAutosaveSound;
	private LinkLabel emailField;
	private PropertyChangeListener emailFieldChangeListener = new PropertyChangeListener() {
		public void propertyChange(PropertyChangeEvent evt) {
			if( emailField.getText().length() == 0 ) {
				emailField.setText("<html><i>" + I18n.getString("OptionsFrame.NOT_LOGGED_IN") + "</i></html>");
			}
		}
	 };
	private JCheckBox chbDelKey;
	private JLabel lblDelKey;
	private JButton btnPortchecker;
	private JButton btnReset;
	private JLabel lbls3versionnumber;
	private JComboBox<String> s3versionnumber;
	private JComboBox<String> jcbresolution;
	private JLabel lblresolution;

	/**
	 * Erzeugt das OptionsFrame
	 */
	public OptionsFrame(SettlersLobby settlersLobby, String title, Image icon) {
		super(title);
		this.settlersLobby = settlersLobby;

		// add the possible s3 datetime-formats to the list
		DATETIME_FORMATS.add("dd.MM.yyyy HH:mm:ss");
		DATETIME_FORMATS.add("MM/dd/yyyy KK:mm:ss a");
		DATETIME_FORMATS.add("dd/MM/yyyy HH:mm:ss");
		DATETIME_FORMATS.add("dd-MM-yyyy HH:mm:ss");
		DATETIME_FORMATS.add("dd.MM.yyyy HH:mm:ss");
		DATETIME_FORMATS.add("yyyy-MM-dd HH:mm:ss");
		DATETIME_FORMATS.add("dd. MM. yyyy HH:mm:ss");
		DATETIME_FORMATS.add("dd/MM/yyyy KK:mm:ss a");
		DATETIME_FORMATS.add("yyyy.MM.dd. HH:mm:ss");
		DATETIME_FORMATS.add("dd.MM.yyyy. HH:mm:ss");
		DATETIME_FORMATS.add("yyyy/MM/dd HH:mm:ss");
		
		setIconImage(icon);
		
		s3exeFileChooser = new FileChooser(this, icon);
		soundFileChooser = new FileChooser(this, icon);
		inputBackgroundColor = settlersLobby.theme.inputBoxBackgroundColor;
		inputBackgroundColorError = settlersLobby.theme.importantColor;
		setResizable(false);
		tabbedPane = new JTabbedPane();
		tabbedPane.addMouseWheelListener(new MouseWheelListenerForJTabbedPane());
		tabbedPane.setBackground(settlersLobby.theme.backgroundColor);
	
		setLayout(new BorderLayout());
		getContentPane().add(tabbedPane, BorderLayout.CENTER);
		getContentPane().add(cr8ButtonPanel(), BorderLayout.SOUTH);
		
		getContentPane().setBackground(settlersLobby.theme.lobbyBackgroundColor);
		
		tabbedPane.add(I18n.getString("OptionsFrame.TAB_GENERAL"), cr8GeneralPanel()); //$NON-NLS-1$		
		tabbedPane.add(I18n.getString("OptionsFrame.TAB_CHAT"), cr8ChatPanel()); //$NON-NLS-1$		
		tabbedPane.add(I18n.getString("OptionsFrame.TAB_S3"), cr8SiedlerPanel()); //$NON-NLS-1$
		tabbedPane.add(I18n.getString("OptionsFrame.TAB_BUDDYS"), cr8BuddyPanel()); //$NON-NLS-1$
		tabbedPane.add(I18n.getString("OptionsFrame.TAB_MAPDOWNLOAD"), cr8MapbasePanel()); //$NON-NLS-1$
		tabbedPane.add(I18n.getString("OptionsFrame.TAB_LANPARTY"), cr8LanpartyPanel()); //$NON-NLS-1$
		tabbedPane.add(I18n.getString("OptionsFrame.TAB_UPDATE"), cr8UpdatePanel()); //$NON-NLS-1$
		tabbedPane.add(I18n.getString("OptionsFrame.TAB_LANGUAGE"), cr8LanguagePanel()); //$NON-NLS-1$
		tabbedPane.add(I18n.getString("OptionsFrame.TAB_SAVEMANAGER"), cr8SaveManagerPanel()); //$NON-NLS-1$
		tabbedPane.add(I18n.getString("OptionsFrame.TAB_VPN"), cr8VpnPanel()); //$NON-NLS-1$

		addListeners();

		pack();
	}

	/**
	 * Fügt allen Komponenten einen entsprechenden Listener hinzu, damit der
	 * "Übernehmen" Button enabled wird, wenn sie sich verändern.
	 */
	private void addListeners() {
		// Für JTextField:
		DocumentListener documentListener = new DocumentListener() {
			@Override
            public void changedUpdate(DocumentEvent arg0) {
				// Tue nichts.
			}
			@Override
            public void insertUpdate(DocumentEvent arg0) {
				uebernehmenButton.setEnabled(true);
			}
			@Override
            public void removeUpdate(DocumentEvent arg0) {
				uebernehmenButton.setEnabled(true);
			}
		};
		// Für JComboBox, JCheckBox, JRadioButton:
		ItemListener itemListener = new ItemListener() {
			@Override
            public void itemStateChanged(ItemEvent arg0) {
				uebernehmenButton.setEnabled(true);
			}
		};
		
		// save window-position on specific screen on closing
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent evt)
			{
				settlersLobby.config.setScreen(evt.getComponent().getGraphicsConfiguration().getDevice().getIDstring());
			}			
		});

		inputAwayMsg.getDocument().addDocumentListener(documentListener);
		inputAwayTime.getDocument().addDocumentListener(documentListener);
		inputFontSize.getDocument().addDocumentListener(documentListener);
		inputTimeout.getDocument().addDocumentListener(documentListener);
		inputAutoSaveSoundFile.getDocument().addDocumentListener(documentListener);
		inputHighlightingWords.getDocument().addDocumentListener(documentListener);
		
		// set Listener for input-field to define a sound-file
		inputBlinkSound.getDocument().addDocumentListener(documentListener);

		inputLanguage.addItemListener(itemListener);
		chbNotifyIfBuddyComesOnline.addItemListener(itemListener);
		inputAutomaticMapDownloadPath.addItemListener(itemListener);
		inputMapbaseUrl.addItemListener(itemListener);
		inputBuddyUrl.addItemListener(itemListener);
		radioLanpartyModus.addItemListener(itemListener);
		radioInternetModus.addItemListener(itemListener);
		radioHostDirectIpGames.addItemListener(itemListener);
		radioHostVpnGames.addItemListener(itemListener);
		// Listener for auto-vpn-checkbox
		chbAutoVPNStart.addItemListener(itemListener);
		chbWarnAboutThirdPartyFirewall.addItemListener(itemListener);
		chbPlaySoundWhenHighlighted.addItemListener(itemListener);
		chbActivateAwayOnIdle.addItemListener(itemListener);
		chbActivateTimeInChat.addItemListener(itemListener);
		chbActivateAutoChatLogging.addItemListener(itemListener);
		chbActivateFlashing.addItemListener(itemListener);
		chbAutoSave.addItemListener(itemListener);
		chbStopStoringTools.addItemListener(itemListener);
		chbDisableAutosaveSound.addItemListener(itemListener);
		chbActivateScreenSizeCheck.addItemListener(itemListener);
		chbActivateForegroundWindowCheck.addItemListener(itemListener);
		chbActivateS3StatsScreenCapture.addItemListener(itemListener);
		chbAutoLoadBuddys.addItemListener(itemListener);
		inputChannel.addItemListener(itemListener);
		inputURL.addItemListener(itemListener);
		theme.addItemListener(itemListener);
		gameDateTimeFormat.addItemListener(itemListener);
		inputAutoSaveTimer.addItemListener(itemListener);
		chbIsSortBuddys.addItemListener(itemListener);
		chbIsSortAway.addItemListener(itemListener);
		chbIsSortInGame.addItemListener(itemListener);
		chbSaveWindowPosition.addItemListener(itemListener);
		chbMessagesIngame.addItemListener(itemListener);
		chbShowTeamspeak.addItemListener(itemListener);
		chbAutoCompleteNicknames.addItemListener(itemListener);
		chbDelKey.addItemListener(itemListener);
		inputBackupCount.addItemListener(itemListener);
		inputCleanupTime.addItemListener(itemListener);
        inputSaveCleanupTime.addItemListener(itemListener);
        chbDoCleanup.addItemListener(itemListener);
		chbDoSaveCleanup.addItemListener(itemListener);
		chbAutoPlace.addItemListener(itemListener);
		chbStartSavemanager.addItemListener(itemListener);
		chbStartWatchdog.addItemListener(itemListener);
        if (SettlersLobby.isBetaUsed()) {
            s3versionnumber.addItemListener(itemListener);
        }
		jcbresolution.addItemListener(itemListener);
	}

	/**
	 * Baut das Panel mit den Optionen zum Chat Teil 1.
	 * @return Das fertige Panel.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private JPanel cr8GeneralPanel() {
		JPanel panel = cr8GridBagPanel();
		panel.setBackground(settlersLobby.theme.backgroundColor);

		// configure the ComboBox to choose a theme
		// -> including check for dependents like Christmas-time
		// This must be done here becauce in Java the enumeration can not be
		// modified.
		JComboBox<ALobbyThemes> themeChooser = new JComboBox<ALobbyThemes>();
		
		// build enumeration an add each item to the combobox
        for (ALobbyThemes v : ALobbyThemes.values())
        {
        	Boolean addToList = true;
        	// do not show the Christmas theme in ComboBox
        	// if it's not between 01.12. and 31.12.
        	if( v.compareTo(ALobbyThemes.CHRISTMAS) == 0 ) {
        		addToList = settlersLobby.config.itsChristmastime();
        	}
        	// do not show the Halloween theme in ComboBox
        	// if it's not between 24.10. and 05.11.
        	if( v.compareTo(ALobbyThemes.HALLOWEEN) == 0 ) {
        		addToList = settlersLobby.config.itsHalloweentime();
        	}
        	// do not show the Easter theme in ComboBox
        	// if it's not between easter-time
        	if( v.compareTo(ALobbyThemes.EASTER) == 0 ) {
        		addToList = settlersLobby.config.itsEastertime();
        	}
        	if( addToList ) {
        		themeChooser.addItem(v);
        	}
        }
		
		panel.add(inputURL = new JComboBox<String>(), new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		panel.add(inputChannel = new JComboBox<String>(), new GridBagConstraints(2, 1, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		panel.add(inputAwayMsg = new JTextField(), new GridBagConstraints(2, 3, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		panel.add(inputFontSize = new JTextField("12"), new GridBagConstraints(2, 4, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, 0, ABSTAND, ABSTAND), 0, 0));  //$NON-NLS-1$
		panel.add(inputURLLabel = new JLabel(I18n.getString("OptionsFrame.URL_IRC_SERVER")), new GridBagConstraints(0, 0, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(inputChannelLabel = new JLabel(I18n.getString("OptionsFrame.CHANNEL")), new GridBagConstraints(0, 1, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(inputAwayMsgLabel = new JLabel(I18n.getString("OptionsFrame.DEFAULT_AWAY_MSG")), new GridBagConstraints(0, 3, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(inputFontSizeLabel = new JLabel(I18n.getString("OptionsFrame.FONTSIZE")), new GridBagConstraints(0, 4, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, ABSTAND, ABSTAND), 0, 0)); //$NON-NLS-1$

		panel.add(gameDateTimeFormat = new JComboBox(DATETIME_FORMATS.toArray()), new GridBagConstraints(2, 5, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		panel.add(lblGameDateTime = new JLabel(I18n.getString("OptionsFrame.DATETIME")), new GridBagConstraints(0, 5, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(theme = themeChooser, new GridBagConstraints(2, 7, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		panel.add(themeLabel = new JLabel(I18n.getString("OptionsFrame.ALOBBY_THEME")), new GridBagConstraints(0, 7, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		
		emailField = new LinkLabel();
		emailField.addPropertyChangeListener("text", emailFieldChangeListener);
		emailField.setText("<html><i>" + I18n.getString("OptionsFrame.NOT_LOGGED_IN") + "</i></html>");
		panel.add(emailField, new GridBagConstraints(2, 9, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		panel.add(emailLabel = new JLabel(I18n.getString("OptionsFrame.YOUR_EMAIL")), new GridBagConstraints(0, 9, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
		
		deleteLabel = new LinkLabel();
		deleteLabel.setText(MessageFormat.format(String.format("<html>%s</html>", I18n.getString("OptionsFrame.DELETE_LINK")), hexcolor));
		panel.add(deleteLabel, new GridBagConstraints(0, 11, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		
		changeEmailLabel = new LinkLabel();
		changeEmailLabel.setText(MessageFormat.format(String.format("<html>%s</html>", I18n.getString("OptionsFrame.CHANGE_EMAIL_LINK")), hexcolor));
		panel.add(changeEmailLabel, new GridBagConstraints(0, 13, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		
		// add button which starts the portchecker-window
		btnPortchecker = new JButton();
		btnPortchecker.setText(I18n.getString("OptionsFrame.BTN_PORTCHECKER"));
		btnPortchecker.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new PortcheckerWindow(settlersLobby);
			}
		});
		panel.add(btnPortchecker, new GridBagConstraints(0, 15, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		
		// add button which allows to reset all settings
		btnReset = new JButton();
		btnReset.setText(I18n.getString("OptionsFrame.BUTTON_RESET"));
		btnReset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// show hint that the complete configuration will be reset
				MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
	    		messagewindow.addMessage(I18n.getString("OptionsFrame.RESET_HINT"), UIManager.getIcon("OptionPane.infoIcon"));
	    		messagewindow.addButton(
	    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	    			new ActionListener() {
	    			    @Override
	    			    public void actionPerformed(ActionEvent e) {
	    			    	// deletes the config.xml and inform the user to restart the lobby
	    					File configFile = settlersLobby.config.getConfigFilePath();
	    					if( configFile.exists() ) {
	    						configFile.delete();
	    						
	    						MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
	    			    		messagewindow.addMessage(I18n.getString("OptionsFrame.RESET_SUCCESS"), UIManager.getIcon("OptionPane.infoIcon"));
	    			    		messagewindow.addButton(
	    			    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	    			    			new ActionListener() {
	    			    			    @Override
	    			    			    public void actionPerformed(ActionEvent e) {
	    			    			        System.exit(0);
	    			    			    }
	    			    			}
	    			    		);
	    			    		messagewindow.setVisible(true);
	    					}
	    			    }
	    			}
	    		);
	    		messagewindow.addButton(
    				I18n.getString("OpenNewS4GameDialog.BUTTON_CANCEL"),
					new ActionListener() {
	    			    @Override
	    			    public void actionPerformed(ActionEvent e) {
	    			    	messagewindow.dispose();
	    			    }
	    			}
	    		);
	    		messagewindow.setVisible(true);
			}
		});
		panel.add(btnReset, new GridBagConstraints(0, 17, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		
		inputAwayMsg.setPreferredSize(new Dimension(200, 25));
		inputFontSize.setPreferredSize(new Dimension(200, 25));
		inputURL.setEditable(true);
		inputURL.getEditor().getEditorComponent().setFont(inputURL.getEditor().getEditorComponent().getFont().deriveFont(0));
		inputChannel.setEditable(true);
		inputChannel.getEditor().getEditorComponent().setFont(inputURL.getEditor().getEditorComponent().getFont().deriveFont(0));
		inputFontSize.setDocument(new IntegerPlainDocument());
		gameDateTimeFormat.setEditable(true);
		gameDateTimeFormat.getEditor().getEditorComponent().setFont(inputURL.getEditor().getEditorComponent().getFont().deriveFont(0));

		theme.getEditor().getEditorComponent().setFont(inputURL.getEditor().getEditorComponent().getFont().deriveFont(0));
		theme.getEditor().getEditorComponent().setBackground(inputBackgroundColor);

		return panel;
	}

	/**
	 * Baut das Panel mit den Optionen zum Chat Teil 2.
	 * @return Das fertige Panel.
	 */
	private JPanel cr8ChatPanel() {
		
		// add button to search for file on local hd
		JButton btnBlinkSound = new JButton(I18n.getString("OptionsFrame.BROWSE")); //$NON-NLS-1$
		btnBlinkSound.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent arg0) {
			    soundFileChooser.show();
                if (soundFileChooser.getPath() != null)
                {
                    inputBlinkSound.setText(soundFileChooser.getPath());
                }
			}
		});
		
		// create the panel with all available chat-settings
		JPanel panel = cr8GridBagPanel();

		// create panel for checkbox with label and text-field
		JPanel pnlAwayOnIdle = new JPanel();
		// -> set background for the panel
		pnlAwayOnIdle.setBackground(settlersLobby.theme.backgroundColor);
		// -> set layout for the panel
		pnlAwayOnIdle.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		// -> set label before text-field
		pnlAwayOnIdle.add(autoAway1Label = new JLabel(I18n.getString("OptionsFrame.AUTO_AWAY1"))); //$NON-NLS-1$
		// -> set text-field
		pnlAwayOnIdle.add(inputAwayTime = new JTextField("")); //$NON-NLS-1$
		// -> set label after text-field
		pnlAwayOnIdle.add(autoAway2Label = new JLabel(I18n.getString("OptionsFrame.AUTO_AWAY2"))); //$NON-NLS-1$
		// set checkbox for activation away on idle
		panel.add(chbActivateAwayOnIdle = new JCheckBox(), new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0));
		// set checkbox for time in chat
		panel.add(chbActivateTimeInChat = new JCheckBox(), new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0));
		// set checkbox for logging
		panel.add(chbActivateAutoChatLogging = new JCheckBox(), new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0));
		// set panel for away-message
		panel.add(pnlAwayOnIdle, new GridBagConstraints(1, 0, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
		// set label for time in chat
		panel.add(timeInChatLabel = new JLabel(I18n.getString("OptionsFrame.TIME_IN_CHAT")), new GridBagConstraints(1, 1, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		// set label for logging
		panel.add(chatLogLabel = new JLabel(I18n.getString("OptionsFrame.CHATLOG")), new GridBagConstraints(1, 2, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		// set checkbox for saving window position
		panel.add(chbSaveWindowPosition = new JCheckBox(), new GridBagConstraints(0, 3, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0));
		// set label for window position
		panel.add(saveWindowPositionLabel = new JLabel(I18n.getString("OptionsFrame.SAVE_WINDOW_POSITION")), new GridBagConstraints(1, 3, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		// set checkbox for user-sort
		panel.add(chbIsSortBuddys = new JCheckBox(), new GridBagConstraints(0, 4, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(2*ABSTAND, ABSTAND, 0, ABSTAND), 0, 0));
		// set label for user-sort
		panel.add(sortBuddysLabel = new JLabel(I18n.getString("OptionsFrame.SORT_BUDDYS")), new GridBagConstraints(1, 4, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(2*ABSTAND, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		// set checkbox for sort away users to bottom
		panel.add(chbIsSortAway = new JCheckBox(), new GridBagConstraints(0, 5, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0));
		// set label for sort away users to bottom
		panel.add(sortAwayLabel = new JLabel(I18n.getString("OptionsFrame.SORT_AWAY")), new GridBagConstraints(1, 5, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		// set checkbox for sort users in game to bottom
		panel.add(chbIsSortInGame = new JCheckBox(), new GridBagConstraints(0, 6, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0));
		// set label for sort users in game to bottom
		panel.add(sortAwayInGameLabel = new JLabel(I18n.getString("OptionsFrame.SORT_AWAY_IN_GAME")), new GridBagConstraints(1, 6, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		// set label for input-field for highlighting words
		panel.add(highlightLabel = new JLabel(I18n.getString("OptionsFrame.HIGHLIGHT")), new GridBagConstraints(0, 7, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		// set input-field for highlighting words
		panel.add(inputHighlightingWords = new JTextField(), new GridBagConstraints(0, 8, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, ABSTAND, ABSTAND, ABSTAND), 0, 0));
		// set checkbox for blinking
		panel.add(chbActivateFlashing = new JCheckBox(), new GridBagConstraints(0, 9, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0));
		// set label for blinking
		panel.add(blinkingLabel = new JLabel(I18n.getString("OptionsFrame.BLINKING")), new GridBagConstraints(1, 9, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		// set checkbox to activate the highlight-sound
		panel.add(chbPlaySoundWhenHighlighted = new JCheckBox(), new GridBagConstraints(0, 10, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		// set label for field to add a highlight-sound
		panel.add(lblPlaySoundWhenHighlighted = new JLabel(I18n.getString("OptionsFrame.ACTIVATE_BLINK_SOUND")), new GridBagConstraints(1, 10, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		// set label for field to add a highlight-sound
		panel.add(lblBlinkSound = new JLabel(I18n.getString("OptionsFrame.ADDFILE_BLINK_SOUND")), new GridBagConstraints(1, 11, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		// set inputfield for path to soundfile
		panel.add(inputBlinkSound = new WidthTextField(175, 175), new GridBagConstraints(1, 12, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, ABSTAND, ABSTAND, ABSTAND), 0, 0));
		// set button for search for file on local hd
		panel.add(btnBlinkSound, new GridBagConstraints(3, 12, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(ABSTAND, ABSTAND, ABSTAND, ABSTAND), 0, 0));

		// set checkbox for Teamspeak-Window
		panel.add(chbShowTeamspeak = new JCheckBox(), new GridBagConstraints(0, 13, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		// set label for Teamspeak-Window
		panel.add(lblShowTeamspeak = new JLabel(I18n.getString("OptionsFrame.SHOW_TEAMSPEAK")), new GridBagConstraints(1, 13, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		
		// set checkbox for autocomplete-function
		panel.add(chbAutoCompleteNicknames = new JCheckBox(), new GridBagConstraints(0, 14, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		// set label for autocomplete-function
		panel.add(lblAutoCompleteNicknames = new JLabel(I18n.getString("OptionsFrame.USE_AUTOCOMPLETENICKNAMES")), new GridBagConstraints(1, 14, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$

		// set checkbox for enabling the usage of del-key to empty the complete insertmessage-field
		panel.add(chbDelKey = new JCheckBox(), new GridBagConstraints(0, 15, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		// set label for Teamspeak-Window
		panel.add(lblDelKey = new JLabel(I18n.getString("OptionsFrame.USE_DEL_KEY")), new GridBagConstraints(1, 15, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		
		// set value for input-Field for away time
		inputAwayTime.setDocument(new IntegerPlainDocument());
		
		// return the created panel
		return panel;
	}

	/**
	 * Baut das Panel mit den Optionen für Siedler3
	 * @return Das fertige Panel.
	 */
	private JPanel cr8SiedlerPanel() {
		JPanel panel = cr8GridBagPanel();

		JButton btnDurchsuchenSound = new JButton(I18n.getString("OptionsFrame.BROWSE")); //$NON-NLS-1$
		btnDurchsuchenSound.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent arg0) {
			    soundFileChooser.show();
                if (soundFileChooser.getPath() != null)
                {
                    inputAutoSaveSoundFile.setText(soundFileChooser.getPath());
                }
			}
		});

		JPanel pnlAutoSave = new JPanel();
		pnlAutoSave.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		pnlAutoSave.setBackground(settlersLobby.theme.backgroundColor);
		pnlAutoSave.add(inputAutoSave1Label = new JLabel(I18n.getString("OptionsFrame.AUTOSAVE1"))); //$NON-NLS-1$
		pnlAutoSave.add(inputAutoSaveTimer = new JComboBox<Integer>(AUTOSAVE_TIMER_ITEMS));
		pnlAutoSave.add(inputAutoSave2Label = new JLabel(I18n.getString("OptionsFrame.AUTOSAVE2"))); //$NON-NLS-1$

		// get textcolor as hexcolor to generate colored link
		String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
		
		// panel.add(inputPathLabel = new JLabel(I18n.getString("OptionsFrame.GAMEPATH_S3")), new GridBagConstraints(0, 0, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		// panel.add(inputPath = new WidthTextField(175, 175), new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		// panel.add(btnDurchsuchenPfad, new GridBagConstraints(3, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));

		panel.add(inputAutoSaveSoundFileLabel = new JLabel(I18n.getString("OptionsFrame.PATH_AUTOSAVE_SOUND")), new GridBagConstraints(0, 2, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(inputAutoSaveSoundFile = new WidthTextField(175, 175), new GridBagConstraints(2, 2, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		panel.add(btnDurchsuchenSound, new GridBagConstraints(3, 2, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		
		panel.add(chbDisableAutosaveSound = new JCheckBox(), new GridBagConstraints(0, 3, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
		panel.add(lblDisableAutosaveSound = new JLabel(I18n.getString("OptionsFrame.DISABLE_AUTOSAVE_SOUND")), new GridBagConstraints(1, 3, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		
		panel.add(inputTimeoutLabel = new JLabel(I18n.getString("OptionsFrame.TIMEOUT")), new GridBagConstraints(0, 4, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(inputTimeout = new JTextField(), new GridBagConstraints(2, 4, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		panel.add(chbStopStoringTools = new JCheckBox(), new GridBagConstraints(0, 5, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
		panel.add(stopStoringToolLabel = new JLabel(I18n.getString("OptionsFrame.STOP_TOOLS")), new GridBagConstraints(1, 5, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
	    panel.add(chbAutoSave = new JCheckBox(), new GridBagConstraints(0, 6, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
		panel.add(pnlAutoSave, new GridBagConstraints(1, 6, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
		panel.add(chbMessagesIngame = new JCheckBox(), new GridBagConstraints(0, 7, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
		panel.add(lblMessagesIngame = new JLabel(I18n.getString("OptionsFrame.MSGS_INGAME")), new GridBagConstraints(1, 7, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(chbShowWhisperIngame = new JCheckBox(), new GridBagConstraints(0, 8, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
		panel.add(lblShowWhisperIngame = new JLabel(I18n.getString("OptionsFrame.PRIVATE_MSGS_INGAME")), new GridBagConstraints(1, 8, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(chbActivateScreenSizeCheck = new JCheckBox(), new GridBagConstraints(0, 9, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
        panel.add(lblActivateScreenSizeCheck = new JLabel(I18n.getString("OptionsFrame.ACTIVATE_SCREEN_SIZE_CHECK")), new GridBagConstraints(1, 9, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
        panel.add(chbActivateForegroundWindowCheck = new JCheckBox(), new GridBagConstraints(0, 10, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
        panel.add(lblActivateForegroundWindowCheck = new JLabel(I18n.getString("OptionsFrame.ACTIVATE_FOREGROUND_WINDOW_CHECK")), new GridBagConstraints(1, 10, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
        panel.add(chbActivateS3StatsScreenCapture = new JCheckBox(), new GridBagConstraints(0, 11, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));

        if (SettlersLobby.isBetaUsed()) {
            panel.add(lbls3versionnumber = new JLabel(I18n.getString("OptionsFrame.S3VERSIONNUMBER")), new GridBagConstraints(1, 12, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
            panel.add(s3versionnumber = new JComboBox<String>(), new GridBagConstraints(2, 12, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
            // list of all actual available S3-versions
            s3versionnumber.addItem(I18n.getString("OptionsFrame.S3VERSIONNUMBER_173"));
            s3versionnumber.addItem(I18n.getString("OptionsFrame.S3VERSIONNUMBER_300"));
            s3versionnumber.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent event) {
                    if (event.getStateChange() == ItemEvent.SELECTED) {
                        JComboBox<?> selectBox = (JComboBox<?>) event.getSource();
                        boolean showHint = false;
                        if( selectBox.getSelectedItem().toString().equals(I18n.getString("OptionsFrame.S3VERSIONNUMBER_172")) && settlersLobby.config.getS3Versionnumber() != 172 ) {
                            showHint = false;
                        }
                        if( selectBox.getSelectedItem().toString().equals(I18n.getString("OptionsFrame.S3VERSIONNUMBER_173")) && settlersLobby.config.getS3Versionnumber() != 173 ) {
                            showHint = false;
                        }
                        if( selectBox.getSelectedItem().toString().equals(I18n.getString("OptionsFrame.S3VERSIONNUMBER_300")) && settlersLobby.config.getS3Versionnumber() != 300 ) {
                            showHint = false;
                        }
                        if( false != showHint ) {
                            MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
                            messagewindow.addMessage(I18n.getString("OptionsFrame.S3VERSIONNUMBER_RESTART_HINT"), UIManager.getIcon("OptionPane.infoIcon"));
                            messagewindow.addButton(
                                UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
                                new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        messagewindow.dispose();
                                    }
                                }
                            );
                            messagewindow.setSettlersLobby(settlersLobby);
                            messagewindow.setVisible(true);
                        }
                    }
                }
            });
        }

        // add option to choose resolution for game
        // -> defaults to aLobby-standard
        // -> only for beta-users
        panel.add(lblresolution = new JLabel(I18n.getString("OptionsFrame.RESOLUTION_FOR_GAME")), new GridBagConstraints(1, 13, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
        panel.add(jcbresolution = new JComboBox<String>(), new GridBagConstraints(2, 13, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
        ArrayList<String> dmList = S3Support.getAllDisplayModes(768, 1080);
        for (int i = 0; i < dmList.size(); i++) {
        	jcbresolution.addItem(dmList.get(i));
        }

        LinkLabel lblActivateS3StatsScreenCapturePreparation = new LinkLabel("<html>" + MessageFormat.format(I18n.getString("OptionsFrame.ACTIVATE_S3STATS_SCREEN_CAPTURE"),hexcolor) + "</html>");
        lblActivateS3StatsScreenCapturePreparation.setFont(lblActivateForegroundWindowCheck.getFont().deriveFont(Font.BOLD));
        panel.add(lblActivateS3StatsScreenCapture = lblActivateS3StatsScreenCapturePreparation, new GridBagConstraints(1, 11, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0)); //$NON-NLS-1$

		inputTimeout.setDocument(new IntegerPlainDocument());
		inputTimeout.setPreferredSize(new Dimension(200, 25));

		chbMessagesIngame.addItemListener(new ItemListener() {
		@Override
			public void itemStateChanged(ItemEvent arg0) {
				checkCHBMessageIngameStatus();
			}
		});

		return panel;
	}

    private Component cr8BuddyPanel() {
		JPanel panel = cr8GridBagPanel();

		JButton btnImport = new JButton(I18n.getString("OptionsFrame.BUDDYS_LOAD_NOW")); //$NON-NLS-1$
		btnImport.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {
				if (!settlersLobby.isConnectedWithServer()) {
					JOptionPane.showMessageDialog(OptionsFrame.this, I18n.getString("OptionsFrame.ERROR_BUDDYS_LOGIN_REQUIRED"), I18n.getString("OptionsFrame.ERROR_LOGIN"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					if (!isOkBuddyUrl()) {
						JOptionPane.showMessageDialog(OptionsFrame.this, I18n.getString("OptionsFrame.ERROR_BUDDYS_MISSING_URL"), I18n.getString("OptionsFrame.ERROR_INPUT"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
					} else {
						String oldUrl = settlersLobby.config.getBuddyIgnoreUrl();
						settlersLobby.config.setBuddyIgnoreUrl((String) inputBuddyUrl.getSelectedItem());
						try {
							BuddyHandling.importBuddyIgnore();
							settlersLobby.getIRCCommunicator().updateUserListInGui();
							JOptionPane.showMessageDialog(OptionsFrame.this, I18n.getString("OptionsFrame.BUDDYS_MSG_SUCCESS"), I18n.getString("OptionsFrame.SUCCESS"), JOptionPane.INFORMATION_MESSAGE);	 //$NON-NLS-1$ //$NON-NLS-2$

						} catch (Exception ex) {
							settlersLobby.config.setBuddyIgnoreUrl(oldUrl);
							JOptionPane.showMessageDialog(OptionsFrame.this, I18n.getString("OptionsFrame.ERROR_BUDDYS_CONNECTION") + ex.getMessage(), I18n.getString("OptionsFrame.ERROR_CONNECTION"), JOptionPane.ERROR_MESSAGE);	 //$NON-NLS-1$ //$NON-NLS-2$
						}
					}
				}
			}
		});

		JButton btnExport = new JButton(I18n.getString("OptionsFrame.BUDDYS_SAVE_NOW")); //$NON-NLS-1$
		btnExport.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {
				if (!settlersLobby.isConnectedWithServer()) {
					JOptionPane.showMessageDialog(OptionsFrame.this, I18n.getString("OptionsFrame.ERROR_BUDDYS_LOGIN_REQUIRED2"), I18n.getString("OptionsFrame.ERROR_LOGIN"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					if (!isOkBuddyUrl()) {
						JOptionPane.showMessageDialog(OptionsFrame.this, I18n.getString("OptionsFrame.ERROR_BUDDYS_MISSING_URL2"), I18n.getString("OptionsFrame.ERROR_INPUT"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
					} else {
						String oldUrl = settlersLobby.config.getBuddyIgnoreUrl();
						settlersLobby.config.setBuddyIgnoreUrl((String) inputBuddyUrl.getSelectedItem());
						try {
							BuddyHandling.exportBuddyIgnore();
							JOptionPane.showMessageDialog(OptionsFrame.this, I18n.getString("OptionsFrame.BUDDYS_MSG_SUCCESS2"), I18n.getString("OptionsFrame.SUCCESS"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
						} catch (Exception ex) {
							settlersLobby.config.setBuddyIgnoreUrl(oldUrl);
							JOptionPane.showMessageDialog(OptionsFrame.this, I18n.getString("OptionsFrame.ERROR_BUDDYS_CONNECTION2") + ex.getMessage(), I18n.getString("OptionsFrame.ERROR_CONNECTION"), JOptionPane.ERROR_MESSAGE);	 //$NON-NLS-1$ //$NON-NLS-2$
						}
					}
				}
			}
		});

		panel.add(lblBuddyUrl = new JLabel(I18n.getString("OptionsFrame.URL_BUDDYS")), new GridBagConstraints(0, 0, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(inputBuddyUrl = new JComboBox<String>(), new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		panel.add(btnImport, new GridBagConstraints(0, 1, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0));
		panel.add(btnExport, new GridBagConstraints(2, 1, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		panel.add(chbAutoLoadBuddys = new JCheckBox(), new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0));
		panel.add(lblAutoLoadBuddys = new JLabel(I18n.getString("OptionsFrame.BUDDYS_AUTOMATIC")), new GridBagConstraints(1, 2, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		panel.add(chbNotifyIfBuddyComesOnline = new JCheckBox(), new GridBagConstraints(0, 3, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, 0, ABSTAND), 0, 0));
		panel.add(lblNotifyIfBuddyComesOnline = new JLabel(I18n.getString("OptionsFrame.BUDDYS_BLINK")), new GridBagConstraints(1, 3, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$

		inputBuddyUrl.setEditable(true);
		inputBuddyUrl.getEditor().getEditorComponent().setFont(inputBuddyUrl.getEditor().getEditorComponent().getFont().deriveFont(0));

		return panel;
	}

	/**
	 * Baut das Panel mit den Optionen für die Mapbase
	 * @return Das fertige Panel.
	 */
	private JPanel cr8MapbasePanel() {
		JPanel panel = cr8GridBagPanel();

		panel.add(lblMapbaseUrl = new JLabel(I18n.getString("OptionsFrame.MAP_URL_MANUAL_SEARCH")), new GridBagConstraints(0, 0, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(inputMapbaseUrl = new JComboBox<String>(),  new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));
		panel.add(lblAutomaticMapDownloadPath = new JLabel(I18n.getString("OptionsFrame.MAP_URL_AUTOMATIC_DOWNLOAD")), new GridBagConstraints(0, 1, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(inputAutomaticMapDownloadPath = new JComboBox<String>(),  new GridBagConstraints(2, 1, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, 0, 0, ABSTAND), 0, 0));

		inputMapbaseUrl.setEditable(true);
		inputMapbaseUrl.getEditor().getEditorComponent().setFont(inputMapbaseUrl.getEditor().getEditorComponent().getFont().deriveFont(0));
		inputAutomaticMapDownloadPath.setEditable(true);
		inputAutomaticMapDownloadPath.getEditor().getEditorComponent().setFont(inputAutomaticMapDownloadPath.getEditor().getEditorComponent().getFont().deriveFont(0));

		return panel;
	}

	/**
	 * Erzeugt das Panel für die Lanpartyeinstellungen.
	 * @return Das erzeugte Panel.
	 */
	private JPanel cr8LanpartyPanel() {
		JPanel panel = cr8GridBagPanel();
		panel.add(radioLanpartyModus = new JRadioButton(), new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
		panel.add(radioInternetModus = new JRadioButton(), new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
		panel.add(lblLanpartyModus = new JLabel(I18n.getString("OptionsFrame.LANPARTY_MODE")), new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE,new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		panel.add(lblInternetModus = new JLabel(I18n.getString("OptionsFrame.INTERNET_MODE")), new GridBagConstraints(1, 1, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE,new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		ButtonGroup group = new ButtonGroup();
		group.add(radioLanpartyModus);
		group.add(radioInternetModus);
		return panel;
	}

	/**
	 * Erzeugt das Panel für die Updateeinstellung.
	 * @return Das erzeute Panel
	 */
	private JPanel cr8UpdatePanel()
	{
		JPanel panel = cr8GridBagPanel();

		JButton btnSearchAndInstallUpdate = new JButton(I18n.getString("OptionsFrame.AUTOUPDATE_NOW")); //$NON-NLS-1$
		btnSearchAndInstallUpdate.addActionListener(new ActionListener() {
		    @Override
            public void actionPerformed(ActionEvent ae) {
		    	//das autoupdate darf nicht im GUI-Thread laufen
		    	DownloadActions.startAutoUpdateInBackground(settlersLobby, true);
		    }
		});
		panel.add(btnSearchAndInstallUpdate, new GridBagConstraints(0, 2, 4, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0));

		return panel;
	}

	/**
	 * Erzeugt das Panel für die Spracheinstellung.
	 * @return Das erzeute Panel
	 */
	private JPanel cr8LanguagePanel()
	{
		JPanel panel = cr8GridBagPanel();

		panel.add(lblLanguage = new JLabel(I18n.getString("OptionsFrame.LANGUAGE")), new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, ABSTAND, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(inputLanguage = new LanguageComboBox(), new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, ABSTAND, 0), 0, 0));
		panel.add(lblLocale = new JLabel(I18n.getString("OptionsFrame.S3_LANGUAGE")), new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, ABSTAND, ABSTAND, ABSTAND), 0, 0)); //$NON-NLS-1$
		panel.add(locale = new JComboBox<String>(LocaleChanger.LOCALES), new GridBagConstraints(0, 3, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, ABSTAND, 0), 0, 0));

		locale.getEditor().getEditorComponent().setFont(inputURL.getEditor().getEditorComponent().getFont().deriveFont(0));
		locale.getEditor().getEditorComponent().setBackground(inputBackgroundColor);
		locale.setSelectedIndex(LocaleChanger.readLocale(settlersLobby.getS3Directory()));
		locale.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent arg0)
			{
				LocaleChanger.changeLocale(locale.getSelectedIndex(), settlersLobby.getS3Directory());
			}
		});
		
		inputLanguage.getEditor().getEditorComponent().setFont(inputBuddyUrl.getEditor().getEditorComponent().getFont().deriveFont(0));

		return panel;
	}

    private JPanel cr8SaveManagerPanel()
    {
        JPanel panel = cr8GridBagPanel();
        chbStartSavemanager = new JCheckBox();
        lblStartSavemanager = new JLabel(I18n.getString("OptionsFrame.START_SAVEMANAGER"));
        chbStartWatchdog = new JCheckBox();
        lblStartWatchdog = new JLabel(I18n.getString("OptionsFrame.START_WATCHDOG"));
//       Uncomment and alter following GridBagConstraints for GUI deactivation option:
//       panel.add(chbStartSavemanager, new GridBagConstraints(0,0,1,1,0,0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, 0, ABSTAND, ABSTAND), 0,0));
//       panel.add(lblStartSavemanager, new GridBagConstraints(1,0,2,1,0,0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, ABSTAND, ABSTAND), 0,0));
//       panel.add(chbStartWatchdog, new GridBagConstraints(0,1,1,1,0,0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, 0, ABSTAND, ABSTAND), 0,0));
//       panel.add(lblStartWatchdog, new GridBagConstraints(1,1,3,1,0,0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
        panel.add(inputBackupCount = new JComboBox<Integer>(SAVEMANAGER_BACKUP_COUNT_ITEMS), new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(ABSTAND, 0, ABSTAND, ABSTAND), 0, 0));  //$NON-NLS-1$
		panel.add(lblBackupCount = new JLabel(I18n.getString("OptionsFrame.BACKUP_COUNT")), new GridBagConstraints(0, 0, 2, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(ABSTAND, ABSTAND, ABSTAND, ABSTAND), 0, 0)); //$NON-NLS-1$
		
		JPanel pnlCleanupTime = new JPanel();
		pnlCleanupTime.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		pnlCleanupTime.setBackground(settlersLobby.theme.backgroundColor);
		pnlCleanupTime.add(lblCleanupTime1 = new JLabel(I18n.getString("OptionsFrame.CLEANUP_TIME1"))); //$NON-NLS-1$
		pnlCleanupTime.add(inputCleanupTime = new JComboBox<Integer>(SAVEMANAGER_CLEANUP_TIME)); //$NON-NLS-1$
		pnlCleanupTime.add(lblCleanupTime2 = new JLabel(I18n.getString("OptionsFrame.CLEANUP_TIME2"))); //$NON-NLS-1$
		panel.add(chbDoCleanup = new JCheckBox(), new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
		panel.add(pnlCleanupTime, new GridBagConstraints(1, 1, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));

        JPanel pnlSaveCleanupTime = new JPanel();
        pnlSaveCleanupTime.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        pnlSaveCleanupTime.setBackground(settlersLobby.theme.backgroundColor);
        pnlSaveCleanupTime.add(lblSaveCleanupTime1 = new JLabel(I18n.getString("OptionsFrame.SAVE_CLEANUP_TIME1"))); //$NON-NLS-1$
        pnlSaveCleanupTime.add(inputSaveCleanupTime = new JComboBox<Integer>(SAVEMANAGER_SAVE_CLEANUP_TIME)); //$NON-NLS-1$
        pnlSaveCleanupTime.add(lblSaveCleanupTime2 = new JLabel(I18n.getString("OptionsFrame.SAVE_CLEANUP_TIME2"))); //$NON-NLS-1$
        panel.add(chbDoSaveCleanup = new JCheckBox(), new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
        panel.add(pnlSaveCleanupTime, new GridBagConstraints(1, 2, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));

		panel.add(chbAutoPlace = new JCheckBox(), new GridBagConstraints(0, 3, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
	    panel.add(lblAutoPlace = new JLabel(I18n.getString("OptionsFrame.SAVEMANAGER_AUTO_PLACE")), new GridBagConstraints(1, 3, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));

        return panel;
    }

	/**
	 * Erzeugt das Panel für die VPN Einstellungen.
	 * @return Das erzeugte Panel.
	 */
	private JPanel cr8VpnPanel() {
		
		JButton btnCheckVPNKey = new JButton(I18n.getString("OptionsFrame.CHECK_OWN_VPN_KEY")); //$NON-NLS-1$
		btnCheckVPNKey.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {
				if (!settlersLobby.isConnectedWithServer()) {
					JOptionPane.showMessageDialog(OptionsFrame.this, I18n.getString("OptionsFrame.ERROR_LOADING_VPN"), I18n.getString("OptionsFrame.ERROR_LOGIN"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
				} else {
					settlersLobby.getVpnKeyFromServerIfNeeded( true, true );
				}
			}
		});
		
		// create the panel for vpn-settings
		JPanel panel = cr8GridBagPanel();
		// set radio-button for internetgames
		panel.add(radioHostDirectIpGames = new JRadioButton(), new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
		// set radio-button for vpn games
		panel.add(radioHostVpnGames = new JRadioButton(), new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0));
		// set label for internetgames
		panel.add(lblHostDirectIpGames = new JLabel(I18n.getString("OptionsFrame.HOST_DIRECT_IP")), new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE,new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		// set label for vpn games
		panel.add(lblHostVpnGames = new JLabel(I18n.getString("OptionsFrame.HOST_VPN")), new GridBagConstraints(1, 1, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE,new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		// create button-group based on radio-buttons
		ButtonGroup group = new ButtonGroup();
		group.add(radioHostDirectIpGames);
		group.add(radioHostVpnGames);
		
		// set checkbox for auto-vpn-start
		panel.add(chbAutoVPNStart = new JCheckBox(), new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		// set label auto-vpn-start
		panel.add(lblAutoVPNStart = new JLabel(I18n.getString("OptionsFrame.AUTO_START_VPN")), new GridBagConstraints(1, 2, 2, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		
		// set checkbox for 3rd party firewall warning
		panel.add(chbWarnAboutThirdPartyFirewall = new JCheckBox(), new GridBagConstraints(0, 3, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, ABSTAND), 0, 0)); //$NON-NLS-1$
		// set label for 3rd party firewall warning
		panel.add(lblWarnAboutThirdPartyFirewall = new JLabel(I18n.getString("OptionsFrame.THIRD_PARTY_FIREWALL")), new GridBagConstraints(1, 3, 3, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0)); //$NON-NLS-1$
		
		// check own vpn key
		panel.add(btnCheckVPNKey, new GridBagConstraints(0, 4, 4, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.BOTH, new Insets(ABSTAND, ABSTAND, 0, ABSTAND), 0, 0));
		
		// return created panel
		return panel;
	}

	
	/**
	 * Baut das Panel für die Buttons.
	 * @return Das fertige Panel.
	 */
	private JPanel cr8ButtonPanel() {
		JPanel panel = new JPanel();
		panel.setBackground(settlersLobby.theme.backgroundColor);
		JButton speichernButton = new JButton(I18n.getString("OptionsFrame.OK")); //$NON-NLS-1$
		JButton abbrechenButton = new JButton(I18n.getString("OptionsFrame.CANCEL")); //$NON-NLS-1$
		uebernehmenButton = new JButton(I18n.getString("OptionsFrame.APPLY")); //$NON-NLS-1$
		abbrechenButton.addActionListener(new ActionListener() {
			/**
			 * Der OptionFrame wird unsichtbar gemacht.
			 */
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		speichernButton.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {
				try {
					save();
					setVisible(false);
				} catch (Exception ex) {

				}
			}
		});
		uebernehmenButton.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {
				try {
					save();
				} catch (Exception ex) {

				}
			}
		});
		panel.setLayout(new GridLayout(1, 2));
		panel.add(speichernButton);
		panel.add(abbrechenButton);
		panel.add(uebernehmenButton);
		return panel;
	}

	/**
	 * Baut ein Panel mit BagGridLayout.
	 * @return Das vorbereitete Panel.
	 */
	private JPanel cr8GridBagPanel() {
		JPanel panel = new JPanel();
		panel.setBackground(settlersLobby.theme.backgroundColor);
		panel.setLayout(new GridBagLayout());
		return panel;
	}

	/**
	 * Setzt alle Werte auf die derzeitig eingestellten.
	 */
	private void reset() {
		resetChatSettings();
		resetSiedlerSettings();
		resetBuddySettings();
		resetMapbaseSettings();
		resetLanpartySettings();
        resetUpdateSettings();
        resetLanguageSettings();
        resetSaveManagerSettings();
        resetVPNSettings();
		resetBackgroundColors();
		uebernehmenButton.setEnabled(false);
		pack();
	}

    /**
	 * Setzt die Werte in der Language Registrierkarte.
	 */
	private void resetLanguageSettings() {
		inputLanguage.setSelectedLocale(settlersLobby.config.getLanguage());
	}

	/**
	 * Setzt von allen Komponenten, die bei der Fehlerbehandlung eine neue
	 * Hintergrundfarbe bekommen könnten, die Hintergrundfarben zurück.
	 */
	private void resetBackgroundColors() {
	    for (int i=0; i < tabbedPane.getTabCount(); ++i)
	    {
	        tabbedPane.getComponentAt(i).setBackground(settlersLobby.theme.backgroundColor);
	    }

	    
		inputAutoSaveSoundFile.setBackground(inputBackgroundColor);
		inputURL.getEditor().getEditorComponent().setBackground(inputBackgroundColor);
		inputChannel.getEditor().getEditorComponent().setBackground(inputBackgroundColor);
		inputAwayMsg.setBackground(inputBackgroundColor);
		inputFontSize.setBackground(inputBackgroundColor);
		inputAwayTime.setBackground(inputBackgroundColor);
		inputTimeout.setBackground(inputBackgroundColor);
		inputBuddyUrl.getEditor().getEditorComponent().setBackground(inputBackgroundColor);
		inputHighlightingWords.setBackground(inputBackgroundColor);
		gameDateTimeFormat.getEditor().getEditorComponent().setBackground(inputBackgroundColor);
		// set background-color for input-field for the blink-soundfile
		inputBlinkSound.setBackground(inputBackgroundColor);		
	}

	/**
	 * Setzt die Werte in der Chat Registrierkarte.
	 */
	private void resetChatSettings() {
		inputURL.removeAllItems();
		inputURL.addItem(settlersLobby.config.getServerURL());
		for (String serverUrl : settlersLobby.config.getDefaultServerURLs())
		{
		    if (!settlersLobby.config.getServerURL().equals(serverUrl)) {
		        inputURL.addItem(serverUrl);
		    }
		}
		inputChannel.removeAllItems();
		inputChannel.addItem(settlersLobby.config.getChannel());
        for (String channel : settlersLobby.config.getDefaultChannels())
        {
            if (!settlersLobby.config.getChannel().equals(channel)) {
                inputChannel.addItem(channel);
            }
        }

		for (int i = 0; i < gameDateTimeFormat.getItemCount(); i++) {
			if (gameDateTimeFormat.getItemAt(i).equals(settlersLobby.config.getGameDateTimeFormat())) {
				gameDateTimeFormat.setSelectedIndex(i);
				break;
			}
		}

		theme.setSelectedItem(settlersLobby.config.getTheme());
		inputAwayMsg.setText(settlersLobby.config.getDefaultAwayMsg());
		inputFontSize.setText(Integer.toString(settlersLobby.config.getFontSize()));
		chbActivateAwayOnIdle.setSelected(settlersLobby.config.isActivateAwayOnIdle());
		inputAwayTime.setText(Integer.toString(settlersLobby.config.getAwayTime()/1000));
		chbActivateTimeInChat.setSelected(settlersLobby.config.isShowTime());
		chbActivateAutoChatLogging.setSelected(settlersLobby.config.isLogChat());
		chbActivateFlashing.setSelected(settlersLobby.config.isFlashingWhenHighlighted());
		chbIsSortBuddys.setSelected(settlersLobby.config.isSortBuddys());
		chbIsSortAway.setSelected(settlersLobby.config.isSortAway());
		chbIsSortInGame.setSelected(settlersLobby.config.isSortInGame());
		chbSaveWindowPosition.setSelected(settlersLobby.config.isSaveWindowPosition());
		// set config for highlight-audiofile
		chbPlaySoundWhenHighlighted.setSelected(settlersLobby.config.isPlaySoundWhenHighlighted());

		inputURL.setEnabled(!settlersLobby.isConnectedWithServer());
		inputChannel.setEnabled(!settlersLobby.isConnectedWithServer());

		chbShowTeamspeak.setSelected(!settlersLobby.config.isHideTeamspeakWindow());
		
		chbAutoCompleteNicknames.setSelected(settlersLobby.config.isAutoCompleteNicknames());
		
		chbDelKey.setSelected(settlersLobby.config.isDelKey());

		String words = ""; //$NON-NLS-1$
		for (String word : settlersLobby.config.getHighlightingWords()) {
			words += word + ";"; //$NON-NLS-1$
		}
		if (words.equals("")) { //$NON-NLS-1$
			words = " "; // wegen dem folgenden Abschneiden des letzten Zeichens //$NON-NLS-1$
		}
		inputHighlightingWords.setText(words.substring(0, words.length()-1));
	}

	/**
	 * Setzt die Werte in der Siedler Registrierkarte.
	 */
	private void resetSiedlerSettings() {
		inputAutoSaveSoundFile.setText(settlersLobby.config.getAutoSaveSoundFile());
		inputTimeout.setText(settlersLobby.config.getTimeOut());
		chbDisableAutosaveSound.setSelected(settlersLobby.config.isDisabledAutosaveSound());
		chbStopStoringTools.setSelected(settlersLobby.config.isStopStoringTools());
		chbAutoSave.setSelected(settlersLobby.config.isAutoSave());
		chbShowWhisperIngame.setSelected(settlersLobby.config.getShowWhisperIngame());
		chbMessagesIngame.setSelected(settlersLobby.config.getShowMessagesIngame());
		chbActivateScreenSizeCheck.setSelected(settlersLobby.config.isActivateScreenSizeCheck());
		chbActivateForegroundWindowCheck.setSelected(settlersLobby.config.isActivateForegroundWindowCheck());
		chbActivateS3StatsScreenCapture.setSelected(settlersLobby.config.isActivateS3StatsScreenCapture());
		for (int i = 0; i < inputAutoSaveTimer.getItemCount(); i++) {
			if (inputAutoSaveTimer.getItemAt(i).equals(settlersLobby.config.getAutoSaveTimer())) {
				inputAutoSaveTimer.setSelectedIndex(i);
				break;
			}
		}
		inputBlinkSound.setText(settlersLobby.config.getBlinkSoundFile());
        if (SettlersLobby.isBetaUsed()) {
            s3versionnumber.setSelectedItem(I18n.getString("OptionsFrame.S3VERSIONNUMBER_" + settlersLobby.config.getS3Versionnumber()));
        }
		jcbresolution.setSelectedItem(settlersLobby.config.getS3CEResolution());
		checkCHBMessageIngameStatus();
	}

	private void resetBuddySettings() {
		inputBuddyUrl.removeAllItems();
		inputBuddyUrl.addItem(settlersLobby.config.getBuddyIgnoreUrl());
		if (!settlersLobby.config.getBuddyIgnoreUrl().equals(settlersLobby.config.getDefaultBuddyIgnoreUrl())) {
			inputBuddyUrl.addItem(settlersLobby.config.getDefaultBuddyIgnoreUrl());
		}
		chbAutoLoadBuddys.setSelected(settlersLobby.config.isAutoLoadAndSaveBuddys());
		chbNotifyIfBuddyComesOnline.setSelected(settlersLobby.config.isNotifyIfBuddyComesOnline());
	}

	/**
	 * Setzt die Werte in der Mapbase Registrierkarte.
	 */
	private void resetMapbaseSettings() {
		inputMapbaseUrl.removeAllItems();
		inputMapbaseUrl.addItem(settlersLobby.config.getMapDownloadPath());
		if (!settlersLobby.config.getMapDownloadPath().equals(settlersLobby.config.getDefaultMapDownloadPath())) {
			inputMapbaseUrl.addItem(settlersLobby.config.getDefaultMapDownloadPath());
		}
		inputAutomaticMapDownloadPath.removeAllItems();
		inputAutomaticMapDownloadPath.addItem(settlersLobby.config.getAutomaticMapDownloadPath());
		if (!settlersLobby.config.getAutomaticMapDownloadPath().equals(settlersLobby.config.getDefaultAutomaticMapDownloadPath())) {
			inputAutomaticMapDownloadPath.addItem(settlersLobby.config.getDefaultAutomaticMapDownloadPath());
		}
	}

	/**
	 * Setzt die Werte in der Lanparty Registrierkarte.
	 */
	private void resetLanpartySettings() {
		radioLanpartyModus.setSelected(settlersLobby.config.isLocalMode());
		radioInternetModus.setSelected(!settlersLobby.config.isLocalMode());
	}

	/**
	 * Setzt die Werte in der Update Registrierkarte
	 */
	private void resetUpdateSettings()
	{

	}

    private void resetSaveManagerSettings()
    {
		for (int i = 0; i < inputBackupCount.getItemCount(); i++) {
			if (inputBackupCount.getItemAt(i).equals(settlersLobby.config.getSaveBackupCount())) {
				inputBackupCount.setSelectedIndex(i);
				break;
			}
		}

        for (int i = 0; i < inputCleanupTime.getItemCount(); i++) {
            if (inputCleanupTime.getItemAt(i).equals(settlersLobby.config.getSaveBackupCleanupInterval())) {
                inputCleanupTime.setSelectedIndex(i);
                break;
            }
        }

        for (int i = 0; i < inputSaveCleanupTime.getItemCount(); i++) {
            if (inputSaveCleanupTime.getItemAt(i).equals(settlersLobby.config.getSaveCleanupInterval())) {
                inputSaveCleanupTime.setSelectedIndex(i);
                break;
            }
        }

        chbStartSavemanager.setSelected(settlersLobby.config.isStartSavemanager());
        chbStartWatchdog.setSelected(settlersLobby.config.isStartWatchdog());
        chbAutoPlace.setSelected(settlersLobby.config.isAutoPlace());
        chbDoCleanup.setSelected(settlersLobby.config.isDoSaveBackupCleanup());
        chbDoSaveCleanup.setSelected(settlersLobby.config.isDoSaveCleanup());
    }

	/**
	 * Setzt die Werte in der VPN Registrierkarte.
	 */
	private void resetVPNSettings() {
		radioHostDirectIpGames.setSelected(!settlersLobby.config.isHostUsingVpn());
		radioHostVpnGames.setSelected(settlersLobby.config.isHostUsingVpn());
		// set state of auto-vpn-checkbox
		chbAutoVPNStart.setSelected(settlersLobby.config.isAutoconnectVpn());
		chbWarnAboutThirdPartyFirewall.setSelected(settlersLobby.config.isWarnAboutThirdPartyFirewall());
	}

	/**
	 * Speichert alle Werte ab.
	 */
	private void save() throws Exception {
		resetBackgroundColors();
		try {
			checkValues();
			settlersLobby.config.isNotifyListener = false;
			saveChatSettings();
			saveSiedlerSettings();
			saveBuddySettings();
			saveLanpartySettings();
			saveMapbaseSettings();
			saveUpdateSettings();
			saveLanguage();
			saveSaveManagerSettings();
			saveVPNSettings();
			uebernehmenButton.setEnabled(false);
			settlersLobby.config.isNotifyListener = true;
			settlersLobby.config.notifyConfigurationListeners();
         if (settlersLobby.config.isStartSavemanager())
            settlersLobby.loadSaveManager();
         else
            settlersLobby.stopSaveManager();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.toString(), I18n.getString("OptionsFrame.ERROR_INPUT"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
			throw new Exception("Errors while checking values!"); //$NON-NLS-1$
		}
	}

    /**
	 * Prüft die Werte auf Fehlereingaben und färbt die betroffenen
	 * Eingabefelder in ihrer Hintergrundfarbe um.
	 * @throws Exception Wenn ein oder mehrere Fehleingaben gefunden wurden.
	 */
	private void checkValues() throws Exception {
		Vector<String> fehler = new Vector<String>();
		if (!inputAutoSaveSoundFile.getText().isEmpty())
		{
		    File soundFile = new File(inputAutoSaveSoundFile.getText());
		    if (!soundFile.exists())
		    {
		        fehler.add(I18n.getString("OptionsFrame.ERROR_MISSING_SOUND_FILE")); //$NON-NLS-1$
		        inputAutoSaveSoundFile.setBackground(inputBackgroundColorError);
	            tabbedPane.setBackgroundAt(2, inputBackgroundColorError);
		    }
		    else if (!isSupportedSoundFile(soundFile))
		    {
                fehler.add(I18n.getString("OptionsFrame.ERROR_FORMAT_NOT_SUPPORTED")); //$NON-NLS-1$
                inputAutoSaveSoundFile.setBackground(inputBackgroundColorError);
                tabbedPane.setBackgroundAt(2, inputBackgroundColorError);
		    }
		}
		// check value of inputfield for blinksound
		if (!inputBlinkSound.getText().isEmpty())
		{
		    File soundFile = new File(inputBlinkSound.getText());
		    if (!soundFile.exists())
		    {
		        fehler.add(I18n.getString("OptionsFrame.ERROR_MISSING_SOUND_FILE")); //$NON-NLS-1$
		        inputBlinkSound.setBackground(inputBackgroundColorError);
	            tabbedPane.setBackgroundAt(2, inputBackgroundColorError);
		    }
		    else if (!isSupportedSoundFile(soundFile))
		    {
                fehler.add(I18n.getString("OptionsFrame.ERROR_FORMAT_NOT_SUPPORTED")); //$NON-NLS-1$
                inputBlinkSound.setBackground(inputBackgroundColorError);
                tabbedPane.setBackgroundAt(2, inputBackgroundColorError);
		    }
		}
		if (inputURL.getSelectedItem().equals("")) { //$NON-NLS-1$
			fehler.add(I18n.getString("OptionsFrame.ERROR_MISSING_IRC_SERVER")); //$NON-NLS-1$
			inputURL.getEditor().getEditorComponent().setBackground(inputBackgroundColorError);
			tabbedPane.setBackgroundAt(0, inputBackgroundColorError);
		}
		if (inputChannel.getSelectedItem().equals("")) { //$NON-NLS-1$
			fehler.add(I18n.getString("OptionsFrame.ERROR_MISSING_CHANNEL")); //$NON-NLS-1$
			inputChannel.getEditor().getEditorComponent().setBackground(inputBackgroundColorError);
			tabbedPane.setBackgroundAt(0, inputBackgroundColorError);
		}
		if (inputAwayMsg.getText().equals("")) { //$NON-NLS-1$
			fehler.add(I18n.getString("OptionsFrame.ERROR_MISSING_AWAY_MESSAGE")); //$NON-NLS-1$
			inputAwayMsg.setBackground(inputBackgroundColorError);
			tabbedPane.setBackgroundAt(0, inputBackgroundColorError);
		}
		if (inputFontSize.getText().equals("")) { //$NON-NLS-1$
			fehler.add(I18n.getString("OptionsFrame.ERROR_MISSING_FONTSIZE")); //$NON-NLS-1$
			inputFontSize.setBackground(inputBackgroundColorError);
			tabbedPane.setBackgroundAt(0, inputBackgroundColorError);
		}
		if (!inputFontSize.getText().equals("") && (Integer.parseInt(inputFontSize.getText()) < 10 || Integer.parseInt(inputFontSize.getText()) > 30 ) ) { //$NON-NLS-1$
			fehler.add(I18n.getString("OptionsFrame.ERROR_WRONG_FONTSIZE")); //$NON-NLS-1$
			inputFontSize.setBackground(inputBackgroundColorError);
			tabbedPane.setBackgroundAt(0, inputBackgroundColorError);
		}
		if (inputAwayTime.getText().equals("0")) { //$NON-NLS-1$
			if (chbActivateAwayOnIdle.isSelected()) {
				fehler.add(I18n.getString("OptionsFrame.ERROR_INVALID_AWAY_TIMEOUT")); //$NON-NLS-1$
				inputAwayTime.setBackground(inputBackgroundColorError);
				tabbedPane.setBackgroundAt(1, inputBackgroundColorError);
			}
		}
		if (inputAwayTime.getText().equals("")) { //$NON-NLS-1$
			inputAwayTime.setText("0"); //$NON-NLS-1$
			if (chbActivateAwayOnIdle.isSelected()) {
				fehler.add(I18n.getString("OptionsFrame.ERROR_MISSING_AWAY_TIMEOUT")); //$NON-NLS-1$
				inputAwayTime.setBackground(inputBackgroundColorError);
				tabbedPane.setBackgroundAt(1, inputBackgroundColorError);
			}
		}
		if (inputTimeout.getText().equals("")) { //$NON-NLS-1$
			fehler.add(I18n.getString("OptionsFrame.ERROR_MISSING_TIMEOUT")); //$NON-NLS-1$
			inputTimeout.setBackground(inputBackgroundColorError);
			tabbedPane.setBackgroundAt(2, inputBackgroundColorError);
		}
		if (chbAutoLoadBuddys.isSelected() && !isOkBuddyUrl()) {
			fehler.add(I18n.getString("OptionsFrame.MISSING_BUDDYS_URL")); //$NON-NLS-1$
			// Farbgebung erfolt durch isOkBuddyUrl();
		}

		if (fehler.size() > 0) {
			String fehlerText = fehler.elementAt(0);
			for (int i = 1; i < fehler.size(); i++) {
				fehlerText = fehlerText + "\n" + fehler.elementAt(i); //$NON-NLS-1$
			}
			throw new Exception(fehlerText);
		}
	}

	/**
	 * Wenn die chbMessagesIngame nicht markiert ist, wird chbShowWhisperIngame
	 * disabled, andernfalls enabled.
	 */
	private void checkCHBMessageIngameStatus() {
		if (chbMessagesIngame.isSelected()) {
			chbShowWhisperIngame.setEnabled(true);
			//null übernimmt die Farbe vom parent, in unserem Fall ist
			//es die passende Default Farbe
			lblShowWhisperIngame.setForeground(settlersLobby.theme.getTextColor());
			// set actual configuration state for the checkbox after re-enabling it
			chbShowWhisperIngame.setSelected(settlersLobby.config.getShowWhisperIngame());
		} else {
			chbShowWhisperIngame.setEnabled(false);
			chbShowWhisperIngame.setSelected(false);
			lblShowWhisperIngame.setForeground(Color.GRAY);
		}
	}

	private boolean isOkBuddyUrl() {
		try {
			new URL((String) inputBuddyUrl.getSelectedItem());
			return true;
		} catch (MalformedURLException e) {
			inputBuddyUrl.setBackground(inputBackgroundColorError);
			tabbedPane.setBackgroundAt(3, inputBackgroundColorError);
			return false;
		}
	}

	/**
	 * Speichert die Werte der Registrierkarte Chat.
	 */
	private void saveChatSettings() {
		settlersLobby.config.setServerURL((String) inputURL.getSelectedItem());
		settlersLobby.config.setGameDateTimeFormat((String) gameDateTimeFormat.getSelectedItem());
		settlersLobby.config.setTheme((ALobbyThemes)theme.getSelectedItem());
		
		// if its christmastime, than save in config a marker to 
		// prevent automaticaly return to christmas-theme at next start
		if( settlersLobby.config.itsChristmastime() ) {
			settlersLobby.config.setPreventAutomaticChristmasTheme(true);
		}
		else {
			// reset after christmastime
			settlersLobby.config.setPreventAutomaticChristmasTheme(false);			
		}
		
		// if its halloweentime, than save in config a marker to 
		// prevent automaticaly return to halloween-theme at next start
		if( settlersLobby.config.itsHalloweentime() ) {
			settlersLobby.config.setPreventAutomaticHalloweenTheme(true);
		}
		else {
			// reset after halloween-time
			settlersLobby.config.setPreventAutomaticHalloweenTheme(false);			
		}
		
		// if its eastertime, than save in config a marker to 
		// prevent automaticaly return to easter-theme at next start
		if( settlersLobby.config.itsEastertime() ) {
			settlersLobby.config.setPreventAutomaticEasterTheme(true);
		}
		else {
			// reset after easter-time
			settlersLobby.config.setPreventAutomaticEasterTheme(false);			
		}
		
		settlersLobby.config.setChannel((String) inputChannel.getSelectedItem());
		settlersLobby.config.setDefaultAwayMsg(inputAwayMsg.getText());
		settlersLobby.config.setFontSize(Integer.parseInt(inputFontSize.getText()));
		settlersLobby.config.setActivateAwayOnIdle(chbActivateAwayOnIdle.isSelected());
		settlersLobby.config.setAwayTime(Integer.parseInt(inputAwayTime.getText())*1000);
		settlersLobby.config.setShowTime(chbActivateTimeInChat.isSelected());
		settlersLobby.config.setLogChat(chbActivateAutoChatLogging.isSelected());
		settlersLobby.config.setFlashingWhenHighlighted(chbActivateFlashing.isSelected());
		settlersLobby.config.setSortBuddys(chbIsSortBuddys.isSelected());
		settlersLobby.config.setSortAway(chbIsSortAway.isSelected());
		settlersLobby.config.setSortInGame(chbIsSortInGame.isSelected());
		settlersLobby.config.setSaveWindowPosition(chbSaveWindowPosition.isSelected());
		settlersLobby.config.setHideTeamspeakWindow(!chbShowTeamspeak.isSelected());
		// save config for activation of audiofile when highlighted
		settlersLobby.config.setPlaySoundWhenHighlighted(chbPlaySoundWhenHighlighted.isSelected());
		// save config for nick-competition
		settlersLobby.config.setAutoCompleteNicknames(chbAutoCompleteNicknames.isSelected());
		settlersLobby.config.setDelKey(chbDelKey.isSelected());

		StringTokenizer st = new StringTokenizer(inputHighlightingWords.getText(), ";"); //$NON-NLS-1$
		ArrayList<String> liste = new ArrayList<String>();
		while (st.hasMoreTokens()) {
			liste.add(st.nextToken());
		}
		settlersLobby.config.setHighlightingWords(liste);
	}

	/**
	 * Speichert die Werte der Registrierkarte Siedler 3.
	 */
	private void saveSiedlerSettings() {
		settlersLobby.config.setAutoSaveTimer((Integer) inputAutoSaveTimer.getSelectedItem());
		settlersLobby.config.setAutoSaveSoundFile(inputAutoSaveSoundFile.getText());
		settlersLobby.config.setTimeOut(inputTimeout.getText());
		settlersLobby.config.setAutoSave(chbAutoSave.isSelected());
		settlersLobby.config.setDisabledAutosaveSound(chbDisableAutosaveSound.isSelected());
		settlersLobby.config.setStopStoringTools(chbStopStoringTools.isSelected());
		settlersLobby.config.setShowWhisperIngame(chbShowWhisperIngame.isSelected());
		settlersLobby.config.setShowMessagesIngame(chbMessagesIngame.isSelected());
		settlersLobby.config.setActivateScreenSizeCheck(chbActivateScreenSizeCheck.isSelected());
		settlersLobby.config.setActivateForegroundWindowCheck(chbActivateForegroundWindowCheck.isSelected());
		// save setting for S3-Stats-Screen-Capturing
		settlersLobby.config.setActivateS3StatsScreenCapture(chbActivateS3StatsScreenCapture.isSelected());
		// save setting for blinksound
		settlersLobby.config.setBlinkSoundFile(inputBlinkSound.getText());
        if (SettlersLobby.isBetaUsed()) {
            // save selected s3versionnumber
            int s3versionnumber = 0;
            Test.output("s3version: " + this.s3versionnumber.getSelectedItem());
            if( this.s3versionnumber.getSelectedItem().toString().equals(I18n.getString("OptionsFrame.S3VERSIONNUMBER_172")) ) {
                s3versionnumber = 172;
            }
            if( this.s3versionnumber.getSelectedItem().toString().equals(I18n.getString("OptionsFrame.S3VERSIONNUMBER_173")) ) {
                s3versionnumber = 173;
            }
            if( this.s3versionnumber.getSelectedItem().toString().equals(I18n.getString("OptionsFrame.S3VERSIONNUMBER_300")) ) {
                s3versionnumber = 300;
            }
            settlersLobby.config.setS3Versionnumber(s3versionnumber);
        }
		// save selected resolution
		settlersLobby.config.setS3CEResolution(jcbresolution.getSelectedItem().toString());
	}

	private void saveBuddySettings() {
		settlersLobby.config.setBuddyIgnoreUrl((String) inputBuddyUrl.getSelectedItem());
		settlersLobby.config.setAutoLoadAndSaveBuddys(chbAutoLoadBuddys.isSelected());
		settlersLobby.config.setNotifyIfBuddyComesOnline(chbNotifyIfBuddyComesOnline.isSelected());
	}

	/**
	 * Speichert die Werte der Registrierkarte Lanparty.
	 */
	private void saveLanpartySettings() {
		settlersLobby.config.setLocalMode(radioLanpartyModus.isSelected());
	}

	/**
	 * Speichert die Werte der Registrierkarte Mapbase.
	 */
	private void saveMapbaseSettings() {
		settlersLobby.config.setMapDownloadPath((String) inputMapbaseUrl.getSelectedItem());
		settlersLobby.config.setAutomaticMapDownloadPath((String) inputAutomaticMapDownloadPath.getSelectedItem());
	}

	/**
	 * Speichert die Werte der Update Registrierkarte.
	 */
	private void saveUpdateSettings()
	{

	}

	/**
	 * Speichert die Werte der Sprache Registrierkarte
	 */
	private void saveLanguage() {
		settlersLobby.config.setLanguage(inputLanguage.getSelectedLocale());
	}

    private void saveSaveManagerSettings()
    {
      settlersLobby.config.setStartSavemanager(chbStartSavemanager.isSelected());
      settlersLobby.config.setStartWatchdog(chbStartWatchdog.isSelected());
      settlersLobby.config.setAutoPlace(chbAutoPlace.isSelected());
      settlersLobby.config.setSaveBackupCount((Integer) inputBackupCount.getSelectedItem());
      settlersLobby.config.setSaveBackupCleanupInterval((Integer) inputCleanupTime.getSelectedItem());
      settlersLobby.config.setDoSaveBackupCleanup(chbDoCleanup.isSelected());
      settlersLobby.config.setSaveCleanupInterval((Integer) inputSaveCleanupTime.getSelectedItem());
      settlersLobby.config.setDoSaveCleanup(chbDoSaveCleanup.isSelected());
    }

	/**
	 * Speichert die Werte der Registrierkarte VPN.
	 */
	private void saveVPNSettings() {
		settlersLobby.config.setHostUsingVpn(radioHostVpnGames.isSelected());
		// save settings for auto-vpn-checkbox when closing setting-window
		settlersLobby.config.setAutoconnectVpn(chbAutoVPNStart.isSelected());
		settlersLobby.config.setWarnAboutThirdPartyFirewall(chbWarnAboutThirdPartyFirewall.isSelected());
	}

	/**
	 * Wenn das OptionFrame sichtbar gemacht wird, werden alle Werte auf die
	 * derzeitig eingestellten gesetzt.
	 */
	@Override
	public void setVisible(boolean visibility) {
		if (visibility == true) {
			reset();
		}
		super.setVisible(visibility);
	}

	/**
	 * prüft, ob das übergebene file ein unterstütztes Audioformat enthält.
	 * @param soundFile
	 * @return
	 */
    protected boolean isSupportedSoundFile(File soundFile)
    {
        try
        {
            AudioSystem.getAudioFileFormat(soundFile);
            return true;
        }
        catch (UnsupportedAudioFileException e)
        {
            return false;
        }
        catch (IOException e2)
        {
            return false;
        }

    }
    
    /**
     * Insert the user-email into the field.
     */
    public void setEmail( String email ) {
    	if( email.length() > 0 ) {
    		String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
    		this.emailField.setText("<html>" + MessageFormat.format("<a href=''mailto:" + email + "'' color=''{0}''>" + email + "</a>", hexcolor) + "</html>");
    	}
    	else {
    		this.emailField.setText("");
    	}
    }

}
