/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.additional.ImagePanel;

public class StartFrame extends JWindow
{
    private static final long serialVersionUID = -5710619027591054120L;
    private JProgressBar progressBar;
	private JLabel progressLabel;
	private int steps;
	private Image backgroundImage;
	private SettlersLobby settlersLobby;

	public StartFrame(SettlersLobby settlersLobby, int steps, String firstStep)
	{
		this.settlersLobby = settlersLobby;
		setLayout(new BorderLayout());
		// add(new JLabel("Die Lobby wird gestartet", JLabel.CENTER),
		// BorderLayout.CENTER);
        try
        {
            backgroundImage = ImageIO.read(getClass().getResourceAsStream(
                    ALobbyConstants.PACKAGE_PATH + "/gui/img/LoadingScreen.png"));
        } catch (IOException e)
        {
            backgroundImage = null;
        }
		add(cr8Component(firstStep), BorderLayout.CENTER);
		add(progressBar = new JProgressBar(0, steps), BorderLayout.SOUTH);
		pack();
		Dimension frameSize = new Dimension(500, 300);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int top = (screenSize.height - frameSize.height) / 2;
		int left = (screenSize.width - frameSize.width) / 2;
		this.setSize(frameSize);
		this.setLocation(left, top);
		this.setBackground(settlersLobby.theme.startBackgroundColor);
		setVisible(true);
	}

	private JComponent cr8Component(String firstStep)
	{
		JPanel bugWorkaroundPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		if (backgroundImage != null)
		{
		    bugWorkaroundPanel.setBackground(settlersLobby.theme.startBackgroundColor);
		}
		add(bugWorkaroundPanel);
	    //Panel für das Hintergrund Bild
        JPanel backgroundPanel = new ImagePanel(backgroundImage);

        backgroundPanel.setLayout(new GridBagLayout());
        bugWorkaroundPanel.setLayout(new BorderLayout());
        bugWorkaroundPanel.add(backgroundPanel);
        bugWorkaroundPanel.setBackground(settlersLobby.theme.startBackgroundColor);
        bugWorkaroundPanel.add(progressLabel = new JLabel(firstStep, JLabel.LEFT),
				BorderLayout.SOUTH);
		progressLabel.setForeground(settlersLobby.theme.startForegroundColor);
		return bugWorkaroundPanel;
	}

	@Override
    public void dispose()
	{
		progressBar.setValue(steps);
		setVisible(false);
		super.dispose();
	}

	public void setNewStep(String step)
	{
		progressLabel.setText(step);
		progressBar.setValue(progressBar.getValue() + 1);
	}

}
