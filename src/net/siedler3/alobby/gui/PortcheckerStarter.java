/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

import org.json.JSONObject;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.s3gameinterface.IProcessWindowSupport;
import net.siedler3.alobby.s3gameinterface.ProcessWindowSupportHandler;
import net.siedler3.alobby.util.JavaProcessWrapper;

public class PortcheckerStarter extends Thread
{
	private SettlersLobby settlersLobby;
	private Process process;
	private PortcheckerWindow portcheckerWindow = null;
	private String s3Exe = null;
	private boolean portErrors = false;
	private boolean portError1900udp = false;
	private LoadingJFrame waitingWindow = null;
	final int directionOutgoing = 1;
	final int directionIncoming = 2;
	
	// HashMap for the results
	Map<String,Map<String, String>> portMap = null;
	
	// primary ports from and to
	private int primaryPortsFrom = 2300;
	private int primaryPortsTo = 2312;

	/**
	 * Start the portchecker with necessary parameters.
	 * 
	 * @param settlersLobby	settlerLobby-object
	 * @param s3Exe			path to s3.exe as String
	 * @param caller		calling window
	 * @param minimalCheck	marker if minimalCheck should be run
	 * @throws IOException
	 */
	@SuppressWarnings("static-access")
	public PortcheckerStarter(SettlersLobby settlersLobby, String s3Exe, PortcheckerWindow caller, boolean minimalCheck) throws IOException
	{
		this.settlersLobby = settlersLobby;
		this.portcheckerWindow = caller;
		this.s3Exe = s3Exe;
		setName(getName() + "-portchecker"); //$NON-NLS-1$
		
		// reset the resulting hashmap
		this.portMap = new HashMap<>();
		
		// get a separate window with waiting-message
		waitingWindow  = new LoadingJFrame(this.portcheckerWindow);

		// prepare path
		s3Exe = s3Exe.replace("\\", "\\\\");
		
		// get the local vpn ip
		String myIp = this.settlersLobby.getVpnIpFromSystem();
		
		// get the remote ip which will be checked
		String ip = "irc.siedler3.net";
		if( this.settlersLobby.checkIfVpnIsUsed() ) {
			ip = "10.3.0.1";
		}
		
		// generate a JSON-list with the ports which should be checked with portchecker
		ArrayList<Integer> tcpPorts =  new ArrayList<Integer>();
		ArrayList<Integer> udpPorts =  new ArrayList<Integer>();
		if( minimalCheck ) {
			// set ports for minimal scan
			for( int p = this.primaryPortsFrom; p <= this.primaryPortsTo; p++ ) {
				tcpPorts.add(p);
			}
			tcpPorts.add(47624);
			for( int p = this.primaryPortsFrom; p <= this.primaryPortsTo; p++ ) {
				udpPorts.add(p);
			}
			udpPorts.add(1900);
		}
		else {
			// set ports for complete scan
			// -> tcp-ports
			for( int p = 2300; p <= 2400; p++ ) {
				tcpPorts.add(p);
			}
			tcpPorts.add(47624);
			// -> udp-ports
			for( int p = 2300; p <= 2400; p++ ) {
				udpPorts.add(p);
			}
			udpPorts.add(1900);
		}

		// convert lists to JSON-Strings
		JSONObject tcpPortlist = this.generateJSONObject( tcpPorts );
		JSONObject udpPortlist = this.generateJSONObject( udpPorts );
		
		// generate the complete JSON-list with both types of ports
		JSONObject results = new JSONObject();
		results.put("tcp", tcpPortlist);
		results.put("udp", udpPortlist);
		
		// save the resulting JSON-list to file
		try (FileWriter file = new FileWriter(settlersLobby.configDir + "/portlist.json")) {
			file.write(results.toString());
		}
		
		// set arguments for ProcessBuilder
		List<String> args = new ArrayList<String>();
		// -> exe-file itself
		args.add(s3Exe);
		// -> destination-ip
		args.add("--host-ip");
		args.add(ip);
		// -> set the local ip
		args.add("--ip");
		args.add(myIp);
		// -> configDir where the file with the results will be written
		args.add("--configDir");
		args.add(settlersLobby.configDir);
		// -> debugDir where the logfile will be written
		args.add("--debugDir");
		args.add(settlersLobby.configDir);
		
		// create the process with the given arguments
		process = JavaProcessWrapper.createProcessWithParam(args);
		if( process != null )
		{
		    Test.output("using class " + process.getClass().getName()); //$NON-NLS-1$
		}

        IProcessWindowSupport processWindowSupport = null;
        if (process instanceof IProcessWindowSupport)
        {
            processWindowSupport = (IProcessWindowSupport)process;
        }
        ProcessWindowSupportHandler.getInstance().setProcessWindowSupport(processWindowSupport);

	}

	/**
	 * Wait until the process ends. 
	 * Get the resultcode. 
	 * Process the results.
	 */
	@Override
    public void run()
	{
		int rc = -1;
		try
		{
			rc = process.waitFor();
			Test.output("portchecker exited: " + rc);
			// remove the process window support if it is still used
	        if (process instanceof IProcessWindowSupport)
	        {
	            ProcessWindowSupportHandler.getInstance().removeProcessWindowSupport((IProcessWindowSupport)process);
	        }
		}
		catch (Exception ex)
		{
		    Test.outputException(ex);
		    //something unexpected happened
		    //clear the link to the process
		    replaceExe();
		}
		finally
		{
		    process = null;
		    
		    // close the waiting window
		    if( waitingWindow != null ) {
		    	waitingWindow.dispatchEvent(new WindowEvent(waitingWindow, WindowEvent.WINDOW_CLOSING));
		    }
		    
		    // from the point of the portchecker has exited with resultcode 0 ( = all ok)
            if( rc == 0 && settlersLobby != null && s3Exe != null)
            {
            	replaceExe();
				
            	// read the json and insert the results into the portcheckerWindow-table
				@SuppressWarnings("static-access")
				File f = new File(settlersLobby.configDir + "/portcheckresults.json");
		        if( f.exists() && f.canRead() ){
		            try {
		            	// get the content of the result-file
		                String text = new String(Files.readAllBytes(Paths.get(f.getAbsolutePath())), StandardCharsets.UTF_8);
		                // get the JSON-object of this content
						JSONObject obj = new JSONObject(text);
						// get the tcp-results and add them to result-hashMap
						JSONObject tcpList = obj.getJSONObject("tcp");
						addResultsToHashMap(tcpList, "tcp");
						// get the udp-results and add them to result-hashMap
						JSONObject udpList = obj.getJSONObject("udp");
						addResultsToHashMap(udpList, "udp");
						// iterate through the sorted results
						addResultsToTable();
						
					} catch (IOException e) {
						Test.outputException(e);
					}
		            if( false != this.portErrors ) {
		            	// show hint that result-file could not be read
		            	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
		            	// -> add message
		        		messagewindow.addMessage(
		        			"<html>" + I18n.getString("PortcheckerStarter.ERROR_PORTS") + "</html>", 
		        			UIManager.getIcon("OptionPane.errorIcon")
		        		);
		        		if( false != this.portError1900udp ) {
		        			// -> add message for port 1900 udp
			        		messagewindow.addMessage(
			        			"<html>" + I18n.getString("PortcheckerStarter.ERROR_PORT_1900") + "</html>", 
			        			UIManager.getIcon("OptionPane.errorIcon")
			        		);		        			
		        		}
		        		// -> add ok-button
		        		messagewindow.addButton(
		        			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
		        			new ActionListener() {
		        			    @Override
		        			    public void actionPerformed(ActionEvent e) {
		        			    	messagewindow.dispose();
		        			    }
		        			}
		        		);
		        		// show it
		        		messagewindow.setVisible(true);
		            }
		            else {
		            	// show hint that result-file could not be read
		            	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.INFO"), true);
		            	// -> add message
		        		messagewindow.addMessage(
		        			"<html>" + I18n.getString("PortcheckerStarter.OK_MESSAGE") + "</html>", 
		        			UIManager.getIcon("OptionPane.informationIcon")
		        		);
		        		// -> add button which will send a message into the chat that portchecker-test was successfull
		        		messagewindow.addButton(
		        			"in Chat mitteilen",
		        			new ActionListener() {
		        			    @Override
		        			    public void actionPerformed(ActionEvent e) {
		        			    	settlersLobby.actionChatMessage(I18n.getString("PortcheckerStarter.CHAT_MESSAGE"));
		        			    	messagewindow.dispose();
		        			    }
		        			}
		        		);
		        		// -> add ok-button
		        		messagewindow.addButton(
		        			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
		        			new ActionListener() {
		        			    @Override
		        			    public void actionPerformed(ActionEvent e) {
		        			    	messagewindow.dispose();
		        			    }
		        			}
		        		);
		        		// show it
		        		messagewindow.setVisible(true);
		            }
		        }
		        else {
		        	// show hint that result-file could not be read
	            	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
	            	// -> add message
	        		messagewindow.addMessage(
	        			"<html>" + I18n.getString("PortcheckerStarter.ERROR_RESULTS_NOT_READABLE") + "</html>", 
	        			UIManager.getIcon("OptionPane.errorIcon")
	        		);
	        		// -> add ok-button
	        		messagewindow.addButton(
	        			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
	        			new ActionListener() {
	        			    @Override
	        			    public void actionPerformed(ActionEvent e) {
	        			    	messagewindow.dispose();
	        			    }
	        			}
	        		);
	        		// show it
	        		messagewindow.setVisible(true);
		        }
            }
            else if( rc == 4 ) {
            	// Show hint that something went wrong with portchecker
            	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
            	// -> add message
        		messagewindow.addMessage(
        			"<html>" + I18n.getString("PortcheckerStarter.ERROR_NOT_CONNECTED") + "</html>", 
        			UIManager.getIcon("OptionPane.errorIcon")
        		);
        		// -> add ok-button
        		messagewindow.addButton(
        			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
        			new ActionListener() {
        			    @Override
        			    public void actionPerformed(ActionEvent e) {
        			    	messagewindow.dispose();
        			    }
        			}
        		);
        		// show it
        		messagewindow.setVisible(true);
            }
            else if( rc > 0 ) {
            	// Show hint that something went wrong with portchecker
            	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
            	// -> add message
        		messagewindow.addMessage(
        			"<html>" + I18n.getString("PortcheckerStarter.ERROR_COULD_NOT_RUN") + "</html>", 
        			UIManager.getIcon("OptionPane.errorIcon")
        		);
        		// -> add ok-button
        		messagewindow.addButton(
        			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
        			new ActionListener() {
        			    @Override
        			    public void actionPerformed(ActionEvent e) {
        			    	messagewindow.dispose();
        			    }
        			}
        		);
        		// show it
        		messagewindow.setVisible(true);
            }
		}
	}
	
	/**
	 * Add results from JSON-file to HashMap and interpret them for resulting message.
	 * 
	 * @param list		the results from JSON-file
	 * @param typeName	the type of results (udp or tcp)
	 */
	private void addResultsToHashMap(JSONObject list, String typeName) {
		// get the keys from the list
		Iterator<String> keys = list.keys();
		
		// loop through the keys
		while(keys.hasNext()) {
		    String key = keys.next();
		    if (list.get(key) != null) {
		    	JSONObject valuesObject = new JSONObject(list.get(key).toString());
		    	String port = "";
		    	String result = "";
		    	int direction = 0;
		    	if( !valuesObject.isNull("port") ) {
		    		port = String.valueOf(valuesObject.getInt("port"));
		    	}
		    	if( !valuesObject.isNull("result") ) {
		    		result = String.valueOf(valuesObject.getInt("result"));
		    	}
		    	if( !valuesObject.isNull("direction") ) {
		    		direction = valuesObject.getInt("direction");
		    	}
		    	String directionName = I18n.getString("PortcheckerStarter.INCOMING");
		    	if( directionOutgoing == direction ) {
		    		directionName = I18n.getString("PortcheckerStarter.OUTGOING");
		    	}
		    	String state = I18n.getString("PortcheckerStarter.ERROR");
		    	// special case: port 1900 should be closed in udp outgoing
		    	// => result-value should _not_ be "3" (which stands for "open port")
    			if( "1900".equals(port) && "udp".equals(typeName) && directionOutgoing == direction ) {
    				if( "3".equals(result) ) {
    					portError1900udp = true;
			    		portErrors = true;
    				}
    				else {
    					state = I18n.getString("PortcheckerStarter.OK");
			    	}
    			}
    			else {
			    	// get the result for this port
			    	if( "3".equals(result) ) {
			    		// result is 3, than all ok
			    		state = I18n.getString("PortcheckerStarter.OK");
			    	}
			    	else {
			    		// set marker that there was an error
			    		portErrors = true;
			    	}
    			}
    			
    			// add result to hashMap which will be sorted before displaying the results
    			Map<String, String> objectData = new HashMap<>();
    			objectData.put("port", port);
    			objectData.put("state", state);
    			objectData.put("typename", typeName);
    			objectData.put("directionname", directionName);
    			this.portMap.put(port + typeName + directionName, objectData);
		    }
		}
	}

	/**
	 * Add the hashMap-results into the table after sorting them.
	 */
	private void addResultsToTable() {
		// sort the hashmap
		// -> first tcp port 47624 and udp port 1900
		// -> than tcp and udp ports 2300-2312
		// -> than error-ports, if exists
		// -> than all other
		TreeMap<String, Map<String, String>> portMapSorted = new TreeMap<>(this.portMap);
		
		// first add the ports mentioned above
		addSingleResultToTable(portMapSorted.get("47624tcp" + I18n.getString("PortcheckerStarter.INCOMING")));
		addSingleResultToTable(portMapSorted.get("47624tcp" + I18n.getString("PortcheckerStarter.OUTGOING")));
		addSingleResultToTable(portMapSorted.get("1900udp" + I18n.getString("PortcheckerStarter.OUTGOING")));
		for( int p = this.primaryPortsFrom; p <= this.primaryPortsTo; p++ ) {
			addSingleResultToTable(portMapSorted.get(p + "tcp" + I18n.getString("PortcheckerStarter.INCOMING")));
			addSingleResultToTable(portMapSorted.get(p + "udp" + I18n.getString("PortcheckerStarter.INCOMING")));
			addSingleResultToTable(portMapSorted.get(p + "tcp" + I18n.getString("PortcheckerStarter.OUTGOING")));
			addSingleResultToTable(portMapSorted.get(p + "udp" + I18n.getString("PortcheckerStarter.OUTGOING")));
			// and remove this port from hashmap to prevent double output
			portMapSorted.remove(p + "tcp" + I18n.getString("PortcheckerStarter.INCOMING"));
			portMapSorted.remove(p + "udp" + I18n.getString("PortcheckerStarter.OUTGOING"));
			portMapSorted.remove(p + "tcp" + I18n.getString("PortcheckerStarter.INCOMING"));
			portMapSorted.remove(p + "udp" + I18n.getString("PortcheckerStarter.OUTGOING"));
		}
		
		// and remove this ports from hashmap to prevent double output
		portMapSorted.remove("47624tcp" + I18n.getString("PortcheckerStarter.INCOMING"));
		portMapSorted.remove("47624tcp" + I18n.getString("PortcheckerStarter.OUTGOING"));
		portMapSorted.remove("1900udp" + I18n.getString("PortcheckerStarter.INCOMING"));
		portMapSorted.remove("1900udp" + I18n.getString("PortcheckerStarter.OUTGOING"));
		
		// now create the table from hashmap
		for (Entry<String, Map<String, String>> entry : portMapSorted.entrySet()) {
			addSingleResultToTable(entry.getValue());
		}
		
	}
	
	/**
	 * Add single entry to the resulting-table.
	 * 
	 * @param map
	 */
	private void addSingleResultToTable(Map<String, String> object) {
		// get the tablemodel (we need it to add the result-rows to the table)
		DefaultTableModel tableModel = this.portcheckerWindow.getTableModel();
		
		// generate the state-marker for the table
		Font fontAwesome = settlersLobby.config.getFontAweSome( 14, Font.BOLD );
		JLabel lblState = new JLabel();
		lblState.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		lblState.setToolTipText(object.get("state"));
		lblState.setFont(fontAwesome);
		lblState.setText("\uf00d");
		lblState.setForeground(Color.red);
		if( I18n.getString("PortcheckerStarter.OK").equals(object.get("state")) ) {
			lblState.setText("\uf00c");
			lblState.setForeground(Color.green);
		}
		
		// add the results to the table
    	tableModel.addRow(new Object[]{
			lblState,
			object.get("typename"),
			object.get("port"),
			object.get("directionname")
    	});
	}
	
	/**
	 * Rename original s3_alobby.exe back to its original-name.
	 */
	private void replaceExe() {
		File originalFile = new File(s3Exe + ".original");
		File file = new File(s3Exe);
		try {
			Files.move(originalFile.toPath(), file.toPath(), java.nio.file.StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			Test.outputException(e);
		}
	}
	
	/**
	 * Generate the JSON-Object with given data
	 */
	private JSONObject generateJSONObject( ArrayList<Integer> ports ) {
		// create json-object for all given ports
		JSONObject json = new JSONObject();
		// loop through the data
		for( int port: ports ) {
			// create json-object for single port
			// -> for outgoing data
			JSONObject portOutgoing = new JSONObject();
			portOutgoing.put("direction", this.directionOutgoing);
			portOutgoing.put("port", port);
			// -> add the data for outgoing data for this port
			json.put("" + port + this.directionOutgoing, portOutgoing);
			// -> for incoming data except port 1900
			if( port != 1900 ) {
				JSONObject portIncoming = new JSONObject();
				portIncoming.put("direction", this.directionIncoming);
				portIncoming.put("port", port);
				// -> add the data for incoming data for this port
				json.put("" + port + this.directionIncoming, portIncoming);
			}
		}
		return json;
	}
}
