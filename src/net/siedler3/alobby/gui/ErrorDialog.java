/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Group;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.s3gameinterface.S3Starter;

/**
 * Das ist ein Beispiel, wie man einen Fehlerdialog realisieren kann, welcher
 * das Programm so lange anhält, bis der Benutzer auf OK gedrückt hat.
 *
 * @author Stephan Bauer (aka maximilius)
 */
public class ErrorDialog extends JDialog implements ActionListener
{
	private static final String TITLE = I18n.getString("ErrorDialog.WINDOW_NAME1") + " > " + I18n.getString("ErrorDialog.WINDOW_NAME2"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	private GUI gui;
	// Komponenten für den Error-Dialog
	private JButton	btnOK				= new JButton(I18n.getString("ErrorDialog.OK")); //$NON-NLS-1$
	// changed lblErrorMessage from JLabel to LinkLabel for clickable links in Error-Messages
    private LinkLabel lblErrorMessage	= new LinkLabel();
    private JPanel pnlError				= new JPanel();
    private JButton btnTerminateS3;
    private S3Starter s3Starter;
    private SettlersLobby settlersLobby;

	/**
	 * Konstruktor. Lässt eine Fehlernachricht aufpoppen und hält das Programm
	 * so lange an, bis der Benutzer den OK Button drückt.
	 *
	 * @param message
	 */
    public ErrorDialog(String message, GUI gui, SettlersLobby settlersLobby)
    {
        this(message, gui, true, settlersLobby);
    }

    /**
     * Konstruktor. Lässt eine Fehlernachricht aufpoppen und hält das Programm
     * so lange an, bis der Benutzer den OK Button drückt.
     *
     * @param message
     * @param gui
     * @param reformatMessage gibt an, ob die übergebene Nachricht entsprechend
     * der vorgegebenen Fensterbreite formatiert werden soll.
     */
    public ErrorDialog(String message, GUI gui, boolean reformatMessage, SettlersLobby settlersLobby)
    {
        this(message, gui, reformatMessage, null, settlersLobby);
    }

    /**
     * Lässt eine Fehlernachricht aufpoppen und hält das Programm
     * so lange an, bis der Benutzer den OK oder S3-Beenden Button drückt.
     * Beim einem Klick auf den S3-Beenden Button wird der übergebene
     * Prozess terminiert.
     * Sollte s3Process null sein, wird der S3-Beenden Button nicht angezeigt.
     *
     * @param message
     * @param gui
     * @param reformatMessage
     * @param s3Starter aktiver S3 Prozess
     */
	public ErrorDialog(String message, GUI gui, boolean reformatMessage, S3Starter s3Starter, SettlersLobby settlersLobby)
	{
	    super((JFrame)null, true);
	    this.setTitle(TITLE);
		this.gui = gui;
		this.settlersLobby = settlersLobby;
		setIconImage(gui.getIcon());

		this.s3Starter = s3Starter;

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getContentPane().setBackground(this.settlersLobby.theme.backgroundColor);
		setAlwaysOnTop(true);
        setResizable(false);

        pnlError.setBorder(javax.swing.BorderFactory.createTitledBorder(
                null, 
                " " + I18n.getString("ErrorDialog.ERROR") + " ", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, 
                javax.swing.border.TitledBorder.DEFAULT_POSITION, 
                new java.awt.Font("Trebuchet MS italic", 1, 17), //$NON-NLS-1$ 
                this.settlersLobby.theme.getTextColor()));

        lblErrorMessage.setIcon(new javax.swing.ImageIcon(getClass().getResource(ALobbyConstants.PACKAGE_PATH + "/gui/img/bild.error.png"))); //$NON-NLS-1$
        lblErrorMessage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        if (reformatMessage)
        {
            message = convertInHtmlString2(message);
        }
        lblErrorMessage.setText("<html>" + message + "</html>"); //$NON-NLS-1$ //$NON-NLS-2$

        btnOK.setText(I18n.getString("ErrorDialog.OK")); //$NON-NLS-1$
        btnOK.addActionListener(this);
        btnOK.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        if (s3Starter != null && s3Starter.isS3Running())
        {
            btnTerminateS3 = new JButton(I18n.getString("ErrorDialog.TERMINATE_S3")); //$NON-NLS-1$
            btnTerminateS3.addActionListener(this);
            btnTerminateS3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        }

        javax.swing.GroupLayout pnlErrorLayout = new javax.swing.GroupLayout(pnlError);
        pnlError.setLayout(pnlErrorLayout);
        SequentialGroup group1;

        //der Terminate Button muss ins Layout eingefügt werden, falls er vorhanden ist
        //sieht leider etwas unschön aus
        group1 = pnlErrorLayout.createSequentialGroup()
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
                    GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnOK,
                javax.swing.GroupLayout.DEFAULT_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE,
                javax.swing.GroupLayout.PREFERRED_SIZE);
        if (btnTerminateS3 != null)
        {
            group1.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnTerminateS3,
                    javax.swing.GroupLayout.DEFAULT_SIZE,
                    javax.swing.GroupLayout.PREFERRED_SIZE,
                    javax.swing.GroupLayout.PREFERRED_SIZE);
        }
        group1.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,
                GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);

        Group group2 = pnlErrorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(btnOK);
        if (btnTerminateS3 != null)
        {
                group2.addComponent(btnTerminateS3);
        }

        pnlErrorLayout.setHorizontalGroup(
            pnlErrorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlErrorLayout.createSequentialGroup()
                .addGroup(pnlErrorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlErrorLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblErrorMessage, javax.swing.GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE))
                    .addGroup(group1))
                .addContainerGap())
        );


        pnlErrorLayout.setVerticalGroup(
            pnlErrorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlErrorLayout.createSequentialGroup()
                .addComponent(lblErrorMessage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(group2)
                .addGap(29, 29, 29))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlError, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlError, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();

		setLocationRelativeTo(gui.currentActiveFrame);
		setVisible(true);

	} // Error Dialog

	/**
	 * Wird durch Drücken des OK-Buttons oder S3-Beenden-Buttons ausgelöst.
	 * Zerstört die Fehlernachricht und enabled die Fenster der GUI.
	 */
	@Override
    public void actionPerformed(ActionEvent e)
	{
        if (e.getSource() == btnTerminateS3)
        {
            s3Starter.destroyS3();
        }

	    if (e.getSource() == btnOK || e.getSource() == btnTerminateS3)
	    {
    		setVisible(false);
    		dispose();

    		if (gui.currentActiveFrame != null)
    		{
    			// Der Befehl hat den gewünschten Nebeneffekt, dass der Frame aus der Taskleiste geöffnet wird
    			gui.currentActiveFrame.setAlwaysOnTop(true);
    			// Anschließend soll er aber wieder deaktiviert werden, weil der eigentliche Zweck unerwünscht ist
    			gui.currentActiveFrame.setAlwaysOnTop(false);
    		}
	    }
	}

	// Function is not used, so its commented out for test
	/*private String convertInHtmlString(String message)
	{
		int maxLength = 40;

		if(message.length() > maxLength)
	    {
	    	String[] splittedMessage = message.split(" "); //$NON-NLS-1$
	    	if(splittedMessage.length <= 0)
	    	{
	    		return message;
	    	}
	    	message = " "; //$NON-NLS-1$
	    	int countRows = 1;
	    	for(int i = 0; i < splittedMessage.length; ++i)
	    	{
	    		if((message + splittedMessage[i]).length() > maxLength * countRows)
	    		{
	    			message += "<br>" + splittedMessage[i] + " "; //$NON-NLS-1$ //$NON-NLS-2$
	    			countRows++;
	    		}
	    		else
	    		{
	    			message += splittedMessage[i] + " "; //$NON-NLS-1$
	    		}
	    	}
	    }
		return message;
	}*/
	private String convertInHtmlString2(String message)
	{
	    return message.replace("\n", "<br>"); //$NON-NLS-1$ //$NON-NLS-2$
	}
}
