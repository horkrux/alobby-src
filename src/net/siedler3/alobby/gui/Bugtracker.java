package net.siedler3.alobby.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.GroupLayout.Alignment;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.httpRequests;

/**
 * Creates a window for users to upload their log-files.
 * 
 * @author Zwirni
 *
 */
class Bugtracker extends JFrame {
	
	private SettlersLobby settlersLobby;
	private JTextField txtNickname;
	private JTextField txtTitle;
	private JTextArea txtMessage;

	/**
	 * Create the window-instance.
	 * 
	 * @param settlersLobby
	 * @param title
	 */
	public Bugtracker(SettlersLobby settlersLobby, String title) {
		super(title);
		this.settlersLobby = settlersLobby;
		
		// set the icon
		settlersLobby.getGUI().setIcon(this);
		
		// set the default text-orientation in this window (possible language-depending)
		this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		// set the size
		this.setSize(600,400);

		// center this window
		this.setLocationRelativeTo(this.settlersLobby.getGUI().getMainWindow());
		
		// define what happens if windows is closed
		// -> simply close the window itself
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// create the main panel where all parts will be added
		JPanel mainPanel = new JPanel();
		mainPanel.setSize(new Dimension(530, 360));
		mainPanel.setPreferredSize(new Dimension(640, 400));
		mainPanel.setMinimumSize(new Dimension(530, 360));
		mainPanel.setLocale(Locale.GERMANY);
		mainPanel.setAutoscrolls(true);
		mainPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		// create panel for some user-data
		JPanel userdataPanel = new JPanel();
		userdataPanel.setAlignmentX(0.0f);
		userdataPanel.setAlignmentY(Component.TOP_ALIGNMENT);
		
		// create field for nickname with label
		JLabel lblNickname = new JLabel(I18n.getString("Bugtracker.YOUR_NICKNAME"));
		txtNickname = new JTextField();
		txtNickname.setToolTipText(I18n.getString("Bugtracker.YOUR_NICKNAME_HOVER"));
		txtNickname.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		txtNickname.setAutoscrolls(false);
		txtNickname.setHorizontalAlignment(SwingConstants.LEFT);
		txtNickname.setColumns(10);
		txtNickname.setBorder(BorderFactory.createCompoundBorder(txtNickname.getBorder(), BorderFactory.createEmptyBorder(3, 3, 2, 3)));
		if( this.settlersLobby.getNick().length() > 0 ) {
			txtNickname.setText(this.settlersLobby.getNick());
			txtNickname.setEditable(false);
		}
		
		// create field for title for the request
		JLabel lblTitle = new JLabel(I18n.getString("Bugtracker.TITLE"));
		txtTitle = new JTextField();
		txtTitle.setToolTipText(I18n.getString("Bugtracker.TITLE_HOVER"));
		txtTitle.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		txtTitle.setAutoscrolls(false);
		txtTitle.setHorizontalAlignment(SwingConstants.LEFT);
		txtTitle.setColumns(10);
		txtTitle.setBorder(BorderFactory.createCompoundBorder(txtTitle.getBorder(), BorderFactory.createEmptyBorder(3, 3, 2, 3)));
		
		// create field for a message for the request
		JLabel lblMessage = new JLabel(I18n.getString("Bugtracker.PROBLEM_DESCRIPTION"));
		txtMessage = new JTextArea();
		txtMessage.setLineWrap(true);
		txtMessage.setWrapStyleWord(true);
		txtMessage.setAutoscrolls(true);
		txtMessage.setBorder(BorderFactory.createCompoundBorder(txtNickname.getBorder(), BorderFactory.createEmptyBorder(3, 3, 2, 3)));
		JScrollPane scrollPaneMessage = new JScrollPane(txtMessage);
		
		// create layout for user-data-panel
		GroupLayout gl_userdataPanel = new GroupLayout(userdataPanel);
		gl_userdataPanel.setAutoCreateGaps(true);
		gl_userdataPanel.setAutoCreateContainerGaps(true);

		gl_userdataPanel.setHorizontalGroup(
			gl_userdataPanel.createParallelGroup()
				.addGroup(gl_userdataPanel.createParallelGroup(Alignment.LEADING)
					.addGroup(Alignment.TRAILING, gl_userdataPanel.createSequentialGroup()
						.addComponent(lblNickname, 20, 160, 160)
						.addComponent(txtNickname)
					)
					.addGroup(Alignment.TRAILING, gl_userdataPanel.createSequentialGroup()
						.addComponent(lblTitle, 20, 160, 160)
						.addComponent(txtTitle)
					)
					.addGroup(Alignment.TRAILING, gl_userdataPanel.createSequentialGroup()
						.addComponent(lblMessage, 20, 160, 160)
						.addComponent(scrollPaneMessage)
					)
				)
		);
		gl_userdataPanel.setVerticalGroup(
			gl_userdataPanel.createParallelGroup()
				.addGroup(gl_userdataPanel.createSequentialGroup()
					.addGroup(gl_userdataPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNickname)
						.addComponent(txtNickname)
					)
					.addGroup(gl_userdataPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTitle)
						.addComponent(txtTitle)
					)
					.addGroup(gl_userdataPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblMessage)
						.addComponent(scrollPaneMessage)
					)
				)
		);
		userdataPanel.setLayout(gl_userdataPanel);
		
		// create panel for informations about what will be sent
		JPanel infoPanel = new JPanel();
		
		// create info-area for some text
		JEditorPane description = new JEditorPane();
		SwingUtilities.invokeLater(new Runnable() {
            @SuppressWarnings("static-access")
			public void run() {
				description.setEditable(false);
				description.setContentType("text/html");
				description.setOpaque(false);
				description.setBorder(null);
				description.setSize(20,56);
				HTMLEditorKit kit = new HTMLEditorKit();
				StyleSheet styleSheet = kit.getStyleSheet();
				Color color = settlersLobby.theme.getStaticTextColor();
				styleSheet.addRule(String.format("body, a { color: rgb(%s, %s, %s);font-family: Tahoma;font-size: 10px; } p { margin: 0; } ul { margin-left: 16px;padding-left: 0; }", color.getRed(), color.getGreen(), color.getBlue()));
				Document doc = kit.createDefaultDocument();
				description.setDocument(doc);
				description.setText(MessageFormat.format(I18n.getString("Bugtracker.DESCRIPTION"), "file:///" + settlersLobby.configDir));
				description.addHyperlinkListener(new HyperlinkListener() {
					public void hyperlinkUpdate(HyperlinkEvent e) {
		 		        if(e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
	 		        		if (Desktop.isDesktopSupported()) {
	 		        			settlersLobby.getGUI().openURL(e.getURL());
	 		        		}
		 		        }
		 		    }
		 		});
            }
		});
		JScrollPane scrollPane = new JScrollPane(description);
 		scrollPane.getViewport().setOpaque(false);
 		scrollPane.setOpaque(false);
 		scrollPane.setBorder(null);

 		GroupLayout gl_infoPanel = new GroupLayout(infoPanel);
 		gl_infoPanel.setAutoCreateGaps(true);
 		gl_infoPanel.setAutoCreateContainerGaps(true);
 		gl_infoPanel.setHorizontalGroup(
 				gl_infoPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_infoPanel.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 600, GroupLayout.PREFERRED_SIZE)
				)
		);
 		gl_infoPanel.setVerticalGroup(
 				gl_infoPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_infoPanel.createSequentialGroup()
					.addGroup(gl_infoPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
					)
				)
		);
		infoPanel.setLayout(gl_infoPanel); 		
		
		// create panel for buttons with buttons
		JPanel buttonPanel = new JPanel();
		JButton btnUpload = new JButton(I18n.getString("Bugtracker.BTN_UPLOAD"));
		JButton btnCancel = new JButton(I18n.getString("Bugtracker.BTN_CANCEL"));
		JButton btnManuell = new JButton(I18n.getString("Bugtracker.BTN_MANUALLY"));
		// add repeating backgroundImage to the buttons, if defined for actual theme
		if( this.settlersLobby.theme.isRepeatingButtonStyleAvailable() ) {
			btnUpload = this.settlersLobby.theme.getButtonWithStyle(btnUpload.getText());
			btnCancel = this.settlersLobby.theme.getButtonWithStyle(btnCancel.getText());
			btnManuell = this.settlersLobby.theme.getButtonWithStyle(btnManuell.getText());
		}
		// add specific backgroundImage to the buttons, if defined for actual theme
		// -> all together for consistent view
		if( settlersLobby.theme.isButtonStyleAvailable() ) {
			btnUpload = this.settlersLobby.theme.getButtonWithStyle(btnUpload.getText(), this.settlersLobby.theme.buttonBackgroundImage1, this.settlersLobby.theme.buttonBackgroundImage1Hover);
			btnCancel = this.settlersLobby.theme.getButtonWithStyle(btnCancel.getText(), this.settlersLobby.theme.buttonBackgroundImage1, this.settlersLobby.theme.buttonBackgroundImage1Hover);
			btnManuell = this.settlersLobby.theme.getButtonWithStyle(btnManuell.getText(), this.settlersLobby.theme.buttonBackgroundImage1, this.settlersLobby.theme.buttonBackgroundImage1Hover);
		}
		
		// add events to the buttons
		btnUpload.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// start upload and show result
				doUpload();
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnManuell.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MessageDialog messagewindow = new MessageDialog(null, I18n.getString("Bugtracker.MANUELL_UPLOAD_TITLE"), true);
		    	// -> add message
				messagewindow.addMessage(
					I18n.getString("Bugtracker.MANUELL_UPLOAD_DESCRIPTION"), 
					UIManager.getIcon("OptionPane.infoIcon")
				);
				// -> add ok-button
				messagewindow.addButton(
					UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
					new ActionListener() {
					    @Override
					    public void actionPerformed(ActionEvent e) {
					    	messagewindow.dispose();
					    	dispose();
					    }
					}
				);
				messagewindow.setSettlersLobby(settlersLobby);
				// show it
				messagewindow.setVisible(true);
			}
		});
		
		// create the button-panel-layout
		GroupLayout gl_buttonPanel = new GroupLayout(buttonPanel);
		gl_buttonPanel.setAutoCreateGaps(true);
		gl_buttonPanel.setAutoCreateContainerGaps(true);
		gl_buttonPanel.setHorizontalGroup(
			gl_buttonPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPanel.createSequentialGroup()
					.addComponent(btnUpload, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
					.addComponent(btnManuell, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
					.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
				)
		);
		gl_buttonPanel.setVerticalGroup(
			gl_buttonPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPanel.createSequentialGroup()
					.addGroup(gl_buttonPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(btnUpload, GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
						.addComponent(btnManuell, GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
						.addComponent(btnCancel))
				)
		);
		buttonPanel.setLayout(gl_buttonPanel);
		buttonPanel.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{btnUpload, btnCancel}));
		
		// create the main layout with all panels together		
		GroupLayout groupLayout = new GroupLayout(mainPanel);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(userdataPanel, GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE))
				)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(infoPanel, GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
				)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(buttonPanel, GroupLayout.PREFERRED_SIZE, 600, GroupLayout.PREFERRED_SIZE)
				)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(userdataPanel, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
					.addComponent(infoPanel, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
					.addComponent(buttonPanel, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
				)
		);
		
		mainPanel.setLayout(groupLayout);
		
		// add the content to the window
		this.setContentPane(mainPanel);
		
		// set on main screen
		this.settlersLobby.showOnSpecifiedScreen(this.settlersLobby.config.getScreen(), this);
		
		// prevent resizing
		this.setResizable(false);
		
		// pack the window to its minimum size
		this.pack();
	}

	/**
	 * Perform Upload of 3 log-files to external URL.
	 * Only if user has a nickname and min. 1 file exists.
	 */
	private void doUpload() {
		// get the log-files
		File file1 = new File(SettlersLobby.configDir, "log.txt");
        File file2 = new File(SettlersLobby.configDir, "log.1.txt");
        File file3 = new File(SettlersLobby.configDir, "vpn.log");
        
        // check if all necessary fields are filled
        // and show hint if not
        boolean go = true;
        if( txtNickname.getText().length() == 0 ) {
        	go = false;
        }
        if( txtTitle.getText().length() == 0 ) {
        	go = false;
        }
        if( txtMessage.getText().length() <= 4 ) {
        	go = false;
        }
        if( false == file1.exists() || false == file1.canRead() ) {
        	go = false;
        }
		if( false != go ) {
			// get a window with waiting-message
			LoadingJFrame waitingWindow = new LoadingJFrame(this);
			
			// define a swingWorker which will show a loading-window while http-request is not done
			SwingWorker<?, ?> swingWorker = new SwingWorker<Object, Object>() {
			
				private String response = "";

				@Override
				protected Object doInBackground() throws Exception {
					// create upload-object
					httpRequests httpRequests = new httpRequests();
					
					// -> ignore security-settings
					httpRequests.setIgnorePhpSessId(true);
					
					// -> add the URL
					httpRequests.setUrl("https://www.siedler3.net/xt.php?app=support");
					
					// -> add some parameter
					httpRequests.addParameter("lang", settlersLobby.config.getLanguage().getLanguage());
					httpRequests.addParameter("nickname", txtNickname.getText());
					httpRequests.addParameter("title", txtTitle.getText());
					httpRequests.addParameter("text", txtMessage.getText());
					
					// -> add file 1
					if( file1.exists() && file1.canRead() ) {
						httpRequests.addFile("file1", file1);
					}
					
					// -> add file 2
					if( file2.exists() && file2.canRead() ) {
						httpRequests.addFile("file2", file2);
					}
					
					// -> add file 3
					if( file3.exists() && file3.canRead() ) {
						httpRequests.addFile("file3", file3);
					}
					
					// -> start the upload and get the response-String
					//    which will contain the link to the posting if it was successull
					try {
						response  = httpRequests.doRequest().trim();
					} catch (IOException e1) {
					}
					return response;
				}
				
				/**
		         * Executed in event dispatching thread
		         */
		        @Override
		        public void done() {
		        	// close the waiting window
		        	waitingWindow.dispatchEvent(new WindowEvent(waitingWindow, WindowEvent.WINDOW_CLOSING));
		        	
		        	// if the response contains "error" as string OR is empty
		        	// something went wrong
		        	if( response.equals("error") || response.length() == 0 ) {
		        		// Show hint after Upload
						MessageDialog messagewindow = new MessageDialog(null, I18n.getString("Bugtracker.UPLOAD_TITLE"), true);
				    	// -> add message
						messagewindow.addMessage(
							I18n.getString("Bugtracker.UPLOAD_ERROR"), 
							UIManager.getIcon("OptionPane.errorIcon")
						);
						// -> add ok-button
						messagewindow.addButton(
							UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
							new ActionListener() {
							    @Override
							    public void actionPerformed(ActionEvent e) {
							    	messagewindow.dispose();
							    }
							}
						);
						messagewindow.setSettlersLobby(settlersLobby);
						// show it
						messagewindow.setVisible(true);		
		        	}
		        	else {
		        		// otherwise show success-message with the URL from the response
						MessageDialog messagewindow = new MessageDialog(null, I18n.getString("Bugtracker.UPLOAD_TITLE"), true);
				    	// -> add message
						messagewindow.addMessage(
							MessageFormat.format("<html>" + I18n.getString("Bugtracker.UPLOAD_DESCRIPTION") + "</html>", response.trim()), 
							UIManager.getIcon("OptionPane.infoIcon")
						);
						// -> add ok-button
						messagewindow.addButton(
							UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
							new ActionListener() {
							    @Override
							    public void actionPerformed(ActionEvent e) {
							    	messagewindow.dispose();
							    	// hide bugtracker-window
					        		dispose();
							    }
							}
						);
						messagewindow.setSettlersLobby(settlersLobby);
						// show it
						messagewindow.setVisible(true);        		
		        	}
		        }
			};
			swingWorker.execute();
		}
		else {
			// Show hint after Upload
			MessageDialog messagewindow = new MessageDialog(null, I18n.getString("Bugtracker.UPLOAD_TITLE"), true);
	    	// -> add message
			messagewindow.addMessage(
				I18n.getString("Bugtracker.REQUIREMENTS_MISSED"), 
				UIManager.getIcon("OptionPane.errorIcon")
			);
			// -> add ok-button
			messagewindow.addButton(
				UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
				new ActionListener() {
				    @Override
				    public void actionPerformed(ActionEvent e) {
				    	messagewindow.dispose();
				    }
				}
			);
			messagewindow.setSettlersLobby(settlersLobby);
			// show it
			messagewindow.setVisible(true);	
		}
	}
}