/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.IOException;

import javax.swing.JButton;

/*
 * Generate a replacement for JButton including backgroundImages
 * and mouseover-effects.
 */

public class ButtonImage extends JButton {
	
	// define variables
    private Image _backgroundImage;
    private Image _backgroundImageHover;
    private Image _useThisBackgroundImage;
    
    // call this function to generate the button
    public ButtonImage(String text, Image mainBackgroundImage, Image mainBackgroundImageHover) throws IOException {
    	super(text);
    	
    	// set the passed values to the variables
        this._backgroundImage = mainBackgroundImage;
        this._backgroundImageHover = mainBackgroundImageHover;
        
        // set the values which will be used
        // -> will be overriden by mouse-events
        this._useThisBackgroundImage = this._backgroundImage;
                
        // add the mouse-events
        this.addMouseListener(new addCursorChangesToButton());
        this.addMouseListener(new java.awt.event.MouseAdapter() {
        	
        	// if mouse enters the button change its images
            public void mouseEntered(java.awt.event.MouseEvent evt) {
            	// to not use "this" at this point to reference the variables
            	_useThisBackgroundImage = _backgroundImageHover;
           		// repaint the button to set the new images
            	repaint();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
            	// to not use "this" at this point to reference the variables
            	_useThisBackgroundImage = _backgroundImage;
           		// repaint the button to set the new images
            	repaint();
            }
        });
    }

    /*
     * Override the JButton-paintComponent-method to add the backgroundImages
     * (non-Javadoc)
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    @Override
    public void paintComponent(Graphics g) {
    	super.paintComponent(g);
    	
    	// adds the backgroundImage to the button
    	if( this._useThisBackgroundImage.toString().length() > 0 ) {
    		
    		// get its dimensions
	        int imagewidth = this._useThisBackgroundImage.getWidth(this);
	        int imageheight = this._useThisBackgroundImage.getHeight(this);
	        
	        // generate as 2D-Object
	        Graphics2D g2 = (Graphics2D)g.create();
	        
	        // calculate position for centered text depending on text- and button-width
	        Integer w2 = g2.getFontMetrics().stringWidth(getText()) / 2;
	        Integer posx = getWidth() / 2 - w2;
	        Integer posy = 18;
	        g2.drawString(getText(), posx, posy);
	        
	        if (imagewidth > 0 && imageheight > 0) {
	        	
	        	// draw the image to coordinates
	        	posx = posx - imagewidth - 12;
	            g2.drawImage(this._useThisBackgroundImage, posx, 2, imagewidth, imageheight, this);
	        }
	        
    	}
    }
}