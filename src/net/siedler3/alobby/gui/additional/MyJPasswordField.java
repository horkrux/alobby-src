package net.siedler3.alobby.gui.additional;

import javax.swing.JPasswordField;
import javax.swing.text.Document;

public class MyJPasswordField extends JPasswordField
{
    private final char initialChar = getEchoChar();

    public MyJPasswordField() {
    }

    public MyJPasswordField(String text) {
        super(text);
    }

    public MyJPasswordField(int columns) {
        super(columns);
    }

    public MyJPasswordField(String text, int columns) {
        super(text, columns);
    }

    public MyJPasswordField(Document doc, String txt, int columns) {
        super(doc, txt, columns);
    }

    public void setInputVisible(boolean value)
    {
        if (value)
        {
            setEchoChar('\0');
        }
        else
        {
            setEchoChar(initialChar);
        }
    }
}
