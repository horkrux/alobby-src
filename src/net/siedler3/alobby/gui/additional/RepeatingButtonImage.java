/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;

/*
 * Generate a replacement for JButton including backgroundImages
 * and mouseover-effects.
 * 
 * @author Zwirni
 */

public class RepeatingButtonImage extends JButton {
	
	// define variables
    private Image _repeatingBackgroundImage;
    private Image _repeatingBackgroundImageHover;
    private Image _repeatingBackgroundImageLeft;
    private Image _repeatingBackgroundImageLeftHover;
    private Image _repeatingBackgroundImageRight;
    private Image _repeatingBackgroundImageRightHover;
    private Image _useThisRepeatingBackgroundImage;
    private Image _useThisRepeatingBackgroundImageLeft;
    private Image _useThisRepeatingBackgroundImageRight;
    
    // call this function to generate the button
    public RepeatingButtonImage(String text, Image mainBackgroundImage, Image mainBackgroundImageHover) throws IOException {
    	super(text);
    	
    	// set the passed values to the variables
        this._repeatingBackgroundImage = mainBackgroundImage;
        this._repeatingBackgroundImageHover = mainBackgroundImageHover;
        
        // set the values which will be used
        // -> will be overriden by mouse-events
        this._useThisRepeatingBackgroundImage = this._repeatingBackgroundImage;
                
        // add the mouse-events
        this.addMouseListener(new addCursorChangesToButton());
        this.addMouseListener(new java.awt.event.MouseAdapter() {
        	
        	// if mouse enters the button change its images
            public void mouseEntered(java.awt.event.MouseEvent evt) {
            	// to not use "this" at this point to reference the variables
            	_useThisRepeatingBackgroundImage = _repeatingBackgroundImageHover;
           		// repaint the button to set the new images
            	repaint();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
            	// to not use "this" at this point to reference the variables
            	_useThisRepeatingBackgroundImage = _repeatingBackgroundImage;
           		// repaint the button to set the new images
            	repaint();
            }
        });
    }
    
    // call this function to generate the button
    public RepeatingButtonImage(String text, Image mainBackgroundImage, Image mainBackgroundImageHover, Image mainBackgroundImageLeft, Image mainBackgroundImageLeftHover, Image mainBackgroundImageRight, Image mainBackgroundImageRightHover) throws IOException {
    	super(text);
    	
    	// set the passed values to the variables
        this._repeatingBackgroundImage = mainBackgroundImage;
        this._repeatingBackgroundImageHover = mainBackgroundImageHover;
        this._repeatingBackgroundImageLeft = mainBackgroundImageLeft;
        this._repeatingBackgroundImageLeftHover = mainBackgroundImageLeftHover;
        this._repeatingBackgroundImageRight = mainBackgroundImageRight;
        this._repeatingBackgroundImageRightHover = mainBackgroundImageRightHover;
        
        // set the values which will be used
        // -> will be overriden by mouse-events
        this._useThisRepeatingBackgroundImage = this._repeatingBackgroundImage;
        this._useThisRepeatingBackgroundImageLeft = this._repeatingBackgroundImageLeft;
        this._useThisRepeatingBackgroundImageRight = this._repeatingBackgroundImageRight;

        // add the mouse-events
        this.addMouseListener(new addCursorChangesToButton());
        this.addMouseListener(new java.awt.event.MouseAdapter() {
        	
        	// if mouse clicks the button change its images back to normal
            public void mouseClicked(java.awt.event.MouseEvent evt) {
            	// to not use "this" at this point to reference the variables
            	_useThisRepeatingBackgroundImage = _repeatingBackgroundImage;
           		_useThisRepeatingBackgroundImageLeft = _repeatingBackgroundImageLeft;
           		_useThisRepeatingBackgroundImageRight = _repeatingBackgroundImageRight;
           		// repaint the button to set the new images
            	repaint();
            }
        	
        	// if mouse enters the button change its images
            public void mouseEntered(java.awt.event.MouseEvent evt) {
            	// to not use "this" at this point to reference the variables
            	_useThisRepeatingBackgroundImage = _repeatingBackgroundImageHover;
           		_useThisRepeatingBackgroundImageLeft = _repeatingBackgroundImageLeftHover;
           		_useThisRepeatingBackgroundImageRight = _repeatingBackgroundImageRightHover;
           		// repaint the button to set the new images
            	repaint();
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
            	// to not use "this" at this point to reference the variables
            	_useThisRepeatingBackgroundImage = _repeatingBackgroundImage;
            	_useThisRepeatingBackgroundImageLeft = _repeatingBackgroundImageLeft;
            	_useThisRepeatingBackgroundImageRight = _repeatingBackgroundImageRight;
           		// repaint the button to set the new images
            	repaint();
            }
        });
        
        this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
            	// to not use "this" at this point to reference the variables
            	_useThisRepeatingBackgroundImage = _repeatingBackgroundImage;
            	_useThisRepeatingBackgroundImageLeft = _repeatingBackgroundImageLeft;
            	_useThisRepeatingBackgroundImageRight = _repeatingBackgroundImageRight;
           		// repaint the button to set the new images
            	repaint();			
			}
        });
    };

    /*
     * Override the JButton-paintComponent-method to add the backgroundImages
     * (non-Javadoc)
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    @Override
    public void paintComponent(Graphics g) {
    	super.paintComponent(g);
    	
    	// adds the backgroundImage to the button
    	if( this._useThisRepeatingBackgroundImage.toString().length() > 0 ) {
    		
    		// get its dimensions
	        int imagewidth = this._useThisRepeatingBackgroundImage.getWidth(this);
	        int imageheight = this._useThisRepeatingBackgroundImage.getHeight(this);
	        
	        // generate as 2D-Object
	        Graphics2D g2 = (Graphics2D)g.create();
	        if (imagewidth > 0 && imageheight > 0) {
	        	
	        	// repeat the image on the horizontal scale to fill the button with it
	            for (int x = 0; x < getWidth(); x += imagewidth) {
	            	
	            	// if picture should also repeating on y-axis, than use this:
	            	// -> not used because no theme needs it at the moment
	                /*for (int y = 0; y < getHeight(); y += imageheight) {
	                    g2.drawImage(this._repeatingBackgroundImage, x, y, imagewidth, imageheight, this);
	                }*/
	            	
	            	// draw the image to coordinates
	            	g2.drawImage(this._useThisRepeatingBackgroundImage, x, 1, imagewidth, imageheight, this);
	            }
	        }
	        
	        // adds an optional backgroundImage on the left side of the button
	        if( this._useThisRepeatingBackgroundImageLeft instanceof Image && this._useThisRepeatingBackgroundImageLeft.toString().length() > 0 ) {
	        	Image bgleft = this._useThisRepeatingBackgroundImageLeft;
	        	g2.drawImage(bgleft, 0, 1, bgleft.getWidth(this), bgleft.getHeight(this), this);
    		}
	        // adds an optional backgroundImage on the right side of the button
    		if( this._useThisRepeatingBackgroundImageRight instanceof Image && this._useThisRepeatingBackgroundImageRight.toString().length() > 0 ) {
        		Image bgright = this._useThisRepeatingBackgroundImageRight;
        		g2.drawImage(bgright, this.getWidth()-bgright.getWidth(this), 1, bgright.getWidth(this), bgright.getHeight(this), this);
    		}
    		
	        // calculate position for centered text depending on text- and button-width
	        Integer w2 = g2.getFontMetrics().stringWidth(getText()) / 2;
	        Integer posx = getWidth() / 2 - w2;
	        g2.drawString(getText(), posx, 18);  
	        
    	}
    }
}