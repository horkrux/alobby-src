/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import net.siedler3.alobby.i18n.I18n;


/**
 * Extends TableModel to manage CommunityNews-Entries.
 *
 * @author Zwirni
 */
public class DefaultTableModelForCommunityNews extends DefaultTableModel implements Observer
{
	private static class NewsElement
	{
		public CommunityNewsElement communityNewsElement;
		public String toolTip;

		NewsElement(CommunityNewsElement communityNewsElement, String toolTip)
		{
			super();
			this.communityNewsElement = communityNewsElement;
			this.toolTip = toolTip;
		}
	}

	// List of all news in table
	private List<NewsElement> communityNews;

	/**
	 * Gets the instance for this object.
	 */
	public DefaultTableModelForCommunityNews() {
		// define table-rows
		super(null, new String[] { 
									I18n.getString("GUI.NEWS_DATE"), //$NON-NLS-1$
									I18n.getString("GUI.NEWS_SECTION"),
				                    I18n.getString("GUI.NEWS_TITLE"),
				                    "newmarker",
				                    "Prio"
				                 });
		
		// define list of the news as vector-object.
		communityNews = new Vector<NewsElement>();
	}

	/**
	 * Adds a news-element to the list.
	 * @param doNotSendNewMark 
	 * 
	 * @param newOpenGame	
	 */
	public synchronized void addNews(CommunityNewsElement communityNewsElement, boolean doNotSendNewMark)
	{
	    //internally store the openGame and the corresponding tooltip
		String toolTip = createToolTip(communityNewsElement);
		communityNews.add(new NewsElement(communityNewsElement, toolTip));
		String[] tableEntry = createTableEntry(communityNewsElement, doNotSendNewMark);
		addRow(tableEntry);

		communityNewsElement.addObserver(this);
	}
	
	/**
	 * Create a CommunityNewsElement from given parameters and adds it
	 * to the list.
	 * Including check if the given md5-hash is not used until now from another
	 * News-Element.
	 *
	 * @param prio	priority for since entry which is important for the order of the entries
	 * @param md5	md5-hash for the entry
	 * @param date	the date of the entry
	 * @param title	the title of the entry
	 * @param text 	the text for the entry which will be visible on mouseover
	 * @param link	the link for the entry, its optional
	 * @param type	the type of the entry
	 * @param doNotSendNewMark 
	 */
	public void addNews(String prio, String md5, String date, String title, String text, String link, String type, boolean doNotSendNewMark) {
		if( prio.length() > 0 && md5.length() > 0 && date.length() > 0 && title.length() > 0 && text.length() > 0 && type.length() > 0 ) {
			boolean addNews = true;
			// loop through actual news-list and search for the same md5
			for (int i = 0; i < communityNews.size(); i++) {
	            if( communityNews.get(i).communityNewsElement.getMd5().equals(md5) ) {
	            	addNews = false;
	            }
	        }
			if( false != addNews ) {
				// create the object for this news
				CommunityNewsElement communityNewsElement = new CommunityNewsElement();
				communityNewsElement.setPriority(prio);
				communityNewsElement.setMd5(md5);
				communityNewsElement.setDate(date);
				communityNewsElement.setTitle(title);
				communityNewsElement.setText(text);
				communityNewsElement.setLink(link);
				communityNewsElement.setType(type);
				this.addNews(communityNewsElement, doNotSendNewMark);			
			}
		}
	}
	
	/**
	 * Get an newsElement from specific index
	 */
	private NewsElement getNewsElementByIndex(int index) {
		return communityNews.get(index);
	}

    /**
	 * Returns CommunityNewsElement on given index.
	 * 
	 * @param index
	 * @return the CommunityNewsElement-object
	 */
	public CommunityNewsElement getCommunityNewsElementAt(int index) {
		NewsElement openGameElement = this.getNewsElementByIndex(index);
		if( openGameElement != null ) {
			return openGameElement.communityNewsElement;
		}
		return null;
	}

	/** returns the tooltip associated with the specified row.
	 *
	 * @param index row index
	 * @return returns the tooltip associated with the specified row
	 */
	public String getToolTipAt(int index) {
		NewsElement openGameElement = this.getNewsElementByIndex(index);
		if( openGameElement != null ) {
			return openGameElement.toolTip;
		}
		return null;
	}


	/**
	 * Generates a tooltip with detailed information about the news.
	 * 
	 * @param communityNewsElement 
	 * @param the text
	 * @return
	 */
    private String createToolTip(CommunityNewsElement communityNewsElement)
    {
    	StringBuilder sb = new StringBuilder();
        sb.append("<html>"); //$NON-NLS-1$
        sb.append("<strong>" + I18n.getString("GUI.NEWS_DATE") + ":</strong> " + communityNewsElement.getDateAsString() + "<br>");
        sb.append("<strong>" + I18n.getString("GUI.NEWS_TITLE") + ":</strong> " + communityNewsElement.getTitle() + "<br>");
        sb.append("<br>" + communityNewsElement.getText());
        sb.append("</html>");
    	return sb.toString();
    }

    /**
     * Generates a table-entry depending on given news-object.
     * @param doNotSendNewMark 
     * 
     * @param opengame
     * 
     * @return String[]
     */
    private static String[] createTableEntry(CommunityNewsElement communityNewsElement, boolean doNotSendNewMark)
    {
    	String newMark = "1";
    	if( false != doNotSendNewMark ) {
    		newMark = "0";
    	}
    	
    	// generate entry as String-Array with following entries:
    	// - date
    	// - section
    	// - title of the news
    	// - new-marker (will be reset until user has read the tab)
    	// - priority (will be used to sort the entries in the table)
        String[] tableEntry = {
    		communityNewsElement.getDateAsString(),
    		I18n.getString("GUI.NEWS_" + communityNewsElement.getType().toUpperCase()),
    		communityNewsElement.getTitle(),
    		newMark,
    		communityNewsElement.getPriority()
        };
        
        return tableEntry;
    }

	/**
     * Updates a single entry in the table.
     */
	@Override
	public void update(Observable o, Object arg) {
		
	}

	/**
	 * Remove news with specific type.
	 * 
	 * @param type
	 */
	public void removeNewsByType(String type) {
		// loop through actual news-list and search for items with the given type
		for (int i = 0; i < communityNews.size(); i++) {
	        if( communityNews.get(i).communityNewsElement.getType().equals(type) ) {
	        	communityNews.remove(i);
	        	removeRow(i);
	        }
	    }
	}
	
	/**
	 * Remove all entries from the container.
	 */
	public void clear() {
		communityNews.clear();
	}

	/**
	 * Remove news by its MD5-Hash.
	 * 
	 * @param type
	 */
	public void removeNewsByMd5(String md5) {
		// loop through actual news-list and search for items with the given md5
		for (int i = 0; i < communityNews.size(); i++) {
	        if( communityNews.get(i).communityNewsElement.getMd5().equals(md5) ) {
	        	communityNews.remove(i);
	        	removeRow(i);
	        }
	    }
	}

}
