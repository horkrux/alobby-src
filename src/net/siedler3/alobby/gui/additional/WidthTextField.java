/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.awt.Dimension;

import javax.swing.JTextField;

public class WidthTextField extends JTextField {

	private int maxWidth;
	private int minWidth;
	
	public WidthTextField(int minWidth, int maxWidth) {
		this.maxWidth = maxWidth;
		this.minWidth = minWidth;
	}
	
	public Dimension getPreferredSize() {
		Dimension result = super.getPreferredSize();
		if (result.width > maxWidth) {
			result.width = maxWidth;
		}
		if (result.width < minWidth) {
			result.width = minWidth;
		}
		return result;
	}
	
}
