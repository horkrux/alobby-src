package net.siedler3.alobby.gui.additional;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;

import javax.swing.JButton;

/**
 * Adds mouseAdapter for JButton which changes the cursor
 * if mouse enters and leaves the component.
 * 
 * Usage: JButtonComponent.addMouseListener(new actionMouseAdapter());
 * 
 * @author Zwirni
 */
public class addCursorChangesToButton extends MouseAdapter {
	
	// if mouse enters the button change its images
    public void mouseEntered(java.awt.event.MouseEvent evt) {
    	Object enteredObject = evt.getSource();
    	if( enteredObject instanceof JButton ) {
    		JButton button = (JButton)evt.getSource();
    		// set the cursor
    		button.setCursor(new Cursor(Cursor.HAND_CURSOR));
    	}
    }

    // set the cursor back to standard-cursor if mouse leaves the button
    public void mouseExited(java.awt.event.MouseEvent evt) {
    	Object enteredObject = evt.getSource();
    	if( enteredObject instanceof JButton ) {
    		JButton button = (JButton)evt.getSource();
    		button.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    	}
    }

}