package net.siedler3.alobby.gui.additional;
/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.i18n.I18n;

/**
 * Achtung Baustelle nur Zwischenstand committed!
 *
 * @author Stephan Bauer (aka maximilius)
 */
public class PopupBuilder {

	SettlersLobby settlersLobby;
	private String[] pillowFightPrefixArray = new String[]{
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_1_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_2_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_3_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_4_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_5_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_6_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_7_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_8_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_9_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_10_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_11_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_12_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_13_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_14_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_15_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_16_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_17_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_18_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_19_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_20_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_21_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_22_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_23_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_24_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_25_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_26_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_27_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_28_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_29_a"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_30_a")
	};
	private String[] pillowFightSuffixArray = new String[]{
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_1_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_2_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_3_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_4_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_5_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_6_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_7_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_8_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_9_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_10_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_11_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_12_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_13_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_14_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_15_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_16_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_17_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_18_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_19_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_20_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_21_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_22_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_23_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_24_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_25_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_26_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_27_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_28_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_29_b"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_30_b")
	};
	private String[] eggFightPrefixArray = new String[]{
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_1_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_2_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_3_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_4_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_5_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_6_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_7_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_8_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_9_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_10_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_11_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_12_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_13_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_14_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_15_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_16_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_17_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_18_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_19_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_20_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_21_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_22_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_23_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_24_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_25_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_26_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_27_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_28_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_29_a_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_30_a_easter")
	};
	private String[] eggFightSuffixArray = new String[]{
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_1_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_2_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_3_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_4_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_5_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_6_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_7_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_8_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_9_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_10_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_11_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_12_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_13_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_14_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_15_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_16_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_17_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_18_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_19_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_20_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_21_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_22_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_23_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_24_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_25_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_26_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_27_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_28_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_29_b_easter"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_30_b_easter")
	};
	private String[] halloweenFightPrefixArray = new String[]{
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_1_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_2_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_3_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_4_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_5_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_6_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_7_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_8_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_9_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_10_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_11_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_12_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_13_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_14_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_15_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_16_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_17_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_18_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_19_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_20_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_21_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_22_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_23_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_24_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_25_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_26_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_27_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_28_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_29_a_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_30_a_halloween")
	};
	private String[] halloweenFightSuffixArray = new String[]{
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_1_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_2_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_3_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_4_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_5_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_6_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_7_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_8_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_9_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_10_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_11_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_12_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_13_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_14_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_15_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_16_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_17_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_18_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_19_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_20_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_21_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_22_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_23_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_24_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_25_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_26_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_27_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_28_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_29_b_halloween"),
		I18n.getString("PopupBuilder.MSG_PILLOW_FIGHT_30_b_halloween")
	}; 
	private String[] snowballFightPrefixArray = new String[]{
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_1_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_2_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_3_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_4_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_5_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_6_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_7_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_8_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_9_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_10_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_11_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_12_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_13_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_14_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_15_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_16_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_17_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_18_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_19_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_20_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_21_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_22_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_23_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_24_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_25_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_26_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_27_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_28_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_29_a"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_30_a")
	};
	private String[] snowballFightSuffixArray = new String[]{
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_1_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_2_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_3_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_4_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_5_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_6_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_7_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_8_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_9_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_10_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_11_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_12_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_13_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_14_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_15_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_16_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_17_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_18_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_19_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_20_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_21_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_22_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_23_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_24_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_25_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_26_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_27_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_28_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_29_b"),
		I18n.getString("PopupBuilder.MSG_SNOWBALL_FIGHT_30_b")
	};
	private List<Calendar> comfortTimes = new ArrayList<Calendar>();
	private String[] popcornArray = new String[] {
		"Popcorn ist alle :-(",
		"hat all sein Popcorn aufgegessen :-(",
		"braucht eine neue Popcorn-Packung",
		"schmeißt vor Freude eine Popcorn-Packung in den Chat"
	};

	public PopupBuilder(SettlersLobby settlersLobby) {
		this.settlersLobby = settlersLobby;
	}

	public JPopupMenu cr8PopupMenu(S3User user) {
		JPopupMenu popupMenu = new JPopupMenu();

		JLabel lblNickname = new JLabel(user.getNick());
		lblNickname.setForeground(Color.BLACK);
		popupMenu.add(lblNickname);
	    popupMenu.addSeparator();
	    if (!settlersLobby.getNick().equals(user.getNick())) {
			addWhsiper(popupMenu, user);
			if (user.isIgnored()) {
				addIgnore(popupMenu, I18n.getString("PopupBuilder.ENTRY_UNIGNORE"), user); //$NON-NLS-1$
			} else {
				addIgnore(popupMenu, I18n.getString("PopupBuilder.ENTRY_IGNORE"), user); //$NON-NLS-1$
			}
			if (user.isBuddy()) {
				addBuddy(popupMenu, I18n.getString("PopupBuilder.ENTRY_NO_BUDDY"), user); //$NON-NLS-1$
			} else {
				addBuddy(popupMenu, I18n.getString("PopupBuilder.ENTRY_BUDDY"), user); //$NON-NLS-1$
			}
			if (settlersLobby.getIRCCommunicator().getUserByNick(settlersLobby.getNick()).isOp())
			{
				addBan(popupMenu, user);
				addKick(popupMenu, user);
				addVPNCheck(popupMenu, user);
			}
			popupMenu.addSeparator();
			if( this.settlersLobby.config.isChristmasTheme() ) {
				addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_HUGS"), I18n.getString("PopupBuilder.MSG_PACK"), I18n.getString("PopupBuilder.MSG_WRAPPING_PAPER")); //$NON-NLS-1$ //$NON-NLS-2$
				addComfortCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_COMFORT"), I18n.getString("PopupBuilder.MSG_COMFORT1"), I18n.getString("PopupBuilder.MSG_COMFORT3")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_CHALLENGE"), I18n.getString("PopupBuilder.MSG_CHALLENGE1"), I18n.getString("PopupBuilder.MSG_CHALLENGE3")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_SNOWBALL_FIGHT"), snowballFightPrefixArray, snowballFightSuffixArray); //$NON-NLS-1$
			}
			else {
				if( this.settlersLobby.config.isHalloweenTheme() ) {
					addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_SCARE"), I18n.getString("PopupBuilder.MSG_CHALLENGE4"), null); //$NON-NLS-1$ //$NON-NLS-2$
					addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_BLOOD_KNIFE"), I18n.getString("PopupBuilder.MSG_COMFORT1"), I18n.getString("PopupBuilder.MSG_COMFORT4")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_TRICK_OR_TREATING"), I18n.getString("PopupBuilder.MSG_CHALLENGE1"), I18n.getString("PopupBuilder.MSG_CHALLENGE5")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_PUMPKIN_FIGHT"), halloweenFightPrefixArray, halloweenFightSuffixArray); //$NON-NLS-1$
				}
				else {
					if( this.settlersLobby.config.isEasterTheme() ) {
						addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_HIDE_EGGS"), I18n.getString("PopupBuilder.MSG_HIDE_EGG"), null); //$NON-NLS-1$ //$NON-NLS-2$
						addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_SEARCH_EGGS"), I18n.getString("PopupBuilder.MSG_SEARCH_EGGS"), I18n.getString("PopupBuilder.MSG_SEARCH_EGGS_POST")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_EGGNOG"), I18n.getString("PopupBuilder.MSG_EGGNOG"), I18n.getString("PopupBuilder.MSG_EGGNOG_POST")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_EGG_FIGHT"), eggFightPrefixArray, eggFightSuffixArray); //$NON-NLS-1$
					}
					else {
						addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_HUGS"), I18n.getString("PopupBuilder.MSG_HUGS"), null); //$NON-NLS-1$ //$NON-NLS-2$
						addComfortCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_COMFORT"), I18n.getString("PopupBuilder.MSG_COMFORT1"), I18n.getString("PopupBuilder.MSG_COMFORT2")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_CHALLENGE"), I18n.getString("PopupBuilder.MSG_CHALLENGE1"), I18n.getString("PopupBuilder.MSG_CHALLENGE2")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						addMeCommand(popupMenu, user, I18n.getString("PopupBuilder.ENTRY_PILLOW_FIGHT"), pillowFightPrefixArray, pillowFightSuffixArray); //$NON-NLS-1$
					}
				}
			}
	    } else {
			if (user.isAway()) {
				addBack(popupMenu);
			} else {
				addAway(popupMenu);
			}
			popupMenu.addSeparator();
			if( this.settlersLobby.config.isChristmasTheme() ) {
				addMeCommand(popupMenu, null, I18n.getString("PopupBuilder.ENTRY_BYE"), I18n.getString("PopupBuilder.MSG_BYE2"), null); //$NON-NLS-1$ //$NON-NLS-2$
				addMeCommand(popupMenu, null, I18n.getString("PopupBuilder.ENTRY_HELLO"), I18n.getString("PopupBuilder.MSG_HELLO2"), null); //$NON-NLS-1$ //$NON-NLS-2$
				addMeCommand(popupMenu, null, I18n.getString("PopupBuilder.ENTRY_BEER"), I18n.getString("PopupBuilder.MSG_WINE"), null); //$NON-NLS-1$ //$NON-NLS-2
				addMeCommand(popupMenu, I18n.getString("PopupBuilder.ENTRY_POPCORN"), popcornArray); //$NON-NLS-1$
			}
			else {
				if( this.settlersLobby.config.isHalloweenTheme() ) {
					addMeCommand(popupMenu, null, I18n.getString("PopupBuilder.ENTRY_BYE"), I18n.getString("PopupBuilder.MSG_BYE3"), null); //$NON-NLS-1$ //$NON-NLS-2$
					addMeCommand(popupMenu, null, I18n.getString("PopupBuilder.ENTRY_HELLO"), I18n.getString("PopupBuilder.MSG_HELLO3"), null); //$NON-NLS-1$ //$NON-NLS-2$
					addMeCommand(popupMenu, null, I18n.getString("PopupBuilder.ENTRY_WITCHBLOOD"), I18n.getString("PopupBuilder.MSG_WITCHBLOOD"), null); //$NON-NLS-1$ //$NON-NLS-2
					addMeCommand(popupMenu, I18n.getString("PopupBuilder.ENTRY_POPCORN"), popcornArray); //$NON-NLS-1$
				}
				else {
					if( this.settlersLobby.config.isEasterTheme() ) {
						addMeCommand(popupMenu, null, I18n.getString("PopupBuilder.ENTRY_BYE"), I18n.getString("PopupBuilder.MSG_BYE4"), null); //$NON-NLS-1$ //$NON-NLS-2$
						addMeCommand(popupMenu, null, I18n.getString("PopupBuilder.ENTRY_HELLO"), I18n.getString("PopupBuilder.MSG_HELLO4"), null); //$NON-NLS-1$ //$NON-NLS-2$
						addMeCommand(popupMenu, null, I18n.getString("PopupBuilder.ENTRY_EGG_LIQUEUR"), I18n.getString("PopupBuilder.MSG_EGG_LIQUEUR"), null); //$NON-NLS-1$ //$NON-NLS-2$
						addMeCommand(popupMenu, I18n.getString("PopupBuilder.ENTRY_POPCORN"), popcornArray); //$NON-NLS-1$
					}
					else {
						addMeCommand(popupMenu, null, I18n.getString("PopupBuilder.ENTRY_BYE"), I18n.getString("PopupBuilder.MSG_BYE"), null); //$NON-NLS-1$ //$NON-NLS-2$
						addMeCommand(popupMenu, null, I18n.getString("PopupBuilder.ENTRY_HELLO"), I18n.getString("PopupBuilder.MSG_HELLO"), null); //$NON-NLS-1$ //$NON-NLS-2$
						addMeCommand(popupMenu, null, I18n.getString("PopupBuilder.ENTRY_BEER"), I18n.getString("PopupBuilder.MSG_BEER"), null); //$NON-NLS-1$ //$NON-NLS-2$
						addMeCommand(popupMenu, I18n.getString("PopupBuilder.ENTRY_POPCORN"), popcornArray); //$NON-NLS-1$
					}
				}
			}
			if( settlersLobby.getIRCCommunicator().getUserByNick(settlersLobby.getNick()).isOp() ) {
				popupMenu.addSeparator();
				addShowBannedList(popupMenu);
			}
		}
		return popupMenu;
	}

	private void addIgnore(JPopupMenu popupMenu, String text, S3User user) {
		JMenuItem mi = new JMenuItem(text);
		mi.addActionListener(new TurnIgnoreCommand(user, settlersLobby.getGUI()));
		popupMenu.add(mi);
	}

	private void addBuddy(JPopupMenu popupMenu, String text, S3User user) {
		JMenuItem mi = new JMenuItem(text);
		mi.addActionListener(new TurnBuddyCommand(user));
		popupMenu.add(mi);
	}

	private void addBack(JPopupMenu popupMenu) {
		JMenuItem mi = new JMenuItem(I18n.getString("PopupBuilder.ENTRY_BACK")); //$NON-NLS-1$
		mi.addActionListener(new BackCommand());
		popupMenu.add(mi);
	}

	private void addAway(JPopupMenu popupMenu) {
		JMenuItem mi = new JMenuItem(I18n.getString("PopupBuilder.ENTRY_AWAY")); //$NON-NLS-1$
		mi.addActionListener(new AwayCommand());
		popupMenu.add(mi);
	}
	
	private void addShowBannedList(JPopupMenu popupMenu) {
		JMenuItem mi = new JMenuItem(I18n.getString("PopupBuilder.ENTRY_SHOWBANNED")); //$NON-NLS-1$
		mi.addActionListener(new showBannedList());
		popupMenu.add(mi);
	}
	
	private void addBan(JPopupMenu popupMenu, S3User user) {
		JMenuItem mi = new JMenuItem(I18n.getString("PopupBuilder.ENTRY_BAN")); //$NON-NLS-1$
		mi.addActionListener(new BanCommand(user));
		popupMenu.add(mi);
	}

	private void addKick(JPopupMenu popupMenu, S3User user) {
		JMenuItem mi = new JMenuItem(I18n.getString("PopupBuilder.ENTRY_KICK")); //$NON-NLS-1$
		mi.addActionListener(new KickCommand(user));
		popupMenu.add(mi);
	}
	
	private void addVPNCheck(JPopupMenu popupMenu, S3User user) {
		JMenuItem mi = new JMenuItem(I18n.getString("PopupBuilder.ENTRY_VPNCHECK")); //$NON-NLS-1$
		mi.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GUI.openURL("https://lobby.siedler3.net/vpnrulescheck/vpncheck/" + user.getNick().toLowerCase());				
			}
		});
		popupMenu.add(mi);
	}

	private void addMeCommand(JPopupMenu popupMenu, S3User user, String menuText, String praefix, String suffix) {
		JMenuItem mi = new JMenuItem(menuText);
		mi.addActionListener(new MeCommand(user, praefix, suffix));
		popupMenu.add(mi);
	}
	
	private void addMeCommand(JPopupMenu popupMenu, S3User user, String menuText, String[] prefixArray, String[] suffixArray) {
		JMenuItem mi = new JMenuItem(menuText);
		mi.addActionListener(new MeCommandDynamic(user, prefixArray, suffixArray));
		popupMenu.add(mi);
	}
	
	private void addMeCommand(JPopupMenu popupMenu,String menuText, String[] text) {
		JMenuItem mi = new JMenuItem(menuText);
		mi.addActionListener(new MeCommandDynamicForNoUser(text));
		popupMenu.add(mi);
	}

	private void addWhsiper(JPopupMenu popupMenu, S3User user) {
		JMenuItem mi = new JMenuItem(I18n.getString("PopupBuilder.ENTRY_PRIVATE_MESSAGE")); //$NON-NLS-1$
		mi.addActionListener(new WhisperCommand(user));
		popupMenu.add(mi);
	}

	private class TurnIgnoreCommand implements ActionListener {
		private S3User user;
		private GUI gui;

		public TurnIgnoreCommand(S3User user, GUI gui) {
			this.user = user;
			this.gui = gui;
		}

		@Override
        public void actionPerformed(ActionEvent e) {
			user.setIgnored(!user.isIgnored());
			settlersLobby.getIRCCommunicator().doIgnore(user.getNick());
			gui.updateListUserOnline();
		}
	}

	private class TurnBuddyCommand implements ActionListener {
		private S3User user;

		public TurnBuddyCommand(S3User user) {
			this.user = user;
		}

		@Override
        public void actionPerformed(ActionEvent e) {
			user.setBuddy(!user.isBuddy());
			if (user.isBuddy()) {
				settlersLobby.config.addBuddy(user.getNick());
			} else {
				settlersLobby.config.removeBuddy(user.getNick());
			}
			settlersLobby.getIRCCommunicator().updateUserListInGui();
		}
	}

	private class BackCommand implements ActionListener {
		@Override
        public void actionPerformed(ActionEvent arg0) {
			settlersLobby.doBack();
		}
	}

	private class AwayCommand implements ActionListener {
		@Override
        public void actionPerformed(ActionEvent arg0) {
			String awayText = JOptionPane.showInputDialog(I18n.getString("PopupBuilder.TITLE_AWAY_MESSAGE"), settlersLobby.config.getDefaultAwayMsg()); //$NON-NLS-1$
			settlersLobby.doAway(awayText);
		}
	}
	
	private class showBannedList implements ActionListener {
		@Override
        public void actionPerformed(ActionEvent arg0) {
			settlersLobby.getIRCCommunicator().doBanlist();
		}
	}
	
	private class KickCommand implements ActionListener {
		private S3User user;

		public KickCommand(S3User user) {
			this.user = user;
		}
		@Override
        public void actionPerformed(ActionEvent arg0) {
			String reasonText = JOptionPane.showInputDialog(I18n.getString("PopupBuilder.TITLE_KICK_REASON"), I18n.getString("PopupBuilder.INPUT_KICK_REASON")); //$NON-NLS-1$ //$NON-NLS-2$
			// only kick, if the user didn't click on "Cancel"
			if (reasonText != null)
			{
				settlersLobby.getIRCCommunicator().doKick(user.getNick(), reasonText);
			}
		}
	}

	private class BanCommand implements ActionListener {
		private S3User user;

		public BanCommand(S3User user) {
			this.user = user;
		}

		@Override
        public void actionPerformed(ActionEvent arg0) {
			settlersLobby.getIRCCommunicator().doBan(user.getNick());
		}
	}

	private class MeCommand implements ActionListener {
		private S3User user;
		private String praefix, suffix;

		public MeCommand(S3User user, String praefix, String suffix) {
			this.user = user;
			this.praefix = praefix;
			this.suffix = suffix;
		}

		@Override
        public void actionPerformed(ActionEvent arg0) {
			if (user != null) {
				if (suffix != null) {
					settlersLobby.actionChatMessage("/me " + praefix + " " + user.getNick() + " " + suffix); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				} else {
					settlersLobby.actionChatMessage("/me " + praefix + " " + user.getNick()); //$NON-NLS-1$ //$NON-NLS-2$
				}
			} else {
				settlersLobby.actionChatMessage("/me " + praefix); //$NON-NLS-1$
			}
		}
	}
	
	/**
	 * Get and output random text for another user.
	 * 
	 * @author Zwirni
	 */
	private class MeCommandDynamic implements ActionListener {
		private S3User user;
		private String[] prefixArray;
		private String[] suffixArray;

		public MeCommandDynamic(S3User user, String[] prefixArray, String[] suffixArray) {
			this.user = user;
			this.prefixArray = prefixArray;
			this.suffixArray = suffixArray;
		}

		@Override
        public void actionPerformed(ActionEvent arg0) {
			if (user != null) {
				// get random array-entry
				int randomArrayIndex = new Random().nextInt(this.prefixArray.length);
				String command = "/me " + this.prefixArray[randomArrayIndex].trim() + " " + user.getNick() + " " + this.suffixArray[randomArrayIndex]; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				if( this.suffixArray[randomArrayIndex].length() <= 1 ) {
					command = "/me " + this.prefixArray[randomArrayIndex].trim() + " " + user.getNick() + "" + this.suffixArray[randomArrayIndex]; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$	
				}
				settlersLobby.actionChatMessage(command); 
			}
		}
	}
	
	/**
	 * Get and output random text for the own user.
	 * 
	 * @author Zwirni
	 *
	 */
	private class MeCommandDynamicForNoUser implements ActionListener {
		private String[] text;

		public MeCommandDynamicForNoUser(String[] text) {
			this.text = text;
		}

		@Override
        public void actionPerformed(ActionEvent arg0) {
			// get random array-entry
			int randomArrayIndex = new Random().nextInt(this.text.length);
			String command = "/me " + this.text[randomArrayIndex].trim(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			if( this.text[randomArrayIndex].length() <= 1 ) {
				command = "/me " + this.text[randomArrayIndex].trim(); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$	
			}
			settlersLobby.actionChatMessage(command); 
		}
	}

	private class WhisperCommand implements ActionListener
	{
		private S3User user;

		public WhisperCommand(S3User user)
		{
			this.user = user;
		}

		@Override
        public void actionPerformed(ActionEvent arg0)
		{
			settlersLobby.getGUI().getChatPane().selectMessageRecipient(user);
		}
	}

	/**
	 * Manipulate the comfort-command: only 10 times in 10 minutes.
	 * Except its christmas-time or winter.
	 * 
	 * @param popupMenu
	 * @param user
	 * @param text1
	 * @param text2
	 * @param text3
	 */
	private void addComfortCommand( JPopupMenu popupMenu, S3User user, String text1, String text2, String text3 ) {
		checkComfortTime();
		if( comfortTimes.size() < 10 || this.settlersLobby.config.itsWinter() || this.settlersLobby.config.itsChristmastime() ) {
			addMeCommand(popupMenu, user, text1, text2, text3); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			// add actual timestamp to dates-array
			comfortTimes.add(Calendar.getInstance());
		}
		else {
			addMeCommand(popupMenu, user, text1, I18n.getString("PopupBuilder.ENTRY_NO_COMFORT"), "");
		}
	}
	
	/**
	 * Check the comfortTime-Array.
	 * Remove all entries which are older than 10 minutes  
	 */
	private void checkComfortTime() {
		Calendar now = Calendar.getInstance();
		for (int x = 0; x < comfortTimes.size(); x++) {
			if( now.getTimeInMillis() - comfortTimes.get(x).getTimeInMillis() >= 10*60*1000 ) {
				comfortTimes.remove(x);
			}
		}
	}

}
