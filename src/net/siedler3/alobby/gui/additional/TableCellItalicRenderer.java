package net.siedler3.alobby.gui.additional;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Renderer for italic written table-cells.
 * 
 * @author Zwirni
 */
public class TableCellItalicRenderer extends DefaultTableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        	int realRow = table.convertRowIndexToModel(row);
            Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, realRow, column);
            if (table.getModel().getValueAt(realRow, 3).equals("1") ) {
            	c.setFont(c.getFont().deriveFont(Font.ITALIC));
            }
            return c;
        }
    }