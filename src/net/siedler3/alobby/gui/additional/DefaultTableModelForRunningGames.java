/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.S3Map;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.i18n.I18n;


/**
 * Extends TableModel for running games.
 *
 * @author Zwirni
 */
public class DefaultTableModelForRunningGames extends DefaultTableModel implements Observer
{
	
	private static final int COLUMN_INDEX_GAME_NAME = 0;
	
	private static class OpenGameElement
	{
		public OpenGame runningGame;
		public String toolTip;

		OpenGameElement(OpenGame runningGame, String toolTip)
		{
			super();
			this.runningGame = runningGame;
			this.toolTip = toolTip;
		}
	}

	private List<OpenGameElement> runningGames;

	/**
	 * Liefert eine Instanz
	 * @param data			Daten für das Tablemodel.
	 * @param columnNames	Überschriften für das Tablemodel.
	 */
	public DefaultTableModelForRunningGames() {
		super(null, new String[] {  I18n.getString("GUI.STARTTIME"), //$NON-NLS-1$
									I18n.getString("GUI.NAME"), //$NON-NLS-1$
									I18n.getString("GUI.MAP"), //$NON-NLS-1$
									I18n.getString("GUI.PLAYERS"), //$NON-NLS-1$
									I18n.getString("GUI.OPTIONS"),
									I18n.getString("GUI.GAME_VERSION"),
									I18n.getString("GUI.LEAGUE_GAME") //$NON-NLS-1$
				                 });
		runningGames = new Vector<OpenGameElement>();
		//league game is currently the last column, so just remove it.
		if (Configuration.getInstance().isHideLeagueInterface())
		{
		    setColumnCount(getColumnCount() - 1);
		}
	}

	/**
	 * Fügt ein OpenGame dem TableModel hinzu und registriert sich
	 * als Observer für eventuelle Änderungen des OpenGame.
	 * @param newOpenGame	Das hinzuzufügende OpenGame.
	 */
	public synchronized void addRunningGame(OpenGame newRunningGame)
	{
	    //internally store the openGame and the corresponding tooltip
		String toolTip = createToolTip(newRunningGame);
		runningGames.add(new OpenGameElement(newRunningGame, toolTip));

		String[] gameAsTableEntry = createTableEntry(newRunningGame);
		addRow(gameAsTableEntry);

		newRunningGame.addObserver(this);
	}
	
	/**
	 * Get an openGameElement from specific index
	 */
	private OpenGameElement getOpenGameElementByIndex(int index) {
		return runningGames.get(index);
	}

    /**
	 * Liefert das OpenGame mit dem übergebenen index.
	 * Kann benutzt werden für die selektierte Zeile der Tabelle
	 * @param index Der Index.
	 * @return Das OpenGame mit dem Index.
	 */
	public OpenGame getRunningGameAt(int index) {
		OpenGameElement openGameElement = this.getOpenGameElementByIndex(index);
		if( openGameElement != null ) {
			return openGameElement.runningGame;
		}
		return null;
	}

	/** returns the tooltip associated with the specified row.
	 *
	 * @param index row index
	 * @return returns the tooltip associated with the specified row
	 */
	public String getToolTipAt(int index) {
	    OpenGameElement openGameElement = this.getOpenGameElementByIndex(index);
		if( openGameElement != null ) {
			return openGameElement.toolTip;
		}
		return null;
	}

	/**
	 * erzeugt aus der List der Spieler einen Tooltip.
	 * @param playerList
	 * @return
	 */
    private String createToolTip(OpenGame game)
    {
    	List<String> playerList = game.getPlayerList();
        if (playerList.size() == 0)
        {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<html>"); //$NON-NLS-1$
        for (String player : playerList)
        {
            sb.append(player);
            sb.append("<br>"); //$NON-NLS-1$
        }
        sb.append("</html>"); //$NON-NLS-1$
        return sb.toString();
    }

    private static String[] createTableEntry(OpenGame opengame)
    {
        String amazonen = opengame.isAmazonen() ? I18n.getString("GUI.AMAZONS") : ""; //$NON-NLS-1$ //$NON-NLS-2$
        String trojans = opengame.isTrojans() ? I18n.getString("GUI.TROJANS") : ""; //$NON-NLS-1$ //$NON-NLS-2$
        String coop = opengame.isCoop() ? I18n.getString("GUI.COOP") : ""; //$NON-NLS-1$ //$NON-NLS-2$
        String wimo = opengame.isWimo() ? I18n.getString("GUI.WIMO") : "";
        String isCompetition = opengame.isLeagueGame() ? I18n.getString("GUI.LEAGUE") : ""; //$NON-NLS-1$ //$NON-NLS-2$
        if( opengame.isTournamentGame() ) {
        	isCompetition = opengame.getTournamentName();
        }
        
        String gameVersion = "";
        if (opengame.getMisc() == MiscType.ALOBBY)
        {
        	gameVersion = I18n.getString("GUI.VERSION_S3_CD_GOG");
        } else if (opengame.getMisc() == MiscType.GOG)
        {
        	gameVersion = I18n.getString("GUI.VERSION_S3_GOG");
        } else if (opengame.getMisc() == MiscType.CD)
        {
        	gameVersion = I18n.getString("GUI.VERSION_S3_CD");
        } else if (opengame.getMisc() == MiscType.S4)
        {
        	gameVersion = I18n.getString("GUI.VERSION_S4_CD_GOG");
        } else if (opengame.getMisc() == MiscType.S3CE)
        {
        	gameVersion = I18n.getString("GUI.VERSION_S3_CE");
        }
        
        // connect output for amaz and wimo in option-row
        String options = amazonen + trojans + coop + wimo;
        if( amazonen.length() > 0 && wimo.length() > 0 ) {
        	options = amazonen + ", " + wimo;
        }
        // also for trojans and wimo
        if( trojans.length() > 0 && wimo.length() > 0 && coop.length() == 0 ) {
        	options = trojans + ", " + wimo;
        }
        // also for trojans and coop
        if( trojans.length() > 0 && coop.length() > 0 && wimo.length() == 0 ) {
        	options = trojans + ", " + coop;
        }
               
        String[] gameAsTableEntry = {
			OpenGame.TIME_FORMAT2.format(new Date(opengame.getRunningGameStartTimestamp())).toString(),
	        opengame.getName(),
	        S3Map.getDisplayName(opengame.getMap()),
	        String.valueOf(opengame.getCurrentPlayer()),
	        options,
	        gameVersion,
	        isCompetition
        };
        return gameAsTableEntry;
    }

	@Override
	public void update(Observable observable, Object change) {
		if (observable instanceof OpenGame && change instanceof String)
		{
			OpenGame changedOpenGame = (OpenGame) observable;

			//use a synchronized block so that nothing else can
			//modify the openGames List
			synchronized (this)
			{
				for (int i = 0; i < runningGames.size(); i++)
				{
					OpenGame currentOpenGame = runningGames.get(i).runningGame;
					if (changedOpenGame == currentOpenGame)
					{
						if (change.equals(OpenGame.NEW_NAME))
						{
							setValueAt(currentOpenGame.getName(), i, COLUMN_INDEX_GAME_NAME);
						}
						else if (change.equals(OpenGame.DESTROYED))
						{
						    Test.output("removing game: " + currentOpenGame.toString());
							runningGames.remove(i);
							removeRow(i);
							Test.output("removed game");
						}
						break;
					}
				}
			}
		}
	}

}
