/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.util.Locale;

import javax.swing.JComboBox;

import net.siedler3.alobby.i18n.I18n;

/**
 * Fertigt eine 
 * @author Stephan Bauer (aka maximilius)
 */
public class LanguageComboBox extends JComboBox<String> {
    
    public LanguageComboBox() {
		super.addItem("Deutsch");
		super.addItem("English");
		super.addItem("Polski");
	}
	
	/**
	 * Liefert die Locale zum markierten Element.
	 * @return Die zum markierten Element korrespondierende Locale.
	 */
	public Locale getSelectedLocale() {
		switch (getSelectedIndex()) {
		case 0:
			return Locale.GERMAN;
		case 1:
			return Locale.ENGLISH;
		case 2:
			return I18n.LOCALE_POLISH;
		default:
			return Locale.ENGLISH;
		}
	}	

	/**
	 * Markiert den zur übergebenen Locale entsprechenden Eintrag.
	 * Tut nichts, wenn die Locale nicht unterstützt wird.
	 * @param locale Die neue Locale
	 */
	public void setSelectedLocale(Locale locale) {
		if (locale.equals(Locale.GERMAN)) {
			setSelectedIndex(0);
		} else if (locale.equals(Locale.ENGLISH)) {
			setSelectedIndex(1);
		} else if (locale.equals(I18n.LOCALE_POLISH)) {
			setSelectedIndex(2);  
		}  
	}
	
	/**
	 * Tut nichts.
	 * Es darf sich nichts an der Elementliste verändern.
	 */
	@Override
	public void addItem(String o) {
	}
	
	/**
	 * Tut nichts.
	 * Es darf sich nichts an der Elementliste verändern.
	 */
	@Override
	public void removeAllItems() {
	}
	
	/**
	 * Tut nichts.
	 * Es darf sich nichts an der Elementliste verändern.
	 */
	@Override
	public void removeItem(Object o) {
	}
	
	/**
	 * Tut nichts.
	 * Es darf sich nichts an der Elementliste verändern.
	 */
	@Override
	public void removeItemAt(int i) {
	}	
}
