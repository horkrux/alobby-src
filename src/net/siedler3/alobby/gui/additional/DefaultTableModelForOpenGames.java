/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.util.*;

import javax.swing.table.DefaultTableModel;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.GoodsInStock;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.controlcenter.S3Map;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;

/**
 * Erweitert DefaultTableModel so, dass man OpenGames hinzufügen kann.
 * Meldet sich als Observer bei den OpenGames an und aktualisiert sich
 * automatisch, wenn sie sich ändern.
 * Man bekommt das OpenGame geliefert, wenn man den selektierten Index der
 * Tabelle angibt.
 *
 * @author Stephan Bauer (aka maximilius)
 */
public class DefaultTableModelForOpenGames extends DefaultTableModel implements Observer
{
	private static class OpenGameElement
	{
		public OpenGame openGame;
		public String toolTip;

		OpenGameElement(OpenGame openGame, String toolTip)
		{
			super();
			this.openGame = openGame;
			this.toolTip = toolTip;
		}
	}

	private static final int COLUMN_INDEX_GAME_NAME = 0;
	private static final int COLUMN_INDEX_PLAYERS = 2;
	private static final int COLUMN_INDEX_GAME_OPTIONS = 3;

	private List<OpenGameElement> openGames;

	/**
	 * Liefert eine Instanz
	 * @param data			Daten für das Tablemodel.
	 * @param columnNames	Überschriften für das Tablemodel.
	 */
	public DefaultTableModelForOpenGames() {
		super(null, new String[] { I18n.getString("GUI.NAME"), //$NON-NLS-1$
				                   I18n.getString("GUI.MAP"), //$NON-NLS-1$
				                   I18n.getString("GUI.PLAYERS"), //$NON-NLS-1$
				                   I18n.getString("GUI.OPTIONS"),
				                   I18n.getString("GUI.GOODS"), //$NON-NLS-1$
				                   I18n.getString("GUI.GAME_VERSION"), //$NON-NLS-1$
				                   I18n.getString("GUI.IP_CONNECTION"), //$NON-NLS-1$
				                   I18n.getString("GUI.LEAGUE_GAME") //$NON-NLS-1$
				                 });
		openGames = new Vector<OpenGameElement>();
		//league game is currently the last column, so just remove it.
		if (Configuration.getInstance().isHideLeagueInterface())
		{
		    setColumnCount(getColumnCount() - 1);
		}
	}

	/**
	 * Fügt ein OpenGame dem TableModel hinzu und registriert sich
	 * als Observer für eventuelle Änderungen des OpenGame.
	 * @param newOpenGame	Das hinzuzufügende OpenGame.
	 */
	public synchronized void addOpenGame(OpenGame newOpenGame)
	{
	    //internally store the openGame and the corresponding tooltip
		String toolTip = createToolTip(newOpenGame);
		openGames.add(new OpenGameElement(newOpenGame, toolTip));
		String[] gameAsTableEntry = createTableEntry(newOpenGame);
		addRow(gameAsTableEntry);

		newOpenGame.addObserver(this);
	}
	
	/**
	 * Get an openGameElement from specific index
	 */
	private OpenGameElement getOpenGameElementByIndex(int index) {
		return openGames.get(index);
	}

    /**
	 * Liefert das OpenGame mit dem übergebenen index.
	 * Kann benutzt werden für die selektierte Zeile der Tabelle
	 * @param index Der Index.
	 * @return Das OpenGame mit dem Index.
	 */
	public OpenGame getOpenGameAt(int index) {
		OpenGameElement openGameElement = this.getOpenGameElementByIndex(index);
		if( openGameElement != null ) {
			return openGameElement.openGame;
		}
		return null;
	}

	/** returns the tooltip associated with the specified row.
	 *
	 * @param index row index
	 * @return returns the tooltip associated with the specified row
	 */
	public String getToolTipAt(int index) {
	    OpenGameElement openGameElement = this.getOpenGameElementByIndex(index);
		if( openGameElement != null ) {
			return openGameElement.toolTip;
		}
		return null;
	}

	/**
	 * Aktualisiert die Zeile mit dem OpenGame.
	 */
	@Override
    public void update(Observable observable, Object change)
	{
		if (observable instanceof OpenGame && change instanceof String)
		{
			OpenGame changedOpenGame = (OpenGame) observable;
			String strChange = (String) change;

			//use a synchronized block so that nothing else can
			//modify the openGames List
			synchronized (this)
			{
				for (int i = 0; i < openGames.size(); i++)
				{
					OpenGame currentOpenGame = openGames.get(i).openGame;
					if (changedOpenGame == currentOpenGame)
					{
						if (change.equals(OpenGame.NEW_NAME))
						{
							Test.output("added new game");
							setValueAt(currentOpenGame.getName(), i, COLUMN_INDEX_GAME_NAME);
						}
						else if (change.equals(OpenGame.NEW_CURRENT_PLAYER)
						         || change.equals(OpenGame.NEW_MAX_PLAYER))
						{
							Test.output("changed playernumber to " + currentOpenGame.getCurrentPlayer());
							setValueAt(formatNumberOfPlayers(currentOpenGame.getCurrentPlayer(),
							                                 currentOpenGame.getMaxPlayer()),
							           i,
							           COLUMN_INDEX_PLAYERS);
						}
						else if (change.equals(OpenGame.DESTROYED))
						{
						    Test.output("removing game: " + currentOpenGame.toString());
							openGames.remove(i);
							removeRow(i);
						}
						else if (strChange.startsWith(OpenGame.PLAYER_ADDED) ||
						         strChange.startsWith(OpenGame.PLAYER_REMOVED))
						{
							Test.output("changed playerlist");
							openGames.get(i).toolTip =
							    createToolTip(changedOpenGame);
						}
						else if (strChange.startsWith(OpenGame.NEW_GAME_MODE)) {
							Test.output("game mode changed");
							setValueAt(formatGameOptions(currentOpenGame), i, COLUMN_INDEX_GAME_OPTIONS);
						}
						break;
					}
				}
			}
		}
	}

	/**
	 * erzeugt aus der List der Spieler einen Tooltip.
	 * @param playerList
	 * @return
	 */
    private String createToolTip(OpenGame game)
    {
    	List<String> playerList = game.getPlayerList();
        if (playerList.size() == 0)
        {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<html>"); //$NON-NLS-1$
        if (!game.getSaveGameFileName().isEmpty())
        {
        	if (game.getSaveGameTimestamp() > 0)
        	{
        		sb.append(OpenGame.TIME_FORMAT2.format(new Date(game.getSaveGameTimestamp())));
        		sb.append(" ");
        	}
        	sb.append(game.getSaveGameFileName());
        	sb.append("<br>"); //$NON-NLS-1$
        }
        else {
        	long timestamp = game.getCreationTime();
        	if( timestamp > 0 ) {
        		sb.append(I18n.getString("GUI.GAME_OPEN_SINCE") + ": " + OpenGame.TIME_FORMAT2.format(new Date(timestamp)) + "<br>");
        	}
        }
        for (String player : playerList)
        {
            sb.append(player);
            sb.append("<br>"); //$NON-NLS-1$
        }
        sb.append("</html>"); //$NON-NLS-1$
        return sb.toString();
    }

    private static String formatNumberOfPlayers(int currentPlayers, int maxPlayers)
    {
        return currentPlayers + "/" + maxPlayers; //$NON-NLS-1$
    }

    /**
     * Format the game options which will be visible in options-row.
     * 
     * @param openGame
     * @return String   the resulting string for the options-row
     */
    private static String formatGameOptions(OpenGame openGame) {
    	List<String> labels = new ArrayList<>();

    	if (openGame.getMisc() != MiscType.S4) {
			if (!openGame.isAmazonen()) {
				labels.add(I18n.getString("GUI.NO_AMAZONS"));
			}
		} else {
			if (!openGame.isTrojans()) {
				labels.add(I18n.getString("GUI.NO_TROJANS"));
			}
		}
    	if (openGame.isCoop()) {
    		labels.add(I18n.getString("GUI.COOP"));
		}
    	if (openGame.isWimo()) {
    		labels.add(I18n.getString("GUI.WIMO"));
		}

    	for (OpenGame.S3CEGameOption gameOption : openGame.getCeGameOptions()) {
    		labels.add(I18n.getString(gameOption.getTranslationKey()));
		}

    	return String.join(", ", labels);
	}

    /**
     * Create the table-entry for a single open game.
     * 
     * @param opengame
     * @return
     */
    private static String[] createTableEntry(OpenGame opengame)
    {
        // for competition-row
        String isCompetition = opengame.isLeagueGame() ? I18n.getString("GUI.LEAGUE") : ""; //$NON-NLS-1$ //$NON-NLS-2$
        if( opengame.isTournamentGame() ) {
        	isCompetition = opengame.getTournamentName();
        }
        
        // for version-row
        String gameVersion = "";
        if (opengame.getMisc() == MiscType.ALOBBY)
        {
        	gameVersion = I18n.getString("GUI.VERSION_S3_CD_GOG");
        } else if (opengame.getMisc() == MiscType.GOG)
        {
        	gameVersion = I18n.getString("GUI.VERSION_S3_GOG");
        } else if (opengame.getMisc() == MiscType.CD)
        {
        	gameVersion = I18n.getString("GUI.VERSION_S3_CD");
        } else if (opengame.getMisc() == MiscType.S4)
        {
        	gameVersion = I18n.getString("GUI.VERSION_S4_CD_GOG");
        } else if (opengame.getMisc() == MiscType.S3CE)
        {
        	gameVersion = I18n.getString("GUI.VERSION_S3_CE");
        }
        
        // for connection-row
        String connection;
        int secondIpByte = Integer.parseInt(opengame.getIP().split("\\.")[1]);
        if (opengame.getIP().startsWith(ALobbyConstants.VPN_IP_RANGE)) {
            // -> VPN
        	connection = I18n.getString("GUI.VPN");
        } else if (opengame.getIP().startsWith("10.") || opengame.getIP().startsWith("192.168.") || (opengame.getIP().startsWith("172.") && secondIpByte >= 16 && secondIpByte <= 31)) {
            // -> local network
        	connection = I18n.getString("GUI.LAN");
        } else if (opengame.getIP().startsWith("127.0.0.")) {
            // -> on localhost
        	connection = "Localhost";
        } else {
            // -> all other
        	connection = I18n.getString("GUI.IP_DIRECT");
        }

        // get goods-value
        String goods = "";
        if( opengame.getGoods() >= 0 ) {
        	GoodsInStock goodsInStock = GoodsInStock.getGoodsInStockByIndex(opengame.getGoods());
        	if( goodsInStock != null ) {
        		goods = goodsInStock.getLanguageString();
        	}
        }
        
        // collect all data for the entry
        String[] gameAsTableEntry = {
            opengame.getName(),
            S3Map.getDisplayName(opengame.getMap()),
            formatNumberOfPlayers(opengame.getCurrentPlayer(),
                                  opengame.getMaxPlayer()),
            formatGameOptions(opengame),
            goods,
            gameVersion,
            connection,
            isCompetition
        };
        
        return gameAsTableEntry;
    }

}
