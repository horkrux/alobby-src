/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.EventListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.OptionsFrame;
import net.siedler3.alobby.i18n.I18n;


public class IngameOptionsConfigWindow extends JPanel
{
    private static final long serialVersionUID = 1L;


    /**
     * Action für die Autosave Einstellung. Die JCheckbox wird mit
     * dieser Action initialisiert.
     */
    private final class AutosaveCheckboxAction extends AbstractAction
    {
        public AutosaveCheckboxAction()
        {
            super(I18n.getString("IngameOptionsConfigWindow.AUTOSAVE_ACTION_NAME")); //$NON-NLS-1$
            //ALT+a
            putValue(MNEMONIC_KEY, KeyEvent.VK_A);
            putValue(SELECTED_KEY, getIsAutosave());
        }
        @Override
        public void actionPerformed(ActionEvent e)
        {
        }
    }

    protected int fontSize;
    protected JCheckBox checkBoxAutosave;
    protected JCheckBox checkBoxStopStoringTools;
    protected SettlersLobby settlersLobby;
    protected JComboBox<Integer> comboBoxAutoSaveTimer;
    protected JLabel lblAutoSaveTimer;
    protected JLabel lblAutoSave;
    protected Action autosaveAction;
    protected JPanel pnlAutosave;

    protected IngameOptionsConfigWindow(SettlersLobby settlersLobby)
    {
        super();
        this.settlersLobby = settlersLobby;
        this.fontSize = 0;
        Color backgroundColor = settlersLobby.theme.backgroundColor;
        setBackground(backgroundColor);

        autosaveAction = createAutosaveAction();

        pnlAutosave = new JPanel();
        pnlAutosave.setBackground(backgroundColor);

        checkBoxAutosave = new JCheckBox(autosaveAction);
        checkBoxAutosave.setBackground(backgroundColor);
        // fix the additional and unnecessary painting of border to JCheckbox
        checkBoxAutosave.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
        checkBoxAutosave.setBorderPainted(true);
        checkBoxAutosave.setRolloverEnabled(false);

        lblAutoSave = new JLabel(I18n.getString("IngameOptionsConfigWindow.AUTOSAVE_PART1")); //$NON-NLS-1$
        lblAutoSave.setBackground(backgroundColor);
        lblAutoSave.setLabelFor(checkBoxAutosave);
        
        comboBoxAutoSaveTimer = new JComboBox<Integer>(OptionsFrame.AUTOSAVE_TIMER_ITEMS);
        selectAutosaveTimer();
        comboBoxAutoSaveTimer.setBackground(settlersLobby.theme.backgroundColor);
        comboBoxAutoSaveTimer.setForeground(settlersLobby.theme.getTextColor());
        comboBoxAutoSaveTimer.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));

        lblAutoSaveTimer = new JLabel(I18n.getString("IngameOptionsConfigWindow.AUTOSAVE_PART2")); //$NON-NLS-1$
        lblAutoSaveTimer.setBackground(backgroundColor);
        lblAutoSaveTimer.setLabelFor(comboBoxAutoSaveTimer);

        //html Formatierung aktivert linewrap
        checkBoxStopStoringTools = new JCheckBox(
                "<html>" + I18n.getString("IngameOptionsConfigWindow.MSG_STOP_TOOLS") + "</html>", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                getIsStopStoringTools());
        checkBoxStopStoringTools.setBackground(backgroundColor);
        // fix the additional painting of border to JCheckbox
        checkBoxStopStoringTools.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
        checkBoxStopStoringTools.setBorderPainted(true);
        checkBoxStopStoringTools.setRolloverEnabled(false);
        checkBoxStopStoringTools.setFocusPainted( false );
        checkBoxStopStoringTools.getListeners(EventListener.class);

        updateFonts();
        addKeyBindings();
        createLayout();
    }

    protected void createLayout()
    {
        //Layout für Zeile Autosave
        GroupLayout groupLayout = new GroupLayout(pnlAutosave);
        pnlAutosave.setLayout(groupLayout);

        groupLayout.setHorizontalGroup(
                groupLayout.createSequentialGroup()
                    .addComponent(checkBoxAutosave)
                    .addGap(4)
                    .addComponent(lblAutoSave,
                            0,
                            GroupLayout.DEFAULT_SIZE,
                            GroupLayout.PREFERRED_SIZE)
                    .addGap(4)
                    .addComponent(comboBoxAutoSaveTimer,
                            GroupLayout.PREFERRED_SIZE,
                            GroupLayout.DEFAULT_SIZE,
                            GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAutoSaveTimer)
                        );

        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(checkBoxAutosave)
                    .addGap(4)
                    .addComponent(lblAutoSave)
                    .addGap(4)
                    .addComponent(comboBoxAutoSaveTimer)
                    .addComponent(lblAutoSaveTimer)
                        );

        //Layout insgesamt
        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(
        	layout.createParallelGroup()
                .addComponent(pnlAutosave)
                .addComponent(checkBoxStopStoringTools)
        );
        layout.setVerticalGroup(
        	layout.createSequentialGroup()
                .addComponent(pnlAutosave,
                        GroupLayout.PREFERRED_SIZE,
                        GroupLayout.DEFAULT_SIZE,
                        GroupLayout.PREFERRED_SIZE)
                .addComponent(checkBoxStopStoringTools,
                        GroupLayout.PREFERRED_SIZE,
                        GroupLayout.DEFAULT_SIZE,
                        GroupLayout.PREFERRED_SIZE)
        );

    }

    protected void addKeyBindings()
    {
        //zusätzlich noch CTRL+s registrieren, um Autosave zu aktivieren/deaktivieren
        Action pressAction = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                checkBoxAutosave.doClick();
            }
        };
        KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK);
        checkBoxAutosave.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
            .put(key, "pressAction"); //$NON-NLS-1$
        checkBoxAutosave.getActionMap().put("pressAction", pressAction); //$NON-NLS-1$
    }

    public void setFontSize(int fontSize)
    {
        this.fontSize = fontSize;
        updateFonts();
    }

    /**
     * Mit enabled==false wird der Schalter zum Einstellen von Autosave
     * komplett deaktiviert.
     *
     * @param enabled
     */
    public void setEnabledAutosave(boolean enabled)
    {
        //alle Objekte in der Autosave Zeile erhalten denselben enabled-state.
        autosaveAction.setEnabled(enabled);
        lblAutoSave.setEnabled(enabled);
        lblAutoSaveTimer.setEnabled(enabled);
    }

    public void display(boolean display)
    {
        setVisible(display);
    }


    protected void updateFonts()
    {
        Font font = getFont();
        if (this.fontSize > 0)
        {
            font = getFont().deriveFont((float)this.fontSize);
        }
        setFont(font);
        checkBoxAutosave.setFont(font);
        checkBoxStopStoringTools.setFont(font);
        comboBoxAutoSaveTimer.setFont(font);
        lblAutoSave.setFont(font);
        lblAutoSaveTimer.setFont(font);
    }

    /**
     * Die dargestellten Werte werden an die Werte in {@code settlersLobby}
     * angepasst.
     */
    public void updateView()
    {
        boolean b = getIsAutosave();
        checkBoxAutosave.setSelected(b);
        selectAutosaveTimer();
        checkBoxStopStoringTools.setSelected(getIsStopStoringTools());
    }

    public void writeSelectionToConfiguration()
    {
        settlersLobby.config.setAutoSave(checkBoxAutosave.isSelected());
        settlersLobby.config.setAutoSaveTimer(
                (Integer) comboBoxAutoSaveTimer.getSelectedItem());
        settlersLobby.config.setStopStoringTools(checkBoxStopStoringTools.isSelected());
    }

    public JPanel getPnlAutosave()
    {
        return pnlAutosave;
    }

    public JCheckBox getCheckBoxStopStoringTools()
    {
        return checkBoxStopStoringTools;
    }


    protected void selectAutosaveTimer()
    {
        Integer selection = getAutoSaveTimer();
        comboBoxAutoSaveTimer.setSelectedItem(selection);
    }


    protected Action createAutosaveAction()
    {
        return new AutosaveCheckboxAction();
    }

    protected boolean getIsAutosave()
    {
        return settlersLobby.config.isAutoSave();
    }
    protected int getAutoSaveTimer()
    {
        return settlersLobby.config.getAutoSaveTimer();
    }

    protected boolean getIsStopStoringTools()
    {
        return settlersLobby.config.isStopStoringTools();
    }
}
