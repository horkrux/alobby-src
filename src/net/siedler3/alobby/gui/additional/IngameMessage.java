package net.siedler3.alobby.gui.additional;
/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */


import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.nativesupport.NativeFunctions;

public class IngameMessage extends Thread
{
    private static class IngameMessageElement
    {
        public String message;
        public int priority;
        public int time;
        public int x;
        public int y;
        public boolean showMinimumHalfTime;

        public IngameMessageElement(String message, int priority, int time, int x, int y, boolean showMinimumHalfTime)
        {
            this.message = message;
            this.priority = priority;
            this.time = time;
            this.x = x;
            this.y = y;
            this.showMinimumHalfTime = showMinimumHalfTime;
        }
    }

    private static class IngameMessageWorkerThread extends Thread
    {
        public IngameMessageWorkerThread()
        {
            setDaemon(true);
            setName(getName() + "-displayIngameMessage");
        }

        @Override
        public void run()
        {
            while (!isInterrupted())
            {
                lock.lock();

                try
                {
                    while (list.isEmpty())
                    {
                        notEmpty.await();
                    }
                }
                catch (InterruptedException e)
                {
                    interrupt();
                    return;
                }
                finally
                {
                    lock.unlock();
                }

                displayMessages();
            }
        }

        private void displayMessages()
        {
            //Die Zeiten werden gespeichert, um zu wissen, wie lang
            //der aktuelle Text angezeigt wurde.
            long timeBefore, timeAfter;

            while (!list.isEmpty())
            {
                IngameMessageElement element;
                lock.lock();
                element = list.remove(0);
                lock.unlock();
                currentElement = element;

                adjustElementSettingsBeforeShowing(element);

                timeBefore = getCurrentTime();
                //this call is time consuming, that is why it runs inside WorkerThread
                isS3Opened = NativeFunctions.getInstance().showMessage(
                                element.message,
                                element.x,
                                element.y,
                                getFontSize(),
                                element.time * 1000);
                timeAfter = getCurrentTime();
                currentElement = null;

                if (element.showMinimumHalfTime && IngameMessage.isS3Opened)
                {
                    //Wenn der Text weniger als die Hälfte der
                    //angegeben Zeit angezeigt wurde
                    if ((timeAfter-timeBefore) < (element.time/2*1000))
                    {//Werden die Einträge später erneut angezeigt.
                        lock.lock();
                        int index = getFirstIndex(element.priority);
                        list.add(index, element);
                        lock.unlock();
                    }
                }
            }
        }

    }

	public static final String MESSAGE_NO_USER_INTERACTION = I18n.getString("JNIIngameMessage.NO_USER_INTERACTION_MESSAGE"); //$NON-NLS-1$
	public static final String MESSAGE_AUTO_SAVE = I18n.getString("JNIIngameMessage.AUTO_SAVE_MESSAGE"); //$NON-NLS-1$
	public static final String MESSAGE_AUTO_STATE_SAVE_ACTIVE = "Autosave ist aktiviert"; //$NON-NLS-1$
	public static final String MESSAGE_AUTO_STATE_SAVE_INACTIVE = "Autosave ist deaktiviert"; //$NON-NLS-1$
	public static final String MESSAGE_UPLOAD_QUESTION = I18n.getString("JNIIngameMessage.S3STATS_UPLOAD_QUESTION_MESSAGE"); //$NON-NLS-1$

    private static List<IngameMessageElement> list = new Vector<IngameMessageElement>();
    private static IngameMessageElement currentElement = null;

	//Der Standard-Wert wird genommen, wenn die Zeitangabe 0 ist.
	private static final int STANDARD_TIME = 10;

	//Die Standard-Werte werden genommen, wenn die X-Angabe und/oder die Y-Angabe -1 ist.
    private static final int STANDARD_X_1366 = 250;
    private static final int STANDARD_X_1024 = 250;
	private static final int STANDARD_X_800  = 195;
	private static final int STANDARD_X_640  = 145;
	private static final int STANDARD_X_MC   = 18;//MC = Minichat
    private static final int STANDARD_Y_1366 = 665;
    private static final int STANDARD_Y_1024 = 665;
	private static final int STANDARD_Y_800  = 505;
	private static final int STANDARD_Y_640  = 400;
	private static final int STANDARD_Y_MC   = 470;//MC = Minichat
    private static final int STANDARD_FS_1366 = 25;//FS = FontSize
    private static final int STANDARD_FS_1024 = 25;
	private static final int STANDARD_FS_800  = 20;
	private static final int STANDARD_FS_640  = 15;
	private static final int STANDARD_FS_MC   = 18;//MC = Minichat

	//showMessage gibt zurück, ob das S3 geöffnet ist. Der Wert wird hier gespeichert
	private static boolean isS3Opened = true;

	//Speichert, ob der User noch im Minichat ist. Der Wert
	//wird über setIsInMiniChat verändert.
	//Die Variable wird für die Standard-Werte benötigt, da im
	//MiniChat andere Koordinaten benötigt werden, als bei den
	//InGame 800x600.
	private static boolean isInMiniChat = false;

	//Für die Synchronisation
	//lock is used for list access as well as for waiting condition
	private static final Lock lock = new ReentrantLock();
	private static final Condition notEmpty = lock.newCondition();

	//Thread for displaying the IngameMessages.
	//The thread pauses/sleeps, if there are no IngameMessages.
	//As soon as messages are added using addToWaitingList,
	//the thread gets active and displays the messages
	private static final Thread workerThread = new IngameMessageWorkerThread();

    static {
        workerThread.start();
    }


	/**
	 * !ACHTUNG! Es sollte vorher überprüft Configuration.getShowMessagesIngame(),
	 * damit es beim User nicht zu Fehlern kommt.
	 *
	 * Fügt die Nachricht mit Zeit und Priorität ein.
	 * Wird die Warteschleife neu befüllt, wird automatisch die Ausgabe dieser
	 * gestartet.
	 *
	 * Wenn die Nachricht zu lang für den Bildschirm ist, wird sie gekürzt, damit
	 * die Ausgabe einzeilig ist.
	 *
	 * Wenn showingTime 0 ist, dann wird der Standard-Wert für die Zeit genommen.
	 *
	 * priority kann jeden Wert enthalten. Die Nachrichten werden sofort nach Priorität
	 * sortiert. Als nächstes erscheint immer die Nachricht mit höchster Priorität.
	 *
	 * Wenn x oder y -1 ist, dann werden die Standart-Werte angepasst an die derzeitige
	 * Auflösung in Siedler 3 genommen.
	 *
	 * Wenn showDirectly true ist, dann wird die Nachricht sofort angezeigt,
	 * wenn sie die höchste Priorität hat.
	 * Wenn showDirectly false ist, wird die Nachricht erst angezeigt, nachdem showMessage()
	 * zurückgekehrt ist und wenn sie die höchste Priorität hat.
	 *
	 * @param message
	 * @param showingTime in Sekunden
	 * @param priority
	 * @param x - Position
	 * @param y - Position
	 * @param showDirectly
	 */
	public static void addToWaitingList(String message, int showingTime, int priority,
			int x, int y, boolean showDirectly, boolean showMinimumHalfTime)
	{
		int index;
        IngameMessageElement element = new IngameMessageElement(message, priority, showingTime, x, y, showMinimumHalfTime);

        lock.lock();

	    index = getLastIndex(priority);
	    list.add(index, element);
        notEmpty.signal();
	    lock.unlock();
        if (index == 0 && showDirectly)
        {
            stopCurrentMessage();
        }

	    Thread.yield();
	}

    private static void adjustElementSettingsBeforeShowing(IngameMessageElement element)
    {
        //Die Auflösung kann sich im Laufe der Zeit ändern,
        //daher wird die Position erst kurz vor dem Anzeigen
        //festgelegt.
        if (element.x == -1)
        {
            element.x = getStandardX();
        }
        if (element.y == -1)
        {
            element.y = getStandardY();
        }
        if (element.message.length() > 100)
        {
            element.message = element.message.substring(0, 100);
        }
        if (element.time == 0)
        {
            element.time = STANDARD_TIME;
        }
    }

    /**
     * check if the currently visible message is the given message
     * 
     * @param message
     * @return boolean
     */
	public static boolean checkIfMessageIsCurrentlyShown(String message)
	{
	    if (currentElement != null && message.equals(currentElement.message))
	    	return true;
	    else
	    	return false;
	}

	/**
	 * Beendet das Zeigen der aktuellen Nachricht.
	 * @param message
	 */
	public static void stopCurrentMessage()
	{
		NativeFunctions.getInstance().stopShowing();
	}

	/**
	 * !ACHTUNG! Es sollte vorher überprüft Configuration.getShowMessagesIngame(),
	 * damit es beim User nicht zu Fehlern kommt.
	 * Startet die AutosaveNachricht.
	 */
	public static void startShowAutoSaveMessage()
	{
		addToWaitingList(MESSAGE_AUTO_SAVE, 0, 1, -1, -1, true, false);
	}
	
	public static void startShowAutoSaveStateMessage( boolean activeState )
	{
		if( activeState ) {
			addToWaitingList(MESSAGE_AUTO_STATE_SAVE_ACTIVE, 0, 1, -1, -1, true, false);
		}
		else {
			addToWaitingList(MESSAGE_AUTO_STATE_SAVE_INACTIVE, 0, 1, -1, -1, true, false);
		}
	}
	
	/**
	 * Add Message to list for the moment when s3-stats-screen was taken
	 */
	public static void startShowS3StatsMessage()
	{
		addToWaitingList(I18n.getString("JNIIngameMessage.S3STATS_SAVED_MESSAGE"), 0, 1, 20, 550, true, false);
	}
	
	/**
	 * Add Message to list for the moment when s3-stats-screen was taken
	 */
	public static void startShowS3StatsUploadedMessage()
	{
		// x = 0, as this message is quite long and gets otherwise cut off in Polish
		addToWaitingList(I18n.getString("JNIIngameMessage.S3STATS_UPLOADED_MESSAGE"), 0, 2, 20, 570, true, false);
	}
	
	/**
	 * Add Message to list for the moment when s3-stats-screen can be uploaded
	 */
	public static void startShowS3StatsUploadQuestionMessage()
	{
		addToWaitingList(MESSAGE_UPLOAD_QUESTION, 0, 0, 20, 530, true, false);
	}

	/**
	 * Beendet die Autosave-Nachricht.
	 */
	public static void stopShowAutoSaveMessage()
	{
	    if (checkIfMessageIsCurrentlyShown(IngameMessage.MESSAGE_AUTO_SAVE))
        {
            stopCurrentMessage();
        }
	}
	
	public static void stopShowAnyAutoSaveStateMessage()
	{
	    if (checkIfMessageIsCurrentlyShown(IngameMessage.MESSAGE_AUTO_STATE_SAVE_ACTIVE) || checkIfMessageIsCurrentlyShown(IngameMessage.MESSAGE_AUTO_STATE_SAVE_INACTIVE))
        {
            stopCurrentMessage();
        }
	}

	/**
	 * Kill the s3-stats-screen-message
	 * 
	 * => will not work as s3stats-screen is not reinitialized
	 */
	/*public static void stopShowS3StatsUploadQuestionMessage()
	{
	    if (checkIfMessageIsCurrentlyShown(IngameMessage.MESSAGE_UPLOAD_QUESTION))
        {
            stopCurrentMessage();
        }
	}*/

	/**
	 * Kill the s3-stats-screen-message
	 */
	public static void stopShowS3StatsMessage()
	{
		stopCurrentMessage();
	}

	/**
	 * !ACHTUNG! Es sollte vorher überprüft Configuration.getShowMessagesIngame(),
	 * damit es beim User nicht zu Fehlern kommt.
	 *
     * Zeigt dem User in S3 eine Nachricht an, daß er die Finger von Maus
     * und Tastatur lassen soll.
     * @param timeOut
     */
    public static void startShowNoUserInteractionMessage(int timeOut)
    {
        IngameMessage.addToWaitingList(IngameMessage.MESSAGE_NO_USER_INTERACTION,
        		(timeOut * 10), 1, 250, 250, true, false);
    }

    /**
     * Blendet die Nachricht von {@link #startShowNoUserInteractionMessage()}
     * wieder aus.
     */
    public static void stopShowNoUserInteractionMessage()
    {
	    if (checkIfMessageIsCurrentlyShown(IngameMessage.MESSAGE_NO_USER_INTERACTION))
        {
            stopCurrentMessage();
        }
    }

	public static void startShowS3StatsMapName(String string) {
		addToWaitingList(string, 0, 1, 20, 508, true, false);
	}
	
	public static void stopShowS3StatsMapName(String string) {
		if (checkIfMessageIsCurrentlyShown(string))
        {
			stopCurrentMessage();
        }
	}

    /**
     * Setzt isInMiniChat auf den aktuellen Wert.
     * @param isInMiniChat
     */
    public static void setIsInMiniChat(boolean isInMiniChat)
    {
        IngameMessage.isInMiniChat = isInMiniChat;
    }

	/**
	 * Überprüft die Priorität der angekommenen Message
	 * und gibt den Index zurück, an dem diese Priorität Platz hat.
	 * Dieser Index gibt den Platz an, an dem zum ersten Mal eine Nachricht
	 * mit geringerer Priorität als die angegebene auftaucht. Somit wird die
	 * Nachricht als letzte unter den Nachrichten mit gleicher Priorität
	 * angezeigt
	 * @param priority
	 * @return
	 */
	private static int getLastIndex(int priority)
	{
		if (!list.isEmpty())
		{
			if (priority == 0)
			{
				//Die letzte befüllte Position ist size()-1.
				//Um den nächsten Index zu befüllen
				//müsste man +1 rechen.
				return list.size();
			}

			else
			{
				//Überprüft, ab welchem Index die Priorität höher ist
				//als die eingespeicherte Priorität. Dort wird die Nachricht
				//eingeschoben.
		    	for (int i = 0; i < list.size(); i++)
		    	{
		    		if (priority > list.get(i).priority)
		    		{
		    			return i;
		    		}
		    	}
		    	//Die letzte befüllte Position ist size()-1.
		    	//Um den nächsten Index zu befüllen
				//müsste man +1 rechen.
		    	return list.size();
			}
		}
		else
		{
			return 0;
		}
	}

	/**
	 * Überprüft die Priorität der angekommenen Message
	 * und gibt den Index zurück, an dem diese Priorität Platz nehmen soll.
	 * Dieser Index gibt den Platz an, an dem zum ersten mal eine Nachricht mit
	 * der Priorität auftaucht. Somit wird die Nachricht als erste unter den
	 * Nachrichten mit der gleichen Priorität angezeigt.
	 * @param priority
	 * @return
	 */
	private static int getFirstIndex(int priority)
	{
		if (!list.isEmpty())
		{

			//Überprüft, ab welchem Index die Priorität gleich
			//der eingespeicherte Priorität ist. Dort wird die Nachricht
			//eingeschoben.
		    for (int i = 0; i < list.size(); i++)
		    {
		    	if (priority == list.get(i).priority)
		    	{
		    		return i;
		    	}
		    }
		    //Die letzte befüllte Position ist size()-1. Um den nächsten Index zu befüllen
			//müsste man +1 rechen.
		    return list.size();

		}
		else
		{
			return 0;
		}
	}

	/**
	 * Gibt die aktuelle Zeit in Millisekunden zurück.
	 * @return
	 */
	private static long getCurrentTime()
	{
	    return System.currentTimeMillis();
	}


	/**
	 * Gibt den Standard X-Wert zurück. Dieser ist abhängig von der
	 * in Siedler 3 ausgewählten Auflösung.
	 * @return
	 */
	private static int getStandardX()
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		if (isInMiniChat)
			return STANDARD_X_MC;
        else if (screenSize.width == 1366)
            return STANDARD_X_1366;
        else if (screenSize.width == 1024)
            return STANDARD_X_1024;
		else if (screenSize.width == 800)
			return STANDARD_X_800;
		else if (screenSize.width == 640)
		    return STANDARD_X_640;
		else//Wenn die Breite nicht zu den drei Auflösungen von Siedler 3 passt,
			//ist es evtl. möglich, dass der User sich auf dem Desktop
			//befindet. Die am weitesten verbreitete Auflösung wird wohl
			//1024x768 sein. Daher wird der Standard-Wert dieser genommen.
	        return STANDARD_X_1024;
	}

	/**
	 * Gibt den Standard Y-Wert zurück. Dieser ist abhängig von der
	 * in Siedler 3 ausgewählten Auflösung.
	 * @return
	 */
	private static int getStandardY()
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		if (isInMiniChat)
			return STANDARD_Y_MC;
        else if (screenSize.width == 1366)
            return STANDARD_Y_1366;
		else if (screenSize.width == 1024)
			return STANDARD_Y_1024;
		else if (screenSize.width == 800)
			return STANDARD_Y_800;
		else if (screenSize.width == 640)
		    return STANDARD_Y_640;
		else//Wenn die Breite nicht zu den drei Auflösungen von Siedler 3 passt,
			//ist es evtl. möglich, dass der User sich auf dem Desktop
			//befindet. Die am weitesten verbreitete Auflösung wird wohl
			//1024x768 sein. Daher wird der Standard-Wert dieser genommen.
	        return STANDARD_Y_1024;
	}

	private static int getFontSize()
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		if (isInMiniChat)
			return STANDARD_FS_MC;
        else if (screenSize.width == 1366)
            return STANDARD_FS_1366;
		else if (screenSize.width == 1024)
			return STANDARD_FS_1024;
		else if (screenSize.width == 800)
			return STANDARD_FS_800;
		else if (screenSize.width == 640)
		    return STANDARD_FS_640;
		else//Wenn die Breite nicht zu den drei Auflösungen von Siedler 3 passt,
			//ist es evtl. möglich, dass der User sich auf dem Desktop
			//befindet. Die am weitesten verbreitete Auflösung wird wohl
			//1024x768 sein. Daher wird der Standard-Wert dieser genommen.
			return STANDARD_FS_1024;
	}

}
