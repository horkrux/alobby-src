/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

public class ListModel extends AbstractListModel<S3User>
{
    private static final long serialVersionUID = 3743148626949083127L;
    private List<S3User> userOnline;

    public ListModel()
    {
        this.userOnline = new ArrayList<S3User>();
    }
	/**
	 * creates a read-only model for the given
	 * {@code List}. It directly accesses the List,
	 * therefore it must not be changed afterwards.
	 * @param userOnline
	 */
	public ListModel(final List<S3User> userOnline)
	{
		this.userOnline = userOnline;
	}

	@Override
    public int getSize()
    {
    	return userOnline.size();
    }

    @Override
    public S3User getElementAt(int i)
    {
    	return userOnline.get(i);
    }
    
    /**
     * Return a single S3User from list by given nickname.
     * 
     * @return
     */
    public S3User getElementByS3UserName( String nickname ) {
		for (int i = 0; i < userOnline.size(); i++) {
			if( userOnline.get(i).getNick().equalsIgnoreCase(nickname) ) {
				return userOnline.get(i);
			}
		}
    	return null;
    }

}
