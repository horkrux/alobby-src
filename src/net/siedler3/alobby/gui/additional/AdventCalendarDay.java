package net.siedler3.alobby.gui.additional;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import org.json.JSONObject;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.gui.OpenNewGameDialog;
import net.siedler3.alobby.gui.ShowMapDetail;
import net.siedler3.alobby.i18n.I18n;

/**
 * Represents a single day in AdventCalendar including all interactions with the
 * specific day.
 * 
 * @author Zwirni
 *
 */
public class AdventCalendarDay {
	
	private AdventCalendar adventCalendar = null;

	private String type = "";

	private String content = "";

	// coordinates for the day
	private int x1 = 0;
	private int x2 = 0;
	private int y1 = 0;
	private int y2 = 0;

	private SettlersLobby settlersLobby;

	// the actual day
	private int day;

	// the actual year
	private int year;

	// the title-bar icon
	private Image titleIcon;
		
	// Constructor
	public AdventCalendarDay( AdventCalendar adventCalendar, SettlersLobby settlersLobby, int year, int day ) {
		this.adventCalendar = adventCalendar;
		this.settlersLobby = settlersLobby;
		this.day = day;
		this.year = year;
		this.titleIcon = settlersLobby.getGUI().getIcon();
	}

	/**
	 * Return info if this advent-day has been reached
	 * 
	 * @return
	 */
	public boolean hasBeenReached() {
		GregorianCalendar dayc = this.adventCalendar.calendar;
		// set it to today
		// hint: 11 = December
		dayc.set(Calendar.MONTH, 11); 
		dayc.set(Calendar.DAY_OF_MONTH, this.day); 

		// convert it to date
		Date date = this.adventCalendar.calendar.getTime();
		
		// get today
		Date today = new Date();
		
		if( date.compareTo(today) == 0 || date.compareTo(today) < 0 ) {
			return true;
		}
		
		return false;
	}

	/**
	 * Set the type of the day which is responsible for what happens on click
	 * 
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Set content for the day which is used depending on chosen type
	 * 
	 * @param string
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * Set the icon for the window-title.
	 * If not given use the aLobby-default-icon.
	 * 
	 * @param filename
	 */
	public void setTitleIcon(String filename) {
		try {
			this.titleIcon = ImageIO.read(getClass().getResourceAsStream(
			        ALobbyConstants.PACKAGE_PATH + "/gui/img/" + filename));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the coordinates where the day should be placed on.
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	public void setCoordinats(String x1, String y1, String x2, String y2) {
		if( x1 != null && x2 != null && y1 != null && y2 != null ) {
			this.x1 = Integer.parseInt(x1);
			this.x2 = Integer.parseInt(x2);
			this.y1 = Integer.parseInt(y1);
			this.y2 = Integer.parseInt(y2);
		}
	}

	/**
	 * Add the resulting day-object on the calendar-image. 
	 */
	public void show() {
		
		// calculate the size of the overlay
		int width = this.x2 - this.x1;
		int height = this.y2 - this.y1;
		
		// generate the label which will be used as click-overlay
		// and is absolute positioned on the image matching the day
		JLabel lblDay = new JLabel();
		lblDay.setLayout(null);
		lblDay.setBackground(new Color(0, 0, 0, 0));
		lblDay.setBorder(new EmptyBorder(0, 0, 0, 0));
		lblDay.setBounds(this.x1, this.y1, width, height);
		lblDay.setSize(new Dimension(width, height));
		lblDay.setPreferredSize(new Dimension(width, height));
		lblDay.setMinimumSize(new Dimension(width, height));
		lblDay.setOpaque(true);
		
		// set the mouse-listener which will wait for click or mouse-moving
		lblDay.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// decide by day-type what happens on click
				switch( type ) {
					case "text":
						// generate messagewindow
			        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("AdventsCalendarDay.DOOR") + " " + day, true);
			        	messagewindow.setIconImage(titleIcon);
			        	// -> add message
			    		messagewindow.addMessage(
		    				MessageFormat.format("<html>" + content.replace("&gt;", ">").replace("&lt;", "<") + "</html>", adventCalendar.hexcolor), 
			    			UIManager.getIcon("OptionPane.informationIcon")
			    		);
			    		// -> add ok-button
			    		messagewindow.addButton(
			    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
			    			new ActionListener() {
			    			    @Override
			    			    public void actionPerformed(ActionEvent e) {
			    			    	messagewindow.dispose();
			    			    	adventCalendar.setVisible(true);
			    			    }
			    			}
			    		);
			    		// hide adventcalender
			    		adventCalendar.setVisible(false);
			    		// show it
			    		messagewindow.setVisible(true);
						break;
					case "map":
						// get the map-data from content
						JSONObject obj = new JSONObject(content);
						@SuppressWarnings("unused") String filename = obj.getString("filename");
						String mapname = obj.getString("name");
						
						// generate messagewindow
			        	MessageDialog messagewindow1 = new MessageDialog(null, I18n.getString("AdventsCalendarDay.DOOR") + " " + day, true);
			        	messagewindow1.setIconImage(titleIcon);
			        	// -> add message
			    		messagewindow1.addMessage(
		    				MessageFormat.format("<html>" + I18n.getString("AdventsCalendarDay.MAP_RECOMMENDATION") + "</html>", mapname), 
			    			UIManager.getIcon("OptionPane.informationIcon")
			    		);
			    		// -> add ok-button
			    		messagewindow1.addButton(
			    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
			    			new ActionListener() {
			    			    @Override
			    			    public void actionPerformed(ActionEvent e) {
			    			    	new ShowMapDetail(settlersLobby.getGUI().getMainWindow(), settlersLobby, mapname, true);
			    			    	messagewindow1.dispose();
			    			    	adventCalendar.dispose();
			    			    }
			    			}
			    		);
			    		// -> add cancel-button
			    		messagewindow1.addButton(
		    				I18n.getString("OptionsFrame.CANCEL"),
			    			new ActionListener() {
			    			    @Override
			    			    public void actionPerformed(ActionEvent e) {
			    			    	messagewindow1.dispose();
			    			    	adventCalendar.setVisible(true);
			    			    }
			    			}
			    		);
			    		// hide adventcalender
			    		adventCalendar.setVisible(false);
			    		// show it
			    		messagewindow1.setVisible(true);
						break;
					case "tetris":
						// generate messagewindow
			        	MessageDialog messagewindow11 = new MessageDialog(null, I18n.getString("AdventsCalendarDay.DOOR") + " " + day, true);
			        	messagewindow11.setIconImage(titleIcon);
			        	// -> add message
			    		messagewindow11.addMessage(
		    				MessageFormat.format("<html>" + I18n.getString("AdventsCalendarDay.TETRIS_DESCRIPTION") + "</html>", adventCalendar.hexcolor), 
			    			UIManager.getIcon("OptionPane.informationIcon")
			    		);
			    		// -> add ok-button
			    		messagewindow11.addButton(
			    			I18n.getString("AdventsCalendarDay.TETRIS_PLAY_NOW"),
			    			new ActionListener() {
			    			    @Override
			    			    public void actionPerformed(ActionEvent e) {
			    			    	// enable tetris-tab in openmapchoosingdialog-window
			    			    	settlersLobby.getGUI().displayMapChoosingDialog();
			    			    	OpenNewGameDialog mapChoosingDialog = settlersLobby.getGUI().getMapChoosingDialog();
			    			    	mapChoosingDialog.getTabListe().setSelectedIndex(2);
			    			    	// and force it into the front
			    			    	java.awt.EventQueue.invokeLater(new Runnable() {
			    			    	    @Override
			    			    	    public void run() {
			    			    	    	mapChoosingDialog.toFront();
					    			    	mapChoosingDialog.repaint();
			    			    	    }
			    			    	});
			    			    	// close all other windows
			    			    	messagewindow11.dispose();
			    			    	adventCalendar.dispose();
			    			    }
			    			}
			    		);
			    		// -> add cancel-button
			    		messagewindow11.addButton(
		    				I18n.getString("AdventsCalendarDay.TETRIS_LATER"),
			    			new ActionListener() {
			    			    @Override
			    			    public void actionPerformed(ActionEvent e) {
			    			    	messagewindow11.dispose();
			    			    	adventCalendar.setVisible(true);
			    			    }
			    			}
			    		);
			    		// hide adventcalender
			    		adventCalendar.setVisible(false);
			    		// show it
			    		messagewindow11.setVisible(true);
					default:
						break;
				}
				
				// save marker that user has opened a door
				settlersLobby.config.setAdventCalendarDayShow(year, day, true);
			}
			// if mouse enters the button change the cursor
            public void mouseEntered(java.awt.event.MouseEvent evt) {
            	// set the cursor to a hand-cursor
            	adventCalendar.setCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
            	// set the cursor back to standard-cursor
            	adventCalendar.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
		});
		
		// add label to the contentPane
		this.adventCalendar.getContentPane().add(lblDay);
	}
	
}