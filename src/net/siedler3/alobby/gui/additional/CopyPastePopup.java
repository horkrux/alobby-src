/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.text.JTextComponent;

import net.siedler3.alobby.controlcenter.SettlersLobby;

import net.siedler3.alobby.i18n.I18n;
/**
 * Eine Instanz dieser Klasse kann einer Component hinzugefügt werden und
 * bietet automatisch die Copy, Paste und Cut Funktionalität.
 * Es sind jeweils Copy, Paste und Cut einzeln ein- und ausblendbar.
 * @author Stephan Bauer
 */
public class CopyPastePopup extends JPopupMenu {
	
	private JTextComponent textComponent;
	private JMenuItem mItCopy;
	private JMenuItem mItCut;
	private JMenuItem mItPaste;
	private JMenuItem mItTranslate;
	private boolean copyIsVisible;
	private boolean cutIsVisible;
	private boolean pasteIsVisible;
	private boolean translateIsVisible;
	private SettlersLobby settlersLobby;
	
	/**
	 * Konstruktor.
	 * @param textComponent
	 * 		Die Textkomponente, auf die sich die Copy, Paste und Cut Befehle
	 * 		auswirken sollen.
	 */
	public CopyPastePopup(JTextComponent textComponent, SettlersLobby settlersLobby) {
		this.textComponent = textComponent;
		this.settlersLobby = settlersLobby;
		cr8mItCopy();
		cr8mItCut();
		cr8mItPaste();
		cr8mItTranslate();
		copyIsVisible = true;
		cutIsVisible = true;
		pasteIsVisible = true;
		translateIsVisible = true;
		refreshMenuItems();
	}

	/**
	 * Erzeugt den Copy Menueintrag
	 */
	private void cr8mItCopy() {
		mItCopy = new JMenuItem(I18n.getString("CopyPastePopup.COPY")); //$NON-NLS-1$
		mItCopy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textComponent.copy();
			}
		});
	}

	/**
	 * Erzeugt den Cut Menüeintrag.
	 */
	private void cr8mItCut() {
		mItCut = new JMenuItem(I18n.getString("CopyPastePopup.CUT")); //$NON-NLS-1$
		mItCut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textComponent.cut();
			}
		});
	 }

	/**
	 * Erzeugt den Paste Menüeintrag.
	 */
	private void cr8mItPaste() {
		mItPaste = new JMenuItem(I18n.getString("CopyPastePopup.PASTE")); //$NON-NLS-1$
		mItPaste.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textComponent.paste();
			}
		});
	}
	
	/**
	 * Generate the translate-entry
	 */
	private void cr8mItTranslate() {
		if (Desktop.isDesktopSupported()) {
			mItTranslate = new JMenuItem(I18n.getString("CopyPastePopup.TRANSLATE")); //$NON-NLS-1$
			mItTranslate.addActionListener(new ActionListener() {
				@SuppressWarnings("static-access")
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// only perform action if text is selected
					if( textComponent.getSelectedText().length() > 0 ) {
						// generate Google Translate-Link
						// -> sl => source-language (set to auto as we don't know the text-language)
						// -> tl => target-language (set to user-language)
						// -> hl => webside-language
						// -> text => the selected text
						String link = "https://translate.google.com/?sl=&tl=" + settlersLobby.config.getLanguage() + "&ie=UTF-8&text=" + textComponent.getSelectedText();
						settlersLobby.getGUI().openURL(link);
					}
				}
			});
		}
	}
	
	/**
	 * Blendet den Copy Menüeintrag ein oder aus.
	 * @param isVisible
	 * 		Ob der Menüeintrag dargestellt werden soll.
	 */
	public void setVisibleCopy(boolean isVisible) {
		copyIsVisible = isVisible;
		refreshMenuItems();
	}

	
	/**
	 * Blendet den Cut Menüeintrag ein oder aus.
	 * @param isVisible
	 * 		Ob der Menüeintrag dargestellt werden soll.
	 */
	public void setVisibleCut(boolean isVisible) {
		cutIsVisible = isVisible;
		refreshMenuItems();
	}

	
	/**
	 * Blendet den Paste Menüeintrag ein oder aus.
	 * @param isVisible
	 * 		Ob der Menüeintrag dargestellt werden soll.
	 */
	public void setVisiblePaste(boolean isVisible) {
		pasteIsVisible = isVisible;
		refreshMenuItems();
	}
	
	/**
	 * Change visibility of translate-item
	 * @param isVisible
	 * 		whether the entry should be displayed or not
	 */
	public void setVisibleTranslate(boolean isVisible) {
		translateIsVisible = isVisible;
		refreshMenuItems();
	}
	
	/**
	 * Baut die Menüeinträge je nachdem, ob sie angezeigt werden sollen,
	 * zusammen.
	 */
	private void refreshMenuItems() {
		removeAll();
		if (copyIsVisible) {
			add(mItCopy);
		}
		if (cutIsVisible) {
			add(mItCut);
		}
		if (pasteIsVisible) {
			add(mItPaste);	
		}
		if (translateIsVisible) {
			add(mItTranslate);	
		}
	}
	
}
