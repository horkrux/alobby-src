/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui.additional;

import net.siedler3.alobby.controlcenter.AwayStatus;

public class S3User
{

	private 	  String 	nick;
	private 	  boolean 	admin;
	private       boolean   away;
	private       boolean   isBuddy;
	private       boolean   isIgnored;
	private       String    awayMsg;
	private 	  boolean 	vpnState;
	private boolean betaUser;

	/**
	 * Konstruktor
	 * @param nick
	 * @param admin
	 * @param b 
	 */
	public S3User(String nick, boolean admin, boolean isAway, String awayMsg, boolean isIgnored, boolean isBuddy, boolean vpnState)
	{
		this.nick = nick;
		this.admin = admin;
		this.away = isAway;
		this.awayMsg = awayMsg;
		this.isIgnored = isIgnored;
		this.isBuddy = isBuddy;
		this.vpnState = vpnState;
		this.betaUser = false;
	}


	public String getNick()
	{
		return nick;
	}

	public void setNick(String nick)
	{
		this.nick = nick;
	}

	public boolean isAdmin()
	{
		return admin;
	}

    public boolean isAway()
    {
        return away;
    }

    public boolean isInGame() {
    	return away && AwayStatus.isInGameMessage(awayMsg);
    }

    public String getAwayMsg()
    {
        return awayMsg;
    }

    /**
     * Getter
     * @return Ob der ircUser ein Freund ist.
     */
	public boolean isBuddy() {
		return isBuddy;
	}

	/**
	 * Setzt den Freundstatus.
	 */
	public void setBuddy(boolean isBuddy) {
		this.isBuddy = isBuddy;
	}

    /**
     * Getter
     * @return Ob der ircUser ignoriert wird.
     */
	public boolean isIgnored() {
		return isIgnored;
	}

	/**
	 * Setzt den Ignorestatus.
	 */
	public void setIgnored(boolean isIgnored) {
		this.isIgnored = isIgnored;
	}
	
	/**
	 * Get the vpn-state
	 */
	public boolean getVpnState() {
		return this.vpnState;
	}

	/**
	 * Get info if user has an aLobby-beta-version running.
	 * 
	 * @return
	 */
	public boolean isBetaUser() {
		return this.betaUser;
	}


	public void setBetaUser(boolean b) {
		this.betaUser = b;
	}
	
}
