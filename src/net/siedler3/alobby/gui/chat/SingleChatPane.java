package net.siedler3.alobby.gui.chat;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sound.sampled.LineListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import net.siedler3.alobby.controlcenter.MessageHandler;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.gui.additional.CopyPastePopup;
import net.siedler3.alobby.gui.additional.MessageHistory;
import net.siedler3.alobby.gui.additional.MyTextPane;
import net.siedler3.alobby.gui.additional.UrlListener;
import net.siedler3.alobby.i18n.I18n;

public class SingleChatPane extends AbstractChatPane implements LineListener
{
	//private final JTextPane textpaneChat = new JTextPane(); // saves all chat messages
    // prepared change to own textpane-class
    private final MyTextPane textpaneChat = new MyTextPane(); // saves all chat messages
    private final JScrollPane scrollPaneChat = new JScrollPane();
    private JButton btnSendMessage = new JButton(I18n.getString("GUI.SEND")); //$NON-NLS-1$
    final JTextField inputMessage = new JTextField();
    private boolean doScroll;

    // Initialisiere Style-Typen für den Chatbereich
    private StyledDocument document = new DefaultStyledDocument();
    protected SimpleAttributeSet messageStyle;
    protected SimpleAttributeSet messageStyleBold;
    protected SimpleAttributeSet programStyle;
    protected SimpleAttributeSet urlStyle;
    
    private PrivateMessageUserHistory privateMessageUserHistory;
    
    private Map<String, String> autocompleteListForInputMessageField;
    private List<String> lastAutocompletedStrings = new ArrayList<String>();
    private String lastAutocompletePrefix = "";
    
    // nickname-autocompletition via tab-key
    private KeyAdapter autoCompleteListener = new KeyAdapter() {
    	
    	private void doCompletition( String prefix, String content, int w ) {
    		// check if the searched word is not complete written in the list but could be positioned in this list
    		List<String> listForBinarySearch = new ArrayList<String>();
    		for(Entry<String, String> entry : autocompleteListForInputMessageField.entrySet()) {
				String key = entry.getKey();
    			listForBinarySearch.add(key);
    		}
    		Object[] autocompleteListForInputMessageFieldKeys = autocompleteListForInputMessageField.keySet().toArray();
    	    int n = Collections.binarySearch(listForBinarySearch, prefix);
    	    if (n < 0 && -n <= autocompleteListForInputMessageField.size()) {
    	    	// search for the matching words in the list
    	    	for( int m=n+1;-m<autocompleteListForInputMessageField.size();m-- ) {
    	    		// get the word on this position
    	    		String match = listForBinarySearch.get(-m);
    	    		// if tab was used before with match and this is this previous match
    	    		// than use the previous search-string to get the next nickname from list which starts with the same string
    	    		if( !lastAutocompletedStrings.isEmpty() && lastAutocompletedStrings.contains(match) ) {
    	    			prefix = lastAutocompletePrefix;
    	    			for( int k=n+1;-k<autocompleteListForInputMessageField.size();k-- ) {
    	    				String match2 = listForBinarySearch.get(-k);
    	    				if( !lastAutocompletedStrings.contains(match2) && match2.startsWith(prefix) ) {
    	    					String nextNick = autocompleteListForInputMessageField.get(autocompleteListForInputMessageFieldKeys[-k]);
    	    					String resultingContent = content.substring(0, w + 1) + nextNick;
            	    			inputMessage.setText(resultingContent);
            	    			// add this match to list as "shown" to the next run
            	    			lastAutocompletedStrings.add(nextNick.toLowerCase());
            	    			lastAutocompletePrefix = prefix;
            	    			// check if the next nickname also matches the prefix
            	    			// -> if not, than reset the autocompletitionlist
            	    			if( !listForBinarySearch.get(-k+1).startsWith(prefix) ) {
            	    				lastAutocompletedStrings.clear();
            	    			}
            	    			break;
    	    				}
    	    			}
    	    			break;
    	    		}
    	    		// if match found, complete string in field to this word (not the lowercase-matching-word, the original)
    	    		if( match.startsWith(prefix) ) {
    	    			String resultingContent = content.substring(0, w + 1) + autocompleteListForInputMessageField.get(autocompleteListForInputMessageFieldKeys[-m]);
    	    			inputMessage.setText(resultingContent);
    	    			// save this match for next run
    	    			lastAutocompletedStrings.add(match);
    	    			lastAutocompletePrefix = prefix;
    	    			break;
    	    		}
    	    	}
    	    }
    	}
    	
        @Override
        public void keyPressed(KeyEvent ev) {
        	if (ev.getKeyCode() == KeyEvent.VK_TAB) {
        		int pos = inputMessage.getCaretPosition() - 1;
        	    String content = null;
        	    try {
        	      content = inputMessage.getText(0, pos + 1).toLowerCase();
        	    } catch (BadLocationException e) {
        	      e.printStackTrace();
        	    }
        	    
        	    // find where the last inserted word starts
        	    int w;
        	    for (w = pos; w >= 0; w--) {
        	      if (!Character.isLetter(content.charAt(w)) && !Character.isDigit(content.charAt(w))) {
        	        break;
        	      }
        	    }

        	    // too few chars
        	    if (pos - w < 1)
        	      return;

        	    // get the searched word depending on calculated positions
        	    String prefix = content.substring(w + 1).toLowerCase().trim();
        	    if( !lastAutocompletePrefix.isEmpty() ) {
        	    	prefix = lastAutocompletePrefix;
        	    }
        	    doCompletition( prefix, content, w );
        	}
        	else {
        		// reset the completition-settings
        		lastAutocompletedStrings.clear();
        		lastAutocompletePrefix = "";
        	}
        }
    };

    // Nach Klicken auf "Senden", rufe settlersLobby.actionChatMessage auf
    private final ActionListener actionListenerToSendMessage = new ActionListener()
    {
        @Override
        public void actionPerformed(ActionEvent evt)
        {
        	if( inputMessage.isEnabled() && btnSendMessage.isEnabled() ) {
	        	boolean sendMessage = true;
	        	if( messageHistory != null && messageHistory.checkIfStringEqualsLastEntries(inputMessage.getText()) ) {
	        		sendMessage = false;
	        		// send message to user that he has flooded the chat
	        		settlersLobby.messageHandler.addServerMessage(I18n.getString("SingleChatPane.MSG_FLOODING"));
	        		// disable input-field for 10 seconds
	        		inputMessage.setEditable(false);
	        		btnSendMessage.setEnabled(false);
	        		Timer timer = new Timer(10000, new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							inputMessage.setEditable(true);
							btnSendMessage.setEnabled(true);
						}
	        			
	        		});
	        		timer.start();
	        	}
	        	if( sendMessage ) {
	        		settlersLobby.actionChatMessage(inputMessage.getText());
		            //use invokeLater, as the MessageHistory is also listening to this action event
		            //and uses inputMessage.getText()
		            //if we delete the text too early, messageHistory might miss it
		            SwingUtilities.invokeLater(new Runnable() {
		                @Override
		                public void run()
		                {
		                    inputMessage.setText(""); //$NON-NLS-1$
		                    inputMessage.requestFocusInWindow();
		                }
		            });
	        	}
        	}
        }
    };

    //Ersetzt "/r" automatisch durch "/msg " und den letzten flüsternden user
    private final DocumentListener documentListenerToMessage = new DocumentListener() {

        @Override
        public void removeUpdate(DocumentEvent arg0) {
        }

        @Override
        public void insertUpdate(DocumentEvent e)
        {
            try
            {
                Document doc = e.getDocument();
                if (doc.getLength() >= 3 && doc.getText(0, 3).equals("/r "))
                {
                    final String lastWhisper = privateMessageUserHistory.getFirst();
                    if (lastWhisper != null)
                    {
                        final String text = doc.getText(3, doc.getLength() - 3);
                        EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                inputMessage.setText("/msg " + lastWhisper + " " + text);
                            }
                        });
                    }
                }
            }
            catch (BadLocationException ex)
            {
            }
        }

        @Override
        public void changedUpdate(DocumentEvent arg0) {
        }
    };
	private MessageHistory messageHistory = null;


    public SingleChatPane(SettlersLobby settlersLobby, GUI gui)
    {
        super(settlersLobby, gui);

        // get a graphical button if theme supports it
        if( settlersLobby.theme.isRepeatingButtonStyleAvailable() ) {
        	btnSendMessage = settlersLobby.theme.getButtonWithStyle(I18n.getString("GUI.SEND"));
        }
        
        addKeyBindings();

        if( settlersLobby.theme.singleChatPaneBackgroundImage.length() > 0 ) { 
       		textpaneChat.addBackgroundImageFile(settlersLobby.theme.singleChatPaneBackgroundImage, 24, 24, "right", false);
        }
        
        addKeyBindings();

        textpaneChat.setEditable(false);
        textpaneChat.setDocument(document);
        textpaneChat.setFont(new Font("Tahoma", Font.PLAIN, settlersLobby.config.getFontSize())); //$NON-NLS-1$
        scrollPaneChat.setViewportView(textpaneChat);

        //der Caret wird nur manuell aktualisiert,
        //ansonsten bleibt er genau da, wo er ist.
        //Notwendig damit das Scrollen problemlos abgeschaltet werden kann,
        //falls der Scrollbalken nicht am Ende des Chatfensters ist.
        //Andernfalls müsste man den Caret einmalig verschieben,
        //woraus einmalig eine Verschiebung des Views resultieren würde,
        //was aber nicht gewünscht ist.
        DefaultCaret c = ((DefaultCaret)textpaneChat.getCaret());
        c.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);

        //inputMessage.setBackground(settlersLobby.theme.backgroundColor);
        inputMessage.setFont(textpaneChat.getFont());

        //input Feld auf 500 Zeichen begrenzen,
        //IRC erlaubt maximal 512 inklusive Befehl
        ((AbstractDocument)inputMessage.getDocument()).setDocumentFilter(new DocumentSizeFilter(400));
        
        messageHistory = MessageHistory.attachToInput(inputMessage);
        privateMessageUserHistory = new PrivateMessageUserHistory(inputMessage, this);

        /***********************************************************************
         * Definiere Style-Typen für den Chat-Bereich
         **********************************************************************/

        messageStyle = new SimpleAttributeSet();
        StyleConstants.setForeground(messageStyle, settlersLobby.theme.getTextColor());

        messageStyleBold = new SimpleAttributeSet(messageStyle);
        StyleConstants.setBold(messageStyleBold, true);

        programStyle = new SimpleAttributeSet();
        StyleConstants.setForeground(programStyle, settlersLobby.theme.getTextColor());
        StyleConstants.setItalic(programStyle, true);

        urlStyle = new SimpleAttributeSet();
        StyleConstants.setForeground(urlStyle, settlersLobby.theme.highlightColor);
        StyleConstants.setUnderline(urlStyle, true);

        addListeners();
        setContextMenu();

        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        panel.setOpaque(false);
        //panel.setBackground(settlersLobby.theme.lobbyBackgroundColor);
        //from left to right
        layout.setHorizontalGroup(
	        layout.createParallelGroup()
	           //.addComponent(scrollPaneChat, javax.swing.GroupLayout.DEFAULT_SIZE, 441, Short.MAX_VALUE)
	           .addComponent(scrollPaneChat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
	           .addGroup(
	               javax.swing.GroupLayout.Alignment.TRAILING,
	               layout.createSequentialGroup()
	                   //.addComponent(inputMessage, javax.swing.GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
	                   .addComponent(inputMessage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
	                   .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                   .addComponent(btnSendMessage)
	           )
	        );

        //from top to bottom
        layout.setVerticalGroup(
	        layout.createSequentialGroup()
	            //.addComponent(scrollPaneChat, javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
	            .addComponent(scrollPaneChat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
	            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	            .addGroup(
	                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(inputMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(btnSendMessage)
	            )
	        );
    }

    private void addListeners()
    {
        // Nach Klicken auf "Senden", rufe GUI.addChatMessage(String
        // chatMessage) auf
        // Alt+s über Mnemonic von btnSendMessage
        btnSendMessage.addActionListener(actionListenerToSendMessage);

        // Abschicken der Message mit [Return]
        // Alt+s über Mnemonic von btnSendMessage erledigt
        inputMessage.addActionListener(actionListenerToSendMessage);

        // Ersetzen von "/r" durch "/w nick"
        inputMessage.getDocument().addDocumentListener(documentListenerToMessage);
        
        // remove the content of the input-field if user pressed DEL-key
        // only if enabled in setting
        inputMessage.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyPressed(KeyEvent e) {
				
				if( e.getKeyCode() == KeyEvent.VK_DELETE && false != settlersLobby.config.isDelKey() ) {
					inputMessage.setText("");
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {
			}
        });

        inputMessage.setFocusTraversalKeysEnabled(false);
        addAutoCompleteForMessageField();
        
        //urlListener als MouseListener, falls auf eine URL geklickt wird
        //und als MouseMotionListener, damit über einer URL der Cursor zum
        //HandCursor wird
        UrlListener urlListener = new UrlListener(urlStyle);
        textpaneChat.addMouseListener(urlListener);
        textpaneChat.addMouseMotionListener(urlListener);

    }

    private void setContextMenu()
    {
        // Copy & Paste Kontextmenüs
    	
    	// -> set contextmenu to the input-field (without translation-Option)
    	CopyPastePopup inputMessagePopup = new CopyPastePopup(inputMessage, settlersLobby);
    	inputMessagePopup.setVisibleTranslate(false);
    	inputMessagePopup.setVisibleCopy(false);
		inputMessagePopup.setVisibleCut(false);
    	inputMessagePopup.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
			@Override
			public void popupMenuCanceled(javax.swing.event.PopupMenuEvent arg0) {}
			@Override
			public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent arg0) {}
			@Override
			public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent arg0) {
				inputMessagePopup.setVisiblePaste(true);
				if( null != inputMessage.getSelectedText() && inputMessage.getSelectedText().length() > 0 ) {
					inputMessagePopup.setVisibleCopy(true);
					inputMessagePopup.setVisibleCut(true);
				}
			}
        	
        });
        inputMessage.setComponentPopupMenu(inputMessagePopup);
    	
    	// -> set contextmenu to the chat
        CopyPastePopup chatPopup = new CopyPastePopup(textpaneChat, settlersLobby);
        // ---> disable cut and paste in chat
        chatPopup.setVisibleCut(false);
        chatPopup.setVisiblePaste(false);
        // ---> hide popup if no text is selected
        chatPopup.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
			@Override
			public void popupMenuCanceled(javax.swing.event.PopupMenuEvent arg0) {}
			@Override
			public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent arg0) {}
			@Override
			public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent arg0) {
				if( null == textpaneChat.getSelectedText() ) {
					chatPopup.setVisibleCopy(false);
					chatPopup.setVisibleTranslate(false);
				}
				
			}
        	
        });
        textpaneChat.setComponentPopupMenu(chatPopup);
    }

    @Override
    public void preDisplayChat()
    {
        //als MessageListener registrieren
        addAsMessageListener(settlersLobby.getMessageHandler());

        inputMessage.requestFocus();
    }

    @Override
    public void addInputText(String text)
    {
        inputMessage.setText(inputMessage.getText() + text);
        inputMessage.setCaretPosition(inputMessage.getText().length());
    }

    @Override
    public void setInputText(String text)
    {
        inputMessage.setText(text);
        inputMessage.setCaretPosition(inputMessage.getText().length());
    }

    @Override
    public void selectMessageRecipient(String userName)
    {
        if (userName == null || userName.isEmpty())
        {
            inputMessage.setText("");
        }
        else
        {
            inputMessage.setText("/msg " + userName + " ");
        }
        inputMessage.setCaretPosition(inputMessage.getText().length());
        inputMessage.requestFocus();
    }


    @Override
    public JPanel getPanel()
    {
        return panel;
    }

    @Override
    public void setFontSize(int fontSize)
    {
        inputMessage.setFont(inputMessage.getFont().deriveFont((float) fontSize));
        textpaneChat.setFont(textpaneChat.getFont().deriveFont((float) fontSize));
    }

    private void addKeyBindings()
    {
        btnSendMessage.setMnemonic(KeyEvent.VK_S);
    }

    /**
     * überprüft, ob der aktuelle Scrollbalken vom Chatfenster
     * relativ nah am unteren Ende ist.
     * Falls ja, wird doScroll auf true gesetzt, andernfalls auf false.
     */
    @Override
    protected void checkScrollPosition()
    {
        JScrollBar bar = scrollPaneChat.getVerticalScrollBar();
        if (!bar.isShowing())
        {
            //scrollbar noch nicht sichtbar
            doScroll = true;
            return;
        }
        // getValue() liefert das obere Ende des Schieberreglers.
        // Zusammen mit getVisibleAmount() erhält man das untere
        // Ende des Schiebereglers.
        // Wenn dieses in der Nähe des Maximums liegt,
        // soll weiterhin gescrollt werden.
        // Das Maximum orientiert sich an der "Höhe" des Dokumentes,
        // so dass 10 * fontsize in etwa 10 Zeilen entsprechen sollten
        // (passt nicht ganz, da zwischen den Zeilen noch Abstand ist,
        // aber zumindest ungefähr).
        // Solange zumindest die zehntletzte Zeile zu sehen ist,
        // soll weiter gescrollt werden.
        int val = bar.getValue();
        int maxVal = val + bar.getVisibleAmount();
        int maximum = bar.getMaximum();
        int bias = textpaneChat.getFont().getSize() * 10;
        if (maximum - maxVal < bias)
        {
            doScroll = true;
        }
        else
        {
            doScroll = false;
        }
    }


    /**
     * Es wird die übergebene chatMessage am Ende das Chatnachrichtenfenster
     * angehangen.
     *
     * @param message Die Chatnachricht
     * @param bold
     * @param italic
     * @param newline
     */
    @Override
    protected void addMessage(String message, boolean bold, boolean italic, boolean newline) throws BadLocationException
    {
        if (newline)
        {
            message = message + '\n';
        }
        SimpleAttributeSet style;
        if (!bold && !italic)
        {
            style = messageStyle;
        }
        else if (bold)
        {
            style = messageStyleBold;
        }
        else
        {
            style = programStyle;
        }
        // Insert message
        document.insertString(document.getLength(), message, style);

        if (doScroll)
        {
            //caret ans Ende setzen, damit automatisch gescrollt wird
            textpaneChat.setCaretPosition(document.getLength());
        }
    }



    /**
     * prüft, ob in den letzten length Zeichen eine URL vorhanden ist,
     * und markiert sie.
     * @param document
     * @param length die letzten length Zeichen werden überprüft.
     */
    protected void highlightUrl(StyledDocument document, int length)
    {
        int startOffset = document.getLength() - length;
        startOffset = Math.max(0, startOffset);
        try
        {
            String message = document.getText(startOffset, document.getLength() - startOffset);
            highlightUrl(startOffset, message, document);
        }
        catch (Exception e)
        {
        	Test.outputException(e);
        }
    }
    /**
     * überprüft, ob in message eine Url vorhanden ist. Die message beginnt
     * im document ab offset startOffset. Falls ja wird der Text mit style
     * urlstyle dargestellt.
     * @param startOffset
     * @param message
     * @param document
     */
    private void highlightUrl(int startOffset, String message, StyledDocument document)
    {
        Matcher matcher = Pattern.compile(
        "(?i)(\\b(http://|https://|www.|ftp://|file:/)\\S+)(\\s+)") //$NON-NLS-1$
        .matcher(message);

        while (matcher.find())
        {
            String url = matcher.group(1);
            Matcher dotEndMatcher = Pattern.compile("([\\W&&[^/]]+)$").matcher(url); //$NON-NLS-1$

            //Ending non alpha characters like [.,?%] shouldn't be included
            // in the url.
            int end = url.length();
            if (dotEndMatcher.find())
            {
                end = dotEndMatcher.start();
            }
            //Der Text muss komplett ersetzt werden, da in der Url eventuell
            //ein Highlightingwort vorkommt, wodurch unterschiedliche
            //CharcterElements entstanden wären, wodurch wiederum
            //der Klick auf die Url nicht funktioniert, da dort nur
            //auf das CharacterElement geschaut wird
            try
            {
                document.remove(startOffset + matcher.start(), end);
                document.insertString(startOffset + matcher.start(),
                        url.substring(0, end), urlStyle);
            }
            catch (BadLocationException e) {
            	Test.outputException(e);
            }
        }
    }

    /**
     * Zeigt die message im Chat an. überprüft und markiert HighlightingWords
     * sowie URLs. Bei einem Highlighting wird zusätzlich das Blinken aktiviert.
     *
     * @param message
     * @param bold
     * @param italic
     * @param newline
     * @throws BadLocationException
     */
    @Override
    public void addMessageWithPossibleHighlightingAndUrl(
            String message, boolean bold, boolean italic, boolean newline) throws BadLocationException
    {
        addMessageWithPossibleHighlighting(message, bold, italic, newline);
        //check for URL, +1 wegen \n
        highlightUrl(document, message.length()+1);
    }

    /**
     * Cleart das Chatfenster.
     */
    @Override
    public void clearChat()
    {
        if (textpaneChat != null)
        {
            textpaneChat.setText(""); //$NON-NLS-1$
        }
    }

    private  void addAsMessageListener(MessageHandler messageHandler){
        messageHandler.registerNewListener(this);
    }

    private void removeAsMessageListener(MessageHandler messageHandler){
       messageHandler.removeListener(this);
    }

    @Override
    public void postHideChat()
    {
        removeAsMessageListener(settlersLobby.getMessageHandler());
    }

    @Override
    public synchronized void addWhisperedMessage(String sender, String receiver, String message)
    {
        super.addWhisperedMessage(sender, receiver, message);
        if (!settlersLobby.config.isIgnoredUser(sender))
        {
            if (receiver.equals("")) //$NON-NLS-1$
            {
                privateMessageUserHistory.add(sender);
            }
            else
            {
                privateMessageUserHistory.add(receiver);
            }
        }
    }

    @Override
    public void setChannel(String channel)
    {
    }

	@Override
	public void setKeywordList(Map<String, String> keywords) {
		autocompleteListForInputMessageField = keywords;
	}

	@Override
	public void removeAutoCompleteForMessageField() {
		inputMessage.removeKeyListener(autoCompleteListener);
	}

	@Override
	// Maps the tab key to the commit action, which finishes the completition
	// when given a suggestion
	public void addAutoCompleteForMessageField() {
		inputMessage.addKeyListener(autoCompleteListener);
	}

	@Override
	public String getInputText() {
		return inputMessage.getText();
	}
    
}
