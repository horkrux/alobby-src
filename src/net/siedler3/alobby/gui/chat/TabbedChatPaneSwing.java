package net.siedler3.alobby.gui.chat;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyledDocument;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.gui.additional.MessageHistory;
import net.siedler3.alobby.gui.additional.UrlListener;

public class TabbedChatPaneSwing extends SingleChatPane
{
    private JTabbedPane tabbedPane;
    private JPanel mainChatPanel;
    private JPanel panel;

    public TabbedChatPaneSwing(SettlersLobby settlersLobby, GUI gui)
    {
        super(settlersLobby, gui);
        mainChatPanel = super.getPanel();
        tabbedPane = new JTabbedPane();
        tabbedPane.setBackground(settlersLobby.theme.backgroundColor);
        tabbedPane.addTab("channel", mainChatPanel);
        int index = tabbedPane.indexOfComponent(mainChatPanel);
        tabbedPane.setTabComponentAt(index, new LabelTabComponent(tabbedPane));


        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(tabbedPane);
        panel.setBackground(settlersLobby.theme.lobbyBackgroundColor);

        tabbedPane.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                int index = tabbedPane.getSelectedIndex();
                ITabComponent tabComponent = getTabComponentAt(index);
                if (tabComponent != null)
                {
                    tabComponent.setStyleToNotifyStyle(false);
                }
            }
        });
    }

    @Override
    public JPanel getPanel()
    {
        return panel;
    }

    @Override
    public void setChannel(String channel)
    {
        if (tabbedPane.getTabCount() > 0)
        {
            tabbedPane.setTitleAt(0, channel);
        }
    }


    @Override
    public synchronized void addWhisperedMessage(String sender, String receiver, String message)
    {

        if (!settlersLobby.config.isIgnoredUser(sender))
        {
            String windowName = receiver.isEmpty() ? sender : receiver;

            if (settlersLobby.isShowTime() || settlersLobby.isAway())
            {
                addMessageToWindow(windowName, getCurrentTime() + " ", false, false, false);    //$NON-NLS-1$
            }

            addMessageToWindow(windowName, sender + ": ", true, false, false); //$NON-NLS-1$

            addMessageToWindow(windowName, message, false, false, true, true);

            //Highlight if message received from somebody else
            if (receiver.isEmpty())
            {
                if (settlersLobby.config.isFlashingWhenHighlighted())
                {
                    gui.flashWindowIfNotFocused();
                }
            }
        }
    }

    private ITabComponent getTabComponentOf(JPanel tab)
    {
        int index = tabbedPane.indexOfComponent(tab);
        return getTabComponentAt(index);
    }

    private ITabComponent getTabComponentAt(int index)
    {
        if (index != -1)
        {
            if (tabbedPane.getTabComponentAt(index) instanceof ITabComponent)
            {
                ITabComponent button = (ITabComponent) tabbedPane.getTabComponentAt(index);
                return button;
            }
        }
        return null;
    }

    private JPanel getOrCreateTab(final String tabName)
    {
        JPanel tab = getTab(tabName);
        if (tab == null)
        {
            tab = createMessageTab(tabName);
            if (tab == null)
            {
                throw new NullPointerException();
            }
            tabbedPane.addTab(tabName, tab);
            int index = tabbedPane.indexOfComponent(tab);
            initTabComponent(index);
        }
        return tab;
    }

    private JPanel getTab(String tabName)
    {
        int index = tabbedPane.indexOfTab(tabName);
        if (index != -1)
        {
            return (JPanel) tabbedPane.getComponentAt(index);
        }
        //slow manual search
        for (index = 0; index < tabbedPane.getTabCount(); index++)
        {
            if (tabbedPane.getTitleAt(index).toLowerCase().equals(tabName == null? "" : tabName.toLowerCase()))
            {
                return (JPanel) tabbedPane.getComponentAt(index);
            }
        }
        return null;
    }

    private void markTabAsChanged(final JPanel tab)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                if (tabbedPane.getSelectedComponent() != tab)
                {
                    ITabComponent tabComponent = getTabComponentOf(tab);
                    if (tabComponent != null)
                    {
                        tabComponent.setStyleToNotifyStyle(true);
                    }
                }
            }
        });
    }


    private JPanel createMessageTab(final String sender)
    {
        final JPanel tab = new JPanel();
        tab.setBackground(settlersLobby.theme.lobbyBackgroundColor);
        JTextPane textpaneChat = new JTextPane();
        JScrollPane scrollPaneChat = new JScrollPane();
        final JTextField inputMessage = new JTextField();

        textpaneChat.setEditable(false);
        textpaneChat.setFont(new Font("Tahoma", Font.PLAIN, settlersLobby.config.getFontSize())); //$NON-NLS-1$
        scrollPaneChat.setViewportView(textpaneChat);
        

        inputMessage.setBackground(settlersLobby.theme.backgroundColor);
        inputMessage.setFont(textpaneChat.getFont());

        ((AbstractDocument)inputMessage.getDocument()).setDocumentFilter(new DocumentSizeFilter(500));
        MessageHistory.attachToInput(inputMessage);

        GroupLayout layout = new GroupLayout(tab);
        tab.setLayout(layout);
        //from left to right
        layout.setHorizontalGroup(
                        layout.createParallelGroup()
                           //.addComponent(scrollPaneChat, javax.swing.GroupLayout.DEFAULT_SIZE, 441, Short.MAX_VALUE)
                           .addComponent(scrollPaneChat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                           .addComponent(inputMessage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                        );

        //from top to bottom
        layout.setVerticalGroup(
                        layout.createSequentialGroup()
                            //.addComponent(scrollPaneChat, javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
                            .addComponent(scrollPaneChat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(inputMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        );


        inputMessage.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                String message = inputMessage.getText();
                if (message.startsWith("/")) //$NON-NLS-1$
                {
                    settlersLobby.actionChatMessage(message);
                }
                else
                {
                    String currentSenderName;
                    int index = tabbedPane.indexOfComponent(tab);
                    if (index != -1)
                    {
                        //nick may have changed, use the current title
                        currentSenderName = tabbedPane.getTitleAt(index);
                    }
                    else
                    {
                        currentSenderName = sender;
                    }
                    settlersLobby.actionChatMessage(String.format("/msg %s %s", currentSenderName, message));
                }

                //use invokeLater, as the MessageHistory is also listening to this action event
                //and uses inputMessage.getText()
                //if we delete the text too early, messageHistory might miss it
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run()
                    {
                        inputMessage.setText(""); //$NON-NLS-1$
                        inputMessage.requestFocusInWindow();
                    }
                });
            }
        });

        UrlListener urlListener = new UrlListener(urlStyle);
        textpaneChat.addMouseListener(urlListener);
        textpaneChat.addMouseMotionListener(urlListener);

        return tab;

    }

    private JTextPane getTextAreaFromTab(JPanel tab)
    {
        return (JTextPane) ((JScrollPane)tab.getComponent(0)).getViewport().getView();
    }


    @Override
    public void selectMessageRecipient(final String userName)
    {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run()
            {
                JPanel tab = getOrCreateTab(userName);
                if (tab != null)
                {
                    tabbedPane.setSelectedComponent(tab);
                }
            }

        });
    }

    private void initTabComponent(int i) {
        tabbedPane.setTabComponentAt(i, new ButtonTabComponent(tabbedPane));
    }

    @Override
    protected void addMessage(String message, boolean bold, boolean italic, boolean newline)
                    throws BadLocationException
    {
        super.addMessage(message, bold, italic, newline);
        markTabAsChanged(mainChatPanel);
    }

    private void addMessageToWindow(String windowName, String message, boolean bold, boolean italic, boolean newline)
    {
        addMessageToWindow(windowName, message, bold, italic, newline, false);
    }

    private void addMessageToWindow(final String windowName, String message, boolean bold, boolean italic, boolean newline, final boolean highlightUrl)
    {
        if (newline)
        {
            message = message + '\n';
        }
        final String finalMessage = message;
        final SimpleAttributeSet style;
        if (!bold && !italic)
        {
            style = messageStyle;
        }
        else if (bold)
        {
            style = messageStyleBold;
        }
        else
        {
            style = programStyle;
        }

        // Insert message
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                JPanel tab = getOrCreateTab(windowName);
                JTextPane textArea = getTextAreaFromTab(tab);
                try
                {
                    textArea.getDocument().insertString(textArea.getDocument().getLength(), finalMessage, style);
                    if (highlightUrl)
                    {
                        highlightUrl((StyledDocument)textArea.getDocument(), finalMessage.length());
                    }
                }
                catch (BadLocationException e)
                {
                    Test.outputException(e);
                }
                finally
                {
                    markTabAsChanged(tab);
                }
            }
        });
    }

    @Override
    public void onNickChange(final String oldNick, final String newNick)
    {
        super.onNickChange(oldNick, newNick);

        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                JPanel tab = getTab(oldNick);
                if (tab != null)
                {
                    int index = tabbedPane.indexOfComponent(tab);
                    tabbedPane.setTitleAt(index, newNick);
                    if (tabbedPane.getTabComponentAt(index) != null)
                    {
                        tabbedPane.getTabComponentAt(index).revalidate();
                    }
                }
            }
        });
    }

    @Override
    public void addInputText(String text)
    {
        modifyInputText(text, true);
    }

    @Override
    public void setInputText(String text)
    {
        modifyInputText(text, true);
    }

    private void modifyInputText(final String text, final boolean append)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                int index = tabbedPane.getSelectedIndex();
                if (index != -1)
                {
                    JPanel panel = (JPanel) tabbedPane.getComponentAt(index);
                    for (int i = 0; i < panel.getComponentCount(); ++i)
                    {
                        if (panel.getComponent(i) instanceof JTextField)
                        {
                            JTextField tf = (JTextField) panel.getComponent(i);
                            if (append)
                            {
                                tf.setText(tf.getText() + text);
                            }
                            else
                            {
                                tf.setText(text);
                            }
                            tf.setCaretPosition(tf.getText().length());
                            tf.requestFocusInWindow();
                            break;
                        }
                    }
                }
            }
        });
    }








}
