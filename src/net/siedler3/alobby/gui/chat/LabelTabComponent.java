package net.siedler3.alobby.gui.chat;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JTabbedPane;

public class LabelTabComponent extends JLabel implements ITabComponent
{
    private final JTabbedPane pane;

    public LabelTabComponent(final JTabbedPane pane)
    {
        if (pane == null) {
            throw new NullPointerException("TabbedPane is null");
        }
        this.pane = pane;
    }

    @Override
    public String getText() {
        if (pane != null)
        {
            int i = pane.indexOfTabComponent(LabelTabComponent.this);
            if (i != -1) {
                return pane.getTitleAt(i);
            }
        }
        return null;
    }


    @Override
    public void setStyleToNotifyStyle(boolean value)
    {
        if (value)
        {
            setForeground(Color.red);
        }
        else
        {
            setForeground(null);
        }
    }

}
