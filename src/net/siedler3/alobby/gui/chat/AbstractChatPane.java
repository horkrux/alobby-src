package net.siedler3.alobby.gui.chat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.swing.JPanel;
import javax.swing.text.BadLocationException;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.IMessageListener;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.GUI;
import net.siedler3.alobby.gui.additional.OffsetFinder;
import net.siedler3.alobby.gui.additional.S3User;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.s3gameinterface.S3InGameCheck;
import net.siedler3.alobby.util.Sound;

public abstract class AbstractChatPane implements IChatPane, IMessageListener, LineListener
{

    protected GUI gui;
    protected SettlersLobby settlersLobby;
    protected JPanel panel;
    
    private S3InGameCheck s3InGame = null;
    private Sound Sound = null;

    protected AbstractChatPane(SettlersLobby settlersLobby, GUI gui)
    {
        this.settlersLobby = settlersLobby;
        this.gui = gui;
        panel = new JPanel();
        // ingame check added
        s3InGame = new S3InGameCheck();
        // Sound-Class added
        Sound = new Sound();
    }

    @Override
    public synchronized void addServerMessage(String serverMessage)
    {
        //wird von GUI und IRC aufgerufen, daher synchronized
        checkScrollPosition();
        try
        {
            this.addMessage(I18n.getString("GUI.SERVER") + ": ", false, true, false); //$NON-NLS-1$ //$NON-NLS-2$

            addMessageWithPossibleHighlightingAndUrl(serverMessage, false, true, true);
        }
        catch (BadLocationException e)
        {
            Test.outputException(e);
        }
    }

    protected void checkScrollPosition()
    {
    }

    @Override
    public synchronized void addChatMessage(String user, String message, boolean bold)
    {
        //wird von GUI und IRC aufgerufen, daher synchronized
        checkScrollPosition();
        try
        {
            if (!settlersLobby.config.isIgnoredUser(user)) {
                if(settlersLobby.isShowTime() || settlersLobby.isAway())
                {
                    this.addMessage(getCurrentTime() + " " , false, false, false); //$NON-NLS-1$
                }

                this.addMessage(user + ": ", true, false, false); //$NON-NLS-1$

                addMessageWithPossibleHighlightingAndUrl(message, bold, false, true);
            }
        }
        catch (BadLocationException e)
        {
            Test.outputException(e);
        }
    }

    @Override
    public synchronized void addWhisperedMessage(String sender, String receiver, String message)
    {
        //wird von GUI und IRC aufgerufen, daher synchronized
        checkScrollPosition();
        try
        {
            if (!settlersLobby.config.isIgnoredUser(sender)) {
                if(settlersLobby.isShowTime() || settlersLobby.isAway())
                    this.addMessage(getCurrentTime() + " ", false, false, false);    //$NON-NLS-1$

                this.addMessage(sender, true, false, false);
                this.addMessage(" [" + I18n.getString("GUI.WHISPER"), false, false, false); //$NON-NLS-1$ //$NON-NLS-2$
                if (!receiver.equals("")) //$NON-NLS-1$
                {
                    this.addMessage(" " + I18n.getString("GUI.TO") + " " + receiver, false, false, false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                }
                this.addMessage("]: ", false, false, false); //$NON-NLS-1$

                addMessageWithPossibleHighlightingAndUrl(message, false, false, true);

                //Automatisches Highlighten, wenn von jemand anders geflüstert wird
                if (receiver.equals("")) //$NON-NLS-1$
                {
                    if (settlersLobby.config.isFlashingWhenHighlighted())
                    {
                        gui.flashWindowIfNotFocused();
                    }
                    // play blink-sound
                    playBlinkSound();
                }
            }
        }
        catch (BadLocationException e)
        {
            Test.outputException(e);
        }
    }

    @Override
    public synchronized void addNoticeMessage(String sender, String receiver, String message)
    {
        //wird von GUI und IRC aufgerufen, daher synchronized
        checkScrollPosition();
        try
        {
            if (!settlersLobby.config.isIgnoredUser(sender)) {
                if(settlersLobby.isShowTime() || settlersLobby.isAway())
                    this.addMessage(getCurrentTime() + " ", false, false, false); //$NON-NLS-1$

                this.addMessage(sender, true, true, false);

                this.addMessage(" [" + I18n.getString("GUI.MSG_NOTICE"), false, true, false); //$NON-NLS-1$ //$NON-NLS-2$
                if (!receiver.equals("")) //$NON-NLS-1$
                {
                    this.addMessage(" " + receiver, false, true, false); //$NON-NLS-1$
                }
                this.addMessage("]: ", false, true, false); //$NON-NLS-1$

                addMessageWithPossibleHighlightingAndUrl(message, false, false, true);
            }
        }
        catch (BadLocationException e)
        {
            Test.outputException(e);
        }
    }

    @Override
    public synchronized void addActionMessage(String user, String message)
    {
        //wird von GUI und IRC aufgerufen, daher synchronized
        checkScrollPosition();
        try
        {
            if (!settlersLobby.config.isIgnoredUser(user)) {
                if (settlersLobby.isShowTime() || settlersLobby.isAway())
                {
                    this.addMessage(getCurrentTime() + " " , false, false, false); //$NON-NLS-1$
                }

                this.addMessage("* " + user + " ", true, false, false); //$NON-NLS-1$ //$NON-NLS-2$

                addMessageWithPossibleHighlightingAndUrl(message, false, false, true);
            }
        }
        catch (BadLocationException e)
        {
            Test.outputException(e);
        }

    }

    @Override
    public void selectMessageRecipient(S3User user)
    {
        selectMessageRecipient(user.getNick());
    }

    public void addMessageWithPossibleHighlighting(String message, boolean bold, boolean italic, boolean newline)
                    throws BadLocationException
    {
        OffsetFinder offsetFinder = new OffsetFinder(message);
        offsetFinder.markWord(settlersLobby.getNick());
        offsetFinder.markWords(settlersLobby.config.getHighlightingWords());
        if (offsetFinder.isWordsMarked())
        {
            if (bold)
            {
                //Wenn die Nachricht komplett fett gedruckt wird, braucht
                //man kein Highlighting
                addMessage(message, bold, italic, newline);
            }
            else
            {
                addMessageWithHighlighting(message, italic, offsetFinder.getOffsetsStart(), offsetFinder.getOffsetsEnd());
                // play blink-sound
                playBlinkSound();
            }
            gui.flashWindowIfNotFocused();
        }
        else
        {
            addMessage(message, bold, italic, newline);
        }
    }
    
    /**
     * 	one function for playing blink-sound
     * 	@throws BadLocationException
     */
    private void playBlinkSound() throws BadLocationException {
    	// add beep sound when the lobby would blink/highlight
        // only if user is currently not playing
        // and only if a soundfile for this situation is configured
        // and the playback is activated
        if (!s3InGame.isInGame() && settlersLobby.config.isPlaySoundWhenHighlighted() )
        {
        	String soundFile = settlersLobby.config.getBlinkSoundFile();
        	// check if an user-spezific soundFile is provided
        	// and if so, check if the file has a length
            if (soundFile == null || soundFile.length() == 0)
            {
            	// set soundfile to play, when no user-specific soundfile is available
            	// Out of this free-to-use file:
            	// http://www.pdsounds.org/sounds/thunderstorm_after_hot_summer_day_17_minutes_03_of_04
            	soundFile = ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/thunder.wav";
            	// if the theme is the christmas-theme, than use another sound-file
            	if( settlersLobby.config.isChristmasTheme() ) {
            		soundFile = ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/bells.wav";
            	}
            }
    		// call function to play the configured sound
    		Sound.addFileAsString(soundFile);
    		// add this class as Listener (e.g. when playback should stop)
    		Sound.addListener(this);
    		// play the sound
    		Sound.play();
        }
    }
    
    protected abstract void addMessage(String message, boolean bold, boolean italic, boolean newline) throws BadLocationException;
    protected abstract void addMessageWithPossibleHighlightingAndUrl(
                    String message, boolean bold, boolean italic, boolean newline) throws BadLocationException;


    /**
     * Fügt die Message dem Chat hinzu. Die in den offsets markierten
     * Wörter werden jeweils fett gedruckt.
     * @param message Die gesamt Nachricht.
     * @param italic Ob die Nachricht kursiv gezeichnet werden soll.
     * @param offsetsStart Die Startmarken der fett zu druckenden Wörter.
     * @param offsetsEnd Die Endmarken der fett zu druckenden Wörter.
     * @throws BadLocationException
     */
    void addMessageWithHighlighting(String message, boolean italic, Vector<Integer> offsetsStart, Vector<Integer> offsetsEnd) throws BadLocationException
    {
        if (offsetsStart.elementAt(0) > 0) {
            addMessage(message.substring(0, offsetsStart.elementAt(0)), false, italic, false);
        }
        for (int i = 0; i < offsetsStart.size()-1; i++) {
            addMessage(message.substring(offsetsStart.elementAt(i), offsetsEnd.elementAt(i)), true, italic, false);
            addMessage(message.substring(offsetsEnd.elementAt(i), offsetsStart.elementAt(i+1)), false, italic, false);
        }
        addMessage(message.substring(offsetsStart.lastElement(), offsetsEnd.lastElement()), true, italic, false);
        if (offsetsEnd.lastElement() < message.length()) {
            addMessage(message.substring(offsetsEnd.lastElement()), false, italic, true);
        } else {
            addMessage("", false, italic, true); //$NON-NLS-1$
        }
    }

    protected String getCurrentTime()
    {
        return new SimpleDateFormat("HH:mm").format(new Date()); //$NON-NLS-1$
    }

    @Override
    public void showTopicMessage(String channel, String topic)
    {
        addServerMessage(topic);
    }

    @Override
    public void onNickChange(String oldNick, String newNick)
    {
    }
    
    // function to respond on LineListener-Events
    @Override
    public void update(LineEvent event)
    {
    	if (event.getType() == LineEvent.Type.STOP)
        {
	    	// remove Listener from Sound-Clip
	    	Sound.removeListener(this);
	    	// stop Sound
	    	Sound.stop();
	    	// close Sound
	    	Sound.close();
        }
    }
    

}
