package net.siedler3.alobby.gui;

import java.awt.Dialog;
import java.awt.Label;

import javax.swing.JFrame;

import net.siedler3.alobby.controlcenter.SettlersLobby;

public class WaitDialog extends Dialog {

	private SettlersLobby settlersLobby;

	public WaitDialog(Dialog owner) {
		super(owner);
	}
	
	@SuppressWarnings("static-access")
	public WaitDialog(SettlersLobby settlersLobby, JFrame currentActiveFrame, String string, boolean b, String label) {
		super(currentActiveFrame, string, b);
		this.settlersLobby = settlersLobby;
		add(new Label(label));
		setIconImage(this.settlersLobby.getGUI().getIcon());
		setBackground(this.settlersLobby.theme.lobbyBackgroundColor);
		setForeground(this.settlersLobby.theme.getTextColor());
		pack();
		setLocation(this.settlersLobby.getGUI().getCenterLocationForWindow(this));		
	}
	
}
