/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.swing.AbstractAction;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.Tetris;
import net.siedler3.alobby.util.httpRequests;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

/**
 * Create the Tetris-Dialog as JPanel which will be insert into the OpenNewGameDialog-JFrame.
 * 
 * @author Zwirni
 *
 */
public class OpenTetrisGameDialog extends JPanel {
	
	@SuppressWarnings("unused")
	private GUI gui;
	private JLabel lblHighscoreHint = new JLabel();
	private JCheckBox chkShowPreview;

	// initialize the object
	@SuppressWarnings("static-access")
	public OpenTetrisGameDialog( SettlersLobby settlersLobby, GUI gui ) {
		this.gui = gui;
				
		// set the size for the form
		setSize(new Dimension(530, 360));
		setPreferredSize(new Dimension(640, 400));
		setMinimumSize(new Dimension(530, 360));
		setLocale(Locale.GERMANY);
		setAutoscrolls(true);
		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
				
		// set properties for this panel
		this.setAlignmentX(LEFT_ALIGNMENT);
		this.setAlignmentY(TOP_ALIGNMENT);
		
		// create the highscore hint which will be shown on top together with play-button
		JPanel pnlHighscoreHint = new JPanel();
		String text = I18n.getString("Tetris.NO_HIGHSCORE");
		if( settlersLobby.config.getTetrisHighscore() > 0 ) {
			text = String.format("%s %s %s", I18n.getString("Tetris.HIGHSCORE1"), settlersLobby.config.getTetrisHighscore(), I18n.getString("Tetris.HIGHSCORE2"));
		}
		lblHighscoreHint.setText(text);
		pnlHighscoreHint.add(lblHighscoreHint);
				
		// create play-button
		JButton btnStartGame = new JButton(I18n.getString("OpenNewS3GameDialog.BUTTON_STARTGAME"));
		// -> add repeating backgroundImage to the buttons, if defined for actual theme
		if( settlersLobby.theme.isRepeatingButtonStyleAvailable() ) {
			btnStartGame = settlersLobby.theme.getButtonWithStyle(btnStartGame.getText());
		}
				
		// create settings-panel
		JPanel pnlSettings = new JPanel();
		// -> enable or disable the preview-panel
		chkShowPreview = new JCheckBox();
		chkShowPreview.setText(I18n.getString("Tetris.PREVIEW_VISIBILITY"));
		pnlSettings.add(chkShowPreview);
				
		// create description-area
		JEditorPane description = new JEditorPane();
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
				description.setEditable(false);
				description.setContentType("text/html");
				description.setOpaque(false);
				description.setBorder(null);
				description.setSize(20,56);
				HTMLEditorKit kit = new HTMLEditorKit();
				StyleSheet styleSheet = kit.getStyleSheet();
				Color color = settlersLobby.theme.getStaticTextColor();
				styleSheet.addRule(String.format("body, a { color: rgb(%s, %s, %s);font-family: Tahoma;font-size: 10px; }", color.getRed(), color.getGreen(), color.getBlue()));
				Document doc = kit.createDefaultDocument();
				description.setDocument(doc);
				description.setText(I18n.getString("Tetris.DESCRIPTION"));
            }
		});
		JScrollPane scrollPane = new JScrollPane(description);
 		scrollPane.getViewport().setOpaque(false);
 		scrollPane.setOpaque(false);
 		scrollPane.setBorder(null);
 		description.addHyperlinkListener(new HyperlinkListener() {
 		    public void hyperlinkUpdate(HyperlinkEvent e) {
 		        if(e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
 		        	if (Desktop.isDesktopSupported())
 		        	{
 		        		settlersLobby.getGUI().openURL(e.getURL());
 		        	}
 		        }
 		    }
 		});
		add(scrollPane);
				
		// put all together in a layout
        GroupLayout layout = new GroupLayout(this);
        layout.setHorizontalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addGroup(layout.createParallelGroup(Alignment.CENTER)
        				.addGroup(layout.createSequentialGroup()
        					.addComponent(pnlHighscoreHint, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        					.addPreferredGap(ComponentPlacement.RELATED)
        					.addComponent(btnStartGame))
        				.addComponent(pnlSettings, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        				.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        			.addContainerGap())
        );
        layout.setVerticalGroup(
        	layout.createParallelGroup(Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
        			.addGroup(layout.createParallelGroup(Alignment.CENTER)
        				.addComponent(pnlHighscoreHint, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        				.addComponent(btnStartGame))
        			.addGap(6)
        			.addComponent(pnlSettings, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
        			.addGap(6)
        			.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );
        setLayout(layout);
        layout.setAutoCreateContainerGaps(true);

        // add actionListener to start the game
		btnStartGame.addActionListener(new AbstractAction()
		{
			
			// marker if tetris is actual running
			boolean running = true;
			
			// pause
			boolean paused = false;

			// starttime
			private long starttime = 0;
			
			private double tps;
			private int  fps;
			private long  delta;
			private int  fpsLimit = 60;
			int frames = 0;
			long startTimer = System.currentTimeMillis();
			long updateTimer = 0;
			long timer1 = System.nanoTime();
			long timer2 = 0;
			int nextdelta = 0;
			private Tetris game;
			private boolean movedTetrinimo = false;
			private boolean movedTetrinimoDeltaDone = false;
			private boolean softdropTetrinimo = false;
			private boolean softdropTetrinimoDeltaDone = false;
			private int lastPressedKey;
			private Integer[] moveKeys = new Integer[]{ KeyEvent.VK_LEFT, KeyEvent.VK_RIGHT, KeyEvent.VK_NUMPAD4, KeyEvent.VK_NUMPAD6 };
			private Integer[] softdropKeys = new Integer[] { KeyEvent.VK_DOWN, KeyEvent.VK_NUMPAD2 };
			
			@Override
			public void actionPerformed(ActionEvent evt)
			{
				
				if( false == settlersLobby.isPlayingTetris() ) {
				
					Calendar calendar = Calendar.getInstance();
			    	calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
			    	starttime  = calendar.getTimeInMillis();
					settlersLobby.setPlayingTetris(true);
					chkShowPreview.setEnabled(false);
					
					JFrame f = new JFrame("Tetris");
					// set close-operation
					f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					// set default aLobby-Icon
					gui.setIcon(f);
					// set this window focusable
					f.setFocusableWindowState(true);
					
					game = new Tetris();
					game.setChristmas(settlersLobby.config.itsChristmastime());
					if( chkShowPreview.isSelected() ) {
						game.showPreview(6);
					}
					game.init();
					f.getContentPane().add(game);
					f.setSize(game.getPlayfieldWidth()*game.getBlockWidthHeight()+game.getWindowSizeWidth(), game.getPlayfieldHeight()*game.getBlockWidthHeight());
					
					// make window visible and not resizeable
					f.setResizable(false);
					f.setVisible(true);
					// center this window
					f.setLocationRelativeTo(gui.getMainWindow());
					f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
					
					setState(true, game);
					
					// if window closes post actual points in chat
					f.addWindowListener(new WindowAdapter(){
						public void windowClosing(WindowEvent e){
							if( game.getScore() > 0 && game.getRows() > 0 ) {
								if( false == chkShowPreview.isSelected() ) {
									// save the result to the highscore-table
									// start first request to get actual VPN-Key-Settings
						        	httpRequests httpRequests = new httpRequests();
									httpRequests.setUrl(settlersLobby.config.getTetrisHighscoreUrl() + settlersLobby.getNick());
									httpRequests.addParameter("rows", game.getRows());
									httpRequests.addParameter("level", game.getLevel());
									httpRequests.addParameter("score", game.getScore());
									httpRequests.addParameter("starttime", starttime);
									Calendar calendar = Calendar.getInstance();
							    	calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
									httpRequests.addParameter("endtime", calendar.getTimeInMillis());
									// -> get the response-String
									try {
										httpRequests.doRequest();
									} catch (IOException e1) {
									}
								}
								settlersLobby.actionChatMessage("/me " + I18n.getString("Tetris.HAVE") + " " + game.getScore() + " " + I18n.getString("Tetris.POINTS") + " " + game.getLevel() + " " + I18n.getString("Tetris.GET"));
								// and save the new score if it is a new highscore
								if( settlersLobby.config.getTetrisHighscore() < game.getScore() ) {
									settlersLobby.config.setTetrisHighscore(game.getScore());
									lblHighscoreHint.setText(I18n.getString("Tetris.HIGHSCORE1") + " " + game.getScore() + " " + I18n.getString("Tetris.HIGHSCORE2"));
								}
							}
							// enable the settings
							chkShowPreview.setEnabled(true);
							// set player not playing tetris anymore
							settlersLobby.setPlayingTetris(false);
							// stop tetris-thread after it is closed
							game.gameover = true;
							// and close the window
							f.dispose();
						}
					});
					
					// Keyboard controls
					f.addKeyListener(new KeyListener() {

						public void keyTyped(KeyEvent e) { /* not used */ }
						
						public void keyPressed(KeyEvent e) {
							// save the last pressed key, if it is another than now
							if( lastPressedKey != e.getKeyCode() ) {
								lastPressedKey = e.getKeyCode();
							}
							// key-combination: ctrl + arrow-up => rotate the block counterclockwise
							if ((e.getKeyCode() == KeyEvent.VK_UP) && ((e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0)) {
								game.rotate(+1, false);
			                }
							else {
								switch (e.getKeyCode()) {
									// rotate clockwise
									case KeyEvent.VK_UP:
									case KeyEvent.VK_X:
									case KeyEvent.VK_NUMPAD1:
									case KeyEvent.VK_NUMPAD5:
									case KeyEvent.VK_NUMPAD9:
										if( running && false == game.gameover ) {
											game.rotate(-1, false);
										}
										break;
									// rotate counter-clockwise
									case KeyEvent.VK_Y:
									case KeyEvent.VK_NUMPAD3:
									case KeyEvent.VK_NUMPAD7:
										if( running && false == game.gameover ) {
											game.rotate(+1, false);
										}
										break;
									// soft drop
									case KeyEvent.VK_DOWN:
									case KeyEvent.VK_NUMPAD2:
										if( running && false == game.gameover ) {
											game.dropDown();
											softdropTetrinimo = true;
											game.score += 1;
										}
										break;
									// move to the left
									case KeyEvent.VK_LEFT:
									case KeyEvent.VK_NUMPAD4:
										if( running && false == game.gameover ) {
											if( game.move(-1, false) ) {
												movedTetrinimo = true;
											}
											else {
												movedTetrinimo = false;
											}
										}
										break;
									// move to the right
									case KeyEvent.VK_RIGHT:
									case KeyEvent.VK_NUMPAD6:
										if( running && false == game.gameover ) {
											if( game.move(+1, false) ) {
												movedTetrinimo = true;
											}
											else {
												movedTetrinimo = false;
											}
										}
										break;
									// hard drop
									case KeyEvent.VK_SPACE:
									case KeyEvent.VK_NUMPAD8:
										if( running && false == game.gameover ) {
											game.hardDropDown();
										}
										break;
									// pause game or end pause of the game
									case KeyEvent.VK_ESCAPE:
									case KeyEvent.VK_F1:
										if( false == game.gameover ) {
											if ( running ) {
												setState(false, game);
												paused = true;
											}
											else {
												setState(true, game);
												paused = false;
											}
										}
										break;
									// hold actual piece
									case KeyEvent.VK_C:
									case KeyEvent.VK_SHIFT:
										if( running && false == game.gameover ) {
											game.setPieceInHold();
										}
										break;
								}
							}
						}
						
						public void keyReleased(KeyEvent e) {
							// if a block was moved and the move-key was released
							// set this setting to false
							if( Arrays.asList(moveKeys).contains(e.getKeyCode()) ) {
								movedTetrinimo = false;
								movedTetrinimoDeltaDone = false;
							}
							// if a block was softdropped and the softdrop-key was released
							// set this setting to false
							if( Arrays.asList(softdropKeys).contains(e.getKeyCode()) ) {
								softdropTetrinimo = false;
								softdropTetrinimoDeltaDone = false;
							}
						}
					});
					
					// Make the falling piece drop every second
					Thread tetrisThread = new Thread() {

						@Override public void run() {
							while (false == game.gameover) {
								settlersLobby.userWasActive();
								timer2 = System.nanoTime();
								tps = (double)(timer2 - timer1) / 1000000000;
								timer1 = timer2;

								delta = game.getSleepTime() + nextdelta;
								
								// if a block is moved reduce the delta
								// to a value which allows to move the block from left to right
								// of the playfield in 0.5 seconds
								if( false == movedTetrinimoDeltaDone && false != movedTetrinimo && Arrays.asList(moveKeys).contains(lastPressedKey) ) {
									delta = (long) (0.5 / game.getPlayfieldWidth() * 1000);
									movedTetrinimoDeltaDone = true;
								}
								
								// if a block is softdropped reduce the delta
								// to a value 20 times of actual delta
								if( false == softdropTetrinimoDeltaDone && false != softdropTetrinimo && Arrays.asList(softdropKeys).contains(lastPressedKey) ) {
									delta = delta / 20;
									softdropTetrinimoDeltaDone = true;
								}
								
								if(fpsLimit != 0) {

									if( false == paused ) {
										if ( KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == null ) {
											setState(false, game);
										}
										else {
											setState(true, game);
										}
									}
									game.pause(delta);
									frames++;
									updateTimer = System.currentTimeMillis();
	
									if(updateTimer - startTimer >= 1000) {
										fps = frames;
										frames = 0;
										startTimer = System.currentTimeMillis();
										if(fps > fpsLimit) {
											nextdelta++;
										} else if(fps < fpsLimit && nextdelta > 0) {
											nextdelta--;
										}
									}

								} else {
									fps = (int)(1 / tps);
								}

								if( running ) {
									if( false == movedTetrinimo ) {
										if( game.dropDown() ) {
											game.pause(delta);
										}
									}
								}
							}
						}
					};
					tetrisThread.setName("tetrisThread");
					tetrisThread.start();
				}
			}
			
			// set the state of tetris (running or not)
			public void setState( boolean newState, Tetris game ) {
				running = newState;
				game.setEnabled(newState);
				game.printPauseOnScreen(newState ? false : true);
			}
			
		});
		
		//add(btnStartGame);		
		
	}
	
}

