/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.ComponentOrientation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

import net.siedler3.alobby.gui.additional.MouseWheelListenerForJTabbedPane;

/**
 * Generate a window with tab-navigation to open new game(s).
 * 
 * @author Zwirni
 *
 */
public class OpenNewGameDialog extends JFrame {
	
	// path to chosen map
	private String mapPath;
	
	// gui-Object
	private GUI gui;
	
	// list of tabs in this window
	private JTabbedPane tabLeiste = new JTabbedPane();

	private boolean resetOnClose = false;
	
	// initialize the Object
	protected OpenNewGameDialog(GUI gui) {
		this.gui = gui;
		
		// set the window-size
		this.setSize(500, 500);
		
		// set always on top so user must use it
		//this.setAlwaysOnTop(true);
		
		// set the default text-orientation in this window (possible language-depending)
		this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		// if window is closed reset the dialog in GUI if requested
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	if( false != resetOnClose ) {
		    		gui.resetMapChoosingDialog();
		    	}
		    	dispose();
		    }
		});
		
		this.tabLeiste.addMouseWheelListener(new MouseWheelListenerForJTabbedPane());
		
		// center this window
		this.setLocationRelativeTo(gui.getMainWindow());
		
		// disable that the window is resizable
		// (this would result in terrible views ..)
		//this.setResizable(false);
		
	}
	
	/**
	 * Add a new Tab to the Dialog
	 * @param tabName
	 * @param s3PanelDialog
	 * 
	 * @return JFrame
	 */
	public OpenNewGameDialog addTab( String tabname, JPanel s3PanelDialog ) {
		
		// add the panel to dialog
		this.tabLeiste.addTab(tabname, s3PanelDialog);
		
		// return the window
		return this;
	}
	
	/**
	 * Add a new Tab to the Dialog
	 * @param tabName
	 * 
	 * @return panel
	 */
	public OpenNewGameDialog addTab( String tabname ) {
		return this.addTab( tabname, new JPanel() );
	}
	
	/**
	 * Return all Tabs in this dialog.
	 * 
	 */
	public JTabbedPane getTabListe() {
		return this.tabLeiste;
	}
	
	/**
	 * Return the Path to the chosen map
	 * 
	 * @return mapPath
	 */
	protected String getMapPath() {
		return this.mapPath;
	}

	/**
	 * Show the dialog with all components
	 */
	protected void display() {
		
		// set default aLobby-Icon
		this.gui.setIcon(this);
		
		// if only one tab is visible, then 
		// hide the tab navbar through set the height of it to zero. 
		this.tabLeiste.setUI(new BasicTabbedPaneUI() {  
		    @Override  
		    protected int calculateTabAreaHeight(int tab_placement, int run_count, int max_tab_height) {  
		        if (tabLeiste.getTabCount() > 1)
		            return super.calculateTabAreaHeight(tab_placement, run_count, max_tab_height);  
		        else  
		            return 0;  
		    } 
		});
		
		// add the tabs to the window
		this.add(this.tabLeiste);
		
		// shrink the size of the window to the only necessary size
		this.pack();
		
		// show the dialog
		this.setVisible(true);
		
		// set the window-position
		this.setLocationRelativeTo(this.gui.getMainWindow());
		
	}

	/**
	 * If this dialog should be resetted on closing, the b to true
	 */
	public void resetOnClose(boolean b) {
		this.resetOnClose  = b;
	}
	
	
}