/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.gui;

import java.awt.Dialog;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.additional.LanguageComboBox;
import net.siedler3.alobby.gui.additional.addCursorChangesToButton;
import net.siedler3.alobby.i18n.I18n;

/**
 * Dialog, der für die Auswahl der Sprache da ist.
 * @author Stephan Bauer (aka maximilius)
 */
public class LanguageDialog extends JDialog {

	private LanguageComboBox cobLanguages;
	private Configuration config;
	private JLabel label;
	/**
	 * Erzeugt den fertigen Dialog.
	 * @param config Die Configuration.
	 */
	public LanguageDialog(Configuration config, SettlersLobby settlersLobby) {
		super(null, I18n.getString("LanguageDialog.TITLE"), Dialog.ModalityType.APPLICATION_MODAL); //$NON-NLS-1$
		this.config = config;

		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);

		setLayout(new GridBagLayout());
		cobLanguages = new LanguageComboBox();
		JButton btnOk = new JButton(I18n.getString("LanguageDialog.BUTTON")); //$NON-NLS-1$
		btnOk.addMouseListener(new addCursorChangesToButton());
		btnOk.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(ActionEvent e) {
				LanguageDialog.this.config.setLanguage(cobLanguages.getSelectedLocale());
				setVisible(false);
				dispose();
			}
		});

		label = new JLabel(I18n.getString("LanguageDialog.LABEL"));
		getContentPane().setBackground(settlersLobby.theme.backgroundColor);
		cobLanguages.setBackground(settlersLobby.theme.backgroundColor);
		cobLanguages.setForeground(settlersLobby.theme.getTextColor());
		cobLanguages.getEditor().getEditorComponent().setBackground(settlersLobby.theme.backgroundColor);

		add(label, new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(10, 10, 10, 10), 0, 0)); //$NON-NLS-1$
		add(cobLanguages, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 10, 10, 10), 0, 0));
		add(btnOk, new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 10, 10, 10), 0, 0));
		pack();

		setLocation(GUI.getCenterLocationForWindow(this));
	}


}
