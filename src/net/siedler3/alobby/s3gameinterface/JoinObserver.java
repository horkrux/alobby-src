/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import net.siedler3.alobby.controlcenter.Test;

/**
 * Überwacht den Host und meldet Spieleranzahländerungen und Spielstart.
 *
 * @author Stephan Bauer
 * @version 0.1
 */
public class JoinObserver extends ObserverFramework
{
	private String ip;
	private String hostUserName;
	private String mapName;
	private int goods;
	private boolean league;
	private int tournamentid;
	private String tournamentname;

	protected JoinObserver(GameStarterVanilla gameStarter, String ip, String hostUserName, String mapName, int goods, boolean league, int tournamentid, String tournamentname)
	{
		super(gameStarter);
		this.ip = ip;
		this.hostUserName = hostUserName;
		this.mapName = mapName;
		this.goods = goods;
		this.league = league;
		this.tournamentid = tournamentid;
		this.tournamentname = tournamentname;
		setName(getName() + "-JoinObserver");
	}

	@Override
    public void run()
	{
	    try
	    {
        	observeTillMiniChat();
        	sendOrderToIncrement();
        	observeMiniChat();
	    }
	    catch (InterruptedException e)
	    {
	        //abort
	        Thread.currentThread().interrupt();
	    }
	    finally
	    {
	    	gameStarter.setRunning(false);
	    }
	}

	private void sendOrderToIncrement()
	{
		if (!isInterrupted())
		{
			settlersLobby.onSendIncrementPlayerNumber(hostUserName);
		}
	}

    private void observeMiniChat() throws InterruptedException
    {
        do
        {
            sleep(1000);
        } while (!isInterrupted() && !startColor[currentEdition].match() && !s3InGameCheck.isInGame());
        if (!isInterrupted())
        {
            setStartTimeInOpenGame();
            settlersLobby.actionGameStarted(this.hostUserName, this.ip, this.mapName, this.goods, this.league, this.tournamentid, this.tournamentname);
        }
    }

    private void setStartTimeInOpenGame()
    {
        try
        {
            settlersLobby.getOpenJoinGame().setStartTime(System.currentTimeMillis());
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
    }

}
