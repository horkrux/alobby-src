/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import net.siedler3.alobby.controlcenter.GoodsInStock;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.s3gameinterface.GameStarter.StartOptions;

/**
 * Überwacht den Host und meldet Spieleranzahländerungen und Spielstart.
 *
 * @author Stephan Bauer
 * @version 0.1
 */
public class HostObserver extends ObserverFramework
{
    private static class GoodsInStockEntry
    {
        public GoodsInStock goodsInStock;
        public ImageChecker imageChecker;

        GoodsInStockEntry(GoodsInStock goodsInStock,
                ImageChecker imageChecker)
        {
            super();
            this.goodsInStock = goodsInStock;
            this.imageChecker = imageChecker;
        }
    }

    private boolean hostWasAway = true;
    private boolean isAmaz = false;
    private GoodsInStock goodsInStock = null;
    private GoodsInStockEntry[] imageCheckerGoods;
    private MultiImageChecker multiCheckerGoods = new MultiImageChecker();
    private int lastGoodsInStockMatchIndex = -1;
    private MultiImageChecker namesInChatChecker;
    private boolean isWimo = false;

    protected HostObserver(GameStarterVanilla gameStarter)
    {
        super(gameStarter);
        setName(getName() + "-HostObserver");
        createImageCheckers();
    }

    private void createImageCheckers()
    {
        imageCheckerGoods = new GoodsInStockEntry[GoodsInStock.values().length];
        ImageChecker imageChecker;
        int i = 0;
        for (GoodsInStock goods : GoodsInStock.values())
        {
            String key = goods.getLanguageMiniChatString();
            BufferedImage image = ReferenceImageHandling.getReferenceImage(key, true);
            //"Warenbestand: Wenig/Mittel/Viel"
            imageChecker = new ImageChecker(robot, 500, 45, 250, 25, image, true, true, key);
            imageChecker.setScreenSizeS3Menu();
            imageCheckerGoods[i++] = new GoodsInStockEntry(goods, imageChecker);
            multiCheckerGoods.add(imageChecker);
        }
    }

    private void sendOpenGame()
    {
        if (!isInterrupted())
        {
        	StartOptions startOptions = gameStarter.getStartOptions();
        	// if goodsInStock could not be read from S3
        	// use the value from the "new game"-dialog
        	if( goodsInStock == null ) {
        		goodsInStock = startOptions.getSelectGoodsInStock();
        	}
            settlersLobby.newGame(startOptions.getGameName(), startOptions.getMapName(),
                    maxPlayerNumber, currentPlayerNumber, isAmaz, goodsInStock, startOptions.isLeagueGame(), isWimo, startOptions.getTournamentId(), startOptions.getTournamentName(), startOptions.getTournamentRound(), startOptions.getTournamentGroupnumber(), startOptions.getTournamentMatchnumber(), startOptions.getTournamentPlayerlist(), new ArrayList<>());
        }
    }

    private void observeMiniChat() throws InterruptedException
    {
        int newCurrentPlayerNumber;
        int newMaxPlayerNumber;
        GoodsInStock newGoodsInStock = null;
        Test.output("observing minichat");
        do
        {
            sleep(1000);
            try
            {
            	Test.output("hostWasAway: " + hostWasAway);
                newCurrentPlayerNumber = getCurrentPlayerNumber();
                newMaxPlayerNumber = getMaxPlayerNumber();
                newGoodsInStock = getGoodsInStock();

                if (hostWasAway || maxPlayerNumber != newMaxPlayerNumber)
                {
                	Test.output("set maxplayernumber to " + newMaxPlayerNumber);
                    maxPlayerNumber = newMaxPlayerNumber;
                    settlersLobby.newMaxPlayerNumber(maxPlayerNumber);
                }

                if (hostWasAway || currentPlayerNumber != newCurrentPlayerNumber)
                {
                	Test.output("set currentplayernumber to " + newCurrentPlayerNumber);
                    currentPlayerNumber = newCurrentPlayerNumber;
                    settlersLobby.newCurrentPlayerNumber(currentPlayerNumber);
                }

                //hostWasAway nicht nötig für den Warenbestand, da er sich
                //normalerweise nicht ändert
                if (newGoodsInStock != null && goodsInStock != newGoodsInStock)
                {
                    goodsInStock = newGoodsInStock;
                    if (goodsInStock != null)
                    {
                        Test.output("GoodsInStock: " + goodsInStock.toString());
                    }
                }

                checkNamesInChat();

                setHostAway(false);
            } catch (Exception e)
            {
                setHostAway(true);
            }
        //if the startColor matcher fails, then also check if the game is already active
        } while (!isInterrupted() && !startColor[currentEdition].match() && !s3InGameCheck.isInGame());
        if (!isInterrupted())
        {
            Test.output("Game started!");
            setStartTimeInOpenGame();
            settlersLobby.actionGameStarted(gameStarter.getStartOptions());
        }
    }

    private void setStartTimeInOpenGame()
    {
        try
        {
            settlersLobby.getOpenHostGame().setStartTime(System.currentTimeMillis());
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
    }


    @Override
    public void run()
    {
        try
        {
            observeTillMiniChat();
            initPlayerNumbersAndGoodsInStock();
            initWimo();
            sendOpenGame();
            observeMiniChat();
        }
        catch (InterruptedException e)
        {
            //abort
            Thread.currentThread().interrupt();
        }
        finally
        {
            gameStarter.setRunning(false);
        }
    }

    protected synchronized int getMaximumPlayerNumber()
    {
        return maxPlayerNumber;
    }

    protected synchronized boolean isHostAway()
    {
        return hostWasAway;
    }

    protected void initPlayerNumbersAndGoodsInStock()
    {
        initPlayerNumbers();
        // wenn initPlayerNumbers zurückkehrt, kann hoffentlich
        // auch der Warenbestand erkannt werden.
        // falls nicht wird er später ermittelt.
        try
        {
            //Warenbestand erkennen dauert lange, zuerst
            //nach Amazonen schauen
            isAmaz = amazonen.match();
            goodsInStock = getGoodsInStock();
        }
        catch (Exception e)
        {
        }
    }
    
    protected void initWimo()
    {
    	try
        {
    		isWimo = gameStarter.getStartOptions().isWiMo();
        }
    	catch (Exception e)
        {
        }
    }

    private GoodsInStock getGoodsInStock()
    {
        if (!isMiniChatVisible())
        {
            //hier wird keine Exception geworfen (im Gegensatz zu z.B.
            //getCurrentPlayerNumber(), da sich der Warenbestand eher selten
            //ändern, daher muss allein wegen ihm nicht zwingend nochmal
            //aktualisiert werden
            return null;
        }
        if (lastGoodsInStockMatchIndex >= 0 &&
                imageCheckerGoods[lastGoodsInStockMatchIndex].imageChecker.match())
        {
            return imageCheckerGoods[lastGoodsInStockMatchIndex].goodsInStock;
        }

        ImageChecker imageChecker = multiCheckerGoods.matchFirst();
        if (imageChecker != null)
        {
            lastGoodsInStockMatchIndex = multiCheckerGoods.getIndex(imageChecker);
            return imageCheckerGoods[lastGoodsInStockMatchIndex].goodsInStock;
        }
        return null;
    }

    private void checkNamesInChat()
    {
        if (!isMiniChatVisible())
        {
            return;
        }
        List<String> possibleNamesInChat = settlersLobby.getGameState().getNameList();
        namesInChatChecker = new MultiImageChecker();
        for (String text : possibleNamesInChat)
        {
            ImageChecker imageChecker = new ImageChecker(
                        robot, 26, 105, 115, 407, text, false);
            namesInChatChecker.add(imageChecker);
        }
        namesInChatChecker.matchAll();
        for (ImageChecker imageChecker : namesInChatChecker.getImageCheckers())
        {
            String text = imageChecker.getTextInImage();
            boolean isInMiniChat = imageChecker.getMatchCenter() != null;
            settlersLobby.getGameState().setIsInMiniChat(text, isInMiniChat);
        }
    }

    protected synchronized void setHostAway(boolean hostAway)
    {
        this.hostWasAway = hostAway;
    }

}
