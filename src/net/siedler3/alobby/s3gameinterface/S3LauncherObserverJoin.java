package net.siedler3.alobby.s3gameinterface;

import java.io.File;

import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.util.GameDataFile;

/**
 * Observer-object for s3-games which are launched as player with s3launcher.
 * 
 * @author Zwirni
 */
public class S3LauncherObserverJoin extends S3LauncherObserver {

	// Marker of user-info has been send
	boolean userInfoSend = false;
	
	protected S3LauncherObserverJoin(SettlersLobby settlersLobby, GameStarterLauncher gameStarter) {
		super(settlersLobby, gameStarter);
	}

	/**
	 * Check actual observing-state depending on available values
	 * to start necessary commands to interact with aLobby.
	 */
	@Override
	void checkState() {
		Test.output("isInMiniChat? " + isInMiniChat);
		Test.output("userInfoSend? " + userInfoSend);
		Test.output("hasLeftGame? " + hasLeftGame);
		Test.output("gameHasStarted? " + gameHasStarted);
		Test.output("openJoinGame? " + settlersLobby.getOpenJoinGame());
		if( false != isInMiniChat && false == userInfoSend ) {
			// send no info about joined game, its handled by host
			userInfoSend = true;			
		}
		// if user left the game
		// -> only reset user-specific settings
		// -> game-settings are handled by host
		if( false != hasLeftGame && settlersLobby.getOpenJoinGame() != null ) {
			gameStarter.setRunning(false);
			settlersLobby.actionCreateGameAborted();
			settlersLobby.setAsOpenJoinGame(null);
			settlersLobby.getAwayStatus().onGameClosed();
			settlersLobby.unselectGameInGui();
		}
		// if game has been started
		if( false != gameHasStarted ) {
			settlersLobby.getOpenJoinGame().setStartTime(System.currentTimeMillis());
			settlersLobby.actionGameStarted(gameStarter.getStartOptions());
		}
		// upload taken statistic screenshot
		if( !"".equals(statisticScreenshotPath) && settlersLobby.getOpenJoinGame() != null ) {
			uploadStatisticScreenshot(statisticScreenshotPath);
		}
	}
	
	/**
	 * Upload statistic Screenshot incl. game-data and set link in chat after upload.
	 * 
	 * @param statisticScreenshotPath	the absolute path to the screenshot
	 */
	private void uploadStatisticScreenshot(String statisticScreenshotPath) {
		if( settlersLobby.getOpenJoinGame() != null ) {
			this.playercount = settlersLobby.getOpenJoinGame().getCurrentPlayer();
			if( settlersLobby.getOpenJoinGame().isLeagueGame() ) {
				this.league = 1;
			}
			this.tournamentid = settlersLobby.getOpenJoinGame().getTournamentId();
			this.round = settlersLobby.getOpenJoinGame().getTournamentRound();
			this.matchnumber = settlersLobby.getOpenJoinGame().getTournamentMatchnumber();
			this.groupnumber = settlersLobby.getOpenJoinGame().getTournamentGroupnumber();
			this.wimo = settlersLobby.getOpenJoinGame().isWimo();
			this.mapname = settlersLobby.getOpenJoinGame().getPlainMap();
			this.goods = settlersLobby.getOpenJoinGame().getGoods();
			
			// load the game-data file
			this.gameDataFile = new GameDataFile(settlersLobby, settlersLobby.getOpenJoinGame());
			this.racelist = gameDataFile.get("usersWithRaces");
			this.startTime = gameDataFile.get("creationTime");
			
			// upload screenshot if it is activated in settings
			if( Configuration.getInstance().isActivateS3StatsScreenCapture() ) {
				performStatisticScreenshotUpload(new File(statisticScreenshotPath));
			}
		}
	}
}