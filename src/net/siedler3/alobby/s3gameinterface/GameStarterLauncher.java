package net.siedler3.alobby.s3gameinterface;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.util.KeyBlockStarter;

/**
 * Start the s3ce.exe with given game-parameters.
 * 
 * @author Zwirni
 */
public class GameStarterLauncher implements GameStarter {

	private SettlersLobby settlersLobby;
	public boolean isRunning;
	private Thread starterThread;
	protected Thread ipcThread;
	private StartOptions startOptions;
	private String hostUserName;
	public static S3Starter s3Starter;

	/**
	 * Constructor
	 * 
	 * @param settlersLobby
	 */
	public GameStarterLauncher(SettlersLobby settlersLobby) {
		this.settlersLobby = settlersLobby;
	}

	/**
	 * Join a game of user hostUsername with given settings.
	 * 
	 * @param startOptions
	 * @param hostUserName
	 * @param mapName
	 * @param goods
	 * @param league
	 * @param tournamentid
	 * @param tournamentname
	 */
	@Override
	public void joinGame(StartOptions startOptions, String hostUserName, String mapName, int goods, boolean league,	int tournamentid, String tournamentname) {
		this.hostUserName = hostUserName;
		if (starterThread != null && starterThread.isAlive())
        {
			// Thread is already running
            return;
        }
        if (isRunning())
        {
            return;
        }

        this.startOptions = startOptions;
        GameStarterLauncher obj = this;
        Runnable runnable = new Runnable(){
            @Override
            public void run()
            {
            	try
                {
                    // Start Launcher
                	s3Starter = new S3Starter(settlersLobby, startOptions, false);
                    s3Starter.start();
                    
                    // start observer for this non-host-game
                    S3LauncherObserverJoin joinObserver = new S3LauncherObserverJoin(settlersLobby, obj);
                    joinObserver.start();
                    
                    // set marker in GameStarter for running s3 to true
                    setRunning(true);
                }
                catch (Exception e)
                {
                    Test.outputException(e);
                    setRunning(false);
                }
            }
        };
        starterThread = new Thread(runnable);
        starterThread.setName(starterThread.getName() + "-JoinGame"); //$NON-NLS-1$
        starterThread.start();
	}

	/**
	 * Create a game as host and start s3 with given game-settings.
	 * 
	 * @param StartOptions startOptions
	 */
	@Override
	public void createGame(StartOptions startOptions) {
		if (starterThread != null && starterThread.isAlive())
	    {
	        // Thread is already running
	        return;
	    }
		
		if (isRunning())
        {
            return;
        }
		
		this.startOptions = startOptions;
		GameStarterLauncher obj = this;
		Runnable runnable = new Runnable(){
			@Override
            public void run()
            {
                try
                {
                	// set marker in GameStarter that s3 is Running to true
                	setRunning(true);
                	
                    // Start Launcher
                	s3Starter = new S3Starter(settlersLobby, startOptions, true);
                    s3Starter.start();
                    
                    // start observer for this Host-game
                    S3LauncherObserverHost hostObserver = new S3LauncherObserverHost(settlersLobby, obj);
                    hostObserver.start();
                }
                catch (Exception e)
                {
                    Test.outputException(e);
                    setRunning(false);
                }
            }
        };
        starterThread = new Thread(runnable);
        starterThread.setName(starterThread.getName() + "-CreateGame"); //$NON-NLS-1$
        starterThread.start();
	}
	
	/**
	 * Set a marker that S3 is running or not.
	 * 
	 * @param boolean	true if S3 is running
	 */
	public synchronized void setRunning(boolean isRunning)
    {
        this.isRunning = isRunning;
    }

	/**
	 * If game has been stopped, cancel starterThread and set S3 as not running.
	 * 
	 * @return void
	 */
	@Override
	public void stopGame() throws Exception {
		stopStarterThread();
		setRunning(false);
	}
	
	/**
	 * Cancels the {@code #starterThread}.
	 * 
	 * @return void
	 */
	private void stopStarterThread()
	{
        for( int i = 0; i < 10 && starterThread != null && starterThread.isAlive(); i++)
        {
            Test.output("stop starterThread " + i); //$NON-NLS-1$
            starterThread.interrupt();
            try
            {
                starterThread.join(10);
            }
            catch (InterruptedException e)
            {
                Thread.currentThread().interrupt();
            }
            //Wenn der Starterthread selber derjenige war, der den Abbruch
            //ausgelöst hat, brauchen wir nicht weiter versuchen uns selber
            //zu unterbrechen. Dies kann neuerdings passieren, wenn die
            //s3.exe nicht gestartet werden kann
            if (Thread.currentThread() == starterThread)
            {
                Test.output("stop selfabort"); //$NON-NLS-1$
                break;
            }
        }
	}

	/**
	 * Returns max player count for this game. 
	 */
	@Override
	public int getMaximumPlayerNumber() {
		if( isRunning() ) {
			return s3Starter.getStartOptions().getMaxPlayerNumber();
		}
		return 0;
	}

	/**
	 * Returns marker if S3 is running or not.
	 * 
	 * @return boolean	true if S3 is running
	 */
	@Override
	public boolean isRunning() {
		if( s3Starter != null ) {
			return s3Starter.isS3Running();
		}
		return false;
	}

	/**
	 * Return if S3 is ready to run (true) or not (false).
	 * 
	 * @return boolean	true if s3 is ready to run
	 */
	@Override
	public boolean isReady() {
		return !isRunning();
	}

	/**
	 * Only for compatibility with vanilla-way to start S3.
	 * Returns false on every call.
	 */
	@Override
	public boolean isHostAway() {
		return false;
	}

	/**
	 * Return the s3Starter-object.
	 * 
	 * @return S3Starter	the object for s3Starter
	 */
	@Override
	public S3Starter getS3Starter() {
		if (isS3Running())
        {
            return s3Starter;
        }
        return null;
	}

	/**
	 * Only for compatibility with vanilla-way to start S3.
	 * Returns null on every call.
	 */
	@Override
	public KeyBlockStarter getKeyBlockStarter() {
		return null;
	}
	
	/**
	 * Return s3Startoptions
	 * 
	 * @return StartOptions
	 */
	public StartOptions getStartOptions() {
		return this.startOptions;
	}
	
	/**
	 * Return the username of the host.
	 * 
	 * @return
	 */
	public String getHostUsername() {
		return this.hostUserName;
	}

	@Override
	public boolean isS3Running() {
		return isRunning();
	}
	
}
