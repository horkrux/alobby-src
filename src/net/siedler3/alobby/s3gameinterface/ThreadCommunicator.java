/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.AWTException;
import java.awt.Robot;

import net.siedler3.alobby.controlcenter.SettlersLobby;


/**
 * Singleton, damit HostObserver-Thread auch den Robot benutzen kann.
 * 
 * @author Stephan Bauer (maximilius)
 * @version 0.1
 */
public class ThreadCommunicator
{
	private static ThreadCommunicator instance;
	private Robot robot;
	private SettlersLobby actionListener;
	private int currentEdition;

	/**
	 * Konstruktor.
	 * 
	 * @throws AWTException
	 *             Wenn Fehler beim Initialisieren des Robotters.
	 */
	private ThreadCommunicator() throws AWTException
	{
		robot = new Robot();
	}

	/**
	 * Zum Erhalt der Instanz des Singleton.
	 * 
	 * @return RobotContainer Die Instanz des Singletons.
	 * @throws AWTException
	 *             Wenn Fehler beim Initialisieren des Robotters.
	 */
	public static ThreadCommunicator getInstance() throws AWTException
	{
		if (instance == null)
		{
			instance = new ThreadCommunicator();
		}
		return instance;

	}

	/**
	 * Liefert den Robotor zurück.
	 * 
	 * @return Robot Der Robotter.
	 */
	public Robot getRobot()
	{
		return robot;

	}

	/**
	 * Setzt den actionListener.
	 * 
	 * @param actionListener
	 *            Der zu setzende actionListener.
	 */
	public void setActionListener(SettlersLobby actionListener)
	{
		this.actionListener = actionListener;
	}

	/**
	 * Liefert den actionListener zurück.
	 * 
	 * @return SettlersLobby Der actionListener.
	 */
	public SettlersLobby getActionListener()
	{
		return actionListener;
	}

	/**
	 * Liefert die aktuelle Spieleversion zurück.
	 * 
	 * @return int Die aktuelle Spieleversion;
	 */
	public int getCurrentEdition()
	{
		return currentEdition;
	}

	/**
	 * Setzt die aktuelle Spieleversion.
	 * 
	 * @param currentEdition
	 *            Die aktuelle Spieleversion.
	 */
	public void setCurrentEdition(int currentEdition)
	{
		this.currentEdition = currentEdition;
	}
}
