/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.util.TimerTaskCompatibility;


public class getRaceTask extends TimerTaskCompatibility
{
    private static final String path = ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/";
    private static Robot robot;
    private static BufferedImage imgBuildingMenu = null;
    private static BufferedImage imgBuildingMenuSelected = null;
    private static BufferedImage romanBuilding = null;
    private static BufferedImage asiaBuilding = null;
    private static BufferedImage egyBuilding = null;
    private static BufferedImage amazBuilding = null;

    private static final Dimension supportedScreenSize = new Dimension(1024, 768);

    private static boolean isInitialised = false;


    static
    {
        try
        {
            robot = ThreadCommunicator.getInstance().getRobot();

            // building-menu-icon
            imgBuildingMenu = ImageIO.read(
                        getRaceTask.class.getResourceAsStream(
                            path + "Baumenu.bmp"));
            // selected building-menu-icon
            imgBuildingMenuSelected = ImageIO.read(
                    getRaceTask.class.getResourceAsStream(
                        path + "Baumenu_selected.bmp"));
            // roman building
            romanBuilding = ImageIO.read(
                    getRaceTask.class.getResourceAsStream(
                        path + "roemer.bmp"));
            
            // asia building
            asiaBuilding = ImageIO.read(
                    getRaceTask.class.getResourceAsStream(
                        path + "asia.bmp"));
            
            // egy building
            egyBuilding = ImageIO.read(
                    getRaceTask.class.getResourceAsStream(
                        path + "egy.bmp"));
            
            // amaz building
            amazBuilding = ImageIO.read(
                    getRaceTask.class.getResourceAsStream(
                        path + "amaz.bmp"));

            isInitialised  = true;
        }
        catch (Exception e)
        {
            isInitialised  = false;
            Test.outputException(e);
        }
    }

    private ImageChecker[] imageCheckerRaces = null;
    private MultiImageChecker imageCheckerBuildingMenu = null;
    private S3InGameCheck s3InGame = null;
    private boolean initialised = false;

    private SettlersLobby settlersLobby;

    /**
     * Task, which checks the choosen race after game-start
     * and save the value to the game-settings for league/tournament.
     */
    public getRaceTask(SettlersLobby settlersLobby)
    {
        this.settlersLobby = settlersLobby;
        if (isInitialised)
        {
            try
            {
                ImageChecker tmpImageChecker;

                imageCheckerBuildingMenu = new MultiImageChecker();
                tmpImageChecker = new ImageChecker(robot, 18, 240,
                		imgBuildingMenu.getWidth(), imgBuildingMenu.getHeight(), imgBuildingMenu);
                imageCheckerBuildingMenu.add(tmpImageChecker);

                tmpImageChecker = new ImageChecker(robot, 18, 240,
                		imgBuildingMenuSelected.getWidth(), imgBuildingMenuSelected.getHeight(), imgBuildingMenuSelected);
                imageCheckerBuildingMenu.add(tmpImageChecker);

                int pixelPictureX = 40;
                int pixelPictureY = 350;
                imageCheckerRaces = new ImageChecker[4];
                imageCheckerRaces[0] = new ImageChecker(robot, pixelPictureX, pixelPictureY,
                		romanBuilding.getWidth(), romanBuilding.getHeight(), romanBuilding);
                imageCheckerRaces[1] = new ImageChecker(robot, pixelPictureX, pixelPictureY,
                		asiaBuilding.getWidth(), asiaBuilding.getHeight(), asiaBuilding);
                imageCheckerRaces[2] = new ImageChecker(robot, pixelPictureX, pixelPictureY,
                		egyBuilding.getWidth(), egyBuilding.getHeight(), egyBuilding);
                imageCheckerRaces[3] = new ImageChecker(robot, pixelPictureX, pixelPictureY,
                		amazBuilding.getWidth(), amazBuilding.getHeight(), amazBuilding);
                
                s3InGame = new S3InGameCheck();
                initialised = true; //alle Vorbereitungen ok, der Task ist einsatzbereit
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }
        }
    }

    @Override
    public void run()
    {
        if (!initialised)
        {
            Test.output("Task nicht initialisiert");
            cancel();
            return;
        }
        try
        {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            if (!screenSize.equals(supportedScreenSize))
            {
                //die aktuelle Auflösung entspricht nicht der unterstützten,
                //der Task wird beendet, aber nicht gecancelt
                return;
            }
        }
        catch (Exception e)
        {
            Test.outputException(e);
            //unerwartete Exception, beenden
            return;
        }

        //Anfangserkennung nur mit einzelnen Pixeln, da die
        //Suche nach einem kompletten Image eventuell eher dazu neigt
        //Structured Exceptions von S3 beim Starten zu erzeugen
        //Die Erkennung eines einzelnen Pixels ist scheinbar weniger störend
        if (s3InGame.isInGame())
        {
            //sobald S3 im Spiel angekommen ist,
            //wird der Task beendet.
            //Anschliessend wird in einem extra Thread das Klicken erledigt,
            //damit der TimerThread nicht belastet wird.
            Test.output("match S3");
            cancel();
            SettlersLobby.executor.execute(new getRace());
        }
    }

    /**
     * runnable, das die entsprechenden Mausklicks zum Stoppen
     * der Einlagerung ausführt.
     *
     * @author jim
     *
     */
    private class getRace implements Runnable
    {
        @Override
        public void run()
        {
            try
            {

                //für den Fall, daß aus Versehen die S3 Erkennung
                //fehlerhaft war, da der user die richtige Auflösung
                //benutzt und an der überprüften Stelle die S3-entsprechende
                //Farbe stand, wird hier solange gewartet, bis der
                //Waren-Button sichtbar ist. Normalerweise ist das
                //schon in der ersten Iteration der Fall.
                //Für zuvor beschriebenen Ausnahmefall wird aber recht lange
                //versucht, den Button zu finden, bevor aufgegeben wird.
                ImageChecker buildingMenuMatch = imageCheckerBuildingMenu.waitUntilMatchFirst(1000, 50);
                if (buildingMenuMatch == null)
                {
                    Test.output("Fehler");
                    return;
                }
                //aktuelle Mausposition merken
                Point currentPosition = GameStarterVanilla.getCurrentMousePosition();

                GameStarterVanilla.mouseClick(robot, buildingMenuMatch.getMatchCenter());
                GameStarterVanilla.mouseClick(robot, 30, 310); // click on first building-submenu
                
                // small pause
                Thread.sleep(80);
                
                // check which race-building is visible
                String race = "";
                if( imageCheckerRaces[0].match() ) {
                	race = ALobbyConstants.Races.ROMANS.getValue();
                }
                if( imageCheckerRaces[1].match() ) {
                	race = ALobbyConstants.Races.ASIAN.getValue();
                }
                if( imageCheckerRaces[2].match() ) {
                	race = ALobbyConstants.Races.EGYS.getValue();
                }
                if( imageCheckerRaces[3].match() ) {
                	race = ALobbyConstants.Races.AMAZ.getValue();
                }
               	settlersLobby.getIRCCommunicator().sendProgramMessage(settlersLobby.getProgrammMessageProtocol().createMsgSendRaceToPlayersInGame(settlersLobby.getGameState().getHostUserName().getNick(), settlersLobby.getNick(), race ));

                if (currentPosition != null)
                {
                    // back to first position and click to disable the buildingmenu again
                    robot.mouseMove(currentPosition.x, currentPosition.y);
                    GameStarterVanilla.mouseClick(robot, currentPosition.x, currentPosition.y);
                }
            }
            catch (InterruptedException e)
            {
                Thread.currentThread().interrupt();
                Test.outputException(e);
            }
        }
    }

}
