/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;
import java.awt.Color;
/**
 * Kapselt Farbe und Bildschirmposition eines Pixels.
 * @author Stephan Bauer (maximilius)
 * @version 0.1
 */
public class XYColor
{
	private int x, y;
	private Color color;

	/**
	 * Konstruktor.
	 * 
	 * @param r
	 *            Rot.
	 * @param g
	 *            Grün.
	 * @param b
	 *            Blau.
	 * @param x
	 *            X-Position.
	 * @param y
	 *            Y-Position.
	 */
	public XYColor(int r, int g, int b, int x, int y)
	{
		color = new Color(r, g, b);
		this.x = x;
		this.y = y;
	}

	/**
	 * Getter.
	 * 
	 * @return int X-Position.
	 */
	public int getX()
	{
		return x;
	}

	/**
	 * Getter.
	 * 
	 * @return int Y-Position.
	 */
	public int getY()
	{
		return y;
	}

	/**
	 * Getter.
	 * 
	 * @return Color Die Farbe.
	 */
	public Color getColor()
	{
		return color;
	}

}
