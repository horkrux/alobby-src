/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.event.KeyEvent;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.gui.additional.IngameMessage;
import net.siedler3.alobby.nativesupport.NativeFunctions;

public class AutoSaveTaskStateByKey implements Runnable
{
	private boolean isRunning = true;

    private SettlersLobby settlersLobby;

    public AutoSaveTaskStateByKey(SettlersLobby settlersLobby)
    {
    	this.settlersLobby = settlersLobby;
    }
    
    public void startMonitoring()
    {
        SettlersLobby.executor.execute(this);
    }

    public void stopMonitoring()
    {
    	setRunning(false);
    }

    @Override
    public void run()
    {
    	// only start after 10 seconds waiting
    	try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	
    	IngameMessage.stopShowAnyAutoSaveStateMessage();
    	
        //call this once to delete any result between the last call to isPressed and upcoming ones.
        //if someone presses space while chatting between to savepoints, this might be visible
        //in the first call to isPressed (LSB set to one)
    	NativeFunctions.getInstance().isPressed(KeyEvent.VK_CONTROL);
		NativeFunctions.getInstance().isPressed(KeyEvent.VK_S);
		
        boolean ctrlPressed = false;
        boolean sPressed = false;
        
    	while (isRunning())
    	{
    		ctrlPressed = NativeFunctions.getInstance().isPressed(KeyEvent.VK_CONTROL);
    		sPressed = NativeFunctions.getInstance().isPressed(KeyEvent.VK_S);
    		if( false != ctrlPressed && false != sPressed ) {
    			stopMonitoring();
    			settlersLobby.setAutoSave(false == settlersLobby.isAutoSave() ? true : false);
                // change state
    			settlersLobby.updateAutoSaveState();
    			// show the hint for actual autosave-state
                IngameMessage.startShowAutoSaveStateMessage(settlersLobby.isAutoSave());
    		}
    		
    		// only run every seconds to check if keys are pressed, not between
    		try {
    			Thread.sleep(1000);
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    }

    private synchronized boolean isRunning()
    {
        return isRunning;
    }

    private synchronized void setRunning(boolean isRunning)
    {
        this.isRunning = isRunning;
    }
}
