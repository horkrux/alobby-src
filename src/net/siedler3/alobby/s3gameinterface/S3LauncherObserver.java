package net.siedler3.alobby.s3gameinterface;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.controlcenter.state.UserInGame;
import net.siedler3.alobby.controlcenter.state.UserInGameList;
import net.siedler3.alobby.gui.additional.IngameMessage;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.s3gameinterface.IpcClient.StatsScreenAvailableListener;
import net.siedler3.alobby.s3gameinterface.IpcClient.PreGameLobbyUpdateEvent.Player;
import net.siedler3.alobby.util.GameDataFile;
import net.siedler3.alobby.util.httpRequests;

/**
 * Observer-object for s3-games which are launched with s3launcher.
 * 
 * @author Zwirni
 *
 */
public abstract class S3LauncherObserver extends Thread {
	
	protected GameStarterLauncher gameStarter;
	
	// Marker if user has reached minichat
	protected boolean isInMiniChat = false;
	
	// max count of players in this game
	protected int maxPlayerNumber = 0;

	// the map name of the hosted map
	protected String mapName = "";

	// the path to the hosted map (or the hosted save)
	protected String mapFileName = "";

	// whether the game is a save game
	protected boolean isSaveGame = false;
	
	// SettlersLobby-object
	protected SettlersLobby settlersLobby;

	// Marker if user has left the game
	protected boolean hasLeftGame = false;

	// Marker if game has been started
	protected boolean gameHasStarted;

	// Marker if Amazon is allowed
	protected boolean isAmazAllowed = false;

	// actual count of players in this game
	protected int playerNumber = 0;

	// Marker if this is an economy-game
	protected boolean isEconomyMode;

	// list of players in game as comma-separated string
	protected String playerList = "";

	// list of players which have been removed as array
	protected List<String> removedPlayer = new ArrayList<>();

	// list of players in game
	protected UserInGameList userInGameList;

	// list of active game modes
	protected long gameModeMask;

	// path to taken statistic-screenshot
	protected String statisticScreenshotPath = "";
	
	// game-data for screenshot-upload
	protected int playercount = 0;
	protected int league = 0;
	protected int tournamentid = 0;
	protected int round = 0;
	protected int matchnumber = 0;
	protected int groupnumber = 0;
	protected boolean wimo = false;
	protected GameDataFile gameDataFile;
	protected String racelist = "";
	protected String startTime = "";
	protected String positionTeamList = "";
	protected boolean won = false;
	protected boolean endOfGame = false;
	protected String mapname = "";
	protected int goods = 0;
	
	// Marker if upload has been done
	private boolean uploaded = false;


	/**
	 * Create Observer-object with some game-data.
	 * 
	 * @param gameStarter
	 * @param s3Starter
	 */
	protected S3LauncherObserver(SettlersLobby settlersLobby, GameStarterLauncher gameStarter) {
		this.settlersLobby = settlersLobby;
		this.gameStarter = gameStarter;
	}
	
	/**
	 * Run this to start ipcClient and add necessary listener.  
	 */
	public void run() {
		if( false != gameStarter.getS3Starter().getRunning() && !"".equals(gameStarter.getS3Starter().getPipeName()) ) {
			Test.output("start ipcclient");
	        // start the ipc-client
	        IpcClient ipcClient = new IpcClient(gameStarter.getS3Starter().getPipeName());
	        
	        // add listener for game-data
			ipcClient.addListener((IpcClient.PreGameLobbyListener) data -> {
				Test.output("IPC: Received pre-lobby info with game-data");
				// get max playernumber for this game
				maxPlayerNumber = data.numPlayers.get();
				// get the map name for this game
				mapName = data.mapName.get();
				// get the file name of the map used for this game
				mapFileName = data.mapFileName.get();
				// whether the game is a save game
				isSaveGame = data.isSaveGame.get();
				// get marker if Amaz is allowed or not
				isAmazAllowed = data.isAmazonAllowed.get();
				// get marker if economy mode is active or not
				isEconomyMode = data.isEconomyMode.get();
				// get list of players in the game
				Player[] players = data.players;
				playerNumber = 0;
				playerList = "";
				removedPlayer = new ArrayList<>();
				userInGameList = new UserInGameList();
				// loop through the list of players
				for (int i = 0; i < players.length; i++) {
		            Player x = players[i];
		            Test.output("spieler " + i + ": " + x.name + " -> " + x.slotStatus);
		            // if player-name is set ..
		            if( !"".equals(x.name.toString()) ) {
		            	// .. and slotStatus is 1, than add its name to the playerlist
		            	if( x.slotStatus.get() == 1 ) {
			            	// count player
			            	playerNumber++;
			            	// add playername to list
			            	if( i > 0 ) {
			            		playerList = playerList + ",";		            		
			            	}
			            	playerList = playerList + x.name.toString();
			            	userInGameList.updateUser(new UserInGame(x.name.toString()), false);
		            	}
		            	// .. otherwise remove the nickname from playerlist 
		            	else {
		            		removedPlayer.add(x.name.toString());
		            	}
		            }
		            // if slotStatus = 2 than the slot is reserved for AI
		            if( x.slotStatus.get() == 2 ) {
	            		// reduce maxplayer-count
		            	maxPlayerNumber = maxPlayerNumber - 1;
	            	}
		            // if slotStatus = 4 than the slot is set to "locked"
		            if( x.slotStatus.get() == 4 ) {
		            	// reduce maxplayer-count
		            	maxPlayerNumber = maxPlayerNumber - 1;
	            	}
		        }

				// Update the list of active game modes
				gameModeMask = data.gameModes.get();

				// check if something on the game data has been changed and should be noticed
				checkState();
			});

			// add listener for ingame dialog-changes
			ipcClient.addListener((IpcClient.DialogChangedListener) (oldDialogId, newDialogId) -> {
				Test.output("dialogchanges: " + oldDialogId + " -> " + newDialogId);
				if (newDialogId == IpcClient.LOAD_MULTIPLAYER_GAME_DIALOG_ID) {
					// Game just launched, user is seeing the golden loading screen
					Test.output("IPC: Game just launched");
					gameHasStarted = true;
				} else if (newDialogId == IpcClient.PRE_GAME_LOBBY_DIALOG_ID) {
					// Game is seeing the pre-game lobby (it has not loaded fully yet)
					Test.output("IPC: User is seeing the pre-game lobby");
					isInMiniChat = true;
				} else if (oldDialogId == IpcClient.PRE_GAME_LOBBY_DIALOG_ID) {
					// User left the pre-game lobby by going back
					Test.output("IPC: User left the pre-game");
					hasLeftGame  = true;
				} else if (oldDialogId == IpcClient.LOAD_MULTIPLAYER_GAME_DIALOG_ID) {
					// User left the game (
					// => do nothing as it is handled by DisconnectListener
					Test.output("IPC: User left the game");
				}
				
				// check if something on the game data has been changed and should be noticed
				checkState();
			});
			
			// add listener for new screenshot
			ipcClient.addListener((StatsScreenAvailableListener) (screenPath) -> {
				Test.output("screenshot taken: " + screenPath);
	            statisticScreenshotPath = screenPath;
	            checkState();
	        });
			
			// handle IPC-disconnects (aka s3 is shut down) 
			ipcClient.addListener((IpcClient.DisconnectListener) () -> {
				Test.output("s3 shut down event");
				gameStarter.getS3Starter().setRunning(false);
				hasLeftGame  = true;
				checkState();
			});
			
			try {
				Test.output("IPC: Connecting...");
				ipcClient.connect();
			} catch (IOException e) {
				Test.outputException(e);
			}
		}
	};
	
	/**
     * Uploads the screenshot with all game-settings.
     * 
     * @param screenShotFile	the screenshot-file to be uploaded
	 * @throws IOException 
     */
    protected void performStatisticScreenshotUpload(File screenShotFile)
    {
		// prevent double uploads
    	if( !uploaded  ) {
    	
    		// preparation for league-screenshots
			// -> set to true if image should not be deleted through screen-Management
			boolean preventDeletion = false;
			if( this.league > 0 ) {
        		preventDeletion = true;
        	}
			
			// do not prevent the deletion of this screenshot if only 1 users played this game
			if( this.playercount == 1 ) {
				preventDeletion = false;
			}
			
			Test.output("prepare httprequest");
    		
	    	//IngameMessage.stopShowS3StatsUploadQuestionMessage();
			// show hint that image was taken and upload is in progress
			IngameMessage.startShowS3StatsMessage();
			httpRequests httpRequests = new httpRequests();
			// disable PHP-Session-security if it is not a league-game
			if( this.league == 0 ) {
				httpRequests.setIgnorePhpSessId(true);
			}
			httpRequests.setUrl(settlersLobby.config.getDefaultScreenUploader());
			httpRequests.addParameter("smalloutput", "1");
			if( preventDeletion ) {
    			httpRequests.addParameter("preventDeletion", 1);
    		}
    		else {
    			httpRequests.addParameter("preventDeletion", 0);
    		}
			// add mapname and goods as parameter
    		httpRequests.addParameter("mapname", this.mapname);
    		httpRequests.addParameter("goods", this.goods);
	    	// add nickname as parameter
	    	httpRequests.addParameter("nickname", settlersLobby.getNick());
	    	if( this.tournamentid > 0 ) {
	    		httpRequests.addParameter("liga", 0);
	    		httpRequests.addParameter("turnierid", this.tournamentid);
	    	}
	    	else {
	    		httpRequests.addParameter("liga", this.league);
	    		httpRequests.addParameter("turnierid", 0);
	    	}
	    	httpRequests.addParameter("round", this.round);
	    	httpRequests.addParameter("matchnumber", this.matchnumber);
	    	httpRequests.addParameter("groupnumber", this.groupnumber);
	    	// add the position-team-list
	    	httpRequests.addParameter("positionteamlist", positionTeamList);
	    	// add the race-list
	    	httpRequests.addParameter("racelist", this.racelist);
	    	// add the real-starttime of this game
	    	httpRequests.addParameter("starttime", this.startTime);
	    	// add game-result for actual user to request
	    	if( this.won ) {
	    		httpRequests.addParameter("result", 1);
	    	}
	    	else {
	    		httpRequests.addParameter("result", 0);
	    	}
	    	// add WiMo-Marker
	    	if( this.wimo ) {
	    		httpRequests.addParameter("wimo", 1);
	    	}
	    	else {
	    		httpRequests.addParameter("wimo", 0);
	    	}
	    	// marker, if game ends normal
	    	if( this.endOfGame ) {
	    		httpRequests.addParameter("endOfGame", 1);
	    	}
	    	else {
	    		httpRequests.addParameter("endOfGame", 0);
	    	}
	    	// add name of host
	    	httpRequests.addParameter("hostusername", settlersLobby.getNick());
	    	// add the screenshot-file itself as file
			httpRequests.addFile("file", screenShotFile);
			
			// perform request and get the result
			Test.output("start uploading screenshot");
			String result = "";
			try {
				result = httpRequests.doRequest().trim();
			} catch (IOException e) {
				Test.outputException(e);
			}
			Test.output("upload results in: " + result);

			String pictureurl = settlersLobby.config.getDefaultScreenUploaderDomain() + result;
			
			Test.output("post link for screenshot if " + playercount + " > 1 and " + pictureurl.matches(ALobbyConstants.REGEX_URL) + " is true");
						
			// if result is available and usable and more than 1 player played this game, than post it in chat
			if( playercount > 1 && result.length() > 0 && pictureurl.matches(ALobbyConstants.REGEX_URL) ) {
				this.settlersLobby.actionChatMessage(I18n.getString("IRCCommunicator.MSG_AUTO_STATISTICSCREEN") + " " + pictureurl);
			}
		
			this.uploaded = true;
			
    	}
		return;
    }
	
	/**
	 * Check actual observing-state depending on available values
	 * to start necessary commands to interact with aLobby.
	 */
	abstract void checkState();
}
