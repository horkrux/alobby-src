/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.nativesupport.NativeFunctions;

public class CheckForS3StatsPrintKey implements Runnable
{
	private boolean isRunning = true;
    private CheckForS3Stats checkForS3Stats;
    private SettlersLobby settlersLobby;
    private File screenShotFile;

    public CheckForS3StatsPrintKey(CheckForS3Stats checkForS3Stats, File screenShotFile, SettlersLobby settlersLobby)
    {
    	this.checkForS3Stats = checkForS3Stats;
    	this.screenShotFile = screenShotFile;
    	this.settlersLobby = settlersLobby;
    }

    public void startMonitoring()
    {
        SettlersLobby.executor.execute(this);
    }

    public void stopMonitoring()
    {
    	setRunning(false);
    	//IngameMessage.stopShowS3StatsUploadQuestionMessage();
    }

    @Override
    public void run()
    {
        // call this once to delete any result between the last call to isPressed and upcoming ones.
        // if someone presses space while chatting between to savepoints, this might be visible
        // in the first call to isPressed (LSB set to one)
    	
    	// since there is this terrible "bug": http://bugs.java.com/bugdatabase/view_bug.do?bug_id=4455060
    	// we cannot use KeyEvent.VK_PRINTKEY at this point
    	// the alternative "isReleased" is not available in NativeFunctions
        NativeFunctions.getInstance().isPressed(KeyEvent.VK_SPACE);
    	while( isRunning() )
    	{
    		if (!settlersLobby.IS_WINE && checkForS3Stats.getIndexOfResolution() != 1) {
	    		// stop monitoring if resolution is not 800x600
	            stopMonitoring();
	            Test.output("Monitoring stopped because of wrong resolution");
    		}
    		if (!checkForS3Stats.isStatsDisplayed()) {
	    		// stop monitoring if the stats are not displayed anymore
	            stopMonitoring();
	            Test.output("Monitoring stopped because the stats are not displayed anymore");
    		}
    		
    		if ( NativeFunctions.getInstance().isPressed(KeyEvent.VK_SPACE) )
    		{
    			// disable key-monitoring
    			Test.output("spacebar is pressed");
    		    setRunning(false);
    		    try {
    		    	checkForS3Stats.setUploadScreenshot(true);
        			// save the file in screenuploader
        			// if it is not disabled by the user
        			if (screenShotFile != null) {
        				Test.output("perform upload after spacebar was pressed");
        				checkForS3Stats.performUpload(screenShotFile);
        			}
				} catch (IOException e) {
					Test.outputException(e);
				}
    		}
    	}
    }

    private synchronized boolean isRunning()
    {
        return isRunning;
    }

    private synchronized void setRunning(boolean isRunning)
    {
        this.isRunning = isRunning;
    }
}
