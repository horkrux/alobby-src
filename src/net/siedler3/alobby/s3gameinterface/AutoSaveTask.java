/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.SaveGame;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.additional.IngameMessage;
import net.siedler3.alobby.util.TimerTaskCompatibility;
import net.siedler3.alobby.util.Sound;


/**
 * TimerTask der in S3 auf Spiel speichern klickt.
 * Wenn er mit Wiederholung geschedult wird, erhält man eine
 * Autosavefunktionalität.
 *
 * @author jim
 * @author Stephan Bauer (aka maximilius)
 *
 */
public class AutoSaveTask extends TimerTaskCompatibility implements LineListener
{
    private ImageChecker[] imageChecker = null;
    private String soundFile;
    private boolean isAborted = false;
    private AutoSaveAbortKey autoSaveAbortKey;
    private String savePath;
    private Sound Sound = null;
    private boolean showAutoSaveMessageIngame;
	private boolean disabledAutosaveSound = false;

    private static boolean initialised;

    private static final Dimension[] screenSizes = {
        new Dimension(640, 480),
        new Dimension(800, 600),
        new Dimension(1024, 768),
        new Dimension(1366, 768)
    };

    private static final Point[] clickPoints = {
        new Point(50, 225),
        new Point(50, 270),
        new Point(50, 350),
        new Point(50, 350)
    };

    private static final BufferedImage[] gameButtonImage =
        new BufferedImage[3];

    private static Robot robot;

    static
    {
        try
        {
            robot = ThreadCommunicator.getInstance().getRobot();
            //SpielButton ist der Button unten links mit dem Computer
            gameButtonImage[0] = ImageIO.read(AutoSaveTask.class.getResourceAsStream(
                    ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/SpielButton0.bmp")); //$NON-NLS-1$
            gameButtonImage[1] = ImageIO.read(AutoSaveTask.class.getResourceAsStream(
                    ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/SpielButton1.bmp")); //$NON-NLS-1$
            gameButtonImage[2] = ImageIO.read(AutoSaveTask.class.getResourceAsStream(
                    ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/SpielButton2.bmp")); //$NON-NLS-1$
            robot = ThreadCommunicator.getInstance().getRobot();

            initialised = true;
        }
        catch (Exception e)
        {
            initialised = false;
            Test.outputException(e);
        }
    }

    public AutoSaveTask(boolean disabledAutosaveSound, String soundFile, String gamePath, boolean showAutoSaveMessageIngame)
    {
    	this.savePath = SaveGame.getSavegameDirFromGamePath(gamePath);
        this.soundFile = soundFile;
        // if no soundfile is available get the default-soundfile
        if( this.soundFile.length() == 0 )
        {
        	this.soundFile = ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/Morsecode5-mono-22000.wav";
        }
        this.disabledAutosaveSound  = disabledAutosaveSound;
        this.showAutoSaveMessageIngame = showAutoSaveMessageIngame;

        try
        {
            imageChecker = new ImageChecker[4];
            //Die Vergleichskoordinaten sind absolut fix
            imageChecker[0] = new ImageChecker(robot, 14, 447,
                    gameButtonImage[0].getWidth(), gameButtonImage[0].getHeight(), gameButtonImage[0]);
            imageChecker[1] = new ImageChecker(robot, 17, 564,
                    gameButtonImage[1].getWidth(), gameButtonImage[1].getHeight(), gameButtonImage[1]);
            imageChecker[2] = new ImageChecker(robot, 19, 719,
                    gameButtonImage[2].getWidth(), gameButtonImage[2].getHeight(), gameButtonImage[2]);
            imageChecker[3] = new ImageChecker(robot, 19, 719,
                    gameButtonImage[2].getWidth(), gameButtonImage[2].getHeight(), gameButtonImage[2]);
        }
        catch (Exception e)
        {
            Test.outputException(e);
        }
        
        // Sound-Class added
        Sound = new Sound();
        
    }

    /**
     * gibt den Index der Bildschirmauflösung zurück, anhand derer das passende
     * Vergleichsbild ausgewählt wird.
     * @return
     */
    private int getIndexOfResolution()
    {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        for (int i = screenSizes.length - 1; i >= 0; i--)
        {
            Dimension refScreenSize = screenSizes[i];
            if (screenSize.equals(refScreenSize))
            {
                return i;
            }
        }
        //keine der S3-Auflösungen, default ist 1024x768
        return screenSizes.length - 1;
    }

    @Override
    public void run()
    {
    	//wenn Fehler während der Initialisierung aufgetreten sind,
    	//muss hier abgebrochen werden, Autosave ist nicht möglich
        if (!initialised)
        {
            Test.output("AutoSaveTask not initialised"); //$NON-NLS-1$
            cancel();
            return;
        }

    	long lastSaveInterval = getLastSaveInterval();
    	final int minimumInterval = 64 * 1000;
        //skip saving if less than an ingame minute
    	if (lastSaveInterval != -1 && lastSaveInterval < minimumInterval)
    	{
            Test.output("Skipping autosave");
    		Test.output("lastSaveInterval: " + Long.toString(lastSaveInterval) + " < " + minimumInterval); //$NON-NLS-1$ $NON-NLS-2$
    		return;
    	}

    	isAborted = false;
        int index = getIndexOfResolution();
        if (imageChecker == null || imageChecker[index] == null)
        {
            Test.output("Autosave cancel"); //$NON-NLS-1$
            cancel();
            return;
        }

        if (imageChecker[index].match())
        {
            Test.output("Autosave..."); //$NON-NLS-1$
            try
            {
            	// start monitoring of ingame-actions if user would like to prevent autosave
            	autoSaveAbortKey = new AutoSaveAbortKey(this);
                autoSaveAbortKey.startMonitoring();
              	// call function to play the sound
                // if if is not disabled
                if( false == this.disabledAutosaveSound ) {
	               	Sound.addFileAsString(this.soundFile);
	               	Sound.addListener(this);
	                Sound.play();
                }
            	
                if (showAutoSaveMessageIngame)
                {
                    IngameMessage.startShowAutoSaveMessage();
                }
            }
            catch (Exception e)
            {
                Test.outputException(e);
                if (autoSaveAbortKey != null)
                {
                    autoSaveAbortKey.stopMonitoring();
                }
                IngameMessage.stopShowAutoSaveMessage();
                //Wenn irgendetwas mit dem sound abspielen nicht klappt,
                //einfach direkt das Save ausführen.
                //Nicht schön, aber besser als gar nicht saven zu können
                doSave();
            }
        }
        else
        {
            Test.output("Autosave not executed"); //$NON-NLS-1$
        }
    }


    @Override
    public void update(LineEvent event)
    {
        if (event.getType() == LineEvent.Type.STOP)
        {
            try
            {
            	// remove Listener from Sound-Clip
            	Sound.removeListener(this);
            	// stop Sound
            	Sound.stop();
            	// close Sound
    	    	Sound.close();
            	
                IngameMessage.stopShowAutoSaveMessage();
                autoSaveAbortKey.stopMonitoring();
            }
            finally
            {
                doSave();
            }
        }
    }

    /**
     * Gibt das Alter des letzten Saves in Millisekunden zurück.
     * @return Das Alter des letzten Saves in Millisekunden oder -1, falls kein Savegame existiert.
     */
    private long getLastSaveInterval() {
    	File dir = new File(savePath);
    	File[] saveFiles = dir.listFiles(new SaveGame.SavegameFilenameFilter());
    	if (saveFiles == null ||  saveFiles.length == 0) {
    		return -1;
	    } else {
	    	long result = 0;
	    	for (int i = 0; i < saveFiles.length; i++) {
	    		if (saveFiles[i].lastModified() > result) {
	    			result = saveFiles[i].lastModified();
	    		}
	    	}
	    	return System.currentTimeMillis() - result;
	    }
	}

	/**
     * Überprüft noch einmal, ob der Spiel Button immer noch sichtbar ist
     * und klickt dann auf "Spiel speichern".
     */
    private void doSave()
    {
        int index = getIndexOfResolution();
        ImageChecker imgChecker = imageChecker[index];
        if (imgChecker != null && imgChecker.match() && !isAborted)
        {
            Test.output("click on save");
            //aktuelle Mausposition speichern
            Point currentPosition = GameStarterVanilla.getCurrentMousePosition();
            Point clickPoint = imgChecker.getMatchCenter();
            //falls der Spieler gerade eine Maustaste gedrückt
            //hält, wird diese erst losgelassen
            robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
            robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
            //Auf den Spielbutton klicken
            GameStarterVanilla.mouseClick(robot, clickPoint.x, clickPoint.y);
            //auf "Spiel speichern" klicken
            GameStarterVanilla.mouseClick(robot, clickPoints[index].x, clickPoints[index].y);
            if (currentPosition != null)
            {
                //zurück zur Ursprungsposition
                robot.mouseMove(currentPosition.x, currentPosition.y);
            }
        }
        else
        {
            Test.output("doSave aborted, " + (imgChecker != null ? imgChecker.match() : "x") + ", " + isAborted);
        }
    }

    public synchronized void abort()
    {
        isAborted = true;
        Test.output("Autosave aborted"); //$NON-NLS-1$
        IngameMessage.stopShowAutoSaveMessage();
        autoSaveAbortKey.stopMonitoring();
        try
        {
	    	// remove Listener from Sound-Clip
	    	Sound.removeListener(this);
	    	// stop Sound
	    	Sound.stop();
	    	// close Sound
	    	Sound.close();
        } finally {
        	
        }
    }
}
