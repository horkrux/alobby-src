package net.siedler3.alobby.s3gameinterface;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import javolution.io.Struct;
import net.siedler3.alobby.controlcenter.Test;

/**
 * Object to handle communication between s3launcher and aLobby via a named pipe.
 * 
 * @author JHPN
 *
 */
public class IpcClient {
    public static final int PRE_GAME_LOBBY_DIALOG_ID = 3;
    public static final int LOAD_MULTIPLAYER_GAME_DIALOG_ID = 5;

    public static final int PRE_GAME_SLOT_FREE = 0;
    public static final int PRE_GAME_SLOT_HUMAN = 1;
    public static final int PRE_GAME_SLOT_COMP = 2;
    public static final int PRE_GAME_SLOT_CLOSED = 4;

    interface DialogChangedListener {
        void onDialogChanged(int oldDialogId, int newDialogId);
    }
    interface DisconnectListener {
        void onDisconnect();
    }
    interface PreGameLobbyListener {
        void onLobbyUpdate(PreGameLobbyUpdateEvent data);
    }
    interface StatsScreenAvailableListener {
        void onStatsScreenAvailable(String screenPath);
    }

    private interface Dispatcher<T> {
        void dispatch(T listener);
    }

    abstract static class IpcMessage<T> extends Struct {
        public final Header header = new Header();
        public final T data = initData();

        public final ByteOrder byteOrder() {
            return ByteOrder.LITTLE_ENDIAN;
        }

        abstract protected T initData();
    }

    public static class Header extends Struct {
        public final Unsigned16 eventType = new Unsigned16();
        public final Unsigned16 dataLength = new Unsigned16();

        public ByteOrder byteOrder() {
            return ByteOrder.LITTLE_ENDIAN;
        }
    }

    public static class DialogChangeEvent extends Struct {
        public final Signed32 oldDialogId = new Signed32();
        public final Signed32 newDialogId = new Signed32();

        public ByteOrder byteOrder() {
            return ByteOrder.LITTLE_ENDIAN;
        }
    }

    public static class PreGameLobbyUpdateEvent extends Struct {
        public static class Player extends Struct {
            public final UTF8String name = new UTF8String(20);
            public final Unsigned16 slotStatus = new Unsigned16();
            public final Unsigned16 team = new Unsigned16();

            public ByteOrder byteOrder() {
                return ByteOrder.LITTLE_ENDIAN;
            }
        }

        public final Bool isJoined = new Bool();
        public final Bool isHost = new Bool();
        public final Bool isEconomyMode = new Bool();
        public final Bool isAmazonAllowed = new Bool();
        public final Unsigned8 goodsSetting = new Unsigned8();
        public final Bool isSaveGame = new Bool();
        public final Unsigned16 numPlayers = new Unsigned16();
        public final Player[] players = array(new Player[20]);
        public final Unsigned32 gameModes = new Unsigned32();
        public final UTF8String mapName = new UTF8String(260);
        public final UTF8String mapFileName = new UTF8String(260);

        public ByteOrder byteOrder() {
            return ByteOrder.LITTLE_ENDIAN;
        }
    }

    public static class StatsScreenAvailableEvent extends Struct {
        public final UTF8String screenPath = new UTF8String(260);
        public ByteOrder byteOrder() {
            return ByteOrder.LITTLE_ENDIAN;
        }
    }

    /**
     * Standalone main method for testing only the IPC Client (alternative to SettlersLobby::main)
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        IpcClient client = new IpcClient("s3-alobby");
        client.addListener((DialogChangedListener) (oldDialogId, newDialogId) -> {
            if (newDialogId == PRE_GAME_LOBBY_DIALOG_ID) {
            	Test.output("Now in pre-game lobby");
            } else if (newDialogId == LOAD_MULTIPLAYER_GAME_DIALOG_ID) {
            	Test.output("Now in game loading screen");
            }
        });
        client.addListener((DisconnectListener) () -> {
        	Test.output("S3 exited");
        });
        client.addListener((PreGameLobbyListener) (event) -> {
        	Test.output("Pre-Game-Lobby changed:");
        	Test.output("Is-Joined: " + event.isJoined.get());
        	Test.output("Num-Players: " + event.numPlayers.get());
        	Test.output("Is-Amazon-Allowed: " + event.isAmazonAllowed.get());
        });
        client.addListener((StatsScreenAvailableListener) (screenPath) -> {
            Test.output("Stats-Screen at: " + screenPath);
        });
        Test.output("Connecting...");
        client.connect();
    }

    private final String pipeName;
    private boolean isConnected = false;
    private RandomAccessFile pipe;
    private Thread readThread;
    private final List<Object> listeners = new ArrayList<>();

    /**
     * Constructor.
     *
     * @param pipeName the pipe name passed to the launcher
     * @throws IOException
     */
    IpcClient(String pipeName) {
        this.pipeName = pipeName;
    }

    /**
     * Add a listener to this object.
     * 
     * @param listener
     */
    void addListener(Object listener) {
        this.listeners.add(listener);
    }

    synchronized void disconnect() {
        if (isConnected) {
            setDisconnected();
        }

        if (pipe != null) {
            try {
                pipe.close();
            } catch (IOException e) {
                // Nothing to do.
            }

            pipe = null;
        }

        if (readThread != null) {
            try {
                readThread.interrupt();
                readThread.join();
            } catch (InterruptedException e) {
                // Nothing to do.
            }
            readThread = null;
        }
    }

    /**
     * Return if the pipe is connected.
     * 
     * @return boolean	true if it is connected
     */
    public boolean isConnected() {
        return isConnected;
    }

    /**
     * Connect to the pipe it if is not connected atm.
     * 
     * @throws IOException
     */
    synchronized void connect() throws IOException {
        if (isConnected) {
            return;
        }

        // clean-up if previously exited.
        this.disconnect();

        try {
            isConnected = true;
            pipe = new RandomAccessFile(getPipePath(), "rw");

            readThread = new Thread(this::runReads);
            readThread.start();
        } catch (Throwable e) {
            Test.output("Error while connecting: " + e.getMessage());
            setDisconnected();

            throw e;
        }
    }

    /**
     * Wait to shutdown.
     * 
     * @throws InterruptedException
     */
    void waitForShutdown() throws InterruptedException {
        readThread.join();
    }

    /**
     * Set as disconnected. 
     */
    private void setDisconnected() {
        isConnected = false;
        dispatchEvent(DisconnectListener.class, DisconnectListener::onDisconnect);
    }

    /**
     * Return the path to the pipe.
     * 
     * @return
     */
    private String getPipePath() {
        return "\\\\.\\pipe\\" + pipeName;
    }

    /**
     * Send a message to the pipe.
     * 
     * @param <T>
     * @param message
     * @throws IOException
     */
    <T> void send(IpcMessage<T> message) throws IOException {
        if (!isConnected) {
            this.connect();
        }

        synchronized (this) {
            byte[] bytes = message.getByteBuffer().array();
            pipe.write(bytes);
        }
    }

    /**
     * Read from pipe.
     */
    private void runReads() {
        try {
            while (isConnected) {
                byte[] headerBuf = new byte[4];
                pipe.readFully(headerBuf, 0, 4);

                Header header = new Header();
                header.setByteBuffer(ByteBuffer.wrap(headerBuf).order(ByteOrder.LITTLE_ENDIAN), 0);

                int dataLength = header.dataLength.get();
                byte[] dataBuf = new byte[dataLength];
                pipe.readFully(dataBuf, 0, dataLength);

                switch (header.eventType.get()) {
                    case 1:
                        DialogChangeEvent dialogEvt = new DialogChangeEvent();
                        dialogEvt.setByteBuffer(ByteBuffer.wrap(dataBuf).order(ByteOrder.LITTLE_ENDIAN), 0);

                        int oldDialogId = dialogEvt.oldDialogId.get();
                        int newDialogId = dialogEvt.newDialogId.get();

                        dispatchEvent(DialogChangedListener.class, listener ->
                                listener.onDialogChanged(oldDialogId, newDialogId));

                        break;

                    case 2:
                        PreGameLobbyUpdateEvent lobbyEvt = new PreGameLobbyUpdateEvent();
                        lobbyEvt.setByteBuffer(ByteBuffer.wrap(dataBuf).order(ByteOrder.LITTLE_ENDIAN), 0);
                        dispatchEvent(PreGameLobbyListener.class, listener -> listener.onLobbyUpdate(lobbyEvt));
                        break;

                    case 3:
                        StatsScreenAvailableEvent screenAvailableEvent = new StatsScreenAvailableEvent();
                        screenAvailableEvent.setByteBuffer(ByteBuffer.wrap(dataBuf).order(ByteOrder.LITTLE_ENDIAN), 0);
                        dispatchEvent(StatsScreenAvailableListener.class, listener -> listener.onStatsScreenAvailable(screenAvailableEvent.screenPath.get()));
                        break;

                    default:
                    	Test.output("Unknown Event-Type: " + header.eventType.get());
                }


                System.out.println("--------------------------------------------");
            }
        } catch (Throwable e) {
            System.out.println("IPC Read-Thread exited.");
            Test.outputException(e);
            setDisconnected();
        }
    }

    private <T> void dispatchEvent(Class<T> listenerCls, Dispatcher<T> dispatcher) {
        for (Object listener : listeners) {
            if (listenerCls.isInstance(listener)) {
                try {
                    dispatcher.dispatch(listenerCls.cast(listener));
                } catch (Throwable e) {
                	Test.outputException(e);
                }
            }
        }
    }
}
