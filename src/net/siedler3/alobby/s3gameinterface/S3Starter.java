/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.OpenGame.MiscType;
import net.siedler3.alobby.controlcenter.S3Map;
import net.siedler3.alobby.controlcenter.S3RandomMap;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.s3gameinterface.GameStarter.StartOptions;
import net.siedler3.alobby.util.JavaProcessWrapper;

/**
 * Start S3 with alobby-own unified exe or s3launcher depending on configured version
 * as single Thread.
 */
public class S3Starter extends Thread
{
	// the settlersLobby-object
	private SettlersLobby settlersLobby;
	
	// the process-object
	private Process process;
	
	// marker to cleanup the process
	private boolean cleanup = true;
	
	// marker if game is running
	private boolean isRunning = false;
	
	// the name of the pipe to communicate with s3launcher
	private String pipeName;

	// marker if unified s3.exe should be started
	private boolean startVanilla;

	// the startOptions to start the game
	private StartOptions startOptions;

	/**
	 * Start S3 depending on startOptions and other settings.
	 * 
	 * @param settlersLobby
	 * @param startOptions
	 * @param host
	 * @throws IOException
	 */
	public S3Starter(SettlersLobby settlersLobby, StartOptions startOptions, boolean host) throws IOException
	{
		this.settlersLobby = settlersLobby;
		this.startOptions = startOptions;
		
		// Check if the path to the s3.exe is valid and throw error if it is not valid
		if (!settlersLobby.isS3GamePathValid())
		{
		    throw new IOException(I18n.getString("S3Starter.ERROR_S3_NOT_FOUND")); //$NON-NLS-1$
		}
		
		// set the process-name
		setName(getName() + "-S3Starter"); //$NON-NLS-1$

		// get the directory where s3.exe resides
		File directory = new File(settlersLobby.config.getGamePathS3()).getParentFile();
		
		// check which version should be started
		// -> true if Vanilla-version should be started
		startVanilla = ((startOptions != null && startOptions.getMisc() != MiscType.S3CE) || startOptions == null) ? true : false;
		Test.output("startVanilla: " + startVanilla);
		
		// start s3 depending on the version of the open game
		if( false != startVanilla ) {
			Test.output("starting: " + settlersLobby.config.getGamePathS3());
			process = JavaProcessWrapper.createProcess(settlersLobby.config.getGamePathS3(), directory);
		}
		// -> after 3.00
		else if( false == startVanilla && startOptions != null ) {
			// set arguments for ProcessBuilder
			List<String> args = new ArrayList<>();
			args.add(directory.toString() + File.separator + "s3ce.exe");

			// -> launch without gui
			args.add("-launch");

			// -> set Nickname
			args.add("-nickname");
			args.add(settlersLobby.getNick());

            // -> set the graphics mode
            args.add("-graphic-mode");
            args.add("-1");

            // -> disable-balancing-patches for the release
            args.add("-disable-balancing-patches");
            args.add("1");

			// -> set resolution
			String resolution = settlersLobby.config.getS3CEResolution();
			Test.output("Setting game resolution to: " + resolution);
			args.add("-display-width");
			args.add(resolution.split("x")[0]);
			args.add("-display-height");
			args.add(resolution.split("x")[1]);
			
			// -> start game as host or player
			if( false != host ) {
				// -> start as host
				args.add("-host");
	
				// -> set map-type
				int mapCategory = 2; // Multiplayer-Map
				if( startOptions.getMapName().contains(ALobbyConstants.PATH_RANDOM + File.separator) ) {
					S3RandomMap map = (S3RandomMap) S3Map.newInstance(startOptions.getMapNameOriginal());
					mapCategory = 0; // Random-Map
					// -> set map-size for the random map
					args.add("-map-size");
					args.add("" + RandomMapSizesForDLL.valueOf("size" + map.getSize()).getIndex());
					// -> set mirror-type
					int mirrortype = 0;
					if( map.isMirrorXAxis() ) {
						mirrortype = 1;					
					}
					if( map.isMirrorYAxis() ) {
						mirrortype = 2;					
					}
					if( map.isMirrorBoth() ) {
						mirrortype = 3;					
					}
	
					args.add("-mirror-type");
					args.add("" + mirrortype);
				}
				if( startOptions.getMapName().startsWith(ALobbyConstants.LABEL_SAVE_GAME) ) {
					mapCategory = 4; // Save-Game
					// -> set map-name
					args.add("-map-name");
					String mapname = startOptions.getMapName().replace(ALobbyConstants.LABEL_SAVE_GAME + File.separator, "");
					if( !mapname.endsWith(".mps") ) {
						StringBuilder b = new StringBuilder(mapname);
						b.replace(mapname.lastIndexOf("."), mapname.length(), "");
						mapname = b.toString();
					}
					args.add(mapname);
				}
				if( startOptions.getMapName().contains(ALobbyConstants.PATH_USER + File.separator) ) {
					mapCategory = 3; // User-Game
					// -> set map-name
					args.add("-map-name");
					args.add(startOptions.getMapName().replace(ALobbyConstants.PATH_USER + File.separator, ""));
				}
				args.add("-map-category");
				args.add("" + mapCategory);
	
				// -> set goods
				args.add("-goods-setting");
				args.add("" + startOptions.getSelectGoodsInStock().getIndex());
	
				// -> set random positions
				args.add("-random-positions");
				args.add(startOptions.isSelectRandomPosis() ? "1" : "0");
			}
			else {
				// -> join a game with given IP-address
				args.add("-join");
				args.add(startOptions.getIp());
			}

			// -> generate a pipe-name for the communication between launcher and aLobby
			pipeName = "s3-" + Long.toString(System.currentTimeMillis());
			// -> set the pipe-name
			args.add("-pipe");
			args.add(pipeName);
			
			// -> set the screenshot-directory
			args.add("-screen-directory");
			args.add(settlersLobby.configDir + File.separator + "screens");

			// create and start the process with the given arguments
			Test.output("starting: " + String.join(" ", args));
			ProcessBuilder builder = new ProcessBuilder(args);
			builder.directory(directory);
			builder.redirectErrorStream(true);
			builder.inheritIO();

			try {
				if (builder.start().waitFor() == 0) {
					setRunning(true);
				}
			} catch (InterruptedException e) {
				Test.outputException(e);
				setRunning(false);
			}
		}
		if (process != null)
		{
		    Test.output("using class " + process.getClass().getName()); //$NON-NLS-1$
		}

        IProcessWindowSupport processWindowSupport = null;
        if (process instanceof IProcessWindowSupport)
        {
            processWindowSupport = (IProcessWindowSupport)process;
        }
        ProcessWindowSupportHandler.getInstance().setProcessWindowSupport(processWindowSupport);
	}

	@Override
    public void run()
	{
		// only run this method if unified s3.exe is used
		if( false != startVanilla ) {
			try
			{
				int rc = process.waitFor();
				Test.output("S3 exited: " + rc);
				//remove the process window support if it is still used
		        if (process instanceof IProcessWindowSupport)
		        {
		            ProcessWindowSupportHandler.getInstance().removeProcessWindowSupport((IProcessWindowSupport)process);
		        }
			}
			catch (InterruptedException e)
			{
			    //the gamestarter may interrupt this thread, in this case
			    //cleanup is false
				Test.outputException(e);
				Thread.currentThread().interrupt();
			}
			catch (Exception ex)
			{
			    Test.outputException(ex);
			    //something unexpected happened
			    //clear the link to the process
			}
			finally
			{
			    process = null;
			    //from the point of the alobby S3 has exited,
			    //so call onS3ShutDown() to cleanup all related tasks, e.g. the gamestarter
	            if (cleanup && settlersLobby != null)
	            {
	                settlersLobby.onS3ShutDown();
	            }
			}
		}
	}

	/**
	 * Kill the running process.
	 */
	public void destroyS3()
	{
        if (process != null)
        {
            process.destroy();
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {}
            if (process != null)
            {
                process.destroyForcibly();
            }
        }
	}

	/**
	 * Return marker if S3 is running.
	 * 
	 * @return boolean
	 */
    public boolean isS3Running()
    {
        return process != null || getRunning();
    }

    /**
     * Set cleanup-marker.
     * 
     * @param boolean
     */
    public synchronized void setCleanup( boolean setCleanup )
    {
        cleanup = setCleanup;
    }

	/**
	 * Map the aLobby-size to the DLL-size.
	 * 
	 * @author Zwirni
	 */
	private enum RandomMapSizesForDLL {
		size384("384", 0),
		size448("448", 1),
		size512("512", 2),
		size576("576", 3),
		size640("640", 4),
		size704("704", 5),
		size768("768", 6);

		@SuppressWarnings("unused")
		private String value;
		private int index;

		RandomMapSizesForDLL(String value, int index) {
			this.value = value;
            this.index = index;
		}
		
		/*
		*
        * @return String with human-friendly displayName
        */
		public int getIndex() {
			return index;
		}
	};
	
	/**
	 * Return the Pipename which is used to communicate between aLobby and Launcher.
	 * 
	 * @return String
	 */
	public String getPipeName() {
		return this.pipeName;
	}
	
	/**
	 * Set marker for s3 is running.
	 * 
	 * @param boolean isRunning
	 */
	public void setRunning(boolean b) {
		isRunning = b;
	}
	
	/**
	 * Return marker for s3 is running.
	 * 
	 * @return
	 */
	public boolean getRunning() {
		return isRunning;
	}
	
	/**
	 * Return the startOptions for the game.
	 * 
	 * @return
	 */
	public StartOptions getStartOptions() {
		return this.startOptions;
	}
	
}
