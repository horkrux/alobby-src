/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2020 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.UIManager;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.GoodsInStock;
import net.siedler3.alobby.controlcenter.ReqQuery;
import net.siedler3.alobby.controlcenter.S3RandomMap;
import net.siedler3.alobby.controlcenter.SaveGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.MessageDialog;
import net.siedler3.alobby.gui.additional.IngameMessage;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.nativesupport.NativeFunctions;
import net.siedler3.alobby.util.KeyBlockStarter;

/**
 * Zum Fernsteuern von Siedler3 zum Erzeugen und Beitreten von TCP/IP
 * Multyplayerspielen.
 *
 * @author maximilius (Stephan Bauer)
 * @version 0.8
 */
public class GameStarterVanilla implements GameStarter
{
    public static final int ORANGE_AND_GREEN = 0;
	public static final int LILA = 1;
	public static final int GOLD = 2;
	public static final int RED = 3;
	public static final int ORANGE_AND_GREEN2 = 4;
	public static final int LILA2 = 5;
	public static final int GOLD2 = 6;
	private static final int WAIT_FOR_SECOND_GAME = 100;

	//auf langsamen Rechnern werden eventuell
	//zu schnell zu viele Input events generiert,
	//so daß der Eingabe Puffer überläuft.
	//Der Wert gibt die Anzahl Millisekunden an,
	//die nach einem Klick beim Karte selektieren
	//gewartet werden soll.
	//Ein Wert von 1 oder 2 sollte selbst auf langsamen
	//Rechnern ausreichend sein, auf schnellen
	//Rechnern funktioniert auch 0 tadellos.
	public static int mapSelectDelayAfterClick = 0;
	public static boolean mapSelectFastVariant = true;

	//ist useSendInputNative true, so wird Text in S3
	//über eine native SendInput Funktion eingegeben,
	//andernfalls über die Java robot Klasse
	public static boolean useSendInputNative = true;
	
	public boolean isRunning;
	public static S3Starter s3Starter;

	private static Map<Character, String> altKeyMap = new HashMap<Character, String>();

	static
	{
	    altKeyMap.put('-', "045");
	    altKeyMap.put('.', "046");
	    altKeyMap.put(';', "059");
	    altKeyMap.put('_', "095");
	    //auf belgischen Rechnern sind auch die Zahlen nur mittels
	    //shift erreichbar, daher wird hier versucht mittels des
	    //alt codes zu arbeiten
	    altKeyMap.put('0', "048");
	    altKeyMap.put('1', "049");
	    altKeyMap.put('2', "050");
	    altKeyMap.put('3', "051");
	    altKeyMap.put('4', "052");
	    altKeyMap.put('5', "053");
	    altKeyMap.put('6', "054");
	    altKeyMap.put('7', "055");
	    altKeyMap.put('8', "056");
	    altKeyMap.put('9', "057");


	}
	private static final int[] NUMPAD_KEYS = {
	    KeyEvent.VK_NUMPAD0,
	    KeyEvent.VK_NUMPAD1,
	    KeyEvent.VK_NUMPAD2,
	    KeyEvent.VK_NUMPAD3,
	    KeyEvent.VK_NUMPAD4,
	    KeyEvent.VK_NUMPAD5,
	    KeyEvent.VK_NUMPAD6,
	    KeyEvent.VK_NUMPAD7,
	    KeyEvent.VK_NUMPAD8,
	    KeyEvent.VK_NUMPAD9,
	};


	private int currentEdition;
	private Robot robot; // Klasse zum simulieren von Eingaben
	private Color fontColor = new Color(255, 223, 0); // Schriftfarbe in
	// Siedler3
	private PixelColorChecker[] menuColor; // Array für Menüfarben
	private PixelColorChecker colorTCPIP;
	private PixelColorChecker colorTCPIP_old;
	private HostObserver hostObserver;
	private JoinObserver joinObserver;
	private ThreadCommunicator threadCommunicator;

    private SettlersLobby settlersLobby;
	public static KeyBlockStarter keyBlockStarter = null;
    private boolean isAborted = false;

    private StartOptions startOptions = new StartOptions("", null, "", 0, false, 0, "", null); //$NON-NLS-1$
    private S3FontSize s3Fontsize = null;

    private MultiImageChecker s3LanguageChecker;
    private MultiImageChecker tcpIpDirectPlayChecker;
    private ImageChecker tcpIpVisibleChecker;

    private Thread starterThread;

	enum MapType
	{
		// Der Inhalt von SettlersLobby.PATH_RANDOM, SettlersLobby.PATH_MULTI,
		// SettlersLobby.PATH_USER muss gleich den entsprechenden Konstanten
		// sein. Bsp.: SettlersLobby.PATH_RANDOM = "RANDOM";
	    RANDOM(ALobbyConstants.PATH_RANDOM)
	    {
	        @Override
            public String[] getMaps(SettlersLobby settlersLobby)
	        {
	            return new String[0];
	        }
	    },
	    MULTI(ALobbyConstants.PATH_S3MULTI)
        {
           @Override
        public String[] getMaps(SettlersLobby settlersLobby)
           {
                return settlersLobby.getMultiS3Maps();
           }
	    },
	    USER(ALobbyConstants.PATH_USER)
        {
           @Override
        public String[] getMaps(SettlersLobby settlersLobby)
           {
                return settlersLobby.getUserMaps();
           }
	    },
	    SAVEGAME(ALobbyConstants.LABEL_SAVE_GAME)
	    {
           @Override
        public String[] getMaps(SettlersLobby settlersLobby)
           {
                return settlersLobby.getSavegameFileNames();
           }
	    };

	    private String path;
	    MapType(String path)
	    {
	        this.path = path;
	    }

	    public String getPath()
	    {
	        return path;
	    }

	    abstract public String[] getMaps(SettlersLobby settlersLobby);

	    public static MapType getValueFromMapName(String mapName)
	    {
	    	MapType result = null;
	        int index = mapName.indexOf('\\');
            if (mapName.startsWith(ALobbyConstants.LABEL_SAVE_GAME))
            {
                result = SAVEGAME;
            }
	    	if (index >= 0)
	        {
    	        String dir = mapName.substring(0, index);
	        	try
    	        {
    	            result = valueOf(dir);
    	        }
    	        catch(Exception e)
    	        {
    	        }
	        }
	        return result;
	    }
	}

	private static class S3FontSize
	{
	    private int fontsize;
	    private int numberOfGamesOnPage;

        private S3FontSize(int fontsize, int numberOfGamesOnPage)
        {
            this.fontsize = fontsize;
            this.numberOfGamesOnPage = numberOfGamesOnPage;
        }

        private S3FontSize(int fontsize)
        {
            this.fontsize = fontsize;
        }

        public int getFontsize()
        {
            return fontsize;
        }

        public int getNumberOfGamesOnPage()
        {
            return numberOfGamesOnPage;
        }
	}

	/**
	 * Konstruktor
	 *
	 * @throws Exception
	 */
	public GameStarterVanilla(SettlersLobby settlersLobby) throws Exception
	{
		this.settlersLobby = settlersLobby;
	    
		try
		{
			threadCommunicator = ThreadCommunicator.getInstance();
			robot = threadCommunicator.getRobot();
			threadCommunicator.setActionListener(this.settlersLobby);
			menuColor = new PixelColorChecker[7];
			menuColor[ORANGE_AND_GREEN] = new PixelColorChecker(robot, 255, 93,
					24, 6, 0);
            menuColor[ORANGE_AND_GREEN2] = new PixelColorChecker(robot, 255, 93,
                            24, 6, 30);
			menuColor[LILA] = new PixelColorChecker(robot, 140, 105, 255, 6, 0);
			menuColor[LILA2] = new PixelColorChecker(robot, 140, 105, 255, 6, 30);
			menuColor[GOLD] = new PixelColorChecker(robot, 173, 154, 90, 5, 0);
			menuColor[GOLD2] = new PixelColorChecker(robot, 173, 154, 90, 5, 30);
			menuColor[RED] = new PixelColorChecker(robot, 156, 8, 24, 13, 12);
            for (PixelColorChecker tmp : menuColor)
            {
                tmp.setScreenSizeS3Menu();
            }
			colorTCPIP_old = new PixelColorChecker(robot, 255, 223, 0, 133, 126);
			colorTCPIP_old.add(255, 223, 0, 311, 126);
			colorTCPIP_old.add(255, 223, 0, 314, 126);
			colorTCPIP_old.add(255, 223, 0, 324, 133);
			colorTCPIP_old.add(255, 223, 0, 323, 134);
			colorTCPIP_old.setScreenSizeS3Menu();
			// Neue TCPIP Erkennung:
			colorTCPIP = new PixelColorChecker(robot, 255, 223, 0, 324, 0);
			colorTCPIP.setScreenSizeS3Menu();
			// Neue TCPIP Erkennung - Ende

		}
		catch (AWTException e)
		{
			throw new Exception(
					MessageFormat.format(I18n.getString("GameStarter.ERROR_INIT"), //$NON-NLS-1$
							e.toString()));
		}
	}

	/**
	 * Zum Umwandeln von Zeichen in Tastatureingaben. Akzeptiert derzeit nur
	 * "a"-"z", "A"-"Z", und " ".
	 *
	 * @param c
	 *            Das zu übersetzende Zeichen.
	 * @return Das übersetzte Zeichen.
	 */
	private int getCharCode(char c)
	{
		int erg;
		switch (c)
		{
		case 'a':
			erg = KeyEvent.VK_A;
			break;
		case 'b':
			erg = KeyEvent.VK_B;
			break;
		case 'c':
			erg = KeyEvent.VK_C;
			break;
		case 'd':
			erg = KeyEvent.VK_D;
			break;
		case 'e':
			erg = KeyEvent.VK_E;
			break;
		case 'f':
			erg = KeyEvent.VK_F;
			break;
		case 'g':
			erg = KeyEvent.VK_G;
			break;
		case 'h':
			erg = KeyEvent.VK_H;
			break;
		case 'i':
			erg = KeyEvent.VK_I;
			break;
		case 'j':
			erg = KeyEvent.VK_J;
			break;
		case 'k':
			erg = KeyEvent.VK_K;
			break;
		case 'l':
			erg = KeyEvent.VK_L;
			break;
		case 'm':
			erg = KeyEvent.VK_M;
			break;
		case 'n':
			erg = KeyEvent.VK_N;
			break;
		case 'o':
			erg = KeyEvent.VK_O;
			break;
		case 'p':
			erg = KeyEvent.VK_P;
			break;
		case 'q':
			erg = KeyEvent.VK_Q;
			break;
		case 'r':
			erg = KeyEvent.VK_R;
			break;
		case 's':
			erg = KeyEvent.VK_S;
			break;
		case 't':
			erg = KeyEvent.VK_T;
			break;
		case 'u':
			erg = KeyEvent.VK_U;
			break;
		case 'v':
			erg = KeyEvent.VK_V;
			break;
		case 'w':
			erg = KeyEvent.VK_W;
			break;
		case 'x':
			erg = KeyEvent.VK_X;
			break;
		case 'y':
			erg = KeyEvent.VK_Y;
			break;
		case 'z':
			erg = KeyEvent.VK_Z;
			break;
		case '1':
			erg = KeyEvent.VK_1;
			break;
		case '2':
			erg = KeyEvent.VK_2;
			break;
		case '3':
			erg = KeyEvent.VK_3;
			break;
		case '4':
			erg = KeyEvent.VK_4;
			break;
		case '5':
			erg = KeyEvent.VK_5;
			break;
		case '6':
			erg = KeyEvent.VK_6;
			break;
		case '7':
			erg = KeyEvent.VK_7;
			break;
		case '8':
			erg = KeyEvent.VK_8;
			break;
		case '9':
			erg = KeyEvent.VK_9;
			break;
		case '0':
			erg = KeyEvent.VK_0;
			break;
		case ' ':
			erg = KeyEvent.VK_SPACE;
			break;
		case '_':
		    erg = KeyEvent.VK_UNDERSCORE;
		    break;
        case '-':
            erg = KeyEvent.VK_MINUS;
            break;
        case '.':
            erg = KeyEvent.VK_PERIOD;
            break;
		default:
			erg = 0;
		}
		return erg;
	}

	/**
	 * Manövriert die Maus an x, y Position, klickt die Maus, simuliert so lange
	 * Backspace, bis Pixelfarbe an x, y Position nicht mehr gelb und simuliert
	 * dann Tastatureingabe von text.
	 *
	 * @param text
	 *            Der zu simulierende Text.
	 * @param x
	 *            die x Position des Mausklicks und Einlesen der Pixelfarbe.
	 * @param y
	 *            die y Position des Mausklicks und Einlesen der Pixelfarbe.
	 * @return false bei Zeitüberschreitung, ansonsten true.
	 * @throws Exception
	 */
	private void enterStringAt(String text, int x, int y, String errorMessage)
			throws Exception
	{
		mouseClick(x, y);
		PixelColorChecker cursor = new PixelColorChecker(robot, 255, 223, 0, x, y);
		cursor.add(255, 223, 0, x + 1, y + 23);
		int i = 0;
		long endTime = System.currentTimeMillis() + timeoutInSeconds() * 1000;
		while (!(cursor.match()) && (System.currentTimeMillis() <= endTime) && i < 255)
		{
			pressKey(KeyEvent.VK_BACK_SPACE);
			i++;
			Thread.sleep(10);
		}
		if (System.currentTimeMillis() < endTime)
		{
		    if (useSendInputNative)
		    {
		        //benutze native Methode, diese berücksichtigt (angeblich)
		        //das TastaturLayout
		        //Damit sind hoffentlich auch die Probleme mit unterschiedlichen
		        //Tastaturlayouts erledigt
		        if (!NativeFunctions.getInstance().enterText(text))
		        //if (!SendInputNative.enterText(text))
		    	{
	                throw new Exception(
	                        MessageFormat.format(I18n.getString("GameStarter.ERROR_ENTER_AT"), //$NON-NLS-1$
	                                errorMessage, "enterTextNative=false")); //$NON-NLS-1$
		        }
		        return;
		    }

			try
			{
			    // Beim Schreiben von Texten gibt es Probleme mit unterschiedlichen
			    // Tatstaturlayouts. Auslöser war in dem Fall ein belgisches
			    // Tastaturlayout, dort ist der '.' keine Taste.
			    // Für a-z und 0-9 wird aktuell vorausgesetzt, daß die zugehörige
			    // KeyEvent.VK_ Taste definiert ist.
			    // Sonderzeichen werden jetzt mit ALT und dem zugehörigen
			    // Tastaturcode umgesetzt, das funktioniert nicht nur im Editor,
			    // sondern auch in S3. Merkwürdigerweise funktioniert es, egal
			    // ob NumLock an oder aus ist.
			    // Die Codes sind in altKeyMap hinterlegt.
				String textLow = text.toLowerCase();
				for (i = 0; i < text.length(); i++)
				{
				    char c = textLow.charAt(i);
					/*if (c == '_' || c == '.' || c =='-')
					{
					    keyTypeUsingAlt(c);
					}
					else
					{*/
						if (c != text.charAt(i))
						{
						    //Großschreibung mittels Shift
							robot.keyPress(KeyEvent.VK_SHIFT);
							keyType(c);
							robot.keyRelease(KeyEvent.VK_SHIFT);
						} else
						{
						    //durch shift-release wird sichergestellt,
						    //dass kein shift aktiv ist, selbst wenn es vom user
						    //gedrückt wird
						    robot.keyRelease(KeyEvent.VK_SHIFT);
						    keyType(c);
						}
					/*}*/
				}
			} catch (Exception e)
			{
				throw new Exception(
						MessageFormat.format(I18n.getString("GameStarter.ERROR_ENTER_AT"), //$NON-NLS-1$
								errorMessage, e.getMessage()));
			}
		} else
		{
			throw new Exception(
			        MessageFormat.format(I18n.getString("GameStarter.ERROR_TIMEOUT_ENTER_AT"), //$NON-NLS-1$
							errorMessage));
		}
	}

    private int timeoutInSeconds()
    {
        return settlersLobby.config.getTimeOutAsInt();
    }

	/**
	 * Versucht das Zeichen mittels des zugehörigen Virtual Keycodes zu erzeugen.
	 * Schlägt das fehl, wird versucht das Zeichen mittels eines alt-keycodes zu
	 * erzeugen.
	 *
	 * @param c
	 * @throws InterruptedException
	 */
	public void keyType(char c) throws InterruptedException
	{
	    try
	    {
	        pressKey(getCharCode(c));
	    }
	    catch (IllegalArgumentException e)
	    {
	        Test.output("using alt-code for " + c);
	        keyTypeUsingAlt(c);
	    }
	}
	/**
	 * erzeugt das Zeichen 'c' mittels Tastendrücken, indem es den
	 * zugehörigen ALT+xyz Code eingibt.
	 *
	 * @param c
	 * @throws InterruptedException
	 */
	private void keyTypeUsingAlt(char c) throws InterruptedException
	{
	    if (!altKeyMap.containsKey(c))
	    {
	        throw new IllegalArgumentException("unsupported Character"); //$NON-NLS-1$
	    }
	    String numpadSequence = altKeyMap.get(c);
	    keyTypeUsingAlt(numpadSequence);
	}

	/**
	 * drückt ALT und die durch numpadSequence angegebene Folge an
	 * Nummernblock Tasten.
	 *
	 * @param numpadSequence Zahlenfolge der NumpadTasten
	 * @throws InterruptedException
	 */
	private void keyTypeUsingAlt(String numpadSequence) throws InterruptedException
    {
	    if (numpadSequence.length() > 4)
	    {
	        throw new IllegalArgumentException("invalid numpadSequence"); //$NON-NLS-1$
	    }
        robot.keyPress(KeyEvent.VK_ALT);
        try
        {
            //verwandelt den String z.b. "095"
            //in Tastendrücke VK_NUMPAD_0, VK_NUMPAD_9, VK_NUMPAD_5
            for (Character c : numpadSequence.toCharArray())
            {
                pressKey(NUMPAD_KEYS[Integer.parseInt(c.toString())]);
            }
        }
        catch (NumberFormatException e) {
            Test.outputException(e);
        }
        finally
        {
            robot.keyRelease(KeyEvent.VK_ALT);
        }
    }

    /**
	 * Startet Siedler3 und wartet solange, bis das Startmenü angezeigt wird.
	 * Das Warten wird durch einlesen von Pixelfarben realisiert. Derzeit ist
	 * nur die Siedler3 Goldedition unterstützt.
	 *
	 * @return false bei Zeitüberschreitung, ansonsten true.
	 * @throws Exception
	 */
	private void startGame() throws Exception
	{
        if (s3Starter != null && s3Starter.isAlive())
        {
            //wenn S3 noch läuft, hätte diese Funktion gar nicht aufgerufen
            //werden dürfen.
            //Kill S3
            s3Starter.setCleanup(false); //use noCleanup as a mechanism to avoid call of onS3Shutdown in s3Starter
            s3Starter.destroyS3();
            Thread.sleep(500);
            Test.output("interrupting s3Starter");
            s3Starter.interrupt();
            //wait until the thread finally ends
            s3Starter.join();
        }
        if (this.keyBlockStarter != null && keyBlockStarter.isAlive())
        {
            keyBlockStarter.destroyKeyBlock();
            Thread.sleep(500);
            Test.output("interrupting keyBlockStarter");
            keyBlockStarter.interrupt();
            //wait until the thread finally ends
            keyBlockStarter.join();
        }
        // First we try to disable the intro..
        ReqQuery.setS3Intro(0);
        try
        {
            s3Starter = new S3Starter(settlersLobby, null, false);
            s3Starter.start();
        } catch (IOException e)
        {
            Test.outputException(e);
            //wenn der S3Starter gar nicht erst gestartet wurde,
            //wird von ihm auch niemals settlersLobby.onS3Shutdown
            //aufgerufen.
            //daher wird das jetzt hier erledigt, anschliessend
            //wird der gamestarter abgebrochen.
            settlersLobby.onS3ShutDown();
            throw new Exception(MessageFormat.format(I18n.getString("GameStarter.ERROR_STARTING_S3"),
                                                     e.getMessage() != null ? e.getMessage() : e.toString()));
        }

        try
        {
            this.keyBlockStarter = new KeyBlockStarter(settlersLobby);
            keyBlockStarter.start();
        } catch (IOException e)
        {
            Test.output("Starting the KeyBlock failed!");
            Test.outputException(e);
        }
		int i = 0;
		boolean match = false;
		// Falls S3Intro -1 oder 1 liefert (Fehler oder aktiviert),
		// könnte ein Intro angezeigt werden
		boolean isPossibleIntro = ReqQuery.getS3Intro() != 0;
        if (isPossibleIntro) {
            Test.output("Disabling the intro failed and it is still " + ReqQuery.getS3Intro());
        } else {
            Test.output("s3 Intro: " + ReqQuery.getS3Intro());
        }

		do
		{
			if (menuColor[ORANGE_AND_GREEN].match())
			{
				match = true;
				currentEdition = ORANGE_AND_GREEN;
			} else if (menuColor[LILA].match())
			{
				match = true;
				currentEdition = LILA;
            } else if (menuColor[LILA2].match())
            {
                match = true;
                currentEdition = LILA;
			} else if (menuColor[GOLD].match())
			{
				match = true;
				currentEdition = GOLD;
            } else if (menuColor[GOLD2].match())
            {
                match = true;
                currentEdition = GOLD;
			} else if (menuColor[RED].match())
			{
				match = true;
				currentEdition = RED;
			}
			Thread.sleep(500);
			if (isPossibleIntro && i > 10)
			{
			    //drücke Shift, um das Intro abzuwürgen,
			    //falls es zu sehen sein sollte
			    //(frühestens nach 5 Sekunden, wenn jemand das Intro
			    // aktivert hat, soll er es wenigstens kurz zu sehen bekommen)
			    pressKey(KeyEvent.VK_SHIFT);
			}
			i++;

		} while (!match && (i <= timeoutInSeconds() * 2));
		Test.output("S3-Edition: " + currentEdition);
		threadCommunicator.setCurrentEdition(currentEdition);

		startShowNoUserInteractionMessage();

		if (i > timeoutInSeconds() * 2)
		{
			// get textcolor as hexcolor to generate colored link in error-message if game opening fails
	        String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
			
        	// -> show hint to user
        	MessageDialog messagewindow = new MessageDialog(null, I18n.getString("SettlersLobby.ERROR"), true);
        	// -> add message
    		messagewindow.addMessage(
    			"<html>" + I18n.getString("GameStarter.ERROR_TIMEOUT_S3") + "</html>", 
    			UIManager.getIcon("OptionPane.errorIcon")
    		);
    		// -> add ok-button
    		messagewindow.addButton(
    			UIManager.get("OptionPane.okButtonText") != null ? UIManager.get("OptionPane.okButtonText").toString() : "OK",
    			new ActionListener() {
    			    @Override
    			    public void actionPerformed(ActionEvent e) {
    			    	messagewindow.dispose();
    			    }
    			}
    		);
    		// show it
    		messagewindow.setVisible(true);
    		throw new Exception(
                    // set hexcolor as color for links in errortext
                    MessageFormat.format(I18n.getString("GameStarter.ERROR_TIMEOUT_S3"),hexcolor)); //$NON-NLS-1$
		}
	}

    /**
     * Zeigt dem User in S3 eine Nachricht an, daß er die Finger von Maus
     * und Tastatur lassen soll.
     */
    private void startShowNoUserInteractionMessage()
    {
        if (settlersLobby.config.getShowMessagesIngame())
        {
            IngameMessage.startShowNoUserInteractionMessage(settlersLobby.config.getTimeOutAsInt());
        }
    }

	/**
	 * Manövriert die Maus an x, y Position und simuliert einen Linksklick.
	 *
	 * @param x
	 *            x Position des Mausklicks.
	 * @param y
	 *            y Position des Mausklicks.
	 * @throws InterruptedException
	 */
	private void mouseClick(int x, int y) throws InterruptedException
	{
	    if (Thread.currentThread().isInterrupted())
	    {
	        throw new InterruptedException();
	    }
	    
	    // set mouse on its position twice
	    robot.mouseMove(x, y);
	    robot.mouseMove(x, y);
		
		// press mouse button and release it
		robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
	}

    /**
     * Manövriert die Maus an x, y Position und simuliert einen Linksklick.
     *
     * @param x
     * @param y
     * @throws InterruptedException
     */
    private void fastMouseClick(int x, int y) throws InterruptedException
    {
        if (Thread.currentThread().isInterrupted())
        {
            throw new InterruptedException();
        }

        // set mouse on its position
		//this.betterMouseMove(x, y, this.scaleFactor);
		robot.mouseMove(x, y);
		
		// press the mouse-button and release it
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    }

	/**
	 * Manövriert die Maus an x, y Position und simuliert einen Linksklick.
	 * Danach wird gewartet.
	 *
	 * @param x
	 * @param y
	 * @param delay Wartezeit in ms, die nach dem Klick gewartet wird, >=0.
	 * @throws InterruptedException
	 */
	private void fastMouseClickAndDelay(int x, int y, int delay) throws InterruptedException
	{
        if (Thread.currentThread().isInterrupted())
        {
            throw new InterruptedException();
        }
        // set mouse on its position
     	//this.betterMouseMove(x, y, this.scaleFactor);
     	robot.mouseMove(x, y);
     	
     	// press the mouse-button and release it
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        
        // sleep afterwards
        Thread.sleep(delay);
	}

	/**
	 * TODO dummy löschen, wenn neue Funktion funktioniert
	 * Wartet, bis an bestimmter Position gelber Pixel erscheint und klickt.
	 * Derzeit funktioniert das nur, wenn ausschließlich TCP/IP Protokoll
	 * installiert ist. Sobald mehrere Protokolle installiert sind, kann es
	 * passieren, dass TCP/IP nicht an erster Stelle in der Auswahlliste steht.
	 *
	 * @return false bei Zeitüberschreitung, ansonsten true.
	 * @throws Exception
	 */
	private void chooseTCPIP_old() throws Exception
	{
		int i = 0;
		int position = -1;
		do
		{
			Thread.sleep(500);
			for (int ii = 0; ((position == -1) && (ii < 7)); ii++)
			{
				if (colorTCPIP_old.match(0, 13 * ii))
				{
					position = ii;
				}
			}
			i++;
		} while ((position == -1) && (i <= timeoutInSeconds()));
		if (i <= timeoutInSeconds())
		{
			mouseClick(133, (126 + 13 * position));
		} else
		{
			throw new Exception(
					I18n.getString("GameStarter.ERROR_TIMEOUT_TCP_IP")); //$NON-NLS-1$
		}
	}

	/**
	 * Versucht auf das TCP/IP Protokoll zu selektieren.
	 * Zuerst wird ein ImageVergleich versucht.<br>
	 * Schlägt dies fehl, wird gewartet, bis an einer bestimmten Position
	 * ein gelber Pixel erscheint und dann ausgewählt. Diese 2. Variante
	 * funktioniert nur, wenn ausschließlich TCP/IP Protokoll installiert ist.
	 *
	 * @return false bei Zeitüberschreitung, ansonsten true.
	 * @throws Exception
	 */
	private void chooseTCPIP() throws Exception
	{
		int i = 0;
		Point p;

		// click 5 times on the position where tcp-ip should be visible atm
		Thread.sleep(timeoutInSeconds()*100);
		for( i=0;i<5;i++ ) {
			mouseClick(222,130);
			Thread.sleep(500);
		}
		/*
		//erstmal solange warten, bis TCP/IP sichtbar sein sollte
		//falls es nicht klappt, werden die weiteren Checker trotzdem
		//aufgerufen
		Test.output("check1 start");
		boolean imageCheckerTest = ImageChecker.waitUntilImageMatch(tcpIpVisibleChecker, timeoutInSeconds(), 500);
		Test.output("check1 ende, result: " + imageCheckerTest);

		//dann 5mal versuchen, TCP/IP tatsächlich per ImageCheck zu entdecken
		//eigentlich sollte schon der erste Versuch erfolgreich sein,
		//zur Sicherheit trotzdem mehrmals versuchen
		ImageChecker imageChecker = null;
		Test.output("check2 start");
        imageChecker = tcpIpDirectPlayChecker.waitUntilMatchFirst(5, 500);
        Test.output("check2 ende");

        if (imageChecker != null)
        {
            p = imageChecker.getMatchCenter();
            mouseClick(p.x, p.y);
            return;
        }
        Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
		Test.output("ImageChecker für TCP/IP fehlgeschlagen: " + screensize.getWidth() + "x" + screensize.getHeight()); //$NON-NLS-1$

        //benutze die bisherigen Varianten zur TCP/IP Erkennung
		int position = -1;
		i = 0;
		do
		{
			Thread.sleep(500);
			position = getYPositionOfTCPIP();
			i++;
		} while ((position == -1) && (i <= timeoutInSeconds()));

		if (i <= timeoutInSeconds())
		{
			mouseClick(324, position);
		}
		else if ((p = tcpIpVisibleChecker.getMatchCenter()) != null)
		{
		    //weitere Fallback-Lösung, falls am Anfang zumindest die Schriftfarbe
		    //erkannt wurde, klicke dort
	        Test.output("ImageChecker für TCP/IP fehlgeschlagen, klicke ersten Eintrag");
	        mouseClick(p.x, p.y);
	    }
	    else
	    {
	        throw new Exception(
				I18n.getString("GameStarter.ERROR_TIMEOUT_TCP_IP")); //$NON-NLS-1$
	    }*/
	}

	/**
	 * Ermittelt die Y Position des TCPIP Protokolls, wenn es angezeigt wird,
	 * @return Die Y Position, bei Scheitern -1
	 */
	private int getYPositionOfTCPIP()
	{
		int result = -1;
		for (int i = 127; i < 171; i++)
		{
			if (colorTCPIP.match(0, i) || colorTCPIP.match(1, i))
			{
				result = i;
				break;
			}
		}
		return result;
	}

	/**
	 * Simmuliert Tastatureingabe.
	 *
	 * @param keycode
	 *            Der Wert der Taste.
	 * @throws InterruptedException
	 */
	private void pressKey(int keycode) throws InterruptedException
	{
	    if (Thread.currentThread().isInterrupted())
	    {
	        throw new InterruptedException();
	    }
		robot.keyPress(keycode);
		robot.keyRelease(keycode);
	}

	/**
	 * Startet Siedler3, startet Multiplayer-LAN, wählt TCP/IP, gibt
	 * Spielernamen ein.
	 *
	 * @param playerName
	 *            Der einzugebende Spielername.
	 * @throws IOException
	 * @throws Exception
	 */
	private void readyForMultiplayer(String playerName) throws IOException, InterruptedException,
			Exception
	{
		startGame();
		// Button [Multiplayer-Spiel LAN]
		mouseClick(100, 280);
		try
		{
			//First make sure the language we have set is really the currently
			//used S3 language.
			Test.output("language: " + S3Language.getCurrentS3Language(settlersLobby.getS3Directory()));
			S3Language.setCurrentLanguage(S3Language.getCurrentS3Language(settlersLobby.getS3Directory()));
			chooseTCPIP();
		}
		catch (InterruptedException ie)
		{
		    //Die InterruptedException muss bis zum initialen Aufrufer
		    //weitergeworfen werden (createGame() oder joinGame())
		    throw ie;
		}
		catch (Exception e) {
		    Test.outputException(e);
			settlersLobby.error(I18n.getString("GameStarter.ERROR_NEW_VARIANT")); //$NON-NLS-1$
			chooseTCPIP_old();
		}
		if (!useSendInputNative)
		{
	        //falls CAPSLOCK an ist, muss es auf off gesetzt werden
	        //leider kann es nicht in die Funktion enterStringAt()
	        //verschoben werden, da die getLockingKeyState() Funktion
	        //Änderungen am CAPSLOCK Status nicht sofort liefert, somit würde sie
	        //länger true liefern als es tatsächlich gesetzt wäre
	        //Dies hier ist die Stelle bevor zum ersten Mal enterStringAt aufgerufen
	        //wird, daher wird hier überprüft
		    checkCapsLock();
		}
		else
		{
	        //caps lock is already checked when using sendInputNative, so no extra check required
	        //in this case.
		}
		enterStringAt(playerName, 457, 120, I18n.getString("GameStarter.PLAYERNAME")); //$NON-NLS-1$
	}

	/**
	 * Es wird gewartet, bis Spiele angezeigt werden, dann wird das erste Spiel
	 * geklickt. Es muss noch implementiert werden, dass bei Anzeige mehrerer
	 * Spiele die Routine abbricht, damit der Benutzer manuell das richtige
	 * Spiel auswählen kann, statt eventuell ins falsche beizutreten.
	 *
	 * @throws Exception
	 */
	private boolean chooseUniqueGame(StartOptions startOptions) throws Exception
	{
		// get textcolor as hexcolor to generate colored link in error-message if game opening fails
		String hexcolor = SettlersLobby.getHexColorStringByColor(settlersLobby.theme.getTextColor());
		Color colorOfGame = null;
		int i = 0;
		int ii;
		do
		{
			ii = 133;
			Thread.sleep(500);

            if (ProcessWindowSupportHandler.getInstance().isProcessWindowSupportActiveAndNotForegroundWindow())
            {
                // The window we want to check is not visible
                continue;
            }

			do
			{
				colorOfGame = robot.getPixelColor(ii, 327);
				ii++;
			} while (!(PixelColorChecker.match(colorOfGame, fontColor)) && (ii <= 200));
			i++;
		} while (!(PixelColorChecker.match(colorOfGame, fontColor)) && (i <= timeoutInSeconds()));
		if (i <= timeoutInSeconds())
		{
		    colorOfGame = null;
			Thread.sleep(WAIT_FOR_SECOND_GAME);

            if (ProcessWindowSupportHandler.getInstance().isProcessWindowSupportActiveAndNotForegroundWindow())
            {
                // The window we want to check is not visible anymore, abort
                return false;
            }

			ii = 133;
			do
			{
				colorOfGame = robot.getPixelColor(ii, 340);
				ii++;
			} while (!(PixelColorChecker.match(colorOfGame, fontColor)) && (ii <= 200));
			if (!(PixelColorChecker.match(colorOfGame, fontColor))) // Wenn kein 2. Spiel
			{
				mouseClick(135, 327);
				mouseClick(600, 570);
				return true;
			} else
			{
				return false;
			}
		} else
		{
            String ip = startOptions.getIp();
            if (ip.startsWith(ALobbyConstants.VPN_IP_RANGE)) {
                throw new Exception(
                        // set hexcolor as color for links in errortext
                        MessageFormat.format(I18n.getString("GameStarter.ERROR_TIMEOUT_OPEN_GAMES_VPN"),hexcolor)); //$NON-NLS-1$
            } else {
                throw new Exception(
                        // set hexcolor as color for links in errortext
                        MessageFormat.format(I18n.getString("GameStarter.ERROR_TIMEOUT_OPEN_GAMES"),hexcolor)); //$NON-NLS-1$
            }
		}
	}

	/**
	 * Siedler3 wird gestartet und bis zum Erreichen des Minichats
	 * ferngesteuert, außer bei Zeitüberschreitung.
	 * Es wird versucht an einem Spiel bei der übergebenen IP-Adresse
	 * (innerhalb der startOptions) teilzunehmen.
	 *
	 * @param startOptions, muss die IP-Adresse des Spielehostes und den
	 *     Spielernamen des Spielers enthalten.
	 * @param hostUserName Name des Hostes im Chat
	 * @throws Exception
	 */
	private synchronized void joinGameInternal(StartOptions startOptions, String hostUserName, String mapName, int goods, boolean league, int tournamentid, String tournamentname) throws Exception
	{
		if (!isRunning())
		{
		    setRunning(true);
			this.startOptions = startOptions;
			String ip = startOptions.getIp();
			//kein mapName zum joinen nötig
			createAllReferenceImagesAndCheckers(null);

			readyForMultiplayer(startOptions.getPlayerName());
			enterStringAt(ip, 457, 196, I18n.getString("GameStarter.IP_ADDRESS")); //$NON-NLS-1$
			mouseClick(150, 240); // Button [Suche Spiele]
			if (chooseUniqueGame(startOptions))
			{
				joinObserver = new JoinObserver(this, ip, hostUserName, mapName, goods, league, tournamentid, tournamentname);
				joinObserver.start();
			} else
			{
				setRunning(false);
			}
		}
	}

	/**
	 * siehe {@link #joinGame(StartOptions, String)}, nur als Hintergrund-Thread.
	 * Man sollte entweder nur die Threaded-Varianten oder nur die ohne
     * Thread benutzen.
     *
	 * @param startOptions startOptions, muss die IP-Adresse des Spielehostes und den
     *     Spielernamen des Spielers enthalten.
	 * @param hostUserName Name des Hostes im Chat
	 */
	public synchronized void joinGame(final StartOptions startOptions, final String hostUserName, final String mapName, final int goods, final boolean league, final int tournamentid, final String tournamentname )
	{
        if (starterThread != null && starterThread.isAlive())
        {
            //es läuft noch ein Thread
            return;
        }
        if (isRunning())
        {
            return;
        }

        Runnable runnable = new Runnable(){
            @Override
            public void run()
            {
                try
                {
                    joinGameInternal(startOptions, hostUserName, mapName, goods, league, tournamentid, tournamentname);
                    stopShowNoUserInteractionMessage();
                }
                catch (InterruptedException ie)
                {
                    //Der Thread wurde unterbrochen, isRunning zur Sicherheit auf false
                    //setzen
                    setRunning(false);
                    Thread.currentThread().interrupt();
                    Test.outputException(ie);
                    stopShowNoUserInteractionMessage();
                }
                catch (Exception e)
                {
                    setRunning(false);
                    Test.outputException(e);
                    stopShowNoUserInteractionMessage();
                    settlersLobby.error(e.getMessage());
                }
            }
        };
        starterThread = new Thread(runnable);
        starterThread.setName(starterThread.getName() + "-JoinGame"); //$NON-NLS-1$
        starterThread.start();
	}

	/**
     * Siedler3 wird gestartet und bis zum Erreichen der Kartenauswahl
     * ferngesteuert, außer bei Zeitüberschreitung.
     * Anschliessend wird versucht automatisch die Karte auszuwählen.
	 *
	 * @param startOptions muss mindestens playerName und mapName enthalten
	 * @throws Exception
	 */
	private synchronized void createGameInternal(StartOptions startOptions) throws Exception
	{
	    if (isRunning())
	    {
	        return;
	    }
	    setRunning(true);
	    this.startOptions = startOptions;
	    String mapName = startOptions.getMapName();
	    createAllReferenceImagesAndCheckers(mapName);

		readyForMultiplayer(startOptions.getPlayerName());
		mouseClick(700, 560); // Button [Neues Spiel]
		hostObserver = new HostObserver(this);
		hostObserver.start();

	    selectMap(mapName);
	}

	/**
	 * siehe {@link #createGameInternal(StartOptions)}, nur als Hintergrund-Thread.
	 * Man sollte entweder nur die Threaded-Varianten oder nur die ohne
	 * Thread benutzen.
	 *
	 * @param startOptions
	 */
	public synchronized void createGame(final StartOptions startOptions)
	{
	    if (starterThread != null && starterThread.isAlive())
	    {
	        //es läuft noch ein Thread
	        return;
	    }
        if (isRunning())
        {
            return;
        }

        Runnable runnable = new Runnable(){
            @Override
            public void run()
            {
                try
                {
                    createGameInternal(startOptions);
                    //aktuell sollte es reichen, stopShowNoUserInteractionMessage()
                    //erst hier aufzurufen und nicht schon in createGame(),
                    //da der Klick auf den Weiter Button die letzte Aktion in
                    //createGame() ist.
                    stopShowNoUserInteractionMessage();
                }
                catch (InterruptedException ie)
                {
                    Test.outputException(ie);
                    stopShowNoUserInteractionMessage();
                    //Der Thread wurde unterbrochen, isRunning zur Sicherheit auf false
                    //setzen
                    setRunning(false);
                    Thread.currentThread().interrupt();
                }
                catch (Exception e)
                {
                    Test.outputException(e);
                    setRunning(false);
                    stopShowNoUserInteractionMessage();
                    settlersLobby.error(e.getMessage());
                }
            }
        };
        starterThread = new Thread(runnable);
        starterThread.setName(starterThread.getName() + "-CreateGame"); //$NON-NLS-1$
        starterThread.start();
	}

    /**
     * Es wird darauf gewartet, dass der Kartenauswahlbildschirm
     * von S3 manuell vom User aktiviert wird. Sobald dies erkannt wird,
     * wird versucht automatisch die Karte auszuwählen.
     *
     * @param mapName Kartenname, wie er vom MapChoosingDialog geliefert wird
     * @throws Exception
     */
    public void selectMapStandalone(String mapName) throws Exception
    {
        if (waitUntilMapChoosingDialogIsVisible())
        {
            Test.output("Kartenauswahlfenster gefunden."); //$NON-NLS-1$
            selectMap(mapName);
        }
        else
        {
            if (!isAborted)
            {
                Test.output("Kartenauswahlfenster nicht gefunden"); //$NON-NLS-1$
                settlersLobby.error(I18n.getString("GameStarter.ERROR_MAP_SELECTION_WINDOW")); //$NON-NLS-1$
            }
            else
            {
                Test.output("gamestarter abort"); //$NON-NLS-1$
            }
        }
    }


	/**
	 * versucht die angegebene map automatisch auszuwählen.
	 * @param mapName
	 * @throws InterruptedException
	 */
	private void selectMap(String mapName) throws InterruptedException
	{
        MapType mapType = MapType.getValueFromMapName(mapName);
        Test.output("Debug Map-Starter: " + mapName + " ->type: " + mapType);
	    if (mapType == null)
	    {
	    	return;
	    }
	    if (mapType == MapType.SAVEGAME)
	    {
	        int index = mapName.indexOf('\\');
	        if (index != -1)
	        {
	        	String mapFileName = mapName.substring(index+1);
	        	if (SaveGame.isBackupSave(mapFileName))
	        	{
	        		mapName = mapName.substring(0, index+1+40); // +1 because of the index itself
	        	}
	        }
	        
	    }
	    s3Fontsize = determineS3FontSizeUsingImage(mapName);
	    //falls die neue Variante fehlschlägt, probiere die alte
	    Test.output("s3FontSize: " + s3Fontsize.fontsize); 
	    if (s3Fontsize == null)
	    {
	        s3Fontsize = determineS3FontSize();
	        selectMapType(MapType.RANDOM); //zurück auf Random wechseln
	        Thread.sleep(100); //100 ms Pause sollten reichen, um die Ansicht
	        //zu aktualisieren
	    }

	    if (mapType == MapType.MULTI || mapType == MapType.USER ||
	        mapType == MapType.SAVEGAME)
	    {
	        // der folgende Abschnitt war ursprünglichen nur für MULTI
	        // und USER maps gedacht, funktioniert durch ein paar wenige
	        // Anpassungen auf für Savegames. Wenn im Folgenden also
	        // von einer Karte die Rede ist, kann das meist auch ein
	        // Savegame anstatt einer Karte sein.
	        // mapName bezeichnet dann keine Karte, sondern ein Savegame
            if (ProcessWindowSupportHandler.getInstance().isProcessWindowSupportActiveAndNotForegroundWindow())
            {
                // The window where we want to click is not visible anymore, abort
                return;
            }


	        // Karte aus der Liste auswählen
	        // zuerst wird sicherheitshalber nochmal auf Random selektiert
	        selectMapType(MapType.RANDOM);
            // die Farbe vom gedrückten Zufallskarten button holen
            Color c = robot.getPixelColor(121, 136);
            selectMapType(mapType);
            //Es muß gewartet werden, bis S3 die Anzeige aktualisiert,
            //ansonsten gibt es (warum auch immer) einen runtime Error.
            //Sie ist aktualisiert, sobald der Zufallskarten button wieder
            //oben ist (die Farbe wechselt von dunkel nach hell).
            //maximal wird 100*100ms gewartet, das sollte immer reichen
            int i = 0;
            while(PixelColorChecker.match(robot.getPixelColor(121, 136), c) && i < 100)
            {
                i++;
                Thread.sleep(100);
            }
            if (ProcessWindowSupportHandler.getInstance().isProcessWindowSupportActiveAndNotForegroundWindow())
            {
                // The window where we want to click is not visible anymore, abort
                return;
            }

	        if (s3Fontsize == null)
	        {
	            return;
	        }
	        final int gamesOnPage = s3Fontsize.getNumberOfGamesOnPage();
            if (gamesOnPage <= 0)
            {
                //error
                return;
            }
            String[] sortedMaps = mapType.getMaps(settlersLobby);
            Arrays.sort(sortedMaps); //mit sort wird so sortiert wie in S3
            
    	    int index = getLocalMapIndex(mapName, sortedMaps);
    	    if (index == -1)
    	    {
    	        //Karte nicht gefunden?!?
    	    	Test.output("mapName " + mapName + " not found in sortedMaps.");
    	        return;
    	    }

            //berechne den MittelIndex.
            //der MittelIndex ist der Index der Karte, die in der obersten
            //Zeile angezeigt wird, wenn man genau in die Mitte
            //des Scrollbalkens klickt (y = 257 + 85)
            int size = sortedMaps.length;
            int middleIndex = size / 2;
            //der Index (size/2) wird genau in der Mitte des Kartenauswahlfensters
            //dargestellt. Uns interessiert aber der Index am Anfang des Fensters
            middleIndex = middleIndex - gamesOnPage/2;

            int topIndex;
            int bottomIndex;

            //mouseClick(233,257); //Scrollbalken Anfang
            //mouseClick(233,427); //Scrollbalken Ende
            //mouseClick(233,257 + 85); //Scrollbalken Mitte

            //solange der Mittelindex nicht größer ist als die Anzahl der
            //Spiele pro Seite, zählt man einfach von vorne durch.
            //In dem Fall ist nämlich kein (oder nur ein kurzer) Scrollbalken
            //vorhanden, so daß die verschiedenen Varianten eventuell nicht funktionieren.
            //Ist (mapSelectFastVariant==false), wird ebenfalls grundsätzlich von
            //ganz oben beginnend gesucht.
            if (middleIndex < gamesOnPage || mapSelectFastVariant == false)
            {
                topIndex = 0;
                bottomIndex = topIndex + (gamesOnPage - 1);
            }
            else
            {
                // es wird ermittelt, ob der Index im 1., 2., 3. oder 4.
                // Viertel liegt. Je nachdem wird der Startpunkt
                // auf den Anfang, die Mitte oder das Ende gesetzt
                // Die eigentliche Suchfunktion verschiebt dann von diesem
                // Startpunkt aus das Kartenauswahlfenster entweder nach oben
                // oder unten, um den Index der gewünschten Karte zu erreichen
                if (index < middleIndex)
                {
                    //Suchen in der er oberen Hälfte
                    if (index <= (middleIndex + 1) / 2)
                    {
                        //1. Viertel, von oben nach unten suchen
                        topIndex = 0;
                        bottomIndex = topIndex + (gamesOnPage - 1);
                    }
                    else
                    {
                        //2. Viertel, von der Mitte aus nach oben suchen
                        topIndex = middleIndex;
                        bottomIndex = topIndex + (gamesOnPage - 1);
                        mouseClick(233, 257 + 85); //Scrollbalken Mitte
                        //robot.delay(1000);
                    }
                }
                else
                {
                    //Suchen in der unteren Hälfte
                    int numberOfElementsInSecondHalf = size - middleIndex;
                    if (index <= middleIndex + ((numberOfElementsInSecondHalf + 1 ) / 2))
                    {
                        //3. Viertel, von der Mitte aus nach unten suchen
                        topIndex = middleIndex;
                        bottomIndex = topIndex + (gamesOnPage - 1);
                        mouseClick(233, 257 + 85); //Scrollbalken Mitte
                        //robot.delay(1000);
                    }
                    else
                    {
                        //4. Viertel, von unten aus nach oben suchen
                        //der grösste gültige Index ist (size - 1)
                        bottomIndex = (size - 1);
                        topIndex = bottomIndex - (gamesOnPage - 1);
                        mouseClick(233, 427); //Scrollbalken Ende
                        //robot.delay(1000);
                        //ab dem Wechsel von 1204 maps zu 1205
                        //landet man bei einem Klick auf das Ende auf einmal
                        //nicht mehr bei der letzten Karte, sondern einen davor.
                        //Alle 172 Schritte fehlt dann eine weitere map zum Ende hin
                        //(zumindest bei normaler Schriftgrösse)
                        //daher wird jetzt versucht diesen Fehler zu errechnen und
                        //entsprechend auszugleichen
                        //die 172 entsteht eventuell aus den 170 pixeln,
                        //die man beim Scrollbalken effektiv klicken kann
                        //und jeweils ein Pixel drüber und drunter für den
                        //minimalen scrollKnopf, der 3 pixel groß ist
                        int amount = (size + 171) / 172;
                        //Annahme: es hängt irgendwie damit zusammen, wieviele
                        //Zeilen im Fenster zu sehen sind
                        //auf jeden Fall muß man bei normaler Schriftgröße 7 abziehen
                        //und bei großer Schriftgröße 6
                        // mit ((gamesOnPage+1)/2) passt es im Moment
                        amount = amount - ((gamesOnPage+1)/2);
                        amount = Math.max(amount, 0); //nicht kleiner als 0
                        topIndex -= amount;
                        bottomIndex -= amount;
                        if (amount > 0)
                        {
                            Test.output("Korrekturindex: " + (-amount)); //$NON-NLS-1$
                        }
                    }
                }
            }
            moveMapSelectionWindowToIndexAndSelectMap(
                    index, topIndex, bottomIndex, mapName);
            //Ende MULTI- und USER- und Savegame- Abhandlung
	    }
	    else
	    {
            selectMapType(mapType);
            if (mapType == MapType.RANDOM)
            {
            	selectRandomMap(mapName);
            	selectGoodsInStock();
            	// select wimo-modus if its activated
                selectWiMoOption();
            	stopShowNoUserInteractionMessageInMapSettingsWindow();
            }
        }
	}

	/**
	 * Wählt die entsprechende Random Map aus.
	 * @param mapName Der Kartenname.
	 * @throws InterruptedException
	 */
	private void selectRandomMap(String mapName) throws InterruptedException
	{
	    int yOffset=0;

     	mouseClick(574, 156);
     	S3RandomMap map = new S3RandomMap(mapName);
     	if (!map.isValid())
     	{
     	    return;
     	}

		if (map.getSize().equals(S3RandomMap.RANDOM_MAP_SIZE_0))
		{
		    yOffset=0;
		} else if (map.getSize().equals(S3RandomMap.RANDOM_MAP_SIZE_1))
		{
		    yOffset=1;
		} else if (map.getSize().equals(S3RandomMap.RANDOM_MAP_SIZE_2))
		{
			yOffset=2;
		}  else if (map.getSize().equals(S3RandomMap.RANDOM_MAP_SIZE_3))
		{
			yOffset=3;
		}  else if (map.getSize().equals(S3RandomMap.RANDOM_MAP_SIZE_4))
		{
			yOffset=4;
		}  else if (map.getSize().equals(S3RandomMap.RANDOM_MAP_SIZE_5))
		{
			yOffset=5;
		}  else if (map.getSize().equals(S3RandomMap.RANDOM_MAP_SIZE_6))
		{
			yOffset=6;
		}

        int y = 170; //erste Zeile, oberstes pixel
        //dem yOffset entsprechend viele Zeilen tiefer klicken
        y = y + yOffset * s3Fontsize.getFontsize();
        //Mitte des Textes
        y = y + (s3Fontsize.getFontsize() / 2);
        //robot.mouseMove(430, y);robot.delay(3000);
        fastMouseClick(430, y);


		fastMouseClick(574, 196);
		if (map.isMirrorNone())
		{
		    yOffset=0;
		} else if (map.isMirrorXAxis())
		{
			yOffset=1;
		} else if (map.isMirrorYAxis())
		{
			yOffset=2;
		} else if (map.isMirrorBoth())
		{
			yOffset=3;
		}
        y = 210; //erste Zeile, oberstes pixel
        //dem yOffset entsprechend viele Zeilen tiefer klicken
        y = y + yOffset * s3Fontsize.getFontsize();
        //Mitte des Textes
        y = y + (s3Fontsize.getFontsize() / 2);
        //robot.mouseMove(430, y);robot.delay(3000);
        fastMouseClick(430, y);

		fastMouseClick(629, 468); //OK
		robot.mouseMove(305, 491); //Teameinstellungen
	}

	/**
	 * Bewegt das Kartenauswahlfenster, das aktuell die Karten von topIndex bis
	 * bottomIndex anzeigt, solange nach oben oder unten, bis die Karte mit Index
	 * <code>index<code> im Bereich von topIndex bis bottomIndex liegt und damit im
	 * Fenster erscheint. Anschliessend wird die Karte ausgewählt.
	 * s3Fontsize wird benötigt, um aus dem Zeilenoffset zwischen topIndex und bottomIndex
	 * den entsprechenden Offset in Bildschirmpixeln zu erhalten.
	 *
	 * @param index
	 * @param topIndex
	 * @param bottomIndex
	 * @param mapName
	 * @throws InterruptedException
	 */
	private void moveMapSelectionWindowToIndexAndSelectMap(int index, int topIndex,
	        int bottomIndex, String mapName) throws InterruptedException
	{
	    if (s3Fontsize == null)
	    {
	        return;
	    }
	    final Color buttonColor = new Color(123,121,123);
	    if (index < 0 || topIndex < 0 || bottomIndex < 0 || topIndex > bottomIndex)
	    {
	        //Indizes müssen positiv sein und topIndex kleiner als bottomIndex
	        throw new IllegalArgumentException();
	    }
	    int numberOfClicks;
	    int yOffset = 0;
	    if (index < topIndex)
	    {
	        //search upwards
            Test.output("top: " + topIndex + ", bottom: " + bottomIndex +  //$NON-NLS-1$ //$NON-NLS-2$
                    ", search upwards"); //$NON-NLS-1$

	        //der index liegt oberhalb von indexTop
            //solange nach oben klicken, bis indexTop den Index erreicht
            numberOfClicks = topIndex - index;
            if (numberOfClicks > 0)
            {
                if(!doNumberOfClicksAt(numberOfClicks, 234, 250, buttonColor))
                {
                    return;
                }
                //jeder Klick verschiebt den sichtbaren Index um eins nach oben
                topIndex -= numberOfClicks;
                bottomIndex -= numberOfClicks;
            }
            yOffset = index - topIndex; //Zeilenoffset relativ zur ersten sichtbaren Zeile

            //die Berechnung des yOffset kann theoretisch entfallen, da er immer 0 ist,
            //solange man nur für (index < topIndex) in diesen Zweig springt.
            //durch die offset Berechnung wäre der Zweig sogar bis (index <= bottomIndex)
            //gültig. Auch aus Gründen der Symmetrie zum else-Zweig wird sie beibehalten.
	    }
	    else
	    {
	        //search downwards
            Test.output("top: " + topIndex + ", bottom: " + bottomIndex +  //$NON-NLS-1$ //$NON-NLS-2$
                    ", search downwards"); //$NON-NLS-1$

            // Für alle Karten, die unterhalb von
            // bottomIndex liegen, müssen wir entsprechend oft
            // auf den Pfeil nach unten klicken
	        // Liegt der index im Bereich von topIndex und bottomIndex,
	        // so wird gar nicht nach unten geklickt, sondern nur der
	        // offset zur ersten Zeile berechnet und entsprechend geklickt
	        numberOfClicks = index - bottomIndex;
            if (numberOfClicks > 0)
            {
                if (!doNumberOfClicksAt(numberOfClicks, 234, 434, buttonColor))
                {
                    return;
                }
                //jeder Klick verschiebt den sichtbaren Index um eins nach unten
                topIndex += numberOfClicks;
                bottomIndex += numberOfClicks;
            }
            yOffset = index - topIndex; //Zeilenoffset relativ zur ersten sichtbaren Zeile
	    }
        int y = 256; //erste Zeile
        //dem yOffset entsprechend viele Zeilen tiefer klicken
        y = y + yOffset * s3Fontsize.getFontsize();

        T2<Boolean, Point> result = checkMapNameVisible(mapName, y, s3Fontsize.getFontsize());
        boolean isVisible = result.e1;
        Point clickPoint = result.e2;

        if (!isVisible)
        {
            //die Karte hätte gefunden werden müssen, das ist aber nicht passiert, Abbruch.
            return;
        }
        //Karte auswählen
        if (clickPoint != null)
        {
            mouseClick(clickPoint.x, clickPoint.y);
        }
        else
        {
            mouseClick(131, y);
        }

        //robot.delay(1000);
        mouseClick(600, 465); //OK
        robot.mouseMove(700, 565); //schonmal auf Weiter setzen, der Optik wegen
        if (mapName.startsWith(ALobbyConstants.LABEL_SAVE_GAME))
        {
            //bei savegames landet man direkt im minichat
            return;
        }

        waitUntilMapChoosingDialogIsInvisible();
        clickRandomPosis();
        selectGoodsInStock();
        
        // select wimo-modus if its activated
        selectWiMoOption();

        //stopShowNoUserInteractionMessage muss hier extra nochmal aufgerufen
        //werden, da es sonst Probleme mit der Anzeige gibt.
        stopShowNoUserInteractionMessageInMapSettingsWindow();

        robot.mouseMove(700, 565); //Weiter
        clickToMiniChat();
	}

    private boolean waitUntilMapChoosingDialogIsVisible() throws InterruptedException
    {
        //4 Ecken von S3:
        PixelColorChecker ready = new PixelColorChecker(robot, 181, 199, 16, 0, 0);
        ready.add(206, 235, 74, 799, 0);
        ready.add(123, 158, 8, 0, 599);
        ready.add(99, 125, 148, 799, 599);
        //4 Ecken vom eigentlichen Kartenauswahlfenster
        ready.add(173, 195, 206, 101, 101);
        ready.add(156, 170, 173, 698, 101);
        ready.add(132, 138, 132, 101, 498);
        ready.add(0, 8, 8, 698, 498);
        //ein zusätzlicher Punkt im Fenster
        ready.add(239, 231, 107, 119, 108);

        return waitUntilPixelColorMatch(ready, timeoutInSeconds() * 4, 500);
    }

	private void waitUntilMapChoosingDialogIsInvisible() throws InterruptedException
	{
		PixelColorChecker ready = new PixelColorChecker(robot, 0, 28, 49, 298, 453);
		for (int i = 0; !ready.match() && i < timeoutInSeconds()*2; i++)
		{
		    Thread.sleep(500);
		}
	}

	/**
	 * Wenn der Benutzer "bis Minichat navigieren" gewählt hat, wird auf den
	 * "Weiter" Button geklickt.
	 * @throws InterruptedException
	 */
	private void clickToMiniChat() throws InterruptedException
	{
		if (startOptions.isSelectMiniChat())
		{
		    //kurze Pause, ansonsten wird die Auswahl für den Warenbestand
		    //oder randomposis eventuell nicht schnell genug übernommen
	        Thread.sleep(100);
			fastMouseClick(700, 565);
		}
	}

	/**
	 * Wenn der Benutzer Zufallspositionen gewählt hat, werden die Team
	 * decrement und wieder incrementiert, sodass die Kartenvoreinstellungen
	 * rausgenommen sind.
	 * @throws InterruptedException
	 */
	private void clickRandomPosis() throws InterruptedException
	{
		if (startOptions.isSelectRandomPosis())
		{
			fastMouseClick(305, 468); // decrement Teamanzahl
			fastMouseClick(305, 459); // increment Teamanzahl
		}
	}

	/**
	 * wählt als Warenbestand goodsInStock aus. Es wird vorausgesetzt, dass der
	 * Bildschirm mit den Karteneinstellungen von S3 bereits zu sehen ist.
	 * @throws InterruptedException
	 *
	 */
	private void selectGoodsInStock() throws InterruptedException
	{
	    GoodsInStock selectGoodsInStock = startOptions.getSelectGoodsInStock();
        if (selectGoodsInStock == null || s3Fontsize == null)
	    {
	        return;
	    }
	    if (selectGoodsInStock == GoodsInStock.Vorgabe)
	    {
	        //Vorgabe ist initial ausgewählt, in dem Fall ist nichts zu tun
	        return;
	    }
	    //aktuelle Mausposition sichern
	    Point currentPosition = getCurrentMousePosition();

	    int index = selectGoodsInStock.getIndex();
	    // überprüfe ein Pixel von den Hoch-Runter Pfeilen,
	    // falls die Farbe passt, gehts weiter
	    Point point = new Point(373, 240);
	    PixelColorChecker arrows = new PixelColorChecker(robot, 123, 121, 123,
	            point.x, point.y);
	    if (!waitUntilPixelColorMatch(arrows, timeoutInSeconds(), 100))
	    {
	        Test.output("Warenbestand konnte nicht ausgewählt werden"); //$NON-NLS-1$
	        return;
	    }
	    mouseClick(point.x, point.y);
	    //obere Ecke des Fensters für den Warenbestand (Vorgabe, Wenig, Mittel, Viel)
	    Point start = new Point(148, 262);

	    // von der oberen Ecke muss man index viele Zeilen nach
	    // unten gehen (dann steht man auf der obersten Pixelzeile
	    // der Schriftzeile), von dort dann nochmal die halbe Fontgröße,
	    // um in etwa die Mitte der Schrift zu treffen
	    int y = start.y + index * s3Fontsize.getFontsize()
	        + s3Fontsize.getFontsize() / 2;

	    mouseClick(start.x + 20, y);
	    // Maus zurück auf den Ursprungspunkt
	    robot.mouseMove(currentPosition.x, currentPosition.y);
	}

	/**
	 * überprüft ob der Kartenname im Kartenauswahlbildschirm bei der übergebenen
	 * y-Koordinate gefunden werden kann.
	 *
	 * @param mapName
	 * @param y
	 * @param fontSize
	 * @return <true, Point> falls die Karte gefunden wurde,
	 * <false, null> falls der Font bekannt ist (ist jetzt eigentlich immer das Fall),
	 * aber die Karte nicht gefunden wurde
	 * (in diesem Fall hätte sie definitiv gefunden werden müssen).
	 * @throws InterruptedException
	 */
	private T2<Boolean, Point> checkMapNameVisible(String mapName, int y, int fontSize) throws InterruptedException
    {
	    mapName = getVisibleMapName(mapName);

        ImageChecker mapSelect = new ImageChecker(robot, 123, y - fontSize, 100, 2 * fontSize,
                mapName, false);
        mapSelect.setScreenSizeS3Menu();
        //es muß gewartet werden, bis S3 alle Klicks tatsächlich durchgeführt hat,
        //andernfalls wird an der falschen Stellt gesucht. Leider läßt sich dies
        //nicht abfragen, daher wird eine feste Zeitspanne gewartet.
        //10 ms reichen auf einem schnellen Rechner.
        Thread.sleep(100);

        //liefert true, wenn der mapname gefunden wurde
        // es wird 20 mal versucht, da es scheinbar immernoch timing
        // Probleme gibt, wenn geklickt wird
        boolean isMatch = ImageChecker.waitUntilImageMatch(mapSelect, 20, 100);

        Point point = mapSelect.getMatchCenter();
        Test.output(point + ", " + 131 + "|" + y + ", " + isMatch); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        return T2.t2(isMatch, point);
    }

	/**
	 * Klickt an x/y {@code numberOfClicks}-mal. Vor jedem Klick wird überprüft,
	 * ob dort die Farbe {@code color} zu sehen ist, andernfalls wird abgebrochen und
	 * {@code false} zurückgegeben.
	 *
	 * @param numberOfClicks Anzahl Klicks.
	 * @param x
	 * @param y
	 * @param color Die Farbe, die an x/y zu sehen sein muß, andernfalls wird abgebrochen.
	 * @return true falls alle Klicks ausgeführt wurden, sonst false (in dem Fall
	 * stimmte die Farbe nicht mehr überein).
	 * @throws InterruptedException
	 */
	private boolean doNumberOfClicksAt(int numberOfClicks, int x, int y, Color color) throws InterruptedException
	{
	    boolean result = true;
	    PixelColorChecker checker = new PixelColorChecker(robot, color.getRed(),
	            color.getGreen(), color.getBlue(), x, y);
        if (mapSelectDelayAfterClick > 0)
        {
            for (int i = 0; i < numberOfClicks; ++i)
            {
                //auch hier timing Probleme, mehrmals versuchen die Farbe zu erkennen
                if (!waitUntilPixelColorMatch(checker, 100, 10))
                {
                    result = false;
                    Test.output("abort clicks: "  + i + "/" + numberOfClicks); //$NON-NLS-1$ //$NON-NLS-2$
                    break;
                }
                fastMouseClickAndDelay(x, y, mapSelectDelayAfterClick);
            }
        }
        else
        {
            for (int i = 0; i < numberOfClicks; ++i)
            {
                //auch hier timing Probleme, mehrmals versuchen die Farbe zu erkennen
                if (!waitUntilPixelColorMatch(checker, 100, 10))
                {
                    Test.output("abort clicks: "  + i + "/" + numberOfClicks); //$NON-NLS-1$ //$NON-NLS-2$
                    result = false;
                    break;
                }
                fastMouseClick(x, y);
            }
        }
        return result;
	}

	/**
	 * Klickt an x/y {@code numberOfClicks}-mal.
	 * @param numberOfClicks
	 * @param x
	 * @param y
	 * @throws InterruptedException
	 */
	@SuppressWarnings("unused")
    private void doNumberOfClicksAt(int numberOfClicks, int x, int y) throws InterruptedException
	{
	    //aus Performancegründen wird die If-Abfrage
	    //einmal vor der Schleife gemacht
	    //und nicht jedesmal innerhalb.
	    //hier geht es prinzipiell um mehrere hundert Klicks
        if (mapSelectDelayAfterClick > 0)
        {
            for (int i = 0; i < numberOfClicks; ++i)
            {
                fastMouseClickAndDelay(x, y, mapSelectDelayAfterClick);
            }
        }
        else
        {
            for (int i = 0; i < numberOfClicks; ++i)
            {
                fastMouseClick(x, y);
            }
        }
	}

	/**
	 * sucht im Kartenauswahlbildschirm nach leeren Zeilen.
	 * Startend von xStart/y wird nach rechts gesucht, ob dort
	 * irgendwo die Standardschriftfarbe auftaucht. Falls ja
	 * ist die (Pixel-)Zeile nicht leer.
	 *
	 * @param xStart
	 * @param y
	 * @return true wenn die Schriftfarbe in der aktuellen PixelZeile nicht
	 * gefunden wurde
	 */
	private boolean isEmptyPixelLine(int xStart, int y)
	{
	    Color colorOfGame;
        int x = xStart - 1;
        final int xMax = 200;
        boolean matchFontColor = false;

        if (ProcessWindowSupportHandler.getInstance().isProcessWindowSupportActiveAndNotForegroundWindow())
        {
            // The window we want to check is not visible anymore, abort
            return false;
        }

        do
        {
            x++;
            colorOfGame = robot.getPixelColor(x, y);
            matchFontColor = PixelColorChecker.match(colorOfGame, fontColor);
        } while ((!matchFontColor) && (x <= xMax));
        //wenn der StandardSchriftFarbe nicht entdeckt wurde, ist die Zeile leer
        return !matchFontColor;
	}

	/**
	 * ermittelt die Fontgröße anhand des erzeugten ReferenzeImages
	 * für den mapnamen (dieses hat nämlich als Höhe genau die Fontgröße).
	 * Zusätzlich wird versucht zu berechnen, wieviele Karten/Zeilen in der
	 * Mapauswahl sichtbar sind.
	 *
	 * @param mapName
	 * @return
	 */
	private S3FontSize determineS3FontSizeUsingImage(String mapName)
	{
	    S3FontSize result;
        mapName = getVisibleMapName(mapName);

        BufferedImage image = ReferenceImageHandling.getReferenceImage(mapName, false);
        if (image == null)
        {
            return null;
        }
        int fontSize = image.getHeight();
        final int yStart = 248;
        final int yMax = 436; //gehört nicht mehr zum Fenster
        final int diff = yMax - yStart;
        // soviele Zeilen sind zu sehen, eine letzte nicht vollständige wird
        // nicht mitgezählt
        int amount = diff  / fontSize;

        result = new S3FontSize(fontSize, amount);
        Test.output("fontsize: " + fontSize + ", " + amount); //$NON-NLS-1$ //$NON-NLS-2$

        return result;
	}

	/**
	 * versucht im Kartenauswahlbildschirm die Fontgröße zu ermitteln.
	 * Dazu wird zuerst Kategorie Multi selektiert und anschliessend versucht die
	 * Anzahl der sichtbaren Zeilen zu ermitteln. Dieser Wert ist notwendig um die Auswahl
	 * später an die richtige Stelle bewegen zu können.
	 *
	 * @return
	 * @throws InterruptedException
	 */
	private S3FontSize determineS3FontSize() throws InterruptedException
	{
	    S3FontSize result = null;
	    //Multi selektieren um herauszubekommen, wieviele maps auf dem
	    //screen angezeigt werden können
	    //Die maps im Multiverzeichnis wurden hoffentlich nicht verändert,
	    //so daß sie eine gute Erkennungsmöglichkeit liefern.
	    //Gemessen wird vom oberen Ende der Großbuchstaben bzw. Zahlen (wie z.B. ABC123)
	    //bis zum nächsten oberen Ende. Oberhalb der Großbuchstaben liegen
	    //nur die Punkte von Ä,Ö,Ü, diese sollten in Mapnamen nicht vorkommen,
	    //so daß hoffentlich zumindest diese leere Zeile entdeckt wird
	    //und als Trennung zwischen zwei Schriftzeilen erkannt wird.
	    //Dies klappt bei MS Sans Serif perfekt.
	    //Leider gibt es Schriften, in denen einige Kleinbuchstaben höher
	    //sind als die Großbuchstaben (z.B. Verdana, dort ist der I-Punkt
	    //über den Großbuchstaben), aber bei bisherigen Tests wurde trotzdem
	    //im Mittel über die ganzen Einträge die passende Größe erkannt.
	    //Sollte jemand aber maps mit ÄÖÜ ins Multiverzeichnis kopieren oder
	    //eine map, die fast nur aus Unterstrichen besteht bzw. einem Namen,
	    //der nur Kleinbuchstaben besteht, die eine geringe Höhe haben, so
	    //wird die Berechnung vermutlich fehlschlagen.

	    selectMapType(MapType.MULTI);

	    boolean isEmptyLine;
	    int counter = 0;
	    final int yStart = 248;
	    final int yMax = 436; //gehört nicht mehr zum Fenster

	    //Warten bis Bildschirm aktualisiert ist, damit die Erkennung starten kann
	    //bei y = 254 sollte egal mit welcher Schriftgröße die Standardschrift zu
	    //finden sein
	    do
	    {
	        isEmptyLine = isEmptyPixelLine(124, 254);
	        counter++;
	        Thread.sleep(100);
	    } while (isEmptyLine && counter < timeoutInSeconds()*2);

	    if (isEmptyLine)
	    {
	        //StandardFarbe nicht entdeckt, Abbruch
	        return null;
	    }

	    //start soll die oberste Pixelzeile eines Kartennamens
	    //enthalten, end die unterste Pixelzeile
	    //(end zeigt niemals auf eine leere Pixelzeile)
	    List<Integer> start = new ArrayList<Integer>();
	    List<Integer> end = new ArrayList<Integer>();
        int y = yStart;
        int xStart = 124;
        do
        {
            //solange suchen, bis die erste nichtleere Pixelzeile
            //gefunden wird.
            isEmptyLine = isEmptyPixelLine(xStart, y++);
            if (!isEmptyLine)
            {
                //Zeile ist nicht leer, oberes Ende des Kartennamens
                start.add(y - 1);
                //Ende wird gleich mit demselben Wert initialisiert.
                //sollte direkt darauf eine leere Pixelzeile folgen
                //wird dieser Eintrag später komplett gelöscht
                end.add(y - 1);
                while (y < 436)
                {
                    isEmptyLine = isEmptyPixelLine(xStart, y++);
                    if (isEmptyLine)
                    {
                        //leere Pixelzeile gefunden,
                        //der in der vorigen iteration geschriebene Wert
                        //für "end" is korrekt
                        //Abbruch der inneren Schleife
                        break;
                    }
                    else
                    {
                        //noch keine leere Pixelzeile gefunden,
                        //also ist das Ende frühestens in dieser Pixelzeile
                        end.set(end.size() - 1, y - 1);
                    }
                }
                //wenn man hier ankommt, wurden die Werte für die letzte erkannte
                //Schriftzeile in "start" und "end" gespeichert.
                //die letzte Zeile war leer (oder y >= yMax, dann eh Abbruch)
                //somit kann mit der Erkennung einer weiteren Schriftzeile
                //fortgefahren werden.
            }
        } while (y < yMax);

        for (int i = start.size() - 1; i >= 0; --i)
        {
            //es gibt keine Zeilen, die nur aus 1 oder 2 pixel hohen Buchstaben
            //bestehen. In dem Fall hat man eher die Punkte von den Umlauten oder
            //einen Unterstrich gefunden, ignorieren
            if (end.get(i) - start.get(i) < 2)
            {
                start.remove(i);
                end.remove(i);
            }
        }

//        for (int i = 0; i < start.size() - 1; ++i)
//        {
//            Test.output(start.get(i) + "/" + end.get(i) + "=" + (start.get(i + 1) - start.get(i)));
//        }

        if (start.size() > 1)
        {
            int sum = 0;
            //Es wird ein Durchschnittswert über die ermittelten
            //Fontgrößen berechnet. Dieser ergibt hoffentlich
            //die passende Größe
            for (int i = 0; i < start.size() - 1; ++i)
            {
                sum += start.get(i + 1) - start.get(i);
            }
            Test.output("fontSize: " + (sum / (double) (start.size() - 1))); //$NON-NLS-1$
            int fontSize = (int) Math.round((sum / (double) (start.size() - 1)));

            int amount = start.size();
            if (436 - end.get(end.size() - 1) < fontSize / 2 )
            {
                //letzte Schriftzeile kaum sichtbar, nicht mitzählen
                //TODO: eventuell wird sogar < fontSize benötigt
                amount--;
            }
            //Gegenrechnung, wenn alles glatt ging, müßte
            // fontsize * amount ungefähr so groß sein
            // wie das komplette Fenster
            int windowHeight = yMax - yStart;
            if (Math.abs(windowHeight - fontSize * amount) < fontSize)
            {
                result = new S3FontSize(fontSize, amount);
                Test.output("fontsize: " + fontSize + ", " + amount); //$NON-NLS-1$ //$NON-NLS-2$
            }
            else
            {
                Test.output("Fehler in fontSize Berechnung"); //$NON-NLS-1$
            }
        }
        return result;
	}

	/**
	 * klickt auf die entsprechende Kategorie im Kartenauswahlbildschirm:
     * Random, Multi, User, Save.
	 * @param mapType
	 * @throws InterruptedException
	 */
	private void selectMapType(MapType mapType) throws InterruptedException
	{
	    switch(mapType)
	    {
	        case MULTI:
	            mouseClick(130, 175);
	            break;
	        case USER:
                mouseClick(130, 200);
                break;
	        case SAVEGAME:
                mouseClick(130, 225);
                break;
	        case RANDOM:
	            mouseClick(130, 135);
	        default:
	            break; //nichts zu tun
	    }
	}

	/**
	 * ermittelt den Index für die übergebene Map im lokalen
	 * Verzeichnis.
	 *
	 * @param mapName
	 * @param mapType
	 * @return lokalen index
	 */
	private int getLocalMapIndex(String mapName, String[] sortedMaps)
	{
        int index = mapName.indexOf('\\');
        if (index == -1)
        {
            return -1;
        }
        String mapFileName = mapName.substring(index+1);
        int resultIndex;
        boolean found = false;
        for (resultIndex = 0; resultIndex < sortedMaps.length; resultIndex++)
        {
            if (mapFileName.equals(sortedMaps[resultIndex]))
            {
                found = true;
                break;
            }
        }
        if (found)
        {
            Test.output("index: " + resultIndex + ", " + mapName); //$NON-NLS-1$ //$NON-NLS-2$
            return resultIndex;
        }
        return -1;
	}

	private void stopObserve(ObserverFramework observer)
	{
		if (observer != null)
		{
		    if (Thread.currentThread() == observer)
		    {
		        //if the host/joinObserver detects that the S3 game itself was started
		        //it calls settlersLobby.actionGameStarted, which will finally
		        //call stopGame(), which generally stops all threads, as it can be called
		        //in different situations.
		        //If the calling thread is the observer thread, no interrption is required,
		        //as this can only happen when settlersLobby.actionGameStarted was called
		        //from the observer thread, i.e. the thread is already outside the while
		        //loop and will exit normally as soon as we return from here.
		        return;
		    }
		    //versuche mehrmals interrupt aufzurufen, da es gelegentlich
		    //vorkommt, dass der imagechecker die InterruptedException schluckt
		    //zehnmal hintereinander sollte das niemals passieren
		    for (int i = 0; i < 10 && observer.isAlive(); ++i)
		    {
		        observer.interrupt();
    			try
    			{
    				observer.join(10);
    				if (i > 0)
    				{
    				    Test.output("stop observe, i=" +i);
    				}
    			}
    			catch (InterruptedException e)
    			{
    			    Test.outputException(e);
                    Thread.currentThread().interrupt();
    			}
		    }
		}
	}

	/**
	 * unterbricht den {@code #starterThread}.
	 */
	private void stopStarterThread()
	{
	    // wenn S3 beendet wird, während der gamestarter noch läuft,
	    // muss auch der gamestarter unterbrochen werden, damit nicht
	    // irgendwelche unsinnigen Mausklicks in fremde Fenster durchgeführt
	    // werden.
	    // Unterbrochen wird mit dem klassischen Thread.interrupt().
	    // Dabei kann es allerdings vorkommen, dass sich der Thread aktuell in
	    // robot.delay() befindet (wird leider intern selbst mit autodelay=0 nach jedem
	    // Mausklick/Tastatureingabe aufgerufen), dort wird aber jegliche
	    // InterruptedException gefangen und ignoriert.
	    // Aus diesem Grund wird im folgenden versucht, den starterThread mehrfach
	    // zu beenden, mit der Hoffnung dass wenigstens einer der interrupt()
	    // Aufrufe von einem Thread.sleep() erfasst wird und nicht von einem
	    // robot.delay().
        for( int i = 0; i < 10 && starterThread != null && starterThread.isAlive(); i++)
        {
            Test.output("stop starterThread " + i); //$NON-NLS-1$
            starterThread.interrupt();
            try
            {
                starterThread.join(10);
            }
            catch (InterruptedException e)
            {
                Thread.currentThread().interrupt();
            }
            //Wenn der Starterthread selber derjenige war, der den Abbruch
            //ausgelöst hat, brauchen wir nicht weiter versuchen uns selber
            //zu unterbrechen. Dies kann neuerdings passieren, wenn die
            //s3.exe nicht gestartet werden kann
            if (Thread.currentThread() == starterThread)
            {
                Test.output("stop selfabort"); //$NON-NLS-1$
                break;
            }
        }
	}

	/**
	 * Falls aktuell eine Spieleüberwachung läuft, wird diese gestoppt.
	 *
	 * @throws Exception
	 */
	public void stopGame() throws Exception
	{
	    try
	    {
    	    IngameMessage.stopShowNoUserInteractionMessage();
    		stopObserve(hostObserver);
    		stopObserve(joinObserver);
    		//falls noch ein starterThread läuft, wird der auch noch beendet
    		//dieses kann passieren, wenn S3 mit Alt+F4 beendet wird, während
    		//noch automatische Klicks durchgeführt werden
    		stopStarterThread();
	    }
	    finally
	    {
	        setRunning(false);
	    }
	}

	public int getMaximumPlayerNumber()
	{
		if (isRunning())
		{
			return hostObserver.getMaximumPlayerNumber();
		} else
		{
			return 0;
		}
	}

	public boolean isHostAway()
	{
		return isRunning() && hostObserver.isHostAway();
	}

    public synchronized boolean isRunning()
    {
        return isRunning;
    }

    public boolean isKeyBlockRunning()
    {
       
        if (this.keyBlockStarter != null)
        {
            return this.keyBlockStarter.isKeyBlockRunning();
        }
        return false;
    }


    public boolean isS3Running()
    {
        if (s3Starter != null)
        {
            return s3Starter.isS3Running();
        }
        return false;
    }

    /**
     * Simulate Mouseclick for static access.
     * 
     * @param robot
     * @param x
     * @param y
     */
    public static void mouseClick(Robot robot, int x, int y)
    {
    	robot.mouseMove(x, y);
    	robot.mouseMove(x, y);
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    }

    public static void mouseClick(Robot robot, Point point)
    {
        mouseClick(robot, point.x, point.y);
    }

    /**
     * ermittelt die aktuelle Mausposition.
     *
     * @return akutelle Mausposition, null falls unbekannt
     */
    public static Point getCurrentMousePosition()
    {
        Point currentPosition = null;
        try
        {
            currentPosition = MouseInfo.getPointerInfo().getLocation();
        }
        catch (Exception e)
        {
            currentPosition = null;
        }
        return currentPosition;
    }

    /**
     * ruft {@code counter} mal pixelColorChecker.match() auf, bis true geliefert wird.
     * Zwischen den Versuchen wird wird {@code delay} ms lang gewartet.
     *
     * @param pixelColorChecker
     * @param counter Anzahl Versuche
     * @param delay Pause in ms zwischen den Versuchen
     * @return true falls die Pixel innerhalb der Anzahl Versuche erkannt wurde,
     *  sonst false
     * @throws InterruptedException
     */
    private boolean waitUntilPixelColorMatch(PixelColorChecker pixelColorChecker, int counter, int delay) throws InterruptedException
    {
        boolean isMatch = false;
        for (int i = 0; i < counter && (!isAborted); ++i)
        {
            isMatch = pixelColorChecker.match();
            if (isMatch)
            {
                break;
            }
            Thread.sleep(delay);
        }
        return isMatch;
    }

    /**
     * unterbricht den Gamestarter. Aktuell nur für
     * selectMapStandalone() implementiert.
     * @param isAborted
     */
    public void setAborted(boolean isAborted)
    {
        this.isAborted = isAborted;
    }

    void createAllReferenceImagesAndCheckers(String mapName)
    {
        createReferenceImages(mapName);
        createReferenceImagesLanguageSpecific();
        createImageCheckers();
        createImageCheckersLanguageSpecific();
    }


    void createReferenceImagesLanguageSpecific()
    {
        ReferenceImageHandling.updateS3Font();

        List<String> referenceInput = new ArrayList<String>();

        for (GoodsInStock goodsInStock : GoodsInStock.values())
        {
            referenceInput.add(goodsInStock.getLanguageMiniChatString());
        }

        ReferenceImageHandling.createReferenceImage(referenceInput, true);
    }
    /**
     * erzeugt die nötigen Referenzbilder, die sprachunabhängig sind
     * @param mapName map-Name, kann null sein.
     */
    void createReferenceImages(String mapName)
    {
        ReferenceImageHandling.updateS3Font();

        List<String> referenceInput = new ArrayList<String>();

        referenceInput.addAll(Arrays.asList(S3Language.languageCheck));
        referenceInput.addAll(Arrays.asList(S3Language.tcpIpDirectPlay));
        referenceInput.addAll(Arrays.asList(S3Language.playerLabel));

        if (mapName != null && !mapName.isEmpty())
        {
            //mapname ist sprachunabhängig
            referenceInput.add(getVisibleMapName(mapName));
        }
        ReferenceImageHandling.createReferenceImage(referenceInput, false);

    }

    /**
     * Creates the ImageCheckers that are needed by the Gamestarter.
     * They are created everytime as font/fontsize may have changed.
     */
    protected void createImageCheckers()
    {
    	// Create the imageCheckers for Network-Type.
    	// -> create the object
        s3LanguageChecker = new MultiImageChecker();
        // -> loop through the possible texts
        for (String text : S3Language.languageCheck)
        {
        	// -> create the image to search for with given language-specific text
            BufferedImage image = ReferenceImageHandling.getReferenceImage(text, false);
            
            // -> add imageChecker for given language-specific image
            s3LanguageChecker.add(new ImageChecker(robot, 129, 88, 100, 25, image, true, true, text));
        }
        // -> set the screensize for the imageCheckers
        s3LanguageChecker.setScreenSizeS3Menu();

        // Create the imageCheckers for TCP/IP-selection
        // -> create the object
        tcpIpDirectPlayChecker = new MultiImageChecker();
        // -> loop through the possible texts
        for (String text : S3Language.tcpIpDirectPlay)
        {
        	// -> create the image to search for with given language-specific text
            BufferedImage image = ReferenceImageHandling.getReferenceImage(text, false);
            
            // -> add imageChecker for given language-specific image
            tcpIpDirectPlayChecker.add(new ImageChecker(robot, 130, 123, 261, 96, image, true, true, text));
        }
        // -> set the screensize for the imageCheckers
        tcpIpDirectPlayChecker.setScreenSizeS3Menu();

        // Create imageChecker to check for given font-color in specific position
        // before we search for the tcp/ip-selection.
        tcpIpVisibleChecker = new ImageChecker(robot, 130, 123, 50, 25, ImageChecker.S3_FONT_COLOR);
        // -> set the screensize for this imageChecker
        tcpIpVisibleChecker.setScreenSizeS3Menu();

    }

    protected void createImageCheckersLanguageSpecific()
    {
        //keine sprachabhängigen ImageChecker im Gamestarter
    }

    public String getVisibleMapName(String mapName)
    {
        int index1 = mapName.indexOf("\\"); //$NON-NLS-1$
        int index2 = mapName.toLowerCase().lastIndexOf(".map"); //$NON-NLS-1$
        //der Name sollte einen Backslash und .map am Ende enthalten.
        //falls nicht lasse den Namen unverändert (auch wenn der Imagevergleich
        //anschliessend vermutlich fehlschlagen wird)
        if (index1 >= 0 && index2 > index1)
        {
            mapName = mapName.substring(index1 + 1, index2) + "  "; //$NON-NLS-1$
        }
        else
        {
            index2 = mapName.toLowerCase().lastIndexOf(".mps"); //$NON-NLS-1$
            if (index1 >= 0 && index2 > index1)
            {
                //es ist keine map, sondern ein savegame
                //bei einem savegame wird nicht der Name, sondern das
                //Datum angezeigt
                mapName = mapName.substring(index1 + 1); // cut "Savegame\\"
                mapName = SaveGame.getNameDisplayedInGameSelection(
                        mapName, settlersLobby.config.getGamePathS3(), settlersLobby.config.getGameDateTimeFormat());
            }
        }
        return mapName;
    }

    public StartOptions getStartOptions()
    {
        return startOptions;
    }

    public void setStartOptions(StartOptions startOptions)
    {
        this.startOptions = startOptions;
    }

    /**
     * gibt an ob der Gamestarter bereit ist, ein Spiel zu starten bzw.
     * einem Spiel beizutreten.
     * @return true falls weder der GameStarter
     * noch Siedler3 läuft.
     */
    public boolean isReady()
    {
        // es muss nicht nur isRunning, sondern auch isS3Running() abgefragt werden,
        // da man sonst schon wieder ein Spiel starten könnte, während man noch
        // aktiv in einem bereits gestarteten Spiel ist
        // isRunning wird nämlich auch dann auf false gesetzt, wenn das Spiel normal
        // gestartet wird, da auch dann der Gamestarter als solches seine Arbeit
        // erledigt hat, allerdings läuft S3 zu dem Zeitpunkt weiterhin, folglich
        // darf man auch kein neues Spiel starten
        return (!isRunning()) && (!isS3Running());
    }

    /**
     * falls CAPSLOCK aktiv ist, wird es deaktiviert. Die Funktion darf nur
     * einmal aufgerufen werden, da die getLockingKeyState() träge reagiert
     * und die durch diese Funktion durchgeführte Änderung des CAPSLOCK states
     * nicht sofort mitbekommt.
     * @throws InterruptedException
     */
    private void checkCapsLock() throws InterruptedException
    {
        if (Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK))
        {
            Test.output("CAPSLOCK"); //$NON-NLS-1$
            Toolkit.getDefaultToolkit().setLockingKeyState(KeyEvent.VK_CAPS_LOCK, false);
        }
    }

    /**
     * siehe {@link SettlersLobby#stopShowNoUserInteractionMessage()}
     */
    private void stopShowNoUserInteractionMessage()
    {
        if (settlersLobby != null)
        {
            IngameMessage.stopShowNoUserInteractionMessage();
        }
    }

    /**
     * ruft {@link #stopShowNoUserInteractionMessage()} auf. Es wird vorausgesetzt,
     * dass der Karteneinstellungsbildschirm sichtbar ist. Damit die
     * NoUserInteractionMessage tatsächlich verschwindet wird extra noch in den
     * Kartennamen geklickt, wodurch der Bildschirm von S3 aktualisiert wird und
     * somit die bisherige Manipulation aktiv übererschrieben wird.
     *
     * @throws InterruptedException
     */
    private void stopShowNoUserInteractionMessageInMapSettingsWindow() throws InterruptedException
    {
        stopShowNoUserInteractionMessage();
        Point currentPosition = getCurrentMousePosition();
        mouseClick(400, 90);
        mouseClick(350, 90);
        robot.mouseMove(currentPosition.x, currentPosition.y);
    }

    /**
     * liefert den aktiven KeyBlock Prozess, falls vorhanden, sonst null.
     * @return KeyBlock Prozess, falls vorhanden, sonst null
     */
    public KeyBlockStarter getKeyBlockStarter()
    {
        if (isKeyBlockRunning())
        {
            return this.keyBlockStarter;
        }
        return null;
    }

    /**
     * liefert den aktiven S3 Prozess, falls vorhanden, sonst null.
     * @return S3 Prozess, falls vorhanden, sonst null
     */
    public S3Starter getS3Starter()
    {
        if (isS3Running())
        {
            return s3Starter;
        }
        return null;
    }
    
    public synchronized void setRunning(boolean isRunning)
    {
        this.isRunning = isRunning;
    }
    
    /**
     * Select the WiMo-Option to start a WiMo-Game
     * @throws InterruptedException 
     */
    private void selectWiMoOption() throws InterruptedException {    	
    	if(startOptions.isWiMo())
		{
    		fastMouseClick(732, 483); // activate WiMo
		}
    }
    
	/*
	 * // Für Testzwecke: public static void main(String[] args) { GameStarter
	 * gameStarter; try { gameStarter = new GameStarter(new SettlersLobby(),
	 * "D:\\spiele\\BlueByte\\Siedler3\\S3.EXE", 20);
	 * gameStarter.createGame("Erasor"); //gameStarter.joinGame("0.0.0.0",
	 * "maximilius"); } catch (Exception e) { e.printStackTrace(); } }
	 */
}
