/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import net.siedler3.alobby.controlcenter.ReqQuery;
import net.siedler3.alobby.controlcenter.Test;

/**
 * Erzeugt und speichert alle Bilder, die für etwaigen Textvergleich
 * für die Überwachung von S3 benötigt werden.
 * Benutzt dazu die Klasse WindowFont.
 * 
 * @author jim
 *
 */
public class ReferenceImageHandling
{    
    private static Map<String, BufferedImage> map = new HashMap<String, BufferedImage>();
    
    public static BufferedImage createReferenceImage(String text, boolean isLanguageSpecific)
    {
        String key = createKey(text, isLanguageSpecific);
        
        BufferedImage image = map.get(key);
        if (image == null)
        {
            image = WindowFont.createReferenceImage(text);
            if (image != null)
            {
                map.put(key, image);
            }
        }
        return image;
    }
    
    public static BufferedImage getReferenceImage(String text, boolean isLanguageSpecific)
    {
        String key = createKey(text, isLanguageSpecific);
        
        BufferedImage image = map.get(key);
        return image;
    }
    
    private static String createKey(String text, boolean isLanguageSpecific)
    {
        String font = ReqQuery.s3Font;
        int fontSize = ReqQuery.s3FontSize;
        
        String key = font + "_" +  fontSize + "_" + text;
        if (isLanguageSpecific)
        {
            key = S3Language.getCurrentLanguage().toString() + "_" + key;
        }

        return key;
    }
    
    public static void updateS3Font()
    {
        ReqQuery.updateS3Font();
    }

    public static List<BufferedImage> createReferenceImage(
            List<String> referenceInput, boolean isLanguageSpecific)
    {
        List<String> candidateList = new ArrayList<String>();
        
        //suche die Einträge, die es noch nicht gibt
        for (String text : referenceInput)
        {
            String key = createKey(text, isLanguageSpecific);
            
            BufferedImage image = map.get(key);
            if (image == null)
            {
                candidateList.add(text);
            }
        }
        //erzeuge alle auf einmal und lege vom Ergebnis ImageEntries an
        // die BufferedImages können null sein, falls Probleme auftreten
        List<BufferedImage> result = WindowFont.createReferenceImage(candidateList);
        for (int i = 0; i < result.size(); ++i)
        {
            String text = candidateList.get(i);
            String key = createKey(text, isLanguageSpecific);
            BufferedImage image = result.get(i);
            map.put(key, image);
        }
        return result;
    }
    
    /**
     * schreibt alle Referenz Bilder ins Temp-Dir.
     * Kann zum debuggen benutzt werden.
     * Auf jede Datei wird deleteOnExit() aufgerufen, so dass
     * die Dateien gelöscht werden, sobald die Lobby beendet wird.
     */
    public static void dumpAllImages()
    {
        for (BufferedImage image : map.values())
        {
            try
            {
                File tmpfile = File.createTempFile("alobby_pic_", ".bmp");
                ImageIO.write(image, "bmp", tmpfile);
                tmpfile.deleteOnExit();
            }
            catch (Exception e) 
            {
                Test.outputException(e);
            }

        }
    }
}
