/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.imageio.ImageIO;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;

public class MultiImageChecker
{

    private List<ImageChecker> imageCheckers = new ArrayList<ImageChecker>();
    
    public void add(ImageChecker imageChecker)
    {
        imageCheckers.add(imageChecker);
    }
    
    public List<ImageChecker> getImageCheckers()
    {
        return imageCheckers;
    }
    
    
    public boolean matchAll()
    {
        boolean result = false;
        for (int i = 0; i < imageCheckers.size(); ++i)
        {
            ImageChecker imageChecker = imageCheckers.get(i);
            // Es soll nur einmal ein Screenshot erzeugt werden,
            // dies erledigt der erste ImageChecker.
            // Alle weiteren übernehmen von ihm den gemachten Screenshot
            // Sollte die performance verbessern
            if (i > 0)
            {
                //in case getImageCapture() returns null, this does not harm
                //as now all imageCheckers will try to make a capture on their own
                imageChecker.setUserSuppliedImageCapture(
                        imageCheckers.get(0).getImageCapture());
            }
            else
            {
                imageChecker.setAutoCleanup(false);
            }
            boolean isMatch = imageChecker.match();
            result |= isMatch;
        }
        if (imageCheckers.size() > 0)
        {
            imageCheckers.get(0).setAutoCleanup(true);
        }
        cleanup();
        return result;

    }
    
    /**
     * Returns true if one matching ImageChecker was found.
     * 
     * @return ImageChecker
     */
    public ImageChecker matchFirst()
    {
    	// set result to null
        ImageChecker result = null;
        
        // loop through all available imageCheckers
        for (int i = 0; i < imageCheckers.size(); ++i)
        {
        	// get the actual imageChecker
            ImageChecker imageChecker = imageCheckers.get(i);
            imageChecker.setIndex(i);
            
            Test.output("check on " + i);
            
            // Create screenshot to check with the first imageChecker.
            // All following will use the same image.
            // TODO possible problem to ignore the first index?
            if (i > 0) {
            	Test.output("get new image");
                // In case getImageCapture() returns null, this does not harm
                // as now all imageCheckers will try to make a capture on their own
                imageChecker.setUserSuppliedImageCapture(imageCheckers.get(0).getImageCapture());
                Test.output("got image: " +  imageChecker.getUserSuppliedImageCapture());
                
                // FIXME remove after test
                if( SettlersLobby.isBetaUsed() ) {
	                // get Date
	    			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");
	    			Calendar now = Calendar.getInstance();
	    			
	    			// save picture and create screenshot-Directory if it doesn't exists
	    			File screenShotDir = new File(SettlersLobby.configDir, "debug");
	    			if (!screenShotDir.exists()) {
	    				screenShotDir.mkdirs();
	    			}
	    			File screenShotFile = new File(screenShotDir.getAbsolutePath(), formatter.format(now.getTime()) + "_find_" + i + ".png");
	    			try {
						ImageIO.write(imageChecker.getUserSuppliedImageCapture(), "png", screenShotFile);
					} catch (IOException e) {
						Test.outputException(e);
					}
                }                
            }
            else
            {
            	Test.output("reset imageChecker");
            	// Disable cleanup for the first imageChecker-Object.
                imageChecker.setAutoCleanup(false);
            }
            
            // Check if imageChecker does match.
            boolean isMatch = imageChecker.match();
            
            Test.output("match? " + isMatch);
            
            if (isMatch) {
                result = imageChecker;
                break;
            }
        }
        
        // Cleanup the first imageChecker if list does contain more than 0 of them.
        if (imageCheckers.size() > 0)
        {
            imageCheckers.get(0).setAutoCleanup(true);
        }
        
        // return the matching result
        return result;
    }
    
    /**
     * Get index of given imageChecker.
     * 
     * @param imageChecker
     * @return int	the index-position
     */
    public int getIndex(ImageChecker imageChecker)
    {
        return imageCheckers.indexOf(imageChecker);
    }
    
    /**
     * Wait until a imageCheck matches with given delay.
     * 
     * @param counter
     * @param delay
     * @return
     * @throws InterruptedException
     */
    public ImageChecker waitUntilMatchFirst(int counter, int delay) throws InterruptedException
    {
        ImageChecker imageChecker = null;
        for (int i = 0; i < counter; ++i)
        {
            imageChecker = matchFirst();
            Test.output("imageChecker: " + imageChecker);
            if (imageChecker != null)
            {
                break;
            }
            Thread.sleep(delay);
        }
        Test.output("result: " + imageChecker);
        return imageChecker;
    }
    
    /**
     * sets the screensize for all internal imageCheckers.
     * @param width
     * @param height
     */
    public void setScreenSize(int width, int height)
    {
        for (ImageChecker imageChecker : imageCheckers)
        {
            imageChecker.setScreenSize(width, height);
        }
    }

    public void setScreenSizeS3Menu()
    {
        setScreenSize(800, 600);
    }
    
    public void cleanup()
    {
        for (ImageChecker imageChecker : imageCheckers)
        {
            imageChecker.cleanup();
        }
    }

}
