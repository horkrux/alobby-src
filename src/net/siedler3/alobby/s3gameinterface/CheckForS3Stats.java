/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;

import net.siedler3.alobby.controlcenter.ALobbyConstants;
import net.siedler3.alobby.controlcenter.Configuration;
import net.siedler3.alobby.controlcenter.OpenGame;
import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;
import net.siedler3.alobby.gui.additional.IngameMessage;
import net.siedler3.alobby.i18n.I18n;
import net.siedler3.alobby.util.GameDataFile;
import net.siedler3.alobby.util.LocaleChanger;
import net.siedler3.alobby.util.TimerTaskCompatibility;
import net.siedler3.alobby.util.httpRequests;

/**
 * TimerTask to check for S3 Stats-Window
 * and if it is active than take a snapshot.
 *
 * @author Zwirni
 *
 */
public class CheckForS3Stats extends TimerTaskCompatibility implements LineListener
{
    private ImageChecker[] imageChecker = null;
    private ImageChecker[] teamChecker = null;

    private static boolean initialised;
    private SettlersLobby settlersLobby;
    private GameDataFile gameDataFile;
    private boolean uploadScreenshot = false;
    private boolean screenshotSavingIsActive = false;
    private File screenShotFile;
    private boolean uploaded = false;
    private boolean won = false;
    private int groupnumber = 0;

    private CheckForS3StatsPrintKey checkForS3StatsPrintKey;

	private String hostUserName;
	private String ip;
	private String mapName;
	private String mapNameForScreenshot;
	private int goods;
	private int league = 0;
	private int tournamentid = 0;
	private int round = 0;
	private int matchnumber = 0;
	private int playercount = 0;
	private String racelist = "";
	private String startTime = null;
	private boolean wimo;
	
	// Marker for end of Game (if true than was win/lost visible).
    boolean endOfGame = false;
    
    // TODO ??? really each dimension? possibly its only 800x600
    private static final Dimension[] screenSizes = {
        new Dimension(640, 480),
        new Dimension(800, 600),
        new Dimension(1024, 768)
    };

    private static final BufferedImage[] gameButtonImage = new BufferedImage[5];
    private static final BufferedImage[] teamPositionImage = new BufferedImage[20];

    private static Robot robot;

    /**
     * Task to get a s3-statistic-screenshot and upload it including some game-settings to screenuploader
     * under screen.siedler3.net.
     *      
     * @param settlersLobby
     * @param hostUserName - the Nickname of the Host
     * @param ip - the IP of the Host
     * @param mapName - the filename of the played map
     * @param goods - the goods as String (e.g. "Wenig")
     */
    public CheckForS3Stats(SettlersLobby settlersLobby, String hostUserName, String ip, String mapName, int goods)
    {

    	this.settlersLobby = settlersLobby;
    	this.hostUserName = hostUserName;
    	this.ip = ip;
    	this.mapName = mapName;
    	this.mapNameForScreenshot = mapName.replace(ALobbyConstants.LABEL_SAVE_GAME + File.separator, "").replace(ALobbyConstants.PATH_S3MULTI + File.separator, "").replace(ALobbyConstants.PATH_USER + File.separator, "").replace(ALobbyConstants.PATH_S4USER + File.separator, "");
    	this.goods = goods;
    	Test.output("mapName: " + mapName);
    	
    	// get actual chosen S3-language
    	// -> used to minimize the loading of images for following imageCheck to get
    	//    get the S3-Stats-Screen
        String locale = LocaleChanger.LOCALES_ISO_3166[LocaleChanger.readLocale(this.settlersLobby.getS3Directory())];

        // get the picture for comparison later
        try {
        	        	
			//robot = ThreadCommunicator.getInstance().getRobot();

			// this is a pixel-picture to double-check the actual screen
	        // -> the top left blue background in stats-line from first player in stats
	        gameButtonImage[0] = ImageIO.read(AutoSaveTask.class.getResourceAsStream(
	                ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/stati0.bmp")); //$NON-NLS-1$
	        
	        // define language-depended position for the pixel-picture
	        int pixelPictureX = 190;
	        int pixelPictureY = 102;
	        if( locale.matches("jp") ) {
	        	pixelPictureY = 104;
	        }

	        // -> load the winPicture if that file exists in the S3-language
	        String winPictureFile = ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/stati_" + locale + "_win.bmp";
	        if( this.getClass().getResource(winPictureFile) != null ) {
	        	gameButtonImage[1] = ImageIO.read(AutoSaveTask.class.getResourceAsStream(winPictureFile)); //$NON-NLS-1$
	        }
	        else {
	        	gameButtonImage[1] = null;
	        }

	        // -> load the lostPicture if that file exists in the S3-language
	        String lostPictureFile = ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/stati_" + locale + "_lost.bmp";
	        if( this.getClass().getResource(lostPictureFile) != null ) {
	        	gameButtonImage[2] = ImageIO.read(AutoSaveTask.class.getResourceAsStream(lostPictureFile)); //$NON-NLS-1$
	        }
	        else {
	        	gameButtonImage[2] = null;
	        }

	        // -> load the Wine winPicture if that file exists in the S3-language
	        String winWinePictureFile = ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/stati_" + locale + "_wine_win.bmp";
	        if( this.getClass().getResource(winWinePictureFile) != null ) {
	        	gameButtonImage[3] = ImageIO.read(AutoSaveTask.class.getResourceAsStream(winWinePictureFile)); //$NON-NLS-1$
	        }
	        else {
	        	Test.output("image3 error");
	        	gameButtonImage[3] = null;
	        }

	        // -> load the Wine lostPicture if that file exists in the S3-language
	        String lostWinePictureFile = ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/stati_" + locale + "_wine_lost.bmp";
	        if( this.getClass().getResource(lostWinePictureFile) != null ) {
	        	gameButtonImage[4] = ImageIO.read(AutoSaveTask.class.getResourceAsStream(lostWinePictureFile)); //$NON-NLS-1$
	        }
	        else {
	        	gameButtonImage[4] = null;
	        }
	        
	        robot = ThreadCommunicator.getInstance().getRobot();
	        
	        // create the imageChecker depending on above defined imageRessource
	        imageChecker = new ImageChecker[5];
	        // -> at first pixel-picture
            imageChecker[0] = new ImageChecker(robot, pixelPictureX, pixelPictureY,
                    gameButtonImage[0].getWidth(), gameButtonImage[0].getHeight(), gameButtonImage[0]);
            // -> than the language-specific win-picture, if available
            if( gameButtonImage[1] != null ) {
            	imageChecker[1] = new ImageChecker(robot, 283, 28,
                    gameButtonImage[1].getWidth(), gameButtonImage[1].getHeight(), gameButtonImage[1]);
            }
            // -> than the language-specific lost-picture, if available
            if( gameButtonImage[2] != null ) {
            	imageChecker[2] = new ImageChecker(robot, 283, 28,
                    gameButtonImage[2].getWidth(), gameButtonImage[2].getHeight(), gameButtonImage[2]);
            }
            // -> than the language-specific Wine win-picture, if available
            if( gameButtonImage[3] != null ) {
            	imageChecker[3] = new ImageChecker(robot, 283, 28,
                    gameButtonImage[3].getWidth(), gameButtonImage[3].getHeight(), gameButtonImage[3]);
            }
            // -> than the language-specific Wine lost-picture, if available
            if( gameButtonImage[4] != null ) {
            	imageChecker[4] = new ImageChecker(robot, 283, 28,
                    gameButtonImage[4].getWidth(), gameButtonImage[4].getHeight(), gameButtonImage[4]);
            }
                        
            // create a list of team-number-images in order to get the team-assignments for
            // each position
            for( int t=1;t<=20;t++ ) {
            	String teamfile = ALobbyConstants.PACKAGE_PATH + "/s3gameinterface/img/team_" + t + ".bmp";
    	        if( this.getClass().getResource(teamfile) != null ) {
    	        	teamPositionImage[t-1] = ImageIO.read(AutoSaveTask.class.getResourceAsStream(teamfile)); //$NON-NLS-1$
    	        	
    	        }
    	        else {
    	        	teamPositionImage[t-1] = null;
    	        }
            }
                        
            // generate the teamChecker-array with all possible team-assignments on each possible position in statistic
            int teamImagePositionX = 52;
            int teamImagePositionY = 101;
            teamChecker = new ImageChecker[400];
            int tc = -1;
            for( int position=1;position<=20;position++ ) {
            	for( int team=1;team<=20;team++ ) {
            		tc = tc + 1;
            		teamChecker[tc] = new ImageChecker(robot, teamImagePositionX, teamImagePositionY,
    	        		teamPositionImage[team-1].getWidth(), teamPositionImage[team-1].getHeight(), teamPositionImage[team-1]);
            	}
            	teamImagePositionY = teamImagePositionY + 20;
            }
                        
            initialised = true;
	            	        
		} catch (AWTException e1) {
			Test.outputException(e1);
		} catch (IOException e) {
			Test.outputException(e);
		}
      
    }

    /**
     * gibt den Index der Bildschirmauflösung zurück, anhand derer das passende
     * Vergleichsbild ausgewählt wird.
     * @return
     */
    protected int getIndexOfResolution()
    {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        for (int i = screenSizes.length - 1; i >= 0; i--)
        {
            Dimension refScreenSize = screenSizes[i];
            if (screenSize.equals(refScreenSize))
            {
                return i;
            }
        }
        return screenSizes.length - 1;
    }

    public boolean isStatsDisplayed()
    {
    	if (!initialised || imageChecker == null) {
    		return false;
    	}
    	return imageChecker[0].match();
    }

    @Override
    public void run()
    {
    	
    	// cancel schedule if error happened during initialization
        if( !initialised || imageChecker == null )
        {
            Test.output("S3-Stats-Check not initialised"); //$NON-NLS-1$
            cancel();
            return;
        }

        // check for Array-Index depending on resolution
        int index = this.getIndexOfResolution();
        
        Test.output("resolutionIndex: " + index);
        
        // If resolution is not 800x600, do nothing at this point.
        // Ignore this check in case ActivateScreenSizeCheck is deactivated.
        if( Configuration.getInstance().isActivateScreenSizeCheck() && index != 1 ) {
        	Test.output("S3-Stats-Check not performed because resolution is " + index + " and not 800x600 and ActivateScreenSizeCheck is disabled."); //$NON-NLS-1$
        	return;
        }
        
        Test.output("check screen for stat #1");
                        
        // General Marker for visible statistic. 
        boolean statiVisible = false;
                
        // check for matches with picture templates
        // -> index 0 must match in first place (pixel-picture within stats-table which must match)
        if( imageChecker[0].match() ) {
        	
        	Test.output("check screen for stat #2");
        	
        	// Set the general marker true to start the space-bar-check
        	statiVisible = true;
        	        	
        	// check for each other image for match
        	for ( int i = 1; i < imageChecker.length; i++ )
            {
        		if( imageChecker[i] != null && imageChecker[i].match() ) {
        			// The game is obviously over (win or lost for user).
        			endOfGame = true;
        			// did this user win?
        			if( !(i % 2 == 0) ) {
        				won = true;
        			}
        			break;
        		}
            }
        }

        if( statiVisible )
        {
            Test.output("S3-Stats-Check..."); //$NON-NLS-1$
            try
            {
            	// get some data to check whether this screenshot should be posted in chat
            	// and which parameters will be send to screenuploader
    			playercount = 0;
    			if( settlersLobby.isHost() ) {
    				Test.output("is host");
    				playercount = settlersLobby.getRunningHostGame().getCurrentPlayer();
    				if( settlersLobby.getRunningHostGame().isLeagueGame() ) {
    					this.league = 1;
    				}
    				this.tournamentid = settlersLobby.getRunningHostGame().getTournamentId();
    				this.round = settlersLobby.getRunningHostGame().getTournamentRound();
    				this.matchnumber = settlersLobby.getRunningHostGame().getTournamentMatchnumber();
    				this.groupnumber = settlersLobby.getRunningHostGame().getTournamentGroupnumber();
    				this.wimo = settlersLobby.getRunningHostGame().isWimo();
    				this.gameDataFile = new GameDataFile(settlersLobby, settlersLobby.getRunningHostGame());
    			}
    			else {
    				Test.output("is not host");
    				OpenGame thegame = settlersLobby.getRunningGames().getOpenGame(this.ip, this.hostUserName);
    				Test.output("thegame: " + thegame);
    				if( thegame instanceof OpenGame ) {
	    				playercount = thegame.getCurrentPlayer();
	    				if( thegame.isLeagueGame() ) {
	    					this.league = 1;
	    				}
	    				this.tournamentid = thegame.getTournamentId();
	    				this.round = thegame.getTournamentRound();
	    				this.matchnumber = thegame.getTournamentMatchnumber();
	    				this.groupnumber = thegame.getTournamentGroupnumber();
	    				this.wimo = thegame.isWimo();
    				}
    				else {
    					thegame = settlersLobby.getRunningGame();
    					Test.output("thegame #2: " + thegame);
    				}
    				// get the gamedatafile
    				Test.output("get the gamedatafile: " + thegame);
        			this.gameDataFile = new GameDataFile(settlersLobby, thegame);
    			}
    			
    			this.racelist = gameDataFile.get("usersWithRaces");
    			
    			this.startTime = gameDataFile.get("creationTime");
    			
    			// TODO - each screenshot for testing purposes is a league-screenshot
    			//this.league = 1;
            	
            	// check if the screenshot should be forced automatically uploaded; ONLY if:
            	// -> user is host AND has activated the upload in its settings
            	// -> OR user has won this game AND this is a league- or tournament-game
            	if( this.settlersLobby.isHost() && Configuration.getInstance().isActivateS3StatsScreenCapture() ) {
            		Test.output("check screen for stat #3");
            		this.setUploadScreenshot(true);
            	}
            	if( this.won && ( this.league > 0 || this.tournamentid > 0 )) {
            		Test.output("check screen for stat #4");
            		this.setUploadScreenshot(true);
            	}
            	
            	// get the screenshot and save it local
            	// -> returns the File for upload
            	screenShotFile = doSave();
            	
            	// check if forced upload is enabled
                // -> if not skip the following screenupload and show a hint that the user
            	//    could press empty-space to upload the screenshot
                if( !this.uploadScreenshot ) {
                	Test.output("S3-Stats-Check not performed because upload is not forced (not host or not won this game)"); //$NON-NLS-1$
                	// Listen for pressed key to upload manually (only if uploadScreenshot is still false).
                	this.checkForS3StatsPrintKey = new CheckForS3StatsPrintKey(this, screenShotFile, settlersLobby);
                	this.checkForS3StatsPrintKey.startMonitoring();
                	// cancel the s3-stats-schedule to prevent duplicate checks
                	cancel();
                	// and show the hint
                    IngameMessage.startShowS3StatsUploadQuestionMessage();
                	return;
                }
            	
            	// Save screenshot if user is host or has won this game
                if( endOfGame ) {
                	Test.output("S3-Stats-Check: the game is over"); //$NON-NLS-1$
        			// save the file in screenuploader if it is an forced upload
        			if (screenShotFile != null && this.uploadScreenshot) {
        				performUpload(screenShotFile);
        			}
                } else {
                	// Listen for pressed key to upload manually (only if uploadScreenshot is still false).
                	this.checkForS3StatsPrintKey = new CheckForS3StatsPrintKey(this, screenShotFile, settlersLobby);
                	this.checkForS3StatsPrintKey.startMonitoring();
                	// Cancel the scheduler if it's not the end of game. 
                    cancel();
                	IngameMessage.startShowS3StatsUploadQuestionMessage();
                }
            }
            catch (Exception e)
            {
                Test.outputException(e);
                if( this.checkForS3StatsPrintKey != null )
                {
                	this.checkForS3StatsPrintKey.stopMonitoring();
                }
            }
        }
        else
        {
            Test.output("S3-Stats-Check not executed"); //$NON-NLS-1$
        }
    }

    @Override
    public void update(LineEvent event)
    {
        if (event.getType() == LineEvent.Type.STOP)
        {
            try
            {
                checkForS3StatsPrintKey.stopMonitoring();
            }
            finally
            {
                try {
					screenShotFile = this.doSave();
        			// save the file in screenuploader
        			// if it is not disabled by the user
        			if (screenShotFile != null && (Configuration.getInstance().isActivateS3StatsScreenCapture() || this.uploadScreenshot)) {
        				performUpload(screenShotFile);
        			}
				} catch (IOException e) {
					Test.outputException(e);
				}
            }
        }
    }
    
    /**
     * Set marker to upload created screen on screen.siedler3.net
     * 
     * @param uploadScreenshot true or false
     */
    public void setUploadScreenshot( boolean uploadScreenshot ) {
    	Test.output("uploadScreenshot set to " + uploadScreenshot);
    	this.uploadScreenshot = uploadScreenshot;
    }
    
 
  
	/**
     * Generates the Screenshot and saves it local
	 * @throws IOException 
     */
    public File doSave() throws IOException
    {
    	File screenShotFile = null;

    	// if saving is still active, don't do it again
    	Test.output("screenshotSavingIsActive" + this.screenshotSavingIsActive);
    	if( this.screenshotSavingIsActive ) {
    		return screenShotFile;
    	}
    	
    	// mark as active
    	this.screenshotSavingIsActive = true;

    	// get the s3font
		ReferenceImageHandling.updateS3Font();
		
		// add the actual username as image-reference to search for it via ImageChecker later 
		ReferenceImageHandling.createReferenceImage(settlersLobby.getNick(), false);
		
		// create a imageChecker to perform search for the nickname of the actual user
		ImageChecker playerSelect = new ImageChecker(robot, 0, 0, 800, 600, settlersLobby.getNick(), false);
		Test.output("playerSelect: " + playerSelect);
		
		// variable to hold the coordinates where the nickname of the actual user is visible
		Point coordinates = null;
		try {
			// check if the nickname is anywhere in the statistic screen
			if( false != ImageChecker.waitUntilImageMatch(playerSelect, 20, 100) ) {
				// get the coordinates (they are the center of the matching image)
				coordinates = playerSelect.getMatchCenter();
				Test.output("coordindates: " + coordinates); 
			}
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
    	
		// add the game-name on statistic before screenshot is taken
    	Test.output("start show mapname");
		IngameMessage.startShowS3StatsMapName("Map: " + this.mapNameForScreenshot);
		
    	BufferedImage image = null;
		try {
			// generate Screenshot
			image = new Robot().createScreenCapture(new Rectangle(new Dimension(800, 600)));
		} catch (HeadlessException e) {
			Test.outputException(e);
		} catch (AWTException e) {
			Test.outputException(e);
		}
		// if then picture was taken, save it
		if( image instanceof BufferedImage ) {
			
			if( coordinates != null ) {
				// draw a line under the nickname of the actual user
				Graphics2D g2d = image.createGraphics();
				g2d.setColor(new Color(255,253,0));
				g2d.drawLine(coordinates.x - 20, coordinates.y + 7, coordinates.x + 40, coordinates.y + 7);
			}
			
			// get Date
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");
			Calendar now = Calendar.getInstance();
			
			// save picture and create screenshot-Directory if it doesn't exists
			File screenShotDir = new File(SettlersLobby.configDir, "screens");
			if (!screenShotDir.exists()) {
				screenShotDir.mkdirs();
			}
			screenShotFile = new File(screenShotDir.getAbsolutePath(), formatter.format(now.getTime()) + ".png");
			ImageIO.write(image, "png", screenShotFile);
		}
		
		// hide the the visible game-name on statistic after screenshot is taken
		Test.output("stop show mapname");
		IngameMessage.stopShowS3StatsMapName("Map: " + this.mapNameForScreenshot);
		
		this.screenshotSavingIsActive = false;
		return screenShotFile;
    }

	/**
     * Generates the Screenshot and saves it
	 * @throws IOException 
     */
    public void performUpload(File screenShotFile) throws IOException
    {
    	// prevent double uploads
    	if( !this.uploaded ) {
    	
    		// preparation for league-screenshots
			// -> set to true if image should not be deleted through screen-Management
			boolean preventDeletion = false;
			if( this.league > 0 ) {
        		preventDeletion = true;
        	}
			
			// get the team-assignments for each position and create an CSV-compatible list of them
        	int tc = -1;
        	String positionTeamList = "";
        	for( int position=1;position<=20;position++ ) 
        	{
        		Test.output("getting teamlists #1");
        		for( int team=1;team<=20;team++ ) 
        		{
        			Test.output("getting teamlists #2");
        			tc = tc + 1;
        			if( teamChecker[tc].match() ) {
        				Test.output("team-position-match on " + position + " / " + team);
        				if( positionTeamList.length() > 0 ) {
        					positionTeamList = positionTeamList + ";";
        				}
        				// format for entry: 'position,team'
        				positionTeamList = positionTeamList + "\"'" + position + "','" + team + "'\"";
        				if( position == playercount ) {
        					Test.output("playerlist completed: " + playercount + " player");
        					position = 20;break; 
        				}
        			}
        		}
        	}

			// do not prevent the deletion of this screenshot if only 1 users played this game
			if( playercount == 1 ) {
				preventDeletion = false;
			}
			
			Test.output("prepare httprequest");
    		
	    	//IngameMessage.stopShowS3StatsUploadQuestionMessage();
			// show hint that image was taken and upload is in progress
			IngameMessage.startShowS3StatsMessage();
			httpRequests httpRequests = new httpRequests();
			// disable PHP-Session-security if it is not a league-game
			if( this.league == 0 ) {
				httpRequests.setIgnorePhpSessId(true);
			}
			httpRequests.setUrl(settlersLobby.config.getDefaultScreenUploader());
			httpRequests.addParameter("smalloutput", "1");
			if( preventDeletion ) {
    			httpRequests.addParameter("preventDeletion", 1);
    		}
    		else {
    			httpRequests.addParameter("preventDeletion", 0);
    		}
			// add mapname and goods as parameter
    		httpRequests.addParameter("mapname", this.mapName);
    		httpRequests.addParameter("goods", this.goods);
	    	// add nickname as parameter
	    	httpRequests.addParameter("nickname", settlersLobby.getNick());
	    	if( this.tournamentid > 0 ) {
	    		httpRequests.addParameter("liga", 0);
	    		httpRequests.addParameter("turnierid", this.tournamentid);
	    	}
	    	else {
	    		httpRequests.addParameter("liga", this.league);
	    		httpRequests.addParameter("turnierid", 0);
	    	}
	    	httpRequests.addParameter("round", this.round);
	    	httpRequests.addParameter("matchnumber", this.matchnumber);
	    	httpRequests.addParameter("groupnumber", this.groupnumber);
	    	// add the position-team-list
	    	httpRequests.addParameter("positionteamlist", positionTeamList);
	    	// add the race-list
	    	httpRequests.addParameter("racelist", this.racelist);
	    	// add the real-starttime of this game
	    	httpRequests.addParameter("starttime", this.startTime);
	    	// add game-result for actual user to request
	    	if( won ) {
	    		httpRequests.addParameter("result", 1);
	    	}
	    	else {
	    		httpRequests.addParameter("result", 0);
	    	}
	    	// add WiMo-Marker
	    	if( this.wimo ) {
	    		httpRequests.addParameter("wimo", 1);
	    	}
	    	else {
	    		httpRequests.addParameter("wimo", 0);
	    	}
	    	// marker, if game ends normal
	    	if( this.endOfGame ) {
	    		httpRequests.addParameter("endOfGame", 1);
	    	}
	    	else {
	    		httpRequests.addParameter("endOfGame", 0);
	    	}
	    	// add name of host
	    	httpRequests.addParameter("hostusername", this.hostUserName);
	    	// add the screenshot-file itself as file
			httpRequests.addFile("file", screenShotFile);
			
			// perform request and get the result
			Test.output("start uploading screenshot");
			String result = httpRequests.doRequest().trim();
			Test.output("upload results in: " + result);

			String pictureurl = settlersLobby.config.getDefaultScreenUploaderDomain() + result;
	
			// Stop the previous message
			IngameMessage.stopShowS3StatsMessage();
		
			// show hint that upload is done and user can now return to aLobby
			IngameMessage.startShowS3StatsUploadedMessage();
			
			Test.output("post link for screenshot if " + playercount + " > 1 and " + pictureurl.matches(ALobbyConstants.REGEX_URL) + " is true");
						
			// if result is available and usable and more than 1 player played this game, than post it in chat
			if( playercount > 1 && result.length() > 0 && pictureurl.matches(ALobbyConstants.REGEX_URL) ) {
				this.settlersLobby.actionChatMessage(I18n.getString("IRCCommunicator.MSG_AUTO_STATISTICSCREEN") + " " + pictureurl);
			}
			
			if( checkForS3StatsPrintKey != null ) {
				checkForS3StatsPrintKey.stopMonitoring();
			}
			
			// stop scheduler to prevent more screenshots
			this.settlersLobby.stopCheckForS3Stats();
		
			this.uploaded = true;
			
    	}
		return;
    }

	public void resetSettings() {
		this.screenShotFile = null;
		this.uploaded = false;
	}
}