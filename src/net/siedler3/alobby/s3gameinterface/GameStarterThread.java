/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2021 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
package net.siedler3.alobby.s3gameinterface;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.Test;

public class GameStarterThread extends Thread
{

    private GameStarterVanilla gameStarter;
    private SettlersLobby settlersLobby;
    private String mapName;
    
    public GameStarterThread(SettlersLobby settlersLobby, String gamePath,
            int timeOut, GameStarterVanilla.StartOptions startOptions)
    {
        //Namen anpassen
        setName(getName() + "-Gamestarter");
        this.settlersLobby = settlersLobby;
        this.mapName = startOptions.getMapName();
        try
        {
            gameStarter = new GameStarterVanilla(settlersLobby);
            gameStarter.setStartOptions(startOptions);
            //Da S3 manuell gestartet wird, muss hier createReferenceImages
            //explizit aufgerufen werden
            gameStarter.createAllReferenceImagesAndCheckers(mapName);
        }
        catch (Exception e) 
        {
        }
    }

    @Override
    public void run()
    {
        if (gameStarter != null)
        {
            try
            {
                gameStarter.selectMapStandalone(mapName);
            }
            catch (Exception e) {
                Test.outputException(e);
            }
        }
        settlersLobby.actionCloseMapWait();
    }
    
    public void abort()
    {
        if (gameStarter != null)
        {
            gameStarter.setAborted(true);
        }
    }
    
    
}
