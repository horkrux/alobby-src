package net.siedler3.alobby.nativesupport;

import java.lang.reflect.Field;
import java.util.List;

import com.sun.jna.Structure;

public class StructureByteConverter {
	public static byte[] getBytes(Structure struct, List<Field> list) {
		byte[] bytes = new byte[struct.size()];
		int offset = 0;
		try {
			for (Field field : list) {
				byte[] buffer = null;;
				int bufferBytes = 0;
				if (field.getType().toString().equals("int") ) {
					buffer = getBytes(field.getInt(struct));
					bufferBytes = 4;
				} else if (field.getType().toString().equals("short")) {
					buffer = getBytes(field.getShort(struct));
					bufferBytes = 2;
				}

				for (int j = 0; j < bufferBytes; j++) {
					bytes[j + offset] = buffer[j];
				}
				offset += bufferBytes;

			}
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bytes;
	}

	/**Lösung 1**/
	private static byte[] getBytes(int i) {
		return getBytes(i, 4);
	}

	private static byte[] getBytes(short i) {
		return getBytes(i, 2);
	}

	private static byte[] getBytes(long value, int byteCount) {
		byte[] bytes = new byte[byteCount];
		for (int i = 0 ; i < byteCount ; i++) {
			bytes[i] = (byte) (value & 0xff);
			value = value >> 8;
		}
		return bytes;
	}
	
	/**Lösung 2**/
	/*private static byte[] getBytes(int int1) {
		ByteBuffer buffer = ByteBuffer.allocate(4);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.putInt(int1);
		if (buffer.hasArray()) {
			return buffer.array();
		}
		System.out.println("Error: No array available");
		return null;
	}
	private static byte[] getBytes(short short1) {
		ByteBuffer buffer = ByteBuffer.allocate(2);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.putShort(short1);
		if (buffer.hasArray()) {
			return buffer.array();
		}
		System.out.println("Error: No array available");
		return null;
	}*/
}
