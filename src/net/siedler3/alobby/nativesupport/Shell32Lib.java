package net.siedler3.alobby.nativesupport;

import com.sun.jna.LastErrorException;
import com.sun.jna.Native;
import com.sun.jna.platform.win32.Shell32;
import com.sun.jna.win32.W32APIOptions;

public interface Shell32Lib extends Shell32{
	public Shell32Lib INSTANCE = (Shell32Lib)Native.loadLibrary("shell32", Shell32Lib.class, W32APIOptions.DEFAULT_OPTIONS);
	
	boolean IsUserAnAdmin() throws LastErrorException;
}
