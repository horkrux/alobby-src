package net.siedler3.alobby.nativesupport;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Native;
import com.sun.jna.Structure;
import com.sun.jna.platform.win32.GDI32;
import com.sun.jna.platform.win32.WinDef.HDC;
import com.sun.jna.platform.win32.WinDef.HFONT;
import com.sun.jna.platform.win32.WinGDI;
import com.sun.jna.platform.win32.WinUser.SIZE;
import com.sun.jna.win32.W32APIOptions;

public interface GDI32Lib extends GDI32 {
	public final int BI_RGB = 0;
	public final int HORZRES = 8;
	public final int VERTRES = 10;
	public final int DIB_RGB_COLORS = 0;

	public interface ByteStructure {
		public byte[] getBytes();
	}

	public static class BitmapFileHeader extends Structure implements ByteStructure {
		public short bfType;
		public int bfSize;
		public short bfReserved1;
		public short bfReserved2;
		public int bfOffBits;

		public BitmapFileHeader() {
			setAlignType(Structure.ALIGN_NONE);
		}

		@Override
		public byte[] getBytes() {
			return StructureByteConverter.getBytes(this, getFields(true));
		}

        @Override
        protected List<String> getFieldOrder()
        {
            return Arrays.asList(new String[] { "bfType", "bfSize", "bfReserved1", "bfReserved2", "bfOffBits" });
        }
	}

	public static class BitmapInfoHeader extends WinGDI.BITMAPINFOHEADER implements ByteStructure {

		@Override
		public byte[] getBytes() {
			return StructureByteConverter.getBytes(this, getFields(true));
		}

	}

	public GDI32Lib INSTANCE = (GDI32Lib)Native.loadLibrary("gdi32", GDI32Lib.class, W32APIOptions.DEFAULT_OPTIONS);

	HFONT CreateFont(int nHeight, int nWidth, int nEscapement, int nOrientation, int fnWeight, int fdwItalic, int fdwUnderline, int fdwStrikeOut, int fdwCharSet, int fdwOutputPrecision, int fdwClipPrecision, int fdwQuality, int fdwPitchAndFamily, String lpszFace);
	int SetBkMode(HDC hdc, int flags);
	int SetBkColor(HDC hdc, int colorref);
	int SetTextColor(HDC hdc, int colorref);
	HDC CreateDC(String driver, String device, String output, Structure deviceMode);
	boolean GetTextExtentPoint32(HDC hdc, String text, int length, SIZE size);
	boolean TextOut(HDC hdc, int x, int y, String text, int length);

}
