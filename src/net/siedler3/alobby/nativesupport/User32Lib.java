package net.siedler3.alobby.nativesupport;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.win32.W32APIOptions;

public interface User32Lib extends User32{
	public User32Lib INSTANCE = (User32Lib)Native.loadLibrary("user32", User32Lib.class, W32APIOptions.DEFAULT_OPTIONS);
	public long KEYEVENTF_KEYUP = 2;

	public final int MONITOR_DEFAULTTONEAREST = 0x00000002;
    public static class HMONITOR extends WinNT.HANDLE
    {

        public HMONITOR()
        {
        }

        public HMONITOR(Pointer p)
        {
            super(p);
        }
    }

    public static class MONITORINFO extends Structure
    {
        public int cbSize;
        public RECT rcMonitor;
        public RECT rcWork;
        public int dwFlags;

        @Override
        protected List<String> getFieldOrder()
        {
            return Arrays.asList(new String[] {
                            "cbSize", "rcMonitor", "rcWork", "dwFlags"
                        });
        }

        public MONITORINFO()
        {
        }

        public MONITORINFO(Pointer memory)
        {
            super(memory);
            read();
        }


        public static class ByReference extends MONITORINFO implements Structure.ByReference
        {
            public ByReference()
            {
            }

            public ByReference(Pointer memory)
            {
                super(memory);
            }
        }
    }


	int DrawText(HDC hdc, String text, int uCount, RECT rect, int format);
	boolean IsWindow(HWND hwnd);
	short VkKeyScan(char c);
	com.sun.jna.platform.win32.WinUser.HMONITOR MonitorFromWindow(HWND hwnd, int dwFlags);
	boolean GetMonitorInfo(com.sun.jna.platform.win32.WinUser.HMONITOR hMonitor, MONITORINFO.ByReference lpmi);
	short GetKeyState(int nVirtKey);

}
