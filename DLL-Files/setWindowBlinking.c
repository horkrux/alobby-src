/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2015 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include "unicodeHandling.h"
#include "setWindowBlinking.h"
#include <jni.h>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

JNIEXPORT void JNICALL Java_net_siedler3_alobby_gui_additional_JNIHighlighting_setWindowBlinking
  (JNIEnv *env, jclass cla, jstring frameTitle, jint time, jint maximalFlashingTime)
{
	LPCTSTR windowName = (*env)->GET_STRING_CHARS(env, frameTitle, NULL);
    HWND mainFrame = FindWindow(NULL, windowName);
    int i;
    if (mainFrame != NULL)
    {
        Sleep(time);
        for (i = 0 ; i <= maximalFlashingTime ; i = i + time)
        {
            if (mainFrame == GetForegroundWindow())
                break;
            FlashWindow(mainFrame, 1);
            Sleep(time);

        }
        FlashWindow(mainFrame, 0);
        FlashWindow(mainFrame, 1);
    }
    (*env)->RELEASE_STRING_CHARS(env, frameTitle, windowName);
}
