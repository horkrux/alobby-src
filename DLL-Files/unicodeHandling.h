/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2015 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */

/*
 * unicodeHandling.h
 *
 */

#ifndef UNICODEHANDLING_H_
#define UNICODEHANDLING_H_

#ifdef UNICODE
#define GET_STRING_CHARS GetStringChars
#define RELEASE_STRING_CHARS ReleaseStringChars
#else
#define GET_STRING_CHARS GetStringUTFChars
#define RELEASE_STRING_CHARS ReleaseStringUTFChars
#endif

#endif /* UNICODEHANDLING_H_ */
