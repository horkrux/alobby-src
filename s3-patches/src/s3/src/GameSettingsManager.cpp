#include <ThiefPatch.h>
#include <RandomBalancingPatch.h>
#include <WaterworksWorkareaPatch.h>
#include <debug_console.h>
#include "GameSettingsManager.h"
#include <CoalBugFix.h>
#include "SpellPatch.h"

typedef struct settingsChunk: file_chunk_header {
    PatchGameSettings settings;
} settings_chunk;

void GameSettingsManager::Update(PatchGameSettings newSettings) {
    if (newSettings == m_gameSettings) {
        return;
    }

    dprintf("Updating game settings (%s ---> %s)\n", m_gameSettings.GetDescription(&m_englishI18n, true, true).c_str(), newSettings.GetDescription(&m_englishI18n, true, true).c_str());

    if (m_gameSettings.useRevisedThief() != newSettings.useRevisedThief()
            || m_gameSettings.isStealingDisallowed() != newSettings.isStealingDisallowed()) {
        if (m_gameSettings.useRevisedThief()) {
            m_patchManager->disable<ThiefPatch>();
        }

        if (newSettings.useRevisedThief()) {
            m_patchManager->enable(new ThiefPatch(m_i18n, newSettings.isStealingDisallowed()));
        }
    }

    if (m_gameSettings.useRevisedRandomBalancing() != newSettings.useRevisedRandomBalancing()) {
        if (m_gameSettings.useRevisedRandomBalancing()) {
            m_patchManager->disable<RandomBalancingPatch>();
            m_patchManager->disable<WaterworksWorkareaPatch>();
            m_patchManager->disable<SpellPatch>();
        }

        if (newSettings.useRevisedRandomBalancing()) {
            m_patchManager->enable(new RandomBalancingPatch);
            m_patchManager->enable(new WaterworksWorkareaPatch);
            m_patchManager->enable(new SpellPatch);
        }
    }

    if (m_gameSettings.useIncreasedTransportLimit() != newSettings.useIncreasedTransportLimit()) {
        if (m_gameSettings.useIncreasedTransportLimit()) {
            m_patchManager->disable<CoalBugFix>();
        }

        if (newSettings.useIncreasedTransportLimit()) {
            m_patchManager->enable(new CoalBugFix());
        }
    }

    m_gameSettings = newSettings;
}

void GameSettingsManager::OnLoadSave(logic_thread* logicThread, void* saveFile) {
    auto chunk = reinterpret_cast<settings_chunk*>(s3_file_chunk_read(saveFile, FILE_CHUNK_PATCH_GAME_SETTINGS));
    if (chunk) {
        dprintf("Restoring saved settings (%s)\n", chunk->settings.GetDescription(&m_englishI18n, true, true).c_str());
        Update(chunk->settings);
    } else {
        dprintf("Settings chunk not found\n");
        Update(0);
    }
    s3_file_chunk_free(saveFile, FILE_CHUNK_PATCH_GAME_SETTINGS);
}

void GameSettingsManager::OnSave(logic_thread* logicThread, void* saveFile) {
    dprintf("Writing settings chunk\n");
    settings_chunk chunk;
    chunk.metaInfo = 0;
    chunk.type = FILE_CHUNK_PATCH_GAME_SETTINGS;
    chunk.size = sizeof(settings_chunk);
    chunk.settings = m_gameSettings;
    s3_file_chunk_write(saveFile, FILE_CHUNK_PATCH_GAME_SETTINGS, &chunk);
}

std::string GameSettingsManager::getRulesDescription() {
    return m_gameSettings.GetDescription(&m_englishI18n, true, true);
}
