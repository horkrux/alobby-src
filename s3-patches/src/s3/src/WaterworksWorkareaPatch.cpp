
#include "constants.h"
#include "debug_console.h"
#include "s3.h"
#include "WaterworksWorkareaPatch.h"
#include <asmjit/x86.h>

using namespace asmjit;

//#define WW_DEBUG 1

void on_check_coordinate(hl::CpuContext* ctx) {
    int xCoord = ctx->EBX;
    int yCoord = ctx->EAX;

    dprintf("Checking coordinates for water [%d,%d]\n", xCoord, yCoord);
}

void on_select_water_location(hl::CpuContext* ctx) {
    int xCoord = ctx->EBX;
    int yCoord = ctx->ECX;
    int waterworksSettlerIndex = ctx->EBP;
    game_data* gameData = reinterpret_cast<game_data*>(ctx->ESI);

    dprintf("Collecting water at [%d,%d] (work location index: %d)\n", xCoord, yCoord, gameData->settlerData[waterworksSettlerIndex].workLocationIndex);
}

void WaterworksWorkareaPatch::enable() {
#ifdef WW_DEBUG
    hooker.hookDetour(Constants::S3_EXE + 0xF3048, 7, on_check_coordinate);
    hooker.hookDetour(Constants::S3_EXE + 0xf318d, 5, on_select_water_location);
#endif

    Environment env;
    env.setArch(Environment::kArchX86);
    CodeHolder code;
    code.init(env, Constants::S3_EXE + 0xF3024);
    x86::Assembler a(&code);
    a.jnz((uint64_t*)(Constants::S3_EXE + 0xF3177));

    maintainWorkLocation.apply(Constants::S3_EXE + 0xF3024, (char*)code.textSection()->data(), code.textSection()->bufferSize());
}

void WaterworksWorkareaPatch::disable() {
    maintainWorkLocation.revert();
}
