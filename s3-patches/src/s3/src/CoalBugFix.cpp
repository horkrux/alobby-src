#include "CoalBugFix.h"
#include <asmjit/x86.h>
#include <constants.h>
#include <MachineCodePatchBuilder.h>

using namespace asmjit;

void CoalBugFix::enable() {
    MachineCodePatchBuilder builder(Constants::S3_EXE + 0xE4461);
    builder.padNop(20);
    builder.apply(&m_processingExitConditionPatch);
}

void CoalBugFix::disable() {
    m_processingExitConditionPatch.revert();
}
