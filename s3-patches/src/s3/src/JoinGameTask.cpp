#include "debug_console.h"
#include "FunctionHook.h"
#include "JoinGameTask.h"
#include "constants.h"
#include "s3.h"
#include "S3EventDispatcher.h"
#include <imgui_spinner.h>
#include <imgui_utils.h>

static SELECT_SESSION_PROC s3_select_session_proxy = reinterpret_cast<SELECT_SESSION_PROC>(Constants::S3_EXE + 0x7ECC0);

#define ACTION_SEARCH_GAME 1
#define ACTION_SELECT_GAME 2
#define ACTION_JOIN 3

void __fastcall select_session(connection_dialog *This, void *notUsed, int a2, char a3) {
    s3_select_session_proxy(This, a2, a3);
    dprintf("select_session(%d, %d)\n", a2, a3);
}

void JoinGameTask::OnRenderFrame(RenderContext* context) {
    if (isCompleted || ImGui::GetIO().DisplaySize.x <= 0) {
        return;
    }

    ImGuiWidgets::LoadingOverlay(m_i18n->__("joining_game", "Joining Game...").c_str());
}


void JoinGameTask::install() {
#ifndef NDEBUG
    m_selectSessionHook.hook((void**)&s3_select_session_proxy, (void*)select_session);
#endif
}

void JoinGameTask::onCompleted() {
    if (isCompleted) {
        return;
    }
    isCompleted = true;
}

JoinGameTask::~JoinGameTask() {
    onCompleted();
}

void JoinGameTask::OnBeforeMenuDrawLoop(lp_menu_draw_thread* menuDrawThread) {
    if (isCompleted) {
        return;
    }

    std::chrono::duration<double> runtimeSeconds = std::chrono::system_clock::now() - startTime;

    if (menuDrawThread->activeDialogId == 0) {
        s3_update_menu_screen(menuDrawThread, START_MENU_MULTIPLAYER_LAN_BTN);
    } else if (menuDrawThread->activeDialogId == 2 && menuDrawThread->activeDialog) {
        if (!menuDrawThread->menuSharedData->isDirectPlayActionRunning) {
            connection_dialog* connectionDialog = (connection_dialog*)menuDrawThread->activeDialog;
            menu_shared_data* menuData = menuDrawThread->menuSharedData;

            if (menuData->numConnectionShortcuts > 0) {
                if (connectionDialog->directplayProviderList->selectedValue == -1) {
                    dprintf("Selecting conn provider\n");
                    connectionDialog->directplayProviderList->selectedValue = 0;
                    s3_select_conn_provider(connectionDialog, 0);
                }

                // The provider change is not active immediately as it has to be processed by the menu net thread first,
                // so we have to check again for the value here until it becomes selected.
                if (connectionDialog->directplayProviderList->selectedValue != -1) {
                    if (connectionDialog->ipInput->inputValue.actualTextLength <= 0) {
                        dprintf("Updating IP value\n");
                        if (!playerNickname.empty()) {
                            str_struct nickname;
                            s3_str_init(&nickname, playerNickname.c_str());
                            s3_str_init(&connectionDialog->nicknameInput->inputValue, &nickname);
                        }

                        str_struct ipAddr;
                        s3_str_init(&ipAddr, ipAddress.c_str());
                        s3_str_init(&connectionDialog->ipInput->inputValue, &ipAddr);
                    }

                    if (connectionDialog->runningSessions.numSessions <= 0) {
                        if (acquireActionPermission(ACTION_SEARCH_GAME)) {
                            dprintf("Searching games\n");
                            s3_search_games(connectionDialog, 0);
                        }
                    } else {
                        auto session = connectionDialog->runningSessions.sessions[0];

                        if (s3_get_selected_list_item_value(connectionDialog->gamesList) == -1) {
                            if (acquireActionPermission(ACTION_SELECT_GAME)) {
                                dprintf("Selecting session\n");
                                s3_select_session(connectionDialog, 0, 0);
                            }
                        } else {
                            if (acquireActionPermission(ACTION_JOIN)) {
                                s3_join_session(connectionDialog, 0);
                                onCompleted();

                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    // If we haven't been able to join within 20 seconds, let's just abort. The game might have been
    // closed by the host or there may be a connection issue.
    if (runtimeSeconds.count() >= 20) {
        onCompleted();

        str_struct errorMessage;
        s3_str_init(&errorMessage, utf8ToWindows(m_i18n->__("automatic_join_failed_error", "Automatic Join failed.\n\nThe host may have closed the game or there might be a connection issue.\n\nCheck the game status in the aLobby client.")).c_str());
        s3_show_message_box(menuDrawThread, &errorMessage, MESSAGE_BOX_OK_BUTTON);
        s3_update_menu_screen(menuDrawThread, START_MENU_MULTIPLAYER_LAN_BTN);
    }
}

bool JoinGameTask::acquireActionPermission(int action) {
    auto now = std::chrono::system_clock::now();
    if (m_lastAction == action) {
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - m_lastActionAt);

        if (duration.count() <= 500) {
            return false;
        }
    }

    m_lastAction = action;
    m_lastActionAt = now;

    return true;
}
