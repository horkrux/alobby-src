#include <debug_console.h>
#include "ObserverModePatch.h"
#include <set>

void ObserverModePatch::enable() {
}

void ObserverModePatch::disable() {
}

ObserverModePatch::ObserverModePatch(ObserverMode* observerMode):
    listener(observerMode) {
}

ObserverModePatch::~ObserverModePatch() {
}

void ObserverModePatch::ObserverListener::OnBeforeGameLoop(logic_thread* logicThread) {
    if (m_observerMode->isActive() || logicThread->currentGameTick % 50 != 0) {
        return;
    }

    CheckIfActivate(logicThread);
}

void ObserverModePatch::ObserverListener::OnAfterGameInit(logic_thread* logicThread) {
    m_observerMode->disable();
    std::set<int> teamIds;
    for (short i=0;i<20;i++) {
        auto player = &logicThread->gameData->players[i];
        if (player->type != PLAYER_TYPE_EMPTY) {
            teamIds.insert(player->team);
        }
    }
    m_hasMoreThan2Teams = teamIds.size() > 2;

    CheckIfActivate(logicThread);
}

void ObserverModePatch::ObserverListener::CheckIfActivate(logic_thread* logicThread) {
    if (!m_hasMoreThan2Teams || m_observerMode->isActive()) {
        return;
    }

    std::set<int> aliveTeams;
    s3_update_alive_teams(logicThread, &aliveTeams, false);
    auto ownTeam = logicThread->gameData->players[logicThread->gameData->actingPlayer].team;
    if (aliveTeams.find(ownTeam) != aliveTeams.end() || aliveTeams.size() < 2) {
        return;
    }

    m_observerMode->enable();

    str_struct_wchar msg1;
    s3_str_init(&msg1, "You are now in observer mode.");
    s3_add_chat_message(logicThread, &msg1, s3_get_player_color(logicThread->gameData->actingPlayer));

    str_struct_wchar msg2;
    s3_str_init(&msg2, "The entire map is visible, but chatting is restricted to other observers.");
    s3_add_chat_message(logicThread, &msg2, s3_get_player_color(logicThread->gameData->actingPlayer));
}

ObserverModePatch::ObserverListener::ObserverListener(ObserverMode* observerMode): m_observerMode(observerMode) {
}

void ObserverMode::enable() {
    m_isActive = true;
    s3_set_map_visibility(true);
}

void ObserverMode::disable() {
    m_isActive = false;
    s3_set_map_visibility(false);
}

bool ObserverMode::isActive() {
    return m_isActive;
}
