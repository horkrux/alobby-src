/**
 * START GDIPLUS HEADER FIXES
 * Taken from Stackoverflow here:
 * https://stackoverflow.com/questions/45957830/gdipluspath-throws-ambiguous-byte-for-cstddef-and-rpcndr-h/45958334#45958334
 */

//  global compilation flag configuring windows sdk headers
//  preventing inclusion of min and max macros clashing with <limits>
#define NOMINMAX 1

//  override byte to prevent clashes with <cstddef>
#define byte win_byte_override

#include <Windows.h> // gdi plus requires Windows.h
// ...includes for other windows header that may use byte...

//  Define min max macros required by GDI+ headers.
#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#else
#error max macro is already defined
#endif
#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#else
#error min macro is already defined
#endif

#include <gdiplus.h>

//  Undefine min max macros so they won't collide with <limits> header content.
#undef min
#undef max

//  Undefine byte macros so it won't collide with <cstddef> header content.
#undef byte
/**
 * END GDIPLUS HEADER FIXES
 */

#include "debug_console.h"
#include "StatsScreenPatch.h"
#include <ShlObj.h>
#include <ctime>
#include "ipc_events.h"
#include <boost/algorithm/string/predicate.hpp>

using namespace Gdiplus;

void StatsScreenPatch::enable() {
    GdiplusStartupInput gdiplusStartupInput;
    GdiplusStartup(&m_gdiPlusToken, &gdiplusStartupInput, NULL);
}

void StatsScreenPatch::disable() {
    GdiplusShutdown(m_gdiPlusToken);
}

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
    UINT  num = 0;          // number of image encoders
    UINT  size = 0;         // size of the image encoder array in bytes

    ImageCodecInfo* pImageCodecInfo = NULL;

    GetImageEncodersSize(&num, &size);
    if(size == 0)
        return -1;  // Failure

    pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
    if(pImageCodecInfo == NULL)
        return -1;  // Failure

    GetImageEncoders(num, size, pImageCodecInfo);

    for(UINT j = 0; j < num; ++j)
    {
        if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
        {
            *pClsid = pImageCodecInfo[j].Clsid;
            free(pImageCodecInfo);
            return j;  // Success
        }
    }

    free(pImageCodecInfo);
    return -1;  // Failure
}

void StatsScreenPatch::OnAfterMenuDrawLoop(lp_menu_draw_thread* menuDrawThread) {
    auto lastDialogId = m_lastDialogId;
    m_lastDialogId = menuDrawThread->activeDialogId;
    if (lastDialogId == m_lastDialogId || m_lastDialogId != STATS_SCREEN_DIALOG_ID) {
        return;
    }

    auto ddSurface = s3_get_dd_surface(menuDrawThread->drawer->primarySurface);

    HDC hWindowDC;
    ddSurface->GetDC(&hWindowDC);
    HDC  hCaptureDC = CreateCompatibleDC(hWindowDC);

    int width = menuDrawThread->drawer->width;
    int height = menuDrawThread->drawer->height;

    dprintf("Copying window (width=%d, height=%d)\n", width, height);

    HBITMAP hCaptureBitmap = CreateCompatibleBitmap(hWindowDC, width, height);
    SelectObject(hCaptureDC, hCaptureBitmap);
    BitBlt(hCaptureDC, 0, 0, width, height, hWindowDC, 0, 0, SRCCOPY);

    CLSID   encoderClsid;
    auto encoderRs = GetEncoderClsid(L"image/png", &encoderClsid);
    if (encoderRs < 0) {
        dprintf("Encoder not found\n");
        return;
    }

    Bitmap image(hCaptureBitmap, NULL);
    char cFilename[MAX_PATH];

    std::wstring filename;
    if (m_screenDirectory.empty()) {
        PWSTR folderPath = NULL;
        SHGetKnownFolderPath(FOLDERID_RoamingAppData, 0, NULL, &folderPath);
        filename.append(folderPath);
        CoTaskMemFree(folderPath);

        filename.append(L"\\aLobby\\screens\\");
    } else {
        wchar_t wDirectoryName[MAX_PATH];
        mbstowcs(wDirectoryName, m_screenDirectory.c_str(), MAX_PATH);
        filename.append(wDirectoryName);

        if (!boost::algorithm::ends_with(m_screenDirectory, "\\")) {
            filename.append(L"\\");
        }
    }

    char currentTimestamp[20];
    time_t t = time(NULL);
    strftime(currentTimestamp, 20, "%Y-%m-%d_%H.%M.%S", localtime(&t));

    wchar_t wCurrentTimestamp[20];
    mbstowcs(wCurrentTimestamp, currentTimestamp, 20);
    filename.append(wCurrentTimestamp);
    filename.append(L".png");

    wcstombs(cFilename, filename.c_str(), MAX_PATH);

    dprintf("Saving image to %s\n", cFilename);
    Status stat = image.Save(filename.c_str(), &encoderClsid, NULL);
    if (stat != Ok) {
        dprintf("Saving screen Failure: stat = %d\n", stat);
        dprintf("OK = %d, Win32Error=%d\n", Ok, Win32Error);
    } else {
        ipc_event_stats_screen_available_data eventData;
        strncpy(eventData.screenPath, cFilename, sizeof(eventData.screenPath));
        m_ipcServer->SendEvent(IPC_EVENT_TYPE_STATS_SCREEN_AVAILABLE, reinterpret_cast<BYTE*>(&eventData), sizeof(eventData));
    }

    ddSurface->ReleaseDC(hWindowDC);
    DeleteDC(hCaptureDC);
    DeleteObject(hCaptureBitmap);
}
