#include <constants.h>
#include "DsoundPatch.h"
#include <dsound.h>
#include "debug_console.h"
#include <comdef.h>

#define SOUND_EFFECT_SWORD_FIGHT 30
#define SOUND_EFFECT_DYING_MALE 35
#define SOUND_EFFECT_DYING_FEMALE 113
//#define DEBUG_SOUND 1

typedef int (__thiscall *HANDLE_DS_RESULT_PROC)(void *This, int a2, char* a3);
HANDLE_DS_RESULT_PROC real_s3_handle_ds_result = reinterpret_cast<HANDLE_DS_RESULT_PROC>(Constants::S3_EXE + 0x1C2000);

typedef void (__thiscall *PLAY_SOUND_EFFECT_PROC)(void *soundEffect, LONG volume, LONG lPan, DWORD playFlags);
PLAY_SOUND_EFFECT_PROC s3_play_sound_effect = reinterpret_cast<PLAY_SOUND_EFFECT_PROC>(Constants::S3_EXE + 0x1C2FC0);

typedef void* (__thiscall *GET_SOUND_EFFECT_PROC)(void *soundData, unsigned int effectId_1, int a3);
GET_SOUND_EFFECT_PROC s3_get_sound_effect = reinterpret_cast<GET_SOUND_EFFECT_PROC>(Constants::S3_EXE + 0x1FF4F0);

static unsigned int g_lastEffectId = 0;
static int g_fightingSoundAdjustment = -1500;

int __fastcall handle_ds_result(void* This, void* notUsed, int resultCode, char* currentOperation) {
    if (resultCode != DS_OK) {
        _com_error dsErr(resultCode);
        dprintf("Direct Sound Error: %s (%d, InvalidCall: %ld, BufferToSmall: %ld)\n", dsErr.ErrorMessage(), resultCode, DSERR_INVALIDCALL, DSERR_BUFFERTOOSMALL);
    }

    return real_s3_handle_ds_result(This, resultCode, currentOperation);
}

void __fastcall play_sound_effect(void* soundEffect, void* notUsed, LONG volume, LONG lPan, DWORD playFlags) {
#ifdef DEBUG_SOUND
    dprintf("Playing sound effect (id=%d, volume=%ld, lPan=%ldd, flags=%lu)\n", g_lastEffectId, volume, lPan, playFlags);
#endif

    if (g_lastEffectId == SOUND_EFFECT_SWORD_FIGHT) {
        volume += g_fightingSoundAdjustment;
    } else if (g_lastEffectId == SOUND_EFFECT_DYING_MALE || g_lastEffectId == SOUND_EFFECT_DYING_FEMALE) {
        volume += g_fightingSoundAdjustment;
    }

    s3_play_sound_effect(soundEffect, volume, lPan, playFlags);
}

void* __fastcall get_sound_effect(void* soundData, void* notUsed, unsigned int effectId, int a3) {
    g_lastEffectId = effectId;

    return s3_get_sound_effect(soundData, effectId, a3);
}

void DsoundPatch::enable() {
    m_resultHandlingHook.hook(reinterpret_cast<void**>(&real_s3_handle_ds_result), reinterpret_cast<void*>(handle_ds_result));
    m_playSoundEffectHook.hook(reinterpret_cast<void**>(&s3_play_sound_effect), reinterpret_cast<void*>(play_sound_effect));
    m_getSoundEffectHook.hook(reinterpret_cast<void**>(&s3_get_sound_effect), reinterpret_cast<void*>(get_sound_effect));
}

void DsoundPatch::disable() {
    m_resultHandlingHook.revert();
}

void DsoundPatch::OnBeforeUpdateDrawData(logic_thread* logicThread) {
    g_fightingSoundAdjustment = static_cast<int>(static_cast<float>(m_config->getFightingSoundVolume()) / 100.0f * 5000.f) - 5000;
}
