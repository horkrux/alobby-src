#include "BuilderPathFindingPatch.h"
#include "constants.h"
#include "s3.h"
#include "debug_console.h"

typedef int (__fastcall * PATH_FINDING_WHEN_STUCK_PROC)(game_data *This, void* notUsed, game_data_settler *settler, void *pathFindingData);
PATH_FINDING_WHEN_STUCK_PROC original_find_path_when_not_moved_since_last_tick =
    reinterpret_cast<PATH_FINDING_WHEN_STUCK_PROC>(Constants::S3_EXE + 0xA3AB0);

int __fastcall new_find_path_when_not_moved_since_last_tick(game_data *gameData, void* notUsed, game_data_settler *settler, void *pathFindingData) {
    if (settler->type == SETTLER_TYPE_BUILDER
            && settler->currentState == SETTLER_IS_HEADING_TO_CONSTRUCTION_SITE) {

        game_data_map_tile* targetMapTile = &gameData->mapTiles[settler->targetXCoord][settler->targetYCoord];
        int result = original_find_path_when_not_moved_since_last_tick(gameData, notUsed, settler, pathFindingData);

        if (targetMapTile->settlerIndex) {
            game_data_settler* blockingSettler = &gameData->settlerData[targetMapTile->settlerIndex];
            dprintf("Builder of player %d blocked at [%d, %d] with target [%d, %d] by settler of type %d in state %d -> path-finding result: %d.\n",
                    settler->owningPlayer, settler->xCoord, settler->yCoord, settler->targetXCoord, settler->targetYCoord, blockingSettler->type, blockingSettler->currentState,
                    result);

            return result;

            return 255; // The default value as if this method had not been called. Path finding will proceed with second
                        // bitmask check: if (settler->bitmask & 2).
        } else {
            dprintf("Builder of player %d blocked at [%d, %d] with target [%d, %d] -> path-finding result: %d.\n",
                    settler->owningPlayer, settler->xCoord, settler->yCoord, settler->targetXCoord, settler->targetYCoord, result);

            return result;
        }
    }

    // Continue regular code path for: if (settler->bitmask & 1)
    return original_find_path_when_not_moved_since_last_tick(gameData, notUsed, settler, pathFindingData);
}

void BuilderPathFinding::enable() {
    pathFindingHook.hook((void**)&original_find_path_when_not_moved_since_last_tick, (void*)new_find_path_when_not_moved_since_last_tick);
    dataPatch1.apply(Constants::S3_EXE + 0x317ACC, "\x01\x00\x00\x00\x03", 5);
    dataPatch2.apply(Constants::S3_EXE + 0x31CF14, "\x01", 1);
    dataPatch3.apply(Constants::S3_EXE + 0x31CF18, "\x04", 1);
}

void BuilderPathFinding::disable() {
    pathFindingHook.revert();
    dataPatch1.revert();
    dataPatch2.revert();
    dataPatch3.revert();
}
