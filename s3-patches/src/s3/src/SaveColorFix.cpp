#include "SaveColorFix.h"
#include <asmjit/x86.h>
#include "constants.h"

using namespace asmjit;

void SaveColorFix::enable() {
    uintptr_t pos = Constants::S3_EXE + 0x10AA9E;

    Environment env;
    env.setArch(Environment::kArchX86);

    CodeHolder code;
    code.init(env, pos);
    x86::Assembler a(&code);
    CodeBuffer &buf = code.textSection()->buffer();
    a.mov(x86::ptr(x86::ebx, 4, 4), 48);
    patch.apply(pos, (char*)buf.data(), buf.size());
}
void SaveColorFix::disable() {
    patch.revert();
}
