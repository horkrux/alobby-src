#include "IpcServer.h"
#include <windows.h>
#include <debug_console.h>

#define BUFSIZE 4096
#define PIPE_TIMEOUT 5000

void IpcServer::Run() {
    char buffer[BUFSIZE];
    DWORD dwRead, dwEvent;

    m_isStarted = true;
    m_isTerminated = false;

    m_pipeActivity.hEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
    if (m_pipeActivity.hEvent == NULL) {
        dprintf("Failed to create pipe activity event (error=%lu)\n", GetLastError());
    }

    std::string lpName("\\\\.\\pipe\\");
    lpName.append(m_pipeName);
    dprintf("Creating named pipe %s (bufSize=%d)\n", lpName.c_str(), BUFSIZE * sizeof(char));

    m_hPipe = CreateNamedPipe(TEXT(lpName.c_str()),
                            PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED | FILE_FLAG_FIRST_PIPE_INSTANCE,
                            PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT | PIPE_REJECT_REMOTE_CLIENTS,
                            1,
                            BUFSIZE * sizeof(char),
                            BUFSIZE * sizeof(char),
                            PIPE_TIMEOUT,
                            NULL);
    if (m_hPipe == INVALID_HANDLE_VALUE) {
        dprintf("Failed to create named pipe (error=%lu)\n", GetLastError());
    }

    HANDLE events[2] = {m_outgoingEventHandle, m_pipeActivity.hEvent};
    m_shouldTerminate = false;

    while (!m_shouldTerminate && m_hPipe != INVALID_HANDLE_VALUE) {
        ConnectNamedPipe(m_hPipe, &m_pipeActivity);
        dwEvent = WaitForMultipleObjects(2, events, FALSE, INFINITE);
        switch (dwEvent)
        {
        case WAIT_OBJECT_0 + 0:
        {
            ResetEvent(events[0]);

            dprintf("No client connected to which to dispatch IPC event, buffering...\n");

            // If there is no client connected, we start dropping events when there are more than 50 events queued.
            m_eventLock.lock();
            int nbEvents = m_events.size();
            if (nbEvents > 50)
            {
                dprintf("Buffered event queue too large, dropping some events\n");
                do
                {
                    auto event = m_events.front();
                    m_events.pop();
                    delete event;
                    nbEvents--;
                } while (nbEvents > 40);
            }
            m_eventLock.unlock();
            break;
        }

        case WAIT_OBJECT_0 + 1:
            ResetEvent(events[1]);
            if (GetOverlappedResult(m_hPipe, &m_pipeActivity, &dwRead, TRUE)) {
                dprintf("IPC client connected; flushing buffered events\n");
                FlushEvents();

                bool isConnected = true;
                while (isConnected) {
                    m_pipeActivity.Offset = 0;
                    m_pipeActivity.OffsetHigh = 0;
                    ReadFile(m_hPipe, buffer, sizeof(buffer) - 1, NULL, &m_pipeActivity);

                    dprintf("Waiting for new events/client commands\n");
                    dwEvent = WaitForMultipleObjects(2, events, FALSE, INFINITE);
                    switch (dwEvent) {
                    case WAIT_OBJECT_0 + 0:
                        ResetEvent(events[0]);
                        FlushEvents();
                        break;

                    case WAIT_OBJECT_0 + 1:
                        ResetEvent(events[1]);
                        if (!GetOverlappedResult(m_hPipe, &m_pipeActivity, &dwRead, FALSE)) {
                            isConnected = false;
                        } else {
                            // TODO: Process received data.
                        }
                        break;
                    }
                }

                dprintf("IPC client disconnected\n");
                DisconnectNamedPipe(m_hPipe);
            } else {
                dprintf("IPC client failed to connect\n");
            }
            break;
        }
    }

    if (m_hPipe) {
        CloseHandle(m_hPipe);
        m_hPipe = NULL;
    }
    if (m_pipeActivity.hEvent) {
        CloseHandle(m_pipeActivity.hEvent);
        m_pipeActivity.hEvent = NULL;
    }

    m_isTerminated = true;
}

void IpcServer::RequestTermination() {
    if (m_shouldTerminate || m_isTerminated) {
        return;
    }

    m_shouldTerminate = true;

    // Signal outgoing events to cause a new loop iteration.
    SetEvent(m_outgoingEventHandle);
}

bool IpcServer::IsTerminated()
{
    return m_isTerminated;
}

void IpcServer::SendEvent(unsigned char eventType, BYTE* eventData, unsigned short eventDataSize)
{
    if (!m_isStarted || m_isTerminated) {
        return;
    }

    int bufSize = sizeof(ipc_event) + eventDataSize;
    BYTE* buffer = new BYTE[bufSize];
    memset(buffer, 0, bufSize);

    ipc_event evt;
    evt.eventType = eventType;
    evt.dataLength = eventDataSize;
    memcpy(&buffer[0], &evt, sizeof(ipc_event));
    memcpy(&buffer[sizeof(ipc_event)], eventData, eventDataSize);

    m_eventLock.lock();
    m_events.push(buffer);
    m_eventLock.unlock();
    if (!SetEvent(m_outgoingEventHandle)) {
        dprintf("Failed to signal outgoing events\n");
    }
}

IpcServer::~IpcServer() {
    if (m_hPipe) {
        CloseHandle(m_hPipe);
        m_hPipe = NULL;
    }
    if (m_pipeActivity.hEvent) {
        CloseHandle(m_pipeActivity.hEvent);
        m_pipeActivity.hEvent = NULL;
    }
}

void IpcServer::FlushEvents() {
    m_eventLock.lock();
    while (!m_events.empty()) {
        BYTE* event = m_events.front();
        m_events.pop();
        m_eventLock.unlock();

        ipc_event* eventHeader = reinterpret_cast<ipc_event*>(event);
        DWORD eventSize = sizeof(ipc_event) + eventHeader->dataLength;
        DWORD dwWritten;

        dprintf("Writing event (type=%d, headerSize=%d, dataSize=%d, eventSize=%lu)\n", eventHeader->eventType, sizeof(ipc_event), eventHeader->dataLength, eventSize);
        m_pipeActivity.Offset = 0;
        m_pipeActivity.OffsetHigh = 0;
        WriteFile(m_hPipe, event, eventSize, &dwWritten, &m_pipeActivity);
        if (GetLastError() == ERROR_IO_PENDING) {
            WaitForSingleObject(m_pipeActivity.hEvent, INFINITE);
            GetOverlappedResult(m_hPipe, &m_pipeActivity, &dwWritten, FALSE);
        }
        if (dwWritten != eventSize) {
            dprintf("Failed to send event (bytes-written=%lu, bytes-expected=%lu)\n", dwWritten, eventSize);
        }

        delete event;
        m_eventLock.lock();
    }
    m_eventLock.unlock();
}
