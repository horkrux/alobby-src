#include <debug_console.h>
#include <spirit_po/spirit_po.hpp>
#include "I18n.h"
#include <fstream>
#include <iostream>
#include <hacklib/MessageBox.h>

I18n::I18n(): m_activeLanguage(ENGLISH), m_catalog(loadTranslations()) {
}

std::string I18n::__(std::string translationId, std::string englishText) {
    if (m_activeLanguage == ENGLISH) {
        return std::move(englishText);
    }

    auto translatedText = m_catalog.gettext_str(translationId);
    if (translatedText == translationId) {
        return englishText;
    }

    return translatedText;
}

spirit_po::default_catalog I18n::loadTranslations() {
    auto translationFile = findTranslationFile();
    if (!translationFile.empty()) {
        dprintf("Translation-File: %s\n", translationFile.c_str());
        try {
            std::ifstream ifs(translationFile);
            std::string po_file{ std::istreambuf_iterator<char>{ ifs }, std::istreambuf_iterator<char>() };

            return std::move(spirit_po::default_catalog{ spirit_po::default_catalog::from_range(po_file) });
        } catch (std::exception& e) {
            std::string errorMessage("Failed to parse translation file '");
            errorMessage.append(translationFile);
            errorMessage.append("':\n\n");
            errorMessage.append(e.what());

            hl::MsgBox("S3 CE: Invalid translation file", errorMessage);
        }
    }

    // Load an empty catalogue.
    std::istringstream ifs("msgid \"\"\nmsgstr \"\"\n");
    std::string po_file{ std::istreambuf_iterator<char>{ ifs }, std::istreambuf_iterator<char>() };

    return std::move(spirit_po::default_catalog::from_range(po_file));
}

std::string I18n::findTranslationFile() {
    std::string expectedFilename("s3ce.");
    switch (m_activeLanguage) {
    case GERMAN:
        expectedFilename.append("de");
        break;
    case ENGLISH:
        return "";
    case ITALIAN:
        expectedFilename.append("it");
        break;
    case FRENCH:
        expectedFilename.append("fr");
        break;
    case POLISH:
        expectedFilename.append("pl");
        break;
    default:
        dprintf("Unknown language %d", m_activeLanguage);
        return "";
    }

    expectedFilename.append(".po");

    struct stat buffer;
    if (stat(expectedFilename.c_str(), &buffer) == 0) {
        return expectedFilename;
    }

    std::string resPath("res/");
    resPath.append(expectedFilename);
    if (stat(resPath.c_str(), &buffer) == 0) {
        return resPath;
    }

    return "";
}

void I18n::setActiveLanguage(Language language) {
    m_activeLanguage = language;
    m_catalog = std::move(loadTranslations());
}

std::string utf8ToWindows(std::string utf8Str) {
    auto size = MultiByteToWideChar(CP_UTF8, 0, utf8Str.c_str(), -1, 0, 0);
    std::wstring tmpStr(size, 0);
    MultiByteToWideChar(CP_UTF8, 0, utf8Str.c_str(), -1, &tmpStr[0], size);

    size = WideCharToMultiByte(CP_WINDOWS, 0, &tmpStr[0], -1, 0, 0, 0, 0);
    std::string convertedStr(size, 0);
    WideCharToMultiByte(CP_WINDOWS, 0, &tmpStr[0], -1, &convertedStr[0], size, 0, 0);

    return convertedStr;
}
