#include "GuiPatch.h"
#include <imgui.h>
#include <imgui_impl_dx9.h>
#include <imgui_impl_win32.h>
#include <implot.h>
#include <debug_console.h>
#include <imgui_utils.h>
#include <sentry.h>

typedef LRESULT (__thiscall *HANDLE_WM_PROC)(void* This, HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
HANDLE_WM_PROC s3_handle_wm = reinterpret_cast<HANDLE_WM_PROC>(Constants::S3_EXE + 0x5C860);

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

static S3EventDispatcher* g_dispatcher;

LRESULT __fastcall handle_wm(void *This, void* notUsed, HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {
    if (ImGui::GetCurrentContext() != NULL) {
        // We do not want ImGui to overwrite the Settlers cursor unless it wants to draw a more specific cursor like
        // for a text input, or a window resize or something like that.
        if (Msg != WM_SETCURSOR || ImGui::GetMouseCursor() != ImGuiMouseCursor_Arrow) {
            if (ImGui_ImplWin32_WndProcHandler(hWnd, Msg, wParam, lParam)) {
                return true;
            }
        }

        ImGuiIO& io = ImGui::GetIO();
        if (io.WantCaptureMouse && Msg >= 0x200 && Msg <= 0x20E) {
            return true;
        }

        if (io.WantCaptureKeyboard && ImGui::IsPopupOpen(nullptr, ImGuiPopupFlags_AnyPopupId)) {
            return true;
        }
    }

    if (Msg == WM_MOUSEWHEEL) {
        for (auto listener : g_dispatcher->GetWmListener()) {
            if (listener->OnWindowMessage(hWnd, Msg, wParam, lParam)) {
                return true;
            }
        }
    }

    return s3_handle_wm(This, hWnd, Msg, wParam, lParam);
}

GuiPatch::~GuiPatch() {
    terminateRenderer();
}

void GuiPatch::shutdown() {
    ImGui_ImplDX9_Shutdown();
    ImGui_ImplWin32_Shutdown();
    ImGui::DestroyContext();
    m_guiOverlay.close();
    m_needsRestore = false;
}

void GuiPatch::enable() {
    g_dispatcher = m_dispatcher;
    m_wmHook.hook(reinterpret_cast<void**>(&s3_handle_wm), reinterpret_cast<void*>(handle_wm));
    m_isActive = true;
}

void GuiPatch::disable() {
    m_wmHook.revert();
    terminateRenderer();
}

void GuiPatch::terminateRenderer() {
    m_isActive = false;
    if (m_isStarted) {
        m_renderThread.join();
        m_isStarted = false;
    }
}

void GuiPatch::render() {
    *m_guiActive = true;

    if (!init()) {
        *m_guiActive = false;
        m_gameSettingsManager->Update(0);
        return;
    }

    while (m_isActive) {
        if (m_needsRestore) {
            dprintf("Shutting down overlay and re-creating\n");
            shutdown();
            if (!init()) {
                *m_guiActive = false;
                return;
            }
        }

        if (!m_scaleSet) {
            if (ImGui::GetIO().DisplaySize.x > 0) {
                auto scaledVector = ImScaledVec2(1, 1);
                ImGuiIO& io = ImGui::GetIO();
                io.FontGlobalScale = scaledVector.y;

                ImGuiStyle& style = ImGui::GetStyle();
                auto sizeScale = scaledVector.y > scaledVector.x ? scaledVector.y : scaledVector.x;
                style.ScaleAllSizes(sizeScale);

                dprintf("Font-Scale: %f, Size-Scale: %f\n", scaledVector.y, sizeScale);

                m_scaleSet = true;
            }
        }

        ImGui_ImplDX9_NewFrame();
        ImGui_ImplWin32_NewFrame();
        ImGui::NewFrame();

        for (auto listener : m_dispatcher->GetGuiListener()) {
            listener->OnRenderFrame(&m_renderContext);
        }

        ImGui::Render();

        m_guiOverlay.beginDraw();
        ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
        if (!m_guiOverlay.swapBuffers()) {
            m_needsRestore = true;
        }
    }

    shutdown();
}

void GuiPatch::OnBeforeMenuDrawLoop(lp_menu_draw_thread* menuDrawThread) {
    if (m_isActive && !m_isStarted) {
        m_renderThread = std::move(std::thread([&] { render(); }));
        m_isStarted = true;
    }

    m_renderContext.activeDialogId = menuDrawThread->activeDialogId;
}

bool GuiPatch::init() {
    auto result = m_guiOverlay.create();
    if (result != hl::WindowOverlay::Error::Okay) {
        dprintf("failed to create GUI overlay (result: %d)\n", result);

        auto event = sentry_value_new_message_event(SENTRY_LEVEL_ERROR, nullptr, "GUI failed to initialize");
        sentry_capture_event(event);

        return false;
    }

    m_guiOverlay.setTargetRefreshRate(60);
    m_guiOverlay.registerResetHandlers(
        []{
            dprintf("Invalidating device objects\n");
            ImGui_ImplDX9_InvalidateDeviceObjects();
        },
        [&]{
            if (!ImGui_ImplDX9_CreateDeviceObjects()) {
                m_needsRestore = true;
            }
        }
    );

    ImGui::CreateContext();
    ImPlot::CreateContext();

    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;

    ImGui_ImplDX9_Init(m_guiOverlay.getContext());

    HWND hWnd = hl::WindowOverlay::GetTargetWindow();
    ImGui_ImplWin32_Init(hWnd);

    m_scaleSet = false;

    return true;
}

void GuiPatch::OnAfterGameInit(logic_thread* logicThread) {
    m_renderContext.activeDialogId = -1;
}
