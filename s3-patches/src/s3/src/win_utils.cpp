#include "win_utils.h"
#include <windows.h>

bool is_compat_mode() {
    char buf[1024];
    if (GetEnvironmentVariable("__COMPAT_LAYER", buf, sizeof(buf))) {
        char* s = strtok(buf, " ");

        while (s) {
            if (_strcmpi(s, "WIN95") == 0 || _strcmpi(s, "WIN98") == 0 || _strcmpi(s, "NT4SP5") == 0) {
                return true;
            }

            s = strtok(NULL, " ");
        }
    }

    return false;
}