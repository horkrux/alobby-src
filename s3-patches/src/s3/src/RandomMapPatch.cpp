#include <algorithm>
#include <vector>
#include <map>
#include <debug_console.h>
#include "RandomMapPatch.h"
#include "s3.h"
#include <asmjit/x86.h>

using namespace asmjit;

//#define DEBUG_START_LOCATIONS 1

// Set this to generate a specific random map (useful for debugging how changes in algorithm affect placement)
//#define RANDOM_SEED 18467

typedef void(__thiscall* RANDOM_PICK_START_LOCATIONS)(game_data* gameData);
RANDOM_PICK_START_LOCATIONS static s3_random_pick_start_locations =
    reinterpret_cast<RANDOM_PICK_START_LOCATIONS>(Constants::S3_EXE + 0x1074B0);

typedef signed int(__thiscall* CREATE_BUILDING_PROC)(game_data* This, unsigned int xCoord, unsigned int yCoord, int buildingType,
                                                     signed int playerIndex, unsigned int mode);
CREATE_BUILDING_PROC static s3_create_building = reinterpret_cast<CREATE_BUILDING_PROC>(Constants::S3_EXE + 0xD99E0);

typedef bool (__stdcall *TERRAIN_OF_TYPE_PROC)(int terrainType, int terrain);
TERRAIN_OF_TYPE_PROC static s3_is_terrain_of_type = reinterpret_cast<TERRAIN_OF_TYPE_PROC>(Constants::S3_EXE + 0x148A10);

typedef int (__stdcall *TRANSFORM_COORD_PROC)(int coord, int shiftType);
TRANSFORM_COORD_PROC static s3_transform_x = reinterpret_cast<TRANSFORM_COORD_PROC>(Constants::S3_EXE + 0x103700);
TRANSFORM_COORD_PROC static s3_transform_y = reinterpret_cast<TRANSFORM_COORD_PROC>(Constants::S3_EXE + 0x103730);

typedef void (__thiscall *UPDATE_TILE_PROC)(game_data*, int, int);
UPDATE_TILE_PROC static s3_update_height = reinterpret_cast<UPDATE_TILE_PROC>(Constants::S3_EXE + 0x119670);
UPDATE_TILE_PROC static s3_update_bitmask = reinterpret_cast<UPDATE_TILE_PROC>(Constants::S3_EXE + 0x11B7F0);

typedef void (__thiscall *TERRAIN_PROC)(game_data*, int, int, unsigned __int8, int);
TERRAIN_PROC static s3_randomly_replace_terrain = reinterpret_cast<TERRAIN_PROC>(Constants::S3_EXE + 0x119330);

typedef void (__thiscall *TERRAIN_PROC2)(game_data*, int, int);
TERRAIN_PROC2 static s3_replace_terrain = reinterpret_cast<TERRAIN_PROC2>(Constants::S3_EXE + 0x119210);

typedef void (__thiscall *RESOURCES_PROC)(game_data*, char, int);
RESOURCES_PROC static s3_add_resources = reinterpret_cast<RESOURCES_PROC>(Constants::S3_EXE + 0x11AD40);

static RandomMapPatch* g_activePatch = nullptr;

class LocationOptimizer {
private:
    typedef struct {
        unsigned int x;
        unsigned int y;
    } Position;

    // A cluster of suitable start positions
    typedef struct {
        unsigned int sumX;
        unsigned int sumY;
        unsigned short centerX;
        unsigned short centerY;
        unsigned int numPoints;
    } Cluster;

    typedef struct {
        unsigned int min;
        unsigned int max;
        double average;
        double standardDeviation;
    } Stats;

    // Stats about the chosen set of starting locations
    typedef struct {
        Stats distances;
        Stats scores;
    } LocationStats;

    typedef struct resource_field {
        unsigned int centerX;
        unsigned int centerY;
        unsigned int numCoal;
        unsigned int numIron;
        unsigned int numGold;
        unsigned int numSulfur;
        unsigned int numGems;
    } resource_field;

    typedef struct selected_clusters {
        std::vector<unsigned int> list;
        std::set<unsigned int> set;
    } selected_clusters;

    // Padding around the map where we do not place any locations.
    const static int locationPadding = 64;

    // The size of resource fields
    const static int resourceFieldSize = 16;

    // The max allowed distance when creating a cluster of locations.
    const static unsigned int maxAllowedDistance = 30;

    // The maximum distance after which further increases in distance do not increase the value of the objective function.
    // In other words, once that distance is reached, we do not further try to place positions apart, but instead focus on other factors.
    const static unsigned int awardedMinDistance = 200;

    // If there is no water within this distance, we will place a pond at the location.
    const static unsigned int maxWaterDistance = 100;

    bool m_increaseResourceYields;

    class Context {
    public:
        // Number of players in the game.
        unsigned int m_numPlayers;
        game_data* m_gameData;
        std::vector<Position> waterLocations;
        std::vector<resource_field> resourceFields;
        std::vector<Position> suitableLocations;
        std::vector<Cluster> clusters;
        std::map<unsigned int, unsigned int> scoreByCluster;
        std::map<unsigned int, std::vector<resource_field>> resourceFieldsByCluster;
        bool m_increaseResourceYields;
        unsigned int m_nextPondSize;

        Context(game_data* gameData, bool increaseResourceYields) : m_gameData(gameData), m_increaseResourceYields(increaseResourceYields) {
            updateNextPondSize();
        }

        void updateNextPondSize() {
            m_nextPondSize = 50 + s3_next_random_value(m_gameData, 50);
        }

        bool isSuitableLocation(unsigned int x, unsigned int y) {
            auto mapTile = m_gameData->mapTiles[x][y];
            if (mapTile.terrain != TERRAIN_LAND || mapTile.objects) {
                return false;
            }

            // Check if a small tower could be built in this location (simulation mode, so building is actually created there).
            if (!s3_create_building(m_gameData, x, y, BUILDING_SMALL_TOWER, 0, 2)) {
                return false;
            }

            int numFish = 0, numCoal = 0, numIron = 0, numGold = 0, numSulfur = 0, numGems = 0, numFreeLand = 0, numLand = 0, numTrees = 0,
                numStones = 0, numMountain = 0, numEmptyMountain = 0;
            for (int i = 0; i < 20000; i++) {
                auto offset = m_gameData->radiusOffsets[i];
                auto tile = m_gameData->mapTiles[x + offset.x][y + offset.y];
                auto terrain = tile.terrain;
                auto objects = tile.objects;

                if (i < 3000) {
                    if (terrain == TERRAIN_LAND) {
                        numLand++;
                        if (!objects) {
                            numFreeLand++;
                        }
                    }
                    if (objects >= OBJECTS_TREES_START && objects <= OBJECTS_TREES_END) {
                        numTrees++;
                    } else if (objects >= OBJECTS_STONES_START && objects <= OBJECTS_STONES_END) {
                        numStones += OBJECTS_STONE_CRUMBS - objects;
                    }
                }

                auto resourceType = get_resource_type(tile.resource);
                auto resourceCount = get_resource_count(tile.resource);
                if (resourceType == RESOURCE_TYPE_FISH) {
                    numFish += resourceCount;
                } else {
                    if (i < 9995 && (terrain == TERRAIN_MOUNTAIN_1 || terrain == TERRAIN_MOUNTAIN_2)) {
                        numMountain++;
                        if (resourceCount <= 0) {
                            numEmptyMountain++;
                        }

                        if (resourceType == RESOURCE_TYPE_COAL) {
                            numCoal += resourceCount;
                        } else if (resourceType == RESOURCE_TYPE_IRON) {
                            numIron += resourceCount;
                        } else if (resourceType == RESOURCE_TYPE_GOLD) {
                            numGold += resourceCount;
                        } else if (resourceType == RESOURCE_TYPE_GEMS) {
                            numGems += resourceCount;
                        } else if (resourceType == RESOURCE_TYPE_SULFUR) {
                            numSulfur += resourceCount;
                        }
                    }
                }
            }

            if (numFreeLand < 800 || numLand < 2200) {
                return false;
            }

            if (numCoal >= 800 && numIron >= 400) {
                return true;
            }

            if (numCoal >= 100 && numIron >= 50 && numEmptyMountain >= 200) {
                return true;
            }

            return false;
        }

        bool supportsPond(int x, int y) {
            for (int i = 0; i < m_nextPondSize + 100; i++) {
                auto offset = m_gameData->radiusOffsets[i];
                auto tile = &m_gameData->mapTiles[x + offset.x][y + offset.y];
                if (tile->terrain != TERRAIN_LAND) {
                    return false;
                }
                if (i < 40 && tile->objects) {
                    return false;
                }
            }

            return true;
        }

        void addStream(int centerX, int centerY, int x, int y) {
            int xDirection = centerX < x ? 1 : -1;
            int yDirection = centerY < y ? 1 : -1;

            int maxLength = 4;
            int length = 0;
            unsigned char nextDirection = 0;
            while (length < maxLength) {
                auto terrain = m_gameData->mapTiles[x][y].terrain;
                if (terrain != TERRAIN_LAND && terrain != 48) {
                    break;
                }

                auto newTerrain = TERRAIN_STREAM;
                if (length <= 0) {
                    newTerrain = TERRAIN_STREAM_4;
                } else if (length <= 1) {
                    newTerrain = TERRAIN_STREAM_3;
                } else if (length <= 2) {
                    newTerrain = TERRAIN_STREAM_2;
                }

                m_gameData->mapTiles[x][y].terrain = newTerrain;
                length++;

                switch (nextDirection) {
                case 0:
                    x += xDirection;
                    break;
                case 1:
                    y += yDirection;
                    break;
                case 2:
                    x += xDirection;
                    y += yDirection;
                    break;
                }
                nextDirection = (nextDirection + 1) % 3;
            }
        }

        void addPond(int x, int y) {
            std::vector<Position> streams;
            unsigned char maxHeight = 0;
            for (int i = 30; i < 40; i++) {
                auto offset = m_gameData->radiusOffsets[i];
                auto tile = &m_gameData->mapTiles[x + offset.x][y + offset.y];
                if (tile->height > maxHeight) {
                    maxHeight = tile->height;
                }
            }

            int pondSize = m_nextPondSize;
            updateNextPondSize();
            int waterSize = pondSize / 4;
            for (int i = 0; i < pondSize; i++) {
                auto offset = m_gameData->radiusOffsets[i];
                auto tileX = x + offset.x;
                auto tileY = y + offset.y;
                auto tile = &m_gameData->mapTiles[tileX][tileY];

                if (i < waterSize) {
                    if (i < waterSize / 2) {
                        tile->terrain = TERRAIN_WATER_LEVEL_2;
                    } else {
                        tile->terrain = TERRAIN_WATER_LEVEL_1;
                    }

                    if (s3_next_random_value(m_gameData) >= 0x9C40) {
                        int resourceValue = 5 + s3_next_random_value(m_gameData, 10);
                        if (m_increaseResourceYields) {
                            resourceValue = std::min(15, resourceValue * 2);
                        }
                        tile->resource = create_resource(RESOURCE_TYPE_FISH, resourceValue);
                    }
                } else if (i == waterSize || i == waterSize + 3 || i == waterSize + 6 || i == waterSize + 9) {
                    streams.push_back({ static_cast<unsigned int>(x + offset.x), static_cast<unsigned int>(y + offset.y) });
                } else {
                    tile->terrain = TERRAIN_SHORE;
                }

                s3_update_bitmask(m_gameData, tileX, tileY);
            }

            for (auto pos : streams) {
                addStream(x, y, pos.x, pos.y);
            }
        }

        bool isSwampOrLand(int xCoord, int yCoord) {
            auto terrain = m_gameData->mapTiles[xCoord][yCoord].terrain;

            return terrain == TERRAIN_LAND || terrain == TERRAIN_SWAMP || terrain == 241;
        }

        void addSwamp(unsigned int startX, unsigned int startY) {
            unsigned short swampsAdded = 0;
            unsigned short attempts = 0;
            short lastX = 0;
            short lastY = 0;
            while (swampsAdded < 2 && attempts < 50) {
                auto offset = m_gameData->radiusOffsets[1000 + attempts * 100 + s3_next_random_value(m_gameData, 1000)];
                short x = startX + offset.x;
                short y = startY + offset.y;

                if (s3_calculate_distance(lastX, lastY, x, y) < 10) {
                    continue;
                }

                if (supportsSwamp(x, y)) {
                    addSwampAround(x, y);
                    lastX = x;
                    lastY = y;
                    swampsAdded += 1;
                }

                attempts += 1;
            }
        }

        bool supportsSwamp(int x, int y) {
            if (x-1 < 0 || y-1 < 0 || x+1 >= m_gameData->mapSize || y+1 >= m_gameData->mapSize) {
                return false;
            }

            if (m_gameData->mapTiles[x][y].objects) {
                return false;
            }

            return isSwampOrLand(x+1, y)
                   && isSwampOrLand(x, y+1)
                   && isSwampOrLand(x-1, y)
                   && isSwampOrLand(x, y-1)
                   && isSwampOrLand(x+1, y+1)
                   && isSwampOrLand(x-1, y-1)
                   && isSwampOrLand(x-1, y+1)
                   && isSwampOrLand(x+1, y-1);
        }

        void addSwampAround(int centerX, int centerY) {
            m_gameData->mapTiles[centerX][centerY].terrain = 241;
            s3_randomly_replace_terrain(m_gameData, 16, 241, 240, 80);
            s3_replace_terrain(m_gameData, 240, 241);
            s3_randomly_replace_terrain(m_gameData, 16, 241, 240, 60);
            s3_replace_terrain(m_gameData, 240, 241);
            s3_randomly_replace_terrain(m_gameData, 16, 241, 240, 40);
            s3_replace_terrain(m_gameData, 240, 241);
            s3_randomly_replace_terrain(m_gameData, 16, 241, 240, 20);
            s3_replace_terrain(m_gameData, 240, 241);

            for (int i=0; i<1000; i++) {
                auto offset = m_gameData->radiusOffsets[i];
                auto x = centerX + offset.x;
                auto y = centerY + offset.y;
                auto tile = &m_gameData->mapTiles[x][y];

                if (tile->terrain == 241 && !supportsSwamp(x, y)) {
                    tile->terrain = 16;
                }
            }

            s3_replace_terrain(m_gameData, 241, TERRAIN_SWAMP);
        }

        bool isSpaceAround(int x, int y, int terrainType) {
            for (int i = 0; i<6; i++) {
                auto tileX = s3_transform_x(x, i);
                auto tileY = s3_transform_y(y, i);
                auto tile = m_gameData->mapTiles[tileX][tileY];

                if (tile.objects || tile.bitmask || !s3_is_terrain_of_type(terrainType, tile.terrain)) {
                    return false;
                }
            }

            return true;
        }

        unsigned int addStonesAround(unsigned int centerX, unsigned int centerY, unsigned int toAddCount) {
            unsigned int addedCount = 0;

            for (int i = 0; i < 3000; i++) {
                auto offset = m_gameData->radiusOffsets[i];
                unsigned int x = centerX + offset.x;
                unsigned int y = centerY + offset.y;
                auto tile = &m_gameData->mapTiles[x][y];
                if (!s3_is_terrain_of_type(4, tile->terrain)) {
                    continue;
                }
                if (tile->objects || (tile->bitmask & 15) || !isSpaceAround(x, y, 4)) {
                    continue;
                }

                tile->objects = OBJECTS_STONES_START;
                tile->bitmask |= 1;

                for (int j=0; j<6; j++) {
                    auto surroundingTile = &m_gameData->mapTiles[s3_transform_x(x, j)][s3_transform_y(y, j)];
                    surroundingTile->bitmask |= 1;
                }

                addedCount += 12;
                if (addedCount >= toAddCount) {
                    return addedCount;
                }
            }

            return addedCount;
        }

        unsigned int addStones(unsigned int startX, unsigned int startY, unsigned int toAddCount) {
            unsigned int addedCount = 0;

            // First try to increase yield of existing stones.
            for (int i = 500; i < 3000; i++) {
                auto offset = m_gameData->radiusOffsets[i];
                int x = startX + offset.x;
                int y = startY + offset.y;

                auto tile = &m_gameData->mapTiles[x][y];
                if (tile->objects >= OBJECTS_STONES_START && tile->objects <= OBJECTS_STONES_END) {
                    unsigned int currentCount = OBJECTS_STONE_CRUMBS - tile->objects;
                    tile->objects = OBJECTS_STONES_START;
                    unsigned int newCount = OBJECTS_STONE_CRUMBS - tile->objects;
                    addedCount += newCount - currentCount;
                    if (addedCount >= toAddCount) {
                        return addedCount;
                    }
                }
            }

            // Second, we try to place a new stone tile in proximity to existing stone tiles.
            for (int i = 0; i < 3000; i++) {
                auto offset = m_gameData->radiusOffsets[i];
                int x = startX + offset.x;
                int y = startY + offset.y;

                auto objects = m_gameData->mapTiles[x][y].objects;
                if (objects >= OBJECTS_STONES_START && objects <= OBJECTS_STONES_END) {
                    addedCount += addStonesAround(x, y, toAddCount - addedCount);
                    if (addedCount >= toAddCount) {
                        return addedCount;
                    }
                }
            }

            // Lastly for any remaining stones, we pick random points and add stones around them.
            for (int i = 500; i < 100; i++) {
                auto offset = m_gameData->radiusOffsets[1000 + s3_next_random_value(m_gameData, 2000)];
                int x = startX + offset.x;
                int y = startY + offset.y;

                addedCount += addStonesAround(x, y, toAddCount - addedCount);
                if (addedCount >= toAddCount) {
                    return addedCount;
                }
            }

            return addedCount;
        }

        unsigned int addTreesAround(unsigned int centerX, unsigned int centerY, unsigned int toAddCount) {
            unsigned int addedCount = 0;

            for (int i = 0; i < 100; i++) {
                auto offset = m_gameData->radiusOffsets[i];
                int x = centerX + offset.x;
                int y = centerY + offset.y;
                auto tile = &m_gameData->mapTiles[x][y];
                if (tile->objects || (tile->bitmask & 9) || tile->terrain != TERRAIN_LAND || !isSpaceAround(x, y, 4)) {
                    continue;
                }

                tile->objects = OBJECTS_TREES_START + s3_next_random_value(m_gameData, 5);
                tile->bitmask |= 1;
                addedCount++;
                if (addedCount >= toAddCount) {
                    return addedCount;
                }
            }

            return addedCount;
        }

        unsigned int addTrees(unsigned int startX, unsigned int startY, unsigned int toAddCount) {
            unsigned int addedCount = 0;

            // We first try to place more trees in proximity to existing trees.
            for (int i = 500; i < 3000; i++) {
                auto offset = m_gameData->radiusOffsets[i];
                int centerX = startX + offset.x;
                int centerY = startY + offset.y;

                auto objects = m_gameData->mapTiles[centerX][centerY].objects;
                if (objects >= OBJECTS_TREES_START && objects <= OBJECTS_TREES_END) {
                    addedCount += addTreesAround(centerX, centerY, toAddCount - addedCount);
                    if (addedCount >= toAddCount) {
                        return addedCount;
                    }
                }
            }

            // For any remaining trees, we are picking random points and try to place trees in that location.
            for (int i = 0; i < 100; i++) {
                auto offset = m_gameData->radiusOffsets[1000 + s3_next_random_value(m_gameData, 2000)];
                int centerX = startX + offset.x;
                int centerY = startY + offset.y;

                addedCount += addTreesAround(centerX, centerY, toAddCount - addedCount);
                if (addedCount >= toAddCount) {
                    return addedCount;
                }
            }

            return addedCount;
        }

        int addResourceAround(int x, int y, int resourceType, int toAddCount) {
            int addedCount = 0;
            int mineSize = 32 + (s3_next_random_value(m_gameData) & 0x3F);
            int skipProbability = (s3_next_random_value(m_gameData) & 0x7FFF) + 0x8000;

            for (int i = 0; i < mineSize; i++) {
                auto innerOffset = m_gameData->radiusOffsets[i];
                if (innerOffset.x > resourceFieldSize / 2 && innerOffset.y > resourceFieldSize / 2) {
                    break;
                }
                if (innerOffset.x > resourceFieldSize / 2 || innerOffset.y > resourceFieldSize / 2) {
                    continue;
                }

                auto innerX = x + innerOffset.x;
                auto innerY = y + innerOffset.y;

                auto surroundingTile = &m_gameData->mapTiles[innerX][innerY];
                auto terrain = surroundingTile->terrain;
                if (terrain == TERRAIN_MOUNTAIN_1 || terrain == TERRAIN_MOUNTAIN_2) {
                    auto existingType = get_resource_type(surroundingTile->resource);
                    if ((!existingType || existingType == resourceType) && s3_next_random_value(m_gameData) < skipProbability) {
                        auto tileCount = get_resource_count(surroundingTile->resource);
                        auto newTileCount = std::min(15, tileCount + 1 + s3_next_random_value(m_gameData, 14));
                        surroundingTile->resource = create_resource(resourceType, newTileCount);
                        addedCount += newTileCount - tileCount;

                        if (addedCount >= toAddCount) {
                            return addedCount;
                        }
                    }
                }
            }

            return addedCount;
        }

        int addResource(int clusterIndex, int resourceOffset, int resourceType, int toAddCount) {
            if (toAddCount <= 0) {
                return 0;
            }

            int addedCount = 0;

            // Locate empty mountain space where we can place a mine.
            for (auto resourceField : resourceFieldsByCluster[clusterIndex]) {
                for (int i=0; i<10; i++) {
                    auto offset = m_gameData->radiusOffsets[s3_next_random_value(m_gameData, 300)];
                    auto x = resourceField.centerX + offset.x;
                    auto y = resourceField.centerY + offset.y;

                    int numResourceFields = 0;
                    for (int j=0;j<32;j++) {
                        auto curOffset = m_gameData->radiusOffsets[j];
                        auto curX = x + curOffset.x;
                        auto curY = y + curOffset.y;
                        if (m_gameData->mapTiles[x][y].resource) {
                            numResourceFields++;
                        }
                    }

                    if (numResourceFields <= 5) {
                        addedCount += addResourceAround(x, y, resourceType, toAddCount - addedCount);
                        if (addedCount >= toAddCount) {
                            return addedCount;
                        }
                    }
                }
            }

            // Try to locate existing occurrences of the resource and increase them around that location.
            for (auto resourceField : resourceFieldsByCluster[clusterIndex]) {
                int currentCount = *(&resourceField.centerX + resourceOffset);
                if (currentCount <= 0) {
                    continue;
                }

                for (int i = 0; i < 3000; i++) {
                    auto offset = m_gameData->radiusOffsets[i];
                    int x = resourceField.centerX + offset.x;
                    int y = resourceField.centerY + offset.y;
                    if (get_resource_type(m_gameData->mapTiles[x][y].resource) == resourceType) {
                        addedCount += addResourceAround(x, y, resourceType, toAddCount - addedCount);
                        if (addedCount >= toAddCount) {
                            return addedCount;
                        }
                    }
                }
            }

            // We still haven't added as much as we would like to add. Let's use empty mountain space to add any remaining portion.
            for (auto resourceField : resourceFieldsByCluster[clusterIndex]) {
                for (int i = 0; i < 3000; i++) {
                    auto offset = m_gameData->radiusOffsets[i];
                    if (offset.x > resourceFieldSize / 2 && offset.y > resourceFieldSize / 2) {
                        break;
                    }
                    if (offset.x > resourceFieldSize / 2 || offset.y > resourceFieldSize / 2) {
                        continue;
                    }

                    int x = resourceField.centerX + offset.x;
                    int y = resourceField.centerY + offset.y;
                    auto tile = &m_gameData->mapTiles[x][y];
                    auto terrain = tile->terrain;
                    if (terrain == TERRAIN_MOUNTAIN_1 || terrain == TERRAIN_MOUNTAIN_2) {
                        if (!tile->resource) {
                            addedCount += addResourceAround(x, y, resourceType, toAddCount - addedCount);
                            if (addedCount >= toAddCount) {
                                return addedCount;
                            }
                        }
                    }
                }
            }

            return addedCount;
        }

        Stats computeMetrics(std::vector<unsigned int> values) {
            int sum = 0;
            unsigned int min = std::numeric_limits<unsigned int>::max();
            unsigned int max = std::numeric_limits<unsigned int>::min();
            for (unsigned int value : values) {
                sum += value;
                if (value < min) {
                    min = value;
                }
                if (value > max) {
                    max = value;
                }
            }

            double average = round(static_cast<double>(sum) / static_cast<double>(values.size()));

            double sumDiff = 0.0;
            for (double distance : values) {
                sumDiff += pow(distance - average, 2);
            }
            int numValues = values.size();
            double standardDeviation = sqrt(sumDiff / numValues);

            return { min, max, average, standardDeviation };
        }

        Stats computeDistances(std::vector<unsigned int>* clusterList) {
            int numElements = clusterList->size();
            std::vector<unsigned int> distances;

            for (int i = 0; i < numElements - 1; i++) {
                auto clusterI = clusters[clusterList->at(i)];

                unsigned int minDistance = std::numeric_limits<unsigned int>::max();

                for (int j = i + 1; j < numElements; j++) {
                    auto clusterJ = clusters[clusterList->at(j)];

                    auto distance = s3_calculate_distance(clusterI.centerX, clusterI.centerY, clusterJ.centerX, clusterJ.centerY);
                    distances.push_back(distance);
                    if (distance < minDistance) {
                        minDistance = distance;
                    }
                }
            }

            return computeMetrics(distances);
        }

        Stats rateSelectedClusters(selected_clusters* selectedClusters) {
            int numElements = selectedClusters->list.size();
            if (numElements > 20) {
                dprintf("Analyzing more than 20 selected clusters, this should never happen.\n");
                return { 0, 0, 0 };
            }

            typedef struct {
                int numCoal;
                int numIron;
            } available_resources;

            std::vector<unsigned int> clusterScores(20, 0);
            std::map<int, available_resources> clusterResources;
            for (int i = 0; i < numElements; i++) {
                clusterResources[i] = { 0, 0 };
            }

            for (auto resourceField : resourceFields) {
                unsigned int locMinDistance = std::numeric_limits<unsigned int>::max();
                int minClusterIndex = -1;
                for (int i = 0; i < numElements; i++) {
                    auto cluster = clusters[selectedClusters->list.at(i)];
                    auto distance = s3_calculate_distance(resourceField.centerX, resourceField.centerY, cluster.centerX, cluster.centerY);
                    if (distance < locMinDistance) {
                        locMinDistance = distance;
                        minClusterIndex = i;
                    }
                }
                if (minClusterIndex >= 0) {
                    available_resources resources = clusterResources[minClusterIndex];
                    resources.numIron += resourceField.numIron;
                    resources.numCoal += resourceField.numCoal;
                    clusterResources[minClusterIndex] = resources;
                }
            }
            for (int i = 0; i < numElements; i++) {
                auto resources = clusterResources[i];
                unsigned int score = std::min(resources.numCoal / 2, resources.numIron);
                clusterScores[i] = score;
            }

            return computeMetrics(clusterScores);
        }

        LocationStats computeStats(selected_clusters* selectedClusters) {
            return { computeDistances(&selectedClusters->list), rateSelectedClusters(selectedClusters) };
        }

        int computeTotalScore(LocationStats stats) {
            unsigned int total = 0;

            // We award up to 100 points if the minimum distance is bigger, but beyond that, it does not add any additional points to
            // further increase the distance between players.
            unsigned int distancePoints = static_cast<unsigned int>(static_cast<double>(std::min(awardedMinDistance, stats.distances.min)) /
                                                                    awardedMinDistance * 100);
            total += distancePoints;

            unsigned int averageDistance = static_cast<unsigned int>(round(stats.distances.average));
            unsigned int distanceAveragePoints = static_cast<unsigned int>(std::min(400u, averageDistance)) / 4;
            total += distanceAveragePoints;

            // Award up to 200 points, the more the smaller the deviation between spots' long term viability
            unsigned int opportunityPoints =
                static_cast<unsigned int>(std::max(0, static_cast<int>(round(10000 - stats.scores.standardDeviation)))) / 100 * 2;
            total += opportunityPoints;
#ifdef DEBUG_START_LOCATIONS
            dprintf("Distance-Points: %d -- Distance-Deviation-Points: %d (avg: %d) -- Opportunity: %d\n", distancePoints,
                    distanceAveragePoints, averageDistance, opportunityPoints);
#endif

            return total;
        };

        bool isFirstClusterBetter(LocationStats statsA, LocationStats statsB) {
            return computeTotalScore(statsA) > computeTotalScore(statsB);
        }
    };

public:
    LocationOptimizer(bool increaseResourceYields) : m_increaseResourceYields(increaseResourceYields) {}

    /**
     * Runs the location optimization algorithm and assigns spots to players.
     *
     * @param gameData
     */
    void run(game_data* gameData) {
        Context ctx(gameData, m_increaseResourceYields);

        updateNumPlayers(&ctx);
        updateWaterLocations(&ctx);
        addInlandWaterLocations(&ctx);
        updateResourceFields(&ctx);
        updateSuitableLocations(&ctx);
        createLocationClusters(&ctx);

        auto selectedClusters = selectInitialClusters(&ctx);
        if (!selectedClusters.list.empty()) {
            auto pickedStats = ctx.computeStats(&selectedClusters);
#ifdef DEBUG_START_LOCATIONS
            dprintf("Initial cluster configuration (Distances -- min: %d, avg: %.4f, Scores -- min: %d, max: %d, dev: %.4f)\n",
                    pickedStats.distances.min, pickedStats.distances.average, pickedStats.scores.min, pickedStats.scores.max,
                    pickedStats.scores.standardDeviation);
#endif
            pickedStats = optimizeCluster(&ctx, &selectedClusters, 100);
#ifdef DEBUG_START_LOCATIONS
            dprintf("Optimized cluster configuration (Distances -- min: %d, avg: %.4f, Scores -- min: %d, max: %d, dev: %.4f)\n",
                    pickedStats.distances.min, pickedStats.distances.average, pickedStats.scores.min, pickedStats.scores.max,
                    pickedStats.scores.standardDeviation);
#endif
            assignResourceFieldsToClusters(&ctx, &selectedClusters);

            unsigned int maxSoldierCount = estimateMaxSoldiersPerCluster(&ctx, &selectedClusters);
            unsigned int targetSoldierCount = std::min(300u, maxSoldierCount / 2);
            assignSpotsToPlayers(&ctx, &selectedClusters, targetSoldierCount);
        }
    }

private:
    void updateNumPlayers(Context* ctx) {
        ctx->m_numPlayers = 0;
        for (int i = 0; i < 20; i++) {
            if (ctx->m_gameData->players[i].type) {
                ctx->m_numPlayers++;
            }
        }
    }

    void updateWaterLocations(Context* ctx) {
        for (unsigned short x = 0; x < ctx->m_gameData->mapSize; x++) {
            for (unsigned short y = 0; y < ctx->m_gameData->mapSize; y++) {
                if (ctx->m_gameData->mapTiles[x][y].terrain == TERRAIN_STREAM) {
                    ctx->waterLocations.push_back({ x, y });
                }
            }
        }
    }

    void addInlandWaterLocations(Context* ctx) {
        unsigned int waterOutsidePadding = 200;
        if (ctx->m_gameData->mapSize > waterOutsidePadding * 2) {
            for (int i = 0; i < 100; i++) {
                int x = waterOutsidePadding + s3_next_random_value(ctx->m_gameData, ctx->m_gameData->mapSize - waterOutsidePadding * 2);
                int y = waterOutsidePadding + s3_next_random_value(ctx->m_gameData, ctx->m_gameData->mapSize - waterOutsidePadding * 2);

                unsigned int nextWaterDistance = std::numeric_limits<int>::max();
                for (auto loc : ctx->waterLocations) {
                    auto distance = s3_calculate_distance(loc.x, loc.y, x, y);
                    if (distance < nextWaterDistance) {
                        nextWaterDistance = distance;
                    }
                }

                if (nextWaterDistance > maxWaterDistance) {
                    for (int j = 0; j < 2000; j++) {
                        auto offset = ctx->m_gameData->radiusOffsets[j];
                        auto centerX = x + offset.x;
                        auto centerY = y + offset.y;

                        if (ctx->supportsPond(centerX, centerY)) {
                            ctx->addPond(centerX, centerY);
                            ctx->waterLocations.push_back({ static_cast<unsigned short>(centerX), static_cast<unsigned short>(centerY) });
                            break;
                        }
                    }
                }
            }
        }
    }

    void updateResourceFields(Context* ctx) {
        // Mountain-like Terrains: 32, 33, 34, 35, 128, 129
        // Fish-Terrains: 0, 1, 2, 3, 4, 5, 6, 7
        for (unsigned short startX = 0; startX < ctx->m_gameData->mapSize; startX += resourceFieldSize) {
            for (unsigned short startY = 0; startY < ctx->m_gameData->mapSize; startY += resourceFieldSize) {
                unsigned int numCoal = 0, numIron = 0, numGold = 0, numSulfur = 0, numGems = 0;
                for (unsigned short x = startX; x < startX + resourceFieldSize; x++) {
                    for (unsigned short y = startY; y < startY + resourceFieldSize; y++) {
                        auto tile = ctx->m_gameData->mapTiles[x][y];
                        auto terrain = tile.terrain;
                        if (terrain != TERRAIN_MOUNTAIN_1 && terrain != TERRAIN_MOUNTAIN_2) {
                            continue;
                        }

                        auto resource = tile.resource;
                        auto resourceType = resource & 0xF0;
                        auto resourceCount = resource & 0xF;
                        if (resourceType == RESOURCE_TYPE_COAL) {
                            numCoal += resourceCount;
                        } else if (resourceType == RESOURCE_TYPE_IRON) {
                            numIron += resourceCount;
                        } else if (resourceType == RESOURCE_TYPE_GOLD) {
                            numGold += resourceCount;
                        } else if (resourceType == RESOURCE_TYPE_GEMS) {
                            numGems += resourceCount;
                        } else if (resourceType == RESOURCE_TYPE_SULFUR) {
                            numSulfur += resourceCount;
                        }
                    }
                }

                if (numCoal > 0 || numIron > 0 || numGold > 0 || numSulfur > 0 || numGems > 0) {
                    ctx->resourceFields.push_back({ static_cast<unsigned int>(startX + resourceFieldSize / 2),
                                                    static_cast<unsigned int>(startY + resourceFieldSize / 2), numCoal, numIron, numGold,
                                                    numSulfur, numGems });
                }
            }
        }
    }

    void updateSuitableLocations(Context* ctx) {
        int maxCoord = ctx->m_gameData->mapSize - locationPadding;
        for (unsigned short x = locationPadding; x < maxCoord; x++) {
            for (unsigned short y = locationPadding; y < maxCoord; y++) {
                auto mapTile = ctx->m_gameData->mapTiles[x][y];
                if (mapTile.terrain != TERRAIN_LAND || mapTile.objects) {
                    continue;
                }

                if (ctx->isSuitableLocation(x, y)) {
                    ctx->suitableLocations.push_back({ x, y });
                }
            }
        }
#ifdef DEBUG_START_LOCATIONS
        dprintf("Found %d suitable starting locations\n", ctx->suitableLocations.size());
#endif
    }

    void createLocationClusters(Context* ctx) {
        unsigned int allowedDistance = maxAllowedDistance;
        while (allowedDistance > 0 && ctx->clusters.size() < ctx->m_numPlayers) {
            ctx->clusters.clear();
            for (auto loc : ctx->suitableLocations) {
                bool found = false;

                for (auto& cluster : ctx->clusters) {
                    if (s3_calculate_distance(cluster.centerX, cluster.centerY, loc.x, loc.y) <= maxAllowedDistance) {
                        cluster.sumX += loc.x;
                        cluster.sumY += loc.y;
                        cluster.numPoints++;
                        cluster.centerX = cluster.sumX / cluster.numPoints;
                        cluster.centerY = cluster.sumY / cluster.numPoints;
                        found = true;
                        break;
                    }
                }
                if (found) {
                    continue;
                }

                Cluster newCluster;
                newCluster.sumX = loc.x;
                newCluster.sumY = loc.y;
                newCluster.centerX = loc.x;
                newCluster.centerY = loc.y;
                newCluster.numPoints = 1;
                ctx->clusters.push_back(newCluster);
            }

            allowedDistance -= 5;
        }

#ifdef DEBUG_START_LOCATIONS
        dprintf("Created %d clusters of possible start locations\n", ctx->clusters.size());
#endif
    }

    selected_clusters selectInitialClusters(Context* ctx) {
        if (ctx->clusters.empty()) {
            return { std::vector<unsigned int>(), std::set<unsigned int>() };
        }

        selected_clusters bestClusters;
        selected_clusters pickedClusters;

        LocationStats pickedStats;
        pickedStats.distances.min = 0;
        pickedStats.scores.min = 0;

        int iterations = 0;
        do {
            iterations++;

            pickedClusters.list.clear();
            pickedClusters.set.clear();
            bool skipToNextIteration = false;

            int attempts = 0;
            while (pickedClusters.set.size() < ctx->m_numPlayers) {
                int clusterIndex;
                do {
                    clusterIndex = s3_next_random_value(ctx->m_gameData, ctx->clusters.size() - 1);
                    attempts++;

                    if (attempts > 10000) {
                        skipToNextIteration = true;
                        break;
                    }
                } while (pickedClusters.set.find(clusterIndex) != pickedClusters.set.end());

                if (skipToNextIteration) {
                    break;
                }

                auto newCluster = ctx->clusters[clusterIndex];
                bool isEligible = true;
                for (auto existingIndex : pickedClusters.list) {
                    auto existingCluster = ctx->clusters[existingIndex];
                    if (s3_calculate_distance(existingCluster.centerX, existingCluster.centerY, newCluster.centerX, newCluster.centerY) <
                        30) {
                        isEligible = false;
                        break;
                    }
                }
                if (!isEligible) {
                    continue;
                }

                pickedClusters.set.insert(clusterIndex);
                pickedClusters.list.push_back(clusterIndex);
            }

            if (skipToNextIteration) {
                continue;
            }

            auto stats = optimizeCluster(ctx, &pickedClusters, 5);
            if (ctx->isFirstClusterBetter(stats, pickedStats)) {
                pickedStats = stats;
                bestClusters.list = pickedClusters.list;
                bestClusters.set = pickedClusters.set;
            }
        } while (iterations < 10);

        return bestClusters;
    }

    unsigned int estimateMaxSoldiersPerCluster(Context* ctx, selected_clusters* selectedClusters) {
        for (auto clusterIndex : selectedClusters->list) {
            ctx->scoreByCluster[clusterIndex] = 0;
        }

        // Compute max possible soldiers for each cluster assuming that all resources are used and only considering iron and coal
        // (no char burners or trading or most notably Amazon labs).
        unsigned int max = std::numeric_limits<unsigned int>::min();
        for (auto clusterIndex : selectedClusters->list) {
            unsigned int numCoal = 0, numIron = 0;
            for (auto field : ctx->resourceFieldsByCluster[clusterIndex]) {
                numCoal += field.numCoal;
                numIron += field.numIron;
            }

            unsigned int numSoldiers = std::min(numCoal / 2, numIron);
            if (numSoldiers > max) {
                max = numSoldiers;
            }
            ctx->scoreByCluster[clusterIndex] = numSoldiers;
        }

        return max;
    }

    void assignResourceFieldsToClusters(Context* ctx, selected_clusters* selectedClusters) {
        // At this point, we have determined the starting locations that we will be using in the game. We are scoring and sorting
        // them according to what we believe is the long term potential of the spot (i.e. the maximum number of soldiers you can
        // make here).
        for (auto clusterIndex : selectedClusters->list) {
            ctx->resourceFieldsByCluster[clusterIndex] = std::vector<resource_field>();
        }

        // Group resource fields by clusters (assuming radial expansion)
        for (auto resourceField : ctx->resourceFields) {
            unsigned int minDistance = 1000;
            int minClusterIndex;
            for (unsigned int i = 0; i < ctx->m_numPlayers; i++) {
                auto clusterIndex = selectedClusters->list[i];
                auto cluster = ctx->clusters[clusterIndex];

                auto distance = s3_calculate_distance(resourceField.centerX, resourceField.centerY, cluster.centerX, cluster.centerY);
                if (distance < minDistance) {
                    minDistance = distance;
                    minClusterIndex = clusterIndex;
                }
            }

            ctx->resourceFieldsByCluster.at(minClusterIndex).push_back(resourceField);
        }

        // Sort resource fields by distance to cluster (closest fields first).
        for (auto clusterIndex : selectedClusters->list) {
            auto clusterFields = ctx->resourceFieldsByCluster[clusterIndex];
            if (clusterFields.empty()) {
                continue;
            }

            auto cluster = ctx->clusters[clusterIndex];
            std::sort(clusterFields.begin(), clusterFields.end(), [cluster](resource_field a, resource_field b) -> bool {
                return s3_calculate_distance(a.centerX, a.centerY, cluster.centerX, cluster.centerY) <
                       s3_calculate_distance(b.centerX, b.centerY, cluster.centerX, cluster.centerY);
            });
            ctx->resourceFieldsByCluster[clusterIndex] = clusterFields;
        }
    }

    LocationStats optimizeCluster(Context* ctx, selected_clusters* selectedClusters, int maxMoves) {
        auto clusterSet = &selectedClusters->set;
        auto clusterList = &selectedClusters->list;

        bool hasChanged = true;
        int moves = 0;
        LocationStats currentStats = ctx->computeStats(selectedClusters);

        while (hasChanged && moves < maxMoves) {
            hasChanged = false;
            moves++;
            unsigned int minDistance = 10000;
            unsigned int maxDistance = 0;
            int minClusterIndexA = 0, minClusterIndexB = 0;
            int maxClusterIndexA = 0, maxClusterIndexB = 0;
            for (unsigned int i = 0; i < ctx->m_numPlayers - 1; i++) {
                auto clusterI = ctx->clusters[clusterList->at(i)];
                for (unsigned int j = i + 1; j < ctx->m_numPlayers; j++) {
                    auto clusterJ = ctx->clusters[clusterList->at(j)];

                    auto distance = s3_calculate_distance(clusterI.centerX, clusterI.centerY, clusterJ.centerX, clusterJ.centerY);
                    if (distance < minDistance) {
                        minDistance = distance;
                        minClusterIndexA = i;
                        minClusterIndexB = j;
                    }

                    if (distance > maxDistance) {
                        maxDistance = distance;
                        maxClusterIndexA = i;
                        maxClusterIndexB = j;
                    }
                }
            }

            std::vector<int> moveCandidates;
            if (minClusterIndexA != minClusterIndexB) {
                moveCandidates.push_back(minClusterIndexA);
                moveCandidates.push_back(minClusterIndexB);
            }
            if (maxClusterIndexA != maxClusterIndexB) {
                moveCandidates.push_back(maxClusterIndexA);
                moveCandidates.push_back(maxClusterIndexB);
            }
            for (unsigned int i = 0; i < ctx->m_numPlayers; i++) {
                if (i != minClusterIndexA && i != minClusterIndexB && i != maxClusterIndexA && i != maxClusterIndexB) {
                    moveCandidates.push_back(i);
                }
            }

            for (auto candidate : moveCandidates) {
                selected_clusters newClusters;
                newClusters.list = *clusterList;
                newClusters.set = *clusterSet;

                newClusters.list.erase(newClusters.list.begin() + candidate);

                int movingClusterIndex = clusterList->at(candidate);
                newClusters.set.erase(movingClusterIndex);

                LocationStats newStats = currentStats;
                int newClusterIndex = -1;
                newClusters.list.push_back(-1);
                int newClusterListIndex = newClusters.list.size() - 1;
                for (unsigned int i = 0; i < ctx->clusters.size(); i++) {
                    if (clusterSet->find(i) != clusterSet->end()) {
                        continue;
                    }

                    newClusters.set.erase(newClusters.list[newClusterListIndex]);
                    newClusters.list[newClusterListIndex] = i;
                    newClusters.set.insert(i);

                    LocationStats stats = ctx->computeStats(&newClusters);
                    if (ctx->isFirstClusterBetter(stats, newStats)) {
                        newStats = stats;
                        newClusterIndex = i;
                    }
                }

                if (newClusterIndex >= 0) {
#ifdef DEBUG_START_LOCATIONS
                    dprintf("Changing Cluster (%d -> %d, Distances -- min: %d -> %d - avg: %.4f -> %.4f, Scores -- min: %d -> %d - "
                            "dev: %.4f -> %.4f)\n",
                            movingClusterIndex, newClusterIndex, currentStats.distances.min, newStats.distances.min,
                            currentStats.distances.average, newStats.distances.average, currentStats.scores.min, newStats.scores.min,
                            currentStats.scores.standardDeviation, newStats.scores.standardDeviation);
#endif
                    hasChanged = true;
                    clusterList->erase(std::remove(clusterList->begin(), clusterList->end(), movingClusterIndex), clusterList->end());
                    clusterSet->erase(movingClusterIndex);
                    clusterList->push_back(newClusterIndex);
                    clusterSet->insert(newClusterIndex);

                    currentStats = newStats;
                }

                if (hasChanged) {
                    break;
                }
            }
        }

#ifdef DEBUG_START_LOCATIONS
        dprintf("Completing optimization after %d moves\n", moves);
#endif

        return currentStats;
    }

    void assignSpotsToPlayers(Context* ctx, selected_clusters* selectedClusters, unsigned int targetSoldierCount) {
        std::vector<unsigned int> bestClusterList = selectedClusters->list;
        std::sort(bestClusterList.begin(), bestClusterList.begin() + ctx->m_numPlayers,
                  [&](int i, int j) -> bool { return ctx->scoreByCluster[i] > ctx->scoreByCluster[j]; });

        // We will be randomly drawing from the player pool and assign spots starting with the best spot first and avoiding to
        // assign all the best spots to the same team.
        std::set<unsigned short> pickedPositions;
        std::set<unsigned short> pickedTeamIds;
        std::vector<short> availablePlayers;
        while (pickedPositions.size() < ctx->m_numPlayers) {
            availablePlayers.clear();
        pickPlayers:
            for (unsigned short i = 0; i < ctx->m_numPlayers; i++) {
                auto player = &ctx->m_gameData->players[i];
                if (!player->type || player->startX != -1) {
                    continue;
                }
                if (pickedTeamIds.find(player->team) != pickedTeamIds.end()) {
                    continue;
                }
                availablePlayers.push_back(i);
            }
            if (availablePlayers.empty()) {
                if (pickedTeamIds.empty()) {
                    break;
                }

                pickedTeamIds.clear();
                goto pickPlayers;
            }

            short pickedPlayerPos = s3_next_random_value(ctx->m_gameData, availablePlayers.size() - 1);
            short playerIndex = availablePlayers[pickedPlayerPos];
            auto player = &ctx->m_gameData->players[playerIndex];

            auto clusterIndex = bestClusterList[0];
            auto cluster = ctx->clusters[clusterIndex];
            bestClusterList.erase(bestClusterList.begin());

            pickedTeamIds.insert(player->team);

            // Now that we have assigned the position, we do some final touches on the spot. We are doing this last
            // as we did not want to limit the number of available spots caused by additional constraints earlier.
            // Specifically, we ensure the following:
            // - Romans/Asians: Gold is available
            // - Amazons: Sulfur/Gems are available
            // - Egypt: Gems are available
            // - a minimum number of trees and stones are available
            // - the maximum potential soldier production is at best equal
            unsigned int addedGold = 0, addedGems = 0, addedSulfur = 0, addedIron = 0, addedCoal = 0;
            unsigned int numGold = 0, numSulfur = 0, numGems = 0, numIron = 0, numCoal = 0;
            for (auto field : ctx->resourceFieldsByCluster[clusterIndex]) {
                numGold += field.numGold;
                numSulfur += field.numSulfur;
                numGems += field.numGems;
                numIron += field.numIron;
                numCoal += field.numCoal;
            }

            unsigned int numTrees = 0, numStones = 0;
            unsigned int addedTrees = 0, addedStones = 0;
            for (int i = 0; i < 3000; i++) {
                auto offset = ctx->m_gameData->radiusOffsets[i];
                int x = cluster.centerX + offset.x;
                int y = cluster.centerY + offset.y;

                auto tile = &ctx->m_gameData->mapTiles[x][y];
                if (tile->objects >= OBJECTS_TREES_START && tile->objects <= OBJECTS_TREES_END) {
                    numTrees++;
                } else if (tile->objects >= OBJECTS_STONES_START && tile->objects <= OBJECTS_STONES_END) {
                    numStones += OBJECTS_STONE_CRUMBS - tile->objects;
                }
            }

            auto maxSoldiers = ctx->scoreByCluster[clusterIndex];

            unsigned int minTrees = 0, minStones = 0;
            if (player->race == RACE_ROMAN || player->race == RACE_ASIA) {
                static int minGold = 200;
                if (numGold < minGold) {
                    addedGold = ctx->addResource(clusterIndex, offsetof(resource_field, numGold), RESOURCE_TYPE_GOLD, minGold - numGold);
                }

                minTrees = 12;
                minStones = 40;
                if (player->race == RACE_ASIA) {
                    minTrees = 25;
                    minStones = 15;
                    ctx->addSwamp(cluster.centerX, cluster.centerY);
                }
            } else if (player->race == RACE_AMAZON) {
                minTrees = 25;
                minStones = 20;

                static int minSulfur = 200;
                static int minGems = 200;
                if (numSulfur < minSulfur) {
                    addedSulfur +=
                        ctx->addResource(clusterIndex, offsetof(resource_field, numSulfur), RESOURCE_TYPE_SULFUR, minSulfur - numSulfur);
                }
                if (numGems < minGems) {
                    addedGems += ctx->addResource(clusterIndex, offsetof(resource_field, numGems), RESOURCE_TYPE_GEMS, minGems - numGems);
                }

                unsigned int maxSoldiersWithLabs = maxSoldiers + std::min(numSulfur + addedSulfur, numGems + addedGems) * 2;
                if (maxSoldiersWithLabs < targetSoldierCount) {
                    int missingSoldiers = static_cast<int>(targetSoldierCount) - maxSoldiersWithLabs;
                    if (missingSoldiers > 0) {
                        int toAddSulfur = missingSoldiers / 2 - numSulfur - addedSulfur;
                        int toAddGems = missingSoldiers / 2 - numGems - addedGems;
                        int toAddCoal = missingSoldiers - numCoal - addedCoal;

                        int newlyAddedSulfur, newlyAddedGems, newlyAddedCoal;
                        do {
                            newlyAddedSulfur = ctx->addResource(clusterIndex, offsetof(resource_field, numSulfur), RESOURCE_TYPE_SULFUR,
                                                                std::min(200, toAddSulfur));
                            addedSulfur += newlyAddedSulfur;
                            toAddSulfur -= newlyAddedSulfur;

                            newlyAddedGems = ctx->addResource(clusterIndex, offsetof(resource_field, numGems), RESOURCE_TYPE_GEMS,
                                                              std::min(200, toAddGems));
                            addedGems += newlyAddedGems;
                            toAddGems -= newlyAddedGems;

                            newlyAddedCoal = ctx->addResource(clusterIndex, offsetof(resource_field, numCoal), RESOURCE_TYPE_COAL, std::min(200, toAddCoal));
                            toAddCoal -= newlyAddedCoal;
                        } while ((toAddSulfur > 0 || toAddGems > 0) && (newlyAddedSulfur > 0 || newlyAddedGems > 0));
                    }
                }

                maxSoldiers += std::min(numGems + addedGems, numSulfur + addedSulfur) * 2;
            } else if (player->race == RACE_EGYPT) {
                minStones = 100;

                static int minGems = 200;
                if (numGems < minGems) {
                    addedGems += ctx->addResource(clusterIndex, offsetof(resource_field, numGems), RESOURCE_TYPE_GEMS, minGems - numGems);
                }
            }

            if (numTrees < minTrees) {
                addedTrees = ctx->addTrees(cluster.centerX, cluster.centerY, minTrees - numTrees);
            }
            if (numStones < minStones) {
                addedStones = ctx->addStones(cluster.centerX, cluster.centerY, minStones - numStones);
            }

            if (player->race != RACE_AMAZON) {
                if (maxSoldiers < targetSoldierCount) {
                    int toAddCoal = static_cast<int>(targetSoldierCount) * 2 - numCoal;
                    int toAddIron = static_cast<int>(targetSoldierCount) - numIron;

                    int newlyAddedCoal, newlyAddedIron;
                    do {
                        newlyAddedCoal =
                            ctx->addResource(clusterIndex, offsetof(resource_field, numCoal), RESOURCE_TYPE_COAL, std::min(200, toAddCoal));
                        addedCoal += newlyAddedCoal;
                        toAddCoal -= newlyAddedCoal;

                        newlyAddedIron =
                            ctx->addResource(clusterIndex, offsetof(resource_field, numIron), RESOURCE_TYPE_IRON, std::min(200, toAddIron));
                        addedIron += newlyAddedIron;
                        toAddIron -= newlyAddedIron;
                    } while ((toAddIron > 0 || toAddCoal > 0) && (newlyAddedIron > 0 || newlyAddedCoal > 0));

                    maxSoldiers = std::min((numCoal + addedCoal) / 2, numIron + addedIron);
                }
            }

            // The cluster center itself might not actually be a suitable start location as it is always the center of suitable
            // locations, but not necessarily one of the suitable locations that it comprises. We have to run an additional check
            // and instead pick a location close to the center that supports building a small tower for the given race.
            for (int i = 0; i < 20000; i++) {
                auto offset = ctx->m_gameData->radiusOffsets[i];
                if (s3_create_building(ctx->m_gameData, cluster.centerX + offset.x, cluster.centerY + offset.y, BUILDING_SMALL_TOWER,
                                       playerIndex, 2)) {
                    player->startX = cluster.centerX + offset.x;
                    player->startY = cluster.centerY + offset.y;
                    break;
                }
            }

#ifdef DEBUG_START_LOCATIONS
            dprintf("Player #%d (Race=%lu, Team=%lu, X=%d, Y=%d, Max Soldiers: %d, Coal: %d (%d), Iron: %d (%d), Gold: %d (%d), "
                    "Gems: %d (%d), Sulfur: %d (%d), Trees: %d (%d), Stones: %d (%d))\n",
                    playerIndex, player->race, player->team, cluster.centerX, cluster.centerY, maxSoldiers, numCoal, addedCoal, numIron,
                    addedIron, numGold, addedGold, numGems, addedGems, numSulfur, addedSulfur, numTrees, addedTrees, numStones,
                    addedStones);
#endif
        }

#ifdef DEBUG_START_LOCATIONS
        for (unsigned int i = 0; i < ctx->clusters.size(); i++) {
            if (selectedClusters->set.find(i) == selectedClusters->set.end()) {
                auto cluster = ctx->clusters[i];
                s3_add_item_pile_near(ctx->m_gameData, ITEM_GRAIN, 8, cluster.centerX, cluster.centerY);
            }
        }
#endif
    }
};

void increase_map_resource_yields(game_data* gameData) {
    for (unsigned int x = 0; x < gameData->mapSize; x++) {
        for (unsigned int y = 0; y < gameData->mapSize; y++) {
            auto tile = &gameData->mapTiles[x][y];
            if (tile->objects >= 115 && tile->objects <= 126) {
                int numStones = std::min(12, ((127 - tile->objects) * 15 - 1) / 10 + 1);
                tile->objects = 127 - numStones;
            }

            auto resourceType = get_resource_type(tile->resource);
            auto resourceCount = get_resource_count(tile->resource);
            if (resourceCount > 0) {
                if (resourceType == RESOURCE_TYPE_FISH || gameData->randomMirrorType != 0) {
                    auto newResourceCount = std::min(15, resourceCount * 2);
                    tile->resource = resourceType + newResourceCount;
                }
            }
        }
    }
}

void __fastcall random_pick_start_locations(game_data* gameData) {
    // Avoid any unnecessary CPU cycles for non-random maps.
    if (s3_is_random_game()) {
        if (g_activePatch->GetGameSettingsManager()->getGameSettings().shouldIncreaseRandomResourceYield()) {
            increase_map_resource_yields(gameData);
        }

        if (gameData->randomMirrorType == 0 &&
            g_activePatch->GetGameSettingsManager()->getGameSettings().shouldOptimizeRandomStartLocations()) {
            LocationOptimizer optimizer(g_activePatch->GetGameSettingsManager()->getGameSettings().shouldIncreaseRandomResourceYield());
            optimizer.run(gameData);
        }
    }

    s3_random_pick_start_locations(gameData);
}

int __cdecl generate_random_seed() {
    static int i = 0;

    int seed = rand();
#ifdef RANDOM_SEED
    if (i % 2 == 1) {
        seed = RANDOM_SEED;
    }
#endif
    i++;

#ifdef DEBUG_START_LOCATIONS
    dprintf("Using random seed: %d\n", seed);
#endif

    return seed;
}

void on_add_map_resources(hl::CpuContext* ctx) {
    game_data* gameData = reinterpret_cast<game_data*>(ctx->ESI);

    if (g_activePatch->GetGameSettingsManager()->getGameSettings().shouldIncreaseRandomResourceYield()) {
        s3_add_resources(gameData, RESOURCE_TYPE_SULFUR, 20);
        s3_add_resources(gameData, RESOURCE_TYPE_GEMS, 15);
        s3_add_resources(gameData, RESOURCE_TYPE_IRON, 100);
        s3_add_resources(gameData, RESOURCE_TYPE_COAL, 300);
    }
}

void RandomMapPatch::enable() {
    if (g_activePatch) {
        g_activePatch->disable();
    }
    g_activePatch = this;

    m_randomPickStartLocations.hook(reinterpret_cast<void**>(&s3_random_pick_start_locations),
                                    reinterpret_cast<void*>(random_pick_start_locations));
    m_hooker.hookDetour(0x518A08, 8, on_add_map_resources);
#ifdef DEBUG_START_LOCATIONS
    patchRandomSeed();
#endif
}

void RandomMapPatch::disable() {
    m_randomPickStartLocations.revert();
    m_randomSeedPatch.revert();
    m_hooker.revert();

    if (g_activePatch == this) {
        g_activePatch = nullptr;
    }
}

void RandomMapPatch::patchRandomSeed() {
    uintptr_t pos = Constants::S3_EXE + 0x83913;
    Environment env;
    env.setArch(Environment::kArchX86);
    CodeHolder code;
    code.init(env, pos);
    x86::Assembler a(&code);
    a.call(&generate_random_seed);

    m_randomSeedPatch.apply(pos, (char*)code.textSection()->data(), code.textSection()->bufferSize());
}

void RandomMapPatch::OnAfterGameInit(logic_thread* logicThread) {
#ifdef DEBUG_START_LOCATIONS
    s3_set_map_visibility(true);
#endif
}
