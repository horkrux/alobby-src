#include <imgui.h>
#include "imgui_utils.h"
#include "imgui_spinner.h"

namespace ImGuiWidgets {
void LoadingOverlay(const char* label) {
    ImGui::OpenPopup("#loader");
    if (ImGui::BeginPopupModal("#loader", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoMove)) {
        ImGui::Spinner("#spinner", 20, 3, S3_YELLOW);
        ImGui::Text("%s", label);
        ImGui::EndPopup();
    }
}

void HelpMarker(const char* desc)
{
    ImGui::TextDisabled("(?)");
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}
}