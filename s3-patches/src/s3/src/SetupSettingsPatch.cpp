#include <debug_console.h>
#include <imgui.h>
#include <imgui_utils.h>
#include <MachineCodePatchBuilder.h>
#include <dll_config.h>
#include "SetupSettingsPatch.h"
#include "constants.h"
#include "I18n.h"

#ifndef NDEBUG
// Uncomment to enable debug messages in debug mode.
//#define ENABLE_SETTING_DEBUG 1

typedef void(__thiscall* QUEUE_EVENT_PROC)(void* This, void* eventPointer, int eventSize, DWORD* shouldFlushQueue);
QUEUE_EVENT_PROC real_s3_queue_event = reinterpret_cast<QUEUE_EVENT_PROC>(Constants::S3_EXE + 0x309C0);

void __fastcall queue_event(void* This, int notUsed, net_setting_change_event* event, int eventSize, DWORD* shouldFlushQueue) {
    if (event->eventCls == 33 && event->settingType > 0) {
#ifdef ENABLE_SETTING_DEBUG
        dprintf("Event - Section: %d, Setting: %d, Value: %d\n", event->settingType, event->settingId, event->settingValue);
#endif
    }

    real_s3_queue_event(This, event, eventSize, shouldFlushQueue);
}
#endif

class Configurator {
public:
    Configurator(logic_thread* logicThread) : m_logicThread(logicThread) {
        eventPrototype.eventCls = 33;
        eventPrototype.eventSize = 12;
        eventPrototype.field3 = 0;
        eventPrototype.field4 = 0;

        auto gameData = logicThread->gameData;
        eventPrototype.actingPlayer = static_cast<unsigned char>(gameData->actingPlayer);
        for (short i = 0; i < gameData->numEcoSectors; i++) {
            if (gameData->ecoSectors[i].owningPlayer == gameData->actingPlayer) {
                m_ecoSectorIndices.push_back(i);
            }
        }

        // For some maps, it seems like numEcoSectors is incorrectly set to 1 at game init time. We fallback to searching
        // the first 100 sectors and pick the first one owned by the player. If such a map would have multiple start sectors,
        // auto-setup would fail for all but the first one, but for multiplayer that should be irrelevant.
        if (m_ecoSectorIndices.empty()) {
            for (short i = 0; i < 100; i++) {
                if (gameData->ecoSectors[i].owningPlayer == gameData->actingPlayer) {
                    m_ecoSectorIndices.push_back(i);
                    break;
                }
            }
        }
    }

    bool HasFoundEcoSectors() { return !m_ecoSectorIndices.empty(); }

    void Change(char section, char id, char value) {
        eventPrototype.settingType = section;
        eventPrototype.settingId = id;
        eventPrototype.settingValue = value;

        for (auto sectorIndex : m_ecoSectorIndices) {
            eventPrototype.sectorIndex = sectorIndex;
            s3_queue_event(m_logicThread->netToQueue, &eventPrototype, 3);
        }
    }

private:
    logic_thread* m_logicThread;
    net_setting_change_event eventPrototype;
    std::vector<short> m_ecoSectorIndices;
};

void SetupSettingsPatch::enable() {
#ifndef NDEBUG
    queueEventHook.hook((void**)&real_s3_queue_event, (void*)queue_event);
#endif
}

void SetupSettingsPatch::disable() {
#ifndef NDEBUG
    queueEventHook.revert();
#endif
}

void SetupSettingsPatch::OnAfterGameInit(logic_thread* logicThread) {
    if (m_config->shouldShowInGameTime()) {
        int* showInGameTime = reinterpret_cast<int*>(Constants::S3_EXE + 0x3DEB54);
        *showInGameTime = 1;
    }

    auto gameSettings = s3_get_game_settings();
    if (gameSettings->isSaveGame) {
        return;
    }

    Configurator config(logicThread);

    auto gameData = logicThread->gameData;
    auto race = gameData->players[gameData->actingPlayer].race;
    auto isRandom = s3_is_random_game();
    auto goodsSetting = static_cast<int>(gameSettings->goodsSetting);

#ifdef ENABLE_SETTING_DEBUG
    auto ownTeam = gameData->players[gameData->actingPlayer].team;
    dprintf("Applying settings (map=%s, race=%lu, goods setting=%lu, team=%lu)\n", gameSettings->mapName.sText, race,
            gameSettings->goodsSetting, ownTeam);
#endif

    auto is_applicable = [&](int selectedRace, int selectedGoods, int selectedMap) -> bool {
        if (selectedRace >= 0 && selectedRace != race) {
            return false;
        }
        if (selectedGoods >= 0 && selectedGoods != goodsSetting) {
            return false;
        }
        if ((selectedMap == 0 && !isRandom) || (selectedMap == 1 && isRandom)) {
            return false;
        }

        return true;
    };

    for (auto change : m_config->getGoodDistributionChanges()) {
        if (!is_applicable(change.selectedRace, change.selectedGoods, change.selectedMap)) {
            continue;
        }
        if (change.settingId == SETTING_ID_COAL_TO_DISTILLERY && race != RACE_ASIA) {
            continue;
        }
        if ((change.settingId == SETTING_ID_GRAIN_TO_BREWERY || change.settingId == SETTING_ID_WATER_TO_BREWERY) && race != RACE_EGYPT) {
            continue;
        }
        if (change.settingId == SETTING_ID_FISH_TO_GEM_MINE && race != RACE_AMAZON && race != RACE_EGYPT) {
            continue;
        }

        config.Change(SETTING_TYPE_GOODS_DISTRIBUTION, change.settingId, change.settingValue);
    }
    for (auto change : m_config->getSettlerDistributionChanges()) {
        if (!is_applicable(change.selectedRace, change.selectedGoods, change.selectedMap)) {
            continue;
        }

        int currentCarrier = 20;
        int currentDigger = 25;
        int currentBuilder = 25;

        // First decrease the desired settings
        while (currentCarrier > change.carrier) {
            config.Change(SETTING_TYPE_SETTLER_USAGE, SETTING_ID_CARRIER_PERCENTAGE, SETTING_DOWN);
            currentCarrier -= 5;
        }
        while (currentDigger > change.digger) {
            config.Change(SETTING_TYPE_SETTLER_USAGE, SETTING_ID_DIGGER_PERCENTAGE, SETTING_DOWN);
            currentDigger -= 5;
        }
        while (currentBuilder > change.builder) {
            config.Change(SETTING_TYPE_SETTLER_USAGE, SETTING_ID_BUILDER_PERCENTAGE, SETTING_DOWN);
            currentBuilder -= 5;
        }

        // Last increase the desired settings
        while (currentCarrier < change.carrier) {
            config.Change(SETTING_TYPE_SETTLER_USAGE, SETTING_ID_CARRIER_PERCENTAGE, SETTING_UP);
            currentCarrier += 5;
        }
        while (currentDigger < change.digger) {
            config.Change(SETTING_TYPE_SETTLER_USAGE, SETTING_ID_DIGGER_PERCENTAGE, SETTING_UP);
            currentDigger += 5;
        }
        while (currentBuilder < change.builder) {
            config.Change(SETTING_TYPE_SETTLER_USAGE, SETTING_ID_BUILDER_PERCENTAGE, SETTING_UP);
            currentBuilder += 5;
        }

        break;
    }

    if (m_config->shouldStopRecruiting()) {
        config.Change(SETTING_TYPE_SETTLER_USAGE, SETTING_ID_RECRUITING, SETTING_DOWN);
    }

    if (m_config->shouldChangeWeaponProduction()) {
        config.Change(SETTING_TYPE_TOOL_AND_WEAPON_PRODUCTION, SETTING_ID_WEAPON_PRODUCTION_SWORDS, m_config->getWeaponProductionSwords());
        config.Change(SETTING_TYPE_TOOL_AND_WEAPON_PRODUCTION, SETTING_ID_WEAPON_PRODUCTION_BOWS, m_config->getWeaponProductionBows());
        config.Change(SETTING_TYPE_TOOL_AND_WEAPON_PRODUCTION, SETTING_ID_WEAPON_PRODUCTION_SPEARS, m_config->getWeaponProductionSpears());
    }

    if (m_config->shouldAdjustItemStorage()) {
        config.Change(SETTING_TYPE_STORAGE, ITEM_SHOVEL, SETTING_DO_NOT_STORE);
        config.Change(SETTING_TYPE_STORAGE, ITEM_HAMMER, SETTING_DO_NOT_STORE);
        config.Change(SETTING_TYPE_STORAGE, ITEM_AX, SETTING_DO_NOT_STORE);
        config.Change(SETTING_TYPE_STORAGE, ITEM_PICK, SETTING_DO_NOT_STORE);
        config.Change(SETTING_TYPE_STORAGE, ITEM_SAW, SETTING_DO_NOT_STORE);
        config.Change(SETTING_TYPE_STORAGE, ITEM_FISHING_ROD, SETTING_DO_NOT_STORE);
        config.Change(SETTING_TYPE_STORAGE, ITEM_SCYTHE, SETTING_DO_NOT_STORE);
        config.Change(SETTING_TYPE_STORAGE, ITEM_GOLD_BARS, SETTING_DO_NOT_STORE);
        config.Change(SETTING_TYPE_STORAGE, ITEM_GEMS, SETTING_DO_NOT_STORE);
    }

    if (m_config->shouldEnableTeamChat()) {
        enableTeamChat(gameData);
    }

    if (config.HasFoundEcoSectors()) {
        short actingPlayerColor = s3_get_player_color(logicThread->gameData->actingPlayer);
        str_struct_wchar successMsg;
        s3_str_init(&successMsg, utf8ToWindows(m_i18n->__("game_settings_applied", "Game Settings: Applied")).c_str());
        s3_add_chat_message(logicThread, &successMsg, actingPlayerColor);
    } else {
        short actingPlayerColor = s3_get_player_color(logicThread->gameData->actingPlayer);

        str_struct_wchar errorMsg;
        s3_str_init(&errorMsg,
                    utf8ToWindows(m_i18n->__("game_settings_eco_sector_not_found", "Game Settings: Failed, eco sector not found")).c_str());
        s3_add_chat_message(logicThread, &errorMsg, actingPlayerColor);
    }
}

void SetupSettingsPatch::enableTeamChat(game_data* gameData) {
    auto ownTeam = gameData->players[gameData->actingPlayer].team;

    bool hasHumanTeamM8s = false;
    for (int i = 0; i < 20; i++) {
        if (i == gameData->actingPlayer) {
            continue;
        }

        auto player = &gameData->players[i];
        if (player->type == PLAYER_TYPE_HUMAN && player->team == ownTeam) {
            hasHumanTeamM8s = true;
            break;
        }
    }
    if (hasHumanTeamM8s) {
        for (int i = 0; i < 20; i++) {
            if (i == gameData->actingPlayer) {
                continue;
            }

            auto player = &gameData->players[i];
            player->chatOn = player->type && player->team == ownTeam;
        }
    }
}

