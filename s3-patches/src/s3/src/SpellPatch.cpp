#include <MachineCodePatchBuilder.h>
#include "SpellPatch.h"

void SpellPatch::enable() {
    applyConvertPatch();
    m_convertDelayPatch.apply(0x658f10, "\x23", 1);
    m_fishCostSpell.apply(0x658E80, "\x04", 1);
    m_attackerStrengthenCost.apply(0x658E94, "\x14", 1);
    m_shieldCost.apply(0x658EA4, "\x0f", 1);
}

void SpellPatch::disable() {
    m_convertPatch.revert();
    m_convertDelayPatch.revert();
    m_fishCostSpell.revert();
}

void __declspec(naked) is_convertible_target() {
    __asm {
        mov     [esp+19], 1
        mov     edx, [esp+28]
        shl     edx, 6
        xor     eax, eax
        mov     al, byte ptr [esi+edx+19553908]
        and     eax, 30
        jnz     SKIP

        mov     edx, [esp+28]
        mov     ecx, esi
        push    edx
        mov     eax, 0x54A457
        jmp     eax

        SKIP:
            mov eax, 0x54A4F5
            jmp eax
    }
}

void SpellPatch::applyConvertPatch() {
    MachineCodePatchBuilder builder(0x54A450);
    builder.a->jmp(reinterpret_cast<uint64_t*>(&is_convertible_target));
    builder.padNop(7);
    builder.apply(&m_convertPatch);
}
