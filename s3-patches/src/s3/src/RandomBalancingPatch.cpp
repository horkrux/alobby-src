#include <constants.h>
#include <s3.h>
#include "debug_console.h"
#include "RandomBalancingPatch.h"
#include <asmjit/x86.h>
#include <MachineCodePatchBuilder.h>

//#define DEBUG_RANDOM_BALANCING

typedef void (__thiscall *INIT_START_LOCATION_PROC)(game_data *This, int playerIndex, int xCoord_1, int yCoord_1);
INIT_START_LOCATION_PROC s3_init_start_location = reinterpret_cast<INIT_START_LOCATION_PROC>(Constants::S3_EXE + 0x106CF0);

typedef void (__thiscall *UPDATE_NEXT_MANA_POINTS_PROC)(game_data *This, int playerIndex);
UPDATE_NEXT_MANA_POINTS_PROC s3_update_next_mana_points = reinterpret_cast<UPDATE_NEXT_MANA_POINTS_PROC>(Constants::S3_EXE + 0x108450);

using namespace asmjit;

void __fastcall init_start_location(game_data *gameData, void* notUsed, int playerIndex, int xCoord, int yCoord) {
    s3_init_start_location(gameData, playerIndex, xCoord, yCoord);

    if (!s3_is_random_game()) {
        return;
    }

    auto player = &gameData->players[playerIndex];
    auto race = player->race;
    auto goodsSetting = s3_get_game_settings()->goodsSetting;

    if (goodsSetting == GOODS_HIGH) {
        if (race == RACE_ASIA) {
            s3_add_item_pile_near(gameData, ITEM_AX, 1, xCoord, yCoord);
            s3_add_item_pile_near(gameData, ITEM_PLANKS, 8, xCoord, yCoord);
            s3_add_item_pile_near(gameData, ITEM_SHOVEL, 3, xCoord-1, yCoord-1);
        } else if (race == RACE_AMAZON) {
            s3_add_item_pile_near(gameData, ITEM_PLANKS, 4, xCoord, yCoord);
        } else if (race == RACE_EGYPT) {
            s3_add_item_pile_near(gameData, ITEM_SHOVEL, 3, xCoord-1, yCoord-1);
            s3_add_item_pile_near(gameData, ITEM_PICK, 4, xCoord, yCoord);
        }

        if (race != RACE_EGYPT) {
            s3_add_item_pile_near(gameData, ITEM_PICK, 5, xCoord, yCoord);
        }
    } else if (goodsSetting == GOODS_MEDIUM || goodsSetting == GOODS_MAP_DEFAULT) {
        if (race == RACE_ASIA) {
            s3_add_item_pile_near(gameData, ITEM_AX, 1, xCoord, yCoord);
            s3_add_item_pile_near(gameData, ITEM_PLANKS, 4, xCoord, yCoord);
            s3_add_item_pile_near(gameData, ITEM_SHOVEL, 2, xCoord-1, yCoord-1);
        } else if (race == RACE_AMAZON) {
            s3_add_item_pile_near(gameData, ITEM_PLANKS, 2, xCoord, yCoord);
        } else if (race == RACE_EGYPT) {
            s3_add_item_pile_near(gameData, ITEM_SHOVEL, 2, xCoord-1, yCoord-1);
            s3_add_item_pile_near(gameData, ITEM_PICK, 3, xCoord, yCoord);
        }

        if (race != RACE_EGYPT) {
            s3_add_item_pile_near(gameData, ITEM_PICK, 4, xCoord, yCoord);
        }
    } else if (goodsSetting == GOODS_LOW) {
        if (race == RACE_ASIA) {
            s3_add_item_pile_near(gameData, ITEM_PLANKS, 2, xCoord, yCoord);
            s3_add_item_pile_near(gameData, ITEM_SHOVEL, 1, xCoord-1, yCoord-1);
        } else if (race == RACE_AMAZON) {
            s3_add_item_pile_near(gameData, ITEM_PLANKS, 1, xCoord, yCoord);
        } else if (race == RACE_EGYPT) {
            s3_add_item_pile_near(gameData, ITEM_SHOVEL, 1, xCoord-1, yCoord-1);
        }
    }
}

int get_next_level_mana_points(int race, int numUpgrades) {
    switch (race) {
    case RACE_EGYPT:
        switch (numUpgrades) {
        case 1:
            return 10;
        case 2:
            return 15;
        case 3:
            return 30; // level 3 after 10+10+15+30=65 points
        case 4:
            return 30;
        case 5:
            return 30;
        default:
            return 10;
        }

    case RACE_AMAZON:
        switch (numUpgrades) {
        case 1:
            return 15;
        case 2:
            return 25;
        case 3:
            return 40; // level 3 after 10+15+25+40 = 90 points
        case 4:
            return 50;
        case 5:
            return 30;
        default:
            return 10;
        }

    default:
        switch (numUpgrades) {
        case 1:
            return 20;
        case 2:
            return 30;
        case 3:
            if (race == RACE_ROMAN) {
                return 60; // level 3 after 10+20+30+60 = 120 points
            }

            return 50; // level 3 after 10+20+30+50 = 110 points
        case 4:
            return 60;
        case 5:
            return 30;
        default:
            return 10;
        }
    }
}

void __fastcall update_next_mana_points(game_data* gameData, void* notUsed, int playerIndex) {
    if (!s3_is_random_game()) {
        s3_update_next_mana_points(gameData, playerIndex);

        return;
    }

    auto player = &gameData->players[playerIndex];
    short numUpgrades = player->madeBowUpgrades + player->madeSpearUpgrades + player->madeSwordUpgrades;
    int manaPoints = get_next_level_mana_points(player->race, numUpgrades);

    if (player->nextLevelRequiredPoints != manaPoints) {
#ifdef DEBUG_RANDOM_BALANCING
        dprintf("Setting next level upgrade points for player %d (race=%lu, numUpgrades=%d) to %d\n", playerIndex, player->race,
                numUpgrades, manaPoints);
#endif
        player->nextLevelRequiredPoints = manaPoints;
    }
}

void RandomBalancingPatch::enable() {
    initStartLocationHook.hook(reinterpret_cast<void**>(&s3_init_start_location), reinterpret_cast<void*>(init_start_location));
    levelUpManaPointsHook.hook(reinterpret_cast<void**>(&s3_update_next_mana_points), reinterpret_cast<void*>(update_next_mana_points));
    applyPickAxPatch();
}

void RandomBalancingPatch::disable() {
    initStartLocationHook.revert();
    levelUpManaPointsHook.revert();
    m_removePickAx1Patch.revert();
}

void RandomBalancingPatch::OnBuildingCreated(game_data* gameData, unsigned int newBuildingIndex) {
    if (!s3_is_random_game()) {
        return;
    }

    auto building = &gameData->buildingData[newBuildingIndex];
    if (building->type == BUILDING_SMALL_TOWER) {
        auto buildingInConstruction = reinterpret_cast<game_data_building_in_construction*>(building);
        auto newNumBuilders = building->race == RACE_AMAZON ? 5 : 4;
        if (newNumBuilders != buildingInConstruction->numAllowedBuilders) {
            buildingInConstruction->numAllowedBuilders = newNumBuilders;
        }
    }
}

void RandomBalancingPatch::OnAfterGameInit(logic_thread* logicThread) {
    if (!s3_is_random_game()) {
        return;
    }

    for (short i=0;i<20;i++) {
        update_next_mana_points(logicThread->gameData, 0, i);
    }
}

void RandomBalancingPatch::applyPickAxPatch() {
    MachineCodePatchBuilder builderHg(0x5071CD);
    builderHg.padNop(13);
    builderHg.apply(&m_removePickAx1Patch);

    MachineCodePatchBuilder builderMg(0x5073D0);
    builderMg.padNop(13);
    builderMg.apply(&m_removePickAx2Patch);
}
