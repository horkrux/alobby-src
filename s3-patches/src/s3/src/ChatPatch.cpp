#include "debug_console.h"
#include "constants.h"
#include "s3.h"
#include "ChatPatch.h"
#include <asmjit/x86.h>
#include <dll_config.h>
#include <set>

using namespace asmjit;

typedef void(__thiscall* ADD_CHARACTER_PROC)(logic_thread* This, wchar_t character);
ADD_CHARACTER_PROC real_add_character = reinterpret_cast<ADD_CHARACTER_PROC>(Constants::S3_EXE + 0x11E2E0);

#define CHAT_MODE_ALL 1
#define CHAT_MODE_TEAM 2
#define CHAT_MODE_CUSTOM 3

static ChatPatch* g_activePatch;

short get_chat_mode_enum(bool isDisabledForSomeTeamM8s, bool isEnabledForSomeNonTeamM8s, bool isDisabledForSomeNonTeamM8s) {
    if (isDisabledForSomeTeamM8s) {
        return CHAT_MODE_CUSTOM;
    } else if (isEnabledForSomeNonTeamM8s) {
        if (isDisabledForSomeNonTeamM8s) {
            return CHAT_MODE_CUSTOM;
        }

        return CHAT_MODE_ALL;
    } else {
        return CHAT_MODE_TEAM;
    }
}

short get_current_chat_mode(logic_thread* logicThread)
{
    auto players = logicThread->gameData->players;
    auto ownTeam = players[logicThread->gameData->actingPlayer].team;
    bool isChatEnabledForSomeNonTeamM8 = false;
    bool isChatDisabledForSomeNonTeamM8s = false;
    bool isChatDisabledForSomeTeamM8 = false;
    for (int i = 0; i < 20; i++) {
        auto player = &players[i];
        if (!player->type || i == logicThread->gameData->actingPlayer) {
            continue;
        }

        if (player->team == ownTeam) {
            if (!player->chatOn) {
                isChatDisabledForSomeTeamM8 = true;
            }
        } else {
            if (player->chatOn) {
                isChatEnabledForSomeNonTeamM8 = true;
            } else {
                isChatDisabledForSomeNonTeamM8s = true;
            }
        }
    }

    return get_chat_mode_enum(isChatDisabledForSomeTeamM8, isChatEnabledForSomeNonTeamM8, isChatDisabledForSomeNonTeamM8s);
}

bool has_team_m8s(logic_thread* logicThread) {
    auto players = logicThread->gameData->players;
    auto ownTeam = players[logicThread->gameData->actingPlayer].team;
    for (int i = 0; i < 20; i++) {
        if (i == logicThread->gameData->actingPlayer) {
            continue;
        }

        auto player = &players[i];
        if (player->type && player->team == ownTeam) {
            return true;
        }
    }

    return false;
}

short get_next_chat_mode(logic_thread* logicThread, short currentMode) {
    if ((currentMode == CHAT_MODE_ALL || currentMode == CHAT_MODE_CUSTOM) && has_team_m8s(logicThread)) {
        return CHAT_MODE_TEAM;
    }

    return CHAT_MODE_ALL;
}

void __fastcall add_character(logic_thread* logicThread, void* notUsed, wchar_t character) {
    if (character == L'\t') {
        auto currentChatMode = get_current_chat_mode(logicThread);
        auto nextChatMode = get_next_chat_mode(logicThread, currentChatMode);

        if (currentChatMode != nextChatMode) {
            if (nextChatMode == CHAT_MODE_TEAM) {
                auto ownTeam = logicThread->gameData->players[logicThread->gameData->actingPlayer].team;
                for (int i = 0; i < 20; i++) {
                    if (i == logicThread->gameData->actingPlayer) {
                        continue;
                    }

                    auto player = &logicThread->gameData->players[i];
                    player->chatOn = player->type && player->team == ownTeam;
                }
            } else if (nextChatMode == CHAT_MODE_ALL) {
                for (int i = 0; i < 20; i++) {
                    if (i == logicThread->gameData->actingPlayer) {
                        continue;
                    }

                    auto player = &logicThread->gameData->players[i];
                    player->chatOn = player->type != PLAYER_TYPE_EMPTY;
                }
            } else {
                dprintf("Unsupported chat mode switch to %d\n", nextChatMode);
            }

            if (g_activePatch) {
                g_activePatch->CheckChatModeChange(logicThread);
            }

            logicThread->gameData->isLeftMenuChanged = true;
        }
    }

    real_add_character(logicThread, character);
}

void on_chat_opened(hl::CpuContext* cpuContext) {
    logic_thread* logicThread = reinterpret_cast<logic_thread*>(cpuContext->ESI);

    if (g_activePatch) {
        g_activePatch->OnChatOpened(logicThread);
    }
}

void on_before_message_send(hl::CpuContext* cpuContext) {
    logic_thread* logicThread = reinterpret_cast<logic_thread*>(cpuContext->ESI);

    if (g_activePatch) {
        g_activePatch->OnBeforeMessageSend(logicThread);
    }
}

std::string get_chat_mode_prefix_for_event(logic_thread* logicThread, net_event_chat_message* event, int actingPlayer) {
    auto actingPlayerTeam = logicThread->gameData->players[actingPlayer].team;

    bool isChatEnabledForSomeNonTeamM8 = false;
    bool isChatDisabledForSomeNonTeamM8s = false;
    bool isChatDisabledForSomeTeamM8 = false;

    bool hasSingleRecipient = true;
    short singleRecipient = -1;

    for (short i=0;i<20;i++) {
        auto player = &logicThread->gameData->players[i];

        // We cannot include computer players below as those all share the same playerSettingsIndex hence
        // we cannot detect to which computer player an event was sent specifically, but for multi-player it
        // should be irrelevant anyway.
        if (player->type != PLAYER_TYPE_HUMAN || i == actingPlayer) {
            continue;
        }

        int playerBitmask = 1 << player->playerSettingIndex;
        bool isChatOn = event->recipients & playerBitmask;

        if (isChatOn) {
            if (singleRecipient == -1) {
                singleRecipient = i;
            } else {
                hasSingleRecipient = false;
            }
        }

        if (player->team == actingPlayerTeam) {
            if (!isChatOn) {
                isChatDisabledForSomeTeamM8 = true;
            }
        } else {
            if (isChatOn) {
                isChatEnabledForSomeNonTeamM8 = true;
            } else {
                isChatDisabledForSomeNonTeamM8s = true;
            }
        }
    }

    auto chatMode = get_chat_mode_enum(isChatDisabledForSomeTeamM8, isChatEnabledForSomeNonTeamM8, isChatDisabledForSomeNonTeamM8s);

    // As the game treats all computers as the same recipient, we cannot distinguish between which computer player
    // was included in the chat message just using the net event data. For outgoing messages, we therefore rely
    // on just using the currently selected scope.
    //
    // This is only relevant when playing with only computers, i.e. likely when doing local testing.
    if (actingPlayer == logicThread->gameData->actingPlayer) {
        chatMode = get_current_chat_mode(logicThread);
    }

    if (chatMode == CHAT_MODE_TEAM) {
        return std::string("[Team] ");
    } else if (chatMode == CHAT_MODE_ALL) {
        return std::string("[All] ");
    } else {
        if (hasSingleRecipient) {
            auto settingsIndex = logicThread->gameData->players[singleRecipient].playerSettingIndex;
            auto player = s3_get_settings_player(settingsIndex);
            if (player->playerName.actualTextLength > 0) {
                std::string prefix;
                prefix.append("[");

                if (actingPlayer == logicThread->gameData->actingPlayer) {
                    prefix.append(">");
                } else {
                    prefix.append("<");
                }

                if (singleRecipient == logicThread->gameData->actingPlayer) {
                    prefix.append("you");
                } else {
                    prefix.append(player->playerName.sText);
                }
                prefix.append("] ");

                return prefix;
            }
        }

        return std::string("[Custom] ");
    }
}

void on_incoming_chat_message(logic_thread* logicThread, net_event_chat_message * event, int sendingPlayer) {
    char mbMessage[256];
    wcstombs(mbMessage, event->message, sizeof(mbMessage));

    std::string prefixedMessage;
    prefixedMessage.append(get_chat_mode_prefix_for_event(logicThread, event, sendingPlayer));
    prefixedMessage.append(mbMessage);

    str_struct_wchar message;
    s3_str_init(&message, prefixedMessage.c_str());

    auto color = s3_get_player_color(sendingPlayer);
    s3_add_chat_message(logicThread, &message, color);
}

void on_outgoing_chat_message(logic_thread* logicThread, net_event_chat_message *event) {
    on_incoming_chat_message(logicThread, event, logicThread->gameData->actingPlayer);
}

bool ChatPatch::IsToggleSupported() {
    return m_currentChatMode != m_nextChatMode;
}

void ChatPatch::CheckChatModeChange(logic_thread* logicThread) {
    if (!logicThread->isChatOpen) {
        return;
    }

    auto chatMode = get_current_chat_mode(logicThread);
    if (chatMode == m_currentChatMode) {
        return;
    }

    m_currentChatMode = chatMode;
    m_nextChatMode = get_next_chat_mode(logicThread, chatMode);

    std::wstring currentMessage(logicThread->pendingChatMessage.sText);

    char mbMessage[200];
    wcstombs(mbMessage,
             currentMessage
                 .substr(logicThread->pendingChatMessagePrefixLength,
                         logicThread->pendingChatMessage.actualTextLength - logicThread->pendingChatMessagePrefixLength)
                 .c_str(),
             sizeof(mbMessage));

    auto newMessage = getChatModePrefix(chatMode);
    auto prefixLength = newMessage.length();
    newMessage.append(mbMessage);

    str_struct_wchar updatedMessage;
    s3_str_init(&updatedMessage, newMessage.c_str());
    s3_str_copy(&logicThread->pendingChatMessage, &updatedMessage);
    logicThread->pendingChatMessagePrefixLength = prefixLength;
    logicThread->hasChatMessageChanged = true;
}

void ChatPatch::OnBeforeMessageSend(logic_thread* logicThread) {
    EnforceObserverTargets(logicThread);

    std::wstring message;
    message.append(m_originalMessagePrefix.sText);
    message.append(
        std::wstring(logicThread->pendingChatMessage.sText)
            .substr(logicThread->pendingChatMessagePrefixLength,
                    logicThread->pendingChatMessage.actualTextLength - logicThread->pendingChatMessagePrefixLength));

    // There should be a simpler way to do this, but haven't found the corresponding function in s3 yet.
    char mbMessage[200];
    wcstombs(mbMessage, message.c_str(), sizeof(mbMessage));

    str_struct_wchar messageToSend;
    s3_str_init(&messageToSend, mbMessage);
    s3_str_copy(&logicThread->pendingChatMessage, &messageToSend);

    logicThread->pendingChatMessagePrefixLength = m_originalMessagePrefix.actualTextLength;
}

void ChatPatch::EnforceObserverTargets(logic_thread* logicThread) {
    if (!m_observerMode->isActive() || isGameOver) {
        return;
    }

    std::set<int> aliveTeams;
    s3_update_alive_teams(logicThread, &aliveTeams, false);

    bool recipientsAdjusted = false;
    for (short i=0; i<20; i++) {
        auto player = &logicThread->gameData->players[i];
        if (player->type == PLAYER_TYPE_EMPTY || !player->chatOn) {
            continue;
        }

        if (aliveTeams.find(player->team) != aliveTeams.end()) {
            player->chatOn = false;
            recipientsAdjusted = true;
        }
    }

    if (recipientsAdjusted) {
        str_struct_wchar adjustMsg;
        s3_str_init(&adjustMsg, "Chatting is restricted to observers only.");
        s3_add_chat_message(logicThread, &adjustMsg, s3_get_player_color(logicThread->gameData->actingPlayer));
    }
}

void ChatPatch::OnChatOpened(logic_thread* logicThread) {
    s3_str_clear(&m_originalMessagePrefix, true);
    s3_str_copy(&m_originalMessagePrefix, &logicThread->pendingChatMessage);

    auto chatMode = get_current_chat_mode(logicThread);
    m_currentChatMode = chatMode;
    m_nextChatMode = get_next_chat_mode(logicThread, chatMode);

    auto prefix = getChatModePrefix(chatMode);
    s3_str_init(&logicThread->pendingChatMessage, prefix.c_str());

    logicThread->pendingChatMessagePrefixLength = logicThread->pendingChatMessage.actualTextLength;

    if (!m_config->hasUsageFlag(USAGE_FLAG_INGAME_CHAT)) {
        str_struct_wchar tutorialMessage;
        s3_str_init(&tutorialMessage, "Use TAB to toggle between chat modes.");
        s3_add_chat_message(logicThread, &tutorialMessage, s3_get_player_color(logicThread->gameData->actingPlayer));

        m_config->setUsageFlag(USAGE_FLAG_INGAME_CHAT);
    }
}

std::string ChatPatch::getChatModePrefix(short chatMode) {
    if (chatMode == CHAT_MODE_ALL) {
        return std::string(" /All: ");
    } else if (chatMode == CHAT_MODE_TEAM) {
        return std::string(" /Team: ");
    } else {
        return std::string(" /Custom: ");
    }
}

ChatPatch::ChatPatch(ObserverMode* observerMode, Config* config): m_observerMode(observerMode), m_config(config) {
    s3_str_clear(&m_originalMessagePrefix, false);
}

void ChatPatch::enable() {
    if (g_activePatch) {
        g_activePatch->disable();
    }
    g_activePatch = this;

    hookIncomingMessage();
    hookOutgoingMessage();

    addCharacterHook.hook((void**)&real_add_character, (void*)add_character);
    activeHooks.push_back(hooker.hookDetour(Constants::S3_EXE + 0x1255B6, 7, &on_chat_opened));
    activeHooks.push_back(hooker.hookDetour(Constants::S3_EXE + 0x1255D6, 8, &on_before_message_send));
}

void ChatPatch::disable() {
    netChatMessageReceivedHook.revert();
    displayLocalChatMessageHook.revert();
    addCharacterHook.revert();

    for (auto hook : activeHooks) {
        hooker.unhook(hook);
    }
    activeHooks.clear();

    if (g_activePatch == this) {
        g_activePatch = nullptr;
    }
}

void ChatPatch::hookOutgoingMessage() {
    uintptr_t pos = Constants::S3_EXE + 0x1256BD;
    uintptr_t endPos = Constants::S3_EXE + 0x1256EA;

    Environment env;
    env.setArch(Environment::kArchX86);
    CodeHolder code;
    code.init(env, pos);
    x86::Assembler a(&code);

    // event
    a.lea(x86::ecx, x86::ptr(x86::esp, 800));
    a.push(x86::ecx);

    // logic thread
    a.push(x86::esi);

    a.call(&on_outgoing_chat_message);
    a.add(x86::esp, 8);

    while (code.textSection()->bufferSize() < endPos - pos) {
        a.nop();
    }

    displayLocalChatMessageHook.apply(pos, (char*)code.textSection()->data(), code.textSection()->bufferSize());
}

void ChatPatch::hookIncomingMessage() {
    uintptr_t pos = Constants::S3_EXE + 0x13B568;
    uintptr_t endPos = Constants::S3_EXE + 0x13B595;
    Environment env;
    env.setArch(Environment::kArchX86);
    CodeHolder code;
    code.init(env, pos);
    x86::Assembler a(&code);

    // sending player
    a.push(x86::eax);

    // event
    a.lea(x86::eax, x86::ptr(x86::ebp, -0x19C));
    a.push(x86::eax);

    // logic thread
    a.push(x86::ebx);

    a.call(&on_incoming_chat_message);
    a.add(x86::esp, 12);
    while (code.textSection()->bufferSize() < endPos - pos) {
        a.nop();
    }

    netChatMessageReceivedHook.apply(pos, (char*)code.textSection()->data(), code.textSection()->bufferSize());
}

void ChatPatch::OnBeforeUpdateDrawData(logic_thread* logicThread) {
    if (logicThread->isChatOpen && logicThread->gameData->isLeftMenuChanged) {
        CheckChatModeChange(logicThread);
    }
}

void ChatPatch::OnGameOver(logic_thread* logicThread) {
    isGameOver = true;
}
