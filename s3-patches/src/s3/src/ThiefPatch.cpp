#include <constants.h>
#include "ThiefPatch.h"

#include <asmjit/x86.h>
#include "debug_console.h"

using namespace asmjit;

#define STEALING_ALLOWED 0
#define STEALING_FROM_ALLIES_PROHIBITED 1
#define STEALING_PROHIBITED_BY_TOWER 2
#define STEALING_PROHIBITED_BY_RULES 3

static ThiefPatch* g_activePatch = nullptr;
static bool isStealingProhibited = false;

unsigned short get_stealing_allowed_state_at(game_data* gameData, unsigned char thiefPlayer, unsigned short xCoord, unsigned short yCoord) {
    auto mapTile = &gameData->mapTiles[xCoord][yCoord];
    if (mapTile->owningPlayer == -1 || mapTile->owningPlayer == thiefPlayer) {
        return STEALING_ALLOWED;
    }

    auto thiefTeam = gameData->players[thiefPlayer].team;
    auto tileTeam = gameData->players[mapTile->owningPlayer].team;
    if (thiefTeam == tileTeam) {
        return STEALING_FROM_ALLIES_PROHIBITED;
    }

    if (isStealingProhibited) {
        return STEALING_PROHIBITED_BY_RULES;
    }

    for (int i = 1; i <= gameData->numBuildings; i++) {
        auto building = &gameData->buildingData[i];

        // Skip any buildings still under construction.
        if (building->constructionState) {
            continue;
        }

        auto buildingType = building->type;
        if (buildingType != BUILDING_SMALL_TOWER && buildingType != BUILDING_BIG_TOWER && buildingType != BUILDING_FORTRESS) {
            continue;
        }

        // Only friendly towers are relevant.
        auto buildingTeam = gameData->players[building->owningPlayer].team;
        if (buildingTeam != tileTeam) {
            continue;
        }

        bool hasBows = false;
        auto soldierIndex = building->settlerIndex;
        while (soldierIndex) {
            auto settler = &gameData->settlerData[soldierIndex];
            auto settlerType = settler->type;
            if (settlerType == SETTLER_TYPE_BOW_LEVEL_1 || settlerType == SETTLER_TYPE_BOW_LEVEL_2 ||
                settlerType == SETTLER_TYPE_BOW_LEVEL_3) {
                hasBows = true;
                break;
            }

            soldierIndex = settler->workLocationIndex;
        }
        if (!hasBows) {
            continue;
        }

        int maxDistance;
        if (buildingType == BUILDING_BIG_TOWER) {
            maxDistance = 40;
        } else if (buildingType == BUILDING_FORTRESS) {
            maxDistance = 50;
        } else {
            maxDistance = 35;
        }

        auto distance = s3_calculate_distance(xCoord, yCoord, building->centerX, building->centerY);
        if (distance <= maxDistance) {
            return STEALING_PROHIBITED_BY_TOWER;
        }
    }

    return STEALING_ALLOWED;
}

bool is_thief_allowed_to_steal_at(game_data* gameData, unsigned int settlerIndex, unsigned short xCoord, unsigned short yCoord) {
    auto thiefPlayer = gameData->settlerData[settlerIndex].owningPlayer;

    return get_stealing_allowed_state_at(gameData, thiefPlayer, xCoord, yCoord) == STEALING_ALLOWED;
}

bool should_issue_steal_command(logic_thread* logicThread) {
    auto gameData = logicThread->gameData;
    auto state = get_stealing_allowed_state_at(gameData, gameData->actingPlayer, gameData->clickXCoord, gameData->clickYCoord);
    if (state == STEALING_ALLOWED) {
        return true;
    }

    str_struct_wchar errorMessage;

    switch (state) {
    case STEALING_FROM_ALLIES_PROHIBITED:
        s3_str_init(&errorMessage, utf8ToWindows(g_activePatch->GetI18n()->__("stealing_from_allies_not_allowed", "Stealing from allies is not allowed.")).c_str());
        break;

    case STEALING_PROHIBITED_BY_TOWER:
        s3_str_init(&errorMessage, utf8ToWindows(g_activePatch->GetI18n()->__("stealing_within_tower_range_not_allowed", "Stealing in range of towers manned with bows is not possible.")).c_str());
        break;

    case STEALING_PROHIBITED_BY_RULES:
        s3_str_init(&errorMessage, utf8ToWindows(g_activePatch->GetI18n()->__("stealing_prohibited_by_rules", "Stealing is prohibited by host rules.")).c_str());
        break;

    default:
        s3_str_init(&errorMessage, utf8ToWindows(g_activePatch->GetI18n()->__("stealing_not_allowed_generic", "Stealing is not allowed at this location.")).c_str());
        break;
    }

    s3_add_chat_message(logicThread, &errorMessage, s3_get_player_color(gameData->actingPlayer));

    return false;
}

void ThiefPatch::enable() {
    if (g_activePatch) {
        g_activePatch->disable();
    }
    g_activePatch = this;

    isStealingProhibited = m_alwaysDisabled;
    applyLookingForGoodPatch();
    applyIssueCommandPatch();
}

void ThiefPatch::disable() {
    if (g_activePatch == this) {
        g_activePatch = nullptr;
    }

    lookingForGoodPatch.revert();
    issueCommandPatch.revert();
}

void ThiefPatch::applyIssueCommandPatch() {
    Environment env;
    env.setArch(Environment::kArchX86);

    uintptr_t startPos = Constants::S3_EXE + 0x1346CA;
    uintptr_t endPos = Constants::S3_EXE + 0x134782;
    uintptr_t proceedIssueCommandPos = Constants::S3_EXE + 0x134787;

    CodeHolder code;
    code.init(env, startPos);
    x86::Assembler a(&code);

    a.pushad();
    a.push(x86::esi); // logic thread
    a.call(&should_issue_steal_command);
    a.add(x86::esp, 4);
    a.cmp(x86::eax, 0);
    a.popad();
    a.jnz((uint64_t*)proceedIssueCommandPos);

    while (code.textSection()->bufferSize() < endPos - startPos) {
        a.nop();
    }

    issueCommandPatch.apply(startPos, (char*)code.textSection()->data(), code.textSection()->bufferSize());
}

void ThiefPatch::applyLookingForGoodPatch() {
    Environment env;
    env.setArch(Environment::kArchX86);

    uintptr_t startPos = Constants::S3_EXE + 0xF794D;
    uintptr_t endPos = Constants::S3_EXE + 0xF7979;

    CodeHolder code;
    code.init(env, startPos);
    x86::Assembler a(&code);

    a.pushad();
    a.pushfd();

    a.push(x86::ebx); // y-coord

    // x-coord
    a.xor_(x86::eax, x86::eax);
    a.mov(x86::eax, x86::ptr(x86::esp, 72));
    a.push(x86::eax);

    a.push(x86::ebp); // settler index
    a.push(x86::esi); // game data
    a.call(&is_thief_allowed_to_steal_at);
    a.add(x86::esp, 16);
    a.popfd();
    a.cmp(x86::eax, 0);
    a.popad();
    while (code.textSection()->bufferSize() < endPos - startPos) {
        a.nop();
    }

    lookingForGoodPatch.apply(startPos, (char*)code.textSection()->data(), code.textSection()->bufferSize());
}

void ThiefPatch::OnAfterGameInit(logic_thread* logicThread) {
    if (s3_get_game_settings()->isSaveGame) {
        return;
    }

    // If patch is active, be nice and send a bow into the starting tower to protect the starting goods.
    for (int i = 1; i <= logicThread->gameData->numBuildings; i++) {
        auto building = &logicThread->gameData->buildingData[i];
        if (building->constructionState) {
            continue;
        }
        if (building->type != BUILDING_SMALL_TOWER && building->type != BUILDING_BIG_TOWER && building->type != BUILDING_FORTRESS) {
            continue;
        }

        auto tower = reinterpret_cast<game_data_finished_military_building*>(building);
        if (tower->numRequestedBows <= 0) {
            tower->numRequestedBows = 1;
        }
    }
}
