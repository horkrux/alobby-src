#include "EnhancedWidescreenFix.h"

#include <asmjit/x86.h>
#include <constants.h>

using namespace asmjit;

uintptr_t patchLocations[67];
int patchSizes[67];
char testMem[0x10000];

void EnhancedWidescreenFix::applyPatch() {
    patchLocations[0] = Constants::S3_EXE + 0xB2D6C;
    patchLocations[1] = Constants::S3_EXE + 0xB2D73;
    patchLocations[2] = Constants::S3_EXE + 0xB2DA7;
    patchLocations[3] = Constants::S3_EXE + 0xB34D3;
    patchLocations[4] = Constants::S3_EXE + 0xB34E2;
    patchLocations[5] = Constants::S3_EXE + 0xB3565;
    patchLocations[6] = Constants::S3_EXE + 0xB3574;
    patchLocations[7] = Constants::S3_EXE + 0xD7C9C;
    patchLocations[8] = Constants::S3_EXE + 0xD7CB7;
    patchLocations[9] = Constants::S3_EXE + 0xD7CCD;
    patchLocations[10] = Constants::S3_EXE + 0xD7CE2;
    patchLocations[11] = Constants::S3_EXE + 0xD7CF6;
    patchLocations[12] = Constants::S3_EXE + 0xD7D0B;
    patchLocations[13] = Constants::S3_EXE + 0xD7D28;
    patchLocations[14] = Constants::S3_EXE + 0xD7D3C;
    patchLocations[15] = Constants::S3_EXE + 0xD7D45;
    patchLocations[16] = Constants::S3_EXE + 0xD7D5C;
    patchLocations[17] = Constants::S3_EXE + 0xD7D77;
    patchLocations[18] = Constants::S3_EXE + 0xD7D8D;
    patchLocations[19] = Constants::S3_EXE + 0xD7DA2;
    patchLocations[20] = Constants::S3_EXE + 0xD7DB6;
    patchLocations[21] = Constants::S3_EXE + 0xD7DCB;
    patchLocations[22] = Constants::S3_EXE + 0xD7DE8;
    patchLocations[23] = Constants::S3_EXE + 0xD7DFC;
    patchLocations[24] = Constants::S3_EXE + 0xD7E05;
    patchLocations[25] = Constants::S3_EXE + 0xD7E1C;
    patchLocations[26] = Constants::S3_EXE + 0xD7E37;
    patchLocations[27] = Constants::S3_EXE + 0xD7E53;
    patchLocations[28] = Constants::S3_EXE + 0xD7E6E;
    patchLocations[29] = Constants::S3_EXE + 0xD7E89;
    patchLocations[30] = Constants::S3_EXE + 0xD7EA4;
    patchLocations[31] = Constants::S3_EXE + 0xD7EC1;
    patchLocations[32] = Constants::S3_EXE + 0xD7EDC;
    patchLocations[33] = Constants::S3_EXE + 0xD7EFF;
    patchLocations[34] = Constants::S3_EXE + 0xD7F13;
    patchLocations[35] = Constants::S3_EXE + 0xD7F1C;
    patchLocations[36] = Constants::S3_EXE + 0xD7F3C;
    patchLocations[37] = Constants::S3_EXE + 0xD7F57;
    patchLocations[38] = Constants::S3_EXE + 0xD7F73;
    patchLocations[39] = Constants::S3_EXE + 0xD7F8E;
    patchLocations[40] = Constants::S3_EXE + 0xD7FA9;
    patchLocations[41] = Constants::S3_EXE + 0xD7FC4;
    patchLocations[42] = Constants::S3_EXE + 0xD7FE1;
    patchLocations[43] = Constants::S3_EXE + 0xD7FFC;
    patchLocations[44] = Constants::S3_EXE + 0xD801F;
    patchLocations[45] = Constants::S3_EXE + 0xD8033;
    patchLocations[46] = Constants::S3_EXE + 0xD803C;
    patchLocations[47] = Constants::S3_EXE + 0xD805C;
    patchLocations[48] = Constants::S3_EXE + 0xD8077;
    patchLocations[49] = Constants::S3_EXE + 0xD8093;
    patchLocations[50] = Constants::S3_EXE + 0xD80AE;
    patchLocations[51] = Constants::S3_EXE + 0xD80C9;
    patchLocations[52] = Constants::S3_EXE + 0xD80E4;
    patchLocations[53] = Constants::S3_EXE + 0xD8101;
    patchLocations[54] = Constants::S3_EXE + 0xD811C;
    patchLocations[55] = Constants::S3_EXE + 0xD813F;
    patchLocations[56] = Constants::S3_EXE + 0xD8153;
    patchLocations[57] = Constants::S3_EXE + 0xD815C;
    patchLocations[58] = Constants::S3_EXE + 0xD8177;
    patchLocations[59] = Constants::S3_EXE + 0xD8184;
    patchLocations[60] = Constants::S3_EXE + 0xD81AF;
    patchLocations[61] = Constants::S3_EXE + 0xD81C6;
    patchLocations[62] = Constants::S3_EXE + 0xD81DB;
    patchLocations[63] = Constants::S3_EXE + 0xD81F0;
    patchLocations[64] = Constants::S3_EXE + 0xD820D;
    patchLocations[65] = Constants::S3_EXE + 0xD8221;
    patchLocations[66] = Constants::S3_EXE + 0xD822A;

    int prevSize = 0;

    Environment env;
    env.setArch(Environment::kArchX86);
    CodeHolder code;
    code.init(env);
    x86::Assembler a(&code);

    a.cmp(x86::eax, 0x1F4);
    patchSizes[0] = a.offset();
    prevSize += patchSizes[0];

    a.mov(x86::ptr((uint64_t)testMem, x86::eax, 2), x86::eax);
    a.mov(x86::ptr((uint64_t)testMem + 0x800, x86::eax, 2), x86::esi);
    a.mov(x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2), x86::eax);
    a.lea(x86::ecx, x86::ptr(x86::eax, 1));
    a.mov(x86::ptr((uint64_t)testMem + 0x1800, x86::eax, 2), x86::esi);
    a.mov(x86::ptr((uint64_t)testMem + 0x2000, x86::eax, 2), x86::ecx);
    a.mov(x86::ptr((uint64_t)testMem + 0x2800, x86::eax, 2), x86::esi);
    patchSizes[1] = a.offset() - prevSize;
    prevSize += patchSizes[1];

    a.mov(x86::dword_ptr(x86::ebx, 0x1425C), 0x1F4);
    a.mov(x86::dword_ptr(x86::ebx, 0x4B530), 0x1F4);
    a.mov(x86::dword_ptr(x86::ebx, 0x5F584), 0x1F5);
    patchSizes[2] = a.offset() - prevSize;
    prevSize += patchSizes[2];

    a.cmp(x86::ecx, 0x1F4);
    patchSizes[3] = a.offset() - prevSize;
    prevSize += patchSizes[3];

    a.mov(x86::eax, x86::ptr((uint64_t)testMem + 0x800, x86::ecx, 2));
    patchSizes[4] = a.offset() - prevSize;
    prevSize += patchSizes[4];

    a.cmp(x86::ecx, 0x1F4);
    patchSizes[5] = a.offset() - prevSize;
    prevSize += patchSizes[5];

    a.mov(x86::eax, x86::ptr((uint64_t)testMem + 0x1800, x86::ecx, 2));
    patchSizes[6] = a.offset() - prevSize;
    prevSize += patchSizes[6];

    a.cmp(x86::eax, 0x1F4);
    patchSizes[7] = a.offset() - prevSize;
    prevSize += patchSizes[7];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem, x86::eax, 2));
    patchSizes[8] = a.offset() - prevSize;
    prevSize += patchSizes[8];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem, x86::eax, 2));
    patchSizes[9] = a.offset() - prevSize;
    prevSize += patchSizes[9];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem, x86::eax, 2));
    patchSizes[10] = a.offset() - prevSize;
    prevSize += patchSizes[10];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem, x86::eax, 2));
    patchSizes[11] = a.offset() - prevSize;
    prevSize += patchSizes[11];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem, x86::eax, 2));
    patchSizes[12] = a.offset() - prevSize;
    prevSize += patchSizes[12];

    a.mov(x86::ptr((uint64_t)testMem, x86::eax, 2), x86::edx);
    patchSizes[13] = a.offset() - prevSize;
    prevSize += patchSizes[13];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x800, x86::eax, 2));
    patchSizes[14] = a.offset() - prevSize;
    prevSize += patchSizes[14];

    a.mov(x86::ptr((uint64_t)testMem + 0x800, x86::eax, 2), x86::edx);
    patchSizes[15] = a.offset() - prevSize;
    prevSize += patchSizes[15];

    a.cmp(x86::eax, 0x1F4);
    patchSizes[16] = a.offset() - prevSize;
    prevSize += patchSizes[16];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem, x86::eax, 2));
    patchSizes[17] = a.offset() - prevSize;
    prevSize += patchSizes[17];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem, x86::eax, 2));
    patchSizes[18] = a.offset() - prevSize;
    prevSize += patchSizes[18];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem, x86::eax, 2));
    patchSizes[19] = a.offset() - prevSize;
    prevSize += patchSizes[19];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem, x86::eax, 2));
    patchSizes[20] = a.offset() - prevSize;
    prevSize += patchSizes[20];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem, x86::eax, 2));
    patchSizes[21] = a.offset() - prevSize;
    prevSize += patchSizes[21];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem, x86::eax, 2));
    patchSizes[22] = a.offset() - prevSize;
    prevSize += patchSizes[22];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x800, x86::eax, 2));
    patchSizes[23] = a.offset() - prevSize;
    prevSize += patchSizes[23];

    a.mov(x86::ptr((uint64_t)testMem + 0x800, x86::eax, 2), x86::edx);
    patchSizes[24] = a.offset() - prevSize;
    prevSize += patchSizes[24];

    a.cmp(x86::eax, 0x1F4);
    patchSizes[25] = a.offset() - prevSize;
    prevSize += patchSizes[25];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[26] = a.offset() - prevSize;
    prevSize += patchSizes[26];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[27] = a.offset() - prevSize;
    prevSize += patchSizes[27];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[28] = a.offset() - prevSize;
    prevSize += patchSizes[28];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[29] = a.offset() - prevSize;
    prevSize += patchSizes[29];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[30] = a.offset() - prevSize;
    prevSize += patchSizes[30];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[31] = a.offset() - prevSize;
    prevSize += patchSizes[31];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[32] = a.offset() - prevSize;
    prevSize += patchSizes[32];

    a.mov(x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2), x86::edx);
    patchSizes[33] = a.offset() - prevSize;
    prevSize += patchSizes[33];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1800, x86::eax, 2));
    patchSizes[34] = a.offset() - prevSize;
    prevSize += patchSizes[34];

    a.mov(x86::ptr((uint64_t)testMem + 0x1800, x86::eax, 2), x86::edx);
    patchSizes[35] = a.offset() - prevSize;
    prevSize += patchSizes[35];
    
    a.cmp(x86::eax, 0x1F4);
    patchSizes[36] = a.offset() - prevSize;
    prevSize += patchSizes[36];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[37] = a.offset() - prevSize;
    prevSize += patchSizes[37];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[38] = a.offset() - prevSize;
    prevSize += patchSizes[38];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[39] = a.offset() - prevSize;
    prevSize += patchSizes[39];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[40] = a.offset() - prevSize;
    prevSize += patchSizes[40];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[41] = a.offset() - prevSize;
    prevSize += patchSizes[41];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[42] = a.offset() - prevSize;
    prevSize += patchSizes[42];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[43] = a.offset() - prevSize;
    prevSize += patchSizes[43];

    a.mov(x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2), x86::edx);
    patchSizes[44] = a.offset() - prevSize;
    prevSize += patchSizes[44];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1800, x86::eax, 2));
    patchSizes[45] = a.offset() - prevSize;
    prevSize += patchSizes[45];

    a.mov(x86::ptr((uint64_t)testMem + 0x1800, x86::eax, 2), x86::edx);
    patchSizes[46] = a.offset() - prevSize;
    prevSize += patchSizes[46];

    a.cmp(x86::eax, 0x1F4);
    patchSizes[47] = a.offset() - prevSize;
    prevSize += patchSizes[47];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[48] = a.offset() - prevSize;
    prevSize += patchSizes[48];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[49] = a.offset() - prevSize;
    prevSize += patchSizes[49];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[50] = a.offset() - prevSize;
    prevSize += patchSizes[50];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[51] = a.offset() - prevSize;
    prevSize += patchSizes[51];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[52] = a.offset() - prevSize;
    prevSize += patchSizes[52];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[53] = a.offset() - prevSize;
    prevSize += patchSizes[53];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2));
    patchSizes[54] = a.offset() - prevSize;
    prevSize += patchSizes[54];

    a.mov(x86::ptr((uint64_t)testMem + 0x1000, x86::eax, 2), x86::edx);
    patchSizes[55] = a.offset() - prevSize;
    prevSize += patchSizes[55];

    a.mov(x86::edx, x86::ptr((uint64_t)testMem + 0x1800, x86::eax, 2));
    patchSizes[56] = a.offset() - prevSize;
    prevSize += patchSizes[56];

    a.mov(x86::ptr((uint64_t)testMem + 0x1800, x86::eax, 2), x86::edx);
    patchSizes[57] = a.offset() - prevSize;
    prevSize += patchSizes[57];

    a.mov(x86::eax, x86::ptr((uint64_t)testMem + 0x2000, x86::edx, 2));
    patchSizes[58] = a.offset() - prevSize;
    prevSize += patchSizes[58];

    a.cmp(x86::edx, 0x1F4);
    patchSizes[59] = a.offset() - prevSize;
    prevSize += patchSizes[59];

    a.mov(x86::esi, x86::ptr((uint64_t)testMem + 0x2000, x86::edx, 2));
    patchSizes[60] = a.offset() - prevSize;
    prevSize += patchSizes[60];

    a.mov(x86::esi, x86::ptr((uint64_t)testMem + 0x2000, x86::edx, 2));
    patchSizes[61] = a.offset() - prevSize;
    prevSize += patchSizes[61];

    a.mov(x86::esi, x86::ptr((uint64_t)testMem + 0x2000, x86::edx, 2));
    patchSizes[62] = a.offset() - prevSize;
    prevSize += patchSizes[62];

    a.mov(x86::esi, x86::ptr((uint64_t)testMem + 0x2000, x86::edx, 2));
    patchSizes[63] = a.offset() - prevSize;
    prevSize += patchSizes[63];

    a.mov(x86::ptr((uint64_t)testMem + 0x2000, x86::edx, 2), x86::esi);
    patchSizes[64] = a.offset() - prevSize;
    prevSize += patchSizes[64];

    a.mov(x86::esi, x86::ptr((uint64_t)testMem + 0x2800, x86::edx, 2));
    patchSizes[65] = a.offset() - prevSize;
    prevSize += patchSizes[65];

    a.mov(x86::ptr((uint64_t)testMem + 0x2800, x86::edx, 2), x86::esi);
    patchSizes[66] = a.offset() - prevSize;

    char* data = (char*)code.textSection()->data();

    for (int i = 0; i < 67; i++) {
        patches[i].apply(patchLocations[i], data, patchSizes[i]);
        data += patchSizes[i];
    }
    //patches[0].apply(Constants::S3_EXE + 0xB2D6C, (char*)code.textSection()->data(), code.textSection()->bufferSize());
    
}

void EnhancedWidescreenFix::enable() {
    applyPatch();

    // For debugging purposes only.
    //    triggerFreezeCondition();
}

void EnhancedWidescreenFix::disable() {
    for (int i = 0; i < 67; i++) {
        patches[i].revert();
    }
}