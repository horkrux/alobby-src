#include "MainScreenPatch.h"

#include <imgui.h>
#include <vector>
#include <map>
#include <imgui_utils.h>

void MainScreenPatch::enable() {
    MachineCodePatchBuilder builder(Constants::S3_EXE + 0x75163);
    builder.a->jmp(reinterpret_cast<uint64_t*>(Constants::S3_EXE + 0x75185));
    builder.apply(&m_hideMultiplayerInternetButtonPatch);
}

void MainScreenPatch::disable() {
    m_hideMultiplayerInternetButtonPatch.revert();
}

void MainScreenPatch::OnRenderFrame(RenderContext* context) {
    if (context->activeDialogId != START_SCREEN_DIALOG_ID) {
        return;
    }

    typedef struct available_setting {
        int id;
        std::string label;
    } available_setting;

    static bool isSettingsOpen = false;
    static std::map<int, std::string> availableSettings = {
        { SETTING_ID_COAL_TO_GOLD_SMELTER, "Coal to Gold Smelter" },
        { SETTING_ID_COAL_TO_TOOL_SMITH, "Coal to Tool Smith" },
        { SETTING_ID_COAL_TO_WEAPON_SMITH, "Coal to Weapon Smith" },
        { SETTING_ID_COAL_TO_DISTILLERY, "Coal to Distillery" },
        { SETTING_ID_GRAIN_TO_BREWERY, "Grain to Brewery" },
        { SETTING_ID_WATER_TO_BREWERY, "Water to Brewery" },
        { SETTING_ID_FISH_TO_COAL, "Fish to Coal Mine" },
        { SETTING_ID_FISH_TO_IRON, "Fish to Iron Mine" },
        { SETTING_ID_FISH_TO_GEM_MINE, "Fish to Gem Mine" },
    };

    static int fightingSoundVolume = 85;
    static bool changeWeaponProduction;
    static bool stopRecruiting;
    static int swords = 0, bows = 0, spears = 0;
    static bool showInGameTime = false;
    static bool adjustItemStorage = false;
    static bool enableTeamChat = false;
    static std::vector<settler_distribution_change> settlerDistributionChanges;
    static std::vector<good_distribution_change> goodDistributionChanges;

    if (ImGui::Begin("#settingButtonContainer", NULL,
                     ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize |
                     ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_AlwaysAutoResize)) {
        auto windowSize = ImGui::GetWindowSize();
        auto targetRight = ImScaledVec2(390, 605);

        ImGui::SetWindowPos(ImVec2(targetRight.x - windowSize.x, targetRight.y - windowSize.y));

        ImGui::PushStyleColor(ImGuiCol_Button, static_cast<ImVec4>(ImColor(148, 130, 57, 255)));
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, static_cast<ImVec4>(ImColor(132, 117, 49, 255)));
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered, static_cast<ImVec4>(ImColor(190, 170, 90, 255)));
        ImGui::PushStyleColor(ImGuiCol_Text, static_cast<ImVec4>(ImColor(0, 5, 40, 255)));
        ImGui::PushStyleColor(ImGuiCol_Border, static_cast<ImVec4>(ImColor(90, 73, 8, 255)));
        ImGui::PushStyleVar(ImGuiStyleVar_FrameBorderSize, 1.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(15, 7));
        if (ImGui::Button("Community Edition Settings")) {
            fightingSoundVolume = m_config->getFightingSoundVolume();
            stopRecruiting = m_config->shouldStopRecruiting();
            changeWeaponProduction = m_config->shouldChangeWeaponProduction();
            swords = m_config->getWeaponProductionSwords();
            bows = m_config->getWeaponProductionBows();
            spears = m_config->getWeaponProductionSpears();
            showInGameTime = m_config->shouldShowInGameTime();
            adjustItemStorage = m_config->shouldAdjustItemStorage();
            enableTeamChat = m_config->shouldEnableTeamChat();
            settlerDistributionChanges = m_config->getSettlerDistributionChanges();
            goodDistributionChanges = m_config->getGoodDistributionChanges();

            ImGui::OpenPopup("Community Edition Settings");
        }
        ImGui::PopStyleColor(5);
        ImGui::PopStyleVar(2);

        ImGui::PushStyleColor(ImGuiCol_Button, ImGui::GetColorU32(ImGuiCol_Button, 1.0f));
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImGui::GetColorU32(ImGuiCol_ButtonActive, 1.0f));
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImGui::GetColorU32(ImGuiCol_ButtonHovered, 1.0f));
        ImGui::PushStyleColor(ImGuiCol_FrameBg, static_cast<ImVec4>(ImColor(50, 50, 50, 255)));
        ImGui::PushStyleColor(ImGuiCol_FrameBgActive, static_cast<ImVec4>(ImColor(70, 70, 70, 255)));
        ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, static_cast<ImVec4>(ImColor(60, 60, 60, 255)));
        ImGui::SetNextWindowBgAlpha(1.0f);
        ImGui::SetNextWindowSizeConstraints(ImScaledVec2(600, 0), ImScaledVec2(800, 600));
        if (ImGui::BeginPopupModal("Community Edition Settings", nullptr,
                                   ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings)) {
            if (ImGui::BeginTabBar("##setting_categories")) {
                if (ImGui::BeginTabItem("Soldiers")) {
                    ImGui::Checkbox("Stop Recruiting", &stopRecruiting);
                    ImGui::Checkbox("Change Weapon Production", &changeWeaponProduction);
                    ImGui::SliderInt("Swords", &swords, SETTING_SCALE_MIN, SETTING_SCALE_MAX, "", ImGuiSliderFlags_AlwaysClamp);
                    ImGui::SliderInt("Bows", &bows, SETTING_SCALE_MIN, SETTING_SCALE_MAX, "", ImGuiSliderFlags_AlwaysClamp);
                    ImGui::SliderInt("Spears", &spears, SETTING_SCALE_MIN, SETTING_SCALE_MAX, "", ImGuiSliderFlags_AlwaysClamp);

                    ImGui::EndTabItem();
                }

                if (ImGui::BeginTabItem("Settler Distribution")) {
                    if (settlerDistributionChanges.size() > 0) {
                        if (settlerDistributionChanges.size() > 1) {
                            ImGui::TextDisabled("Only the first matching entry is used for the given game.");
                        }

                        if (ImGui::BeginTable("##settler_distribution_table", 5, ImGuiTableFlags_SizingStretchProp)) {
                            ImGui::TableSetupColumn("Setting", ImGuiTableColumnFlags_NoSort);
                            ImGui::TableSetupColumn("Race", ImGuiTableColumnFlags_NoSort);
                            ImGui::TableSetupColumn("Starting Goods", ImGuiTableColumnFlags_NoSort);
                            ImGui::TableSetupColumn("Maps", ImGuiTableColumnFlags_NoSort);
                            ImGui::TableHeadersRow();

                            unsigned int actionIndex = 0;
                            int actionType = -1;
                            int i = 0;
                            for (auto change : settlerDistributionChanges) {
                                ImGui::TableNextRow();

                                ImGui::TableNextColumn();
                                std::string text("");
                                text.append(std::to_string(change.carrier));
                                text.append("%/");
                                text.append(std::to_string(change.digger));
                                text.append("%/");
                                text.append(std::to_string(change.builder));
                                text.append("%");
                                ImGui::Text("%s", text.c_str());

                                ImGui::TableNextColumn();
                                if (change.selectedRace == -1) {
                                    ImGui::Text("All");
                                } else if (change.selectedRace == RACE_ROMAN) {
                                    ImGui::Text("Roman");
                                } else if (change.selectedRace == RACE_EGYPT) {
                                    ImGui::Text("Egypt");
                                } else if (change.selectedRace == RACE_ASIA) {
                                    ImGui::Text("Asia");
                                } else if (change.selectedRace == RACE_AMAZON) {
                                    ImGui::Text("Amazons");
                                }

                                ImGui::TableNextColumn();
                                if (change.selectedGoods == -1) {
                                    ImGui::Text("All");
                                } else if (change.selectedGoods == GOODS_LOW) {
                                    ImGui::Text("Low");
                                } else if (change.selectedGoods == GOODS_MEDIUM) {
                                    ImGui::Text("Medium");
                                } else if (change.selectedGoods == GOODS_HIGH) {
                                    ImGui::Text("High");
                                }

                                ImGui::TableNextColumn();
                                if (change.selectedMap == -1) {
                                    ImGui::Text("All");
                                } else if (change.selectedMap == 0) {
                                    ImGui::Text("Random");
                                } else if (change.selectedMap == 1) {
                                    ImGui::Text("Non Random");
                                }

                                ImGui::TableNextColumn();
                                std::string upBtn("Up##sd_row_" + std::to_string(i));
                                if (ImGui::Button(upBtn.c_str())) {
                                    actionIndex = i;
                                    actionType = 1;
                                }
                                ImGui::SameLine();
                                std::string downBtn("Down##sd_row_" + std::to_string(i));
                                if (ImGui::Button(downBtn.c_str())) {
                                    actionIndex = i;
                                    actionType = 2;
                                }
                                ImGui::SameLine();
                                std::string removeBtn("Remove##std_row_" + std::to_string(i));
                                if (ImGui::Button(removeBtn.c_str())) {
                                    actionIndex = i;
                                    actionType = 3;
                                }

                                i++;
                            }

                            if (actionType == 1) { // Up
                                if (actionIndex > 0) {
                                    auto prevIndexChange = settlerDistributionChanges[actionIndex - 1];
                                    settlerDistributionChanges[actionIndex - 1] = settlerDistributionChanges[actionIndex];
                                    settlerDistributionChanges[actionIndex] = prevIndexChange;
                                }
                            } else if (actionType == 2) { // Down
                                if (actionIndex + 1 < settlerDistributionChanges.size()) {
                                    auto nextIndexChange = settlerDistributionChanges[actionIndex + 1];
                                    settlerDistributionChanges[actionIndex + 1] = settlerDistributionChanges[actionIndex];
                                    settlerDistributionChanges[actionIndex] = nextIndexChange;
                                }
                            } else if (actionType == 3) { // Remove
                                settlerDistributionChanges.erase(settlerDistributionChanges.begin() + actionIndex);
                            }

                            ImGui::EndTable();
                        }
                    } else {
                        ImGui::TextDisabled("There are no custom settler distribution settings, the classic defaults will be used.");
                    }

                    if (ImGui::Button("Add Settler Change")) {
                        ImGui::OpenPopup("Add Settler Change");
                    }

                    ImGui::SetNextWindowBgAlpha(1.0f);
                    if (ImGui::BeginPopup("Add Settler Change")) {
                        static int carrier = 20, digger = 25, builder = 25;
                        ImGui::Text("Distribution");
                        if (ImGui::SliderInt("Carrier", &carrier, 5, 50, "%d%%", ImGuiSliderFlags_AlwaysClamp)) {
                            carrier = static_cast<int>(std::roundf(static_cast<float>(carrier) / 5.0f)) * 5;
                            if (carrier + digger + builder > 100) {
                                carrier = 100 - digger - builder;
                            }
                        }
                        if (ImGui::SliderInt("Digger", &digger, 0, 50, "%d%%", ImGuiSliderFlags_AlwaysClamp)) {
                            digger = static_cast<int>(std::roundf(static_cast<float>(digger) / 5.0f)) * 5;
                            if (carrier + digger + builder > 100) {
                                digger = 100 - carrier - builder;
                            }
                        }
                        if (ImGui::SliderInt("Builder", &builder, 0, 50, "%d%%", ImGuiSliderFlags_AlwaysClamp)) {
                            builder = static_cast<int>(std::roundf(static_cast<float>(builder) / 5.0f)) * 5;
                            if (carrier + digger + builder > 100) {
                                builder = 100 - carrier - digger;
                            }
                        }

                        ImGui::NewLine();
                        ImGui::Text("Conditions");
                        static int selectedRace = -1;
                        std::string selectedRacePreview("Race: ");
                        if (selectedRace == RACE_ROMAN) {
                            selectedRacePreview.append("Romans");
                        } else if (selectedRace == RACE_EGYPT) {
                            selectedRacePreview.append("Egypt");
                        } else if (selectedRace == RACE_ASIA) {
                            selectedRacePreview.append("Asia");
                        } else if (selectedRace == RACE_AMAZON) {
                            selectedRacePreview.append("Amazons");
                        } else {
                            selectedRacePreview = "All Races";
                        }

                        if (ImGui::BeginCombo("##sd_race", selectedRacePreview.c_str())) {
                            if (ImGui::Selectable("All Races")) {
                                selectedRace = -1;
                            }
                            if (ImGui::Selectable("Romans")) {
                                selectedRace = RACE_ROMAN;
                            }
                            if (ImGui::Selectable("Egypt")) {
                                selectedRace = RACE_EGYPT;
                            }
                            if (ImGui::Selectable("Asia")) {
                                selectedRace = RACE_ASIA;
                            }
                            if (ImGui::Selectable("Amazons")) {
                                selectedRace = RACE_AMAZON;
                            }
                            ImGui::EndCombo();
                        }

                        static int selectedGoods = -1;
                        std::string selectedGoodsPreview("Starting Goods: ");
                        if (selectedGoods == GOODS_LOW) {
                            selectedGoodsPreview.append("Low");
                        } else if (selectedGoods == GOODS_MEDIUM) {
                            selectedGoodsPreview.append("Medium");
                        } else if (selectedGoods == GOODS_HIGH) {
                            selectedGoodsPreview.append("High");
                        } else {
                            selectedGoodsPreview = "All Starting Goods";
                        }

                        if (ImGui::BeginCombo("##sd_starting_goods", selectedGoodsPreview.c_str())) {
                            if (ImGui::Selectable("All Starting Goods")) {
                                selectedGoods = -1;
                            }
                            if (ImGui::Selectable("Low")) {
                                selectedGoods = GOODS_LOW;
                            }
                            if (ImGui::Selectable("Medium")) {
                                selectedGoods = GOODS_MEDIUM;
                            }
                            if (ImGui::Selectable("High")) {
                                selectedGoods = GOODS_HIGH;
                            }
                            ImGui::EndCombo();
                        }

                        static int selectedMap = -1;
                        std::string selectedMapPreview("Map: ");
                        if (selectedMap == 0) {
                            selectedMapPreview.append("Random");
                        } else if (selectedMap == 1) {
                            selectedMapPreview.append("Non Random");
                        } else {
                            selectedMapPreview = "All Maps";
                        }

                        if (ImGui::BeginCombo("##sd_map", selectedMapPreview.c_str())) {
                            if (ImGui::Selectable("All Maps")) {
                                selectedMap = -1;
                            }
                            if (ImGui::Selectable("Random")) {
                                selectedMap = 0;
                            }
                            if (ImGui::Selectable("Non Random")) {
                                selectedMap = 1;
                            }
                            ImGui::EndCombo();
                        }

                        if (ImGui::Button("Add Change##sd")) {
                            settlerDistributionChanges.insert(settlerDistributionChanges.begin(),
                                                              { static_cast<char>(carrier), static_cast<char>(digger),
                                                                static_cast<char>(builder), static_cast<char>(selectedRace),
                                                                static_cast<char>(selectedGoods), static_cast<char>(selectedMap) });

                            carrier = 20;
                            digger = 25;
                            builder = 25;
                            selectedRace = -1;
                            selectedMap = -1;
                            selectedGoods = -1;

                            ImGui::CloseCurrentPopup();
                        }

                        ImGui::EndPopup();
                    }

                    ImGui::EndTabItem();
                }

                if (ImGui::BeginTabItem("Good Distribution")) {
                    if (goodDistributionChanges.size() > 0) {
                        if (goodDistributionChanges.size() > 1) {
                            ImGui::TextDisabled("All matching entries will be applied in the shown order in a given game.");
                        }

                        if (ImGui::BeginTable("##good_distribution_table", 6, ImGuiTableFlags_SizingStretchProp)) {
                            ImGui::TableSetupColumn("Setting", ImGuiTableColumnFlags_NoSort);
                            ImGui::TableSetupColumn("Value", ImGuiTableColumnFlags_NoSort);
                            ImGui::TableSetupColumn("Race", ImGuiTableColumnFlags_NoSort);
                            ImGui::TableSetupColumn("Starting Goods", ImGuiTableColumnFlags_NoSort);
                            ImGui::TableSetupColumn("Maps", ImGuiTableColumnFlags_NoSort);
                            ImGui::TableHeadersRow();

                            unsigned int actionIndex = 0;
                            int actionType = -1;
                            int i = 0;
                            for (auto change : goodDistributionChanges) {
                                if (availableSettings.find(change.settingId) == availableSettings.end()) {
                                    continue;
                                }

                                ImGui::TableNextRow();

                                ImGui::TableNextColumn();
                                ImGui::Text("%s", availableSettings[change.settingId].c_str());

                                ImGui::TableNextColumn();
                                ImGui::Text("%.02f%%", static_cast<float>(change.settingValue) / SETTING_SCALE_MAX * 100.0f);

                                ImGui::TableNextColumn();
                                if (change.selectedRace == -1) {
                                    ImGui::Text("All");
                                } else if (change.selectedRace == RACE_ROMAN) {
                                    ImGui::Text("Roman");
                                } else if (change.selectedRace == RACE_EGYPT) {
                                    ImGui::Text("Egypt");
                                } else if (change.selectedRace == RACE_ASIA) {
                                    ImGui::Text("Asia");
                                } else if (change.selectedRace == RACE_AMAZON) {
                                    ImGui::Text("Amazons");
                                }

                                ImGui::TableNextColumn();
                                if (change.selectedGoods == -1) {
                                    ImGui::Text("All");
                                } else if (change.selectedGoods == GOODS_LOW) {
                                    ImGui::Text("Low");
                                } else if (change.selectedGoods == GOODS_MEDIUM) {
                                    ImGui::Text("Medium");
                                } else if (change.selectedGoods == GOODS_HIGH) {
                                    ImGui::Text("High");
                                }

                                ImGui::TableNextColumn();
                                if (change.selectedMap == -1) {
                                    ImGui::Text("All");
                                } else if (change.selectedMap == 0) {
                                    ImGui::Text("Random");
                                } else if (change.selectedMap == 1) {
                                    ImGui::Text("Non Random");
                                }

                                ImGui::TableNextColumn();
                                std::string upBtn("Up##gd_row_" + std::to_string(i));
                                if (ImGui::Button(upBtn.c_str())) {
                                    actionIndex = i;
                                    actionType = 1;
                                }
                                ImGui::SameLine();
                                std::string downBtn("Down##gd_row_" + std::to_string(i));
                                if (ImGui::Button(downBtn.c_str())) {
                                    actionIndex = i;
                                    actionType = 2;
                                }
                                ImGui::SameLine();
                                std::string removeBtn("Remove##gd_row_" + std::to_string(i));
                                if (ImGui::Button(removeBtn.c_str())) {
                                    actionIndex = i;
                                    actionType = 3;
                                }

                                i++;
                            }

                            if (actionType == 1) { // Up
                                if (actionIndex > 0) {
                                    auto prevIndexChange = goodDistributionChanges[actionIndex - 1];
                                    goodDistributionChanges[actionIndex - 1] = goodDistributionChanges[actionIndex];
                                    goodDistributionChanges[actionIndex] = prevIndexChange;
                                }
                            } else if (actionType == 2) { // Down
                                if (actionIndex + 1 < goodDistributionChanges.size()) {
                                    auto nextIndexChange = goodDistributionChanges[actionIndex + 1];
                                    goodDistributionChanges[actionIndex + 1] = goodDistributionChanges[actionIndex];
                                    goodDistributionChanges[actionIndex] = nextIndexChange;
                                }
                            } else if (actionType == 3) { // Remove
                                goodDistributionChanges.erase(goodDistributionChanges.begin() + actionIndex);
                            }

                            ImGui::EndTable();
                        }

                    } else {
                        ImGui::TextDisabled("There are no custom good distribution changes, the classic defaults will be used.");
                    }

                    if (ImGui::Button("Add Good Change")) {
                        ImGui::OpenPopup("##add_gd_change");
                    }

                    ImGui::SetNextWindowBgAlpha(1.0f);
                    if (ImGui::BeginPopup("##add_gd_change")) {
                        static int selectedSetting = -1;
                        static int settingValue = 0;
                        std::string settingPreviewText;

                        ImGui::Text("Good Distribution");

                        if (selectedSetting == -1) {
                            settingPreviewText = "Select a setting";
                        } else if (availableSettings.find(selectedSetting) != availableSettings.end()) {
                            settingPreviewText = availableSettings[selectedSetting];
                        }

                        if (ImGui::BeginCombo("##gd_change_setting", settingPreviewText.c_str())) {
                            for (auto entry : availableSettings) {
                                if (ImGui::Selectable(entry.second.c_str(), entry.first == selectedSetting)) {
                                    selectedSetting = entry.first;
                                }
                            }

                            ImGui::EndCombo();
                        }

                        ImGui::SliderInt("##gd_change_value", &settingValue, SETTING_SCALE_MIN, SETTING_SCALE_MAX, "",
                                         ImGuiSliderFlags_AlwaysClamp);

                        ImGui::NewLine();
                        ImGui::Text("Conditions");
                        static int selectedRace = -1;
                        std::string selectedRacePreview("Race: ");
                        if (selectedRace == RACE_ROMAN) {
                            selectedRacePreview.append("Romans");
                        } else if (selectedRace == RACE_EGYPT) {
                            selectedRacePreview.append("Egypt");
                        } else if (selectedRace == RACE_ASIA) {
                            selectedRacePreview.append("Asia");
                        } else if (selectedRace == RACE_AMAZON) {
                            selectedRacePreview.append("Amazons");
                        } else {
                            selectedRacePreview = "All Races";
                        }

                        if (ImGui::BeginCombo("##gd_race", selectedRacePreview.c_str())) {
                            if (ImGui::Selectable("All Races")) {
                                selectedRace = -1;
                            }
                            if (ImGui::Selectable("Romans")) {
                                selectedRace = RACE_ROMAN;
                            }
                            if (ImGui::Selectable("Egypt")) {
                                selectedRace = RACE_EGYPT;
                            }
                            if (ImGui::Selectable("Asia")) {
                                selectedRace = RACE_ASIA;
                            }
                            if (ImGui::Selectable("Amazons")) {
                                selectedRace = RACE_AMAZON;
                            }
                            ImGui::EndCombo();
                        }

                        static int selectedGoods = -1;
                        std::string selectedGoodsPreview("Starting Goods: ");
                        if (selectedGoods == GOODS_LOW) {
                            selectedGoodsPreview.append("Low");
                        } else if (selectedGoods == GOODS_MEDIUM) {
                            selectedGoodsPreview.append("Medium");
                        } else if (selectedGoods == GOODS_HIGH) {
                            selectedGoodsPreview.append("High");
                        } else {
                            selectedGoodsPreview = "All Starting Goods";
                        }

                        if (ImGui::BeginCombo("##gd_starting_goods", selectedGoodsPreview.c_str())) {
                            if (ImGui::Selectable("All Starting Goods")) {
                                selectedGoods = -1;
                            }
                            if (ImGui::Selectable("Low")) {
                                selectedGoods = GOODS_LOW;
                            }
                            if (ImGui::Selectable("Medium")) {
                                selectedGoods = GOODS_MEDIUM;
                            }
                            if (ImGui::Selectable("High")) {
                                selectedGoods = GOODS_HIGH;
                            }
                            ImGui::EndCombo();
                        }

                        static int selectedMap = -1;
                        std::string selectedMapPreview("Map: ");
                        if (selectedMap == 0) {
                            selectedMapPreview.append("Random");
                        } else if (selectedMap == 1) {
                            selectedMapPreview.append("Non Random");
                        } else {
                            selectedMapPreview = "All Maps";
                        }

                        if (ImGui::BeginCombo("##gd_map", selectedMapPreview.c_str())) {
                            if (ImGui::Selectable("All Maps")) {
                                selectedMap = -1;
                            }
                            if (ImGui::Selectable("Random")) {
                                selectedMap = 0;
                            }
                            if (ImGui::Selectable("Non Random")) {
                                selectedMap = 1;
                            }
                            ImGui::EndCombo();
                        }

                        if (ImGui::Button("Add Change##gd") && selectedSetting != -1) {
                            goodDistributionChanges.push_back({ static_cast<char>(selectedSetting), static_cast<char>(settingValue),
                                                                static_cast<char>(selectedRace), static_cast<char>(selectedGoods),
                                                                static_cast<char>(selectedMap) });

                            selectedSetting = -1;
                            settingValue = 0;
                            selectedRace = -1;
                            selectedGoods = -1;
                            selectedMap = -1;

                            ImGui::CloseCurrentPopup();
                        }

                        ImGui::EndPopup();
                    }

                    ImGui::EndTabItem();
                }

                if (ImGui::BeginTabItem("Misc")) {
                    ImGui::Text("Fighting Sounds");
                    ImGui::SameLine();
                    ImGui::SliderInt("Volume", &fightingSoundVolume, 0, 100, "%d%%", ImGuiSliderFlags_AlwaysClamp);

                    ImGui::Checkbox("Disable storage for all items except weapons", &adjustItemStorage);
                    ImGui::Checkbox("Show game time", &showInGameTime);
                    ImGui::Checkbox("Enable team chat", &enableTeamChat);

                    ImGui::EndTabItem();
                }

                ImGui::EndTabBar();
            }

            ImGui::NewLine();
            if (ImGui::Button("Save Changes")) {
                m_config->setGoodDistributionChanges(goodDistributionChanges);
                m_config->setSettlerDistributionChanges(settlerDistributionChanges);
                m_config->setStopRecruiting(stopRecruiting);
                m_config->setChangeWeaponProduction(changeWeaponProduction);
                m_config->setWeaponProductionSwords(swords);
                m_config->setWeaponProductionBows(bows);
                m_config->setWeaponProductionSpears(spears);
                m_config->setAdjustItemStorage(adjustItemStorage);
                m_config->setFightingSoundVolume(fightingSoundVolume);
                m_config->setEnabledTeamChat(enableTeamChat);
                m_config->setShowInGameTime(showInGameTime);
                m_config->persistSettings();

                ImGui::CloseCurrentPopup();
            }

            ImGui::SameLine();
            ImGui::PushStyleColor(ImGuiCol_Button, static_cast<ImVec4>(ImColor(50, 50, 50, 200)));
            if (ImGui::Button("Cancel")) {
                ImGui::CloseCurrentPopup();
            }
            ImGui::PopStyleColor();

            ImGui::EndPopup();
        }
        ImGui::PopStyleColor(6);
    }
    ImGui::End();
}
