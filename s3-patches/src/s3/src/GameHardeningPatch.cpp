#include "GameHardeningPatch.h"
#include "s3.h"

void on_init_building(hl::CpuContext* ctx) {
    auto gameData = reinterpret_cast<game_data*>(ctx->ESI);
    auto buildingIndex = ctx->EBP;

    memset(&gameData->buildingData[buildingIndex], 0, sizeof(game_data_building));
}

void on_init_settler(hl::CpuContext* ctx) {
    auto gameData = reinterpret_cast<game_data*>(ctx->EDI);
    auto settlerIndex = ctx->EBP;

    memset(&gameData->settlerData[settlerIndex], 0, sizeof(game_data_settler));
}

void GameHardeningPatch::enable() {
    applyPatches();
}

void GameHardeningPatch::disable() {
    m_hooker.revert();
}

void GameHardeningPatch::applyPatches() {
    m_hooker.hookDetour(Constants::S3_EXE + 0xDD2DF, 5, on_init_building);
    m_hooker.hookDetour(Constants::S3_EXE + 0x10CA1F, 5, on_init_settler);
}

void GameHardeningPatch::OnAfterGameInit(logic_thread* logicThread) {
    // We have to revert this patch on Vanilla as it will cause de-syncs due to different checksums.
    if (*m_isVanillaVersion) {
        m_hooker.revert();
    } else if (!m_hooker.hasActiveHooks()) {
        applyPatches();
    }
}
