#include "PersistUnitGroupsPatch.h"

#define UNIT_GROUP_DATA_SIZE 2060
static void* unitGroupsStart = reinterpret_cast<void*>(Constants::S3_EXE + 0x3AD5F8);

typedef struct chunk_unit_groups : file_chunk_header {
    BYTE unitGroupsData[UNIT_GROUP_DATA_SIZE];
} chunk_unit_groups;

static bool pendingRestore = false;
static BYTE restoreData[UNIT_GROUP_DATA_SIZE];

void on_restore_unit_groups(hl::CpuContext* ctx) {
    if (pendingRestore) {
        memcpy(unitGroupsStart, restoreData, UNIT_GROUP_DATA_SIZE);
        *reinterpret_cast<int*>(unitGroupsStart) = 0;
        pendingRestore = false;
    }
}

void PersistUnitGroupsPatch::enable() {
    m_hooker.hookDetour(Constants::S3_EXE + 0xFCFDD, 6, on_restore_unit_groups);
}

void PersistUnitGroupsPatch::disable() {
    m_hooker.revert();
}

void PersistUnitGroupsPatch::OnSave(logic_thread* logicThread, void* saveFile) {
    file_chunk_header header;
    header.type = FILE_CHUNK_UNIT_GROUPS;
    header.metaInfo = 0;
    header.size = sizeof(chunk_unit_groups);

    s3_file_chunk_write(saveFile, FILE_CHUNK_UNIT_GROUPS, &header, UNIT_GROUP_DATA_SIZE, unitGroupsStart);
}

void PersistUnitGroupsPatch::OnLoadSave(logic_thread* logicThread, void* saveFile) {
    auto chunk = reinterpret_cast<chunk_unit_groups*>(s3_file_chunk_read(saveFile, FILE_CHUNK_UNIT_GROUPS));
    if (chunk) {
        memcpy(restoreData, chunk->unitGroupsData, sizeof(restoreData));
        pendingRestore = true;
    }

    s3_file_chunk_free(saveFile, FILE_CHUNK_UNIT_GROUPS);
}
