#ifndef SIEDLER3_FIXES_FUNCTIONHOOK_H
#define SIEDLER3_FIXES_FUNCTIONHOOK_H


class FunctionHook
{
public:
    FunctionHook(): m_ppPointer(nullptr), m_pDetour(nullptr) {};
    ~FunctionHook();
    void hook(void** ppPointer, void* pDetour);
    void revert();

private:
    void** m_ppPointer;
    void* m_pDetour;
    bool isActive();
};


#endif // SIEDLER3_FIXES_FUNCTIONHOOK_H
