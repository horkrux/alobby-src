#ifndef SIEDLER3_FIXES_STATSGRAPHPATCH_H
#define SIEDLER3_FIXES_STATSGRAPHPATCH_H

#include "Patch.h"
#include "S3EventDispatcher.h"
#include "DetourHooker.h"
#include "I18n.h"

#define MARKER_PLAYER_DIED 1
#define MARKER_GAME_OVER 2

typedef struct stats_player {
    DWORD tick;
    DWORD pointScore;
    DWORD settlerScore;
    DWORD buildingScore;
    DWORD foodScore;
    DWORD mineScore;
    DWORD goldScore;
    DWORD manaScore;
    DWORD soldierScore;
    DWORD battleScore;
} stats_player;

typedef struct event_marker {
    DWORD tick;
    DWORD markerType;
    WORD actingPlayer;
} event_marker;

typedef struct {
    char name[20];
    unsigned int offset;
} Category;

class StatsGraphPatch : public Patch,
                        public SaveListener,
                        public BeforeGameLoopListener,
                        public LoadSaveListener,
                        public AfterGameInitListener,
                        public GuiListener,
                        public GameLeaveListener {
public:
    StatsGraphPatch(I18n* i18n, bool* isVanillaVersion, uint32_t* gameVersion)
        : m_isVanillaVersion(isVanillaVersion), m_settlersVersion(gameVersion), m_i18n(i18n) {}
    void enable();
    void disable();
    void OnSave(logic_thread* logicThread, void* saveFile);
    void OnBeforeGameLoop(logic_thread* logicThread);
    void OnAfterGameInit(logic_thread* logicThread);
    void OnLoadSave(logic_thread* logicThread, void* saveFile);
    void OnRenderFrame(RenderContext* context);
    void OnGameLeave(logic_thread* logicThread, bool isGameOver);

private:
    std::vector<stats_player> m_statsSnapshots;
    std::vector<event_marker> m_eventMarkers;
    DetourHooker m_hooker;
    I18n* m_i18n;
    int m_lastGameTick;
    int m_nextSnapshotSecond;
    int m_nextSnapshotTick;
    bool m_isStatsHidden = false;
    std::vector<Category> m_categories;

    /**
     * True if the current game is a multiplayer game.
     *
     * This is different from the s3 game settings in that we determine the status only once at the beginning of a game, and then add this
     * status to the save game. It is therefore not possible to load a save as single player game to bypass the restriction unless you
     * temper with the save data itself.
     */
    bool m_isMultiplayerGame = false;

    /**
     * The initial game version this game was created with.
     */
    uint32_t m_initialGameVersion;

    /**
     * The currently active game version.
     */
    uint32_t* m_settlersVersion;

    bool* m_isVanillaVersion;
    unsigned int m_numPlayers;
    bool m_isGuiInitialized = false;
    bool m_showExtendedStats = false;
    int m_xValueCount;
    int* m_xAxisValues;
    void Initialize(logic_thread* logicThread);
    void initializeGuiData();
    void cleanUpGuiData();
    bool shouldHideStats(logic_thread* logicThread);
};

#endif // SIEDLER3_FIXES_STATSGRAPHPATCH_H
