#ifndef SIEDLER3_FIXES_RANDOMMAPPATCH_H
#define SIEDLER3_FIXES_RANDOMMAPPATCH_H

#include "Patch.h"
#include "FunctionHook.h"
#include "DetourHooker.h"
#include "s3_listeners.h"
#include "GameSettingsManager.h"
#include <hacklib/Patch.h>

class RandomMapPatch : public Patch, public AfterGameInitListener {
private:
    GameSettingsManager* m_gameSettingsManager;
    FunctionHook m_randomPickStartLocations;
    hl::Patch m_randomSeedPatch;
    DetourHooker m_hooker;

public:
    RandomMapPatch(GameSettingsManager* manager): m_gameSettingsManager(manager) { }
    void enable();
    void disable();
    void OnAfterGameInit(logic_thread* logicThread);
    GameSettingsManager* GetGameSettingsManager() { return m_gameSettingsManager; }
    void patchRandomSeed();
};


#endif // SIEDLER3_FIXES_RANDOMMAPPATCH_H
