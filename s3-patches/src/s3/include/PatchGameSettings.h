#ifndef SIEDLER3_FIXES_PATCHGAMESETTINGS_H
#define SIEDLER3_FIXES_PATCHGAMESETTINGS_H

#include <cstdint>
#include "I18n.h"

#define FLAG_REVISED_THIEF 1 << 0
#define FLAG_STEALING_DISALLOWED 1 << 1
#define FLAG_REVISED_RANDOM_BALANCING 1 << 2
#define FLAG_SAME_RACES_PER_TEAM 1 << 3
#define FLAG_WAR_MACHINE_DISABLED 1 << 4
#define FLAG_OPTIMIZED_RANDOM_START_LOCATIONS 1 << 5
#define FLAG_INCREASED_RANDOM_RESOURCE_YIELD 1 << 6
#define FLAG_INCREASED_TRANSPORT_LIMIT 1 << 7

class PatchGameSettings
{
private:
    uint64_t m_bitmask;

public:
    PatchGameSettings(uint64_t bitmask = 0): m_bitmask(bitmask) {
    }

    friend bool operator== (PatchGameSettings& lhs, PatchGameSettings& rhs) {
        return lhs.getPatchLevel() == rhs.getPatchLevel();
    }

    friend bool operator!= (PatchGameSettings& lhs, PatchGameSettings& rhs) {
        return lhs.getPatchLevel() != rhs.getPatchLevel();
    }

    PatchGameSettings& operator= (PatchGameSettings& rhs) {
        setPatchLevel(rhs.getPatchLevel());

        return *this;
    }

    std::string GetDescription(I18n* i18n, bool isRandomGame, bool showOptimizePositions) {
        std::string description;

        bool isFirst = true;
        auto Item = [&](const char* label) {
          if (!isFirst) {
              description.append(", ");
          }
          isFirst = false;

          description.append(label);
        };

        if (isRandomGame) {
            if (useRevisedRandomBalancing()) {
                Item(i18n->__("new_balancing", "New Balancing").c_str());
            }
            if (shouldIncreaseRandomResourceYield()) {
                Item(i18n->__("more_resources", "More Resources").c_str());
            }
            if (showOptimizePositions && shouldOptimizeRandomStartLocations()) {
                Item(i18n->__("optimized_locations", "Optimized Locations").c_str());
            }
        }
        if (useRevisedThief()) {
            Item(i18n->__("new_thieves", "New Thieves").c_str());

            if (isStealingDisallowed()) {
                Item(i18n->__("no_stealing", "No Stealing").c_str());
            } else {
                Item(i18n->__("stealing_on", "Stealing On").c_str());
            }
        }
        if (useSameRacesPerTeam()) {
            Item(i18n->__("same_random_races_per_team", "Same Random Races per Team").c_str());
        }
        if (isWarMachineDisabled()) {
            Item(i18n->__("wm_off", "WM Off").c_str());
        }
        if (useIncreasedTransportLimit()) {
            Item(i18n->__("increased_transport_limit", "Increased Transport Limit").c_str());
        }

        if (description.empty()) {
            Item(i18n->__("classic", "Classic").c_str());
        }

        return description;
    }

    void setUseRevisedThief(bool enabled) {
        setFlag(enabled, FLAG_REVISED_THIEF);
    }

    void setUseRevisedRandomBalancing(bool enabled) {
        setFlag(enabled, FLAG_REVISED_RANDOM_BALANCING);
    }

    void setOptimizeRandomStartLocations(bool enabled) {
        setFlag(enabled, FLAG_OPTIMIZED_RANDOM_START_LOCATIONS);
    }

    void setIncreaseTransportLimit(bool enabled) {
        setFlag(enabled, FLAG_INCREASED_TRANSPORT_LIMIT);
    }

    void setIncreaseRandomResourceYields(bool enabled) {
        setFlag(enabled, FLAG_INCREASED_RANDOM_RESOURCE_YIELD);
    }

    void setStealingDisabled(bool enabled) {
        setFlag(enabled, FLAG_STEALING_DISALLOWED);

        if (enabled) {
            setFlag(true, FLAG_REVISED_THIEF);
        }
    }

    void setUseSameRacesPerTeam(bool enabled) {
        setFlag(enabled, FLAG_SAME_RACES_PER_TEAM);
    }

    void setWarMachineDisabled(bool enabled) {
        setFlag(enabled, FLAG_WAR_MACHINE_DISABLED);
    }

    bool useIncreasedTransportLimit() {
        return hasFlag(FLAG_INCREASED_TRANSPORT_LIMIT);
    }

    bool useRevisedThief() {
        return hasFlag(FLAG_REVISED_THIEF);
    }

    bool isStealingDisallowed() {
        return hasFlag(FLAG_STEALING_DISALLOWED);
    }

    bool useRevisedRandomBalancing() {
        return hasFlag(FLAG_REVISED_RANDOM_BALANCING);
    }

    bool shouldOptimizeRandomStartLocations() {
        return hasFlag(FLAG_OPTIMIZED_RANDOM_START_LOCATIONS);
    }

    bool shouldIncreaseRandomResourceYield() {
        return hasFlag(FLAG_INCREASED_RANDOM_RESOURCE_YIELD);
    }

    bool useSameRacesPerTeam() {
        return hasFlag(FLAG_SAME_RACES_PER_TEAM);
    }

    bool isVanilla() {
        return m_bitmask == 0;
    }

    bool isWarMachineDisabled() {
        return hasFlag(FLAG_WAR_MACHINE_DISABLED);
    }

    void setPatchLevel(uint64_t newPatchLevel) {
        m_bitmask = newPatchLevel;
    }

    uint64_t getPatchLevel() {
        return m_bitmask;
    }

    void setFlag(bool enabled, uint64_t flag) {
        if (enabled) {
            m_bitmask |= flag;
        } else {
            m_bitmask &= ~flag;
        }
    }

    bool hasFlag(uint64_t flag) {
        return (m_bitmask & flag) == flag;
    }
};


#endif // SIEDLER3_FIXES_PATCHGAMESETTINGS_H
