#ifndef SIEDLER3_FIXES_AUTOSAVEPATCH_H
#define SIEDLER3_FIXES_AUTOSAVEPATCH_H

#include <random>
#include "Patch.h"
#include "S3EventDispatcher.h"
#include "s3.h"
#include <hacklib/Patch.h>
#include "I18n.h"

class AutoSavePatch : public Patch, public BeforeGameLoopListener, public AfterGameInitListener, public SaveListener
{
public:
    AutoSavePatch(I18n* i18n, bool *isVanillaVersion, bool enabled);
    void enable();
    void disable();
    void OnBeforeGameLoop(logic_thread* logicThread);
    void OnAfterGameInit(logic_thread* logicThread);
    void OnSave(logic_thread* logicThread, void* saveFile);

private:
    I18n* m_i18n;
    bool* m_isVanillaVersion;
    bool m_enabled;
    int nextSaveOnTick = 0;
    int lastKnownSaveOnTick = 0;
    bool isActive = false;

    std::uniform_int_distribution<> random_delay;
    std::mt19937 gen;

    void updateNextSaveTick(logic_thread* logicThread, int baseTick = 0);
};

#endif // SIEDLER3_FIXES_AUTOSAVEPATCH_H
