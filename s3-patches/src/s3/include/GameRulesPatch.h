#ifndef SIEDLER3_FIXES_GAMERULESPATCH_H
#define SIEDLER3_FIXES_GAMERULESPATCH_H

#include "S3EventDispatcher.h"
#include "Patch.h"
#include "PatchGameSettings.h"
#include "GameSettingsManager.h"
#include "DetourHooker.h"
#include "FunctionHook.h"

class GameRulesPatch : public Patch, public BeforeGameLoopListener, public AfterGameInitListener
{
private:
    GameSettingsManager* m_gameSettingsManager;
    FunctionHook m_processLocalMessageHook;
    PatchGameSettings m_gameSettings;
    DetourHooker m_hooker;

public:
    GameRulesPatch(GameSettingsManager* gameSettingsManager): m_gameSettingsManager(gameSettingsManager) {
    }

    GameSettingsManager* GetGameSettingsManager() { return m_gameSettingsManager; }
    void enable();
    void disable();
    void OnBeforeGameLoop(logic_thread* logicThread);
    void OnAfterProcessLocalMessages(logic_thread* logicThread);
    void OnAfterGameInit(logic_thread* logicThread);
};


#endif // SIEDLER3_FIXES_GAMERULESPATCH_H
