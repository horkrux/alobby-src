#ifndef SIEDLER3_FIXES_RANDOMBALANCINGPATCH_H
#define SIEDLER3_FIXES_RANDOMBALANCINGPATCH_H

#include <hacklib/Patch.h>
#include "Patch.h"
#include "FunctionHook.h"
#include "S3EventDispatcher.h"

/**
 * Makes some balancing changes for random games with the goal to provide a more level playing field for all races.
 *
 * All Races:
 * - builders for small towers adjusted to ensure about equal building time for all races (Amazons=5, others=4)
 * - waterworks settler behavior changed to maintain work location if possible (allows more good building spots, helps Egypt in particular)
 * - thieves are not allowed to steal goods that are within range of towers manned with bows
 *
 * Romans:
 * - mana points for soldier upgrades increased slightly (L3 after 120 mana points, up from 110 previously)
 *
 * Asians:
 * - additional resources added (f.e. in high goods: 8 planks, 2 axes, 1 saw, 8 shovels)
 * - mana points unchanged (L3 still after 110 mana points)
 *
 * Egypts:
 * - additional resources added (f.e. in high goods: 8 shovels)
 * - mana points for soldier upgrades reduced (L3 after 65 mana points)
 *
 * Amazons:
 * - additional resources added (f.e. in high goods: 8 planks, 2 axes, 1 saw)
 * - mana points for soldier upgrades reduced modestly (L3 after 90 mana points)
 *
 * Building material addition:
 * ---------------------------
 * Asians and Amazons are early on often disadvantaged when towering against Romans or Egypt. The additional building
 * material allows those races to engage in towering duels and secure mountain without sacrificing speed too much.
 *
 * Shovel additions:
 * -----------------
 * Asians and Egypt generally do not have the same range in build spots as Romans or Amazons. As a result they have
 * to build on spots that take longer to flatten. The shovel addition offsets this disadvantage.
 *
 * Romans in particular are very quick at clearing a map of unwanted objects like stones and trees to allow for
 * additional build options plus they have very compact buildings that can be placed in many spots.
 *
 * Amazons have access to many settlers early on due to quick large houses which in turn also provides plenty of build
 * options. Besides Amazon's early buildings are very small and can be placed in a large number of spots.
 *
 * Mana point changes:
 * -------------------
 * Mana points are currently equal for all races: 10, 20, 30, 50 (L3), 60, 30 mana points for the different upgrades.
 *
 * Asians and Romans have very similar timings for reaching L3 given the guarantee of swamps close to the starting
 * location. Only Egypt and Amazons have significantly later L3 timings. It would be possible to delay L3 for the first
 * two by increasing their mana points. This could result in a more diverse mid-game as L2 would have to be produced more,
 * but it would be a change to which Roman players would have to get used to and adapt their strategies.
 *
 * Instead, the current change therefore reduces the mana points for Egypt/Amazons in order to not alter the
 * play-style of Romans. Egypt is reduced a bit more than Amazons.
 *
 * This change does not affect spells.
 */
class RandomBalancingPatch : public Patch, public BuildingCreatedListener, public AfterGameInitListener
{
public:
    void enable();
    void disable();

    void OnBuildingCreated(game_data* gameData, unsigned int newBuildingIndex);
    void OnAfterGameInit(logic_thread* logicThread);

private:
    FunctionHook initStartLocationHook, levelUpManaPointsHook;
    hl::Patch m_removePickAx1Patch, m_removePickAx2Patch;
    void applyPickAxPatch();
};


#endif // SIEDLER3_FIXES_RANDOMBALANCINGPATCH_H
