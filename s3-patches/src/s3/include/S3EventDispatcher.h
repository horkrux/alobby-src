#ifndef SIEDLER3_FIXES_S3EVENTDISPATCHER_H
#define SIEDLER3_FIXES_S3EVENTDISPATCHER_H

#include <vector>
#include <hacklib/Hooker.h>
#include "FunctionHook.h"
#include "s3.h"
#include "s3_listeners.h"
#include <debug_console.h>

class S3EventDispatcher
{
private:
    FunctionHook gameInitHook, logicLoopHook, drawHook, updateDrawDataHook, menuDrawLoopHook, loadSaveHook, createBuildingHook, initBuildingHook;
    hl::Hooker hooker;
    std::vector<const hl::IHook*> activeHooks;
    std::vector<BeforeGameLoopListener*> beforeGameLoopListeners;
    std::vector<AfterGameInitListener*> afterGameInitListeners;
    std::vector<AfterDrawListener*> afterDrawListeners;
    std::vector<BeforeUpdateDrawDataListener*> beforeUpdateDrawDataListener;
    std::vector<BeforeMenuDrawLoopListener*> beforeMenuDrawLoopListener;
    std::vector<AfterMenuDrawLoopListener*> afterMenuDrawLoopListener;
    std::vector<GameOverListener*> gameOverListener;
    std::vector<SaveListener*> saveListener;
    std::vector<LoadSaveListener*> loadSaveListener;
    std::vector<LoadSaveStage1Listener*> loadSaveStage1Listener;
    std::vector<BuildingCreatedListener*> buildingCreatedListener;
    std::vector<GameLeaveListener*> gameQuitListener;
    std::vector<GuiListener*> guiListener;
    std::vector<WindowMessageListener*> wmListener;

public:
    S3EventDispatcher();

    template <typename T>
    void AddListener(T* listener) {
        auto beforeGameLoop = dynamic_cast<BeforeGameLoopListener*>(listener);
        if (beforeGameLoop != nullptr) {
            beforeGameLoopListeners.push_back(beforeGameLoop);
        }

        auto gameInit = dynamic_cast<AfterGameInitListener*>(listener);
        if (gameInit != nullptr) {
            afterGameInitListeners.push_back(gameInit);
        }

        auto afterDraw = dynamic_cast<AfterDrawListener*>(listener);
        if (afterDraw != nullptr) {
            afterDrawListeners.push_back(afterDraw);
        }

        auto beforeUpdateDrawData = dynamic_cast<BeforeUpdateDrawDataListener*>(listener);
        if (beforeUpdateDrawData != nullptr) {
            beforeUpdateDrawDataListener.push_back(beforeUpdateDrawData);
        }

        auto beforeMenuDrawLoop = dynamic_cast<BeforeMenuDrawLoopListener*>(listener);
        if (beforeMenuDrawLoop != nullptr) {
            beforeMenuDrawLoopListener.push_back(beforeMenuDrawLoop);
        }

        auto afterMenuDrawLoop = dynamic_cast<AfterMenuDrawLoopListener*>(listener);
        if (afterMenuDrawLoop != nullptr) {
            afterMenuDrawLoopListener.push_back(afterMenuDrawLoop);
        }

        auto gameOver = dynamic_cast<GameOverListener*>(listener);
        if (gameOver != nullptr) {
            gameOverListener.push_back(gameOver);
        }

        auto save = dynamic_cast<SaveListener*>(listener);
        if (save != nullptr) {
            saveListener.push_back(save);
        }

        auto loadSave = dynamic_cast<LoadSaveListener*>(listener);
        if (loadSave != nullptr) {
            loadSaveListener.push_back(loadSave);
        }

        auto loadSaveStage1 = dynamic_cast<LoadSaveStage1Listener*>(listener);
        if (loadSaveStage1 != nullptr) {
            loadSaveStage1Listener.push_back(loadSaveStage1);
        }

        auto buildingCreated = dynamic_cast<BuildingCreatedListener*>(listener);
        if (buildingCreated != nullptr) {
            buildingCreatedListener.push_back(buildingCreated);
        }

        auto gameQuit = dynamic_cast<GameLeaveListener*>(listener);
        if (gameQuit != nullptr) {
            gameQuitListener.push_back(gameQuit);
        }

        auto gui = dynamic_cast<GuiListener*>(listener);
        if (gui != nullptr) {
            guiListener.push_back(gui);
        }

        auto wm = dynamic_cast<WindowMessageListener*>(listener);
        if (wm != nullptr) {
            wmListener.push_back(wm);
        }
    }

    template <typename T>
    void RemoveListener(T* listener) {
        auto beforeGameLoop = dynamic_cast<BeforeGameLoopListener*>(listener);
        if (beforeGameLoop != nullptr) {
            RemoveIfExists(&beforeGameLoopListeners, beforeGameLoop);
        }

        auto gameInit = dynamic_cast<AfterGameInitListener*>(listener);
        if (gameInit != nullptr) {
            RemoveIfExists(&afterGameInitListeners, gameInit);
        }

        auto afterDraw = dynamic_cast<AfterDrawListener*>(listener);
        if (afterDraw != nullptr) {
            RemoveIfExists(&afterDrawListeners, afterDraw);
        }

        auto beforeUpdateDrawData = dynamic_cast<BeforeUpdateDrawDataListener*>(listener);
        if (beforeUpdateDrawData != nullptr) {
            RemoveIfExists(&beforeUpdateDrawDataListener, beforeUpdateDrawData);
        }

        auto beforeMenuDrawLoop = dynamic_cast<BeforeMenuDrawLoopListener*>(listener);
        if (beforeMenuDrawLoop != nullptr) {
            RemoveIfExists(&beforeMenuDrawLoopListener, beforeMenuDrawLoop);
        }

        auto afterMenuDrawLoop = dynamic_cast<AfterMenuDrawLoopListener*>(listener);
        if (afterMenuDrawLoop != nullptr) {
            RemoveIfExists(&afterMenuDrawLoopListener, afterMenuDrawLoop);
        }

        auto gameOver = dynamic_cast<GameOverListener*>(listener);
        if (gameOver != nullptr) {
            RemoveIfExists(&gameOverListener, gameOver);
        }

        auto save = dynamic_cast<SaveListener*>(listener);
        if (save != nullptr) {
            RemoveIfExists(&saveListener, save);
        }

        auto loadSave = dynamic_cast<LoadSaveListener*>(listener);
        if (loadSave != nullptr) {
            RemoveIfExists(&loadSaveListener, loadSave);
        }

        auto loadSaveStage1 = dynamic_cast<LoadSaveStage1Listener*>(listener);
        if (loadSaveStage1 != nullptr) {
            RemoveIfExists(&loadSaveStage1Listener, loadSaveStage1);
        }

        auto buildingCreated = dynamic_cast<BuildingCreatedListener*>(listener);
        if (buildingCreated != nullptr) {
            RemoveIfExists(&buildingCreatedListener, buildingCreated);
        }

        auto gameQuit = dynamic_cast<GameLeaveListener*>(listener);
        if (gameQuit != nullptr) {
            RemoveIfExists(&gameQuitListener, gameQuit);
        }

        auto gui = dynamic_cast<GuiListener*>(listener);
        if (gui != nullptr) {
            RemoveIfExists(&guiListener, gui);
        }

        auto wm = dynamic_cast<WindowMessageListener*>(listener);
        if (wm != nullptr) {
            RemoveIfExists(&wmListener, wm);
        }
    }

    std::vector<BeforeGameLoopListener*> GetBeforeGameLoopListeners();
    std::vector<AfterGameInitListener*> GetAfterGameInitListener();
    std::vector<BeforeUpdateDrawDataListener*> GetBeforeUpdateDrawDataListener();
    std::vector<AfterDrawListener*> GetAfterDrawListener();
    std::vector<BeforeMenuDrawLoopListener*> GetBeforeMenuDrawLoopListener();
    std::vector<AfterMenuDrawLoopListener*> GetAfterMenuDrawLoopListener();
    std::vector<GameOverListener*> GetGameOverListener();
    std::vector<SaveListener*> GetSaveListener();
    std::vector<LoadSaveListener*> GetLoadSaveListener();
    std::vector<LoadSaveStage1Listener*> GetLoadSaveStage1Listener();
    std::vector<BuildingCreatedListener*> GetBuildingCreatedListener();
    std::vector<GameLeaveListener*> GetGameQuitListener();
    std::vector<GuiListener*> GetGuiListener();
    std::vector<WindowMessageListener*> GetWmListener();
private:
    template<typename T>
    void RemoveIfExists(std::vector<T*>* listeners, T* listener) {
        auto index = std::find(listeners->begin(), listeners->end(), listener);
        if (index != listeners->end()) {
            listeners->erase(index);
        }
    }
};


#endif // SIEDLER3_FIXES_S3EVENTDISPATCHER_H
