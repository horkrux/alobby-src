#ifndef SIEDLER3_FIXES_DSOUNDPATCH_H
#define SIEDLER3_FIXES_DSOUNDPATCH_H

#include "Patch.h"
#include "FunctionHook.h"
#include "dll_config.h"
#include "s3_listeners.h"

class DsoundPatch : public Patch, public BeforeUpdateDrawDataListener
{
private:
    FunctionHook m_resultHandlingHook, m_playSoundEffectHook, m_getSoundEffectHook;
    Config* m_config;

public:
    DsoundPatch(Config* config): m_config(config) { }
    void enable() override;
    void disable() override;
    void OnBeforeUpdateDrawData(logic_thread* logicThread) override;
};


#endif // SIEDLER3_FIXES_DSOUNDPATCH_H
