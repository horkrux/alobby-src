#ifndef SIEDLER3_FIXES_JOINGAMETASK_H
#define SIEDLER3_FIXES_JOINGAMETASK_H

#include <hacklib/Hooker.h>
#include <chrono>
#include "s3.h"
#include "S3EventDispatcher.h"
#include "I18n.h"

using namespace std::chrono;

class JoinGameTask : public BeforeMenuDrawLoopListener, public GuiListener {
public:
    std::string playerNickname, ipAddress;
    bool isCompleted = false;
    time_point<system_clock> startTime;
    I18n* m_i18n;

    JoinGameTask(I18n* i18n, std::string nickname, std::string hostIp) : m_i18n(i18n), playerNickname(nickname), ipAddress(hostIp) {
        install();
        startTime = std::chrono::system_clock::now();
    };
    ~JoinGameTask();

    void OnBeforeMenuDrawLoop(lp_menu_draw_thread* menuDrawThread);
    void OnRenderFrame(RenderContext* context);

private:
    FunctionHook m_selectSessionHook;
    std::chrono::time_point<system_clock> m_lastActionAt = std::chrono::system_clock::now();
    int m_lastAction = 0;

    void install();
    void onCompleted();
    bool acquireActionPermission(int action);
};


#endif // SIEDLER3_FIXES_JOINGAMETASK_H
