#ifndef SIEDLER3_FIXES_CONSTANTS_H
#define SIEDLER3_FIXES_CONSTANTS_H

#include "stdafx.h"

#define GRAPHIC_MODE_DIRECTX 0
#define GRAPHIC_MODE_OPENGL 1
#define GRAPHIC_MODE_GDI 2
#define GRAPHIC_MODE_DISABLE 3

namespace Constants
{
static const uintptr_t S3_EXE = (uintptr_t)GetModuleHandle(NULL);

#ifndef NDEBUG
static const char* DLL_PATH = "s3d.dll";
static const char* TMP_DLL_PATH = "s3d.dll.tmp";
static const wchar_t* REMOTE_PATH = L"/s3d.dll";
#else
static const char* DLL_PATH = "s3.dll";
static const char* TMP_DLL_PATH = "s3.dll.tmp";
static const wchar_t* REMOTE_PATH = L"/s3.v2.dll";
#endif
}

#endif // SIEDLER3_FIXES_CONSTANTS_H
