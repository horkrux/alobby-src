#ifndef SIEDLER3_FIXES_STATSSCREENPATCH_H
#define SIEDLER3_FIXES_STATSSCREENPATCH_H

#include "Patch.h"
#include "S3EventDispatcher.h"
#include "IpcServer.h"

class StatsScreenPatch: public Patch,
                        public AfterMenuDrawLoopListener
{
public:
    StatsScreenPatch(IpcServer* ipcServer, std::string screenDirectory): m_ipcServer(ipcServer), m_screenDirectory(screenDirectory) { }
    void enable();
    void disable();
    void OnAfterMenuDrawLoop(lp_menu_draw_thread* menuDrawThread);

private:
    std::string m_screenDirectory;
    IpcServer* m_ipcServer;
    int m_lastDialogId = 0;
    ULONG_PTR m_gdiPlusToken;
};

#endif // SIEDLER3_FIXES_STATSSCREENPATCH_H
