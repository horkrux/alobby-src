#ifndef SIEDLER3_FIXES_PATCH_H
#define SIEDLER3_FIXES_PATCH_H

class Patch
{
public:
    virtual void enable() = 0;
    virtual void disable() = 0;
};


#endif // SIEDLER3_FIXES_PATCH_H
