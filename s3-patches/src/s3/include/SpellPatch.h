#ifndef SIEDLER3_FIXES_SPELLPATCH_H
#define SIEDLER3_FIXES_SPELLPATCH_H

#include "Patch.h"
#include <hacklib/Patch.h>

class SpellPatch: public Patch {
private:
    hl::Patch m_convertPatch, m_convertDelayPatch, m_fishCostSpell, m_attackerStrengthenCost, m_shieldCost;

public:
    void enable() override;
    void disable() override;

private:
    void applyConvertPatch();
};


#endif // SIEDLER3_FIXES_SPELLPATCH_H
