#ifndef SIEDLER3_FIXES_CONFIG_H
#define SIEDLER3_FIXES_CONFIG_H

#include <hacklib/MessageBox.h>
#include "debug_console.h"
#include "PatchGameSettings.h"
#include <versionhelpers.h>

#define REGISTRY_KEY "\\Software\\S3 Community Edition\\Game"
#define REGISTRY_VALUE_USAGE_FLAGS "UsageFlags"
#define REGISTRY_VALUE_ENABLE_GUI "EnableGui"
#define REGISTRY_VALUE_FORCE_DISPLAY_RESOLUTION "ForceDisplayResolution"
#define REGISTRY_VALUE_FORCE_WINDOW_MODE "ForceWindowMode"
#define REGISTRY_VALUE_DEFAULT_PATCH_SETTINGS "DefaultPatchSettings"
#define REGISTRY_VALUE_SETTINGS "Settings"
#define REGISTRY_VALUE_SETTINGS_GOOD_CHANGES "GoodChanges"
#define REGISTRY_VALUE_SETTINGS_SETTLER_CHANGES "SettlerChanges"

#define USAGE_FLAG_INGAME_CHAT 1

#define MAX_NICKNAME_LEN 19
#define MAX_IP_ADDR_LEN 15
#define MAX_MAP_NAME_LEN 99
#define MAX_PIPE_NAME_LEN 19

#define MAP_CATEGORY_RANDOM 0
#define MAP_CATEGORY_SINGLE_PLAYER 1
#define MAP_CATEGORY_MULTI_PLAYER 2
#define MAP_CATEGORY_USER 3
#define MAP_CATEGORY_SAVE 4

#define MAP_SIZE_384 0
#define MAP_SIZE_448 1
#define MAP_SIZE_512 2
#define MAP_SIZE_576 3
#define MAP_SIZE_640 4
#define MAP_SIZE_704 5
#define MAP_SIZE_768 6

#define MIRROR_TYPE_NONE 0
#define MIRROR_TYPE_SHORT_AXIS 1
#define MIRROR_TYPE_LONG_AXIS 2
#define MIRROR_TYPE_BOTH_AXES 3

#define GOODS_SETTING_MAP_DEFAULT 0
#define GOODS_SETTING_LOW 1
#define GOODS_SETTING_MEDIUM 2
#define GOODS_SETTING_HIGH 3

typedef struct dll_config_join_game {
    bool enabled;
    char nickname[MAX_NICKNAME_LEN + 1];
    char ipAddr[MAX_IP_ADDR_LEN + 1];
} dll_config_join_game;

typedef struct dll_config_host_game {
    bool enabled;
    unsigned char mapCategory;
    unsigned char mapSize;
    unsigned char mirrorType;
    bool useRandomPositions;
    unsigned char goodsSetting;
    char nickname[MAX_NICKNAME_LEN + 1];
    char mapName[MAX_MAP_NAME_LEN + 1];
} dll_config_host_game;

typedef struct dll_config {
    short graphicMode;
    unsigned short displayWidth;
    unsigned short displayHeight;
    bool applyTextureFilter;
    bool reduceMaxFps;
    char pipeName[MAX_PIPE_NAME_LEN + 1];
    dll_config_join_game joinGameData;
    dll_config_host_game hostGameData;
    bool autoSave;
    bool balancingPatchesDisabled;
    bool classicOnly;
    bool improvedChat;
    char screenDirectory[MAX_PATH];
    bool disableGui;
    bool enableGui;
    bool errorTracking;
    bool forceDisplayResolution;
    bool forceWindowMode;
} dll_config;

typedef struct registry_settings {
    bool stopRecruiting;
    bool changeWeaponProduction;
    int swords;
    int bows;
    int spears;
    int fightingSoundVolume;
    bool adjustItemStorage;
    bool showGameTime;
    bool enableTeamChat;
} registry_settings;

typedef struct settler_distribution_change {
    char carrier;
    char digger;
    char builder;
    char selectedRace;
    char selectedGoods;
    char selectedMap;
} settler_distribution_change;

typedef struct good_distribution_change {
    char settingId;
    char settingValue;
    char selectedRace;
    char selectedGoods;
    char selectedMap;
} good_distribution_change;

class Config {
public:
    Config() {
        m_dllConfig.graphicMode = 0;
        m_dllConfig.applyTextureFilter = true;
        m_dllConfig.balancingPatchesDisabled = false;
        m_dllConfig.reduceMaxFps = false;
        m_dllConfig.joinGameData.enabled = false;

        m_registrySettings.showGameTime = true;
        m_registrySettings.fightingSoundVolume = 70;
        m_registrySettings.stopRecruiting = false;
        m_registrySettings.changeWeaponProduction = false;
        m_registrySettings.swords = 0;
        m_registrySettings.bows = 0;
        m_registrySettings.spears = 0;
        m_registrySettings.adjustItemStorage = false;
        m_registrySettings.enableTeamChat = true;

        auto result = RegOpenKeyEx(HKEY_CURRENT_USER, REGISTRY_KEY, 0, KEY_ALL_ACCESS, &m_registryKey);
        if (result != ERROR_SUCCESS) {
            if (result == ERROR_FILE_NOT_FOUND) {
                result = RegCreateKeyExA(HKEY_CURRENT_USER, REGISTRY_KEY, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL,
                                         &m_registryKey, NULL);
                if (result != ERROR_SUCCESS) {
                    dprintf("failed to create registry key: %lu\n", result);
                }
            } else {
                dprintf("failed to open registry key: %lu\n", result);
            }
        } else {
            dprintf("registry key opened\n");
        }
    };

    int getGraphicMode() { return m_dllConfig.graphicMode; }

    unsigned short getDisplayWidth() { return m_dllConfig.displayWidth; }

    unsigned short getDisplayHeight() { return m_dllConfig.displayHeight; }

    bool shouldApplyTextureFilter() { return m_dllConfig.applyTextureFilter; }

    bool shouldForceDisplayResolution() { return m_dllConfig.forceDisplayResolution; }

    bool shouldForceWindowMode() { return m_dllConfig.forceWindowMode; }

    bool shouldReduceMaxFps() { return m_dllConfig.reduceMaxFps; }

    bool shouldJoinGame() { return m_dllConfig.joinGameData.enabled; }

    bool shouldHostGame() { return m_dllConfig.hostGameData.enabled; }

    bool shouldShowInGameTime() { return m_registrySettings.showGameTime; }

    void setShowInGameTime(bool enabled) { m_registrySettings.showGameTime = enabled; }

    bool shouldStopRecruiting() { return m_registrySettings.stopRecruiting; }

    void setStopRecruiting(bool enabled) { m_registrySettings.stopRecruiting = enabled; }

    bool shouldChangeWeaponProduction() { return m_registrySettings.changeWeaponProduction; }

    void setChangeWeaponProduction(bool enabled) { m_registrySettings.changeWeaponProduction = enabled; }

    unsigned int getWeaponProductionSwords() { return m_registrySettings.swords; }

    void setWeaponProductionSwords(int value) { m_registrySettings.swords = value; }

    unsigned int getWeaponProductionBows() { return m_registrySettings.bows; }

    void setWeaponProductionBows(int value) { m_registrySettings.bows = value; }

    unsigned int getWeaponProductionSpears() { return m_registrySettings.spears; }

    void setWeaponProductionSpears(int value) { m_registrySettings.spears = value; }

    bool shouldAdjustItemStorage() { return m_registrySettings.adjustItemStorage; }

    void setAdjustItemStorage(bool enabled) { m_registrySettings.adjustItemStorage = enabled; }

    bool shouldEnableTeamChat() { return m_registrySettings.enableTeamChat; }

    void setEnabledTeamChat(bool enabled) { m_registrySettings.enableTeamChat = enabled; }

    std::vector<good_distribution_change> getGoodDistributionChanges() { return m_goodDistributionChanges; }

    void setGoodDistributionChanges(std::vector<good_distribution_change> changes) { m_goodDistributionChanges = std::move(changes); }

    std::vector<settler_distribution_change> getSettlerDistributionChanges() { return m_settlerDistributionChanges; }

    void setSettlerDistributionChanges(std::vector<settler_distribution_change> changes) { m_settlerDistributionChanges = std::move(changes); }

    int getFightingSoundVolume() { return m_registrySettings.fightingSoundVolume; }

    void setFightingSoundVolume(int volume) { m_registrySettings.fightingSoundVolume = volume; }

    std::string getIpcPipeName() { return std::string(m_dllConfig.pipeName); }

    bool isClassicOnly() { return m_dllConfig.classicOnly; }

    bool areBalancingPatchesDisabled() { return m_dllConfig.balancingPatchesDisabled; }

    bool isAutoSaveOn() { return m_dllConfig.autoSave; }

    bool useImprovedChat() { return m_dllConfig.improvedChat; }

    bool hasUsageFlag(DWORD64 flag) { return (m_usageFlags & flag) == flag; }

    void setUsageFlag(DWORD64 flag) {
        if (hasUsageFlag(flag)) {
            // Nothing to do, no need to involve the registry.
            return;
        }

        m_usageFlags |= flag;
        writeRegValue<DWORD64, REG_QWORD>(REGISTRY_VALUE_USAGE_FLAGS, m_usageFlags);
    }

    dll_config_join_game* getJoinGameData() { return &m_dllConfig.joinGameData; }

    dll_config_host_game* getHostGameData() { return &m_dllConfig.hostGameData; }

    bool isIpcPipeRequested() { return m_isPipeRequested; }

    void init(dll_config* ptr) {
        memcpy(&m_dllConfig, ptr, sizeof(dll_config));
        if (m_dllConfig.hostGameData.enabled) {
            dprintf("Hosting game is enabled as per dll config (enabled=%d)\n", m_dllConfig.hostGameData.enabled);
        }

        if (strlen(m_dllConfig.pipeName) <= 0) {
            dprintf("Using default pipe name\n");
            strcpy(m_dllConfig.pipeName, "s3-alobby");
            m_isPipeRequested = false;
        } else {
            m_isPipeRequested = true;
        }

        if (isErrorTrackingEnabled()) {
            dprintf("Error Reporting: enabled\n");
        } else {
            dprintf("Error Reporting: disabled\n");
        }

        // :FIXME: If the value is set and is 0, then and only then we must also set the value to false...
        if (readRegValue<DWORD, REG_DWORD>(REGISTRY_VALUE_ENABLE_GUI, 0)) {
            m_dllConfig.enableGui = true;
        }

        // :FIXME: If the value is set and is 0, then and only then we must also set the value to false...
        if (readRegValue<DWORD, REG_DWORD>(REGISTRY_VALUE_FORCE_DISPLAY_RESOLUTION, 0)) {
            m_dllConfig.forceDisplayResolution = true;
        }

        // :FIXME: If the value is set and is 0, then and only then we must also set the value to false...
        if (readRegValue<DWORD, REG_DWORD>(REGISTRY_VALUE_FORCE_WINDOW_MODE, 0)) {
            m_dllConfig.forceWindowMode = true;
        }

        if (isGuiDisabled()) {
            dprintf("GUI: disabled\n");
        } else {
            if (!isGuiSupported() && !m_dllConfig.enableGui) {
                m_dllConfig.disableGui = true;
                dprintf("GUI: enabled, but not supported on current system -> disabled\n");
            } else {
                dprintf("GUI: enabled\n");
            }
        }

        m_usageFlags = readRegValue<DWORD64, REG_QWORD>(REGISTRY_VALUE_USAGE_FLAGS, 0);
        m_defaultSettings = readRegValue<DWORD64, REG_QWORD>(REGISTRY_VALUE_DEFAULT_PATCH_SETTINGS, 0);
        m_registrySettings = std::move(readRegValue<registry_settings, REG_BINARY>(REGISTRY_VALUE_SETTINGS, m_registrySettings));
        m_goodDistributionChanges = std::move(readArrayValue<good_distribution_change>(REGISTRY_VALUE_SETTINGS_GOOD_CHANGES));
        m_settlerDistributionChanges = std::move(readArrayValue<settler_distribution_change>(REGISTRY_VALUE_SETTINGS_SETTLER_CHANGES));
    }

    std::string getScreenDirectory() { return m_dllConfig.screenDirectory; }

    bool isGuiSupported() {
        auto isWine = GetProcAddress(GetModuleHandleA("ntdll.dll"), "wine_get_version") != 0;
        if (isWine) {
            dprintf("GUI: not supported on Wine\n");
            return false;
        }

        typedef void(WINAPI * RtlGetVersion_PROC)(OSVERSIONINFOEXW*);
        RtlGetVersion_PROC RtlGetVersion =
            reinterpret_cast<RtlGetVersion_PROC>(GetProcAddress(GetModuleHandleA("ntdll.dll"), "RtlGetVersion"));
        if (!RtlGetVersion) {
            dprintf("GUI: OS version could not be determined\n");
            return false;
        }

        // For a list of versions, see https://docs.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-osversioninfoexw#remarks
        OSVERSIONINFOEXW version;
        RtlGetVersion(&version);
        if (version.dwMajorVersion >= 10) { // Windows 10, or newer
            return true;
        }
        if (version.dwMajorVersion == 6 && version.dwMinorVersion >= 3) { // Windows 8.1
            return true;
        }

        dprintf("GUI: Current Windows OS version is not supported; current version: %lu.%lu\n", version.dwMajorVersion, version.dwMinorVersion);
        return false;
    }

    bool isGuiDisabled() { return m_dllConfig.disableGui; }

    bool isErrorTrackingEnabled() { return m_dllConfig.errorTracking; }

    PatchGameSettings getDefaultSettings() { return m_defaultSettings; }

    void persistSettings() {
        writeSettings();
    }

    void updateDefaultSettings(PatchGameSettings settings) {
        writeRegValue<DWORD64, REG_QWORD>(REGISTRY_VALUE_DEFAULT_PATCH_SETTINGS, settings.getPatchLevel());
    }

    ~Config() {
        if (m_registryKey) {
            RegCloseKey(m_registryKey);
            m_registryKey = nullptr;
        }
    }

private:
    dll_config m_dllConfig;
    DWORD64 m_usageFlags;
    HKEY m_registryKey;
    bool m_isPipeRequested = false;
    DWORD64 m_defaultSettings;
    registry_settings m_registrySettings;
    std::vector<settler_distribution_change> m_settlerDistributionChanges;
    std::vector<good_distribution_change> m_goodDistributionChanges;

    void writeSettings() {
        writeRegValue(REGISTRY_VALUE_SETTINGS, REG_BINARY, reinterpret_cast<BYTE*>(&m_registrySettings), sizeof(registry_settings));
        writeRegValue(REGISTRY_VALUE_SETTINGS_GOOD_CHANGES, REG_BINARY, reinterpret_cast<BYTE*>(m_goodDistributionChanges.data()),
                      sizeof(good_distribution_change) * m_goodDistributionChanges.size());
        writeRegValue(REGISTRY_VALUE_SETTINGS_SETTLER_CHANGES, REG_BINARY, reinterpret_cast<BYTE*>(m_settlerDistributionChanges.data()),
                      sizeof(settler_distribution_change) * m_settlerDistributionChanges.size());
    }

    template<typename EntryType>
    std::vector<EntryType> readArrayValue(const char* keyName) {
        std::vector<EntryType> entries;

        if (!m_registryKey) {
            dprintf("failed to retrieve value for %s as key is not open\n", keyName);
            return std::move(entries);
        }


        BYTE buf[2048];
        DWORD retType;

        DWORD cbData = sizeof(buf);
        auto result = RegQueryValueExA(m_registryKey, keyName, NULL, &retType, reinterpret_cast<LPBYTE>(buf), &cbData);
        if (result != ERROR_SUCCESS) {
            if (result == ERROR_FILE_NOT_FOUND) {
                dprintf("reg key %s does not exist, using default\n", keyName);
            } else {
                dprintf("failed to query value for %s: %lu\n", keyName, result);
            }
        } else {
            if (retType != REG_BINARY) {
                dprintf("key %s was not of expected type %lu, but got %lu\n", keyName, REG_BINARY, retType);
            } else {
                int numEntries = cbData / sizeof(EntryType);
                auto rawEntries = reinterpret_cast<EntryType*>(buf);
                for (int i=0;i<numEntries; i++) {
                    entries.push_back(rawEntries[i]);
                }

                dprintf("Loaded %d entries from registry\n", numEntries);
            }
        }

        return std::move(entries);
    }

    template <typename ReturnType, DWORD ReturnTypeConstant>
    ReturnType readRegValue(const char* keyName, ReturnType defaultValue) {
        if (!m_registryKey) {
            dprintf("failed to retrieve value for %s as key is not open\n", keyName);
            return std::move(defaultValue);
        }

        DWORD retType;
        ReturnType ret;
        DWORD cbData = sizeof(ReturnType);
        auto result = RegQueryValueExA(m_registryKey, keyName, NULL, &retType, reinterpret_cast<LPBYTE>(&ret), &cbData);
        if (result != ERROR_SUCCESS) {
            if (result == ERROR_FILE_NOT_FOUND) {
                dprintf("reg key %s does not exist, using default\n", keyName);
            } else {
                dprintf("failed to query value for %s: %lu\n", keyName, result);
            }

            return defaultValue;
        }

        if (retType != ReturnTypeConstant) {
            dprintf("key %s was not of expected type %lu, but got %lu\n", keyName, ReturnTypeConstant, retType);
            return defaultValue;
        }

        return ret;
    }

    void writeRegValue(const char* keyName, DWORD regValueType, const BYTE* lpData, DWORD dataSize) {
        if (!m_registryKey) {
            dprintf("failed to write value %s as key is not open\n", keyName);

            return;
        }

        auto result = RegSetValueExA(m_registryKey, keyName, 0, regValueType, lpData, dataSize);
        if (result != ERROR_SUCCESS) {
            dprintf("failed to write key value %s: %lu\n", keyName, result);
        } else {
            dprintf("wrote reg key %s: %ld (value-type: %lu, size: %d)\n", keyName, result, regValueType, dataSize);
        }
    }

    template <typename ValueType, DWORD ValueTypeConstant>
    void writeRegValue(const char* keyName, ValueType value) {
        writeRegValue(keyName, ValueTypeConstant, reinterpret_cast<BYTE*>(&value), sizeof(ValueType));
    }
};


#endif // SIEDLER3_FIXES_CONFIG_H
