#ifndef SIEDLER3_FIXES_SETUPSETTINGSPATCH_H
#define SIEDLER3_FIXES_SETUPSETTINGSPATCH_H

#include "Patch.h"
#include "S3EventDispatcher.h"
#include "FunctionHook.h"
#include "I18n.h"

class SetupSettingsPatch: public Patch, public AfterGameInitListener
{
    I18n* m_i18n;
    Config* m_config;

public:
    SetupSettingsPatch(I18n* i18n, Config* config): m_i18n(i18n), m_config(config) { }
    void enable() override;
    void disable() override;
    void OnAfterGameInit(logic_thread* logicThread) override;

private:
    void enableTeamChat(game_data* gameData);

#ifndef NDEBUG
    FunctionHook queueEventHook;
#endif
};


#endif // SIEDLER3_FIXES_SETUPSETTINGSPATCH_H
