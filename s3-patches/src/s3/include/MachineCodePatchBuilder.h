#ifndef SIEDLER3_FIXES_MACHINECODEPATCHBUILDER_H
#define SIEDLER3_FIXES_MACHINECODEPATCHBUILDER_H

#include <asmjit/x86.h>
#include <hacklib/Patch.h>

using namespace asmjit;

class MachineCodePatchBuilder {
    Environment env;
    CodeHolder code;
    uintptr_t  m_position;

public:
    x86::Assembler* a;

    MachineCodePatchBuilder(uintptr_t patchPosition): m_position(patchPosition) {
        env.setArch(Environment::kArchX86);
        code.init(env, patchPosition);

        a = new x86::Assembler(&code);
    }

    ~MachineCodePatchBuilder() {
        delete a;
    }

    void padNop(unsigned int targetSize) {
        while (code.textSection()->bufferSize() < targetSize) {
            a->nop();
        }
    }

    void apply(hl::Patch* patchHolder) {
        CodeBuffer& buf = code.textSection()->buffer();
        patchHolder->apply(m_position, (char*)buf.data(), buf.size());
    }
};


#endif // SIEDLER3_FIXES_MACHINECODEPATCHBUILDER_H
