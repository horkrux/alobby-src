#ifndef SIEDLER3_FIXES_CHATPATCH_H
#define SIEDLER3_FIXES_CHATPATCH_H

#include "Patch.h"
#include "FunctionHook.h"
#include <hacklib/Hooker.h>
#include <hacklib/Patch.h>
#include "s3.h"
#include "S3EventDispatcher.h"
#include "ObserverModePatch.h"
#include "dll_config.h"

class ChatPatch: public Patch, public BeforeUpdateDrawDataListener, public GameOverListener
{
public:
    ChatPatch(ObserverMode* observerMode, Config* config);

    void enable();
    void disable();

    void OnChatOpened(logic_thread* logicThread);
    void OnBeforeMessageSend(logic_thread* logicThread);

    void OnBeforeUpdateDrawData(logic_thread* logicThread);
    void OnGameOver(logic_thread* logicThread);
    void CheckChatModeChange(logic_thread* logicThread);
    bool IsToggleSupported();
private:
    FunctionHook addCharacterHook;
    hl::Hooker hooker;
    hl::Patch netChatMessageReceivedHook, displayLocalChatMessageHook;
    std::vector<const hl::IHook*> activeHooks;
    str_struct_wchar m_originalMessagePrefix;
    short m_currentChatMode;
    short m_nextChatMode;
    boolean isGameOver = false;
    ObserverMode* m_observerMode;
    Config* m_config;

    std::string getChatModePrefix(short chatMode);
    void hookIncomingMessage();
    void hookOutgoingMessage();
    void EnforceObserverTargets(logic_thread* logicThread);
};

#endif // SIEDLER3_FIXES_CHATPATCH_H
