#ifndef SIEDLER3_FIXES_DETOURHOOKER_H
#define SIEDLER3_FIXES_DETOURHOOKER_H

#include <hacklib/Hooker.h>

class DetourHooker
{
private:
    hl::Hooker m_hooker;
    std::vector<const hl::IHook*> m_activeHooks;

public:
    ~DetourHooker() {
        revert();
    }

    void hookDetour(uintptr_t location, int nextInstructionOffset, hl::Hooker::HookCallback_t cbHook) {
        m_activeHooks.push_back(m_hooker.hookDetour(location, nextInstructionOffset, cbHook));
    }

    void revert() {
        for (auto hook : m_activeHooks) {
            m_hooker.unhook(hook);
        }
        m_activeHooks.clear();
    }

    bool hasActiveHooks() {
        return ! m_activeHooks.empty();
    }
};


#endif // SIEDLER3_FIXES_DETOURHOOKER_H
