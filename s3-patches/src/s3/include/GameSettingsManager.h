#ifndef SIEDLER3_FIXES_GAMESETTINGSMANAGER_H
#define SIEDLER3_FIXES_GAMESETTINGSMANAGER_H

#include <algorithm>
#include "PatchGameSettings.h"
#include "PatchManager.h"
#include "s3_listeners.h"
#include "I18n.h"

class GameSettingsManager : public SaveListener, public LoadSaveStage1Listener
{
private:
    // We use an instance that always uses English as language.
    I18n m_englishI18n;
    I18n* m_i18n;
    PatchGameSettings m_gameSettings;
    PatchManager* m_patchManager;

public:
    GameSettingsManager(PatchManager* patchManager, I18n* i18n, PatchGameSettings defaultSettings): m_patchManager(patchManager), m_i18n(i18n) {
        Update(defaultSettings);
    }

    PatchGameSettings getGameSettings() {
        return m_gameSettings;
    }

    std::string getRulesDescription();
    void Update(PatchGameSettings newSettings);
    void OnLoadSave(logic_thread* logicThread, void* saveFile);
    void OnSave(logic_thread* logicThread, void* saveFile);
};


#endif // SIEDLER3_FIXES_GAMESETTINGSMANAGER_H
