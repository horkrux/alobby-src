#ifndef SIEDLER3_FIXES_OBSERVERMODEPATCH_H
#define SIEDLER3_FIXES_OBSERVERMODEPATCH_H

#include "Patch.h"
#include "S3EventDispatcher.h"

class ObserverMode
{
public:
    void enable();
    void disable();
    bool isActive();

private:
    bool m_isActive = false;
};

class ObserverModePatch: public Patch
{
public:
    ObserverModePatch(ObserverMode* observerMode);
    ~ObserverModePatch();
    void enable();
    void disable();

private:
    class ObserverListener : public BeforeGameLoopListener, public AfterGameInitListener {
    public:
        ObserverListener(ObserverMode* observerMode);
        void OnBeforeGameLoop(logic_thread* logicThread);
        void OnAfterGameInit(logic_thread* logicThread);
        void CheckIfActivate(logic_thread* logicThread);
    private:
        ObserverMode* m_observerMode;
        bool m_hasMoreThan2Teams;
    };

    ObserverListener listener;
};


#endif // SIEDLER3_FIXES_OBSERVERMODEPATCH_H
