#ifndef SIEDLER3_FIXES_DDRAW_PROXY_H
#define SIEDLER3_FIXES_DDRAW_PROXY_H

void install_ddraw_compat(void** coCreatePtr, char renderingMode, char shouldApplyTextureFilter, char reduceMaxFps, char forceDisplayResolution,
                          unsigned int displayWidth, unsigned int displayHeight, char forceWindowMode);
void show_d3d9_driver_warning();
void show_warning(char* message, unsigned int durationSeconds);

#endif
