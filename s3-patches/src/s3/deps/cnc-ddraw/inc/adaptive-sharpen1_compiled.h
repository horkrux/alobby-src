#if 0
//
// Generated by Microsoft (R) HLSL Shader Compiler 10.1
//
// Parameters:
//
//   float2 p1;
//   sampler2D s0;
//
//
// Registers:
//
//   Name         Reg   Size
//   ------------ ----- ----
//   p1           c1       1
//   s0           s0       1
//

    ps_3_0
    def c0, 0, -1, 1, -2
    def c2, 4, 0.0625, -2.4666667, 1.14999998
    def c3, 0.899999976, 0.266666681, 1.38, 0.920000017
    def c4, 0.230000004, 0, 0, 0
    dcl_texcoord v0.xy
    dcl_2d s0
    mov r0.xy, c1
    mad r1, r0.xyxy, c0.xyzy, v0.xyxy
    texld r2, r1, s0
    texld r1, r1.zwzw, s0
    mov_sat r1.xyz, r1
    mov_sat r2.xyz, r2
    add r0.zw, -c1.xyxy, v0.xyxy
    texld r3, r0.zwzw, s0
    mov_sat r3.xyz, r3
    add r4.xyz, r1, r3
    mad r5, r0.xyxy, c0.yzxz, v0.xyxy
    texld r6, r5, s0
    texld r5, r5.zwzw, s0
    mov_sat r5.xyz, r5
    mov_sat r6.xyz, r6
    add r4.xyz, r4, r6
    add r0.zw, c1.xyxy, v0.xyxy
    texld r7, r0.zwzw, s0
    mov_sat r7.xyz, r7
    add r4.xyz, r4, r7
    mad r8, r0.xyxy, c0.yxzx, v0.xyxy
    texld r9, r8, s0
    texld r8, r8.zwzw, s0
    mov_sat r8.xyz, r8
    mov_sat r9.xyz, r9
    add r10.xyz, r2, r9
    add r10.xyz, r8, r10
    add r10.xyz, r5, r10
    mad r4.xyz, r10, -c0.w, r4
    texld r10, v0, s0
    mov_sat r11.xyz, r10
    mov oC0.xyz, r10
    mad r4.xyz, r11, c2.x, r4
    mad r10.xyz, r4, c2.y, -r11
    mad r2.xyz, r4, c2.y, -r2
    mad r9.xyz, r4, c2.y, -r9
    add r2.xyz, r2_abs, r9_abs
    mad r8.xyz, r4, c2.y, -r8
    add r2.xyz, r2, r8_abs
    mad r5.xyz, r4, c2.y, -r5
    add r2.xyz, r2, r5_abs
    mul r2.xyz, r2, c2.w
    mad r2.xyz, r10_abs, c3.z, r2
    mad r3.xyz, r4, c2.y, -r3
    mad r1.xyz, r4, c2.y, -r1
    add r1.xyz, r1_abs, r3_abs
    mad r3.xyz, r4, c2.y, -r6
    add r1.xyz, r1, r3_abs
    mad r3.xyz, r4, c2.y, -r7
    add r1.xyz, r1, r3_abs
    mad r1.xyz, r1, c3.w, r2
    mad r2, r0.xyxy, c0.xwwx, v0.xyxy
    texld r3, r2, s0
    texld r2, r2.zwzw, s0
    mov_sat r2.xyz, r2
    mad r2.xyz, r4, c2.y, -r2
    mov_sat r3.xyz, r3
    mad r3.xyz, r4, c2.y, -r3
    add r2.xyz, r2_abs, r3_abs
    mad r0, r0.xyxy, c0_abs.wxxw, v0.xyxy
    texld r3, r0, s0
    texld r0, r0.zwzw, s0
    mov_sat r0.xyz, r0
    mad r0.xyz, r4, c2.y, -r0
    mov_sat r3.xyz, r3
    mad r3.xyz, r4, c2.y, -r3
    mul r4.xyz, r4, c2.y
    dp3 r0.w, r4, c2.z
    exp r0.w, r0.w
    mad_sat r0.w, r0.w, c3.x, c3.y
    add r2.xyz, r2, r3_abs
    add r0.xyz, r0_abs, r2
    mad r0.xyz, r0, c4.x, r1
    dp3 r0.x, r0, r0
    rsq r0.x, r0.x
    rcp r0.x, r0.x
    mad oC0.w, r0.x, r0.w, -c0.w

// approximately 77 instruction slots used (13 texture, 64 arithmetic)
#endif

const BYTE g_pixelShader_adaptiveSharpen1[] =
{
      0,   3, 255, 255, 254, 255, 
     40,   0,  67,  84,  65,  66, 
     28,   0,   0,   0, 115,   0, 
      0,   0,   0,   3, 255, 255, 
      2,   0,   0,   0,  28,   0, 
      0,   0,   0,   1,   0,   0, 
    108,   0,   0,   0,  68,   0, 
      0,   0,   2,   0,   1,   0, 
      1,   0,   6,   0,  72,   0, 
      0,   0,   0,   0,   0,   0, 
     88,   0,   0,   0,   3,   0, 
      0,   0,   1,   0,   2,   0, 
     92,   0,   0,   0,   0,   0, 
      0,   0, 112,  49,   0, 171, 
      1,   0,   3,   0,   1,   0, 
      2,   0,   1,   0,   0,   0, 
      0,   0,   0,   0, 115,  48, 
      0, 171,   4,   0,  12,   0, 
      1,   0,   1,   0,   1,   0, 
      0,   0,   0,   0,   0,   0, 
    112, 115,  95,  51,  95,  48, 
      0,  77, 105,  99, 114, 111, 
    115, 111, 102, 116,  32,  40, 
     82,  41,  32,  72,  76,  83, 
     76,  32,  83, 104,  97, 100, 
    101, 114,  32,  67, 111, 109, 
    112, 105, 108, 101, 114,  32, 
     49,  48,  46,  49,   0, 171, 
     81,   0,   0,   5,   0,   0, 
     15, 160,   0,   0,   0,   0, 
      0,   0, 128, 191,   0,   0, 
    128,  63,   0,   0,   0, 192, 
     81,   0,   0,   5,   2,   0, 
     15, 160,   0,   0, 128,  64, 
      0,   0, 128,  61, 222, 221, 
     29, 192,  51,  51, 147,  63, 
     81,   0,   0,   5,   3,   0, 
     15, 160, 102, 102, 102,  63, 
    137, 136, 136,  62, 215, 163, 
    176,  63,  31, 133, 107,  63, 
     81,   0,   0,   5,   4,   0, 
     15, 160,  31, 133, 107,  62, 
      0,   0,   0,   0,   0,   0, 
      0,   0,   0,   0,   0,   0, 
     31,   0,   0,   2,   5,   0, 
      0, 128,   0,   0,   3, 144, 
     31,   0,   0,   2,   0,   0, 
      0, 144,   0,   8,  15, 160, 
      1,   0,   0,   2,   0,   0, 
      3, 128,   1,   0, 228, 160, 
      4,   0,   0,   4,   1,   0, 
     15, 128,   0,   0,  68, 128, 
      0,   0, 100, 160,   0,   0, 
     68, 144,  66,   0,   0,   3, 
      2,   0,  15, 128,   1,   0, 
    228, 128,   0,   8, 228, 160, 
     66,   0,   0,   3,   1,   0, 
     15, 128,   1,   0, 238, 128, 
      0,   8, 228, 160,   1,   0, 
      0,   2,   1,   0,  23, 128, 
      1,   0, 228, 128,   1,   0, 
      0,   2,   2,   0,  23, 128, 
      2,   0, 228, 128,   2,   0, 
      0,   3,   0,   0,  12, 128, 
      1,   0,  68, 161,   0,   0, 
     68, 144,  66,   0,   0,   3, 
      3,   0,  15, 128,   0,   0, 
    238, 128,   0,   8, 228, 160, 
      1,   0,   0,   2,   3,   0, 
     23, 128,   3,   0, 228, 128, 
      2,   0,   0,   3,   4,   0, 
      7, 128,   1,   0, 228, 128, 
      3,   0, 228, 128,   4,   0, 
      0,   4,   5,   0,  15, 128, 
      0,   0,  68, 128,   0,   0, 
    137, 160,   0,   0,  68, 144, 
     66,   0,   0,   3,   6,   0, 
     15, 128,   5,   0, 228, 128, 
      0,   8, 228, 160,  66,   0, 
      0,   3,   5,   0,  15, 128, 
      5,   0, 238, 128,   0,   8, 
    228, 160,   1,   0,   0,   2, 
      5,   0,  23, 128,   5,   0, 
    228, 128,   1,   0,   0,   2, 
      6,   0,  23, 128,   6,   0, 
    228, 128,   2,   0,   0,   3, 
      4,   0,   7, 128,   4,   0, 
    228, 128,   6,   0, 228, 128, 
      2,   0,   0,   3,   0,   0, 
     12, 128,   1,   0,  68, 160, 
      0,   0,  68, 144,  66,   0, 
      0,   3,   7,   0,  15, 128, 
      0,   0, 238, 128,   0,   8, 
    228, 160,   1,   0,   0,   2, 
      7,   0,  23, 128,   7,   0, 
    228, 128,   2,   0,   0,   3, 
      4,   0,   7, 128,   4,   0, 
    228, 128,   7,   0, 228, 128, 
      4,   0,   0,   4,   8,   0, 
     15, 128,   0,   0,  68, 128, 
      0,   0,  33, 160,   0,   0, 
     68, 144,  66,   0,   0,   3, 
      9,   0,  15, 128,   8,   0, 
    228, 128,   0,   8, 228, 160, 
     66,   0,   0,   3,   8,   0, 
     15, 128,   8,   0, 238, 128, 
      0,   8, 228, 160,   1,   0, 
      0,   2,   8,   0,  23, 128, 
      8,   0, 228, 128,   1,   0, 
      0,   2,   9,   0,  23, 128, 
      9,   0, 228, 128,   2,   0, 
      0,   3,  10,   0,   7, 128, 
      2,   0, 228, 128,   9,   0, 
    228, 128,   2,   0,   0,   3, 
     10,   0,   7, 128,   8,   0, 
    228, 128,  10,   0, 228, 128, 
      2,   0,   0,   3,  10,   0, 
      7, 128,   5,   0, 228, 128, 
     10,   0, 228, 128,   4,   0, 
      0,   4,   4,   0,   7, 128, 
     10,   0, 228, 128,   0,   0, 
    255, 161,   4,   0, 228, 128, 
     66,   0,   0,   3,  10,   0, 
     15, 128,   0,   0, 228, 144, 
      0,   8, 228, 160,   1,   0, 
      0,   2,  11,   0,  23, 128, 
     10,   0, 228, 128,   1,   0, 
      0,   2,   0,   8,   7, 128, 
     10,   0, 228, 128,   4,   0, 
      0,   4,   4,   0,   7, 128, 
     11,   0, 228, 128,   2,   0, 
      0, 160,   4,   0, 228, 128, 
      4,   0,   0,   4,  10,   0, 
      7, 128,   4,   0, 228, 128, 
      2,   0,  85, 160,  11,   0, 
    228, 129,   4,   0,   0,   4, 
      2,   0,   7, 128,   4,   0, 
    228, 128,   2,   0,  85, 160, 
      2,   0, 228, 129,   4,   0, 
      0,   4,   9,   0,   7, 128, 
      4,   0, 228, 128,   2,   0, 
     85, 160,   9,   0, 228, 129, 
      2,   0,   0,   3,   2,   0, 
      7, 128,   2,   0, 228, 139, 
      9,   0, 228, 139,   4,   0, 
      0,   4,   8,   0,   7, 128, 
      4,   0, 228, 128,   2,   0, 
     85, 160,   8,   0, 228, 129, 
      2,   0,   0,   3,   2,   0, 
      7, 128,   2,   0, 228, 128, 
      8,   0, 228, 139,   4,   0, 
      0,   4,   5,   0,   7, 128, 
      4,   0, 228, 128,   2,   0, 
     85, 160,   5,   0, 228, 129, 
      2,   0,   0,   3,   2,   0, 
      7, 128,   2,   0, 228, 128, 
      5,   0, 228, 139,   5,   0, 
      0,   3,   2,   0,   7, 128, 
      2,   0, 228, 128,   2,   0, 
    255, 160,   4,   0,   0,   4, 
      2,   0,   7, 128,  10,   0, 
    228, 139,   3,   0, 170, 160, 
      2,   0, 228, 128,   4,   0, 
      0,   4,   3,   0,   7, 128, 
      4,   0, 228, 128,   2,   0, 
     85, 160,   3,   0, 228, 129, 
      4,   0,   0,   4,   1,   0, 
      7, 128,   4,   0, 228, 128, 
      2,   0,  85, 160,   1,   0, 
    228, 129,   2,   0,   0,   3, 
      1,   0,   7, 128,   1,   0, 
    228, 139,   3,   0, 228, 139, 
      4,   0,   0,   4,   3,   0, 
      7, 128,   4,   0, 228, 128, 
      2,   0,  85, 160,   6,   0, 
    228, 129,   2,   0,   0,   3, 
      1,   0,   7, 128,   1,   0, 
    228, 128,   3,   0, 228, 139, 
      4,   0,   0,   4,   3,   0, 
      7, 128,   4,   0, 228, 128, 
      2,   0,  85, 160,   7,   0, 
    228, 129,   2,   0,   0,   3, 
      1,   0,   7, 128,   1,   0, 
    228, 128,   3,   0, 228, 139, 
      4,   0,   0,   4,   1,   0, 
      7, 128,   1,   0, 228, 128, 
      3,   0, 255, 160,   2,   0, 
    228, 128,   4,   0,   0,   4, 
      2,   0,  15, 128,   0,   0, 
     68, 128,   0,   0,  60, 160, 
      0,   0,  68, 144,  66,   0, 
      0,   3,   3,   0,  15, 128, 
      2,   0, 228, 128,   0,   8, 
    228, 160,  66,   0,   0,   3, 
      2,   0,  15, 128,   2,   0, 
    238, 128,   0,   8, 228, 160, 
      1,   0,   0,   2,   2,   0, 
     23, 128,   2,   0, 228, 128, 
      4,   0,   0,   4,   2,   0, 
      7, 128,   4,   0, 228, 128, 
      2,   0,  85, 160,   2,   0, 
    228, 129,   1,   0,   0,   2, 
      3,   0,  23, 128,   3,   0, 
    228, 128,   4,   0,   0,   4, 
      3,   0,   7, 128,   4,   0, 
    228, 128,   2,   0,  85, 160, 
      3,   0, 228, 129,   2,   0, 
      0,   3,   2,   0,   7, 128, 
      2,   0, 228, 139,   3,   0, 
    228, 139,   4,   0,   0,   4, 
      0,   0,  15, 128,   0,   0, 
     68, 128,   0,   0, 195, 171, 
      0,   0,  68, 144,  66,   0, 
      0,   3,   3,   0,  15, 128, 
      0,   0, 228, 128,   0,   8, 
    228, 160,  66,   0,   0,   3, 
      0,   0,  15, 128,   0,   0, 
    238, 128,   0,   8, 228, 160, 
      1,   0,   0,   2,   0,   0, 
     23, 128,   0,   0, 228, 128, 
      4,   0,   0,   4,   0,   0, 
      7, 128,   4,   0, 228, 128, 
      2,   0,  85, 160,   0,   0, 
    228, 129,   1,   0,   0,   2, 
      3,   0,  23, 128,   3,   0, 
    228, 128,   4,   0,   0,   4, 
      3,   0,   7, 128,   4,   0, 
    228, 128,   2,   0,  85, 160, 
      3,   0, 228, 129,   5,   0, 
      0,   3,   4,   0,   7, 128, 
      4,   0, 228, 128,   2,   0, 
     85, 160,   8,   0,   0,   3, 
      0,   0,   8, 128,   4,   0, 
    228, 128,   2,   0, 170, 160, 
     14,   0,   0,   2,   0,   0, 
      8, 128,   0,   0, 255, 128, 
      4,   0,   0,   4,   0,   0, 
     24, 128,   0,   0, 255, 128, 
      3,   0,   0, 160,   3,   0, 
     85, 160,   2,   0,   0,   3, 
      2,   0,   7, 128,   2,   0, 
    228, 128,   3,   0, 228, 139, 
      2,   0,   0,   3,   0,   0, 
      7, 128,   0,   0, 228, 139, 
      2,   0, 228, 128,   4,   0, 
      0,   4,   0,   0,   7, 128, 
      0,   0, 228, 128,   4,   0, 
      0, 160,   1,   0, 228, 128, 
      8,   0,   0,   3,   0,   0, 
      1, 128,   0,   0, 228, 128, 
      0,   0, 228, 128,   7,   0, 
      0,   2,   0,   0,   1, 128, 
      0,   0,   0, 128,   6,   0, 
      0,   2,   0,   0,   1, 128, 
      0,   0,   0, 128,   4,   0, 
      0,   4,   0,   8,   8, 128, 
      0,   0,   0, 128,   0,   0, 
    255, 128,   0,   0, 255, 161, 
    255, 255,   0,   0
};
