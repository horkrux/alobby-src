# DDraw Proxy Library

This re-implements parts of the DDraw library and acts as a proxy to more recent graphics interfaces utilizing
Direct3D9 by default with a fallback to OpenGL/GDI when that is unavailable.

Most of the code is from the cnc-ddraw project on GitHub (see license) with custom additions for Settlers3, 
in particular around mouse handling, window messaging, missing DirectDraw implementation details, and 
installation/setup/configuration.
