#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#include <stdio.h>
#include "config.h"
#include "dd.h"
#include "render_d3d9.h"
#include "render_gdi.h"
#include "render_ogl.h"
#include "hook.h"
#include "debug.h"


ddraw_config g_config =
    { .window_rect = { .left = -32000, .top = -32000, .right = 0, .bottom = 0 }, .window_state = -1, .rendering_mode = -1 };

void init_config()
{
#ifndef NDEBUG
    g_ddProxy->fullscreen = FALSE;
    g_ddProxy->border = TRUE;
#else
    if (g_config.force_window_mode) {
        g_ddProxy->fullscreen = FALSE;
        g_ddProxy->border = TRUE;
    } else {
        g_ddProxy->fullscreen = TRUE;
        g_ddProxy->border = FALSE;
    }
#endif

    if (g_ddProxy->wine) {
        g_config.force_window_mode = TRUE;
        if (g_config.force_window_mode) {
            g_ddProxy->windowed = TRUE;
        } else {
            // Wine/Linux displays fullscreens-windowed applications wrong
            g_ddProxy->windowed = FALSE;
        }
        g_config.should_apply_texture_filter = FALSE;
        g_ddProxy->apply_linear_filter = FALSE;
        g_ddProxy->render.maxfps = 60;
        g_ddProxy->nonexclusive = FALSE;
        g_ddProxy->noactivateapp = FALSE;
    } else {
        g_ddProxy->windowed = TRUE;
        g_ddProxy->apply_linear_filter = TRUE;
        g_ddProxy->render.maxfps = 100;
        g_ddProxy->nonexclusive = FALSE;
        g_ddProxy->noactivateapp = TRUE;
    }

    g_ddProxy->force_display_resolution = FALSE;
    g_ddProxy->boxing = FALSE;
    g_ddProxy->maintas = FALSE;
    g_ddProxy->adjmouse = FALSE;
    g_ddProxy->devmode = FALSE;
    g_ddProxy->vsync = FALSE;
    g_ddProxy->vhack = FALSE;
    g_ddProxy->accurate_timers = FALSE;
    g_ddProxy->resizable = FALSE;
    g_ddProxy->fixchildwindows = TRUE;
    g_ddProxy->sierrahack = FALSE;// Sierra Caesar III, Pharaoh, and Zeus hack
    g_ddProxy->dk2hack = FALSE; // Dungeon Keeper 2 hack
    g_ddProxy->s3_scrolling = FALSE;

    if (!g_config.should_apply_texture_filter) {
        g_ddProxy->apply_linear_filter = FALSE;
    }

    if (g_config.force_display_resolution) {
        g_ddProxy->force_display_resolution = TRUE;
        g_ddProxy->display_width = g_config.display_width;
        g_ddProxy->display_height = g_config.display_height;
    }

    g_config.window_rect.right = 0;
    g_config.window_rect.bottom = 0;
    g_config.window_rect.left = -32000;
    g_config.window_rect.top = -32000;

#ifdef _MSC_VER
    g_hook_method = 4;
#endif

    if (g_config.reduce_max_fps) {
        g_ddProxy->render.maxfps = 30;
    }

    g_ddProxy->render.minfps = 0;

    if (g_ddProxy->render.minfps > 1000)
    {
        g_ddProxy->render.minfps = 1000;
    }

    if (g_ddProxy->render.minfps > 0)
    {
        g_ddProxy->render.minfps_tick_len = 1000.0f / g_ddProxy->render.minfps;
    }

    if (g_ddProxy->accurate_timers || g_ddProxy->vsync)
        g_ddProxy->fps_limiter.htimer = CreateWaitableTimer(NULL, TRUE, NULL);
    //can't fully set it up here due to missing g_ddraw->mode.dmDisplayFrequency

    int max_ticks = 0;

    if (max_ticks > 0 && max_ticks <= 1000)
    {
        if (g_ddProxy->accurate_timers)
            g_ddProxy->ticks_limiter.htimer = CreateWaitableTimer(NULL, TRUE, NULL);

        float len = 1000.0f / max_ticks;
        g_ddProxy->ticks_limiter.tick_length_ns = len * 10000;
        g_ddProxy->ticks_limiter.tick_length = len + 0.5f;
    }

    if (max_ticks >= 0)
    {
        //always using 60 fps for flip...
        if (g_ddProxy->accurate_timers)
            g_ddProxy->flip_limiter.htimer = CreateWaitableTimer(NULL, TRUE, NULL);

        float flip_len = 1000.0f / 60;
        g_ddProxy->flip_limiter.tick_length_ns = flip_len * 10000;
        g_ddProxy->flip_limiter.tick_length = flip_len + 0.5f;
    }

    if (g_ddProxy->fullscreen)
    {
        g_config.window_rect.left = g_config.window_rect.top = -32000;
    }

    g_ddProxy->handlemouse = FALSE;
    if (!g_ddProxy->handlemouse)
    {
        g_ddProxy->adjmouse = TRUE;
    }

    SetProcessAffinityMask(GetCurrentProcess(), 1);
    g_ddProxy->render.bpp = 0;

    if (g_ddProxy->render.bpp != 16 && g_ddProxy->render.bpp != 24 && g_ddProxy->render.bpp != 32)
    {
        g_ddProxy->render.bpp = 0;
    }

    g_ddProxy->shader;
    memset(g_ddProxy->shader, 0, sizeof(g_ddProxy->shader));

    char warningMessage[512];
    if (g_config.rendering_mode == 0 && !d3d9_is_available()) {
        sprintf(warningMessage, "Direct3D9 is not available on your PC. Re-Install DirectX components.");
        show_warning(warningMessage, 15);
        g_config.rendering_mode = -1;
    } else if (g_config.rendering_mode == 1 && !oglu_load_dll()) {
        sprintf(warningMessage, "OpenGL is not available on your PC. Update your graphics drivers.");
        g_config.rendering_mode = -1;
    }

    if (g_config.rendering_mode == -1) {
        if (!g_ddProxy->wine && d3d9_is_available()) {
            g_config.rendering_mode = 0;
        } else if (oglu_load_dll()) {
            g_config.rendering_mode = 1;
        } else {
            g_config.rendering_mode = 2;
        }
    }

    if (g_config.rendering_mode == 0) {
        dprintf("Using d3d9 renderer\n");
        g_ddProxy->renderer = d3d9_render_main;
    } else if (g_config.rendering_mode == 1) {
        dprintf("Using ogl renderer\n");
        g_ddProxy->renderer = ogl_render_main;
    } else {
        dprintf("Using GDI renderer\n");
        g_ddProxy->renderer = gdi_render_main;
    }
}
