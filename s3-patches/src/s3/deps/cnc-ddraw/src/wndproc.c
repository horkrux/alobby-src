#include "windows.h"
#include <windowsx.h>
#include "dd.h"
#include "hook.h"
#include "mouse.h"
#include "render_d3d9.h"
#include "config.h"
#include "winapi_hooks.h"
#include "wndproc.h"
#include "ddraw_utils.h"

BOOL g_GameHandlesClose = FALSE;

LRESULT CALLBACK fake_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
//    dprintf("-->%s(msg=%d, wParam=%d, lParam=%d)\n", __FUNCTION__, uMsg, wParam, lParam);

    RECT rc = { 0, 0, g_ddProxy->render.width, g_ddProxy->render.height };

    static BOOL in_size_move = FALSE;
    static int redraw_count = 0;
    BOOL isLocked = g_ddProxy->locked;
    
    switch(uMsg)
    {
        case WM_GETMINMAXINFO:
        case WM_MOVING:
        case WM_NCLBUTTONDOWN:
        case WM_NCLBUTTONUP:
        case WM_NCPAINT:
            return DefWindowProc(hWnd, uMsg, wParam, lParam);

        case WM_NCACTIVATE:
            if (g_ddProxy->locked) {
                mouse_unlock();
            }
            return DefWindowProc(hWnd, uMsg, wParam, lParam);

        case WM_KILLFOCUS:
            if (g_ddProxy->locked) {
                mouse_unlock();
            }

            return DefWindowProc(hWnd, uMsg, wParam, lParam);

        case WM_NCHITTEST:
        {
            LRESULT result = DefWindowProc(hWnd, uMsg, wParam, lParam);

            if (!g_ddProxy->resizable)
            {
                switch (result)
                {
                    case HTBOTTOM:
                    case HTBOTTOMLEFT:
                    case HTBOTTOMRIGHT:
                    case HTLEFT:
                    case HTRIGHT:
                    case HTTOP:
                    case HTTOPLEFT:
                    case HTTOPRIGHT:
                        return HTBORDER;
                }
            }

            return result;
        }
        case WM_SETCURSOR:
        {
            // show resize cursor on window borders
            if ((HWND)wParam == g_ddProxy->hwnd)
            {
                WORD message = HIWORD(lParam);

                if (message == WM_MOUSEMOVE)
                {
                    WORD htcode = LOWORD(lParam);

                    switch (htcode)
                    {
                        case HTCAPTION:
                        case HTMINBUTTON:
                        case HTMAXBUTTON:
                        case HTCLOSE:
                        case HTBOTTOM:
                        case HTBOTTOMLEFT:
                        case HTBOTTOMRIGHT:
                        case HTLEFT:
                        case HTRIGHT:
                        case HTTOP:
                        case HTTOPLEFT:
                        case HTTOPRIGHT:
                            return DefWindowProc(hWnd, uMsg, wParam, lParam);
                        case HTCLIENT:
                            if (!g_ddProxy->locked)
                                return DefWindowProc(hWnd, uMsg, wParam, lParam);
                        default:
                            break;
                    }
                }
            }

            break;
        }
        case WM_D3D9DEVICELOST:
        {
            if (g_ddProxy->renderer == d3d9_render_main && d3d9_on_device_lost())
            {
                if (!g_ddProxy->windowed)
                    mouse_lock();
            }
            return 0;
        }
        case WM_TIMER:
        {
            switch (wParam)
            {
                case IDT_TIMER_LEAVE_BNET:
                {
                    KillTimer(g_ddProxy->hwnd, IDT_TIMER_LEAVE_BNET);

                    if (!g_ddProxy->windowed)
                        g_ddProxy->bnet_was_fullscreen = FALSE;

                    if (!g_ddProxy->bnet_active)
                    {
                        if (g_ddProxy->bnet_was_fullscreen)
                        {
                            int ws = g_config.window_state;
                            util_toggle_fullscreen();
                            g_config.window_state = ws;
                            g_ddProxy->bnet_was_fullscreen = FALSE;
                        }
                        else if (g_ddProxy->bnet_was_upscaled)
                        {
                            util_set_window_rect(0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
                            g_ddProxy->bnet_was_upscaled = FALSE;
                        }
                    }

                    return 0;
                }   
            }
            break;
        }
        case WM_WINDOWPOSCHANGED:
        {
            WINDOWPOS *pos = (WINDOWPOS *)lParam;

            if (g_ddProxy->wine && !g_ddProxy->windowed && (pos->x > 0 || pos->y > 0) &&
                g_ddProxy->last_set_window_pos_tick + 500 < timeGetTime())
            {
                PostMessage(g_ddProxy->hwnd, WM_WINEFULLSCREEN, 0, 0);
            }

            break;
        }
        case WM_WINEFULLSCREEN:
        {
            if (!g_ddProxy->windowed)
            {
                g_ddProxy->last_set_window_pos_tick = timeGetTime();
                real_SetWindowPos(g_ddProxy->hwnd, HWND_TOPMOST, 1, 1, g_ddProxy->render.width,
                                  g_ddProxy->render.height, SWP_SHOWWINDOW);
                real_SetWindowPos(g_ddProxy->hwnd, HWND_TOPMOST, 0, 0, g_ddProxy->render.width,
                                  g_ddProxy->render.height, SWP_SHOWWINDOW);
            }
            return 0;
        }
        case WM_ENTERSIZEMOVE:
        {
            if (g_ddProxy->windowed)
            {
                in_size_move = TRUE;
            }
            break;
        }
        case WM_EXITSIZEMOVE:
        {
            if (g_ddProxy->windowed)
            {
                in_size_move = FALSE;

                if (!g_ddProxy->render.thread)
                    dd_SetDisplayMode(g_ddProxy->width, g_ddProxy->height, g_ddProxy->bpp);
            }
            break;
        }

        case WM_MOVE:
        {
            if (g_ddProxy->windowed)
            {
                int x = (int)(short)LOWORD(lParam);
                int y = (int)(short)HIWORD(lParam);

                if (x != -32000 && y != -32000)
                {
                    util_update_bnet_pos(x, y);
                }

                if (in_size_move || g_ddProxy->wine)
                {
                    if (x != -32000)
                        g_config.window_rect.left = x; // -32000 = Exit/Minimize

                    if (y != -32000)
                        g_config.window_rect.top = y;
                }
            }
            
            if (!g_ddProxy->handlemouse)
                RedrawWindow(hWnd, NULL, NULL, RDW_INVALIDATE | RDW_ALLCHILDREN);

            if (g_ddProxy->sierrahack)
            {
                lParam = 0;
                break;
            }

            return DefWindowProc(hWnd, uMsg, wParam, lParam); /* Carmageddon fix */
        }

        //workaround for a bug where sometimes a background window steals the focus
        case WM_WINDOWPOSCHANGING:
        {
            if (g_ddProxy->locked)
            {
                WINDOWPOS *pos = (WINDOWPOS *)lParam;
                
                if (pos->flags == SWP_NOMOVE + SWP_NOSIZE)
                {
                    mouse_unlock();
                    
                    if (GetForegroundWindow() == g_ddProxy->hwnd)
                        mouse_lock();
                }
            }
            break;
        }

        case WM_MOUSELEAVE:
            mouse_unlock();
            return 0;

        case WM_ACTIVATE:
            if (wParam == WA_ACTIVE || wParam == WA_CLICKACTIVE)
            {
                if (!g_ddProxy->windowed)
                {
                    if (g_ddProxy->renderer != d3d9_render_main)
                    {
                        ChangeDisplaySettings(&g_ddProxy->render.mode, CDS_FULLSCREEN);

                        if (wParam == WA_ACTIVE)
                        {
                            mouse_lock();
                        }
                    }
                }

                if (!g_ddProxy->handlemouse)
                    RedrawWindow(hWnd, NULL, NULL, RDW_INVALIDATE | RDW_ALLCHILDREN);
            }
            else if (wParam == WA_INACTIVE)
            {
                if (!g_ddProxy->windowed && !g_ddProxy->locked && g_ddProxy->noactivateapp)
                    return 0;

                mouse_unlock();

                if (g_ddProxy->wine && g_ddProxy->last_set_window_pos_tick + 500 > timeGetTime())
                    return 0;

                /* minimize our window on defocus when in fullscreen */
                if (!g_ddProxy->windowed)
                {
                    if (g_ddProxy->renderer != d3d9_render_main)
                    {
                        ShowWindow(g_ddProxy->hwnd, SW_MINIMIZE);
                        ChangeDisplaySettings(NULL, g_ddProxy->bnet_active ? CDS_FULLSCREEN : 0);
                    }
                }
            }
            return 0;

        case WM_ACTIVATEAPP:
            if (g_ddProxy->windowed || g_ddProxy->noactivateapp)
            {
                return 0;
            }
            break;

        case WM_AUTORENDERER:
        {
            mouse_unlock();
            real_SetWindowPos(g_ddProxy->hwnd, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
            real_SetWindowPos(g_ddProxy->hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
            mouse_lock();
            return 0;
        }

        /* button up messages reactivate cursor lock */
        case WM_LBUTTONUP:
        case WM_RBUTTONUP:
        case WM_MBUTTONUP:
        case WM_XBUTTONUP:
        case WM_LBUTTONDBLCLK:
        case WM_MBUTTONDBLCLK:
        case WM_RBUTTONDBLCLK:
        case WM_XBUTTONDBLCLK:
        case WM_XBUTTONDOWN:
        case WM_LBUTTONDOWN:
        case WM_RBUTTONDOWN:
        case WM_MBUTTONDOWN:
        case WM_MOUSEWHEEL:
        {
            if (!g_ddProxy->locked)
            {
                mouse_lock();
            }
            /* fall through for lParam */
        }
        /* ignored messages while the application is not active */
        case WM_MOUSEMOVE:
        case WM_MOUSEHOVER:
        {
            fake_GetCursorPos(NULL); /* update our own cursor */
            lParam = MAKELPARAM(g_ddProxy->cursor.x, g_ddProxy->cursor.y);

            // If we just regained lock in this call, we need to move the cursor to its current position before
            // sending the click events. Otherwise it will look as if the user made a selection from the previous
            // known coordinate - which he has not.
            if (!isLocked) {
                g_ddProxy->wndproc(hWnd, WM_MOUSEMOVE, wParam, lParam);
            }

            break;
        }
        case WM_PARENTNOTIFY:
        {
            if (!g_ddProxy->handlemouse)
            {
                switch (LOWORD(wParam))
                {
                    case WM_DESTROY: //Workaround for invisible menu on Load/Save/Delete in Tiberian Sun
                        redraw_count = 2;
                        break;
                    case WM_LBUTTONDOWN:
                    case WM_MBUTTONDOWN:
                    case WM_RBUTTONDOWN:
                    case WM_XBUTTONDOWN:
                    {
                        if (!g_ddProxy->locked)
                        {
                            int x = GET_X_LPARAM(lParam);
                            int y = GET_Y_LPARAM(lParam);

                            g_ddProxy->cursor.x = (x - g_ddProxy->render.viewport.x) * g_ddProxy->render.unscale_w;
                            g_ddProxy->cursor.y = (y - g_ddProxy->render.viewport.y) * g_ddProxy->render.unscale_h;

                            g_ddProxy->hidecursor = FALSE;

                            mouse_lock();
                        }
                        break;
                    }
                }
            }
            break;
        }
        case WM_PAINT:
        {
            if (!g_ddProxy->handlemouse && redraw_count > 0)
            {
                redraw_count--;
                RedrawWindow(hWnd, NULL, NULL, RDW_INVALIDATE | RDW_ALLCHILDREN);
            }

            EnterCriticalSection(&g_ddProxy->cs);
            ReleaseSemaphore(g_ddProxy->render.sem, 1, NULL);
            LeaveCriticalSection(&g_ddProxy->cs);
            break;
        }
        case WM_ERASEBKGND:
        {
            EnterCriticalSection(&g_ddProxy->cs);
            FillRect(g_ddProxy->render.hdc, &rc, (HBRUSH)GetStockObject(BLACK_BRUSH));
            ReleaseSemaphore(g_ddProxy->render.sem, 1, NULL);
            LeaveCriticalSection(&g_ddProxy->cs);
            break;
        }
    }

    if (!g_ddProxy->locked && uMsg < WM_USER) {
//        dprintf("Ignoring app event (id=%d) since application is not in focus.\n");
        return DefWindowProcA(hWnd, uMsg, wParam, lParam);
    }

//    dprintf("Forwarding event (id=%d) to app\n", uMsg);
    LRESULT res = g_ddProxy->wndproc(hWnd, uMsg, wParam, lParam);
//    dprintf("<-- %s(res=%d)\n", __FUNCTION__, res);

    return res;
}
