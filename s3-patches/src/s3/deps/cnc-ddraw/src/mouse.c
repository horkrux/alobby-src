#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#include <debug.h>
#include "dd.h"
#include "hook.h"


void mouse_lock()
{
    RECT rc;

    if (g_hook_active && !g_ddProxy->locked)
    {
        g_ddProxy->wndproc(g_ddProxy->hwnd, WM_ACTIVATEAPP, TRUE, 0);

        // Get the window client area.
        real_GetClientRect(g_ddProxy->hwnd, &rc);
        
        if(g_ddProxy->adjmouse)
        {
            rc.right = g_ddProxy->render.viewport.width;
            rc.bottom = g_ddProxy->render.viewport.height;
        }
        else
        {
            rc.right = g_ddProxy->width;
            rc.bottom = g_ddProxy->height;
        }

        // Convert the client area to screen coordinates.
        POINT pt = { rc.left, rc.top };
        POINT pt2 = { rc.right, rc.bottom };

        real_ClientToScreen(g_ddProxy->hwnd, &pt);
        real_ClientToScreen(g_ddProxy->hwnd, &pt2);
        
        SetRect(&rc, pt.x, pt.y, pt2.x, pt2.y);
        
        rc.bottom -= (g_ddProxy->mouse_y_adjust * 2) * g_ddProxy->render.scale_h;

        if (g_ddProxy->handlemouse)
        {
            SetCapture(g_ddProxy->hwnd);
            real_ClipCursor(&rc);
            while (real_ShowCursor(FALSE) > 0);
        }
        else
        {
            if (g_ddProxy->hidecursor)
            {
                g_ddProxy->hidecursor = FALSE;
                real_ShowCursor(FALSE);
            }

            if (!g_ddProxy->windowed) {
                real_ClipCursor(&rc);
            }
        }

        g_ddProxy->locked = TRUE;
    }
}

void mouse_unlock()
{
    RECT rc;

    if (!g_hook_active)
    {
        return;
    }

    if (g_ddProxy->locked)
    {
        g_ddProxy->locked = FALSE;

        if (g_ddProxy->s3_scrolling) {
            g_ddProxy->s3_scrolling = FALSE;
            real_ClipCursor(NULL);
        }

        // Get the window client area.
        real_GetClientRect(g_ddProxy->hwnd, &rc);
        
        // Convert the client area to screen coordinates.
        POINT pt = { rc.left, rc.top };
        POINT pt2 = { rc.right, rc.bottom };

        real_ClientToScreen(g_ddProxy->hwnd, &pt);
        real_ClientToScreen(g_ddProxy->hwnd, &pt2);

        SetRect(&rc, pt.x, pt.y, pt2.x, pt2.y);
       
        if (g_ddProxy->handlemouse)
        {
            while (real_ShowCursor(TRUE) < 0);
            real_SetCursor(LoadCursor(NULL, IDC_ARROW));
        }
        else
        {
            CURSORINFO ci = { .cbSize = sizeof(CURSORINFO) };
            if (real_GetCursorInfo(&ci) && ci.flags == 0)
            {
                g_ddProxy->hidecursor = TRUE;
                while (real_ShowCursor(TRUE) < 0);
            }
        }

        if (!g_ddProxy->windowed) {
            real_ClipCursor(NULL);
        }
        ReleaseCapture();
    }
}
