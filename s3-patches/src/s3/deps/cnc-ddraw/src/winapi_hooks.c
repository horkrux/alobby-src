#include "windows.h"
#include <debug.h>
#include "dd.h"
#include "hook.h"
#include "config.h"
#include "ddraw_utils.h"
#include "mouse.h"
#include "wndproc.h"


BOOL WINAPI fake_GetCursorPos(LPPOINT lpPoint)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);
    POINT pt, realpt;
    
    if (!real_GetCursorPos(&pt) || !g_ddProxy)
        return FALSE;
    
    realpt.x = pt.x;
    realpt.y = pt.y;
    
    if(g_ddProxy->locked && (!g_ddProxy->windowed || real_ScreenToClient(g_ddProxy->hwnd, &pt)))
    {
        //fallback solution for possible ClipCursor failure
        int diffx = 0, diffy = 0;

        int max_width = g_ddProxy->adjmouse ? g_ddProxy->render.viewport.width : g_ddProxy->width;
        int max_height = g_ddProxy->adjmouse ? g_ddProxy->render.viewport.height : g_ddProxy->height;

        if (pt.x < 0)
        {
            diffx = pt.x;
            pt.x = 0;
        }

        if (pt.y < 0)
        {
            diffy = pt.y;
            pt.y = 0;
        }

        if (pt.x > max_width)
        {
            diffx = pt.x - max_width;
            pt.x = max_width;
        }

        if (pt.y > max_height)
        {
            diffy = pt.y - max_height;
            pt.y = max_height;
        }

        if (diffx || diffy) {
            winapi_dprintf("Adjusting cursor position\n");
            real_SetCursorPos(realpt.x - diffx, realpt.y - diffy);
        }


        if(g_ddProxy->adjmouse)
        {
            g_ddProxy->cursor.x = pt.x * g_ddProxy->render.unscale_w;
            g_ddProxy->cursor.y = pt.y * g_ddProxy->render.unscale_h;
        }
        else
        {
            g_ddProxy->cursor.x = pt.x;
            g_ddProxy->cursor.y = pt.y;
        }

        if (g_ddProxy->vhack && InterlockedExchangeAdd(&g_ddProxy->incutscene, 0))
        {
            diffx = 0;
            diffy = 0;

            if (g_ddProxy->cursor.x > CUTSCENE_WIDTH)
            {
                diffx = g_ddProxy->cursor.x - CUTSCENE_WIDTH;
                g_ddProxy->cursor.x = CUTSCENE_WIDTH;
            }
                
            if (g_ddProxy->cursor.y > CUTSCENE_HEIGHT)
            {
                diffy = g_ddProxy->cursor.y - CUTSCENE_HEIGHT;
                g_ddProxy->cursor.y = CUTSCENE_HEIGHT;
            }

            if (diffx || diffy) {
                winapi_dprintf("Adjusting cursor position #2\n");
                real_SetCursorPos(realpt.x - diffx, realpt.y - diffy);
            }
        }
    }

    if (lpPoint)
    {
        lpPoint->x = (int)g_ddProxy->cursor.x;
        lpPoint->y = (int)g_ddProxy->cursor.y;
    }
    
    return TRUE;
}

BOOL WINAPI fake_ClipCursor(const RECT *lpRect)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if(lpRect)
    {
        // S3 Fix: Someone is scrolling in game, we should make this clip.
        if (lpRect->bottom - lpRect->top == 1 && lpRect->right - lpRect->left == 1) {
            POINT pt;
            if (real_GetCursorPos(&pt)) {
                g_ddProxy->s3_scrolling = TRUE;
                RECT realRect = { pt.x, pt.y, pt.x+1, pt.y+1 };

                return real_ClipCursor(&realRect);
            }
        }

        /* hack for 640x480 mode */
        if (lpRect->bottom == 400 && g_ddProxy && g_ddProxy->height == 480)
            g_ddProxy->mouse_y_adjust = 40;
    } else {
        if (g_ddProxy->s3_scrolling) {
            g_ddProxy->s3_scrolling = FALSE;

            return real_ClipCursor(NULL);
        }
    }

    return TRUE;
}

int WINAPI fake_ShowCursor(BOOL bShow)
{
    static int count;
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if (g_ddProxy && !g_ddProxy->handlemouse)
        return real_ShowCursor(bShow);

    return bShow ? ++count : --count;
}

HCURSOR WINAPI fake_SetCursor(HCURSOR hCursor)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if (g_ddProxy && !g_ddProxy->handlemouse) {
        return real_SetCursor(hCursor);
    }
    
    return NULL;
}

BOOL WINAPI fake_GetWindowRect(HWND hWnd, LPRECT lpRect)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if (lpRect && g_ddProxy)
    {
        if (g_ddProxy->hwnd == hWnd)
        {
            lpRect->bottom = g_ddProxy->height;
            lpRect->left = 0;
            lpRect->right = g_ddProxy->width;
            lpRect->top = 0;

            return TRUE;
        }
        else
        {
            if (real_GetWindowRect(hWnd, lpRect))
            {
                MapWindowPoints(HWND_DESKTOP, g_ddProxy->hwnd, (LPPOINT)lpRect, 2);

                return TRUE;
            }

            return FALSE;
        }
    }

    return real_GetWindowRect(hWnd, lpRect);
}

BOOL WINAPI fake_GetClientRect(HWND hWnd, LPRECT lpRect)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if (lpRect && g_ddProxy && g_ddProxy->hwnd == hWnd)
    {
        lpRect->bottom = g_ddProxy->height;
        lpRect->left = 0;
        lpRect->right = g_ddProxy->width;
        lpRect->top = 0;

        return TRUE;
    }

    return real_GetClientRect(hWnd, lpRect);
}

BOOL WINAPI fake_ClientToScreen(HWND hWnd, LPPOINT lpPoint)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if (g_ddProxy && g_ddProxy->hwnd != hWnd)
        return real_ClientToScreen(hWnd, lpPoint) && real_ScreenToClient(g_ddProxy->hwnd, lpPoint);

    return TRUE;
}

BOOL WINAPI fake_ScreenToClient(HWND hWnd, LPPOINT lpPoint)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if (g_ddProxy && g_ddProxy->hwnd != hWnd)
        return real_ClientToScreen(g_ddProxy->hwnd, lpPoint) && real_ScreenToClient(hWnd, lpPoint);

    return TRUE;
}

BOOL WINAPI fake_SetCursorPos(int X, int Y)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    POINT pt = { X, Y };
    return g_ddProxy && real_ClientToScreen(g_ddProxy->hwnd, &pt) && real_SetCursorPos(pt.x, pt.y);
}

HWND WINAPI fake_WindowFromPoint(POINT Point)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    POINT pt = { Point.x, Point.y };
    return g_ddProxy && real_ClientToScreen(g_ddProxy->hwnd, &pt) ? real_WindowFromPoint(pt) : NULL;
}

BOOL WINAPI fake_GetClipCursor(LPRECT lpRect)
{
    winapi_dprintf("--> %s()\n");

    if (lpRect && g_ddProxy)
    {
        lpRect->bottom = g_ddProxy->height;
        lpRect->left = 0;
        lpRect->right = g_ddProxy->width;
        lpRect->top = 0;

        return TRUE;
    }

    return FALSE;
}

BOOL WINAPI fake_GetCursorInfo(PCURSORINFO pci)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    return pci && g_ddProxy && real_GetCursorInfo(pci) && real_ScreenToClient(g_ddProxy->hwnd, &pci->ptScreenPos);
}

int WINAPI fake_GetSystemMetrics(int nIndex)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if (g_ddProxy)
    {
        if (nIndex == SM_CXSCREEN)
            return g_ddProxy->width;

        if (nIndex == SM_CYSCREEN)
            return g_ddProxy->height;
    }

    return real_GetSystemMetrics(nIndex);
}

BOOL WINAPI fake_SetWindowPos(HWND hWnd, HWND hWndInsertAfter, int X, int Y, int cx, int cy, UINT uFlags)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    UINT req_flags = SWP_NOSIZE | SWP_NOMOVE | SWP_NOZORDER;

    if (g_ddProxy && g_ddProxy->hwnd == hWnd && (uFlags & req_flags) != req_flags)
        return TRUE;

    return real_SetWindowPos(hWnd, hWndInsertAfter, X, Y, cx, cy, uFlags);
}

BOOL WINAPI fake_MoveWindow(HWND hWnd, int X, int Y, int nWidth, int nHeight, BOOL bRepaint)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if (g_ddProxy && g_ddProxy->hwnd == hWnd)
        return TRUE;

    return real_MoveWindow(hWnd, X, Y, nWidth, nHeight, bRepaint);
}

LRESULT WINAPI fake_SendMessageA(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    LRESULT result = real_SendMessageA(hWnd, Msg, wParam, lParam);

    if (result && g_ddProxy && Msg == CB_GETDROPPEDCONTROLRECT)
    {
        RECT *rc = (RECT *)lParam;
        if (rc)
            MapWindowPoints(HWND_DESKTOP, g_ddProxy->hwnd, (LPPOINT)rc, 2);
    }

    return result;
}

LONG WINAPI fake_SetWindowLongA(HWND hWnd, int nIndex, LONG dwNewLong)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if (g_ddProxy && g_ddProxy->hwnd == hWnd && nIndex == GWL_STYLE)
        return 0;

    return real_SetWindowLongA(hWnd, nIndex, dwNewLong);
}

BOOL WINAPI fake_EnableWindow(HWND hWnd, BOOL bEnable)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if (g_ddProxy && g_ddProxy->hwnd == hWnd)
    {
        return FALSE;
    }

    return real_EnableWindow(hWnd, bEnable);
}

int WINAPI fake_GetDeviceCaps(HDC hdc, int index)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if (g_ddProxy && g_ddProxy->bpp && index == BITSPIXEL)
    {
        return g_ddProxy->bpp;
    }

    return real_GetDeviceCaps(hdc, index);
}

HMODULE WINAPI fake_LoadLibraryA(LPCSTR lpLibFileName)
{
    winapi_dprintf("--> %s(lib=%s)\n", __FUNCTION__, lpLibFileName);
    if (strcmp(lpLibFileName, "ddraw.dll") == 0) {
        uintptr_t enumerateAddr = GetProcAddress(g_patched_ddraw_module, "DirectDrawEnumerateA");
        winapi_dprintf("DirectDrawEnumerateA: official address=%02X\n", enumerateAddr);

        winapi_dprintf("<-- %s: returning patched module\n", __FUNCTION__);

        return g_patched_ddraw_module;
    }

    HMODULE hmod = real_LoadLibraryA(lpLibFileName);

    hook_init();

    winapi_dprintf("<-- %s: returning real module\n", __FUNCTION__);
    return hmod;
}

HMODULE WINAPI fake_LoadLibraryW(LPCWSTR lpLibFileName)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    HMODULE hmod = real_LoadLibraryW(lpLibFileName);

    hook_init();

    return hmod;
}

HMODULE WINAPI fake_LoadLibraryExA(LPCSTR lpLibFileName, HANDLE hFile, DWORD dwFlags)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    HMODULE hmod = real_LoadLibraryExA(lpLibFileName, hFile, dwFlags);

    hook_init();

    return hmod;
}

HMODULE WINAPI fake_LoadLibraryExW(LPCWSTR lpLibFileName, HANDLE hFile, DWORD dwFlags)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    HMODULE hmod = real_LoadLibraryExW(lpLibFileName, hFile, dwFlags);

    hook_init();

    return hmod;
}

BOOL WINAPI fake_DestroyWindow(HWND hWnd)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    BOOL result = real_DestroyWindow(hWnd);

    if (g_ddProxy && g_ddProxy->hwnd != hWnd && g_ddProxy->bnet_active)
    {
        RedrawWindow(NULL, NULL, NULL, RDW_ERASE | RDW_INVALIDATE | RDW_ALLCHILDREN);

        if (!FindWindowEx(HWND_DESKTOP, NULL, "SDlgDialog", NULL))
        {
            g_ddProxy->bnet_active = FALSE;
            SetFocus(g_ddProxy->hwnd);
            mouse_lock();

            if (g_ddProxy->windowed)
            {
                g_ddProxy->bnet_pos.x = g_ddProxy->bnet_pos.y = 0;
                real_ClientToScreen(g_ddProxy->hwnd, &g_ddProxy->bnet_pos);

                if (!g_ddProxy->bnet_was_upscaled)
                {
                    int width = g_ddProxy->bnet_win_rect.right - g_ddProxy->bnet_win_rect.left;
                    int height = g_ddProxy->bnet_win_rect.bottom - g_ddProxy->bnet_win_rect.top;

                    UINT flags = width != g_ddProxy->width || height != g_ddProxy->height ? 0 : SWP_NOMOVE;

                    util_set_window_rect(g_ddProxy->bnet_win_rect.left, g_ddProxy->bnet_win_rect.top, width, height, flags);
                }

                g_ddProxy->fullscreen = g_ddProxy->bnet_was_upscaled;

                SetTimer(g_ddProxy->hwnd, IDT_TIMER_LEAVE_BNET, 1000, (TIMERPROC)NULL);

                g_ddProxy->resizable = TRUE;
            }
        }
    }

    return result;
}

HWND WINAPI fake_CreateWindowExA(
    DWORD dwExStyle, LPCSTR lpClassName, LPCSTR lpWindowName, DWORD dwStyle, int X, int Y,
    int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam)
{
    winapi_dprintf("--> %s()\n", __FUNCTION__);

    if (lpClassName && _strcmpi(lpClassName, "SDlgDialog") == 0 && g_ddProxy)
    {
        if (!g_ddProxy->bnet_active)
        {
            g_ddProxy->bnet_was_upscaled = g_ddProxy->fullscreen;
            g_ddProxy->fullscreen = FALSE;

            if (!g_ddProxy->windowed && !g_ddProxy->bnet_was_fullscreen)
            {
                int ws = g_config.window_state;
                util_toggle_fullscreen();
                g_config.window_state = ws;
                g_ddProxy->bnet_was_fullscreen = TRUE;
            }

            real_GetClientRect(g_ddProxy->hwnd, &g_ddProxy->bnet_win_rect);
            MapWindowPoints(g_ddProxy->hwnd, HWND_DESKTOP, (LPPOINT)&g_ddProxy->bnet_win_rect, 2);

            int width = g_ddProxy->bnet_win_rect.right - g_ddProxy->bnet_win_rect.left;
            int height = g_ddProxy->bnet_win_rect.bottom - g_ddProxy->bnet_win_rect.top;

            int x = g_ddProxy->bnet_pos.x || g_ddProxy->bnet_pos.y ? g_ddProxy->bnet_pos.x : -32000;
            int y = g_ddProxy->bnet_pos.x || g_ddProxy->bnet_pos.y ? g_ddProxy->bnet_pos.y : -32000;

            UINT flags = width != g_ddProxy->width || height != g_ddProxy->height ? 0 : SWP_NOMOVE;

            util_set_window_rect(x, y, g_ddProxy->width, g_ddProxy->height, flags);
            g_ddProxy->resizable = FALSE;

            g_ddProxy->bnet_active = TRUE;
            mouse_unlock();
        }

        POINT pt = { 0, 0 };
        real_ClientToScreen(g_ddProxy->hwnd, &pt);

        X += pt.x;
        Y += pt.y;

        dwStyle |= WS_CLIPCHILDREN;
    }

    return real_CreateWindowExA(
        dwExStyle,
        lpClassName,
        lpWindowName,
        dwStyle,
        X,
        Y,
        nWidth,
        nHeight,
        hWndParent,
        hMenu,
        hInstance,
        lpParam);
}

HFONT WINAPI fake_CreateFontIndirectA(const LOGFONTA *lplf) {
    return real_CreateFontIndirectA(lplf);
}

int WINAPI fake_DrawTextA(HDC    hdc, LPCSTR lpchText, int    cchText, LPRECT lprc, UINT   format) {
    return real_DrawTextA(hdc, lpchText, cchText, lprc, format);
}
