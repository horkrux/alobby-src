#include "stdafx.h"
#include <sentry.h>
#include <asmjit/x86.h>
#include "hacklib/Main.h"
#include <cstdio>
#include <thread>
#include <regex>
#include <TlHelp32.h>
#include "JoinGameTask.h"
#include "HostGameTask.h"
#include <debug_console.h>
#include <win_utils.h>
#include "PreGameLobbyPatch.h"
#include "PatchGameSettings.h"
#include "StatsGraphPatch.h"
#include "IpcServer.h"
#include "IpcEventDispatcher.h"
#include "StatsScreenPatch.h"
#include "ObserverModePatch.h"
#include "GameRulesPatch.h"
#include "ChatPatch.h"
#include "SetupSettingsPatch.h"
#include "DdrawPatch.h"
#include "DsoundPatch.h"
#include "QuitSeFix.h"
#include "GameHardeningPatch.h"
#include "FreezeFix.h"
#include "SaveColorFix.h"
#include "dll_config.h"
#include "AutoSavePatch.h"
#include "GuiPatch.h"
#include "RandomMapPatch.h"
#include "PatchManager.h"
#include "GameSettingsManager.h"
#include "DisableAiPatch.h"
#include "MainScreenPatch.h"
#include "PersistUnitGroupsPatch.h"
#include "EnhancedWidescreenFix.h"

#define CE_VERSION 301
#define PATCH_LEVEL   0
#define S3_VANILLA_PATCH_LEVEL 10400913762886703416UL

using namespace asmjit;

void __fastcall multiplayer_select_game_session(connection_dialog* conDialog, void* notUsed1, int notUsed2, char sessionIndex);
void* __fastcall dialog_create_main(void *mainMenuDialog, void* notUsed, void *graphicsData, lp_menu_draw_thread *menuDrawThread, int a4);
void on_after_basic_init(hl::CpuContext* cpuContext);
void __fastcall init_structured_exception_message(void* ex, void* notUsed, str_struct *message);

class S3Main : public hl::Main, public AfterGameInitListener
{
private:
    Config* m_config;
    HANDLE m_configPipe;
    IpcServer ipcServer;
    IpcEventDispatcher ipcEventDispatcher;
    std::thread ipcServerThread;

    S3EventDispatcher m_s3EventDispatcher;
    PatchManager m_patchManager;
    GameSettingsManager m_gameSettingsManager;
    I18n m_i18n;

    hl::Patch levelPatch;
    FunctionHook ensureCompatibleVersionHook, versionDisplayHook, initStructuredExceptionMessageHook;
    DetourHooker hooker;
    uint32_t m_settlersVersionVanilla;
    uint32_t* m_settlersVersion;
    bool m_isVanillaVersion = false;
    bool m_guiActive = false;

    ObserverMode observerMode;

public:
    S3Main(Config* config, HANDLE configPipe):
        m_config(config),
        m_configPipe(configPipe),
        ipcServer(m_config->getIpcPipeName()),
        m_patchManager(&m_s3EventDispatcher),
        m_gameSettingsManager(&m_patchManager, &m_i18n, m_config->getDefaultSettings()),
        ipcEventDispatcher(&ipcServer, &m_gameSettingsManager),
        m_settlersVersion(reinterpret_cast<uint32_t*>(Constants::S3_EXE + 0x2970F0))
    {
        ensureCompatibleVersionHook.hook(reinterpret_cast<void**>(&s3_select_session), reinterpret_cast<void*>(multiplayer_select_game_session));
        initStructuredExceptionMessageHook.hook(reinterpret_cast<void**>(&s3_init_structured_exception_message), reinterpret_cast<void*>(init_structured_exception_message));
        hooker.hookDetour(Constants::S3_EXE + 0x98629, 5, on_after_basic_init);
        m_s3EventDispatcher.AddListener(&m_gameSettingsManager);
        m_s3EventDispatcher.AddListener(&ipcEventDispatcher);
        m_s3EventDispatcher.AddListener(this);
    }

    ~S3Main() {
        shutdownIpcServer();
        sentry_shutdown();
        delete m_config;
    }

    bool init() override
    {
        if (is_compat_mode()) {
            hl::MsgBox("S3 CE: Compatibility Mode not supported", "Please turn off compatibility mode and try again.");
            exit(1);
        }

        setupDebugContext();

        m_patchManager.enable(new DdrawPatch(m_config));
        m_patchManager.enable(new DsoundPatch(m_config));
        m_patchManager.enable(new QuitSeFix);
        m_patchManager.enable(new GameHardeningPatch(&m_isVanillaVersion));
        m_patchManager.enable(new SaveColorFix);
        m_patchManager.enable(new FreezeFix());
        m_patchManager.enable(new DisableAiPatch(&m_isVanillaVersion));
        m_patchManager.enable(new StatsScreenPatch(&ipcServer, m_config->getScreenDirectory()));
        m_patchManager.enable(new StatsGraphPatch(&m_i18n, &m_isVanillaVersion, m_settlersVersion));
        m_patchManager.enable(new MainScreenPatch(m_config));
        if (!m_config->areBalancingPatchesDisabled()) {
            m_patchManager.enable(new PreGameLobbyPatch(&m_gameSettingsManager, &m_i18n, m_config, &m_guiActive));
            m_patchManager.enable(new GameRulesPatch(&m_gameSettingsManager));
            m_patchManager.enable(new RandomMapPatch(&m_gameSettingsManager));
        } else {
            m_gameSettingsManager.Update(0);
        }
        m_patchManager.enable(new SetupSettingsPatch(&m_i18n, m_config));
        m_patchManager.enable(new PersistUnitGroupsPatch);

        if (m_config->isGuiDisabled()) {
            m_guiActive = false;
            m_gameSettingsManager.Update(0);
        } else {
            m_patchManager.enable(new GuiPatch(&m_s3EventDispatcher, &m_guiActive, &m_gameSettingsManager));
        }
        m_patchManager.enable(new AutoSavePatch(&m_i18n, &m_isVanillaVersion, m_config->isAutoSaveOn()));

        if (m_config->isClassicOnly()) {
            m_gameSettingsManager.Update(0);
        }

        if (m_config->useImprovedChat()) {
            m_patchManager.enable(new ChatPatch(&observerMode, m_config));
            m_patchManager.enable(new ObserverModePatch(&observerMode));
        }
        m_patchManager.enable(new EnhancedWidescreenFix());
        // Needs further testing/debugging as it did not seem to make a difference.
//        patchManager.enable(new BuilderPathFinding);

        try {
            // Save the Vanilla version, but adjust to avoid a Vanilla game instance joining a non-Vanilla game.
            m_settlersVersionVanilla = *m_settlersVersion;
            *m_settlersVersion = CE_VERSION;
            setPatchLevel(PATCH_LEVEL);
            versionDisplayHook.hook(reinterpret_cast<void**>(&s3_create_main_menu_dialog), reinterpret_cast<void*>(dialog_create_main));

            if (m_config->shouldJoinGame()) {
                auto joinData = m_config->getJoinGameData();
                dprintf("Setting up auto join (nickname=%s, ipAddr=%s)\n", joinData->nickname, joinData->ipAddr);

                auto user = sentry_value_new_object();
                sentry_value_set_by_key(user, "username", sentry_value_new_string(joinData->nickname));
                sentry_set_user(user);

                auto joinGameTask = new JoinGameTask(&m_i18n, joinData->nickname, joinData->ipAddr);
                m_s3EventDispatcher.AddListener(joinGameTask);
            } else if (m_config->shouldHostGame()) {
                auto hostData = m_config->getHostGameData();
                dprintf("Hosting game (map name=%s)\n\n", hostData->mapName);

                auto user = sentry_value_new_object();
                sentry_value_set_by_key(user, "username", sentry_value_new_string(hostData->nickname));
                sentry_set_user(user);

                auto task = new HostGameTask(&m_i18n, hostData->nickname, hostData->mapName, hostData->mapCategory, hostData->mapSize, hostData->mirrorType, hostData->useRandomPositions, hostData->goodsSetting);
                m_s3EventDispatcher.AddListener(task);
            } else {
//                tasks.push_back(new HostGameTask("Test1234", "", MAP_CATEGORY_RANDOM, MAP_SIZE_704, MIRROR_TYPE_BOTH_AXES, true, GOODS_SETTING_HIGH));
//                auto task = new HostGameTask("Test1234", "10er_BonumLudum.map", MAP_CATEGORY_USER, 0, 0, true, GOODS_SETTING_LOW);
//                tasks.push_back(new HostGameTask("Test1234", "80a8dcb0-d21a-4b10-8e1c-85244c60285c.mps", MAP_CATEGORY_SAVE, 0, 0, true, GOODS_SETTING_MAP_DEFAULT));
//                tasks.push_back(new JoinGameTask("Test1234", "127.0.0.1"));
//                m_s3EventDispatcher.AddListener(task);
                dprintf("No start task defined.\n");
            }

            ipcServerThread = std::move(std::thread([&]{ ipcServer.Run(); }));

            sendInitCompleted();
#ifndef NDEBUG
            hl::MsgBox("S3 Loaded", "Click to continue normal S3 launch.");
#endif

            resumeMainThread();

            return true;
        } catch (...) {
            hl::MsgBox("S3 Init Error", "S3 could not be loaded/patched.");
            exit(1);
        }
    }

    bool step() override
    {
        if (ipcServer.IsTerminated()) {
            // If the pipe was requested, but the IPC server failed, show an error message and exit the game.
            if (m_config->isIpcPipeRequested()) {
                hl::MsgBox("S3 IPC Error", "IPC-Thread terminated prematurely.\n");

                return false;
            }
        }

        std::this_thread::sleep_for(std::chrono::seconds (1));

        return true;
    }

    void shutdown() override {
        shutdownIpcServer();
    }

    bool ensureVersionCompatibility(uint32_t settlersVersion, uint64_t patchLevel)
    {
        // When we join a game of the Vanilla (non-launcher) version, adjust accordingly.
        if (settlersVersion == m_settlersVersionVanilla) {
            *m_settlersVersion = settlersVersion;

            if (patchLevel == S3_VANILLA_PATCH_LEVEL) {
                setPatchLevel(S3_VANILLA_PATCH_LEVEL);

                // Turn-off all new game settings.
                m_gameSettingsManager.Update(0);
                m_isVanillaVersion = true;

                return true;
            }

            dprintf("Vanilla Patch-Level Changed (expected=%llu, actual=%llu)\n", S3_VANILLA_PATCH_LEVEL, patchLevel);

            // Something in the Vanilla version changed. Let's be safe and not adjust the patch-level (causing
            // a join error that patch-levels are not the same).
            return false;
        }

        // In the launcher, we currently do not use the patch-level and just compare the version number.
        return settlersVersion == *m_settlersVersion;
    }

    void onAfterBasicInit() {
        typedef struct {
            BYTE gap[0x28];
            DWORD language;
        } Settings;

        Settings* settings = reinterpret_cast<Settings*>(Constants::S3_EXE + 0x3A7AF0);
        dprintf("Active Language: %lu\n", settings->language);
        m_i18n.setActiveLanguage(static_cast<Language>(settings->language));
    }

    void OnAfterGameInit(logic_thread* logicThread) override {
        setupDebugContext();
    }

    std::string getVersionText() {
        char versionText[50];
        sprintf(versionText, "Community Edition %d.%02d by JHNP727", CE_VERSION / 100, CE_VERSION % 100);

        return versionText;
    }

private:
    void setupDebugContext() {
        sentry_value_t displayInfo = sentry_value_new_object();
        sentry_value_set_by_key(displayInfo, "screen_width", sentry_value_new_int32(GetSystemMetrics(SM_CXSCREEN)));
        sentry_value_set_by_key(displayInfo, "screen_height", sentry_value_new_int32(GetSystemMetrics(SM_CYSCREEN)));
        sentry_value_set_by_key(displayInfo, "render_width", sentry_value_new_int32(m_config->getDisplayWidth()));
        sentry_value_set_by_key(displayInfo, "render_height", sentry_value_new_int32(m_config->getDisplayHeight()));
        sentry_set_context("displayInfo", displayInfo);

        sentry_value_t gameSettings = sentry_value_new_object();
        sentry_value_set_by_key(gameSettings, "rules", sentry_value_new_string(m_gameSettingsManager.getRulesDescription().c_str()));
        sentry_value_set_by_key(gameSettings, "version", sentry_value_new_int32(*m_settlersVersion));
        sentry_set_context("gameSettings", gameSettings);
    }

    void shutdownIpcServer() {
        ipcServer.RequestTermination();
        ipcServerThread.join();
    }

    void setPatchLevel(uint64_t level) {
        uintptr_t pos = Constants::S3_EXE + 0xB417;
        uint64_t* gamePatchLevel = reinterpret_cast<uint64_t*>(Constants::S3_EXE + 0x3A8000);
        *gamePatchLevel = level;

        Environment env;
        env.setArch(Environment::kArchX86);

        uint32_t level1 = level >> 32;
        uint32_t level2 = (uint32_t)level;

        CodeHolder code;
        code.init(env, pos);
        x86::Assembler a(&code);
        a.mov(x86::ptr(Constants::S3_EXE + 0x3A8000, 4), level1);
        a.mov(x86::ptr(Constants::S3_EXE + 0x3A8004, 4), level2);
        a.mov(x86::eax, x86::ptr(Constants::S3_EXE + 0x003A7874, 4));

        CodeBuffer& buf = code.textSection()->buffer();
        while (buf.size() < 26) {
            a.nop();
        }
        levelPatch.apply(pos, (char*)buf.data(), buf.size());
    }

    void sendInitCompleted() {
        const char response[3] = "OK";
        DWORD cbWritten;
        if (!WriteFile(m_configPipe, response, 2, &cbWritten, NULL)) {
            dprintf("failed to write pipe\n");
        }

        CloseHandle(m_configPipe);
    }

    void resumeMainThread() {
        HANDLE hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
        if (!hThreadSnap) {
            return;
        }

        DWORD ownProcessId = GetCurrentProcessId();

        THREADENTRY32 te;
        te.dwSize = sizeof(THREADENTRY32);
        if (Thread32First(hThreadSnap, &te)) {
            do
            {
                if (te.th32OwnerProcessID == ownProcessId) {
                    HANDLE hThread = OpenThread(THREAD_ALL_ACCESS | THREAD_GET_CONTEXT, FALSE, te.th32ThreadID);
                    if (hThread) {
                        ResumeThread(hThread);
                        CloseHandle(hThread);
                    }
                }
            } while (Thread32Next(hThreadSnap, &te));
        }

        CloseHandle(hThreadSnap);
    }
};

class S3MainInit : public hl::StaticInitImpl {
public:
    S3Main* getInstance() {
        return static_cast<S3Main*>(m_pMain);
    }

protected:
    std::unique_ptr<hl::Main> makeMain() const override {
#ifndef NDEBUG
        FILE* fp;
        AllocConsole();
        SetConsoleTitle("S3-Patches Debug Console");
        freopen_s(&fp, "CONIN$", "r", stdin);
        freopen_s(&fp, "CONOUT$", "w", stdout);
        freopen_s(&fp, "CONOUT$", "w", stderr);
#endif

        dll_config rawConfig;

        std::string pipeName = "\\\\.\\pipe\\s3-launcher-" + std::to_string(GetCurrentProcessId());
        HANDLE hPipe = CreateFile(
            TEXT(pipeName.c_str()),
            GENERIC_READ | GENERIC_WRITE,
            0,
            NULL,
            OPEN_EXISTING,
            0,
            NULL
        );
        if (hPipe == INVALID_HANDLE_VALUE) {
            dprintf("failed to connect to pipe\n");

            return NULL;
        }

        DWORD dwMode = PIPE_READMODE_MESSAGE;
        if (!SetNamedPipeHandleState(hPipe, &dwMode, NULL, NULL)) {
            dprintf("failed to set handle states\n");

            return NULL;
        }

        DWORD cbRead;
        if (!ReadFile(hPipe, &rawConfig, sizeof(dll_config), &cbRead, NULL)) {
            dprintf("failed to read pipe\n");

            return NULL;
        }

        auto cfg = new Config();
        cfg->init(&rawConfig);

        if (cfg->isErrorTrackingEnabled()) {
            auto options = sentry_options_new();
            sentry_options_set_dsn(options, "https://91e7200ac32046599846dd4d73a3c888@sentry.s3-ce.com/2");
            if (!sentry_init(options)) {
                dprintf("Sentry initialization failed\n");
            }
        }

        return std::unique_ptr<S3Main>(new S3Main(cfg, hPipe));
    }
};

S3MainInit g_mainInit;

void* __fastcall dialog_create_main(void *mainMenuDialog, void* notUsed, void *graphicsData, lp_menu_draw_thread *menuDrawThread, int a4) {
    typedef struct {
        BYTE gap[0x84C];
        DWORD versionTextStart;
    } dlg;

    str_struct versionTextStr;
    s3_str_init(&versionTextStr, g_mainInit.getInstance()->getVersionText().c_str());

    auto dialog = reinterpret_cast<dlg*>(s3_create_main_menu_dialog(mainMenuDialog, graphicsData, menuDrawThread, a4));
    s3_set_static_text_value(&dialog->versionTextStart, &versionTextStr);

    return dialog;
}

void __fastcall multiplayer_select_game_session(connection_dialog* conDialog, void* notUsed1, int notUsed2, char sessionIndex) {
    if (sessionIndex >= 0 && sessionIndex < conDialog->runningSessions.numSessions) {
        auto session = &conDialog->runningSessions.sessions[sessionIndex];
        if (!g_mainInit.getInstance()->ensureVersionCompatibility(session->settlersVersion, session->patchLevel)) {
            str_struct errorMessage;
            s3_str_init(&errorMessage, "Your Settlers version is not compatible with this game session.\n\nMake sure you run the newest launcher version.");
            s3_show_message_box(conDialog->menuDrawThread, &errorMessage, MESSAGE_BOX_OK_BUTTON);
            s3_update_menu_screen(conDialog->menuDrawThread, START_MENU_MULTIPLAYER_LAN_BTN);
            return;
        }
    }

    s3_select_session(conDialog, notUsed2, sessionIndex);
}

void on_after_basic_init(hl::CpuContext* cpuContext) {
    g_mainInit.getInstance()->onAfterBasicInit();
}

void __fastcall init_structured_exception_message(void* ex, void* notUsed, str_struct *message) {
    s3_init_structured_exception_message(ex, message);

    std::string errorMessage(message->sText);
    errorMessage = std::regex_replace(errorMessage, std::regex("(read|write)\\s+from\\s+[^\\s]+", std::regex_constants::icase), "$1");

    auto event = sentry_value_new_message_event(SENTRY_LEVEL_ERROR, nullptr, errorMessage.c_str());
    sentry_event_value_add_stacktrace(event, nullptr, 0);
    sentry_capture_event(event);
    sentry_shutdown();

    std::string msg("A structured exception occurred and has been logged:\n\n");
    msg.append(errorMessage.c_str());
    msg.append("\n\nClick to close the game.");

    hl::MsgBox("S3 Structured Exception", msg);
    exit(1);
}
