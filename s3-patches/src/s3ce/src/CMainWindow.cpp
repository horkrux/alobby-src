#include "GameLauncher.h"
#include "CMainWindow.h"
#include "constants.h"

#define WINDOW_WIDTH 400
#define WINDOW_HEIGHT 435

#define IDC_GRAPHIC_MODE_DROPDOWN 1
#define IDC_CHANGE_RESOLUTION_CHECKBOX 2
#define IDC_LAUNCH_BUTTON 3
#define IDC_REDUCE_FPS_CHECKBOX 4
#define IDC_HOST_IP_INPUT 5
#define IDC_JOIN_GAME_TITLE 6
#define IDC_NICKNAME_LABEL 7
#define IDC_IP_LABEL 8
#define IDC_NICKNAME_INPUT 9
#define IDC_AUTOSAVE_CHECKBOX 10
#define IDC_GRAPHIC_SETTINGS_TITLE 12
#define IDC_GAME_SETTINGS_TITLE 13
#define IDC_IMPROVED_CHAT 14
#define IDC_DISPLAY_RESOLUTION_DROPDOWN 15

BEGIN_MESSAGE_MAP(CMainWindow, CFrameWnd)
    ON_WM_SHOWWINDOW()
    ON_WM_CTLCOLOR()
    ON_WM_LBUTTONUP()
    ON_CBN_SELCHANGE(IDC_GRAPHIC_MODE_DROPDOWN, &CMainWindow::OnGraphicModeSelect)
    ON_CBN_SELCHANGE(IDC_DISPLAY_RESOLUTION_DROPDOWN, &CMainWindow::OnDisplayResolutionSelect)
    ON_BN_CLICKED(IDC_CHANGE_RESOLUTION_CHECKBOX, &CMainWindow::OnResolutionChangeClicked)
    ON_BN_CLICKED(IDC_LAUNCH_BUTTON, &CMainWindow::OnLaunchClicked)
    ON_BN_CLICKED(IDC_REDUCE_FPS_CHECKBOX, &CMainWindow::OnReduceFpsClicked)
    ON_BN_CLICKED(IDC_AUTOSAVE_CHECKBOX, &CMainWindow::OnAutoSaveClicked)
    ON_BN_CLICKED(IDC_IMPROVED_CHAT, &CMainWindow::OnImprovedChatClicked)
    ON_EN_KILLFOCUS(IDC_NICKNAME_INPUT, &CMainWindow::NicknameInputLooseFocus)
END_MESSAGE_MAP()

CMainWindow::CMainWindow()
{
    Create(NULL, "S3 Community Edition", WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MAXIMIZEBOX,
           CRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT));

    int top = 10;
    int left = 10;
    int right = 370;
    auto app = AfxGetApp();

    graphicSettingsTitle->Create("Graphic Settings", WS_CHILD | WS_VISIBLE, CRect(left, top, right, top + 25), this,
                                 IDC_GRAPHIC_SETTINGS_TITLE);
    top += 25;

    graphicModes.push_back(new GraphicMode(GRAPHIC_MODE_AUTO, "Choose best graphic mode automatically"));
    graphicModes.push_back(new GraphicMode(GRAPHIC_MODE_DIRECTX, "Window Mode using Graphic Card/DirectX"));
    graphicModes.push_back(new GraphicMode(GRAPHIC_MODE_OPENGL, "Window Mode using Graphic Card/OpenGL"));
    graphicModes.push_back(new GraphicMode(GRAPHIC_MODE_GDI, "Window Mode using CPU"));
    graphicModes.push_back(new GraphicMode(GRAPHIC_MODE_DISABLE, "Use Old Graphic Mode"));
    graphicModeDropdown->Create(WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, CRect(left + 3, top, right, top + 30), this,
                                IDC_GRAPHIC_MODE_DROPDOWN);
    top += 30;

    int graphicMode = app->GetProfileIntA("Settings", "GraphicMode", GRAPHIC_MODE_DIRECTX);
    for (auto mode : graphicModes)
    {
        graphicModeDropdown->AddString(mode->m_name);
    }

    int selectedMode = FindGraphicModePosition(graphicMode);
    if (selectedMode < 0)
    {
        selectedMode = 0;
    }
    graphicModeDropdown->SetCurSel(selectedMode);

    displayResolutionDropdown->Create(WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, CRect(left + 3, top, right, top + 30), this, IDC_DISPLAY_RESOLUTION_DROPDOWN);

    displayResolutions.push_back(new DisplayResolution(1024, 768));
    displayResolutions.push_back(new DisplayResolution(1366, 768));
    displayResolutions.push_back(new DisplayResolution(1600, 900));
    displayResolutions.push_back(new DisplayResolution(1920, 1080));

    int index = 0;
    int displayWidth = app->GetProfileIntA("Settings", SETTING_DISPLAY_WIDTH, displayResolutions[0]->m_width);
    int displayHeight = app->GetProfileIntA("Settings", SETTING_DISPLAY_HEIGHT, displayResolutions[0]->m_height);
    for (auto resolution : displayResolutions) {
        displayResolutionDropdown->AddString(resolution->GetDescription().c_str());
        if (resolution->m_width == displayWidth && resolution->m_height == displayHeight) {
            displayResolutionDropdown->SetCurSel(index);
        }
        index++;
    }

    top += 30;

    textureFilterCheckbox->Create("Apply Texture Filter (disable if your game is blurry)",
                                  WS_CHILD | WS_VISIBLE | BS_CHECKBOX, CRect(left + 3, top, right, top + 25), this,
                                  IDC_CHANGE_RESOLUTION_CHECKBOX);
    top += 25;

    reduceFpsCheckbox->Create("Reduce max. FPS (enable if you have lags)", WS_CHILD | WS_VISIBLE | BS_CHECKBOX, CRect(left + 3, top, right, top + 25), this,
                              IDC_REDUCE_FPS_CHECKBOX);
    top += 25;

    top += 15;
    gameSettingsTitle->Create("Game Settings", WS_CHILD | WS_VISIBLE, CRect(left, top, right, top + 20), this, IDC_GAME_SETTINGS_TITLE);
    top += 20;

    autoSaveCheckbox->Create("Auto-Save Multiplayer", WS_CHILD | WS_VISIBLE | BS_CHECKBOX, CRect(left + 3, top, right, top + 25),
                             this, IDC_AUTOSAVE_CHECKBOX);
    autoSaveCheckbox->SetCheck(app->GetProfileIntA("Settings", SETTING_AUTO_SAVE, 1));
    top += 25;

    improvedChat->Create("Improved in-game chat", WS_CHILD | WS_VISIBLE | BS_CHECKBOX,
                                      CRect(left + 3, top, right, top + 25), this, IDC_IMPROVED_CHAT);
    improvedChat->SetCheck(app->GetProfileIntA("Settings", SETTING_IMPROVED_CHAT, 1));
    top += 25;


    top += 15;
    joinGameTitle->Create("Auto-Join Game", WS_CHILD | WS_VISIBLE,
                          CRect(left, top, right, top + 25), this,
                          IDC_JOIN_GAME_TITLE);
    top += 25;

    nicknameLabel->Create("Nickname", WS_CHILD | WS_VISIBLE,
                          CRect(left + 3, top, 70, top + 15), this,
                          IDC_NICKNAME_LABEL);
    nicknameInput->Create(WS_CHILD | WS_VISIBLE | WS_BORDER,
                          CRect(80, top - 2, 370, top + 17), this,
                          IDC_NICKNAME_INPUT);
    nicknameInput->SetWindowTextA(AfxGetApp()->GetProfileStringA("Settings", SETTING_NICKNAME, ""));
    top += 25;

    ipLabel->Create("Host IP", WS_CHILD | WS_VISIBLE,
                    CRect(left + 3, top, 70, top + 15), this, IDC_IP_LABEL);
    hostIpInput->Create(WS_CHILD | WS_VISIBLE | WS_BORDER,
                        CRect(80, top - 2, 370, top + 17), this,
                        IDC_HOST_IP_INPUT);
    top += 20;

    UpdateControlStates();

    top += 15;
    launchButton->Create("Launch S3", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
                         CRect(left, top, right, top + 55), this,
                         IDC_LAUNCH_BUTTON);
    top += 55;
}

int CMainWindow::FindGraphicModePosition(unsigned int graphicMode)
{
    unsigned int pos = 0;
    for (auto mode : graphicModes)
    {
        if (mode->m_id == graphicMode)
        {
            return pos;
        }

        pos += 1;
    }

    return -1;
}

BOOL CMainWindow::DestroyWindow()
{
    WINDOWPLACEMENT wp;
    GetWindowPlacement(&wp);
    AfxGetApp()->WriteProfileBinary("MainWindow", "WP", (LPBYTE)&wp, sizeof(wp));

    return CFrameWnd::DestroyWindow();
}

void CMainWindow::OnShowWindow(BOOL bShow, UINT nStatus)
{
    CFrameWnd::OnShowWindow(bShow, nStatus);

    static bool bOnce = true;

    if (bShow && !IsWindowVisible() && bOnce)
    {
        WINDOWPLACEMENT* lwp;
        UINT nl;

        if (AfxGetApp()->GetProfileBinary("MainWindow", "WP", (LPBYTE*)&lwp, &nl))
        {
            lwp->rcNormalPosition.right = lwp->rcNormalPosition.left + WINDOW_WIDTH;
            lwp->rcNormalPosition.bottom = lwp->rcNormalPosition.top + WINDOW_HEIGHT;
            lwp->showCmd = SW_SHOW;

            SetWindowPlacement(lwp);
            delete[] lwp;
        }
    }

    launchButton->ShowWindow(1);
}

HBRUSH CMainWindow::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
    HBRUSH hbr = CFrameWnd::OnCtlColor(pDC, pWnd, nCtlColor);

    auto dialogId = pWnd->GetDlgCtrlID();
    if (dialogId == IDC_CHANGE_RESOLUTION_CHECKBOX || dialogId == IDC_REDUCE_FPS_CHECKBOX ||
        dialogId == IDC_JOIN_GAME_TITLE || dialogId == IDC_NICKNAME_LABEL || dialogId == IDC_IP_LABEL ||
        dialogId == IDC_AUTOSAVE_CHECKBOX  ||
        dialogId == IDC_IMPROVED_CHAT ||
        dialogId == IDC_GAME_SETTINGS_TITLE || dialogId == IDC_GRAPHIC_SETTINGS_TITLE)
    {
        pDC->SetBkColor(RGB(255, 255, 255));

        return m_whiteBrush;
    }

    return hbr;
}

void CMainWindow::OnGraphicModeSelect()
{
    int graphicMode = graphicModes.at(graphicModeDropdown->GetCurSel())->m_id;
    AfxGetApp()->WriteProfileInt("Settings", SETTING_GRAPHIC_MODE, graphicMode);
    UpdateControlStates();
}

void CMainWindow::OnDisplayResolutionSelect() {
    unsigned int resolutionIndex = displayResolutionDropdown->GetCurSel();
    auto resolution = displayResolutions[resolutionIndex];

    AfxGetApp()->WriteProfileInt("Settings", SETTING_DISPLAY_WIDTH, resolution->m_width);
    AfxGetApp()->WriteProfileInt("Settings", SETTING_DISPLAY_HEIGHT, resolution->m_height);
}

void CMainWindow::NicknameInputLooseFocus()
{
    CString nickname;
    nicknameInput->GetWindowTextA(nickname);
    AfxGetApp()->WriteProfileStringA("Settings", SETTING_NICKNAME, nickname);
}

void CMainWindow::OnReduceFpsClicked()
{
    int newValue = !reduceFpsCheckbox->GetCheck();
    reduceFpsCheckbox->SetCheck(newValue);
    AfxGetApp()->WriteProfileInt("Settings", SETTING_REDUCE_FPS, newValue);
}

void CMainWindow::OnAutoSaveClicked()
{
    int newValue = !autoSaveCheckbox->GetCheck();
    autoSaveCheckbox->SetCheck(newValue);
    AfxGetApp()->WriteProfileInt("Settings", SETTING_AUTO_SAVE, newValue);
}

void CMainWindow::OnImprovedChatClicked()
{
    int newValue = !improvedChat->GetCheck();
    improvedChat->SetCheck(newValue);
    AfxGetApp()->WriteProfileInt("Settings", SETTING_IMPROVED_CHAT, newValue);
}

void CMainWindow::OnResolutionChangeClicked()
{
    int newValue = !textureFilterCheckbox->GetCheck();
    textureFilterCheckbox->SetCheck(newValue);
    AfxGetApp()->WriteProfileInt("Settings", SETTING_APPLY_TEXTURE_FILTER, newValue);
}

void CMainWindow::OnLaunchClicked()
{
    ShowWindow(SW_HIDE);

    dll_config cfg;
    memset(&cfg, 0, sizeof(dll_config));

    int graphicMode = graphicModes.at(graphicModeDropdown->GetCurSel())->m_id;
    cfg.graphicMode = graphicMode;
    cfg.applyTextureFilter = textureFilterCheckbox->GetCheck();
    cfg.reduceMaxFps = reduceFpsCheckbox->GetCheck();
    cfg.autoSave = autoSaveCheckbox->GetCheck();
    cfg.improvedChat = improvedChat->GetCheck();

    auto resolution = displayResolutions[displayResolutionDropdown->GetCurSel()];
    cfg.displayWidth = resolution->m_width;
    cfg.displayHeight = resolution->m_height;

    if (hostIpInput->GetWindowTextLengthA() > 0)
    {
        cfg.joinGameData.enabled = true;

        CString hostIpUnicode;
        hostIpInput->GetWindowTextA(hostIpUnicode);

        CStringA hostIpChar(hostIpUnicode);
        const char* hostIpCharValue = (const char*)hostIpChar;
        memcpy(&cfg.joinGameData.ipAddr, hostIpCharValue, std::min(hostIpChar.GetLength(), MAX_IP_ADDR_LEN));

        CString nicknameUnicode;
        nicknameInput->GetWindowTextA(nicknameUnicode);
        CStringA nicknameChar = CStringA(nicknameUnicode);
        const char* nicknameValue = (const char*)nicknameChar;
        memcpy(&cfg.joinGameData.nickname, nicknameValue, std::min(nicknameChar.GetLength(), MAX_NICKNAME_LEN));
    }

    GameLauncher launcher;
    if (launcher.launch(&cfg) > 0) {
        PostQuitMessage(0);
    } else {
        ShowWindow(SW_SHOW);
    }
}

void CMainWindow::UpdateControlStates()
{
    unsigned int graphicMode = graphicModes.at(graphicModeDropdown->GetCurSel())->m_id;

    bool isSupported = graphicMode == GRAPHIC_MODE_DIRECTX || graphicMode == GRAPHIC_MODE_OPENGL;
    textureFilterCheckbox->EnableWindow(isSupported);
    reduceFpsCheckbox->EnableWindow(isSupported);

    if (!isSupported) {
        textureFilterCheckbox->SetCheck(0);
        reduceFpsCheckbox->SetCheck(0);
    } else {
        int applyTextureFilter = AfxGetApp()->GetProfileIntA("Settings", SETTING_APPLY_TEXTURE_FILTER, 1);
        textureFilterCheckbox->SetCheck(applyTextureFilter);

        int reduceFps = AfxGetApp()->GetProfileIntA("Settings", SETTING_REDUCE_FPS, 0);
        reduceFpsCheckbox->SetCheck(reduceFps);
    }
}
