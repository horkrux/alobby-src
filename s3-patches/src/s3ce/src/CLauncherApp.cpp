#include "CLauncherApp.h"
#include "CMainWindow.h"
#include "utils.h"
#include <hacklib/MessageBox.h>
#include <dll_config.h>
#include <GameLauncher.h>
#include <constants.h>

struct CliOptionsException : public std::exception {
    std::string m_message;

    CliOptionsException(std::string message): m_message(message) {}

    const char* what() const throw () {
        return m_message.c_str();
    }
};

BOOL CLauncherApp::InitInstance()
{
    CWinApp::InitInstance();

    auto isWine = GetProcAddress(GetModuleHandleA("ntdll.dll"), "wine_get_version") != 0;
    if (!isWine && !is_elevated()) {
        hl::MsgBox("S3 Community Edition: Admin Privileges Required", "The launcher must run with admin privileges.");

        return FALSE;
    }

    std::string cmdLine(" ");
    cmdLine.append(m_lpCmdLine);
    cmdLine.append(" ");

#ifdef ENABLE_AUTO_UPDATER
#ifdef NDEBUG
    unsigned long parentProcessId = ParseULongCliFlag(cmdLine, "update");
    AutoUpdater updater(parentProcessId);
    if (!updater.checkForUpdates()) {
        return FALSE;
    }
#endif
#endif

    SetRegistryKey("S3 Community Edition");

    try {
        auto hostIpAddr = ParseStringCliFlag(cmdLine, "join");
        if (!hostIpAddr.empty() || cmdLine.find(" -launch") != std::string::npos || cmdLine.find("-host") != std::string::npos) {
            bool shouldWaitForExit = cmdLine.find(" -wait") != std::string::npos;

            return LaunchGame(cmdLine, hostIpAddr, shouldWaitForExit);
        }
    } catch (CliOptionsException& e) {
        hl::MsgBox("S3 Community Edition: Invalid Start Options", e.what());

        return FALSE;
    }

    CMainWindow* mainWindow = new CMainWindow();
    m_pMainWnd = mainWindow;

    NONCLIENTMETRICS ncm;
    ncm.cbSize = sizeof(ncm);
    SystemParametersInfo(SPI_GETNONCLIENTMETRICS, ncm.cbSize, &ncm, 0);
    LOGFONT lfDlgFont = ncm.lfMessageFont;
    auto font = CreateFontIndirect(&lfDlgFont);
    mainWindow->SendMessageToDescendants(WM_SETFONT, (WPARAM)font, MAKELPARAM(FALSE, 0), TRUE);

    mainWindow->ShowWindow(m_nCmdShow);
    mainWindow->UpdateWindow();

    return TRUE;
}

bool CLauncherApp::HasCliFlag(std::string cmdLine, std::string flagName) {
    auto pos = cmdLine.find(" -" + flagName + " ");

    return pos != std::string::npos;
}

bool CLauncherApp::ParseBoolCliFlag(std::string cmdLine, std::string flagName, bool defaultValue) {
    auto strValue = ParseStringCliFlag(cmdLine, flagName);
    if (strValue.empty()) {
        return defaultValue;
    }

    return strValue.compare("true") == 0 || strValue.compare("on") == 0 || strValue.compare("1") == 0;
}

long CLauncherApp::ParseLongCliFlag(std::string cmdLine, std::string flagName, long defaultValue) {
    auto strValue = ParseStringCliFlag(cmdLine, flagName);
    if (strValue.empty()) {
        return defaultValue;
    }

    return std::stol(strValue, nullptr, 10);
}

std::string CLauncherApp::ParseStringCliFlag(std::string cmdLine, std::string flagName, std::string defaultValue) {
    auto pos = cmdLine.find(" -" + flagName + " ");
    if (pos == std::string::npos) {
        return defaultValue;
    }

    // We have rudimentary support for argument parsing. We support values that contain strings and are enclosed in quotes (single or double quotes).
    // However, we do not support escape sequences inside those values like escaped double quotes. If we need something more complex, we
    // should consider using an existing library instead of re-inventing the wheel.
    auto startPos = pos + flagName.length() + 3;
    auto startChar = cmdLine.at(startPos);
    int endPos;
    if (startChar == '"') {
        endPos = cmdLine.find("\"", startPos + 1);
    } else if (startChar == '\'') {
        endPos = cmdLine.find("'", startPos + 1);
    } else {
        endPos = cmdLine.find(" ", startPos);
    }

    if (endPos == std::string::npos) {
        if (startChar == '"' || startChar == '\'') {
            throw CliOptionsException("Malformed command line value for " + flagName + ": missing terminating character");
        }

        return cmdLine.substr(startPos, cmdLine.length() - startPos);
    } else {
        if (startChar == '"' || startChar == '\'') {
            startPos++;
        }

        return cmdLine.substr(startPos, endPos - startPos);
    }
}

BOOL CLauncherApp::LaunchGame(std::string cmdLine, std::string hostIpAddr, bool shouldWaitForExit) {
    dll_config cfg;
    memset(&cfg, 0, sizeof(dll_config));

    cfg.graphicMode = ParseLongCliFlag(cmdLine, "graphic-mode", GetProfileIntA("Settings", SETTING_GRAPHIC_MODE, -1));
    cfg.displayWidth =
        ParseLongCliFlag(cmdLine, "display-width", GetProfileIntA("Settings", SETTING_DISPLAY_WIDTH, 1366));
    cfg.displayHeight =
        ParseLongCliFlag(cmdLine, "display-height", GetProfileIntA("Settings", SETTING_DISPLAY_HEIGHT, 768));
    cfg.applyTextureFilter = ParseBoolCliFlag(cmdLine, "apply-texture-filter", GetProfileIntA("Settings", SETTING_APPLY_TEXTURE_FILTER, 1));
    cfg.reduceMaxFps = ParseBoolCliFlag(cmdLine, "reduce-fps", GetProfileIntA("Settings", SETTING_REDUCE_FPS, 0));
    cfg.autoSave = ParseBoolCliFlag(cmdLine, "auto-save", GetProfileIntA("Settings", SETTING_AUTO_SAVE, 1));
    cfg.balancingPatchesDisabled = ParseBoolCliFlag(cmdLine, "disable-balancing-patches", GetProfileIntA("Settings", SETTING_BALANCING_PATCHES_DISABLED, 0));
    cfg.improvedChat = ParseBoolCliFlag(cmdLine, "improved-chat", GetProfileIntA("Settings", SETTING_IMPROVED_CHAT, 1));
    cfg.classicOnly = HasCliFlag(cmdLine, "classic-only");
    cfg.disableGui = HasCliFlag(cmdLine, "disable-gui");
    cfg.enableGui = HasCliFlag(cmdLine, "enable-gui");
    cfg.forceDisplayResolution = HasCliFlag(cmdLine, "update-display-resolution");
    cfg.forceWindowMode = HasCliFlag(cmdLine, "window-mode");

    int errorTracking = GetProfileIntA("Settings", SETTING_ERROR_TRACKING, -1);
    if (errorTracking == -1) {
        auto result = MessageBoxA(nullptr, "Enable automatic error reporting so bugs can be fixed?",
                                  "S3 Community Edition: Error Reporting", MB_YESNO);
        WriteProfileInt("Settings", SETTING_ERROR_TRACKING, result == IDYES ? 1 : 0);
        errorTracking = result == IDYES;
    }
    cfg.errorTracking = errorTracking;

    auto pipeName = ParseStringCliFlag(cmdLine, "pipe");
    if (!pipeName.empty()) {
        if (pipeName.length() > MAX_PIPE_NAME_LEN) {
            throw CliOptionsException("The pipe name is too long.");
        }

        strncpy(cfg.pipeName, pipeName.c_str(), sizeof(cfg.pipeName));
    }

    auto screenDirectory = ParseStringCliFlag(cmdLine, "screen-directory");
    if (!screenDirectory.empty()) {
        if (screenDirectory.size() > sizeof(cfg.screenDirectory)) {
            throw CliOptionsException("The screen directory is too long.");
        }

        strncpy(cfg.screenDirectory, screenDirectory.c_str(), sizeof(cfg.screenDirectory));
    }

    if (!hostIpAddr.empty()) {
        cfg.joinGameData.enabled = true;
        strncpy(cfg.joinGameData.ipAddr, hostIpAddr.c_str(), MAX_IP_ADDR_LEN);

        auto nickname = ParseStringCliFlag(cmdLine, "nickname", std::string(GetProfileString("Settings", SETTING_NICKNAME, "")));
        if (!nickname.empty()) {
            strncpy(cfg.joinGameData.nickname, nickname.c_str(), nickname.length());
        }
    } else if (cmdLine.find(" -host") != std::string::npos) {
        cfg.hostGameData.enabled = true;

        auto nickname = ParseStringCliFlag(cmdLine, "nickname", std::string(GetProfileString("Settings", SETTING_NICKNAME, "")));
        if (!nickname.empty()) {
            strncpy(cfg.hostGameData.nickname, nickname.c_str(), nickname.length());
        }

        auto mapCategory = ParseLongCliFlag(cmdLine, "map-category", MAP_CATEGORY_RANDOM);
        cfg.hostGameData.mapCategory = mapCategory;

        auto goodsSetting = ParseLongCliFlag(cmdLine, "goods-setting", GOODS_SETTING_HIGH);
        cfg.hostGameData.goodsSetting = goodsSetting;

        auto randomPositions = ParseBoolCliFlag(cmdLine, "random-positions", true);
        cfg.hostGameData.useRandomPositions = randomPositions;

        if (mapCategory == MAP_CATEGORY_RANDOM) {
            auto mapSize = ParseLongCliFlag(cmdLine, "map-size", MAP_SIZE_768);
            cfg.hostGameData.mapSize = mapSize;

            auto mirrorType = ParseLongCliFlag(cmdLine, "mirror-type", MIRROR_TYPE_NONE);
            cfg.hostGameData.mirrorType = mirrorType;
        } else {
            auto mapName = ParseStringCliFlag(cmdLine, "map-name", "");
            if (!mapName.empty()) {
                strncpy(cfg.hostGameData.mapName, mapName.c_str(), mapName.length());
            }
        }
    }

    GameLauncher launcher(shouldWaitForExit);

    return launcher.launch(&cfg) > 0;
}
