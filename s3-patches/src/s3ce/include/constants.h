#ifndef SIEDLER3_FIXES_CONSTANTS_H
#define SIEDLER3_FIXES_CONSTANTS_H

#define GRAPHIC_MODE_AUTO -1
#define GRAPHIC_MODE_DIRECTX 0
#define GRAPHIC_MODE_OPENGL 1
#define GRAPHIC_MODE_GDI 2
#define GRAPHIC_MODE_DISABLE 3

#define SERVER_NAME L"s3-patches.s3.amazonaws.com"
#define LAUNCHER_TMP_FILE "s3ce.exe.tmp"
#define LAUNCHER_REMOTE_PATH L"s3ce.exe"
#define LAUNCHER_OLD_FILE "s3ce.exe.old"
#define DLL_REMOTE_PATH L"s3.v2.dll"
#define DLL_TMP_FILE "s3.dll.tmp"
#define DLL_OLD_FILE "s3.dll.old"

#define SETTING_SETUP_GAME_SETTINGS "SetupGameSettings"
#define SETTING_AUTO_SAVE "AutoSave"
#define SETTING_BALANCING_PATCHES_DISABLED "BalancingPatchesDisabled"
#define SETTING_GRAPHIC_MODE "GraphicMode"
#define SETTING_DISPLAY_WIDTH "DisplayWidth"
#define SETTING_DISPLAY_HEIGHT "DisplayHeight"
#define SETTING_APPLY_TEXTURE_FILTER "ApplyTextureFilter"
#define SETTING_REDUCE_FPS "ReduceFPS"
#define SETTING_NICKNAME "Nickname"
#define SETTING_IMPROVED_CHAT "ImprovedChat"
#define SETTING_ERROR_TRACKING "ErrorTracking"

#ifndef NDEBUG
#define LAUNCHER_REGULAR_FILE "s3ceDebug.exe"
#define DLL_REGULAR_FILE "s3d.dll"
#else
#define LAUNCHER_REGULAR_FILE "s3ce.exe"
#define DLL_REGULAR_FILE "s3.dll"
#endif

#endif // SIEDLER3_FIXES_CONSTANTS_H
