#ifndef SIEDLER3_FIXES_AUTOUPDATER_H
#define SIEDLER3_FIXES_AUTOUPDATER_H

#include <exception>
#include <string>

struct UpdaterException : public std::exception
{
public:
    std::string title, message;

    UpdaterException(std::string title, std::string message) {
        this->title = title;
        this->message = message;
    }
};

class AutoUpdater
{
public:
    AutoUpdater() {
        AutoUpdater(0);
    }

    AutoUpdater(unsigned long parentProcessId) {
        m_parentLauncherProcessId = parentProcessId;
    }

    bool checkForUpdates();

private:
    std::string computeLocalETag(const char* filename);
    std::string fetchRemoteTag(const wchar_t* remotePath);
    bool shouldUpdate();
    void downloadFile(const wchar_t* remotePath, const char* localPath);

    unsigned long m_parentLauncherProcessId;
    void downloadFile(const wchar_t* remotePath, const char* localPath, std::string expectedETag);
};


#endif // SIEDLER3_FIXES_AUTOUPDATER_H
