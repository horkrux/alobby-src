# s3ce.exe

The s3ce.exe which contains the configuration UI and performs the starting and patching of Settlers3.


## Build Variables

`ENABLE_AUTO_UPDATER` builds with the auto-updater if defined, otherwise auto-updater is not used.

`NDEBUG` when set builds the release version without debug output, etc. 


## Launch Options

`-join IP` immediately joins a game at this IP

`-nickname nickname` sets the nickname to use for starting

`-launch` just launches the game without showing the UI

`-wait` keeps the launcher running for as long as the game is running

`-graphic-mode MODE` sets the graphic mode to use (0=WM DirectX, 1=WM OpenGL, 2=WM GDI, 3=Old Graphic Mode)

`-auto-save VALUE` whether to enable auto-save (to enable use one of true/on/1)

