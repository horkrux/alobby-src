#include "stdafx.h"
#include <hacklib/MessageBox.h>
#include <CLauncherApp.h>

#ifndef NDEBUG
#define S3_DLL "s3d.dll"
#else
#define S3_DLL "s3.dll"
#endif

#pragma comment(linker,"\"/manifestdependency:type='win32' \
    name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
    processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")


CLauncherApp app;