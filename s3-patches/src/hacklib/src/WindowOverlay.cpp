#include "hacklib/WindowOverlay.h"
#include "../../s3/include/debug_console.h"


using namespace hl;


WindowOverlay::WindowOverlay(ModuleHandle hModule) : GfxOverlay(hModule) {}


GfxOverlay::Error WindowOverlay::create()
{
    // get window that belongs to current process
    m_targetWindow = GetTargetWindow();
    if (!m_targetWindow) {
        dprintf("WindowOverlay: Target Window not found\n");
        return Error::Other;
    }

    // adjust metrics
    cbWindowLoop();
    m_isTargetForeground = true;

    char wndTitle[256];
    GetWindowText(m_targetWindow, wndTitle, sizeof(wndTitle));

    return GfxOverlay::create(wndTitle, m_posX, m_posY, m_width, m_height);
}
