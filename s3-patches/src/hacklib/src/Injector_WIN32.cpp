#include "hacklib/Injector.h"
#include <Windows.h>
#include <TlHelp32.h>
#include <Psapi.h>
#include <algorithm>
#include <hacklib/MessageBox.h>
#include <iostream>
#include <chrono>
#include <thread>

#define PIPE_BUFSIZE 4096
#define PIPE_TIMEOUT 5000

//Returns the last Win32 error, in string format. Returns an empty string if there is no error.
std::string GetLastErrorAsString()
{
    //Get the error message ID, if any.
    DWORD errorMessageID = ::GetLastError();
    if(errorMessageID == 0) {
        return std::string(); //No error message has been recorded
    }

    LPSTR messageBuffer = nullptr;

    //Ask Win32 to give us the string version of that message ID.
    //The parameters we pass in, tell Win32 to create the buffer that holds the message for us (because we don't yet know how long the message string will be).
    size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                                 NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

    //Copy the error message into a std::string.
    std::string message(messageBuffer, size);

    //Free the Win32's string's buffer.
    LocalFree(messageBuffer);

    return message;
}

class Injection
{
public:
    Injection(LPVOID dataPtr, SIZE_T dataSize, std::string* error) : m_dataPtr(dataPtr), m_dataSize(dataSize), m_error(error) {}

    ~Injection()
    {
        if (m_remoteMem != NULL)
        {
            VirtualFreeEx(m_hProc, m_remoteMem, 0, MEM_RELEASE);
        }
        if (m_hProc != NULL)
        {
            CloseHandle(m_hProc);
        }
        if (m_hPipe != NULL)
        {
            CloseHandle(m_hPipe);
        }
        if (m_hPipeEvent != NULL)
        {
            CloseHandle(m_hPipeEvent);
        }
    }

    void writeErr(const std::string& error)
    {
        if (m_error)
        {
            *m_error += error;
        }
    }

    bool findFile(const std::string& file)
    {
        LPCSTR path = file.data();

        DWORD written = GetFullPathName(path, MAX_PATH, m_fileName, NULL);
        if (written == 0)
        {
            writeErr("Fatal: Could not get the full library file name\n");
            return false;
        }

        m_fileNameSize = (written + 1) * sizeof(CHAR);

        WIN32_FIND_DATA info;
        HANDLE hFile = FindFirstFile(m_fileName, &info);
        if (hFile == INVALID_HANDLE_VALUE)
        {
            writeErr("Fatal: Could not find the library file\n");
            return false;
        }

        FindClose(hFile);

        written = GetCurrentDirectory(MAX_PATH, m_currentDir);
        if (written == 0)
        {
            writeErr("Fatal: Could not get the current directory\n");
            return false;
        }

        m_currentDirSize = (written + 1) * sizeof(CHAR);

        return true;
    }
    bool findApi()
    {
        HMODULE hMod = GetModuleHandle("kernel32");
        if (hMod == NULL)
        {
            writeErr("Fatal: Could not get kernel32 module handle\n");
            return false;
        }
        m_apiLoadLib = (LPTHREAD_START_ROUTINE)GetProcAddress(hMod, "LoadLibraryA");
        if (m_apiLoadLib == NULL)
        {
            writeErr("Fatal: Could not get LoadLibraryA api address\n");
            return false;
        }
        m_apiSetDllDir = (LPTHREAD_START_ROUTINE)GetProcAddress(hMod, "SetDllDirectoryA");
        if (m_apiLoadLib == NULL)
        {
            writeErr("Fatal: Could not get SetDllDirectoryA api address\n");
            return false;
        }
        return true;
    }
    bool openProc(int pid)
    {
        m_hProc = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_CREATE_THREAD | PROCESS_VM_OPERATION |
                                  PROCESS_VM_WRITE | PROCESS_VM_READ,
                              FALSE, pid);
        if (m_hProc == NULL)
        {
            writeErr("Fatal: Could not open process\n");
            return false;
        }

        // Verify matching bitness of injector and target.
        SYSTEM_INFO info;
        GetNativeSystemInfo(&info);
        if (info.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
        {
            BOOL isWow64;
            if (!IsWow64Process(m_hProc, &isWow64))
            {
                writeErr("Warning: Could not determine bitness of target process\n");
            }
            else
            {
#ifdef ARCH_64BIT
                if (isWow64)
                {
                    writeErr("Fatal: Can not inject into 32-bit process from 64-bit injector\n");
                    return false;
                }
#else
                if (!isWow64)
                {
                    writeErr("Fatal: Can not inject into 64-bit process from 32-bit injector\n");
                    return false;
                }
#endif
            }
        }

        return true;
    }

    bool remoteStoreFileName()
    {
        SIZE_T totalSize = m_fileNameSize + m_currentDirSize;

        m_remoteMem = VirtualAllocEx(m_hProc, NULL, totalSize, MEM_COMMIT, PAGE_READWRITE);
        if (m_remoteMem == NULL)
        {
            writeErr("Fatal: Could not allocate remote memory\n");
            return false;
        }
        SIZE_T written;
        if (!WriteProcessMemory(m_hProc, m_remoteMem, m_fileName, m_fileNameSize, &written) ||
            written != m_fileNameSize)
        {
            writeErr("Fatal: Could not write to remote memory\n");
            return false;
        }
        if (!WriteProcessMemory(m_hProc, (LPVOID)((char*)m_remoteMem + m_fileNameSize), m_currentDir, m_currentDirSize,
                                &written) ||
            written != m_currentDirSize)
        {
            writeErr("Fatal: Could not write to remote memory\n");
        }
        if (m_dataPtr && (!WriteProcessMemory(m_hProc, (LPVOID)((char*)m_remoteMem + m_fileNameSize + m_currentDirSize), m_dataPtr, m_dataSize, &written) || written != m_dataSize)) {
            writeErr("Fatal: Failed to write data ptr to memory\n");
        }
        return true;
    }

    bool prepareConfigPipe(int pid) {
        std::string pipeName("\\\\.\\pipe\\s3-launcher-");
        pipeName.append(std::to_string(pid));

        m_hPipe = CreateNamedPipe(TEXT(pipeName.c_str()),
                                  PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED | FILE_FLAG_FIRST_PIPE_INSTANCE,
                                  PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT | PIPE_REJECT_REMOTE_CLIENTS,
                                  1,
                                  PIPE_BUFSIZE * sizeof(char),
                                  PIPE_BUFSIZE * sizeof(char),
                                  PIPE_TIMEOUT,
                                  NULL);

        if (m_hPipe == INVALID_HANDLE_VALUE) {
            writeErr("Fatal: failed to create pipe (pid=" + std::to_string(pid) + ", error=" + std::to_string(GetLastError())+")\n");
            writeErr(GetLastErrorAsString());
            CloseHandle(m_hPipe);
            m_hPipe = NULL;

            return false;
        }

        return true;
    }

    bool runRemoteThreads()
    {
        if (!runRemoteThread(m_apiSetDllDir, (LPVOID)((char*)m_remoteMem + m_fileNameSize))) {
            writeErr("Fatal: Could not set DLL directory\n");

            return false;
        }
        if (!runRemoteThread(m_apiLoadLib, m_remoteMem)) {
            writeErr("Fatal: Could not load library\n");

            return false;
        }

        return true;
    }

    bool waitForConfirmation() {
        OVERLAPPED activity;
        m_hPipeEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
        activity.hEvent = m_hPipeEvent;
        if (!activity.hEvent) {
            writeErr("Fatal: failed to create system event\n");

            return false;
        }

        BOOL isConnected = ConnectNamedPipe(m_hPipe, &activity);
        if (!isConnected) {
            auto status = GetLastError();
            if (status == ERROR_IO_PENDING) {
                auto waitResult = WaitForSingleObject(activity.hEvent, PIPE_TIMEOUT);
                if (waitResult != WAIT_OBJECT_0) {
                    writeErr("Warn: waiting for pipe connection failed (result=" + std::to_string(waitResult) + ")\n");
                }

                DWORD dwUnused;
                isConnected = GetOverlappedResult(m_hPipe, &activity, &dwUnused, FALSE);
            } else if (status == ERROR_PIPE_CONNECTED) {
                isConnected = true;
            }

            if (!isConnected) {
                writeErr("Fatal: failed to connect pipe\n");
                writeErr(GetLastErrorAsString());

                return false;
            }
        }

        ResetEvent(activity.hEvent);
        DWORD dwWritten;

        activity.Offset = 0;
        activity.OffsetHigh = 0;
        WriteFile(m_hPipe, m_dataPtr, m_dataSize, &dwWritten, &activity);
        if (GetLastError() == ERROR_IO_PENDING) {
            auto result = WaitForSingleObject(activity.hEvent, PIPE_TIMEOUT);
            if (result != WAIT_OBJECT_0) {
                writeErr("Fatal: pipe write failed (result: " + std::to_string(result) + ")\n");

                return false;
            }
            GetOverlappedResult(m_hPipe, &activity, &dwWritten, FALSE);
        }
        if (dwWritten != m_dataSize) {
            writeErr("Fatal: failed to write data (bytes-written=" + std::to_string(dwWritten) + ", bytes-expected=" + std::to_string(m_dataSize)+ ")\n");
        }

        char buffer[2];
        DWORD dwRead;
        activity.Offset = 0;
        activity.OffsetHigh = 0;
        ResetEvent(activity.hEvent);
        ReadFile(m_hPipe, buffer, 2, &dwRead, &activity);
        if (GetLastError() == ERROR_IO_PENDING) {
            auto result = WaitForSingleObject(activity.hEvent, PIPE_TIMEOUT);
            if (result != WAIT_OBJECT_0) {
                writeErr("Fatal: pipe read failed (result: " + std::to_string(result) + ")\n");

                return false;
            }
            GetOverlappedResult(m_hPipe, &activity, &dwRead, FALSE);
        }

        if (dwRead != 2) {
            writeErr("Fatal: failed to read confirmation\n");

            return false;
        }

        if (strncmp(buffer, "OK", 2) != 0) {
            writeErr("Fatal: failed to receive confirmation\n");

            return false;
        }

        DisconnectNamedPipe(m_hPipe);
        CloseHandle(m_hPipe);
        m_hPipe = NULL;
        CloseHandle(m_hPipeEvent);
        m_hPipeEvent = NULL;

        return true;
    }

private:

    bool runRemoteThread(LPTHREAD_START_ROUTINE entryFunc, LPVOID param)
    {
        HANDLE hRemoteThread = CreateRemoteThread(m_hProc, NULL, 0, entryFunc, param, 0, NULL);
        if (hRemoteThread == NULL)
        {
            writeErr("Fatal: Could not create remote thread\n");
            return false;
        }
        if (!SetThreadPriority(hRemoteThread, THREAD_PRIORITY_TIME_CRITICAL))
        {
            writeErr("Warning: Could not set remote thread priority");
        }
        if (WaitForSingleObject(hRemoteThread, 5000) != WAIT_OBJECT_0)
        {
            writeErr("Warning: The remote thread did not respond\n");

            return false;
        }
        else
        {
            DWORD exitCode;
            DWORD returnCode = GetExitCodeThread(hRemoteThread, &exitCode);
            if (!returnCode)
            {
                writeErr("Warning: Could not get remote thread exit code\n");

                return false;
            }
            else
            {
                if (exitCode == 0)
                {
                    writeErr("Warning: Could not confirm that the library was loaded (returnCode: " + std::to_string(returnCode) + ", exit code: " + std::to_string(exitCode) + ")\n");

                    return false;
                }
            }
        }
        CloseHandle(hRemoteThread);
        return true;
    }

private:
    CHAR m_fileName[MAX_PATH + 1];
    CHAR m_currentDir[MAX_PATH + 1];
    SIZE_T m_fileNameSize = 0;
    SIZE_T m_currentDirSize = 0;
    HANDLE m_hProc = NULL;
    LPVOID m_remoteMem = NULL;
    LPTHREAD_START_ROUTINE m_apiLoadLib = NULL;
    LPTHREAD_START_ROUTINE m_apiSetDllDir = NULL;
    LPVOID m_dataPtr;
    HANDLE m_hPipe = NULL;
    HANDLE m_hPipeEvent = NULL;
    SIZE_T m_dataSize;
    std::string* m_error;
};

bool hl::Inject(int pid, const std::string& libFileName, std::string* error) {
    return Inject(pid, libFileName, NULL, 0, error);
}

bool hl::Inject(int pid, const std::string& libFileName, void* dataPtr, unsigned int dataSize, std::string* error)
{
    Injection inj(dataPtr, dataSize, error);

    std::cout << "Locating lib filename..." << std::endl;
    if (!inj.findFile(libFileName)) {
        return false;
    }

    std::cout << "Finding API..." << std::endl;
    if (!inj.findApi()) {
        return false;
    }

    std::cout << "Opening process..." << std::endl;
    if (!inj.openProc(pid)) {
        return false;
    }

    std::cout << "Updating remote memory..." << std::endl;
    if (!inj.remoteStoreFileName()) {
        return false;
    }

    std::cout << "Preparing config..." << std::endl;
    if (!inj.prepareConfigPipe(pid)) {
        return false;
    }

    std::cout << "Starting remote thread..." << std::endl;
    if (!inj.runRemoteThreads()) {
        return false;
    }

    std::cout << "Waiting for start confirmation..." << std::endl;
    if (!inj.waitForConfirmation()) {
        return false;
    }

    std::cout << "Injection completed." << std::endl;

    return true;
}

std::vector<int> hl::GetPIDsByProcName(const std::string& pname)
{
    std::vector<int> result;

    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (hSnapshot == INVALID_HANDLE_VALUE)
        return result;

    PROCESSENTRY32 entry;
    entry.dwSize = sizeof(PROCESSENTRY32);

    if (Process32First(hSnapshot, &entry))
    {
        do
        {
            if (pname == entry.szExeFile)
            {
                result.push_back(entry.th32ProcessID);
            }
        } while (Process32Next(hSnapshot, &entry));
    }

    return result;
}
