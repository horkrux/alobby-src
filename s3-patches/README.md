Settlers 3 Enhancements and Utilities
=====================================
Provides various enhancements, bug fixes, and utilities for Settlers 3.

The different build targets can be found in the src/ folder.

Credits
-------
We use several third-party libraries (some with modifications) which are embedded
in this project:

- asmjit (https://github.com/asmjit/asmjit): for generating machine code
- cnc-ddraw (https://github.com/CnCNet/cnc-ddraw): for DirectDraw proxy implementation
- detours (https://github.com/microsoft/Detours): for function hooking 
- hacklib (https://github.com/rafzi/hacklib): for code patching/injection/hooking related utilities 
- ImGui (https://github.com/ocornut/imgui)
- ImPlot (https://github.com/epezent/implot)
- other ImGui widgets...