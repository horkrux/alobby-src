package net.siedler3.alobby.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class NativeProcessTest
{

    @Test
    public void testNativeProcess()
    {
        //String cmdLine="C:\\Program Files (x86)\\Windows NT\\Accessories\\wordpad.exe";
        String cmdLine="notepad";
        Process p = null;
        Process p2 = null;
        p = NativeProcess.createProcess(cmdLine);
        //try{p = Runtime.getRuntime().exec(cmdLine);} catch (IOException e) {}

        assertEquals(true, ((NativeProcess)p).isForegroundWindow());
        p2 = NativeProcess.createProcess(cmdLine);
        assertEquals(true, ((NativeProcess)p2).isForegroundWindow());
        assertEquals(false, ((NativeProcess)p).isForegroundWindow());
        p2.destroy();
        assertEquals(true, ((NativeProcess)p).isForegroundWindow());
        p.destroy();
    }

}
