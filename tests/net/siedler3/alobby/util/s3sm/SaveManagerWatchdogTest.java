package net.siedler3.alobby.util.s3sm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import net.siedler3.alobby.controlcenter.ALobbyConstants;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SaveManagerWatchdogTest 
{
	
	private File backupDir;
	private File saveDir;
	
    @Before
    public void setUp() throws Exception
    {
    	Path tmp = Files.createTempDirectory("alobby_save");
    	File dir = tmp.toFile();
    	saveDir = new File(dir, "Multi");
    	backupDir = new File(dir, "MultiBackup");
    	saveDir.mkdir();
    	backupDir.mkdir();
    }
    
    @After
    public void tearDown() throws Exception
    {
		deleteFiles(backupDir);
		deleteFiles(saveDir);
    	backupDir.delete();
    	saveDir.delete();
    	saveDir.getParentFile().delete();
    }


	@Test
	public void testSaveBackups() throws InterruptedException 
	{
	    int sleepValue = 200;
	    int coolDown = 50; //without cooldown the Wachdog creates wrong results
	    int sleepTime = 0;
	    
	    SaveManagerWatchdog watchdog = startWatchDog(3, coolDown, sleepTime);
		Thread.sleep(sleepValue);
		String filename = "c2e6878f-1c62-4f04-8f12-d979589f4571.mps";
		writeFile(filename, "01");
		Thread.sleep(sleepValue);
		writeFile(filename, "02");
		Thread.sleep(sleepValue);
		writeFile(filename, "03");
		Thread.sleep(sleepValue);
		
		watchdog.interrupt();
		
		assertEquals("01", readFile(backupDir, filename + ".1"));
		assertEquals("02", readFile(backupDir, filename + ".2"));
		assertEquals("03", readFile(backupDir, filename + ".3"));
		
		deleteFiles(backupDir);
		
		watchdog = startWatchDog(3, coolDown, sleepTime);
		Thread.sleep(sleepValue);
		writeFile(filename, "01");
		Thread.sleep(sleepValue);
		writeFile(filename, "02");
		Thread.sleep(sleepValue);
		writeFile(filename, "03");
		Thread.sleep(sleepValue);
		writeFile(filename, "04");
		Thread.sleep(sleepValue);
		writeFile(filename, "05");
		Thread.sleep(sleepValue);
		
		watchdog.interrupt();
		//assertEquals("03", readFile(backupDir, filename + ".1"));
		//assertEquals("04", readFile(backupDir, filename + ".2"));
		//assertEquals("05", readFile(backupDir, filename + ".3"));
		assertEquals("04", readFile(backupDir, filename + ".1"));
		assertEquals("05", readFile(backupDir, filename + ".2"));
		assertEquals("03", readFile(backupDir, filename + ".3"));
		
		deleteFiles(backupDir);

		watchdog = startWatchDog(5, coolDown, sleepTime);
        Thread.sleep(sleepValue);
        for (int i = 1; i <= 8; ++i)
        {
            writeFile(filename, ""+i);
            Thread.sleep(sleepValue);
        }
        
        watchdog.interrupt();
        assertEquals("6", readFile(backupDir, filename + ".1"));
        assertEquals("7", readFile(backupDir, filename + ".2"));
        assertEquals("8", readFile(backupDir, filename + ".3"));
        assertEquals("4", readFile(backupDir, filename + ".4"));
        assertEquals("5", readFile(backupDir, filename + ".5"));
        
        deleteFiles(backupDir);

		
        watchdog = startWatchDog(5, coolDown, sleepTime);
        Thread.sleep(sleepValue);
        for (int i = 1; i <= 20; ++i)
        {
            writeFile(filename, ""+i);
            Thread.sleep(sleepValue);
        }

        
        watchdog.interrupt();
        assertEquals("16", readFile(backupDir, filename + ".1"));
        assertEquals("17", readFile(backupDir, filename + ".2"));
        assertEquals("18", readFile(backupDir, filename + ".3"));
        assertEquals("19", readFile(backupDir, filename + ".4"));
        assertEquals("20", readFile(backupDir, filename + ".5"));
        
        deleteFiles(backupDir);
		
		
        watchdog = startWatchDog(10, coolDown, sleepTime);
        Thread.sleep(sleepValue);
        for (int i = 1; i <= 12; ++i)
        {
            writeFile(filename, ""+i);
            Thread.sleep(sleepValue);
        }
        
        watchdog.interrupt();
        assertEquals("11", readFile(backupDir, filename + ".1"));
        assertEquals("12", readFile(backupDir, filename + ".2"));
        assertEquals("3", readFile(backupDir, filename + ".3"));
        assertEquals("4", readFile(backupDir, filename + ".4"));
        assertEquals("5", readFile(backupDir, filename + ".5"));
        assertEquals("6", readFile(backupDir, filename + ".6"));
        assertEquals("7", readFile(backupDir, filename + ".7"));
        assertEquals("8", readFile(backupDir, filename + ".8"));
        assertEquals("9", readFile(backupDir, filename + ".9"));
        assertEquals("10", readFile(backupDir, filename + ".10"));
        
        deleteFiles(backupDir);
        

        watchdog = startWatchDog(10, coolDown, sleepTime);
        Thread.sleep(sleepValue);
        for (int i = 1; i <= 23; ++i)
        {
            writeFile(filename, ""+i);
            Thread.sleep(sleepValue);
        }
        
        watchdog.interrupt();
        assertEquals("21", readFile(backupDir, filename + ".1"));
        assertEquals("22", readFile(backupDir, filename + ".2"));
        assertEquals("23", readFile(backupDir, filename + ".3"));
        assertEquals("14", readFile(backupDir, filename + ".4"));
        assertEquals("15", readFile(backupDir, filename + ".5"));
        assertEquals("16", readFile(backupDir, filename + ".6"));
        assertEquals("17", readFile(backupDir, filename + ".7"));
        assertEquals("18", readFile(backupDir, filename + ".8"));
        assertEquals("19", readFile(backupDir, filename + ".9"));
        assertEquals("20", readFile(backupDir, filename + ".10"));
        
        deleteFiles(backupDir);

		
		deleteFiles(saveDir);
	}

    private SaveManagerWatchdog startWatchDog(int numberOfBackups, int coolDown, int sleepTime) throws InterruptedException
    {
        SaveManagerWatchdog watchdog;
        watchdog = createSaveManagerWithoutTimeDifference(saveDir.getAbsolutePath(), 
                                                          backupDir.getAbsolutePath(), 
                                                          numberOfBackups);
        watchdog.coolDownTime = coolDown;
        watchdog.sleepTime = sleepTime;
        watchdog.start();
        //wait due to new check "aktuelleZeit-Sperrzeitpunkt>2000"
        Thread.sleep(2000);
        return watchdog;
    }
	
	private SaveManagerWatchdog createSaveManagerWithoutTimeDifference(String OPfad, String ZPfad, Integer NSicherungsanzahl)
	{
	    return new SaveManagerWatchdog(OPfad, ZPfad, NSicherungsanzahl, "http://siedler3.net/time.php")
	    {
            @Override
            public long getTimeDifference()
            {
                return 0;
            }
	    };
	}
	
	private void writeFile(String name, String content)
	{
		File file = new File(saveDir, name);
		File tmp = new File(saveDir, "temp.mps");
		try
		{
			Files.write(tmp.toPath(), content.getBytes(ALobbyConstants.UTF_8));
			Files.move(tmp.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
			//System.out.println("wrote: " + content + ", " + file.lastModified());
		}
		catch (Exception e)
		{
			fail(e.toString());
		}
	}
	
	private String readFile(File directory, String name)
	{
		File file = new File(directory, name);
		Path path = file.toPath();
		String result = null;
		try
		{
			byte[] content = Files.readAllBytes(path);
			result = new String(content, ALobbyConstants.UTF_8);
		}
		catch (Exception e)
		{
			fail(e.toString());
		}
		return result;
	}
	
	private void deleteFiles(File directory)
	{
		for (File file : directory.listFiles())
		{
			if (file.isFile())
			{
				file.delete();
			}
		}
	}

}
