package net.siedler3.alobby.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.junit.Test;

public class MapBaseInterfaceTest
{

    private static final String INTERFACE_ALOBBY2_PHP = "http://mapbase.siedler3.net/interface.alobby2.php?map=";

    @Test
    public void test() throws Exception
    {
        String search = "schöner";
        String line;

        search = UrlHelper.urlEncode(search);
        //search = "map=" + URLEncoder.encode(search.trim(), "ISO-8859-1");
        URL u = new URL( INTERFACE_ALOBBY2_PHP + search );
        
        URLConnection connection = u.openConnection();
        String charset = UrlHelper.getCharsetFromUrlConnection(connection);
        System.out.println( "" +charset );
        assertEquals("UTF-8", charset);
        
        BufferedReader in = new BufferedReader(
                new InputStreamReader(u.openStream(), charset));

        line = in.readLine();
        in.close();
        System.out.println(line);
        assertTrue(line.startsWith("schöner siedeln ;);http://mapbase.siedler3.net"));
        
        //what a strange name
        search = "448_2Ph,M.plateau";
        assertQueryResult(search, search);
        
        search = "bumsi´s 8er dessert-0";
        assertQueryResult(search, search);
        
        search = "modrige S?mpfe";
        assertQueryResult("modrige", search);
    }
    
    private void assertQueryResult(String search, String response) throws IOException
    {
        search = UrlHelper.urlEncode(search);
        URL u = new URL( INTERFACE_ALOBBY2_PHP + search );
        BufferedReader in = UrlHelper.createBufferedReaderFromUrl(u);
        String line = in.readLine();
        in.close();
        System.out.println(line);
        assertTrue(line.startsWith(response + ";http://mapbase.siedler3.net"));
    }

}
