package net.siedler3.alobby.controlcenter;

import static org.junit.Assert.*;
import org.junit.Test;

public class RandomMapTest
{

    @Test public void testParsing()
    {
        String input;
        S3RandomMap map;
        String path = ALobbyConstants.PATH_RANDOM + "\\"; 
        
        input = S3RandomMap.RANDOM_MAP_SIZE_0 + "_0_0";
        map = new S3RandomMap(input);
        assertEquals(input, map.toString());
        assertTrue(map.parseSettingsFromString(input));
        assertEquals(S3RandomMap.RANDOM_MAP_SIZE_0 + "\\" + S3RandomMap.RANDOM_MAP_MIRROR_0, map.toDisplayString());
        
        input = S3RandomMap.RANDOM_MAP_SIZE_1 + "_1_0";
        map = new S3RandomMap(input);
        assertEquals(input, map.toString());
        assertTrue(map.parseSettingsFromString(input));
        assertEquals(S3RandomMap.RANDOM_MAP_SIZE_1 + "\\" + S3RandomMap.RANDOM_MAP_MIRROR_1, map.toDisplayString());
        
        input = S3RandomMap.RANDOM_MAP_SIZE_2 + "_0_1";
        map = new S3RandomMap(input);
        assertEquals(input, map.toString());
        assertTrue(map.parseSettingsFromString(input));
        assertEquals(S3RandomMap.RANDOM_MAP_SIZE_2 + "\\" + S3RandomMap.RANDOM_MAP_MIRROR_2, map.toDisplayString());

        input = S3RandomMap.RANDOM_MAP_SIZE_3 + "_1_1";
        map = new S3RandomMap(input);
        assertEquals(input, map.toString());
        assertTrue(map.parseSettingsFromString(input));
        assertEquals(S3RandomMap.RANDOM_MAP_SIZE_3 + "\\" + S3RandomMap.RANDOM_MAP_MIRROR_3, map.toDisplayString());
        
        input = S3RandomMap.RANDOM_MAP_SIZE_4 + "_0_0";
        map = new S3RandomMap(input);
        assertEquals(input, map.toString());
        assertTrue(map.parseSettingsFromString(input));
        assertEquals(S3RandomMap.RANDOM_MAP_SIZE_4 + "\\" + S3RandomMap.RANDOM_MAP_MIRROR_0, map.toDisplayString());
        
        input = S3RandomMap.RANDOM_MAP_SIZE_5 + "_0_0";
        map = new S3RandomMap(input);
        assertEquals(input, map.toString());
        assertTrue(map.parseSettingsFromString(input));
        assertEquals(S3RandomMap.RANDOM_MAP_SIZE_5 + "\\" +S3RandomMap.RANDOM_MAP_MIRROR_0, map.toDisplayString());

        //mit path als input, wird intern hinzugefügt
        input = S3RandomMap.RANDOM_MAP_SIZE_6 + "_0_0";
        map = new S3RandomMap(path + input);
        assertEquals(path + input, map.toString());
        assertTrue(map.parseSettingsFromString(path + input));
        assertEquals(path + S3RandomMap.RANDOM_MAP_SIZE_6 + "\\" +S3RandomMap.RANDOM_MAP_MIRROR_0, map.toDisplayString());
        
        //ungültige maps
        input = "968" + "_0_0";
        map = new S3RandomMap(input);
        assertEquals(S3RandomMap.RANDOM_MAP_SIZE_6 + "_0_0", map.toString());
        assertFalse(map.parseSettingsFromString(input));
        
        input = S3RandomMap.RANDOM_MAP_SIZE_5 + "_2_0";
        map = new S3RandomMap(input);
        assertEquals(S3RandomMap.RANDOM_MAP_SIZE_6 + "_0_0", map.toString());
        assertFalse(map.parseSettingsFromString(input));
        
        input = S3RandomMap.RANDOM_MAP_SIZE_5 + "_1_2";
        map = new S3RandomMap(input);
        assertEquals(S3RandomMap.RANDOM_MAP_SIZE_6 + "_0_0", map.toString());
        assertFalse(map.parseSettingsFromString(input));
        
        input = S3RandomMap.RANDOM_MAP_SIZE_5;
        map = new S3RandomMap(input);
        assertEquals(S3RandomMap.RANDOM_MAP_SIZE_6 + "_0_0", map.toString());
        assertFalse(map.parseSettingsFromString(input));





        
        
        


    }
}
