package net.siedler3.alobby.controlcenter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import net.siedler3.alobby.controlcenter.SaveGame.SavegameFilenameFilter;

import org.junit.Test;

public class SavegameTest
{

    private static final String saveName = "c2e6878f-1c62-4f04-8f12-d979589f4571.mps";

    @Test
    public void testIsBackupSave()
    {
        //invalid
        String fileName = saveName;
        assertFalse(SaveGame.isBackupSave(fileName));
        
        fileName = saveName + ".";
        assertFalse(SaveGame.isBackupSave(fileName));
        
        fileName = saveName + "..";
        assertFalse(SaveGame.isBackupSave(fileName));

        fileName = saveName + ".txt";
        assertFalse(SaveGame.isBackupSave(fileName));
        
        fileName = saveName + ".a";
        assertFalse(SaveGame.isBackupSave(fileName));
        
        fileName = saveName + ".1a";
        assertFalse(SaveGame.isBackupSave(fileName));
        
        fileName = saveName + "..1";
        assertFalse(SaveGame.isBackupSave(fileName));
                
        fileName = saveName + "1";
        assertFalse(SaveGame.isBackupSave(fileName));
        
        //valid
        fileName = saveName + ".1";
        assertTrue(SaveGame.isBackupSave(fileName));
        
        fileName = saveName + ".9";
        assertTrue(SaveGame.isBackupSave(fileName));

        fileName = saveName + ".10";
        assertTrue(SaveGame.isBackupSave(fileName));
        
        fileName = saveName + ".20";
        assertTrue(SaveGame.isBackupSave(fileName));
    }

    @Test
    public void testSavegameFilenameFilter()
    {
        SavegameFilenameFilter filter = new SavegameFilenameFilter();
        
        String fileName = saveName;
        assertTrue(filter.accept(null, fileName));
        
        fileName = saveName + ".1";
        assertTrue(filter.accept(null, fileName));

        fileName = saveName + ".9";
        assertTrue(filter.accept(null, fileName));

        fileName = saveName + ".10";
        assertTrue(filter.accept(null, fileName));
        
        fileName = saveName + ".18";
        assertTrue(filter.accept(null, fileName));
        
        fileName = saveName + ".";
        assertFalse(filter.accept(null, fileName));
        
        fileName = saveName + "1";
        assertFalse(filter.accept(null, fileName));
        
        fileName = saveName + "..1";
        assertFalse(filter.accept(null, fileName));
        
        fileName = saveName + "..11";
        assertFalse(filter.accept(null, fileName));
        
        fileName = saveName + ".1a";
        assertFalse(filter.accept(null, fileName));
    }
}
