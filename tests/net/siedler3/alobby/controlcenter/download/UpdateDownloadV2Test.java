package net.siedler3.alobby.controlcenter.download;

import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import net.siedler3.alobby.controlcenter.SettlersLobby;

import org.junit.Ignore;
import org.junit.Test;

public class UpdateDownloadV2Test
{
    /**
     * Hiermit kann man das downloaden einzelner maps debuggen.
     * Dazu einfach das @Ignore entfernen und als Junit Test laufen lassen.
     */
    @Ignore @Test public void debugMapDownload()
    {
        SettlersLobby settlersLobby = new SettlersLobby();
        UpdateDownloadV2 ud = new UpdateDownloadV2(settlersLobby, "http://lobby.siedler3.net/updateV2/download.php");
        File[] files = ud.downloadSignedFile();
        //Sytem.exit aufrufen, damit die GUI geschlossen wird
        assertNotNull(files[0]);
        assertNotNull(files[1]);
        System.exit(0);
    }

    @Ignore @Test public void callUpdate()
    {
        File installer=new File("c:\\a b\\a.exe");
        String[] path = (System.getProperty("user.dir")+"a b\\c").split(" ");
        path[0] = "/D=" + path[0];
        String[] command = {
            "cmd", //$NON-NLS-1$
            "/c", //$NON-NLS-1$
            "start",
            "/wait",
            installer.getAbsolutePath(),
            "/S",
        };
        ArrayList<String> list = new ArrayList<String>();
        list.addAll(Arrays.asList(command));
        list.addAll(Arrays.asList(path));
        list.add("&&");
        list.add(System.getProperty("user.dir") + "\\alobby.exe");
        try
        {
            ProcessBuilder pb = new ProcessBuilder(list);
            pb.redirectErrorStream(true);
            Process p = pb.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(), "CP850")); //$NON-NLS-1$

            String line;
            while ((line = reader.readLine()) != null)
            {
                System.out.println(line);
            }
        }
        catch (IOException io)
        {
            System.out.println(io.getMessage());
        }
    }
}
