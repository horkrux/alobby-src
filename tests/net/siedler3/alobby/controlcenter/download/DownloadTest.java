package net.siedler3.alobby.controlcenter.download;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class DownloadTest
{
    private File testDir;
    private File dir1;
    private File dir2_1;
    private File dir2_2;
    private File dir3;
    private File file1_1;
    private File file1_2;
    private File file2_1_1;
    private File file2_1_2;
    private File file2_2_1;
    private File file3_1;
    private File file3_2;

    private void createFiles()
    {
        try
        {
            testDir = File.createTempFile("alobby_Test", null);
            testDir.delete();
            testDir.mkdir();
            testDir.deleteOnExit();
            
            dir1 = new File(testDir, "dir1");
            dir1.mkdir();
            dir2_1 = new File(dir1, "dir2_1");
            dir2_1.mkdir();
            dir2_2 = new File(dir1, "dir2_2");
            dir2_2.mkdir();
            dir3 = new File(dir2_1, "dir3");
            dir3.mkdir();
            
            file1_1 = new File(dir1, "file1_1");
            file1_1.createNewFile();
            file1_2 = new File(dir1, "file1_2");
            file1_2.createNewFile();
            file2_1_1 = new File(dir2_1, "file2_1_1");
            file2_1_1.createNewFile();
            file2_1_2 = new File(dir2_1, "file2_1_2");
            file2_1_2.createNewFile();
            file2_2_1 = new File(dir2_2, "file2_2_1");
            file2_2_1.createNewFile();
            file3_1 = new File(dir3, "file3_1");
            file3_1.createNewFile();
            file3_2 = new File(dir3, "file3_2");
            file3_2.createNewFile();
        }
        catch (Exception e) {
        }

        
    }
    @Test
    public void testDeleteFiles()
    {
        List<File> files;
        List<File> filesDeleted;
        List<File> filesExisting;
        createFiles();
        //erst Verzeichnisse, dann Dateien
        files = Arrays.asList(dir1, dir2_1, dir2_2, dir3, 
                file1_1, file1_2, file2_1_1, file2_1_2, file2_2_1, file3_1, file3_2);
        Download.deleteFiles(files);
        checkNotExists(files);
        cleanup();
        
        createFiles();
        //erst Dateien, dann Verzeichnisse
        files = Arrays.asList(file1_1, file1_2, file2_1_1, file2_1_2, file2_2_1, file3_1, file3_2,
                dir1, dir2_1, dir2_2, dir3);
        Download.deleteFiles(files);
        checkNotExists(files);
        cleanup();

        
        createFiles();
        files = Arrays.asList(dir1, file1_1, file1_2, 
                dir2_1, file2_1_1, file2_1_2,
                dir2_2, file2_2_1,
                dir3, file3_1, file3_2);
        Download.deleteFiles(files);
        
        checkNotExists(files);
        cleanup();
        
        createFiles();
        //Tiefensuchmässig gemischt
        files = Arrays.asList(dir1, dir2_1, dir3, file3_1, file3_2, 
                file2_1_1, file2_1_2,
                dir2_2, file2_2_1,
                file1_1, file1_2);
        Download.deleteFiles(files);
        
        checkNotExists(files);
        cleanup();

        
        //Alle Dateien löschen ausser file3_2
        //Dadurch bleiben alle Verzeichnisse auf dem pfad zu file3_2 erhalten
        //alles andere wird gelöscht
        createFiles();
        files = Arrays.asList(dir1, file1_1, file1_2, 
                dir2_1, file2_1_1, file2_1_2,
                dir3, file3_1,
                dir2_2, file2_2_1);
        Download.deleteFiles(files);
        
        filesDeleted = Arrays.asList(file1_1, file1_2, 
                file2_1_1, file2_1_2,
                dir2_2, file2_2_1,
                file3_1);
        
        filesExisting = Arrays.asList(dir1, dir2_1, dir3, file3_2);

        checkNotExists(filesDeleted);
        checkExists(filesExisting);
        cleanup();
        
        
        //Alle Dateien löschen ausser file2_2_1
        //Dadurch bleiben alle Verzeichnisse auf dem pfad zu file2_2_1 erhalten
        //alles andere wird gelöscht
        createFiles();
        files = Arrays.asList(dir1, file1_1, file1_2, 
                dir2_1, file2_1_1, file2_1_2,
                dir3, file3_1, file3_2,
                dir2_2);
        Download.deleteFiles(files);
        
        filesDeleted = Arrays.asList(file1_1, file1_2, 
                dir2_1, file2_1_1, file2_1_2,
                dir3, file3_1, file3_2);
        
        filesExisting = Arrays.asList(dir1, dir2_2, file2_2_1);

        checkNotExists(filesDeleted);
        checkExists(filesExisting);
        cleanup();



    }
    
    private void cleanup()
    {
        deleteFileOrDir(testDir);
        assertFalse(testDir.exists());
        
    }
    
    private void checkNotExists(List<File> files)
    {
        for (File file : files)
        {
            assertFalse(file.exists());
        }
    }
    
    private void checkExists(List<File> files)
    {
        for (File file : files)
        {
            assertTrue(file.exists());
        }
    }
    
    /**
     * Löscht die Datei oder das Verzeichnis. Wenn das Verzeichnis nicht leer ist,
     * werden auch alle darin enthaltenen Dateien und Unterverzeichnisse gelöscht.
     * @param fileOrDir
     * @return
     */
    private static boolean deleteFileOrDir(File fileOrDir)
    {
        if (fileOrDir.isDirectory())
        {
            for (File file : fileOrDir.listFiles())
            {
                boolean success = deleteFileOrDir(file);
                if (!success)
                {
                    return false;
                }
            }
        }
        // The directory is now empty so delete it
        return fileOrDir.delete();
    }


}
