package net.siedler3.alobby.controlcenter.download;

import net.siedler3.alobby.controlcenter.SettlersLobby;
import net.siedler3.alobby.controlcenter.download.MapDownload;

import org.junit.Ignore;
import org.junit.Test;

public class MapDownloadTest
{
    /**
     * Hiermit kann man das downloaden einzelner maps debuggen.
     * Dazu einfach das @Ignore entfernen und als Junit Test laufen lassen.
     */
    @Ignore @Test public void debugMapDownload()
    {
        SettlersLobby settlersLobby = new SettlersLobby();
        MapDownload md = new MapDownload(settlersLobby);
        //String mapName = "10BanditsNF";
        //String mapName = "6er Morgenrunde";
        //String mapName = "Rumble";
        String mapName = "palim";
                md.downloadFile(mapName);
        //Sytem.exit aufrufen, damit die GUI geschlossen wird
        System.exit(0); 
    }
}
