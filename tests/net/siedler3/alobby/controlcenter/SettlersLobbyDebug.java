package net.siedler3.alobby.controlcenter;

import java.io.File;
import java.util.Arrays;

//this is no test at all, just a possibility to hook in
//to create shortcuts to special functions
public class SettlersLobbyDebug
{

    private static SettlersLobby settlersLobby;
    
    public static void debug()
    {
        settlersLobby.getGUI().displayChat();
        //settlersLobby.getGUI().displayMapChoosingDialog();
        //settlersLobby.getGUI().displayError("Fehlermeldung");
        
        //DownloadActions.startAutomaticMapDownload(settlersLobby,"schöner");
        //DownloadActions.openManualMapDownload("schöner siedeln");
        //DownloadActions.downloadMap(settlersLobby, "schöner siedeln");
        //DownloadActions.downloadMap(settlersLobby, "USER\\schöner siedeln ;).map");
        
    }

    
    public static void main(String[] args)
    {
        try
        {
            SettlersLobby.commandLineOptions = new CommandLineOptions(args);
            if (SettlersLobby.commandLineOptions.getConfigDir() != null)
            {
                SettlersLobby.configDir = SettlersLobby.commandLineOptions.getConfigDir();
            }
            try
            {
                File file = new File(SettlersLobby.configDir);
                if (!file.exists())
                {
                    file.mkdirs();
                }
            }
            catch (Exception e)
            {
                Test.outputException(e);
            }

            settlersLobby = new SettlersLobby();
            Test.output("command line arguments: " + Arrays.toString(args));
            
            debug();

        }
        catch (Throwable t)
        {
            Test.outputException(t);
        }
    }


}
