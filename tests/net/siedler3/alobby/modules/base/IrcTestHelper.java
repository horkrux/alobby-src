package net.siedler3.alobby.modules.base;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expectLastCall;
import net.siedler3.alobby.communication.IrcEventListener;
import net.siedler3.alobby.communication.IrcInterface;
import net.siedler3.alobby.communication.IrcServerInfo;
import net.siedler3.alobby.communication.NickServ;
import net.siedler3.alobby.communication.NickServ.IrcServer;
import net.siedler3.alobby.communication.NickServCommand;

import org.easymock.IMocksControl;

abstract public class IrcTestHelper
{
    protected static final String USER1 = "User1";
    protected static final String USER2 = "User2";
    protected ResultEventListener mockResultEventListener;
    protected IrcInterface mockIrcInterface;
    protected IMocksControl ctrl;
    protected IrcServerInfo mockIrcServerInfo;
    protected boolean ircSupportsEmailReg = false;
    protected String nick;
    protected String password;
    protected boolean isConnected;
    protected boolean isSendInfoCommandDuringQueryNickStatus = false;
    protected boolean isSendRegisterCommandDuringQueryNickStatus = true;

    abstract protected IrcEventListener getIrcEventListener();
    abstract protected ResultEventProvider getResultEventProvider();

    public void setUp() throws Exception
    {
        ctrl = createStrictControl();
        mockResultEventListener = ctrl.createMock(ResultEventListener.class);
        mockIrcInterface = ctrl.createMock(IrcInterface.class);
        mockIrcServerInfo = ctrl.createMock(IrcServerInfo.class);

        nick = USER1;
        password = "abcde";
        ircSupportsEmailReg = false;
        NickServ.server = IrcServer.siedler3_net;
        isConnected = true;
    }

    protected void expectIsConnected()
    {
        mockIrcInterface.isConnected();
        expectLastCall().andReturn(this.isConnected);
    }

    protected void expectGetNick()
    {
        mockIrcInterface.getNick();
        expectLastCall().andReturn(this.nick);
    }

    protected void expectStopListening()
    {
        mockIrcInterface.removeEventListener(getIrcEventListener());
        expectLastCall().andReturn(false);
    }

    protected void expectStartListening()
    {
        mockIrcInterface.addEventListener(getIrcEventListener());
    }

    protected ResultEvent createResult(ResultCode specialAuthCodeSent)
    {
        return new ResultEvent(getResultEventProvider(), specialAuthCodeSent);
    }

    protected ResultEvent createResultFailed(String errorMessage)
    {
        return new ResultEvent(getResultEventProvider(), ResultCode.FAILED, errorMessage);
    }

    protected void expectOnDisconnect()
    {
        expectOnResultAborted();
    }

    protected void expectOnServerResponse(int code, String message)
    {

    }

    protected void expectOnResultOk()
    {
        expectStopListening();
        mockResultEventListener.onResultEvent(createResult(ResultCode.OK));
    }

    protected void expectOnResultAborted()
    {
        expectStopListening();
        mockResultEventListener.onResultEvent(createResult(ResultCode.ABORTED));
    }

    protected void expectOnResultFailed(String errorMessage)
    {
        expectStopListening();
        mockResultEventListener.onResultEvent(createResultFailed(errorMessage));
    }

    protected void expectOnNickChange(String newNick)
    {
        mockIrcInterface.setNick(newNick);
        expectQueryNickStatus();
    }

    protected void expectOnWrongNickChange(String newNick)
    {
    }

    protected void expectHandleNickServeStatusReply(String nick)
    {
        //the call to isNickServStatusReply() is already handled
        //in expectOnNotice()
        if (nick.equals(this.nick))
        {
        }
    }
    
    protected void expectIsNickServStatusReply()
    {
        expectGetNick();
    }

    protected void expectOnNotice()
    {
        expectIsNickServe();
        //for every notice, isNickServStatusReply is called
        //so expect this already here
        expectIsNickServStatusReply();
    }

    protected void expectIsNickServe()
    {
        mockIrcInterface.isNickServe("NickServ", "services", "irc.siedler3.net");
        expectLastCall().andReturn(true);
    }

    protected void expectChangeNick(String nick)
    {
        mockIrcInterface.changeNick(nick);
    }

    protected void expectQueryNickStatus()
    {
        mockIrcInterface.getIrcServerInfo();
        expectLastCall().andReturn(mockIrcServerInfo);
        mockIrcServerInfo.isRegistrationRequiresEmailAuthentication();
        expectLastCall().andReturn(ircSupportsEmailReg);
        if (ircSupportsEmailReg)
        {
            if (isSendRegisterCommandDuringQueryNickStatus)
            {
                expectSendNickServCommand(NickServCommand.REGISTER, null);
            }
            if (isSendInfoCommandDuringQueryNickStatus)
            {
                expectSendNickServCommand(NickServCommand.INFO, nick);
            }
        }
        mockIrcInterface.queryNickStatus();
    }

    protected void expectSendNickServCommand(NickServCommand command, String options)
    {
        mockIrcInterface.sendNickServCommand(command, options);
    }

    protected void sleep(int milliseconds)
    {
        try
        {
            Thread.sleep(milliseconds);
        }
        catch (Exception e)
        {
        }
    }


}
