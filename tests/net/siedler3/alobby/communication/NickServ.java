package net.siedler3.alobby.communication;

import java.text.MessageFormat;

/**
 * creates the message of anope NickServ
 * @author jim
 *
 */
public class NickServ
{
    public enum IrcServer
    {
        IRC_Mania(0),
        siedler3_net(1);

        private final int value;
        public int getValue()
        {
            return value;
        }
        private IrcServer(int value)
        {
            this.value = value;
        }
    }

    public static IrcServer server = IrcServer.siedler3_net;

    private static final String[] invalidEmail = {
                    "E-Mail-Adressen müssen der Form Benutzer@Domain sein. Es dürfen keine Kontrollzeichen (Farbwechsel, etc) und keine der folgenden Zeichen benutzt werden: , : ; | \\ \" ( ) < > [ ]",
                    "no email ist keine gültige eMail-Adresse"
    };
    private static final String[] invalidPassword = {
                    "Versuchen Sie es erneut mit einem ungewöhnlicheren Passwort. Passwörter sollten mindestens fünf Zeichen lang sein und nicht zu leicht zu erraten sein.",
                    "Passworte sollten mindestens 5 Zeichen lang sein und"
    };
    private static final String[] wrongPassword = {
        "Falsches Passwort.",
        "Falsches Passwort."
    };

    private static final String[] emailSent = {
        null,
        "Ein Password wurde zu {0} gesendet, bitte tippe /msg NickServ confirm <passcode> um die Registrierung abzuschließen"
    };

    private static final String[] requiresAuthCode = {
        null,
        "Dieser Nickname wurde bereits angefordert, bitte prüfe deinen Email Account für das Passwort."
    };


    public static String createInvalidEmail()
    {
        return invalidEmail[server.getValue()];
    }

    public static String createStatus(String nick, int status)
    {
        return "STATUS " + nick + " " + status;
    }

    public static String createPasswordTooShort()
    {
        return invalidPassword[server.getValue()];
    }

    public static String createEmailSent(String email)
    {
        return MessageFormat.format(emailSent[server.getValue()], email);
    }

    public static String createRequiresAuthCode(String email)
    {
        return requiresAuthCode[server.getValue()];
    }

    public static String createWrongPassword()
    {
        return wrongPassword[server.getValue()];
    }


}
