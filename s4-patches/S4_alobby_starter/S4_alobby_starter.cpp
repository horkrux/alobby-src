/*
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include "stdafx.h"
#include "detours.h"
#include <Tlhelp32.h>
#include <vector>
#include <windows.h>
#include <stdio.h> 
#include <stdexcept>
#include <process.h> 
#include <iostream>
#include <winsock.h>

#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "detours.lib")

using namespace std;

#define DLL_PATH "S4_alobby.dll" // The DLL we would like to inject
#define EXE_PATH "S4_alobby.exe" // The EXE we would like to use


bool InjectDll(int argc, _TCHAR* argv[])
{
	//////////////////////////////////////////////////////
	// Output the found IPs for debugging
	// :TODO: If we can't find here the IP requested in the ".ip"-file nor the VPN-IP,
	//	      then we should break the execution here.
    char hostname[255];
    struct hostent *he;
    struct in_addr **addr_list;

    WSAData data;
    WSAStartup(MAKEWORD(2, 2), &data);

    gethostname(hostname, 255);
    std::cout << "Host name is: " << hostname << std::endl;

    if ((he = gethostbyname(hostname)) == NULL) {
		LPCWSTR err_title = L"Fatal Error";
		LPCWSTR err_msg = L"gethostbyname() returned NULL!";
		MessageBox(
			GetDesktopWindow(),
			err_msg,
			err_title,
			MB_OK | MB_ICONERROR | MB_SYSTEMMODAL
			);
    } else {
        std::cout << "Found IP addresses in the following order: "  << std::endl;
        addr_list = (struct in_addr **)he->h_addr_list;
        for(int i = 0; addr_list[i] != NULL; i++) {
            std::cout << inet_ntoa(*addr_list[i]) << std::endl;
        }
    }

	std::cout << "Executing S4_alobby.exe..." << std::endl;

	STARTUPINFO si;
	PROCESS_INFORMATION pi;
 
	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);
	ZeroMemory( &pi, sizeof(pi) );
 
	WCHAR buf[MAX_PATH+1]={0};
	if (argc > 1) {
		std::cout << "Calling 'S4_alobby.exe netgame=1' " << std::endl;
		wcscpy_s(buf, MAX_PATH, L"S4_alobby.exe netgame=1");
	} else {
		std::cout << "Calling 'S4_alobby.exe' " << std::endl;
		wcscpy_s(buf, MAX_PATH, L"S4_alobby.exe");
	}
 
	bool detourProcess = DetourCreateProcessWithDllW(
		NULL, buf, NULL,
		NULL, FALSE, CREATE_DEFAULT_ERROR_MODE, NULL, NULL,
		&si, &pi, DLL_PATH, NULL);
 
	if(detourProcess){
		std::cout << "S4_alobby.exe process has PID " << pi.dwProcessId << std::endl;
		std::cout << DLL_PATH << " successfully patched into the S4_alobby.exe!" << std::endl;
		// Hide the DOS window now
		FreeConsole();
		DWORD exitCode = 0;
		for (;;)
		{
			if (GetExitCodeProcess(pi.hProcess, &exitCode) == FALSE) {
				std::cout << "WTF" << std::endl;
				//system("PAUSE");
			}
			if (exitCode != STILL_ACTIVE) {
				break;
			}
			Sleep(1000);
		}
		return true;
	} else {
		std::cout << "Failed to inject DLL: " << DLL_PATH << std::endl;
		LPCWSTR err_title = L"Fatal Error";
		LPCWSTR err_msg = L"Failed to patch the S4_alobby.dll into the S4_alobby.exe process!";
		MessageBox(
			GetDesktopWindow(),
			err_msg,
			err_title,
			MB_OK | MB_ICONERROR | MB_SYSTEMMODAL
			);
		//system("PAUSE");
		return false;
	}
}


int _tmain(int argc, _TCHAR* argv[])
{
	// First lets check if the file we want to execute does exist at all..
	std::string name = EXE_PATH;
	LPCWSTR err_title = L"Fatal Error";
	LPCWSTR err_msg = L"File S4_alobby.exe was not found!";
	if (FILE *file = fopen(name.c_str(), "r")) {
		fclose(file);
    } else {
		// Hide the DOS window now
		FreeConsole();
		MessageBox(
			GetDesktopWindow(),
			err_msg,
			err_title,
			MB_OK | MB_ICONERROR | MB_SYSTEMMODAL
			);
        return 2;
	}

	std::cout << "Patching S4_alobby.dll into the S4_alobby.exe..." << std::endl;
	std::cout << "number of command line arguments received: " << argc << std::endl;
	if (InjectDll(argc, argv))
	{
		return 0;
	}
	else
	{
		std::cout << "FAIL: Please see above for errors." << std::endl;
		return 1;
	}
}

