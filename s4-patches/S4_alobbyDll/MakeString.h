#pragma once

#include <string>
#include <sstream>

template <class T>
T& make_ref(T const& t)
{
    return const_cast<T&>(t);
}

#ifdef _UNICODE
	#define MAKE_STRING(msg) (static_cast<std::wostringstream&>(make_ref(std::wostringstream()) << msg).str())
#else
	#define MAKE_STRING(msg) (static_cast<std::ostringstream&>(make_ref(std::ostringstream()) << msg).str())
#endif

