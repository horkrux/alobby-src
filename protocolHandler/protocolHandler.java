/*
 * This file is part of the aLobby Project.
 *
 * Copyright (C) 2008-2019 siedler3.net aLobby Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

/**
 * Handler for calls from alobby-protocol, e.g.:
 * alobby://mapbase.siedler3.net/file/16er_Asbestfest.map
 * 
 * Checks which url is called and send it to running aLobby via named pipe.
 * 
 * @author Zwirni
 */
public class protocolHandler
{
	
	// define translations for all texts the user could get
	@SuppressWarnings("rawtypes")
	private static Map<String, HashMap> translations = new HashMap<String, HashMap>() {{
		put("de", new HashMap<String, String>() {{
			put("noargs", "Keine Argumente angegeben");
			put("notinstalled", "Bitte installiere die aLobby um dieses Tool zu nutzen!");
			put("notrunning", "Bitte starte die aLobby um den Download abschließen zu können!");
			put("pressenter", "Press enter to close this window!");
		}});
		put("en", new HashMap<String, String>() {{
			put("noargs", "No arguments given!");
			put("notinstalled", "Please install the aLobby to use this tool!");
			put("notrunning", "aLobby is not running!");
			put("pressenter", "Press enter to close this window!");
		}});
	}};
	
	// set the actual language
	private static Locale locale;

	// set the actual translations
	@SuppressWarnings("rawtypes")
	private static HashMap translation;		
	
	/**
	 * Start protocolHandler which is called via browser-request.
	 * 
	 * @param args	String-Array with the called url as parameter 0
	 */
	public static void main(String[] args) {
		
		// detect the system language
		locale = new Locale("en");;
		if( System.getProperty("user.language").length() > 0 && System.getProperty("user.country").length() > 0 ) {
    		locale = new Locale(System.getProperty("user.language"));
    	}
		
		translation = translations.get("en");
		// get translations for given language
		if( translations.containsKey(locale.toString()) )  {
			translation = translations.get(locale.toString());
		}
		
		// if no arguments are given do nothing
		if (args.length == 0) {
			System.out.println(translation.get("noargs").toString());
			return;
		}
		
		// check if aLobby exists in userdir
		if( false == new File(getUserDir()).exists() ) {
			outputError(translation.get("notinstalled").toString());
			return;
		}
		
		// check if aLobby is running and stop processing if not
		if( false == new File(getUserDir(), "alobby.lock").exists() ) {
			outputError(translation.get("notrunning").toString());
			return;
		}
		
		try {
			// Connect to the named pipe of the aLobby
			RandomAccessFile pipe = new RandomAccessFile("\\\\.\\pipe\\alobby", "rw");
 
			// set the content to write to the named pipe as simple JSON-String
			String echoText = "{ \"URL\": \"" + args[0] + "\" }";
			
			// write the bytes of the string to named pipe
			pipe.write ( echoText.getBytes() );
			
			// close the connection
			pipe.close();
		} catch (FileNotFoundException f ) {
			outputError("The named pipe of aLobby is not available. Possibly aLobby not running?");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Output an error-message to user in console-window. 
	 */
	private static void outputError( String message ) {
		System.out.println(message);
		System.out.println(translation.get("pressenter").toString());
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in); 
		scanner.nextLine();
	}
	
	/**
	 * Get the aLobby-user-directory
	 * 
	 * @return String	absolute path to the directory
	 */
	private static String getUserDir()
    {
        // this only works on windows systems
        String result = System.getenv("APPDATA");

        if (result == null || result.isEmpty())
        {
            // use current working directory instead (old behaviour)
            result = System.getProperty("user.dir");
        }
        else
        {
            result = new File(result, "alobby").getAbsolutePath();
        }
        return result;
    }
	
}